This directory is a cache for all MIBs downloaded by the snmp_exporter generator Makefile. This cache is needed as not all servers allow us to download the MIB for every build.
