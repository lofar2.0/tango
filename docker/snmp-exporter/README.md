SNMP monitoring
======================

The `snmp-exporter` module allows SNMP devices to be periodically queried to obtain Prometheus metrics describing their state.

This setup relies on the following parts:

* `docker/snmp-exporter/`, a daemon derived from `prom/snmp-exporter` that allows on-demand querying of SNMP devices and is extended with custom LOFAR MIBs,
* `infra/jobs/station/snmp-exporter.levant.nomad`, the Nomad job file to run `snmp-exporter`,
* `infra/consul/consul.hcl.j2`, which configures settings for SNMP "services" present,
* `infra/jobs/station/monitoring.levant.nomad`, the Nomad job file that has Prometheus query `snmp-exporter` for each SNMP "service".
