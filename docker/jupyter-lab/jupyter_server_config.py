#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

c = get_config()
c.ServerApp.base_url = "/jupyter"
c.PasswordIdentityProvider.password_required = False
c.IdentityProvider.token = ""
