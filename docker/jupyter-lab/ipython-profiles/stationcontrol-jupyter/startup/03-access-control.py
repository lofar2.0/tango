# Manage read/write access to our proxies based on those included in the "devices" global


class WriteAccess:
    """Allow restricting write access from DeviceProxies.

    Use `with write_access: ...` to temporarily lift write restrictions,
    or `write_access.enable()` to lift it permanently.

    Affects all proxies registered in the global `devices` list.

    Disabling write access:
    * Blocks writing attributes
    * Blocks calling commands."""

    @property
    def devices(self):
        """Devices considered for access control."""
        global devices

        return devices

    def enable(self):
        """Enable write access to all devices."""
        for d in self.devices:
            d.set_access_control(AccessControlType.ACCESS_WRITE)

    def disable(self):
        """Disable write access to all devices."""
        for d in self.devices:
            d.set_access_control(AccessControlType.ACCESS_READ)

    def __enter__(self):
        self.enable()

    def __exit__(self, type, value, traceback):
        self.disable()

    @staticmethod
    def patch_tango():
        """Monkey-patch tango to block command execution if the proxy has no write access."""
        import tango.device_proxy

        _orig_get_command_func = getattr(tango.device_proxy, "__get_command_func")

        def new_get_command_func(dp, cmd_info, name):
            if dp.get_access_control() == AccessControlType.ACCESS_READ:
                Except.throw_exception(
                    "API_ReadOnlyMode",
                    f"Calling command(s) on device {dp.name().lower()} is not authorized",
                    "",
                )
            return _orig_get_command_func(dp, cmd_info, name)

        setattr(tango.device_proxy, "__get_command_func", new_get_command_func)


write_access = WriteAccess()

# Disable write access for commands etc
write_access.patch_tango()

# Disable write access by default
write_access.disable()
