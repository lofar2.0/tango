from IPython.display import display, Markdown

LOFAR_HELP_TEXT = """
# Welcome to this LOFAR station!

The `devices` list contains all devices known. They are in read-only mode by default. To write to attributes or call commands on them, please
call `write_access.enable()` or `with write_access: ...` in your notebook.

For each device, the following functions may prove useful:

* Attributes: `device.get_attribute_list()` to list all attributes, and access `device.ATTRIBUTE` to read or write its value,
* `device.get_command_list()` to list all commands, and use `device.COMMAND(...)` to call one,
* `device.get_property_list("*")` to list all properties, and use `device.get_property(PROPERTY)[PROPERTY]` to obtain their value.
"""


def lofar_help():
    display(Markdown(LOFAR_HELP_TEXT))
