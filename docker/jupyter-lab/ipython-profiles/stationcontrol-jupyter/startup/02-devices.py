# Create shortcuts for our devices, if they exist

from tangostationcontrol.common.proxies.proxy import create_device_proxy


def OptionalDeviceProxy(device_name: str):
    """Return a DeviceProxy for the given device, or None."""
    try:
        return create_device_proxy(device_name)
    except DevFailed:
        # device is not in database, or otherwise not reachable
        return None


aps_l0 = OptionalDeviceProxy("STAT/APSCT/L0")
aps_l1 = OptionalDeviceProxy("STAT/APSCT/L1")
aps_h0 = OptionalDeviceProxy("STAT/APSCT/H0")
apss = [aps_l0, aps_l1, aps_h0]

apsct_l0 = OptionalDeviceProxy("STAT/APSCT/L0")
apsct_l1 = OptionalDeviceProxy("STAT/APSCT/L1")
apsct_h0 = OptionalDeviceProxy("STAT/APSCT/H0")
apscts = [apsct_l0, apsct_l1, apsct_h0]

apspu_l0 = OptionalDeviceProxy("STAT/APSPU/L0")
apspu_l1 = OptionalDeviceProxy("STAT/APSPU/L1")
apspu_h0 = OptionalDeviceProxy("STAT/APSPU/H0")
apspus = [apspu_l0, apspu_l1, apspu_h0]

recvl_l0 = OptionalDeviceProxy("STAT/RECVL/L0")
recvl_l1 = OptionalDeviceProxy("STAT/RECVL/L1")
recvh_h0 = OptionalDeviceProxy("STAT/RECVH/H0")
recvs = [recvl_l0, recvl_l1, recvh_h0]

unb2_l0 = OptionalDeviceProxy("STAT/UNB2/L0")
unb2_l1 = OptionalDeviceProxy("STAT/UNB2/L1")
unb2_h0 = OptionalDeviceProxy("STAT/UNB2/H0")
unb2s = [unb2_l0, unb2_l1, unb2_h0]

sdpfirmware_l = OptionalDeviceProxy("STAT/SDPFirmware/LBA")
sdp_l = OptionalDeviceProxy("STAT/SDP/LBA")
bst_l = OptionalDeviceProxy("STAT/BST/LBA")
sst_l = OptionalDeviceProxy("STAT/SST/LBA")
xst_l = OptionalDeviceProxy("STAT/XST/LBA")
beamlet_l = OptionalDeviceProxy("STAT/Beamlet/LBA")
digitalbeam_l = OptionalDeviceProxy("STAT/DigitalBeam/LBA")
antennafield_l = af_l = OptionalDeviceProxy("STAT/AFL/LBA")

sdpfirmware_h = OptionalDeviceProxy("STAT/SDPFirmware/HBA")
sdp_h = OptionalDeviceProxy("STAT/SDP/HBA")
bst_h = OptionalDeviceProxy("STAT/BST/HBA")
sst_h = OptionalDeviceProxy("STAT/SST/HBA")
xst_h = OptionalDeviceProxy("STAT/XST/HBA")
beamlet_h = OptionalDeviceProxy("STAT/Beamlet/HBA")
digitalbeam_h = OptionalDeviceProxy("STAT/DigitalBeam/HBA")
tilebeam_h = OptionalDeviceProxy("STAT/TileBeam/HBA")
antennafield_h = af_h = OptionalDeviceProxy("STAT/AFH/HBA")

sdpfirmware_h0 = OptionalDeviceProxy("STAT/SDPFirmware/HBA0")
sdp_h0 = OptionalDeviceProxy("STAT/SDP/HBA0")
bst_h0 = OptionalDeviceProxy("STAT/BST/HBA0")
sst_h0 = OptionalDeviceProxy("STAT/SST/HBA0")
xst_h0 = OptionalDeviceProxy("STAT/XST/HBA0")
beamlet_h0 = OptionalDeviceProxy("STAT/Beamlet/HBA0")
digitalbeam_h0 = OptionalDeviceProxy("STAT/DigitalBeam/HBA0")
tilebeam_h0 = OptionalDeviceProxy("STAT/TileBeam/HBA0")
antennafield_h0 = af_h0 = OptionalDeviceProxy("STAT/AFH/HBA0")

sdpfirmware_h1 = OptionalDeviceProxy("STAT/SDPFirmware/HBA1")
sdp_h1 = OptionalDeviceProxy("STAT/SDP/HBA1")
bst_h1 = OptionalDeviceProxy("STAT/BST/HBA1")
sst_h1 = OptionalDeviceProxy("STAT/SST/HBA1")
xst_h1 = OptionalDeviceProxy("STAT/XST/HBA1")
beamlet_h1 = OptionalDeviceProxy("STAT/Beamlet/HBA1")
digitalbeam_h1 = OptionalDeviceProxy("STAT/DigitalBeam/HBA1")
tilebeam_h1 = OptionalDeviceProxy("STAT/TileBeam/HBA1")
antennafield_h1 = af_h1 = OptionalDeviceProxy("STAT/AFH/HBA1")

stationmanager = OptionalDeviceProxy("STAT/StationManager/1")
ccd = OptionalDeviceProxy("STAT/CCD/1")
ec = OptionalDeviceProxy("STAT/EC/1")
pcon = OptionalDeviceProxy("STAT/PCON/1")
protectioncontrol = OptionalDeviceProxy("STAT/ProtectionControl/1")
configuration = OptionalDeviceProxy("STAT/Configuration/1")
calibration = OptionalDeviceProxy("STAT/Calibration/1")
observationcontrol = OptionalDeviceProxy("STAT/ObservationControl/1")
metadata = OptionalDeviceProxy("STAT/Metadata/1")

# Put them in a list in case one wants to iterate
devices = (
    [
        stationmanager,
        metadata,
        observationcontrol,
        calibration,
        ccd,
        ec,
        pcon,
        protectioncontrol,
        configuration,
        sdpfirmware_l,
        sdp_l,
        bst_l,
        sst_l,
        xst_l,
        beamlet_l,
        digitalbeam_l,
        af_l,
        sdpfirmware_h,
        sdp_h,
        bst_h,
        sst_h,
        xst_h,
        beamlet_h,
        digitalbeam_h,
        tilebeam_h,
        af_h,
        sdpfirmware_h0,
        sdp_h0,
        bst_h0,
        sst_h0,
        xst_h0,
        beamlet_h0,
        digitalbeam_h0,
        tilebeam_h0,
        af_h0,
        sdpfirmware_h1,
        sdp_h1,
        bst_h1,
        sst_h1,
        xst_h1,
        beamlet_h1,
        digitalbeam_h1,
        tilebeam_h1,
        af_h1,
    ]
    + apss
    + apscts
    + apspus
    + recvs
    + unb2s
)

# Filter out devices that do not exist
devices = [d for d in devices if d is not None]
