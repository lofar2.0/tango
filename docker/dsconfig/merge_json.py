#!/usr/bin/env python3

"""Merge all JSON files given on the command line at top level."""

import json

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        "Merge input JSON files at top level. Keys from later files override those from earlier files."
    )
    parser.add_argument(
        "files", metavar="FILE", type=str, nargs="+", help="JSON input files"
    )
    args = parser.parse_args()

    result = {}

    # read all provided files
    for filename in args.files:
        with open(filename) as f:
            file_dict = json.load(f)

            # add them to the result
            result.update(file_dict)

    # print result in JSON
    print(json.dumps(result, indent=4))
