#!/usr/bin/env python3
"""

Import/export the object propertis of the Tango Controls Database.

"""

from tango import Database


def read_objects(db=None) -> dict:
    """Read and return all object properties."""

    db = db or Database()

    result = {}

    objects = db.get_object_list("*").value_string

    for obj in objects:
        result[obj] = {}
        properties = db.get_object_property_list(obj, "*").value_string

        for prop in properties:
            value = db.get_property(obj, prop)[prop]

            result[obj][prop] = list(value)

    return result


def write_objects(objects: dict, db=None) -> None:
    """Write the given object properties."""

    db = db or Database()

    for obj, properties in objects.items():
        db.put_property(obj, properties)


if __name__ == "__main__":
    import sys
    import argparse
    import json

    parser = argparse.ArgumentParser(
        "Import/export object properties of the Tango Database using the JSON file format"
    )
    parser.add_argument(
        "-w",
        "--write",
        default=False,
        required=False,
        action="store_true",
        help="import objects from stdin",
    )
    parser.add_argument(
        "-r",
        "--read",
        default=False,
        required=False,
        action="store_true",
        help="export all objects to stdout in JSON",
    )
    args = parser.parse_args()

    if not args.read and not args.write:
        parser.print_help()
        sys.exit(1)

    # import
    if args.write:
        objects = json.load(sys.stdin)
        if "objects" in objects:
            write_objects(objects["objects"])

    # export
    if args.read:
        objects = read_objects()
        print(json.dumps({"objects": objects}, indent=4))
