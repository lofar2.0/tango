import asyncio
import copy
import logging
from datetime import datetime
import time
from math import sin


from asyncua import ua, uamethod, Server


_logger = logging.getLogger(__name__)


class SubHandler(object):
    """
    Subscription Handler. To receive events from server for a subscription
    """

    def datachange_notification(self, node, val, data):
        _logger.warn("Python: New data change event %s %s", node, val)

    def event_notification(self, event):
        _logger.warn("Python: New event %s", event)


async def main():
    server = Server()
    await server.init()
    # server.disable_clock()  #for debuging
    # server.set_endpoint("opc.tcp://localhost:4840/freeopcua/server/")
    server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")
    server.set_server_name("Environmental Control (EC) Simulator")
    # set all possible endpoint policies for clients to connect through
    server.set_security_policy(
        [
            ua.SecurityPolicyType.NoSecurity,
            ua.SecurityPolicyType.Basic256Sha256_SignAndEncrypt,
            ua.SecurityPolicyType.Basic256Sha256_Sign,
        ]
    )

    # setup our own namespace
    # uri = "http://examples.freeopcua.github.io"
    idx2 = await server.register_namespace(
        "urn:SIMATIC.S7-1200.OPC-UA.Application:PLC_1"
    )
    idx3 = await server.register_namespace("http://opcfoundation.org/UA/DI/")
    idx4 = await server.register_namespace("http://Environmental_Control")

    server_interfaces = await server.nodes.objects.add_folder(idx3, "ServerInterfaces")
    obj = await server_interfaces.add_object(idx4, "Environmental_Control")

    await obj.add_variable(idx4, "Temperature_Cab0.0_R", 20.0)
    await obj.add_variable(idx4, "Temperature_Cab0.1_R", 20.0)
    await obj.add_variable(idx4, "Temperature_Cab1.0_R", 20.0)
    await obj.add_variable(idx4, "Temperature_Cab1.1_R", 20.0)
    await obj.add_variable(idx4, "Door_Closed_Cab0.0_R", False)
    await obj.add_variable(idx4, "Door_Closed_Cab0.1_R", False)
    await obj.add_variable(idx4, "Door_Closed_Cab1.0_R", False)

    var = await obj.add_variable(idx4, "Control_Relais_48V_RW", False)
    await var.set_writable()
    var = await obj.add_variable(idx4, "Control_Relais_Heater_RW", False)
    await var.set_writable()
    var = await obj.add_variable(idx4, "Control_Relais_PSOC_RW", False)
    await var.set_writable()

    await obj.add_variable(idx4, "Trigger_Shut_Off_48V_R", False)
    await obj.add_variable(idx4, "Trigger_Shut_Off_PSOC_R", False)

    var = await obj.add_variable(idx4, "Setpoint_Temperature_PID_RW", 23.0)
    await var.set_writable()
    var = await obj.add_variable(idx4, "Fan_Speed_Door_Open_RW", 250.0)
    await var.set_writable()
    var = await obj.add_variable(idx4, "First_Trip_Temperature_RW", 41.0)
    await var.set_writable()
    var = await obj.add_variable(idx4, "Second_Trip_Temperature_RW", 42.0)
    await var.set_writable()
    var = await obj.add_variable(idx4, "InnerFans_Cab0_RPM_RW", 2500.0)
    await var.set_writable()
    var = await obj.add_variable(idx4, "InnerFans_Cab0_RPM_R", 2500.0)

    # creating a default event object
    # The event object automatically will have members for all events properties
    # you probably want to create a custom event type, see other examples
    myevgen = await server.get_event_generator()
    myevgen.event.Severity = 300

    # starting!
    async with server:
        print("Available loggers are: ", logging.Logger.manager.loggerDict.keys())
        # enable following if you want to subscribe to nodes on server side
        # handler = SubHandler()
        # sub = await server.create_subscription(500, handler)
        # handle = await sub.subscribe_data_change(myvar)
        # trigger event, all subscribed clients wil receive it

        while True:
            await asyncio.sleep(0.1)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    # optional: setup logging
    # logger = logging.getLogger("asyncua.address_space")
    # logger.setLevel(logging.DEBUG)
    # logger = logging.getLogger("asyncua.internal_server")
    # logger.setLevel(logging.DEBUG)
    # logger = logging.getLogger("asyncua.binary_server_asyncio")
    # logger.setLevel(logging.DEBUG)
    # logger = logging.getLogger("asyncua.uaprocessor")
    # logger.setLevel(logging.DEBUG)

    asyncio.run(main())
