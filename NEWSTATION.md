# How to add a new station

When deploying to a new station, for example CS123, you will need to:

* `infra/station`: Deploy the LCU as cs123c.control.lofar using the Ansible playbooks,
* `.gitlab_ci.yml`: Add "cs123" as a target to the `deploy_nomad` job, and deploy,
* `CBD/stations/CS123.json`: Create and populate the station-specific Tango configuration. To generate this from LOFAR1, run `tangostationcontrol/tangostationcontrol/toolkit/generate-cdb-from-lofar1.py -s cs123 -C > CDB/stations/CS123.json`,
* Upload the CDB files through http://cs123c.control.lofar:4646/ui/jobs/dsconfig@default/dispatch, see the [CDB Readme](CDB/stations/README.md) for the list files.
* Obtain calibration tables and upload them to MinIO at http://cs123c.control.lofar:9001/browser/caltables/CS123. To generate them from LOFAR1, run `tangostationcontrol/tangostationcontrol/toolkit/generate-caltable-from-lofar1.py -s cs123`.

To integrate it further into LOFAR, you will need to:

* TMSS: Add it to the LOFAR2 station group.
* COBALT: Update `GPUProc/etc/parset-additions.d/default/StationStreams.parset` to receive the station using `udp+sdp` instead of `udp`.
* lcs129: Make sure `rpc.service.lofar-cs123.consul` is reachable from `lcs129.control.lofar`.
* Run an observation to check whether everything is reachable and working.
