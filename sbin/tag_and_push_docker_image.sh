#!/bin/bash -e
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

function usage {
    echo "./$(basename "$0")
      no arguments, downloads remote images and pushes these to ASTRON registry."
    echo ""
    echo "./$(basename "$0") -h
      displays this help message"
    echo ""
    echo "./$(basename "$0") <docker service name> <tag>
      downloads latest version of image from the ASTRON registry, builds the
      specified service and pushes the image with the specified tag to the
      ASTRON registry"
    echo ""
    echo "./$(basename "$0") pull <tag>
      downloads all images for the integration test with the specified tag
      falling back to 'latest' if unavailable. Should neither exist on the
      ASTRON registry the script will exit 1. The images are retagged to match
      the output of docker."
}

SKA_DOCKER_REGISTRY_HOST=artefact.skao.int
SKA_DOCKER_REGISTRY_USER=ska-tango-images

# list of arguments expected in the input
optstring=":h"

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

if [ -z "${LOFAR20_DIR+x}" ]; then
  echo "LOFAR20_DIR not set, did you forget to source setup.sh?"
  exit 1
fi

if [ -z "$(which shyaml)" ]; then
  echo "Shyaml not found!, install using: 'pip install shyaml'"
  exit 1
fi


LOCAL_DOCKER_REGISTRY=$(shyaml get-value registry.astron.url < "${LOFAR20_DIR}/infra/env/common.yaml")

POSTGRES_VERSION=$(shyaml get-value monitoring.db.version < "${LOFAR20_DIR}/infra/env/common.yaml")
MINIO_VERSION=$(shyaml get-value object_storage.minio.version < "${LOFAR20_DIR}/infra/env/common.yaml")
MINIO_CLIENT_VERSION=$(shyaml get-value object_storage.mc.version < "${LOFAR20_DIR}/infra/env/common.yaml")

TANGO_DSCONFIG_VERSION=$(shyaml get-value tango.dsconfig.version < "${LOFAR20_DIR}/infra/env/common.yaml")
TANGO_ITANGO_VERSION=$(shyaml get-value tango.itango.version < "${LOFAR20_DIR}/infra/env/common.yaml")
TANGO_CPP_VERSION=$(shyaml get-value tango.cpp.version < "${LOFAR20_DIR}/infra/env/common.yaml")
TANGO_DB_VERSION=$(shyaml get-value tango.db.version < "${LOFAR20_DIR}/infra/env/common.yaml")
TANGO_DATABASEDS_VERSION=$(shyaml get-value tango.databaseds.version < "${LOFAR20_DIR}/infra/env/common.yaml")
TANGO_REST_VERSION=$(shyaml get-value tango.rest.version < "${LOFAR20_DIR}/infra/env/common.yaml")

PROMETHEUS_VERSION=$(shyaml get-value monitoring.prometheus.version < "${LOFAR20_DIR}/infra/env/common.yaml")
LOKI_VERSION=$(shyaml get-value monitoring.loki.version < "${LOFAR20_DIR}/infra/env/common.yaml")

# List of images and their tag from ska repository
REMOTE_SKA_IMAGES=(
  "tango-dsconfig:${TANGO_DSCONFIG_VERSION}"
  "tango-itango:${TANGO_ITANGO_VERSION}"
  "tango-cpp:${TANGO_CPP_VERSION}"
  "tango-db:${TANGO_DB_VERSION}"
  "tango-databaseds:${TANGO_DATABASEDS_VERSION}"
  "tango-rest:${TANGO_REST_VERSION}"
)

# List of images and their tag from dockerhub
REMOTE_IMAGES=(
  "vector 0.32.1.custom.989ad14-distroless-static timberio"
  "postgres ${POSTGRES_VERSION}"
  "levant latest hashicorp"
  "consul latest hashicorp"
  "minio ${MINIO_VERSION} minio"
  "mc ${MINIO_CLIENT_VERSION} minio"
  "loki ${LOKI_VERSION} grafana"
  "prometheus ${PROMETHEUS_VERSION} prom"
)

# Triple tuple of docker names, image names and if necessary for
# integration tests.
# TODO(Corne): Have this list generated from the .yml files
LOCAL_IMAGES=(
  "lofar-device-base lofar-device-base y"

  "dsconfig dsconfig y"

  "ec-sim ec-sim y"
  "grafana grafana y"
  "jupyter-lab jupyter-lab n"
  "integration-test integration-test n"
  "snmp-exporter snmp-exporter n"
  "landing-page landing-page n"
)

# LOCAL_IMAGES verifier, this has been going wrong a couple of times
# Ensure that LOCAL_IMAGES entries are all tuples with three elements!
oldstate=${*:1:3}
for image in "${LOCAL_IMAGES[@]}"; do
  # shellcheck disable=SC2086
  set -- $image
  if [ -z "${1}" ]; then
    echo "LOCAL_IMAGES element '${image}' tuple insufficient elements (0/3)"
    exit 1
  fi
  if [ -z "${2}" ]; then
    echo "LOCAL_IMAGES element '${image}' tuple insufficient elements (1/3)"
    exit 1
  fi
  if [ -z "${3}" ]; then
    echo "LOCAL_IMAGES element '${image}' tuple insufficient elements (2/3)"
    exit 1
  fi
done

# Restore arguments such as $1, $2 etc
if [[ "${#oldstate[@]}" == 0 ]]; then
  set --
else
  # shellcheck disable=SC2086
  set -- $oldstate
fi

# If first argument of bash script not set run first stage
if [ -z "${1+x}" ]; then
  echo "Pulling and retagging remote images"

  # Iterate over al the REMOTE_IMAGES and pull them from remote and push local
  for image in "${REMOTE_SKA_IMAGES[@]}"; do
    local_url="${LOCAL_DOCKER_REGISTRY}/${image}"
    remote_url="${SKA_DOCKER_REGISTRY_HOST}/${SKA_DOCKER_REGISTRY_USER}-${image}"
    docker pull "${remote_url}"
    docker tag "${remote_url}" "${local_url}"
    docker push "${local_url}"
  done

  for image in "${REMOTE_IMAGES[@]}"; do
    # shellcheck disable=SC2086
    set -- $image
    local_url="${LOCAL_DOCKER_REGISTRY}/${1}:${2}"
    if [ -n "${3+x}" ]; then
      remote_url="${3}/${1}:${2}"
    else
      remote_url="${1}:${2}"
    fi
    docker pull "${remote_url}"
    docker tag "${remote_url}" "${local_url}"
    docker push "${local_url}"
  done

  exit 0
fi

# If first argument set run second stage, determine LOCAL_IMAGE to build and
# push from the argument
if [ -n "${1+x}" ] && [ "${1}" != "pull" ]; then

  # The second argument must pass the tag variable must be set
  if [ -z "${2+x}" ]; then
    echo "Error, second argument must pass tag variable"
    exit 1
  fi

  # Set the tag and image variable, variables $1 and $2 are shadowed later
  local_image="${1}"
  tag="${2}"

  cd "${LOFAR20_DIR}/docker" || exit 1

  # Loop through images and find the specified one
  for image in "${LOCAL_IMAGES[@]}"; do
    # Set, splits tuple into $1, $2 and $3. this shadows previous variables
    # shellcheck disable=SC2086
    set -- $image
    if [ "${local_image}" == "${1}" ]; then
      echo "Building image for ${1} container"
      local_url="${LOCAL_DOCKER_REGISTRY}/${2}"

      # If tag is not latest, than it is not a tagged master build and we can
      # pull the latest image as cache (if it already exists).
      if [ "${tag}" != "latest" ]; then
        docker pull "${local_url}:${tag}" || docker pull "${local_url}:latest" || true
        if [ -n "$(docker image inspect \""${local_url}":"${tag}"\")" ]; then
          docker tag "${local_url}:latest" "${local_url}:${tag}"
        fi
      fi

      TAG="$tag" NO_BUILD=1 make "${1}" || exit 1
      docker tag "${2}" "${local_url}:${tag}" || docker tag "${2/_/-}" "${local_url}:${tag}"
      docker push "${local_url}:${tag}"
    fi
  done

  exit 0
fi

# Final stage, pull images for integration cache try special tag image first
# if it fails download latest instead
if [ -n "${1+x}" ] && [ "${1}" == "pull" ]; then
  echo "Pulling images for integration test cache"

  # The second argument must pass the tag variable must be set
  if [ -z "${2+x}" ]; then
    echo "Error, second argument must pass tag variable"
    exit 1
  fi

  # Set the tag variable
  tag="${2}"

  for image in "${LOCAL_IMAGES[@]}"; do
      # Set, splits tuple into $1 and $2. this shadows previous variables
    # shellcheck disable=SC2086
    set -- $image

    # Only download images which are needed for integration test
    if [ "${3}" == "y" ]; then
      local_url="${LOCAL_DOCKER_REGISTRY}/${2}"
      # Pull images, at least one of the two images must succeed
      echo "docker pull ${local_url}:${tag}"
      docker pull "${local_url}:${tag}" || docker pull "${local_url}:latest" || exit 1
      if [ -n "$(docker image inspect \""${local_url}":"${tag}"\")" ]; then
        docker tag "${local_url}:latest" "${local_url}:${tag}"
      fi
      # Ensure the images will have the same tags as generated by docker
      docker tag "${local_url}:${tag}" "${2}" || docker tag "${local_url}:latest" "${2}" || exit 1
    fi
  done

  exit 0
fi

# Somehow nothing ran, that is an error do not fail silently
exit 1
