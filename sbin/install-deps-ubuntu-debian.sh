#!/bin/bash

sudo apt update
sudo apt install git g++ gcc make docker docker-compose shellcheck graphviz python3-dev \
python3-pip libboost-python-dev pkg-config shellcheck graphviz dnsutils \
bsdextrautils wget pkg-config jq
sudo pip install --break-system-packages tox
