#!/bin/bash
# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

if [ ! -f "setup.sh" ]; then
  echo "submodule-and-lfs.sh must be executed with repository root as working directory!"
  exit 1
fi

cp bin/hooks/* .git/hooks/
