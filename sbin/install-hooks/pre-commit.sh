#!/bin/bash

if [ ! -f "setup.sh" ]; then
  echo "pre-commit.sh must be executed with repository root as working directory!"
  exit 1
fi

pre-commit install --hook-type pre-push

# --allow-missing-config: Allows the installation to proceed
# even if the configuration file is missing, and hooks won't be executed
# without a configuration file.
#
# e.g. switching between branches who might have or not have pre-push config
# files result in no annoying messages

# To uninstall pre-commit:
#
# pre-commit uninstall
# pip uninstall pre-commit
#
# You might want to remove also '.git/hooks/pre-push'
