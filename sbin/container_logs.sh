#!/bin/bash
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

trap "echo Exited!; exit;" SIGINT SIGTERM
while :
do
  docker logs "$1" -f
  sleep 1
done
