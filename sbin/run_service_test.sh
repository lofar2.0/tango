#!/bin/bash -e
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

if [ -z "$LOFAR20_DIR" ]; then
    # We assume we aren't in the PATH, so we can derive our path.
    # We need our parent directory.
    LOFAR20_DIR_RELATIVE=$(dirname "$0")/..

    # As an absolute path
    LOFAR20_DIR=$(readlink -f "${LOFAR20_DIR_RELATIVE}")
fi

if [ -z "$TAG" ]; then
  export TAG="latest"
fi

docker network rm station || true

# prepare a docker volume for nomad
tmp_volume="test_$(hexdump -n 16 -v -e '/1 "%02X"' /dev/urandom)"

function cleanup {
  cd "$LOFAR20_DIR"
  if [ -n "${save_logs}" ]; then
    mkdir -p log
    for container in $(docker ps -a --format "{{.Names}}")
    do
      echo "Saving log for container $container"
      docker logs "${container}" >& "log/${container}.log"
    done
    bash "${LOFAR20_DIR}"/sbin/dsconfig.sh --dump >& log/dump_ConfigDb.log
  fi

  if [ -z "${preserve}" ]; then
    HOME="$JUMPPAD_HOME" jumppad down
    docker volume rm "$tmp_volume" || true
  fi
}

trap cleanup INT EXIT TERM ERR SIGTERM SIGINT

cd "$LOFAR20_DIR" || exit 1

source "${LOFAR20_DIR}"/sbin/prepare_dev_env.sh --volume="$tmp_volume"

if [ -z "$JUMPPAD_HOME" ]; then
  JUMPPAD_HOME="$HOME"
fi

rm -rf "$JUMPPAD_HOME/.jumppad/"

jumppad_options=(
  --var="host_volume=$tmp_volume"
  --var="image_tag=$TAG"
)

HOME="$JUMPPAD_HOME" jumppad up "${jumppad_options[@]}" infra/dev/services.hcl --no-browser

echo "success"
