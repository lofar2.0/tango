#!/bin/bash

BASE_DIR=$(realpath "infra/station/debian")

VERSION=$(curl ftp://get.debian.org/images/release/current/amd64/iso-cd/ | grep -E 'debian-[0-9]' | awk '{print $9}')
curl "ftp://get.debian.org/images/release/current/amd64/iso-cd/$VERSION" -o "$VERSION"

7z x "-o$BASE_DIR/isofiles" "$VERSION"

cp "$BASE_DIR/preseed.cfg" "$BASE_DIR/isofiles/preseed.cfg"
cp "$BASE_DIR/grub/grub.cfg" "$BASE_DIR/isofiles/boot/grub/grub.cfg"
cp "$BASE_DIR/isolinux/auto.cfg" "$BASE_DIR/isofiles/isolinux/auto.cfg"
cp "$BASE_DIR/isolinux/menu.cfg" "$BASE_DIR/isofiles/isolinux/menu.cfg"

#dd if="$VERSION" bs=1 count=432 of=isohdpfx.bin

#xorriso -as mkisofs -o debian.iso \
#        -isohybrid-mbr isohdpfx.bin \
#        -c isolinux/boot.cat -b isolinux/isolinux.bin -no-emul-boot \
#        -boot-load-size 4 -boot-info-table "$BASE_DIR/isofiles"

xorriso -as mkisofs -r -checksum_algorithm_iso sha256,sha512 \
        -V 'Debian 12.5.0 amd64 n' -o debian.iso \
        -J -joliet-long -cache-inodes -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
        -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table \
        -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat \
        -isohybrid-apm-hfsplus "$BASE_DIR/isofiles"
