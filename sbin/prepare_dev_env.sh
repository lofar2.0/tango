#!/bin/bash
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

# Url to the jumppad download location
jumppad_download="https://github.com/jumppad-labs/jumppad/releases/download/0.15.0/jumppad_0.15.0_linux_x86_64.tar.gz"
levant_download="https://releases.hashicorp.com/levant/0.3.3/levant_0.3.3_linux_amd64.zip"

if [ -z "$LOFAR20_DIR" ]; then
    # We assume we aren't in the PATH, so we can derive our path.
    # We need our parent directory.
    LOFAR20_DIR_RELATIVE=$(dirname "$0")/..

    # As an absolute path
    LOFAR20_DIR=$(readlink -f "${LOFAR20_DIR_RELATIVE}")
fi

mkdir -p ./.bin
bin_dir=$(realpath ./.bin)
export PATH="$PATH:$bin_dir"

echo 'Check if jumppad is installed'
if ! [ -x "$(command -v jumppad)" ]; then
  echo 'jumppad is not installed, installing'
  mkdir -p ./.bin
  wget -qO- "${jumppad_download}" | tar xvfz - -C "$bin_dir"
  chmod +x ./.bin/jumppad
fi

echo 'Check if levant is installed'
if ! [ -x "$(command -v levant)" ]; then
  echo 'levant is not installed, installing'
  mkdir -p ./.bin
  wget -qO- "${levant_download}" | zcat >> ./.bin/levant
  chmod +x ./.bin/levant
fi

echo "Generating jobs for station configuration: $STATION_TYPE"
make -C infra/jobs/station DIR_OUT="$( realpath "infra/dev/jobs/station")" render

docker_volume="dev_nomad_station"

# list of arguments expected in the input
optstring_long="help,volume::"
optstring="hv::"

options=$(getopt -l ${optstring_long} -o ${optstring} -- "$@")

eval set -- "$options"

while true; do
  case ${1} in
    -h|--help)
      usage
      exit 0
      ;;
    -v|--volume)
      shift
      docker_volume="$1"
      ;;
    --)
    shift
    break;;
  esac
  shift
done

if [ "$(docker volume list | grep -c "$docker_volume")" = "0" ]; then
  echo "Create volume $docker_volume"
  docker volume create "$docker_volume"
fi

docker pull -q bash
docker run --rm -i -v "$docker_volume":/mnt bash bash  <<- EOM
  mkdir -p /mnt/volumes/tango-database
  mkdir -p /mnt/volumes/monitoring-postgresql-data
  mkdir -p /mnt/volumes/monitoring-loki-data
  mkdir -p /mnt/volumes/monitoring-prometheus-data
  mkdir -p /mnt/volumes/object-storage-data
  mkdir -p /mnt/volumes/jupyter-notebooks
  mkdir -p /mnt/volumes/IERS-data
  chmod 0777 -R /mnt/volumes
EOM
