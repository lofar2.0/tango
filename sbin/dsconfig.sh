#!/bin/bash
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

# defaults
network="station"

# list of arguments expected in the input
optstring_long="help,load,update,dump,file,network::"
optstring="hludf:n::"

options=$(getopt -l ${optstring_long} -o ${optstring} -- "$@")

eval set -- "$options"


while true; do
  case ${1} in
    -h|--help)
      usage
      exit 0
      ;;
    -l|--load)
      echo "Load config"
      action="load"
      ;;
    -u|--update)
      echo "Update config"
      action="update"
      ;;
    -d|--dump)
      echo "Dump config"
      action="dump"
      ;;
    -n|--network)
      shift
      network="$1"
      ;;
    --)
    shift
    break;;
  esac
  shift
done

if [ -z "$TAG" ]; then
  TAG="latest"
fi

echo "Use docker network '$network'"
docker_args=(run --rm  -e "TANGO_HOST=$TANGO_HOST" --network="$network" -i)
docker_image="git.astron.nl:5000/lofar2.0/tango/dsconfig:$TAG"

if [[ -n "$DNS" ]]; then
  echo "Set docker dns to $DNS"
  docker_args+=(--dns "$DNS")
fi

if [[ "$action" != "dump" ]]; then

  if [ ${#} -eq 1 ]; then
      file=$(realpath "${1}")
  else
      echo "A file name must be provided."
      exit 1
  fi

  cmd_args=(--write)
  docker_args+=(-v "${file}":/tmp/dsconfig-update-settings.json)

  if [[ "$action" == "update" ]]; then
    "${LOFAR20_DIR}/sbin/dsconfig.sh" --dump > "${LOFAR20_DIR}"/CDB/dump_"$(date "+%Y.%m.%d_%H.%M.%S")".json

    # update settings, Do not change -i into -it this will break integration tests in gitlab ci!
    docker "${docker_args[@]}" "$docker_image" \
      sh -c '/manage_object_properties.py --write < /tmp/dsconfig-update-settings.json'
    cmd_args+=(--update)
  fi


  # update settings, Do not change -i into -it this will break integration tests in gitlab ci!
  docker "${docker_args[@]}" "$docker_image" \
    json2tango "${cmd_args[@]}" /tmp/dsconfig-update-settings.json

  # somehow json2tango does not return 0 on success
  exit 0
fi

# writes the JSON dump to stdout, Do not change -i into -it incompatible with gitlab ci!
docker "${docker_args[@]}" "$docker_image" \
  bash -c '
  python -m dsconfig.dump > /tmp/dsconfig-configdb-dump.json
  /manage_object_properties.py -r > /tmp/dsconfig-objectdb-dump.json
  /merge_json.py /tmp/dsconfig-objectdb-dump.json /tmp/dsconfig-configdb-dump.json'
