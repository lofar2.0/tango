#!/bin/bash
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

set -e

# Serves as entrypoint script for docker containers

if [[ ! -d "/opt/lofar/tango" ]]; then
  >&2 echo "/opt/lofar/tango volume does not exist!"
  exit 1
fi

# Check required support file exists
if [[ ! -f "/usr/local/bin/wait-for-it.sh" ]]; then
    >&2 echo "/usr/local/bin/wait-for-it.sh file does not exist!"
    exit 1
fi

# Check required environment variable is set
if [[ ! $TANGO_HOST ]]; then
  >&2 echo "TANGO_HOST environment variable unset!"
  exit 1
fi

# Store directory so we can return to it after installation
CWD=$(pwd)

cd /opt/lofar/tango || exit 1

# Check if configured for specific version
if [[ $TANGO_STATION_CONTROL ]]; then
  echo "Assuming station control install performed as part of base image"
else
  # Install the package, exit 1 if it fails
  # pip install ./ will _NOT_ install dependencies in requirements.txt!
  rm -rf /tmp/tangostationcontrol
  # Ideally we would use git copy but it can't copy on subdirectory level
  # DO NOT PUT SPACES IN THE EXCLUDE LIST!
  rsync -av --progress --exclude={".tox","*.egg-info","dist","build",".git","*.pyc","cover",".stestr",".coverage*",".pytest_cache","htmlcov"} /opt/lofar/tango/tangostationcontrol /tmp/
  cd /tmp/tangostationcontrol || exit 1
  pip -vvv install --break-system-packages --no-deps --force-reinstall ./
fi

# Return to the stored the directory, this preserves the working_dir argument in
# docker-compose files.
cd "$CWD" || exit 1

# Replace this script's process with the actual command, allowing any signals
# send to the bash PID to be sent to the command directly.
exec /opt/lofar/tango/bin/fix-tango-exit-status.sh /usr/local/bin/wait-for-it.sh "$TANGO_HOST" --timeout=30 --strict -- "$@"
