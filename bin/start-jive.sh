#!/bin/bash
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

OS=$(uname)

case ${OS} in
    Linux)
        display=""
        XTRA_OPTIONS="-u $(id -u "${USER}"):$(id -g "${USER}") -v /etc/passwd:/etc/passwd:ro -v /etc/groups:/etc/groups:ro"

        ;;
    Darwin)
        display="$(scutil --nwi | grep 'address' | cut -d':' -f2 | tr -d ' ' | head -n1):0"
        XTRA_OPTIONS=""
        ;;
esac

display=${display:-${DISPLAY}}

OPTIONS="-e HOME=/hosthome -e DISPLAY=${display} -e XAUTHORITY=/hosthome/.Xauthority -e TANGO_HOST=databaseds:10000 -v ${HOME}:/hosthome -v /tmp/.X11-unix:/tmp/.X11-unix"

if [ -n "${XTRA_OPTIONS}" ]; then
    OPTIONS="${OPTIONS} ${XTRA_OPTIONS}"
fi

if [ ${#} -eq 1 ]; then
    command=${1}
    shift
else
    command=/usr/local/bin/jive
fi

#docker run --rm -it --network host ${OPTIONS} nexus.engageska-portugal.pt/ska-docker/tango-java:latest ${command} ${@}
container_name=artefact.skao.int/ska-tango-images-tango-java:9.3.4
container=$(docker ps | grep -E ${container_name} | cut -d' ' -f1)
if [ -n "${container}" ]; then
    docker exec -it "${container}" ${command} "${@}"
else
    echo "Container \"${container_name}\" is not running."
fi

