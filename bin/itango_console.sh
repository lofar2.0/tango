#!/bin/bash
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

# defaults
network="station"

# list of arguments expected in the input
optstring_long="help,network::"
optstring="hn::"

options=$(getopt -l ${optstring_long} -o ${optstring} -- "$@")

eval set -- "$options"


while true; do
  case ${1} in
    -h|--help)
      usage
      exit 0
      ;;
    -n|--network)
      shift
      network="$1"
      ;;
    --)
    shift
    break;;
  esac
  shift
done

if [ -z "$TAG" ]; then
  TAG="latest"
fi

echo "Use docker network '$network'"
docker_args=(run --rm  -e "TANGO_HOST=$TANGO_HOST" --network="$network" -it)
docker_image="git.astron.nl:5000/lofar2.0/tango/itango:$TAG"

if [[ -n "$DNS" ]]; then
  echo "Set docker dns to $DNS"
  docker_args+=(--dns "$DNS")
fi

docker "${docker_args[@]}" "$docker_image" itango3
