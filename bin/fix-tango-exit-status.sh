#!/bin/bash
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#
# This script waraps Tango Controls device servers such that they return
# exit code 0 when they should not be restarted. This allows them to
# be run effectively under the "restart: on-failure" strategy of running
# Docker containers.
#
# Exit code 0 will be returned under the following conditions:
#  - Process completed succesfully (Tango returns exit code 0).
#  - Process failed but an immediate restart would be useless
#    (Tango returns exit code 255 ("exit(-1)" in its code base).
#
# Failures for which an immediate restart of the process would be
# useless include:
#   - Device server is not found in the Tango Database
#   - Mandatory properties of devices are missing in the Tango Database
#   - Tango Database cannot be reached
#
# In all other cases, the non-zero exit code is propagated to Docker,
# resulting in a restart of the container. This includes for example
# Segmentation Faults.
#
# If the process is not wrapped with this script, the above conditions
# would result in a flurry of non-stop restarts of the container,
# thus stressing the system needlessly and spamming the logs.

set -e

# run command in background
"$@" &
PID=$!

# propagate signals sent to us to the command
trap 'kill -INT $PID' INT
trap 'kill -TERM $PID' TERM

# wait for the command to finish
wait $PID
RESULT=$?

if [[ $RESULT -eq 255 ]]; then
  # ignore exit status 255, which indicates a fundamental
  # error that is useless to retry execution for.
  RESULT=0
fi

# our exit code is the command's
exit $RESULT
