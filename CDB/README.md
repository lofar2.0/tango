## Tango Configuration Database files

The ConfigDb.json files in this directory are used to populate the Tango configuration Database for various scenarios.
To load a configuration file, use

```bash
../sbin/dsconfig.sh --update file.json
```
Or feed the configuration file to the ``dsconfig`` batch job through the Nomad console at
http://localhost:4646/ui/jobs/dsconfig@default/dispatch.

The tool ``tangostationcontrol.toolkit.analyse_dsconfig_hierarchies`` is provided to check the consistency of
hierarchies defined between the devices in a set of configuration files.

## Station configurations

The ``stations/`` directory contains the Tango base configurations for each station.

## Integration configurations

The ``integrations/`` directory contains the Tango configuration deltas for the integration test suites.
