## Station configurations

This directory contains the Tango base configurations for each station, using common subsets:

- common.json: Settings common to all stations,
- l0.json: Devices representing rack L0,
- l1.json: Devices representing rack L1,
- h0.json: Devices representing rack H0,
- lba.json: Devices needed for the LBA antenna field,
- hba_core.json: Devices needed for the HBA antenna field in core station configuration.
- hba_remote.json: Devices needed for the HBA antenna field in remote station configuration.
- cs.json: Common config for core stations.
- rs.json: Common config for remote stations.

The following stations need the following configurations applied:

- CS001: common.json + l0.json + l1.json + h0.json + lba.json + hba_core.json + cs.json + cs001.json
- RS307: common.json + l0.json + l1.json + h0.json + lba.json + hba_remote.json + rs.json + rs307.json

## Adding a station

Use the `generate-cdb-from-lofar1.py` script from the `tangostationcontrol.toolkit` module to
generate station-specific CDB files like cs001.json.
