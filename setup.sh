#! /usr/bin/env bash
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

# This file's directory is used to determine the station control directory
# location.
if [ -z ${BASH_SOURCE} ]; then
  BASH_SOURCE=${(%):-%x}
fi

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export LOFAR20_DIR=${1:-${ABSOLUTE_PATH}}

# Create a virtual environment directory if it doesn't exist
VENV_DIR="${LOFAR20_DIR}/venv"
if [ ! -d "$VENV_DIR" ]; then
    echo "Creating virtual environment..."
    python3 -m venv "$VENV_DIR"
fi

# Activate the virtual environment
source "$VENV_DIR/bin/activate"
pip install pre-commit

# Install git hooks
if [ ! -f "${LOFAR20_DIR}/.git/hooks/post-checkout" ]; then
  source "${LOFAR20_DIR}/sbin/install-hooks/submodule-and-lfs.sh"
fi

# Install git pre-commit pre-push hook
if [ ! -f "${LOFAR20_DIR}/.git/hooks/pre-push.legacy" ]; then
  source "${LOFAR20_DIR}/sbin/install-hooks/pre-commit.sh"
fi

# It checks if Tango is running within nomad, by trying to query the service from consul.
# If consul is not available it is assumed that the Tango host, the computer that runs the TangoDB,
# is this host. If this is not true, then modify to the Tango host's FQDN and
# port. Example:  export TANGO_HOST=station-xk25.astron.nl:10000
if dig @127.0.0.1 -p 8600 tango.service.consul +short > /dev/null; then
  TANGO_PORT=$(dig @127.0.0.1 -p 8600 tango.service.consul SRV +short  | awk '{printf "%s",$3}')
  TANGO_HOST=$(dig @127.0.0.1 -p 8600 tango.service.consul +short)
  export TANGO_HOST="$TANGO_HOST:$TANGO_PORT"
else
  export TANGO_HOST="$(hostname):10000"
fi

echo "Using tango host $TANGO_HOST"

# Configure to install station control in lofar-device-base image
# TODO(L2SS-520): Extend to support debug and expose as property in devices
export TANGO_STATION_CONTROL=1
