variables:
  STATION: "LOFAR station to deploy on"

stages:
  - render
  - deploy

# NB: Due to a levant bug, we need to render and use nomad to deploy
# see https://github.com/hashicorp/levant/issues/550

# To deploy manually, get the env.yaml and the .levant.nomad file,
# and run docker run --rm -i --net=host -v /path/to/env.yaml:/env.yaml:ro hashicorp/levant deploy --var-file=/env.yaml /dev/stdin < /path/to/file.levant.nomad

.components:
  parallel:
    matrix:
      - COMPONENT:
          - mesh-gateway
          - monitoring
          - logging
          - tango
          - object-storage
          - object-replication
          - sdptr
          - device-server
          - dsconfig
          - ec-sim
          - jupyter
          - snmp-exporter
          - landing-page
          - rpc-server

render_levant:
  extends: .components
  stage: render
  image:
    name: hashicorp/levant
    entrypoint: [ "" ]
  script:
    - |
      if [ "${STATION}" == "dts-lab" ]; then
          # dts-lab test station
          REGION="global"
          EXTRA_VARFILES="-var-file=infra/env/cs.yaml"
      elif [ "${STATION::2}" == "rs" ]; then
          # remote station
          REGION="lofar-${STATION}"
          EXTRA_VARFILES="-var-file=infra/env/rs.yaml"
      else
          # core station
          REGION="lofar-${STATION}"
          EXTRA_VARFILES="-var-file=infra/env/cs.yaml"
      fi

      levant render \
        -var-file=infra/env/common.yaml ${EXTRA_VARFILES} \
        -var image_tag="${CI_COMMIT_REF_SLUG}" \
        -var region="${REGION}" \
        -var station="${STATION}" \
        infra/jobs/station/${COMPONENT}.levant.nomad > ${STATION}-${COMPONENT}.nomad

      # make sure all variables are filled in,
      # see also https://github.com/hashicorp/levant/pull/308
      ! fgrep "<no value>" ${STATION}-${COMPONENT}.nomad
  artifacts:
    paths:
      - ${STATION}-${COMPONENT}.nomad

deploy_nomad:
  extends: .components
  stage: deploy
  # gitlab evaluates parallel.matrix after needs/dependencies,
  # so we cannot use $COMPONENT in the dependencies.
  # we depend on all render_levant jobs instead.
  needs:
    - render_levant
  dependencies:
    - render_levant
  image:
    name: hashicorp/nomad
    entrypoint: [ "" ]
  environment:
    name: $STATION
  script:
    - |
      if [ "${STATION}" == "dts-lab" ]; then
          NOMAD_HOST="dts-lab.lofar.net"
      else
          NOMAD_HOST="monitor.control.lofar"
      fi

      nomad job run -address="http://${NOMAD_HOST}:4646" ${STATION}-${COMPONENT}.nomad

workflow:
  name: 'deploy on $STATION'
  rules:
    - when: always
