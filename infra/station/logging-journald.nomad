job "log-scraping-journald" {
    datacenters = ["stat"]
    type        = "system"

    update {
        min_healthy_time  = "10s"
        healthy_deadline  = "5m"
        progress_deadline = "10m"
        auto_revert       = true
    }

    group "vector" {
        count = 1
        restart {
            attempts = 3
            interval = "10m"
            delay    = "30s"
            mode     = "delay"
        }
        network {
            mode = "bridge"
            port "api" {
                to = 8686
                host_network = "station"
            }
        }

        task "vector" {
            driver = "raw_exec"
            artifact {
              source = "https://packages.timber.io/vector/0.39.0/vector-0.39.0-x86_64-unknown-linux-gnu.tar.gz"
            }
            config {
                command = "vector-x86_64-unknown-linux-gnu/bin/vector"
            }
            # Vector won't start unless the sinks(backends) configured are healthy
            env {
                VECTOR_CONFIG          = "local/vector.toml"
                VECTOR_REQUIRE_HEALTHY = "true"
            }
            # resource limits are a good idea because you don't want your log collection to consume all resources available
            resources {
                cpu    = 512
                memory = 1024
            }
            # template with Vector's configuration
            template {
                destination     = "local/vector.toml"
                change_mode     = "signal"
                change_signal   = "SIGHUP"
                left_delimiter  = "(("
                right_delimiter = "))"
                data            = <<EOF
data_dir                     = "local/"
healthchecks.require_healthy = true

[api]
  enabled              = true
  address              = "0.0.0.0:8686"
  playground           = false

[sources.journald]
  type                 = "journald"
# [transforms.journald-flags]
#   inputs = ["journald"]
#   type   = "remap"
#   source = '''
#     # parse_key_value only supports \", not a literal \n, so we replace them with actual newlines instead.
#     .message         = replace!(.message,"\\n","\n")
#
#     # parse trying various formats
#     structured =
#       parse_syslog(.message) ??
#       parse_json(.message) ??
#       parse_common_log(.message) ??
#       parse_key_value!(.message, whitespace: "strict")
#     .                = merge!(., structured)
#
#     # unify fields across formats
#     .message         = .msg || .message
#     .timestamp       = parse_timestamp(.ts, "%F %T,%3f") ?? .timestamp
#     .level           = downcase!(.level || "info")
#
#     # hard limit on message size to prevent explosion
#     .message         = truncate!(.message, limit: 8192, ellipsis: true)
#
#     # delete labels that vary too much or are duplicates
#     del(.label)
#     del(.ts)
#     del(.msg)
#
#     # labels from the docker_logs source that will vary way too much
#     del(.host)
#  '''
[sinks.loki]
  type                 = "loki"
  inputs               = [ "journald" ]
  endpoint             = "http://loki.service.consul:3100"
  encoding.codec       = "json"
  healthcheck.enabled  = true
  out_of_order_action  = "accept"
  batch.max_bytes      = 4194304
[sinks.loki.labels]
  "source"            = 'journald'
# [sinks.loki_central]
#   type                 = "loki"
#   inputs               = [ "journald" ]
#   tenant_id            = "lofar"
#   endpoint             = "http://logs.service.lofar-central.consul:3100"
#   encoding.codec       = "json"
#   healthcheck.enabled  = true
#   remove_label_fields  = true
#   out_of_order_action  = "accept"
#   batch.max_bytes      = 4194304
# [sinks.loki_central.labels]
#   "station"            = '[[.station]]'
#   "nomad_*"            = '{{ nomad }}'

EOF
            }
            service {
                check {
                    port     = "api"
                    type     = "http"
                    path     = "/health"
                    interval = "30s"
                    timeout  = "5s"
                }
            }
            kill_timeout = "30s"
        }
    }
}
