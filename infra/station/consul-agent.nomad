job "consul-agent" {
  datacenters = ["stat"]
  type        = "system"

  group "consul" {
    count = 1

    volume "consul" {
        type      = "host"
        source    = "consul"
        read_only = true
    }

    network {
      mode = "host"
    }

    task "consul" {
      driver = "exec"

      #config {
      #  image   = "hashicorp/consul:1.18.0"
      #  ports   = ["http", "grpc", "lan_serf"]
      #  command = "agent"
      #  args    = ["-config-file=/local/consul.hcl"]
      #    volumes = [
      #      # Use absolute paths to mount arbitrary paths on the host
      #      "/opt/consul/agent-certs/:/consul/agent-certs/"
      #    ]
      #}
      config {
        # When running a binary that exists on the host, the path must be absolute.
        command = "/bin/consul"
        args    = ["agent", "-config-file=/host/consul-agent.hcl"]
      }

      # docker socket volume mount
      volume_mount {
        volume      = "consul"
        destination = "/host/"
        read_only   = true
      }

      resources {
        cpu    = 1024
        memory     = 2048
      }
    }
  }
}
