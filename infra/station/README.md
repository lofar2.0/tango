# LCU installation

## Base system

The base system is installed using [debian preseeding](https://wiki.debian.org/DebianInstaller/Preseed).
The setup is rather simple. The following settings are applied during installation:
* system user lcuadmin is created
* openssh-server is installed
* custom disk partitioning is used

The full configuration can be found in the `preseed.cfg` in the debian directory.

### Partitioning

| dev            | size   | type | mount     |
|----------------|--------|------|-----------|
| /dev/nvme0n1p1 | 1 GiB  | EFI  | /boot/efi |
| /dev/nvme0n1p2 | 1 GiB  | ext4 | /boot     |
| /dev/nvme0n1p3 | 100%   | lvm  | vg0       |
| /dev/vg0/root  | 50 GiB | ext4 | /         |
| /dev/vg0/home  | 10 GiB | ext4 | /home     |
| /dev/vg0/swap  | 8 GiB  | swap |           |

## Installation

The software of the LCU is installed using ansible.

The following playbooks have to be applied:

#### base.yml

Copies ssh keys, sets a new user password for lcuadmin and disables password login for ssh.

#### consul.yml

Installs the consul server and applies configuration to connect to the central consul datacenter.

Configures dnsmasq as the primary DNS server for the OS and to forward `.consul` queries to the consul DNS server

Creates a role on the central Vault to allow the consul server to sign TLS certificates used for mTLS inter-service configuration

#### nomad.yml
Installs the nomad server as well as docker and the required CNI plugins

Increases settings for inotify and the UDP max receive buffer size

Alters the nomad server to allow memory over-subscription

Deploys nomads own consul agent to join the consul datacenter

#### ovs.yml
Installs OVS on the host system

Configures the internal network interface(s) to be part of the ovs bridge

Installs and configures the [ovs CNI plugin](https://github.com/k8snetworkplumbingwg/ovs-cni)

#### time.yml
Installs and configures chrony as a NTP server

Sets the timezone to UTC

### Run a playbook
Create a file `ansible.cfg` with the content
```ini
[defaults]
host_key_checking = False
inventory = hosts.yaml
```

Create a file `hosts.yaml` with the content
```yaml
station:
  hosts:
    <station>:
      ansible_host: lcu.<station>.lofar
      station_name: <station>
vault:
  hosts:
    monitor:
      ansible_host: monitor.control.lofar
all:
  vars:
    consul_vault_token: "<token>"
    nomad_encrypt: "<encrypt key>"
    nomad_vault_token: "<token>"
    ansible_user: lcuadmin
    consul_encrypt: "<encrypt key>>"
    volume_group: vg0
    eth:
      - enp1s0f0
      - enp1s0f1
    dns_servers:
      - 10.149.64.20
      - 10.149.64.21
    white_rabbit: "<White Rabbit IP address>"
```

Run
```bash
ansible-playbook -v <playbook>.yml
```
