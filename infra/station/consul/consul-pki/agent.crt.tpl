[[ with secret "pki_int/issue/consul-lofar-{{ station_name }}" "common_name=server.lofar-{{ station_name }}.consul" "ttl=720h" "alt_names=localhost,{{ ansible_facts['hostname'] }}.server.lofar-{{ station_name }}.consul" "ip_sans=127.0.0.1"]]
[[ .Data.certificate ]]
[[ end ]]
