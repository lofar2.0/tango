variable "nomad_cluster" {
  default = ""
}

variable "station_type" {
  default = "cs"
}

variable "image_tag" {
  default = "latest"
}

variable "lofar20_dir" {
  default = ""
}

variable "debug_host" {
  default = ""
}
