resource "nomad_job" "tango" {
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/tango.nomad"]

  health_check {
    timeout = "300s"
    jobs    = ["tango"]
  }
}

resource "nomad_job" "object-storage" {
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/object-storage.nomad"]

  health_check {
    timeout = "300s"
    jobs    = ["object-storage"]
  }
}

resource "exec" "init-object-storage" {
  depends_on = ["resource.nomad_job.object-storage"]
  timeout = "300s"
  environment = {
    DOCKER_HOST=docker_host()
  }
  script = <<-EOF
  #!/bin/bash
  docker run --rm --network="station" --dns="192.168.76.1" busybox \
    sh -c  "echo -n 'Waiting for s3 service to become available ..'
            until nc -z -w 1 s3.service.consul 9000; do
              sleep 2
              echo -n '.'
            done
            echo '. [ok]'
            "

  docker run --rm -i --network="station" --dns="192.168.76.1" busybox \
    sh -c  "while true; do
              wget -T 15 -c http://s3.service.consul:9000/minio/v2/metrics/cluster && break
              sleep 2
            done"

  docker run --rm -i --network="station" --dns="192.168.76.1" --entrypoint bash \
    -v "${variable.lofar20_dir}/infra/dev/seed-data":/opt/seed-data \
    minio/mc \
    -c "mc alias set object-storage http://s3.service.consul:9000 minioadmin minioadmin
        echo 'Initialising caltables'
        mc mb object-storage/caltables
        mc cp --recursive /opt/seed-data/object-storage/caltables/ object-storage/caltables/
        echo 'Initialising IERS tables'
        mc mb object-storage/iers
        mc cp --recursive /opt/seed-data/object-storage/iers/ object-storage/iers/
        date +'%F %T'
        echo 'Initialisation completed'"
  EOF
}

resource "nomad_job" "simulators" {
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/simulators.nomad"]

  health_check {
    timeout = "300s"
    jobs    = ["simulators"]
  }
}

resource "nomad_job" "ec-sim" {
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/ec-sim.nomad"]

  health_check {
    timeout = "300s"
    jobs    = ["ec-sim"]
  }
}

resource "exec" "dsconfig" {
  depends_on = ["resource.nomad_job.tango"]
  environment = {
    DNS="192.168.76.1"
    DOCKER_HOST=docker_host()
    TANGO_HOST="tango.service.consul:10000"
    TAG=variable.image_tag
    STATION_TYPE=variable.station_type
  }
  working_directory = "${variable.lofar20_dir}"
  timeout = "300s"
  script = <<-EOF
  #!/bin/bash
  docker run --rm --network="station" --dns="192.168.76.1" busybox \
    sh -c  "echo -n 'Waiting for tango service to become available ..'
            until nc -z -w 1 tango.service.consul 10000; do
              sleep 2
              echo -n '.'
            done
            echo '. [ok]'
            "

  echo "Docker host is $DOCKER_HOST"
  echo "Tango host within docker network is $TANGO_HOST"
  bash sbin/dsconfig.sh --load CDB/stations/common.json
  bash sbin/dsconfig.sh --update CDB/stations/l0.json
  bash sbin/dsconfig.sh --update CDB/stations/l1.json
  bash sbin/dsconfig.sh --update CDB/stations/lba.json
  bash sbin/dsconfig.sh --update CDB/stations/h0.json
  if [ "$STATION_TYPE" = "rs" ]; then
    echo "Configuring dsconfig for remote station"
    bash sbin/dsconfig.sh --update CDB/stations/hba_remote.json
    bash sbin/dsconfig.sh --update CDB/stations/rs.json
    bash sbin/dsconfig.sh --update CDB/stations/rs307.json
    bash sbin/dsconfig.sh --update CDB/stations/testenv_rs307.json
  else
    echo "Configuring dsconfig for core station"
    bash sbin/dsconfig.sh --update CDB/stations/hba_core.json
    bash sbin/dsconfig.sh --update CDB/stations/cs.json
    bash sbin/dsconfig.sh --update CDB/stations/cs001.json
    bash sbin/dsconfig.sh --update CDB/stations/testenv_cs001.json
  fi
  EOF
}

resource "nomad_job" "device-servers" {
  depends_on = [
    "resource.exec.dsconfig",
    "resource.exec.init-object-storage",
    "resource.nomad_job.object-storage"
  ]
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/device-server.nomad"]

  health_check {
    timeout = "3000s"
    jobs    = ["device-servers"]
  }
}

resource "ingress" "minio_s3" {
  port = 9000

  target {
    resource = variable.nomad_cluster
    named_port = "s3"

    config = {
      job = "object-storage"
      group = "minio"
      task = "minio"
    }
  }
}

resource "ingress" "minio_console" {
  port = 9001

  target {
    resource = variable.nomad_cluster
    named_port = "console"

    config = {
      job = "object-storage"
      group = "minio"
      task = "minio"
    }
  }
}
