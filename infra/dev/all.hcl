variable "host_volume" {
  default = "dev_nomad_station"
}
variable "lofar20_dir" {
  default = ""
}
variable "image_tag" {
  default = "latest"
}

variable "debug_host" {
  default = ""
}

variable "station_type" {
  default = "cs"
}

module "nomad" {
  source    = "./nomad"
  variables = {
    host_volume = variable.host_volume
    image_tag = variable.image_tag
  }
}

module "tango" {
  source    = "./tango"
  variables = {
    nomad_cluster = module.nomad.output.nomad_cluster
    lofar20_dir = variable.lofar20_dir
    image_tag = variable.image_tag
    debug_host = variable.debug_host
  }
}

module "services" {
  source    = "./services"
  variables = {
    nomad_cluster = module.nomad.output.nomad_cluster
  }
}
