variable "nomad_cluster" {
  default = ""
}

variable "station_type" {
  default = "cs"
}

variable "lofar20_dir" {
  default = ""
}
