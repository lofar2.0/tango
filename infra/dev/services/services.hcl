resource "nomad_job" "monitoring" {
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/monitoring.nomad"]

  health_check {
    timeout = "300s"
    jobs    = ["monitoring"]
  }
}

resource "nomad_job" "logging" {
  cluster = variable.nomad_cluster

  paths = ["../jobs/station/logging.nomad"]
  depends_on = ["resource.nomad_job.monitoring"]

  health_check {
    timeout = "300s"
    jobs    = ["log-scraping"]
  }
}

resource "ingress" "grafana" {
  port = 3000

  target {
    resource = variable.nomad_cluster
    named_port = "grafana"

    config = {
      job = "monitoring"
      group = "grafana"
      task = "grafana"
    }
  }
}
