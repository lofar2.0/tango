variable "host_volume" {
  default = "dev_nomad_station"
}
variable "image_tag" {
  default = "latest"
}

variable "station_type" {
  default = "cs"
}

module "nomad" {
  source    = "./nomad"
  variables = {
    host_volume = variable.host_volume
    image_tag = variable.image_tag
  }
}

module "services" {
  source    = "./services"
  variables = {
    nomad_cluster = module.nomad.output.nomad_cluster
  }
}
