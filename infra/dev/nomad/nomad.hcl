resource "network" "station" {
  subnet = "192.168.72.0/21"
}

resource "template" "consul_config" {

  source = <<-EOF
  data_dir  = "/consul/data/"
  log_level = "trace"


  datacenter = "dev-stat"

  server = true

  bootstrap_expect = 1
  ui = true

  bind_addr = "0.0.0.0"
  client_addr = "127.0.0.1 {{GetInterfaceIP \"eth0\"}}"
  advertise_addr = "{{GetInterfaceIP \"eth0\"}}"

  ports {
    grpc = 8502
    grpc_tls = 8503
    dns = 53
  }
  connect {
    enabled = true
  }

  auto_encrypt {
    allow_tls = true
  }
  skip_leave_on_interrupt = true
  leave_on_terminate = false
  recursors  = ["127.0.0.11"]
  EOF

  destination = "./tmp/consul.hcl"
}

resource "container" "consul" {
  depends_on = [
    "resource.template.consul_config", "resource.network.station"
  ]
  network {
    id         = resource.network.station.meta.id
    ip_address = "192.168.76.1"
  }

  image {
    name = "hashicorp/consul:latest"
  }

  command = [
    "consul",
    "agent",
    "-dev",
    "-config-file=/consul/config/config.hcl"
  ]

  environment = {
    CONSUL_HTTP_ADDR = "http://localhost:8500"
  }

  volume {
    source      = resource.template.consul_config.destination
    destination = "/consul/config/config.hcl"
    type        = "bind"
  }

  port {
    local  = 8500
    remote = 8500
    host   = 18500
  }

  port {
    local    = 53
    host     = 8600
    remote   = 53
    protocol = "udp"
  }
  privileged = false
}

resource "nomad_cluster" "station" {
  depends_on    = [
    "resource.container.consul",
  ]
  client_nodes  = 1
  client_config = "./config/nomad/client.hcl"
  # consul_config = "./config/nomad/consul.hcl"
  datacenter    = "stat"

  # image {
  #   name = "git.astron.nl:5000/lofar2.0/jumppad-docker-files/nomad:latest"
  # }

  network {
    id = resource.network.station.meta.id
    # ip_address = "192.168.76.3"
  }
  environment = {
    NO_PROXY = "astron.nl,control.lofar,10.0.0.0/8"
  }
  volume {
    source      = "${variable.host_volume}"
    destination = "/localdata"
    type        = "volume"
  }
  volume {
    source = "./config/cni"
    destination = "/opt/cni/config"
  }
  volume {
    source = "./config/resolv.conf"
    destination = "/etc/resolv.conf"
  }
  volume {
    source = "./config/nomad"
    destination = "/opt/consul"
  }
  copy_image {
    name = "git.astron.nl:5000/lofar2.0/tango/ec-sim:${variable.image_tag}"
  }
  copy_image {
    name = "git.astron.nl:5000/lofar2.0/tango/lofar-device-base:${variable.image_tag}"
  }
  copy_image {
    name = "git.astron.nl:5000/lofar2.0/tango/grafana:${variable.image_tag}"
  }
}

resource "exec" "host_macvlan" {
  depends_on = ["resource.nomad_cluster.station"]
  environment = {
    DOCKER_HOST=docker_host()
  }
  timeout = "300s"
  script = <<-EOF
  #!/bin/bash

  # inspired by https://blog.oddbit.com/post/2018-03-12-using-docker-macvlan-networks/
  for container in $(docker ps --format '{{.Names}}' | grep '.client.station.nomad.nomad-cluster.local.jmpd.in'); do
    # get the last octet of the current IP of the nomad client
    n=$(docker exec "$container" ip -o addr show dev eth0 | grep -v inet6 | awk '{print $4}' | sed -e 's/192.168.72.\([0-9]\{1,\}\)\/21/\1/')
    # add 100 to the octet to hopefully get another unique IP in the docker network
    new_ip="192.168.72.$((n+100))/21"
    # attach the nomad client to the network in macvlan mode and route traffic to the container over this interface
    docker exec "$container" ip link add stationnet-shim link eth0 type macvlan mode bridge
    docker exec "$container" ip addr add "$new_ip" dev stationnet-shim
    docker exec "$container" ip link set stationnet-shim up
    docker exec "$container" ip route add 192.168.72.0/22 dev stationnet-shim
  done
  EOF
}

resource "nomad_job" "consul-agent" {
  depends_on = ["resource.nomad_cluster.station"]
  cluster = resource.nomad_cluster.station

  paths = ["./jobs/consul-agent.nomad"]

  health_check {
    timeout = "300s"
    jobs    = ["consul-agent"]
  }
}

resource "exec" "enable_oversubscription" {
  depends_on = ["resource.nomad_cluster.station"]
  environment = {
    NOMAD_ADDR="http://${resource.nomad_cluster.station.external_ip}:${resource.nomad_cluster.station.api_port}"
  }
  timeout = "300s"
  script = <<-EOF
  #!/bin/bash
  data=$(wget -qO- $NOMAD_ADDR/v1/operator/scheduler/configuration | \
  jq '.SchedulerConfig | .MemoryOversubscriptionEnabled=true')
  wget --method=PUT --body-data="$data" -qO- $NOMAD_ADDR/v1/operator/scheduler/configuration
  EOF
}
