plugin "docker" {
  config {
    allow_privileged = true
    volumes {
      enabled      = true
      selinuxlabel = "z"
    }
  }

  gc {
    # do not reap in development, to allow
    # their logs and state to be inspected after teardown
    image = false
    container = false

    dangling_containers {
      enabled = false
    }
  }
}

client {
  cpu_total_compute = 888000
  memory_total_mb = 65536

  host_volume "consul" {
    path      = "/opt/consul"
    read_only = true
  }
  host_volume "monitoring-postgresql-data" {
    path = "/localdata/volumes/monitoring-postgresql-data"
  }

  host_volume "monitoring-loki-data" {
    path = "/localdata/volumes/monitoring-loki-data"
  }

  host_volume "monitoring-prometheus-data" {
    path = "/localdata/volumes/monitoring-prometheus-data"
  }

  host_volume "tango-database" {
    path = "/localdata/volumes/tango-database"
  }

  host_volume "object-storage-data" {
    path = "/localdata/volumes/object-storage-data"
  }

  host_volume "IERS" {
    path = "/localdata/volumes/IERS-data"
  }

  host_volume "jupyter-notebooks" {
    path = "/localdata/volumes/jupyter-notebooks"
  }
  host_network "default" {
    interface = "eth0"
  }
  host_network "external" {
    interface = "eth0"
  }
  host_network "station" {
    interface = "eth0"
  }
  host_network "local" {
    interface = "lo"
  }

  options = {
    "driver.allowlist" = "docker,exec"
  }
}

telemetry {
  collection_interval        = "1s"
  disable_hostname           = true
  prometheus_metrics         = true
  publish_allocation_metrics = true
  publish_node_metrics       = true
}

consul {
  # address              = "192.168.76.2:8500"
  # checks_use_advertise = true # Otherwise will fallback to bind_addr
  addres               = "{{ GetInterfaceIP \"eth0\" }}:8500"
  server_service_name  = "nomad"
  client_service_name  = "nomad-client"
  auto_advertise       = true
  server_auto_join     = true
  client_auto_join     = true
}

advertise {
  http = "{{ GetInterfaceIP \"eth0\" }}"
  rpc  = "{{ GetInterfaceIP \"eth0\" }}"
  serf = "{{ GetInterfaceIP \"eth0\" }}"
}
