job "consul-agent" {
  datacenters = ["stat"]
  type        = "system"

  constraint {
    attribute = "${meta.node_type}"
    operator  = "!="
    value     = "server"
  }

  group "consul" {
    count = 1
    network {
      mode = "host"
    }

    task "consul" {
      driver = "exec"

      artifact {
        source = "https://releases.hashicorp.com/consul/1.20.2/consul_1.20.2_linux_${attr.cpu.arch}.zip"
        destination = "local/"
      }
      config {
        command = "local/consul"
        args    = ["agent", "-config-file=/local/consul-agent.hcl"]
      }

      template {
        destination = "local/consul-agent.hcl"
        data = <<EOF
data_dir  = "/tmp/"
log_level = "trace"

server     = false
datacenter = "dev-stat"

bind_addr      = "0.0.0.0"
advertise_addr = "{{ env "attr.unique.network.ip-address" }}"

ports {
  grpc     = 8502
  grpc_tls = 8503
}

connect {
  enabled = true
}

ui_config {
  enabled = true
}

retry_join              = ["192.168.76.1"]
skip_leave_on_interrupt = true
leave_on_terminate      = false
EOF
      }

      resources {
        cpu    = 1024
        memory     = 2048
      }
    }
  }
}
