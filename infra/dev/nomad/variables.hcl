variable "host_volume" {
  default = "dev_nomad_station"
}

variable "image_tag" {
  default = "latest"
}

output "nomad_cluster" {
  value = resource.nomad_cluster.station
}
output "network_id" {
  value = resource.network.station.meta.id
}
output "external_ip" {
  value = resource.nomad_cluster.station.external_ip
}
