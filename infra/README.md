[[_TOC_]]

# Procedures

## Rolling out a new station

To rollout a new station, you will need:

1. A server to deploy on (LCU)
2. A station switch setup to have:
    1. 25GbE connection to the LCU
    2. Jumbo frames enabled
    3. Connected to LOFAR network & VLAN
    4. Connected to station network & VLAN
3. Netapp information:
    1. IP address of the LCU
    2. IP address of IPMI
    3. IP addresses of the Uniboards (of all racks)
    4. MAC addresses of the Uniboards (of all racks)
    5. IP address of White Rabbit switch
    6. IP address(es) of the PCONs

### Preparation

Before the LCU gets into the field, you want to configure some things:

#### Create configuration files

1. Create CDB files:
    1. Generate the station-specific config from LOFAR1 using `generate_cdb_from_lofar1.py -s CS123` [Script location](../tangostationcontrol/tangostationcontrol/toolkit/generate_cdb_from_lofar1.py).
    2. Make any adjustments needed, and add the file as `cs123.json` to [CDB/stations/](../CDB/stations/).
2. Create Calibration tables:
    1. Generate them from LOFAR1 using `generate_caltable_from_lofar1.py -s CS123`. [Script location](../tangostationcontrol/tangostationcontrol/toolkit/generate_caltable_from_lofar1.py).
    2. Go to http://filefish.lofar.net/ (credentials: minioadmin/minioadmin).
    3. Create a `CS123` path in that bucket, and upload the HDF5 files to that.

#### Add station to LOFAR

1. Update the destination for the beamlets to COBALT in [StationStreams.parset](https://git.astron.nl/lofar2.0/cobalt/-/blob/main/GPUProc/etc/parset-additions.d/default/StationStreams.parset) and deploy it,
2. Prepare a build:
    1. Edit `.deploy.gitlab-ci.yml` to add the station as a deploy target,
    2. Tag the commit to trigger a CI/CD build to deploy later on.

#### Configure LCU OS

1. Hook up the LCU to the station switch
2. Configure the BIOS
    1. Restore on power loss (that is, boot if power was lost and machine was on),
3. Configure the IPMI
    1. Set its IP address,
4. Deploy the OS, see [here](station/README.md).
5. Deploy the software stack:
    1. Activate the ["Deploy" step](../.gitlab-ci.yml) in the Gitlab CI/CD for the pipeline of the version tag you want to deploy.
    2. Wait for the deployment to finish in Nomad, either by:
        1. Going to http://cs123c.control.lofar:4646/ui/jobs, or
        2. Going to http://monitor.control.lofar:4646/ui/jobs and select the station under "Region" in the top left.
6. Load the CDB files:
    1. Go to http://cs123c.control.lofar:4646/ui/jobs/dsconfig
    2. Dispatch jobs for each CDB file, see [here](../CDB/stations/README.md), cut into pieces of max 16 KByte.
    3. Wait until all jobs finished. It is normal for jobs to fail before succeeding, as `dsconfig` can return 1 despite being succesful.
7. Restart the Device Servers:
    1. Go to http://cs123c.control.lofar:4646/ui/jobs/device-servers
    2. Press "Stop Job", and wait for the job to stop.
    3. Press "Start Job", and wait for the job to be started.

#### Verification

After configuration, the following tests should succeed:

1. Nomad is happy about all services: visit http://cs123c.control.lofar:4646/ui/jobs
2. Consul is happy about all services: visit http://cs123c.control.lofar:8500/ui/lofar-cs123/services?status=warning%2Ccritical
3. Jupyter is up and functional: visit http://cs123c.control.lofar/jupyter
4. Grafana is up and receives metrics: visit http://cs123c.control.lofar/grafana and verify the station is OFF

### In the field

Once all the other hardware is installed in the station, it can be turned on:

1. Go to http://cs123c.control.lofar/jupyter/lab/tree/Station%20State%20Transitions.ipynb
2. Run the steps of this notebook to turn the station ON, and validate the result.

# HOWTO

## Access Grafana

Use the result of `dig grafana.service.consul @192.168.76.1` to get the IP address of grafana and add this to the
`/etc/hosts` file.

```text
# Static table lookup for hostnames.
# See hosts(5) for details.

192.168.73.4 grafana.service.consul
```

Now you can navigate to `http://grafana.service.consul:3000/grafana`!

**Limitations:**

1. Loki and vector are running aggregation of logging data fails and won't be shown by Grafana although the connection
to the datasource works.

### Developing Grafana Dashboards

If you need to develop for Grafana as well

```shell
cd grafana-station-dashboards
docker build --tag git.astron.nl:5000/lofar2.0/grafana-station-dashboards:latest .
cd ../tango/docker
make grafana
docker tag git.astron.nl:5000/lofar2.0/tango/grafana:latest git.astron.nl:5000/lofar2.0/tango/grafana:doesnotexist1
cd ../
TAG=doesnotexist1 ./sbin/run_integration_test.sh --module=services
```

## CLI access to S3

To interact with the S3 store, use the `minio/mc` docker image:

```
$ docker run --rm -it --entrypoint=/bin/bash minio/mc -c "mc alias set store http://monitor.control.lofar:9000 minioadmin minioadmin && mc ls store"
mc: Configuration written to `/root/.mc/config.json`. Please update your access credentials.
mc: Successfully created `/root/.mc/share`.
mc: Initialized share uploads `/root/.mc/share/uploads.json` file.
mc: Initialized share downloads `/root/.mc/share/downloads.json` file.
Added `store` successfully.
[2024-02-20 13:08:58 UTC]     0B central-logs/
[2024-02-20 13:08:47 UTC]     0B central-metrics/
[2024-03-07 15:05:36 UTC]     0B central-statistics/
[2024-02-29 09:47:49 UTC]     0B central-tracing/
[2024-06-04 13:04:56 UTC]     0B hdf-test/
[2024-02-27 13:11:48 UTC]     0B tmss-frontend-web-acceptance/
```

Once you have found what you need, mount in a local directory and ask mc to fetch the file:

```
$ docker run -v $PWD:/pwd --rm -it --entrypoint=/bin/bash minio/mc -c "mc alias set store http://monitor.control.lofar:9000 minioadmin minioadmin && mc get store/hdf-test/cs001/lba-xst.h5 /pwd"
mc: Configuration written to `/root/.mc/config.json`. Please update your access credentials.
mc: Successfully created `/root/.mc/share`.
mc: Initialized share uploads `/root/.mc/share/uploads.json` file.
mc: Initialized share downloads `/root/.mc/share/downloads.json` file.
Added `store` successfully.
...ar:9000/hdf-test/cs001/lba-xst.h5: 415.21 MiB / 415.21 MiB ┃▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┃ 107.06 MiB/s 3s

$ ls -l lba-xst.h5
-rw-r--r-- 1 root root 435381526 Jun  6 10:34 lba-xst.h5
```

## Login to the client VM

Almost all docker containers of the software stack run within the client VM,
which is actually run as a docker container in the development environment.

This container can be identified programmatically as follows. Use the additional
``-q`` parameter to obtain just the container ID:

```
$ docker ps --filter 'name=client.station.nomad.nomad-cluster.local.jmpd.in'
CONTAINER ID   IMAGE                     COMMAND                  CREATED         STATUS         PORTS     NAMES
90f6f253fb58   shipyardrun/nomad:1.6.1   "/usr/bin/supervisor…"   3 minutes ago   Up 3 minutes             8592d129.client.station.nomad.nomad-cluster.local.jmpd.in
$
```

You can login interactively to this container using ``sh``:
```
$ CLIENT_CONTAINER_ID=$(docker ps -q --filter 'name=client.station.nomad.nomad-cluster.local.jmpd.in')
$ docker exec -it ${CLIENT_CONTAINER_ID} bash
#
```

## Attach to a running client process

The bulk of our client processes run within the client "VM", which has docker running as well:

```
$ docker exec "${CLIENT_CONTAINER_ID}" docker ps -a
CONTAINER ID   IMAGE                                              COMMAND                  CREATED              STATUS              PORTS     NAMES
00ba94025605   git.astron.nl:5000/lofar2.0/tango/grafana:latest   "/run-wrapper.sh"        12 seconds ago       Up 11 seconds                 grafana-de5690ec-219f-51d5-2946-f2f5133e9612
c30d367c387f   git.astron.nl:5000/lofar2.0/tango/loki:latest      "/usr/bin/loki -conf…"   About a minute ago   Up About a minute             loki-e9073864-c281-7390-1b03-8c10fa072e73
7d1d7990031c   envoyproxy/envoy:v1.26.4                           "/docker-entrypoint.…"   About a minute ago   Up About a minute             connect-proxy-loki-e9073864-c281-7390-1b03-8c10fa072e73
3f02415bd6f6   gcr.io/google_containers/pause-amd64:3.1           "/pause"                 About a minute ago   Up About a minute             nomad_init_e9073864-c281-7390-1b03-8c10fa072e73
02a644ecf279   git.astron.nl:5000/lofar2.0/tango/postgres:15.4    "docker-entrypoint.s…"   About a minute ago   Up About a minute             postgres-4d957b81-0de1-0bc0-425b-743b28ba6a8e
[...]
```

You can interact with these containers by logging into the client VM, or directly by chaining docker calls:

```
$ docker exec "${CLIENT_CONTAINER_ID}" docker logs grafana-de5690ec-219f-51d5-2946-f2f5133e9612
Wait until grafana is ready...
logger=settings t=2024-02-13T13:14:47.766739611Z level=info msg="Starting Grafana" version=10.3.1 commit=00a22ff8b28550d593ec369ba3da1b25780f0a4a branch=HEAD compiled=2024-01-22T18:40:42Z
logger=settings t=2024-02-13T13:14:47.767369632Z level=warn msg="ngalert feature flag is deprecated: use unified alerting enabled setting instead"
logger=settings t=2024-02-13T13:14:47.767679942Z level=info msg="Config loaded from" file=/usr/share/grafana/conf/defaults.ini
logger=settings t=2024-02-13T13:14:47.767713444Z level=info msg="Config loaded from" file=/etc/grafana/grafana.ini
[...]
```

This allows you to use the regular docker commands like ``attach``, ``logs``, and ``restart``. Note that any interactive use requires ``-it`` for in the top lin ``docker exec -it "${CLIENT_CONTAINER_ID}"``.

To debug a device server you could use `/sbin/run_integration_test.sh --interactive` combined with:

```shell
docker exec -it "${CLIENT_CONTAINER_ID}" docker attach device-xxx-xxx
```

Followed by manually starting the integration tests: `tox -e integration`.
If your environment is already setup and you do not which to recreate it you could start the integration test
container manually

```shell
docker run --rm  -e "TANGO_HOST=$TANGO_HOST" -e "DEBUG_HOST=${local_ip}" --network="station" --dns="$DNS" -it
  -v "$LOFAR20_DIR":/opt/lofar/tango:rw -w="/opt/lofar/tango/tangostationcontrol" -e "TEST_MODULE=${test_module}" \
  "git.astron.nl:5000/lofar2.0/tango/ci-build-runner:$TAG" tox -e integration
```

## Patching a device server live

Sometimes it is handy to modify the tangostationcontrol source code for a running device server. To do so:

1. Log into the client VM using ``docker exec -it "${CLIENT_CONTAINER_ID}" bash``
2. Find the docker container of the device server (f.e. stationmanager), using ``docker ps -a | grep stationmanager``
3. Enter the device server container with ``docker exec -it <container> bash``
4. Install an editor, f.e. ``sudo apt-get install -y vim``
5. Edit the relevant source file in ``/usr/local/lib/python3.10/dist-packages/tangostationcontrol`` (for Python 3.10)
6. Call the ``restart_device_server()`` command for any device in the changed device server
7. Once restarted, call the ``boot()`` command for all devices in the changed device server to reconfigure them

## Login to the server

The nomad and consul management processes run on the server, which
is actually a docker container in the development environment.

This container can be identified programmatically as follows. Use the additional
``-q`` parameter to obtain just the container ID:

```
$ docker ps --filter 'name=server.station.nomad.nomad-cluster.local.jmpd.in'
CONTAINER ID   IMAGE                     COMMAND                  CREATED         STATUS         PORTS     NAMES
b75f633c837e   shipyardrun/nomad:1.6.1   "/usr/bin/supervisor…"   2 minutes ago   Up 2 minutes   [...]     server.station.nomad.nomad-cluster.local.jmpd.in
$
```

## Using jummpad: live changes and adaptations

When using the `run_integration_test.sh` the entire setup can be performed and left for further adaptation. This
allows, for instance to step-by-step debug device servers during an integration test!

Steps:
1. Setup the environment: `./sbin/run_integration_test.sh --skip-tests`
2. Identify the test volume: `docker volume ls | grep "test_"`, use this in the run command `export $TEST_VOLUME=xxx`
3. Change the `/infra/dev/` files as required
4. Run `DOCKER_HOST="unix:///var/run/docker.sock" ./.bin/jumppad up --var="host_volume=${TEST_VOLUME}" --var="lofar20_dir=$(pwd)" --var="image_tag=latest" infra/dev/all.hcl`

Example, one could change the entrypoint in `device-server.nomad` for a specific device such that you can start the
device manually and debug it during a run of integration test.

## Using nomad: Manage jobs on the client

The server allows you to manage the jobs on the client through Nomad. Each *job* consists of one or more *tasks* that are collectively managed. The tasks are (typically) the docker containers. A job is run inside an *allocation*, which represents an execution instance of a job.

The nomad server spins up on http://localhost:4646, allowing interactive browsing and control. There is also a CLI however, accessed through

```
$ SERVER_CONTAINER_ID=$(docker ps -q --filter 'name=server.station.nomad.nomad-cluster.local.jmpd.in')
$ docker exec "${SERVER_CONTAINER_ID}" nomad
Usage: nomad [-version] [-help] [-autocomplete-(un)install] <command> [args]
[...]
```

To list the status of the jobs, use ``nomad status``:

```
$ docker exec "${SERVER_CONTAINER_ID}" nomad status
ID          Type     Priority  Status   Submit Date
connector   service  50        running  2024-02-13T13:12:02Z
monitoring  service  50        dead     2024-02-13T13:12:09Z
```

To get more info about a job, use ``nomad status <job>``:

```
$ docker exec "${SERVER_CONTAINER_ID}" nomad status monitoring
ID            = monitoring
Name          = monitoring
Submit Date   = 2024-02-13T13:12:09Z
Type          = service
Priority      = 50
Datacenters   = stat
Namespace     = default
Node Pool     = default
Status        = dead
Periodic      = false
Parameterized = false

Summary
Task Group  Queued  Starting  Running  Failed  Complete  Lost  Unknown
grafana     0       0         0        0       1         0     0
loki        0       0         0        1       1         0     0
postgres    0       0         0        0       1         0     0
prometheus  0       0         0        2       0         0     0

Allocations
No allocations placed
```

To restart a job, use ``nomad job restart <job>``:

```
$ docker exec -t ${SERVER_CONTAINER_ID} nomad job restart connector
==> 2024-02-13T19:32:02Z: Restarting 1 allocation
    2024-02-13T19:32:02Z: Restarting running tasks in allocation "e80cdf6f" for group "connector"
==> 2024-02-13T19:32:03Z: Job restart finished

Job restarted successfully!

$ docker exec -t ${SERVER_CONTAINER_ID} nomad job restart monitoring
No allocations to restart
```

The monitoring job cannot be restarted as it is not running.


## Clean up lingering resources

To clean up jumppad lingering resources:
```
# teardown running configuration
.bin/jumppad down

# remove lingering configuration
rm -rf ~/.jumppad
```

A deeper clean might require:
```
# clear download cache
rm .bin/jumppad

# kill all docker processes
docker ps -a -q | xargs docker rm -f

# clear docker cache
docker system prune

# remove all volumes
docker volume prune

# remove all networks
docker network prune
```

# FAQ

## a network with the label id: module.nomad.resource.network.station, was not found

Solution: jumppad is confused by a lingering configuration. Clean up any lingering
resources (see [above](#clean-up-lingering-resources)).

## unable to destroy resource Name: station, Type: network

Solution: some containers are still running. Stop them, or force stop all containers using ``docker ps -q -a | xargs docker rm -f``.
