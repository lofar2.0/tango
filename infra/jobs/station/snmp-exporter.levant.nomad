job "snmp-exporter" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "snmp-exporter" {
    count = 1

    network {
      mode = "cni/station"
    }

    service {
      tags = ["scrape"]
      name = "snmp-exporter"
      task = "snmp-exporter"
      port = "9116"
      address_mode = "alloc"

      meta {
        metrics_address = "snmp-exporter.service.consul:9116"
        metrics_path = "/metrics"
      }
    }

    task "snmp-exporter" {
      driver = "docker"

      config {
        image   = "[[.registry.astron.url]]/snmp-exporter:[[.image_tag]]"
        ports   = ["http"]
        args    = ["--config.file=/etc/snmp_exporter/snmp.yml", "--snmp.module-concurrency=3", "--snmp.wrap-large-counters"]
      }
      resources {
        cpu    = 100
        memory = 100
      }
    }
  }
}
