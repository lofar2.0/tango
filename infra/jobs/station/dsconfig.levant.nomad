job "dsconfig" {
  parameterized {
    payload = "required"
  }

  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "batch"

  group "dsconfig" {
    count = 1

    network {
      mode = "bridge"
    }

    task "dsconfig" {
      driver = "docker"

      config {
        image = "[[ .registry.astron.url ]]/dsconfig:[[ .image_tag ]]"
        mount {
          type   = "bind"
          source = "local/dsconfig-update-settings.json"
          target = "/tmp/dsconfig-update-settings.json"
        }
        mount {
          type   = "bind"
          source = "local/run.sh"
          target = "/run.sh"
        }
        entrypoint = ["bash", "/run.sh"]

      }

      dispatch_payload {
        file = "dsconfig-update-settings.json"
      }
      template {
        data        = <<EOH
        #!/bin/bash
        /manage_object_properties.py --write < /tmp/dsconfig-update-settings.json
        json2tango --write --update /tmp/dsconfig-update-settings.json
        EOH
        destination = "local/run.sh"
      }
      resources {
        cpu    = 100
        memory = 100
      }
      env {
        TANGO_HOST = "tango.service.consul:10000"
      }
    }

  }
}
