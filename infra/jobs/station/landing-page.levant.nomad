job "landing-page" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "web-server" {
    count = 1

    network {
      mode = "bridge"
      port "minio_console" {
        static = 9001
        host_network = "external"
      }
      port "minio" {
        static = 9000
        host_network = "external"
      }
      port "http" {
        static = 80
        host_network = "external"
      }
    }

    task "nginx" {
      driver = "docker"

      config {
        image = "[[.registry.astron.url]]/landing-page:[[.image_tag]]"
        ports = ["http"]
        mount {
          type   = "bind"
          source = "conf.d/load-balancer.conf"
          target = "/etc/nginx/conf.d/load-balancer.conf"
        }
      }

      resources {
        cpu    = 250
        memory = 512
      }

      template {
        destination     = "conf.d/load-balancer.conf"
        data = <<EOF
{{- if service "grafana" -}}
upstream grafana-backend {
{{- range service "grafana" -}}
  server {{ .Address }}:{{ .Port }};
{{- end }}
}
{{- end }}

{{- if service "jupyter" -}}
upstream jupyter-backend {
{{- range service "jupyter" -}}
  server {{ .Address }}:{{ .Port }};
{{- end }}
}
{{- end }}

EOF
      }
    }

    task "haproxy" {
      driver = "docker"

      config {
        image = "haproxy:alpine"
        ports = ["minio","minio_console"]
        mount {
          type   = "bind"
          source = "haproxy/"
          target = "/usr/local/etc/haproxy/"
        }
      }


      template {
        destination     = "haproxy/haproxy.cfg"
        data = <<EOF
resolvers consul
  accepted_payload_size 8192
  hold valid 5s
  nameserver consul 10.99.250.250:8600

frontend minio_console
  bind *:9001
  bind :::9001
  mode    tcp
  default_backend b_minio_console
frontend minio
  bind *:9000
  bind :::9000
  mode    tcp
  default_backend b_minio

backend b_minio
  mode tcp
  balance roundrobin
  server-template minio 1 _s3._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4

backend b_minio_console
  mode tcp
  balance roundrobin
  server-template minio 1 _s3-console._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4

EOF
      }
    }
  }
}
