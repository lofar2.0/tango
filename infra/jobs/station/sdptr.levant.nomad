job "sdptr" {
    region      = "[[.region]]"
    datacenters = ["stat"]
    type        = "service"

    [[ range $name, $tr := $.sdptr.instances ]]
        group "[[ $name ]]" {
            count = 1
            network {
                mode = "cni/station"
            }

            service {
                name = "sdptr-[[ $name ]]"
                port = "4840"
                address_mode = "alloc"
            }

            task "translator" {
                driver = "docker"

                config {
                    image   = "git.astron.nl:5000/lofar2.0/sdptr:[[ $.sdptr.version ]]"
                    command = "/usr/local/bin/sdptr-[[ $name ]]"
                    args    = [
                        "--port", "4840",
                        "--first_gn", "[[ $tr.first_gn ]]",
                        "--fpgas", "[[ $tr.fpgas ]]", "--nodaemon"
                    ]
                }

                resources {
                    cpu    = 500
                    memory = 100
                }
            }
        }
    [[ end ]]
}
