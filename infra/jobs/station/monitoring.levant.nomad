job "monitoring" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "postgres" {
    count = 1

    network {
      mode = "cni/station"
    }

    volume "postgresql" {
      type      = "host"
      read_only = false
      source    = "monitoring-postgresql-data"
    }

    service {
      name         = "postgres"
      port         = "5432"
      task         = "postgres"
      address_mode = "alloc"

    }

    task "postgres" {
      driver = "docker"

      volume_mount {
        volume      = "postgresql"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }

      config {
        image = "[[.registry.astron.url]]/postgres:[[.monitoring.db.version]]"
      }

      env {
        POSTGRES_DB       = "grafana"
        POSTGRES_USER     = "postgres"
        POSTGRES_PASSWORD = "password"
      }

      resources {
        cpu    = 250
        memory = 512
      }
    }
  }

  group "grafana" {
    network {
      mode = "cni/station"
    }

    service {
      tags = ["haproxy", "scrape"]
      name = "grafana"
      port = "3000"
      address_mode = "alloc"

      meta {
        metrics_path = "/grafana/metrics"
      }
    }

    task "wait-for-db" {
      lifecycle {
        hook    = "prestart"
        sidecar = false
      }
      driver = "docker"

      config {
        image   = "[[.registry.astron.url]]/postgres:[[.monitoring.db.version]]"
        command = "sh"
        args    = ["-c", "while ! pg_isready -h postgres.service.consul; do sleep 1; done"]
      }
      env {
        PGPASSFILE = "local/pgpass.txt"
      }

      template {
        data        = <<EOH
postgres.service.consul:5432:grafana:postgres:password
                EOH
        destination = "local/pgpass.txt"
        perms = "600"
      }
    }

    task "grafana" {
      driver = "docker"

[[ if ne .station "dev" ]]
      vault {
        change_mode = "noop"
        policies = ["default"]
      }
[[ end ]]
      config {
        image = "[[.registry.astron.url]]/grafana:[[.image_tag]]"
        mount {
          type   = "bind"
          source = "local/datasource-prometheus.yaml"
          target = "/etc/grafana/provisioning/datasources/prometheus.yaml"
        }
        mount {
          type   = "bind"
          source = "local/datasource-loki.yaml"
          target = "/etc/grafana/provisioning/datasources/loki.yaml"
        }
      }

      env {
[[ if ne .station "dev" ]]
        GF_SERVER_DOMAIN              = "[[.station]]c.control.lofar"
        GF_LOG_LEVEL                  = "warn"
[[ else ]]
        GF_SERVER_DOMAIN              = "grafana.service.consul"
[[ end ]]
        GF_SERVER_ROOT_URL            = "%(protocol)s://%(domain)s:%(http_port)s/grafana/"
        GF_SERVER_SERVE_FROM_SUB_PATH = "true"
        GF_DATABASE_TYPE              = "postgres"
        GF_DATABASE_HOST              = "postgres.service.consul:5432"
        GF_DATABASE_NAME              = "grafana"
        GF_DATABASE_USER              = "postgres"
        GF_DATABASE_PASSWORD          = "password"
        GF_AUTH_ANONYMOUS_ENABLED     = "true"
        GF_AUTH_ANONYMOUS_ORG_ROLE    = "Admin"
        GF_AUTH_DISABLE_LOGIN_FORM    = "true"
      }

      template {
        data        = <<EOH
        apiVersion: 1

        datasources:
          - name: Prometheus
            type: prometheus
            access: proxy
            orgId: 1
            uid: prometheus
            url: http://prometheus.service.consul:9090
            isDefault: true

        EOH
        destination = "local/datasource-prometheus.yaml"
      }
      template {
        data        = <<EOH
        apiVersion: 1

        datasources:
          - name: Loki
            type: loki
            access: proxy
            orgId: 1
            uid: loki
            url: http://loki.service.consul:3100
            jsonData:
              esVersion:  7.10.0
              includeFrozen: false
              logLevelField:
              logMessageField:
              maxConcurrentShardRequests: 5
              timeField: "@timestamp"

        EOH
        destination = "local/datasource-loki.yaml"
      }
[[ if ne .station "dev" ]]
      template {
        env = true
        destination = "secrets/file.env"
        data = <<EOH
        SLACK_TOKEN                   = "{{with secret "kv-v2/data/grafana/slack"}}{{.Data.data.token}}{{end}}"
        EOH
      }
[[ end ]]
      resources {
        cpu        = 250
        memory     = 1024
        memory_max = 8192
      }
    }
  }
  group "prometheus" {

    network {
      mode = "cni/station"
    }

    volume "prometheus" {
      type      = "host"
      read_only = false
      source    = "monitoring-prometheus-data"
    }

    service {
      tags         = ["haproxy", "scrape"]
      name         = "prometheus"
      port         = "9090"
      address_mode = "alloc"


      check {
        type     = "http"
        name     = "prometheus_health"
        port     = "9090"
        path     = "/-/healthy"
        interval = "20s"
        timeout  = "30s"
        address_mode = "alloc"
      }
    }

    task "prometheus" {
      driver = "docker"

      volume_mount {
        volume      = "prometheus"
        destination = "/prometheus"
        read_only   = false
      }

      config {
        image = "[[.registry.astron.url]]/prometheus:[[.monitoring.prometheus.version]]"
        args  = [
          "--config.file=/local/prometheus.yaml",
          "--web.enable-remote-write-receiver",
          "--storage.tsdb.retention.time=10y"
        ]
      }

      template {
        data          = <<EOH
        global:
          evaluation_interval: 10s
          scrape_interval: 10s
          scrape_timeout: 10s
[[ if ne .station "dev" ]]
        remote_write:
          - url: http://metrics.service.lofar-central.consul:9009/api/v1/push
            write_relabel_configs:
              - target_label: host
                replacement: [[.station]]
            headers:
              "X-Scope-OrgID": lofar
[[ end ]]
        scrape_configs:
          - job_name: 'consul'
            metrics_path: '/v1/agent/metrics'
            params:
              format: ['prometheus']
            static_configs:
              - targets: ['consul.service.consul:8500']
            relabel_configs:
              - target_label: host
                replacement: localhost

          - job_name: 'nomad_metrics'
            consul_sd_configs:
            - server: 'consul.service.consul:8500'
              services: ['nomad']
            relabel_configs:
            - target_label: host
              replacement: localhost
            - source_labels: ['__meta_consul_tags']
              regex: '(.*)http(.*)'
              action: keep
            - source_labels: [__meta_consul_service]
              target_label: instance # avoid a dynamic ip:port or hostname
            scrape_interval: 5s
            metrics_path: /v1/metrics
            params:
              format: ['prometheus']

          - job_name: "consul_services"
            consul_sd_configs:
              - server: 'consul.service.consul:8500'
                services:
          {{range services}}{{ if and (in .Tags "scrape") (.Name | contains "sidecar" | not) }}
                  - '{{.Name}}'
          {{ end }}{{end}}
            relabel_configs:
              - target_label: host
                replacement: localhost
              - source_labels: [__meta_consul_service_metadata_metrics_path]
                regex:  '(/.*)'            # capture '/...' part
                target_label: __metrics_path__  # change metrics path
              - source_labels: [__meta_consul_service_metadata_metrics_address]
                regex:  '(.+)'
                target_label: __address__  # change address
              - source_labels: [__meta_consul_service]
                target_label: instance # avoid a dynamic ip:port or hostname

          - job_name: "consul_snmp_services"
            scrape_interval: 60s
            scrape_timeout: 30s
            metrics_path: /snmp
            params:
              module: [if_mib]
            consul_sd_configs:
              - server: 'consul.service.consul:8500'
                services:
          {{range services}}{{ if and (in .Tags "snmp-scrape") (.Name | contains "sidecar" | not) }}
                  - '{{.Name}}'
          {{ end }}{{end}}
            metric_relabel_configs:
              - source_labels: [prefix, __name__]
                separator: ""
                target_label: __name__
              - action: labeldrop
                regex: prefix
            relabel_configs:
              - target_label: host
                replacement: localhost
              - source_labels: [__meta_consul_service_metadata_mibs]
                target_label: __param_module
              - source_labels: [__meta_consul_service_address]
                target_label: __param_target
              - source_labels: [__meta_consul_service_metadata_auth]
                target_label: __param_auth
              - target_label: __address__
                replacement: "snmp-exporter.service.consul:9116"
              - source_labels: [__meta_consul_service_metadata_prefix]
                target_label: prefix
              - source_labels: [__meta_consul_service]
                target_label: instance # avoid a dynamic ip:port or hostname
        EOH
        change_mode   = "signal"
        change_signal = "SIGHUP"
        destination   = "local/prometheus.yaml"
      }

      resources {
        cpu        = 500
        memory     = 2048
        memory_max = 8192
      }
    }
  }

  group "loki" {

    ephemeral_disk {
      size = 101
    }
    network {
      mode = "cni/station"
    }

    volume "loki" {
      type      = "host"
      read_only = false
      source    = "monitoring-loki-data"
    }

    service {
      tags         = ["haproxy", "scrape"]
      name         = "loki"
      port         = "3100"
      address_mode = "alloc"

    }

    task "loki" {
      driver = "docker"

      volume_mount {
        volume      = "loki"
        destination = "/loki"
        read_only   = false
      }

      config {
        image = "[[.registry.astron.url]]/loki:[[.monitoring.loki.version]]"
        args  = [
          "-config.file=/local/config.file"
        ]
      }

      template {
        data        = <<EOH
        auth_enabled: false

        server:
[[ if eq .station "dev" ]]
          log_level: debug
[[ else ]]
          log_level: warn
[[ end ]]
          http_listen_port: 3100

        common:
          path_prefix: /loki
          storage:
            filesystem:
              chunks_directory: /loki/chunks
              rules_directory: /loki/rules
          replication_factor: 1
          ring:
            kvstore:
              store: inmemory
        limits_config:
          ingestion_rate_mb: 1000
          ingestion_burst_size_mb: 10000
          max_global_streams_per_user: 0
          per_stream_rate_limit: 100MB
          per_stream_rate_limit_burst: 1000MB

[[ if eq .station "dev" ]]
        distributor:
          rate_store:
            max_request_parallelism: 2000
            stream_rate_update_interval: 1s
            ingester_request_timeout: 2500ms
            debug: true
[[ end ]]

        schema_config:
          configs:
            - from: 2024-01-01
              store: tsdb
              object_store: filesystem
              schema: v13
              index:
                prefix: index_
                period: 24h

        ruler:
          alertmanager_url: http://localhost:9093

        analytics:
          reporting_enabled: false
        EOH
        change_mode   = "signal"
        change_signal = "SIGHUP"
        destination   = "local/config.file"
      }

      resources {
        cpu        = 250
        memory     = 768
        memory_max = 8192
      }
    }
  }
}
