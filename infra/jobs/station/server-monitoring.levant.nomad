job "server-monitoring" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "node-exporter" {
    count = 1

    network {
      mode = "host"
      port "http" {
        to = 9100
      }
    }

    service {
      tags = ["scrape"]
      name = "node-exporter"
      task = "node-exporter"
      port = "http"

      meta {
        metrics_address = "10.99.250.250:${NOMAD_PORT_http}"
      }
    }

    task "node-exporter" {
      driver = "docker"

      config {
        # node-exporter must monitor the host, not the container,
        # so must run in the host environment

        image            = "quay.io/prometheus/node-exporter:latest"
        ports            = ["http"]
        args             = ["--path.rootfs=/host"]

        network_mode     = "host"
        pid_mode         = "host"

        mount {
          type     = "bind"
          target   = "/host"
          source   = "/"
          readonly = true
          bind_options {
            propagation = "rslave"
          }
        }
      }
      resources {
        cpu    = 100
        memory = 100
      }
    }
  }

  group "chrony-exporter" {
    count = 1

    network {
      mode = "cni/station"
    }

    service {
      tags = ["scrape"]
      name = "chrony-exporter"
      task = "chrony-exporter"
      port = "80"
      address_mode = "alloc"
    }

    task "chrony-exporter" {
      driver = "docker"

      config {
        image   = "quay.io/superq/chrony-exporter:latest"
        ports   = ["http"]
        # NB: For --collector.serverstats, we need to connect to chrony over the UNIX domain socket,
        # see https://github.com/SuperQ/chrony_exporter/issues/66
        args    = ["--collector.sources", "--chrony.address=10.99.250.250:323", "--web.listen-address=:80"]
      }
      resources {
        cpu    = 100
        memory = 100
      }
    }
  }
}
