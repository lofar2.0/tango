job "tango" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "database" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    network {
      mode = "cni/station"
    }

    volume "database" {
      type      = "host"
      read_only = false
      source    = "tango-database"
    }

    service {
      name = "tangodb"
      port = "3306"
      address_mode="alloc"
      check {
        type     = "tcp"
        interval = "10s"
        timeout  = "2s"
        address_mode="alloc"
        port     = "3306"
      }
    }

    task "database" {
      driver = "docker"

      volume_mount {
        volume      = "database"
        destination = "/var/lib/mysql"
        read_only   = false
      }

      config {
        image = "[[.registry.astron.url]]/tango-db:[[.tango.db.version]]"
      }

      env {
        MYSQL_ROOT_PASSWORD = "secret"
        MYSQL_DATABASE      = "tango"
        MYSQL_USER          = "tango"
        MYSQL_PASSWORD      = "tango"
      }

      resources {
        cpu    = 1024
        memory = 512
      }
    }
  }

  group "device-server" {
    count = 1

    restart {
      attempts = 10
      interval = "10m"
      delay    = "60s"
      mode     = "delay"
    }

    network {
      mode = "cni/station"
    }

    service {
      name = "tango"
      port = "10000"
      address_mode="alloc"

      check {
        type     = "tcp"
        interval = "10s"
        timeout  = "20s"
        port = "10000"
        address_mode="alloc"
      }
    }

    task "wait-for-db" {
      lifecycle {
        hook    = "prestart"
        sidecar = false
      }
      driver = "docker"

      config {
        image   = "[[.registry.astron.url]]/busybox:latest"
        command = "sh"
        args    = ["-c", "while ! nc -z ${MYSQL_HOST} ${MYSQL_PORT}; do sleep 1; done"]
      }

      template {
        wait {
          min     = "30s"
          max     = "300s"
        }
        error_on_missing_key = true
        data        = <<EOH
{{- range service "tangodb" }}
MYSQL_HOST     = "{{ .Address }}"
MYSQL_PORT     = "{{ .Port }}"
{{- end }}
EOH
        destination = "local/env.txt"
        env         = true
      }
    }

    task "database-ds" {
      driver = "docker"


      config {
        image      = "[[.registry.astron.url]]/tango-databaseds:[[.tango.databaseds.version]]"
        entrypoint = [
          "/usr/local/bin/DataBaseds",
          "2",
          "-ORBendPoint",
          "giop:tcp:0.0.0.0:10000",
          "-ORBendPointPublish",
          "giop:tcp:tango.service.consul:10000"
        ]
      }

      env {
        MYSQL_DATABASE = "tango"
        MYSQL_USER     = "tango"
        MYSQL_PASSWORD = "tango"
        TANGO_HOST     = "tango.service.consul:10000"
      }

      template {
        data        = <<EOH
        {{ range service "tangodb" }}
        MYSQL_HOST     = "{{ .Address }}:{{ .Port }}"
        {{ end }}
        EOH
        destination = "local/env.txt"
        env         = true
      }


      resources {
        cpu    = 1024
        memory = 512
      }
    }
  }
}
