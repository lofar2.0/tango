job "object-replication" {
  datacenters = ["stat"]
  type        = "batch"

  periodic {
    cron             = "*/5 * * * * *"
    prohibit_overlap = true
  }
  group "batch" {
    count = 1

    network {
      mode = "bridge"
    }

    task "caltables" {
      driver = "docker"
      config {
        image = "minio/mc:[[.object_storage.mc.version]]"

        entrypoint = ["mc", "batch", "start", "local", "/local/caltables.yaml" ]

        mount {
          type   = "bind"
          source = "local/mc"
          target = "/root/.mc"
        }
      }

      env {
        MINIO_ROOT_USER     = "[[.object_storage.user.name]]"
        MINIO_ROOT_PASSWORD = "[[.object_storage.user.pass]]"
      }

      resources {
        cpu    = 10
        memory = 512
      }

      template {
        destination     = "local/mc/config.json"
        change_mode     = "noop"
        data = <<EOF
{
  "aliases": {
    "local": {
      "url": "http://s3.service.consul:9000",
      "accessKey": "[[.object_storage.user.name]]",
      "secretKey": "[[.object_storage.user.name]]",
      "api": "s3v4",
      "path": "on"
    }
  }
}
EOF
      }
      template {
        destination     = "local/caltables.yaml"
        change_mode     = "noop"
        data = <<EOF
replicate:
  apiVersion: v1
  source:
    type: minio
    bucket: central-caltables
    prefix: [[.station | toUpper]]
    endpoint: "https://s3.lofar.net"
    path: "on"
    credentials:
      accessKey: [[.object_storage.user.name]]
      secretKey: [[.object_storage.user.name]]
  target:
    type: minio
    bucket: caltables
    path: "on"
      EOF
      }
    }

    task "iers" {
      driver = "docker"
      config {
        image = "minio/mc:[[.object_storage.mc.version]]"

        entrypoint = ["mc", "batch", "start", "local", "/local/iers.yaml" ]

        mount {
          type   = "bind"
          source = "local/mc"
          target = "/root/.mc"
        }
      }

      env {
        MINIO_ROOT_USER     = "[[.object_storage.user.name]]"
        MINIO_ROOT_PASSWORD = "[[.object_storage.user.pass]]"
      }

      resources {
        cpu    = 10
        memory = 512
      }

      template {
        destination     = "local/mc/config.json"
        change_mode     = "noop"
        data = <<EOF
{
  "aliases": {
    "local": {
      "url": "http://s3.service.consul:9000",
      "accessKey": "[[.object_storage.user.name]]",
      "secretKey": "[[.object_storage.user.name]]",
      "api": "s3v4",
      "path": "on"
    }
  }
}
EOF
      }
      template {
        destination     = "local/iers.yaml"
        change_mode     = "noop"
        data = <<EOF
replicate:
  apiVersion: v1
  source:
    type: minio
    bucket: central-iers
    prefix: ""
    endpoint: "https://s3.lofar.net"
    path: "on"
    credentials:
      accessKey: [[.object_storage.user.name]]
      secretKey: [[.object_storage.user.name]]
  target:
    type: minio
    bucket: iers
    path: "on"
      EOF
      }
    }
  }
}

