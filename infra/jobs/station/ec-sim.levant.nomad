job "ec-sim" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "ec-sim" {
    count = 1

    network {
      mode = "cni/station"
    }

    service {
      name = "ec-sim"
      port = "4840"
      address_mode="alloc"
    }

    task "sim" {
      driver = "docker"

      config {
        image = "[[.registry.astron.url]]/ec-sim:latest"
      }
      resources {
        cpu    = 100
        memory = 100
      }
    }

  }
}
