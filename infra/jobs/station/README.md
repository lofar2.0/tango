This directory contains the Nomad jobs that deploy the software stack on our stations. It assumes the host is already configured and is running Nomad and Consul.

The following jobs are provided.

Core functionality
------------------------

* `device-server.levant.nomad`: Our Tango Control devices that control the station hardware and expose Prometheus metrics for them,
* `sdptr.levant.nomad`: The drivers for the SDPTR firmware running on the Uniboards,
* `tango.levant.nomad`: Tango Controls services.
* `dsconfig.levant.nomad`: Configuration loader for Tango Controls,

Monitoring and logging
------------------------

* `monitoring.levant.nomad`: Prometheus + Grafana stack to gather and visualise metrics,
* `logging.levant.nomad`: Vector + Loki stack to gather and store logs from all other containers,
* `server-monitoring.levant.nomad`: Services that expose Prometheus metrics for the host server,
* `snmp-monitoring.levant.nomad`: Services that expose Prometheus metrics for external SNMP devices.

Misc infrastructure
------------------------

* `landing-page.levant.nomad`: Web server proxying our HTTP interfaces, or hyperlinking to their exposed HTTP port,
* `object-storage.levant.nomad`: MinIO S3 object store for persistant storage,
* `mesh-gateway.levant.nomad`: Connectivity with the overarching LOFAR mesh network.

Development
------------------------

* `jupyter.levant.nomad`: Jupyter Labs providing a development console,
* `simulators.levant.nomad`: Simulators for station hardware for development environments lacking such hardware,
* `ec-sim.levant.nomad`: Simulators for the station's Environmental Control module.
