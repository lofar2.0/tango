job "object-storage" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "minio" {
    count = 1

    network {
      mode = "cni/station"
    }

    volume "minio" {
      type      = "host"
      read_only = false
      source    = "object-storage-data"
    }

    service {
      name = "s3"
      port = "9000"
      task = "minio"
      address_mode="alloc"
    }
    service {
      name = "s3-console"
      port = "9001"
      task = "minio"
      address_mode="alloc"
    }
    service {
      tags = ["scrape"]
      name = "s3-metrics"
      port = "9598"
      task = "minio"
      address_mode="alloc"
    }

    task "minio" {
      driver = "docker"

      volume_mount {
        volume      = "minio"
        destination = "/data"
        read_only   = false
      }

      config {
        image   = "minio/minio:[[.object_storage.minio.version]]"
        command = "server"
        args    = ["--console-address", ":9001", "/data"]
      }

      env {
        MINIO_REGION               = "[[.station]]"
        MINIO_ROOT_USER            = "[[.object_storage.user.name]]"
        MINIO_ROOT_PASSWORD        = "[[.object_storage.user.pass]]"
        MINIO_PROMETHEUS_URL       = "http://prometheus.service.consul:9090"
        MINIO_PROMETHEUS_JOBID     = "consul_services"
        MINIO_PROMETHEUS_AUTH_TYPE = "public"
        #MINIO_BROWSER_REDIRECT_URL = "http://[[.station]]c.control.lofar/minio"
      }

      resources {
        cpu    = 250
        memory     = 2048
        memory_max = 8192
      }
    }

    task "vector" {
      driver = "docker"
      config {
        image = "timberio/vector:0.32.1.custom.989ad14-distroless-static"
      }
      lifecycle {
        sidecar = true
      }
      # Vector won't start unless the sinks(backends) configured are healthy
      env {
        VECTOR_CONFIG          = "local/vector.toml"
        VECTOR_REQUIRE_HEALTHY = "true"
      }
      # resource limits are a good idea because you don't want your log collection to consume all resources available
      resources {
        cpu    = 10
        memory = 256 # 256MB
      }
      # template with Vector's configuration
      template {
        destination     = "local/vector.toml"
        change_mode     = "signal"
        change_signal   = "SIGHUP"
        left_delimiter  = "(("
        right_delimiter = "))"
        data            = <<EOF
data_dir                     = "alloc/data/vector/"
healthchecks.require_healthy = true
[sources.cluster_metrics]
  type                 = "prometheus_scrape"
  scrape_interval_secs = 60
  endpoints            = [ "http://localhost:9000/minio/v2/metrics/cluster" ]
[sources.bucket_metrics]
  type                 = "prometheus_scrape"
  scrape_interval_secs = 60
  endpoints            = [ "http://localhost:9000/minio/v2/metrics/bucket" ]
[sinks.prometheus_exporter]
  type                 = "prometheus_exporter"
  inputs               = [ "cluster_metrics", "bucket_metrics" ]
EOF
      }
    }

    task "initialise-buckets" {
      lifecycle {
        hook = "poststart"
      }

      driver = "docker"
      config {
        image = "minio/mc:[[.object_storage.mc.version]]"

        entrypoint = ["mc", "mb", "--ignore-existing", "local/caltables", "local/iers"]

        mount {
          type   = "bind"
          source = "local/mc"
          target = "/root/.mc"
        }
      }

      env {
        MINIO_ROOT_USER     = "[[.object_storage.user.name]]"
        MINIO_ROOT_PASSWORD = "[[.object_storage.user.pass]]"
      }

      resources {
        cpu    = 10
        memory = 512
      }

      template {
        destination     = "local/mc/config.json"
        change_mode     = "noop"
        data = <<EOF
{
  "aliases": {
    "local": {
      "url": "http://s3.service.consul:9000",
      "accessKey": "[[.object_storage.user.name]]",
      "secretKey": "[[.object_storage.user.name]]",
      "api": "s3v4",
      "path": "on"
    }
  }
}
EOF
      }
    }
  }
}
