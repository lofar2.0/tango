job "device-servers" {
    region      = "[[.region]]"
    datacenters = ["stat"]
    type        = "service"

    reschedule {
      unlimited = true
      delay = "30s"
      delay_function = "constant"
    }

    group "sync-IERS" {
        count = 1

        network {
          mode = "bridge"

          port "metrics" {
            to = 8081
            host_network = "station"
          }
        }

        service {
            tags = ["scrape"]
            name = "sync-iers"
            port = "metrics"
        }

        volume "IERS" {
            type      = "host"
            read_only = false
            source    = "IERS"
        }

        task "sync-IERS" {
            driver = "docker"

            volume_mount {
                volume      = "IERS"
                destination = "/opt/IERS"
                read_only   = false
            }

            config {
                image       = "[[ $.registry.astron.url ]]/mc:latest"
                entrypoint  = [""]
                command     = "/bin/bash"
                args        = ["-c", "mc alias set object-storage http://s3.service.consul:9000 [[.object_storage.user.name]] [[.object_storage.user.pass]] && mc mirror --preserve --watch object-storage/iers/ /opt/IERS/ --monitoring-address 0.0.0.0:8081" ]
            }

            resources {
                cpu    = 10
                memory = 128
            }
        }
    }

    [[ range $class := $.devices ]]
    [[ $name := $class | toLower ]]
    group "device-[[ $name ]]" {
        count = 1

        network {
            mode = "cni/station"
        }

        volume "IERS" {
          type      = "host"
          read_only = true
          source    = "IERS"
        }

        service {
            tags = ["scrape"]
            name = "device-[[ $name ]]"
            port = "9999"
            address_mode = "alloc"

            meta {
                metrics_address = "device-[[ $name ]].service.consul:8000"
                metrics_path = "/"
            }


            check {
                type = "tcp"
                interval = "20s"
                timeout = "5s"
                port= "9999"
                address_mode="alloc"
            }
        }

        task "device-[[ $name ]]" {
            driver = "docker"

            volume_mount {
              volume      = "IERS"
              destination = "/opt/IERS"
              read_only   = true
            }

            config {
                image = "[[ $.registry.astron.url ]]/lofar-device-base:[[ $.image_tag ]]"
                entrypoint = [""]
                command    = "/bin/bash"
                # The address we listen on (ORBendPoint) is also published for others to connect to for ZMQ
                # event subscriptions. As such, we should not advertise "0" or anything that for other containers
                # will resolve to their local host.
                args       = ["-c", "l2ss-ds [[ $class ]] STAT -v -ORBendPoint giop:tcp:$HOSTNAME:9999 -ORBendPointPublish giop:tcp:device-[[ $name ]].service.consul:9999"]
            }

            env {
                TANGO_HOST               = "tango.service.consul:10000"
                TANGO_ZMQ_EVENT_PORT     = "4505"
                TANGO_ZMQ_HEARTBEAT_PORT = "4506"
                MINIO_ROOT_USER          = "minioadmin"
                MINIO_ROOT_PASSWORD      = "minioadmin"
[[ if eq $.station "dev" ]]
                DEBUG_HOST               = "[[ $.debug_host ]]"
[[ end ]]
            }

            resources {
                cpu    = 25
                memory = 512
                memory_max = 2048
            }
        }
    }
    [[ end ]]
}
