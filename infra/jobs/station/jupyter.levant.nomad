job "jupyter" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  group "jupyter-lab" {
    count = 1

    network {
      mode = "cni/station"
    }

    volume "notebooks" {
      type      = "host"
      read_only = false
      source    = "jupyter-notebooks"
    }

    service {
      tags = ["scrape"]
      name = "jupyter"
      port = "8888"
      task = "jupyter"
      address_mode = "alloc"

      meta {
        metrics_path = "/jupyter/metrics"
      }
    }

    task "jupyter-lab" {
      driver = "docker"

      volume_mount {
        volume      = "notebooks"
        destination = "/home/jovyan/notebooks"
        read_only   = false
      }

      config {
        image = "[[.registry.astron.url]]/jupyter-lab:[[.image_tag]]"
        mount {
          type   = "bind"
          source = "local/jupyter_notebook_config.py"
          target = "/home/jovyan/.jupyter/jupyter_notebook_config.py"
        }
      }

      env {
        TANGO_HOST = "tango.service.consul:10000"
      }

      resources {
        cpu        = 1024
        memory     = 1024
        memory_max = 20480
      }
      template {
        data = <<EOH
c.NotebookApp.base_url = '/jupyter'
c.NotebookApp.password_required = False
c.NotebookApp.token = ''
c.NotebookApp.trust_xheaders = True
c.NotebookApp.allow_origin = '*'

        EOH

        destination = "local/jupyter_notebook_config.py"
      }
    }
  }
}
