job "rpc-server" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"
  reschedule {
    unlimited      = true
    delay          = "30s"
    delay_function = "constant"
  }
  group "station-rpc" {
    network {
      mode = "bridge"

      port "grpc" {
        static       = "50051"
        host_network = "external"
      }

      port "metrics" {
        static       = "8000"
        host_network = "station"
      }
    }

    service {
      name = "rpc"
      port = "grpc"
    }

    service {
      tags = ["scrape"]
      name = "rpc-metrics"
      port = "metrics"

      meta {
        metrics_path = "/"
      }
    }

    task "l2ss-rpc-server" {
      driver = "docker"

      config {
        image      = "[[ $.registry.astron.url ]]/lofar-device-base:[[ $.image_tag ]]"
        entrypoint = [""]
        command    = "l2ss-rpc-server"
        args       = [
          "--port", "${NOMAD_PORT_rpc}",
          "--metrics-port", "${NOMAD_PORT_metrics}"
    [[ range $name, $tr := $.sdptr.instances ]]
          , "--antenna-field", "[[ $name ]]"
          , "--statistics-zmq", "tcp://stingray-[[ $name ]]-bst-zmq.service.consul:6001"
          , "--statistics-zmq", "tcp://stingray-[[ $name ]]-sst-zmq.service.consul:6001"
          , "--statistics-zmq", "tcp://stingray-[[ $name ]]-xst-zmq.service.consul:6001"
    [[ end ]]
        ]
      }
      env {
        TANGO_HOST = "tango.service.consul:10000"
      }

      resources {
        cpu    = 25
        memory = 512
      }
    }
  }
}
