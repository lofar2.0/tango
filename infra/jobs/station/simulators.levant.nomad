job "simulators" {
  region      = "[[.region]]"
  datacenters = ["stat"]
  type        = "service"

  [[ range $name, $tr := $.simulators.pypcc ]]
  group "[[ $name ]]-sim" {
    count = 1

    network {
      mode = "cni/station"
    }

    service {
      name = "[[ $name ]]-sim"
      port = "4840"
      address_mode="alloc"
    }

    task "sim" {
      driver = "docker"

      config {
        image = "git.astron.nl:5000/lofar2.0/pypcc:[[ $.pypcc.version ]]"
        entrypoint = ["hwtr", "--simulator", "--port", "4840", "--config", "[[ $tr.config ]]"]
      }
      resources {
        cpu    = 100
        memory = 256
      }
    }
  }
  [[ end ]]


  group "sdptr-sim" {
    count = 1

    network {
      mode = "cni/station"
    }

    service {
      name = "sdptr-sim"
      port = "4840"
      address_mode = "alloc"
    }

    task "sim" {
      driver = "docker"

      config {
        image = "git.astron.nl:5000/lofar2.0/sdptr:[[ $.sdptr.version ]]"
        entrypoint = ["/usr/local/bin/sdptr", "--port", "4840", "--nodaemon"]
      }
      resources {
        cpu    = 100
        memory = 100
      }
    }

  }
}
