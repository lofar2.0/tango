job "log-scraping" {
    region      = "[[.region]]"
    datacenters = ["stat"]
[[ if ne .station "dev" ]]
    type        = "system"
[[ else ]]
    type        = "service"
[[ end ]]

    update {
        min_healthy_time  = "10s"
        healthy_deadline  = "5m"
        progress_deadline = "10m"
        auto_revert       = true
    }

    group "vector" {
        count = 1
        restart {
            attempts = 3
            interval = "10m"
            delay    = "30s"
            mode     = "delay"
        }
        network {
            mode = "cni/station"
        }

[[ if ne .station "dev" ]]
        volume "docker-sock" {
            type      = "host"
            source    = "docker-sock-ro"
            read_only = true
        }
[[ end ]]

        ephemeral_disk {
            size   = 500
            sticky = true
        }


        task "vector" {
            driver = "docker"
            config {
                image = "timberio/vector:[[.vector.version]]"
[[ if eq .station "dev" ]]
                volumes = [
                  "/var/run/docker.sock:/var/run/docker.sock"
                ]
[[ end ]]
            }
[[ if ne .station "dev" ]]
            # docker socket volume mount
            volume_mount {
                volume      = "docker-sock"
                destination = "/var/run/docker.sock"
                read_only   = true
            }
[[ end ]]
            # Vector won't start unless the sinks(backends) configured are healthy
            env {
                VECTOR_CONFIG          = "local/vector.toml"
                VECTOR_REQUIRE_HEALTHY = "true"
            }
            # resource limits are a good idea because you don't want your log collection to consume all resources available
            resources {
                cpu    = 512
                memory = 1024
            }
            # template with Vector's configuration
            template {
                destination     = "local/vector.toml"
                change_mode     = "signal"
                change_signal   = "SIGHUP"
                left_delimiter  = "(("
                right_delimiter = "))"
                data            = <<EOF
data_dir                     = "alloc/data/vector/"
healthchecks.require_healthy = true

[api]
  enabled              = true
  address              = "0.0.0.0:8686"
  playground           = false

[sources.syslog-remote]
  type                 = "syslog"
  address              = "0.0.0.0:514"
  mode                 = "tcp"
  permit_origin        = [ "10.99.0.0/16" ]

[sources.docker-local]
  type                 = "docker_logs"
  docker_host          = "/var/run/docker.sock"
  exclude_containers   = ["vector-"]

[transforms.syslog-parse]
  inputs = ["syslog-remote"]
  type   = "remap"
  source = '''
    if .facility == "daemon" && (ends_with!(.appname, "tr") || ends_with!(.appname, "tr2")) {
      # RPi translator log format is level:logger:message
      message_parts    = split!(.message, ":", limit: 3)
      .level           = downcase!(message_parts[0])
      .labels.logger   = message_parts[1]
      .message         = message_parts[2]
    }

    # unify fields across formats
    .level           = downcase!(.level || .severity || .detected_level || "info")

    # add syslog info
    .labels.source      = "syslog"
    .labels.appname     = .appname || ""
    .labels.facility    = .facility || ""
    .labels.host        = .host || .hostname || ""
    .labels.source_ip   = .source_ip || ""

    # hard limit on message size to prevent explosion
    .message         = truncate!(.message, limit: 8192, suffix: "...")

    # delete labels that vary too much or are duplicates
    del(.procid)
  '''

[transforms.docker-parse]
  inputs = ["docker-local"]
  type   = "remap"
  source = '''
    # parse_key_value only supports \", not a literal \n, so we replace them with actual newlines instead.
    .message         = replace!(.message,"\\n","\n")

    # parse trying various formats
    structured =
      parse_syslog(.message) ??
      parse_json(.message) ??
      parse_common_log(.message) ??
      parse_key_value!(.message, whitespace: "strict")
    .                = merge!(., structured)

    # unify fields across formats
    .message         = .msg || .message
    .timestamp       = parse_timestamp(.ts, "%F %T,%3f") ?? .timestamp
    .level           = downcase!(.level || .detected_level || "info")

    # add nomad info
    .labels.source          = "docker"
    .labels.nomad_job       = .label."com.hashicorp.nomad.job_name" || ""
    .labels.nomad_task      = .label."com.hashicorp.nomad.task_name" || ""
    .labels.nomad_group     = .label."com.hashicorp.nomad.task_group_name" || ""
    .labels.nomad_namespace = .label."com.hashicorp.nomad.namespace" || ""

    # delete labels that vary too much or are duplicates
    del(.label)
    del(.ts)
    del(.msg)

    # labels from the docker_logs source that will vary way too much
    del(.container_id)
    del(.container_created_at)
    del(.container_name)
    del(.host)
  '''

[transforms.postprocess]
  inputs = ["docker-parse", "syslog-parse"]
  type   = "remap"
  source = '''
    # hard limit on message size to prevent explosion
    .message         = truncate!(.message, limit: 8192, suffix: "...")

    # standard labels
    .labels.level       = .level
    .labels.source_type = .source_type
  '''

#[sinks.console]
#  # useful for debugging loki and/or grafana,
#  # since without them it is harder to see logs
#  # of for example crashing containers
#  type                 = "console"
#  inputs               = [ "syslog-remote" ]
#  encoding.codec       = "logfmt"
#  buffer.when_full     = "drop_newest"
[sinks.loki]
  type                 = "loki"
  inputs               = [ "postprocess" ]
  endpoint             = "http://loki.service.consul:3100"
  encoding.codec       = "json"
  healthcheck.enabled  = true
  remove_label_fields  = true
  out_of_order_action  = "accept"
  batch.max_bytes      = 4194304
[sinks.loki.labels]
  "*"                  = '{{ labels }}'

[[ if ne .station "dev" ]]
[sinks.loki_central]
  type                 = "loki"
  inputs               = [ "postprocess" ]
  tenant_id            = "lofar"
  endpoint             = "http://logs.service.lofar-central.consul:3100"
  encoding.codec       = "json"
  healthcheck.enabled  = true
  remove_label_fields  = true
  out_of_order_action  = "accept"
  batch.max_bytes      = 4194304
[sinks.loki_central.labels]
  "station"            = '[[.station]]'
  "*"                  = '{{ labels }}'
[[ end ]]

[sources.host_metrics]
  type                 = "host_metrics"
  scrape_interval_secs = 10
[sources.vector_metrics]
  type                 = "internal_metrics"
  scrape_interval_secs = 10
[sinks.prometheus_remote_write]
  type                 = "prometheus_remote_write"
  inputs               = [ "host_metrics", "vector_metrics" ]
  endpoint             = "http://prometheus.service.consul:9090/api/v1/write"
  healthcheck.enabled  = false
EOF
            }
            kill_timeout = "30s"
        }

        service {
            name         = "syslog"
            port         = "514"
            address_mode = "alloc"
            check {
                type     = "tcp"
                interval = "30s"
                timeout  = "5s"
                port     = "514"
                address_mode = "alloc"
            }
        }

    }
}
