Enter your LOFAR2.0 Hardware Configuration
===========================================

The software will need to be told various aspects of your station configuration, for example, the hostnames of the station hardware to control. The following settings are installation specific, and are stored as *properties* in the :ref:`tangodb`.

Stock configurations are provided for several stations, as well as using simulators to simulate the station's interface (which is the default after bootstrapping a station). These are provided in the ``CDB/stations/`` directory, and can be loaded using for example::

    sbin/dsconfig.sh --update CDB/stations/LTS_ConfigDb.json

The following sections describe the settings that are station dependent, and thus must or can be set.

Mandatory settings
-------------------

Without these settings, you will not obtain the associated functionality:

:RECV.OPC_Server_Name: Hostname of RECVTR.

  :type: ``string``

:UNB2.OPC_Server_Name: Hostname of UNB2TR.

  :type: ``string``

:SDPFirmware.OPC_Server_Name: Hostname of SDPTR.

  :type: ``string``

:SDP.OPC_Server_Name: Hostname of SDPTR.

  :type: ``string``

:SST.OPC_Server_Name: Hostname of SDPTR.

  :type: ``string``

:SST.FPGA_sst_offload_hdr_eth_destination_mac_RW_default: MAC address of the network interface on the host running this software stack, on which the SSTs are to be received. This network interface must be capable of receiving Jumbo (MTU=9000) frames.

  :type: ``string[N_fpgas]``

:SST.FPGA_sst_offload_hdr_ip_destination_address_RW_default: IP address of the network interface on the host running this software stack, on which the SSTs are to be received.

  :type: ``string[N_fpgas]``

:XST.OPC_Server_Name: Hostname of SDPTR.

  :type: ``string``

:XST.FPGA_xst_offload_hdr_eth_destination_mac_RW_default: MAC address of the network interface on the host running this software stack, on which the XSTs are to be received. This network interface must be capable of receiving Jumbo (MTU=9000) frames.

  :type: ``string[N_fpgas]``

:XST.FPGA_xst_offload_hdr_ip_destination_address_RW_default: IP address of the network interface on the host running this software stack, on which the XSTs are to be received.

  :type: ``string[N_fpgas]``

Optional settings
-------------------

These settings make life nicer, but are not strictly necessary to get your software up and running:

:RECV.Ant_mask_RW_default: Which antennas are installed.

  :type: ``bool[N_RCUs][N_antennas_per_RCU]``

:SDP.RCU_mask_RW_default: Which RCUs are installed.

  :type: ``bool[N_RCUs]``

:UNB2.UNB2_mask_RW_default: Which Uniboard2s are installed in SDP.

  :type: ``bool[N_unb]``

:SDP.TR_fpga_mask_RW_default: Which FPGAs are installed in SDP.

  :type: ``bool[N_fpgas]``

:SDP.FPGA_sdp_info_station_id_RW_default: Numeric identifier for this station.

  :type: ``uint32[N_fpgas]``
