RECVH, RECVL
---------------------

The ``recvh == DeviceProxy("STAT/RECVH/1")`` device controls the RCUs for HBA tiles.

The ``recvl == DeviceProxy("STAT/RECVL/1")`` device controls the RCUs for LBA antennas.

Central to their operations are the masks (see also :ref:`attribute-masks`):

:RCU_mask_RW: Controls which RCUs will actually be configured when attributes referring to RCUs are written.

  :type: ``bool[N_RCUs]``

:Ant_mask_RW: Controls which antennas will actually be configured when attributes referring to antennas are written.

  :type: ``bool[N_antennas]``

Typically, ``N_RCUs == 32``, and ``N_antennas == 96``.

.. note:: The antennas are hooked up to the RCUs in sets of 3, in order.

Error information
`````````````````````

These attributes summarise the basic state of the device. Any elements which are not present in ``FPGA_mask_RW`` will be ignored and thus not report errors:

:RCU_error_R: Whether the RCUs appear usable.

  :type: ``bool[N_RCUs]``

:ANT_error_R: Whether the antennas appear usable.

  :type: ``bool[N_antennas]``

:RCU_IOUT_error_R: Whether there are alarms on any of the amplitudes in the measured currents.

  :type: ``bool[N_RCUs]``

:RCU_VOUT_error_R: Whether there are alarms on any of the voltages in the measured currents.

  :type: ``bool[N_RCUs]``

:RCU_TEMP_error_R: Whether there are alarms on any of the temperatures. NB: These values are also exposed for unused RCUs (the ``RCU_mask_RW`` is ignored).

  :type: ``bool[N_RCUs]``
