TileBeam, DigitalBeam
------------------------

A primary function of the station is to combine its antenna signals to create a more sensitive signal. The antennas are typically aimed at celestial sources moving across the sky, but can also be aimed at stationary targets, for example to point at Earth-bound signals or to let the sky pass through the beam instead.

Given a certain direction, and knowing the speed of light, one can compute the differences in arrival time for light from the observed source (its wave front) towards each antenna. The antenna signals are then aligned towards the source by delaying the signal inputs based on these differences. The antennas closest to the source get the largest delay. For celestial sources, the light is assumed to be infinitely far away and thus travel in parallel towards each antenna, greatly simplifying the calculations involved.

In practice, antenna signals can only be coarsely delayed. Fine delay compensation consists of rotating the signal inputs to compensate for the remaining differences in phase. The amount of rotation is frequency dependent. The aligned signals are subsequently added, creating a single signal output of higher sensitivity towards the observed source, albeit with a narrower field of view.

Beam tracking therefor requires a *pointing* direction in which to observe, as well as the *positions* of the antennas involved. Finally, the antennas need to be periodically realigned to track moving sources. We distinguish the following concepts:

- *Beam forming* is combining individual element signals into one. This is performed by the HBAT hardware and SDP firmware,
- *Beam steering* is uploading the delays or weights to the beam-forming hardware, in order to point the beam in a certain direction,
- *Beam tracking* is updating the beam steering over time to track a celestial target, compensating for the Earth's movement through space.

The ``tilebeam == DeviceProxy("STAT/TileBeam/1")`` device configures the HBA beam former in each HBA tile, which adds the signals of its 16 elements within the tile. The output signal of these tiles is used as input for the digital beam former (just like the direct output of an LBA).

The ``digitalbeam == DeviceProxy("STAT/DigitalBeam/1")`` device configures the digital beam formed in SDP from antenna or tile inputs. The output signal in SDP are *beamlets*, which can

Both devices beamform the antennas configured in its associated ``AntennaField`` device, but differ in what they beamform and with respect to which position:

- TileBeam:

    - Beamforms HBA elements in the HBA tiles of its AntennaField device,
    - Uses ``antennafield.Antenna_Reference_ITRF_R`` as the reference position for each tile,
    - Allows a different pointing per HBA tile,
    - ``N_output := antennafield.nr_antennas_R``,
    - Uploads the computed weights to ``antennafield.HBAT_bf_delay_steps_RW``,
    - These weights are actually *delay steps* to be applied in the tile for each element.

- DigitalBeam

    - Beamforms all the antennas or tiles of its AntennaField device,
    - Uses ``antennafield.Antenna_Field_Reference_ITRF_R`` as the reference position,
    - Allows a different pointing per beamlet,
    - ``N_output := NUM_BEAMLETS = 488``,
    - Uploads the computed weights to ``beamlet.FPGA_bf_weights_pp_RW``,
    - These weights are actually complex *phase rotations* to be applied on each antenna input.

Common functionality
`````````````````````

The following functionality holds for both TileBeam and DigitalBeam.

Beam Tracking
"""""""""""""""""""""""""""""""""

Beam tracking automatically recomputes and reapplies pointings periodically, and immediately when new pointings are configured. It exposes the following interface:

:Tracking_enabled_R: Whether beam tracking is running.

  :type: ``bool``

:Pointing_direction_RW: The direction in which the beam should be tracked for each antenna. The beam tracker will steer the beam periodically, and explicitly whenever the pointings change.

  :type: ``str[N_output][3]``

:Pointing_direction_R: The last applied pointing of each antenna.

  :type: ``str[N_output][3]``

:Pointing_timestamp_R: The timestamp for which the last set pointing for each antenna was applied and set (in seconds since 1970).

  :type: ``float[N_output][3]``

A pointing describes the direction in the sky, and consists of a set of coordinates and the relevant coordinate system. They are represented as a tuple of 3 strings: ``("coordinate_system", "angle1", "angle2")``, where the interpretation of ``angle1`` and ``angle2`` depends on the coordinate system used. For example:

- ``("AZELGEO", "0deg", "90deg")`` points at Zenith (Elevation = 90°, with respect to the Earth geode),
- ``("J2000", "0deg", "90deg")`` points at the North Celestial Pole (Declination = 90°),
- ``("SUN", "0deg", "0deg")`` points at the centre of the Sun.

For a full list of the supported coordinate systems, see https://casacore.github.io/casacore/classcasacore_1_1MDirection.html

Beam Steering
"""""""""""""""""""""""""""""""""

The beam steering is responsible for pointing the beams at a target, by converting the pointing to hardware-specific weights and uploading them to the corresponding device. The beam steering is typically controlled by the beam tracker. To point the antennas in any direction manually, you should disable beam tracking first:

:Tracking_enabled_RW: Enable or disable beam tracking (default: ``True``).

  :type: ``bool``

:set_pointing(pointings): Point the beams towards the specified ``pointings[N_output][3]`` for all outputs.

  :returns: ``None``

The direction of each pointing is derived using *casacore*, which must be periodically calibrated, see also :ref:`casacore-measures`.

Timing
"""""""""""""""""""""""""""""""""

The beam tracking applies an update each *interval*, and aims to apply it at timestamps ``(now % Beam_tracking_interval) - Beam_tracking_application_offset``. To do so, it starts its computations every interval ``Beam_tracking_preparation_period`` seconds before. It then starts to compute the weights, waits to apply them, and applies them by uploading the weights to the underlying hardware.

The following properties are used:

:Beam_tracking_interval: Update the beam tracking at this interval (seconds).

  :type: ``float``

:Beam_tracking_application_offset: Update the beam tracking this amount of time before the next interval (seconds).

  :type: ``float``

:Beam_tracking_preparation_period: Prepare time for each period to compute and upload the weights (seconds).

  :type: ``float``

The following timers allow you to track the durations of each stage:

:Duration_compute_weights_R: Amount of time it took to compute the last weights (seconds).

  :type: ``float``

:Duration_preparation_period_slack_R: Amount of time left in the prepration period between computing and uploading the weights (seconds).

  :type: ``float``

:Duration_apply_weights_R: Amount of time it took to apply (upload) the weights (seconds).

  :type: ``float``

DigitalBeam
`````````````````````

The DigitalBeam device applies the following configuration to compute each beamlet. Here, ``N_ant := antennafield.nr_antennas_R`` and ``N_beamlet := NUM_BEAMLETS == N_output``.

:Antenna_Set_RW: Which antenna set (supported by the antenna field) is requested to be beam formed.

  :type: ``str``

:Antenna_Mask_R: Which antennas are requested to be beam formed, according to the selected antenna set.

  :type: ``bool[N_ant]``

:antennafield.Antenna_Usage_Mask_R: Which antennas are OK to be used (not broken, disabled, etc).

  :type: ``bool[N_ant]``


:beamlet.subband_select_RW: Which subband to beamform for each beamlet.

  :type: ``uint32[N_beamlet]``

:sdp.subband_frequency_R: Central frequency of each subband (in Hz).

  :type: ``float``
