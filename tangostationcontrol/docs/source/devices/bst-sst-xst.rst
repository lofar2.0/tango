BST, SST, and XST
--------------------

The ``bst == DeviceProxy("STAT/BST/1")``, ``sst == DeviceProxy("STAT/SST/1")`` and ``xst == DeviceProxy("STAT/XST/1")`` devices manages the BSTs (beamlet statistics) SSTs (subband statistics) and XSTs (crosslet statistics), respectively. The statistics are emitted piece-wise through UDP packets by the FPGAs on the Uniboards in SDP. By default, each device configures the statistics to be streamed to itself (the device), from where the user can obtain them.

The statistics are exposed in two ways, as:

- *Attributes*, representing the most recently received values,
- *TCP stream*, to allow the capture and recording of the statistics over any period of time.

If the statistics are not received or zero, see :ref:`statistics-debugging`.

See the following links for a full description of the BST, SST, and XST monitoring and control points:

- https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
- https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD

BST Statistics attributes
``````````````````````````````


SST Statistics attributes
``````````````````````````````

The SSTs represent the amplitude of the signal in each subband, for each antenna, as an integer value. They are exposed through the following attributes:

:sst_R: Amplitude of each subband, from each antenna.

  :type: ``uint64[N_ant][N_subbands]``

:sst_timestamp_R: Timestamp of the data, per antenna.

  :type: ``uint64[N_ant]``

:integration_interval_R: Timespan over which the SSTs were integrated, per antenna.

  :type: ``float32[N_ant]``

:subbands_calibrated_R: Whether the subband data was calibrated using the subband weights.

  :type: ``bool[N_ant]``

Typically, ``N_ant == 192``, and ``N_subbands == 512``.

XST Statistics attributes
``````````````````````````````

The XSTs represent the cross-correlations between each pair of antennas, as complex values. The phases and amplitudes of the XSTs represent the phase and amplitude difference between the antennas, respectively. They are exposed as a matrix ``xst[a][b]``, of which only the triangle ``a<=b`` is filled, as the cross-correlation between antenna pairs ``(b,a)`` is equal to the complex conjugate of the cross-correlation of ``(a,b)``. The other triangle contains incidental values, but will be mostly 0.

Complex values which cannot be represented in Tango attributes. Instead, the XST matrix is exposed as both their carthesian and polar parts:

:xst_power_R, xst_phase_R: Amplitude and phase (in radians) of the crosslet statistics.

  :type: ``float32[N_ant][N_ant]``

:xst_real_R, xst_imag_R: Real and imaginary parts of the crosslet statistics.

  :type: ``float32[N_ant][N_ant]``

:xst_timestamp_R: Timestamp of each block.

  :type: ``int64[N_blocks]``

:integration_interval_R: Timespan over which the XSTs were integrated, for each block.

  :type: ``float32[N_blocks]``

Typically, ``N_ant == 192``, and ``N_blocks == 136``.

The metadata refers to the *blocks*, which are emitted by the FPGAs to represent the XSTs between 12 x 12 consecutive antennas. The following code converts block numbers to the indices of the first antenna pair in a block::

  from tangostationcontrol.common.baselines import baseline_from_index

  def first_antenna_pair(block_nr: int) -> int:
      coarse_a, coarse_b = baseline_from_index(block_nr)
      return (coarse_a * 12, coarse_b * 12)

Conversely, to calculate the block index for an antenna pair ``(a,b)``, use::

  from tangostationcontrol.common.baselines import baseline_index

  def block_nr(a: int, b: int) -> int:
      return baseline_index(a // 12, b // 12)

Configuring the XSTs
"""""""""""""""""""""""""""""

The XSTs can be configured with several settings:

.. note:: The XST processing needs to be toggled off and on using ``FPGA_xst_processing_enable_RW`` for new settings to become active on the station.

:FPGA_xst_processing_enable_RW: Whether XSTs are computed on each FPGA.

  :type: ``bool[N_fpgas]``

:FPGA_xst_integration_interval_RW: The time interval to integrate over, per FPGA, in seconds.

  :type: ``float[N_fpgas]``

:FPGA_xst_subband_select_RW: The subband to cross correlate, per FPGA. Note: only the entries ``[x][1]`` should be set, the rest should be zero.

  :type: ``uint32[N_fpgas][8]``

Subscribe to statistics streams
``````````````````````````````````

The TCP stream interface allows a user to subscribe to the statistics packet streams, combined into a single TCP stream. The statistics will be streamed until the user disconnects, or the device is turned off. Any number of subscribers is supported, as bandwidth allows. Simply connect to the following port:

+----------+----------------+
| Device   | TCP end point  |
+==========+================+
| SST      | localhost:5101 |
+----------+----------------+
| XST      | localhost:5102 |
+----------+----------------+

The easiest way to capture this stream is to use our ``statistics_writer``, which will capture the statistics and store them in HDF5 file(s). The writer:

- computes packet boundaries,
- processes the data of each packet, and stores their values into the matrix relevant for the mode,
- stores a matrix per timestamp,
- stores packet header information per timestamp, as HDF5 attributes,
- writes to a new file at a configurable interval.

To install the software locally and run the writer::

  pip install 'tangostationcontrol@git+https://git.astron.nl/lofar2.0/tango.git#subdirectory=tangostationcontrol'
  l2ss-statistics-writer --mode SST --host localhost

The correct port will automatically be chosen, depending on the given mode. See also ``l2ss-statistics-writer -h`` for more information.

The writer can also parse a statistics stream stored in a file. This allows the stream to be captured and processed independently. Capturing the stream can for example be done using ``netcat``::

  nc localhost 5101 > SST-packets.bin
