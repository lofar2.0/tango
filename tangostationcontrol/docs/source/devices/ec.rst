.. _ec:

EC
--------------------

The ``ec == DeviceProxy("STAT/EC/1")`` device controls the Environmental Control (EC).
