Device Configuration
-------------------------

The devices receive their configuration from two sources:

- The TangoDB database, for static *properties*,
- Externally, from the user, or a control system, that set *control attributes* (see the section for each device for what to set, and :ref:`attributes` for how to set them).

.. _tangodb:

TangoDB
````````````````````````````

The TangoDB database is a persistent store for the properties of each device. The properties encode static settings, such as the hardware addresses, and default values for control attributes.

Each device queries the TangoDB for the value of its properties during the ``boot()`` (or ``initialise()``) call. Default values for control attributes can then be applied by explicitly calling ``set_defaults()``. The ``boot`` device also calls ``set_defaults()`` when initialising the station. The rationale being that the defaults can be applied at boot, but shouldn't be applied automatically during operations, as not to disturb running hardware.

Device interaction
````````````````````````````

The properties of a device can be queried from the device directly::

  # get a list of all the properties
  property_names = device.get_property_list("*")

  # fetch the values of the given properties. returns a {property: value} dict.
  property_dict = device.get_property(property_names)

Properties can also be changed::

  changeset = { "property": "new value" }

  device.put_property(changeset)

Note that new values for properties will only be picked up by the device during ``boot()`` (or ``initialise()``), so you will have to turn the device off and on.

Command-line interaction
``````````````````````````

The content of the TangoDB can be dumped from the command line using::

  sbin/dsconfig.sh --dump > tangodb-dump.json

and changes can be applied using::

  sbin/dsconfig.sh --update changeset.json

.. note:: The ``dsconfig`` docker container needs to be running for these commands to work.
