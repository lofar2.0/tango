.. _configuration:

Configuration
--------------------

The ``Configuration == DeviceProxy("STAT/Configuration/1")`` Configuration Device controls the loading, updating, exposing and dumping of the whole Station Configuration
