Beamlet
--------------------

The ``beamlet == DeviceProxy("STAT/Beamlet/LBA")`` device controls the creation and emission of beamlets . Each beamlet is a signal stream characterised by:

- The set of antennas to use as input,
- The pointing towards which to beamform these antennas,
- A single subband (frequency) selected from the PPF,
- The destination MAC, IP, and UDP ports to use for the output.


Output mapping
=====================

The output is split into multiple streams. Each stream can have its own destination, consisting of a MAC address, IP address, and UDP port number. In LOFAR, we employ the following scheme for port numbers:

+----------+----------------------------------------+
| Field    | UDP port number                        |
+==========+========================================+
| LBA      | 10000 + (station number) * 10 + 0 .. 3 |
+----------+----------------------------------------+
| HBA      | 20000 + (station number) * 10 + 0 .. 3 |
+----------+----------------------------------------+
| HBA0     | 20000 + (station number) * 10 + 0 .. 3 |
+----------+----------------------------------------+
| HBA1     | 20000 + (station number) * 10 + 6 .. 9 |
+----------+----------------------------------------+

For example, HBA1 of CS032 will be sent to ports 20326 - 20329. The globally unique port numbers help in identifying the stream at any place in the network without having to inspect the packet content.

If the data is sent to COBALT, the latter will need a matching configuration, either deployed as the default configuration through https://git.astron.nl/lofar2.0/cobalt/-/blob/main/GPUProc/etc/parset-additions.d/default/StationStreams.parset or as an override in `cbm299:/opt/lofar/etc/parset-additions.d/overide/MY_OVERRIDE_FILE.parset`. Note that the `udp+sdp` protocol needs to be specified in order to inform COBALT the data stream has packets in SDP (LOFAR2) format.
