AntennaField-HB (AFH), AntennaField-LB (AFL)
---------------------------------------------

The ``afh == DeviceProxy("STAT/AFH/HBA")`` device represents a set of *antennas* or *tiles* that collectively form a High-Band antenna field.

The ``afl == DeviceProxy("STAT/AFL/LBA")`` device represents a set of *antennas*  that collectively form a Low-Band antenna field.

They represent a selection of inputs from one or more ``RECV`` devices, mapped onto an ``SDP`` device, annotated with metadata such as positional information.

:nr_antennas_R: The number of antennas or tiles in the antenna field.

  :type: ``uint32``

It provides many settings that map onto the ``RECV`` device directly, serving as a funnel:

:ANT_mask_RW: Which antennas are configured when writing settings.

  :type: ``bool[N_antennas]``

.. warning:: Any antennas in the field that are not connected to any ``RECV`` device will return default values (f.e. ``False`` or ``0``).

Observation setup
``````````````````````````````

To use the AntennaField for an observation, it and its downstream ``RECV`` and ``SDP`` devices must be configured correctly. We provide the following functionality:

:Frequency_Band_RW: Which frequency band to select for each antenna, f.e. LBA_10_90. Must be compatible with the antenna type of the field. Writing to this attribute configures and calibrates both RECV and SDP accordingly. When read, it returns "" for any antenna that has an unknown setup.

  :type: ``str[N_antennas]``

Positions
````````````````````

The following attributes expose positional information about the individual antennas in the field, in different formats:

:Antenna_Reference_GEO_R: Reference position of each HBA tile, in latitude/longitude (degrees).

  :type: ``float64[N_tiles][2]``

:Antenna_Field_Reference_GEO_R: Reference position of the antenna field, in latitude/longitude (degrees).

  :type: ``float64[2]``

Additionally, the ``ITRF`` and ``GEOHASH`` variants provide the same information, but in ITRF (x/y/z, metres), and in Geohash strings, respectively.

Also, the offsets of the elements within each HBA tile are provided:

:HBAT_antenna_ITRF_offsets_R: Relative position of each HBA tile element with respect to the tile reference.

  :type: ``float64[N_tiles][N_elements * 3]``
  :shape: ``float64[N_tiles][N_elements][3]``

Configuration
````````````````````

The antennas represented by the antenna field are selected by the following properties:

:RECV_devices: The list of ``RECV`` devices from which antennas are selected.

  :type: ``str[]``

:SDP_device: The ``SDP`` device that processes the antennas.

  :type: ``str``

Antenna mapping
""""""""""""""""""""

These properties configure which inputs in RECV represent the power and control for each antenna:

:HBAT_Power_to_RECV_mapping: Pairs of numbers ``(recv_idx, ant_idx)`` describing the inputs on which the HBAT *power* is connected. The ``recv_idx`` is the index in ``RECV_devices``, starting at 1. The ``ant_idx`` is the absolute index of the antenna in the ``RECV`` device. A value of ``-1`` means the antenna is not connected at all.

  :type: ``int32[]``
  :shape: ``int32[][2]``

:Control_to_RECV_mapping: Pairs of numbers ``(recv_idx, ant_idx)`` describing the inputs on which the Antenna *control* is connected. The ``recv_idx`` is the index in ``RECV_devices``, starting at 1. The ``ant_idx`` is the absolute index of the antenna in the ``RECV`` device. A value of ``-1`` means the antenna is not connected at all.


Positions
""""""""""""""""""""

The positions are given in ETRS, using the following properties:

:Antenna_Reference_ETRS: Reference position of each HBA tile, in ETRS (x/y/z, metres).

  :type: ``float64[N_tiles][3]``

:Antenna_Field_Reference_ETRS: Reference position of the antenna field, in ETRS (x/y/z, metres).

  :type: ``float64[3]``

:ITRF_Reference_Frame: Reference frame to use for converting ETRS to ITRF (f.e. "ITRF2005").

  :type: ``str``

:ITRF_Reference_Epoch: Epoch towards which to extrapolate the ITRF frame, typically in half-year increments (f.e. 2015.5).

  :type: ``float32``

For the ETRS positions, there is an alternative to provide them using the respective ``ITRF`` property, which overrides the automatic ETRS-to-ITRF conversion.

HBAT element positions
````````````````````````

The positions of the elements within an HBA tile are handled differently. Instead of storing the positions of each of the 16 elements in each tile, we use the fact that the relative positions of the elements within each tile is fixed, and that in LOFAR stations, all the HBA tiles of a station are on the same plane (instead of following the curvature of the Earth). This plane is given its own station-local coordinates, the PQR system:

- It's origin is at a chosen center of the station,
- The Q axis is aligned with an absolute North (not the North of the station, which would be a different direction per station),
- The P axis is roughly East,
- The R axis is roughly down,
- The HBA tiles on a station all lie on the same PQ plane, so R == 0.

These facts allow us to use the following information to calculate the absolute position of each tile element. The conversion takes the relative offsets of the elements within a tile, rotates them in PQR space, rotates those into relative ETRS offsets, and finally into absolute positions in ETRS. See ``tangostationcontrol.tilebeam.hba_tile`` for these computations.

:recv.HBAT_PQR_rotation_angles_deg: (property) The horizontal rotation of each HBA tile in the PQ plane, in degrees (Q -> P).

  :type: ``float[96]``

:recv.PQR_to_ETRS_rotation_matrix: (property) The 3D rotation matrix to convert PQR coordinates into relative ETRS coordinates.

  :type: ``float[3][3]``

