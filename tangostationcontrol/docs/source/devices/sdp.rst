SDP
---------------------

The ``sdp == DeviceProxy("STAT/SDP/1")``` device controls the digital signal processing in SDP, performed by the firmware on the FPGAs on the Uniboards.

See the following links for a full description of the SDP monitoring and control points:

- https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
- https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD

Basic configuration
`````````````````````

The following points are significant for the operations of this device:

:FPGA_processing_enable_R: Whether the FPGA is processing its input.

  :type: ``bool[N_fpgas]``

Frequency management
```````````````````````````

To setup the input and output frequencies, the following attributes are offered:

:antenna_RW: The type of antenna connected to each input, as provided by the user (`HBA` or `LBA`).

  :type: ``str[N_fpgas][N_ants_per_fpga]``

:clock_RW: The FPGA clock, in Hz (`200_000_000` or `160_000_000`). NB: This informs the calculations which clock should be assumed. The clock is not actually toggled.

  :type: ``uint32``

:nyquist_zone_RW: The NyQuist zone of the input, per input (0, 1, or 2).

  :type: ``uint32[N_fpgas][N_ants_per_fpga]``

:FPGA_spectral_inversion_RW: Whether to invert the spectrum, both within and across all subbands. This is required in odd-numbered NyQuist zones to have the signal increase in frequency over the subbands. This setting is automatically configured by setting `nyquist_zone_RW` but can be overwritten explicitly as well.

  :type: ``bool[N_fpgas][N_ants_per_fpga]``

All of these are required to compute the actual frequencies of the subbands constructed by the PPF inside the FPGA. For convenience, the device explicitly exposes these:

:subband_frequency_R: The central frequency of each subband for each input, in Hz.

  :type: ``float64[N_fpgas][N_ants_per_fpga][N_subbands]``

Data-quality information
```````````````````````````

The following fields describe the data quality (see also :doc:`../signal_chain`):

:FPGA_signal_input_mean_R: Mean value of the last second of input (in ADC quantisation units). Should be close to 0.

  :type: ``double[N_fpgas][N_ants_per_fpga]``

:FPGA_signal_input_rms_R: Root means square value of the last second of input (in ADC quantisation units). ``rms^2 = mean^2 + std^2``. Values above 2048 indicate strong RFI. Values of 0 indicate a lack of signal input.

  :type: ``double[N_fpgas][N_ants_per_fpga]``

Error information
```````````````````````````

These attributes summarise the basic state of the device. Any elements which are not present in ``FPGA_mask_RW`` will be ignored and thus not report errors:

:FPGA_procesing_error_R: Whether the FPGAs are processing their input from the RCUs. NB: This will also raise an error if the Waveform Generator is enabled.

  :type: ``bool[N_fpgas]``

Version Information
```````````````````````````

The following fields provide version information:

:FPGA_firmware_version_R: The active firmware images.

  :type: ``str[N_fpgas]``

:FPGA_hardware_version_R: The versions of the boards hosting the FPGAs.

  :type: ``str[N_fpgas]``

:TR_software_version_R: The version of the server providing the OPC-UA interface.

  :type: ``str[N_fpgas]``

Waveform Generator
```````````````````````````

The antenna input of SDP can be replaced by an internal waveform generator for debugging and testing purposes. The generator is configured per antenna per FPGA:

.. note:: The Waveform Generator needs to be toggled off and on using ``FPGA_wg_enable_RW`` for new settings to become active on the station.

:FPGA_wg_enable_RW: Whether the waveform generator is enabled for each input.

  :type: ``bool[N_fpgas][N_ants_per_fpga]``

:FPGA_wg_phase_RW: The phases of the generated waves (in degrees). The generator needs to be turned off and on if this is changed, in order to bring the generators in sync.

  :type: ``float32[N_fpgas][N_ants_per_fpga]``

:FPGA_wg_frequency_RW: The frequencies of the generated waves (in Hz). The frequency of a subband ``s`` is LBA: ``s * 200e6/1024``, HBA low band: ``(512 + s) * 200e6/1024``, HBA high band: ``(1024 + s) * 200e6/1024``.

  :type: ``float32[N_fpgas][N_ants_per_fpga]``

:FPGA_wg_amplitude_RW: The amplitudes of the generated waves. Useful is a value of ``0.1``, as higher risks clipping.

  :type: ``float32[N_fpgas][N_ants_per_fpga]``

Usage example
"""""""""""""""""""""""

For example, the following code inserts a wave on LBA subband 102 on FPGAs 8 - 11::

  # configure FPGAs to control
  sdpfirmware.TR_fpga_mask_RW = [False] * 8 + [True] * 4 + [False] * 4

  # configure waveform generator
  sdp.FPGA_wg_phase_RW     = [[0] * 12] * 16
  sdp.FPGA_wg_amplitude_RW = [[0.1] * 12] * 16
  sdp.FPGA_wg_frequency_RW = [[102 * 200e6/1024] * 12] * 16

  # toggle and enable waveform generator
  sdp.FPGA_wg_enable_RW = [[False] * 12] * 16
  sdp.FPGA_wg_enable_RW = [[True] * 12] * 16
