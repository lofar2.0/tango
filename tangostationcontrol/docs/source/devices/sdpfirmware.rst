SDP Firmware
---------------------

The ``sdpfirmware == DeviceProxy("STAT/SDPFirmware/1")``` device controls the firmware functionalities related to the digital signal processing in SDP device. Central to its operation is the mask (see also :ref:`attribute-masks`):

:TR_fpga_mask_RW: Controls which FPGAs will actually be configured when attributes referring to FPGAs are written.

  :type: ``bool[N_fpgas]``

Typically, ``N_fpgas == 16``.

See the following links for a full description of the SDP monitoring and control points:

- https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
- https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD

Basic configuration
`````````````````````

The following points are significant for the operations of this device:

:TR_fpga_communication_error_R: Whether the FPGAs can be reached.

  :type: ``bool[N_fpgas]``

Error information
```````````````````````````

These attributes summarise the basic state of the device. Any elements which are not present in ``FPGA_mask_RW`` will be ignored and thus not report errors:

:FPGA_error_R: Whether the FPGAs appear usable.

  :type: ``bool[N_fpgas]``

