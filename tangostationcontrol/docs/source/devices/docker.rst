.. _docker:

Docker
====================

The ``docker == DeviceProxy("STAT/Docker/1")`` device controls the docker containers. It allows starting and stopping them, and querying whether they are running. Each container is represented by two attributes:

:<container>_R: Returns whether the container is running.

  :type: ``bool``

:<container>_RW: Set to ``True`` to start the container, and to ``False`` to stop it.

  :type: ``bool``

.. warning:: Do *not* stop the ``tango`` container, as doing so cripples the Tango infrastructure, leaving the station inoperable. It is also not wise to stop the ``device_docker`` container, as doing so would render this device unreachable.


