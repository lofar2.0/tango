Protection Control
=============================

The station provides mechanisms to protect itself (or its environment) from immediate damage. The ProtectionControl device ``STAT/ProtectionControl/1`` monitors key attributes of other devices, and automatically triggers a protective station shutdown if their values exceed preconfigured limits.


Observed Attributes
-----------------------------

ProtectionControl is configured to observe the following attributes:

.. literalinclude:: ../../tangostationcontrol/devices/protection_control.py
   :dedent:
   :start-after: START PROTECTION_CONFIG
   :end-before: END PROTECTION_CONFIG

Protection measures are triggered as follows:

* All devices belonging to the described class are tracked,
* A subscription is taken on the described attributes,
* Values that do not fall between ``lower_limit`` and ``upper_limit`` are considered erroneous and are discarded,
* If a value above ``maximum`` appears, protection measures are triggered,
* The ``minimal`` value is not used for now.

Protection Measures
-----------------------------

If ProtectionControl triggers, it calls its `protective_shutdown()` command, which will:

* Expose the (last) attribute that went out of bounds in ``last_threshold_device_attribute_R``,
* Enable the ``stationmanager.protection_lock_RW``, preventing state transitions that turn on more hardware,
* Order the StationManager to transition the station to ``HIBERNATE``, turning off most hardware.

ProtectionControl furthermore exposes the enum ``protection_state_R`` to expose the current state of protecting the station:

+---------------+---------------------------------------------------+
| State         | Description                                       |
+===============+===================================================+
| DEACTIVATED   | All protection is disabled.                       |
+---------------+---------------------------------------------------+
| OFF           | No shutdown is ongoing.                           |
+---------------+---------------------------------------------------+
| ARMED         | Protection is activated. Station is locked.       |
+---------------+---------------------------------------------------+
| ACTIVE        | Protection is ongoing.                            |
+---------------+---------------------------------------------------+
| TRANSITIONING | Protection is ongoing (going to HIBERNATE).       |
+---------------+---------------------------------------------------+
| FAULT         | An unrecoverable error occurred during shutdown.  |
+---------------+---------------------------------------------------+
| ABORTED       | Shutdown was aborted.                             |
+---------------+---------------------------------------------------+

Furthermore, manual intervention is possible through the following commands offered by ProtectionControl:

:protective_shutdown(): Manually trigger a shutdown.

:abort(): Stop an ongoing shutdown.

Unlock Station
-----------------------------

As long as the ``protection_lock_RW`` is enabled, the station cannot be booted. To unlock the station, run::

    stationmanager = DeviceProxy("STAT/StationManager/1")
    stationmanager.protection_lock_RW = False

Note that if the attributes monitored by ProtectionControl are still out of bounds, the station will almost instantly lock again. If you really want to disable the protection, and thus risk damaging the station, run::

    protectioncontrol = DeviceProxy("STAT/ProtectionControl/1")
    protectioncontrol.off()
