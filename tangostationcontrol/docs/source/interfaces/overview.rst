Interfaces
======================

The station provides the following interfaces accessible through your browser (assuming you run on `localhost`):

+---------------------+---------+----------------------+-------------------+
|Interface            |Subsystem|URL                   |Default credentials|
+=====================+=========+======================+===================+
| :ref:`jupyter`      |Jupyter  |http://localhost:8888 |                   |
+---------------------+---------+----------------------+-------------------+
| :doc:`monitoring`   |Grafana  |http://localhost:3000 |admin/admin        |
+---------------------+---------+----------------------+-------------------+
| Alerting            |Alerta   |http://localhost:8081 |admin/alerta       |
+---------------------+---------+----------------------+-------------------+
| :doc:`logs`         |Kibana   |http://localhost:5601 |                   |
+---------------------+---------+----------------------+-------------------+

Futhermore, there are some low-level interfaces:

+---------------------------+------------------+-----------------------+-------------------+
|Interface                  |Subsystem         |URL                    |Default credentials|
+===========================+==================+=======================+===================+
| :ref:`pytango-section`    |Tango             |tango://localhost:10000|                   |
+---------------------------+------------------+-----------------------+-------------------+
| :ref:`prometheus-section` |Prometheus        |http://localhost:9090  |                   |
+---------------------------+------------------+-----------------------+-------------------+
| TANGO-Grafana Exporter    |Python HTTPServer |http://localhost:8000  |                   |
+---------------------------+------------------+-----------------------+-------------------+
| :ref:`rest-api`           |tango-rest        |http://localhost:8080  |tango-cs/tango     |
+---------------------------+------------------+-----------------------+-------------------+
| :ref:`tangodb`            |MariaDB           |http://localhost:3306  |tango/tango        |
+---------------------------+------------------+-----------------------+-------------------+
|Archive Database           |MariaDB           |http://localhost:3307  |tango/tango        |
+---------------------------+------------------+-----------------------+-------------------+
|Log Database               |ElasticSearch     |http://localhost:9200  |                   |
+---------------------------+------------------+-----------------------+-------------------+

.. toctree::
   :hidden:

   control
   monitoring
   logs
