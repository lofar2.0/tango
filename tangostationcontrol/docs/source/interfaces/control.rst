Monitoring & Control
========================

The main API to control the station is through the `Tango Controls <https://tango-controls.readthedocs.io/en/latest/>`_ API we expose on port 10000, which is most easily accessed using a `PyTango <https://pytango.readthedocs.io/en/stable/client_api/index.html>`_ client. The Jupyter Lab installation we provide is such a client.

.. _jupyter:

Jupyter Lab
------------------------

The station offers Jupyter Lab On http://localhost:8888, which allow one to interact with the station, for example to set control points, access monitoring points, or to graph their values.

The notebooks provide some predefined variables, so you don't have to look them up:

.. literalinclude:: ../../../../docker/jupyter-lab/ipython-profiles/stationcontrol-jupyter/startup/02-devices.py

Note: the Jupyter notebooks use enhancements from the ``itango`` suite, which provide tab completions, but also the ``Device`` alias for ``DeviceProxy`` as was used in the Python examples in the next section.

For example, you can start a new *Station Control* notebook (File->New->Notebook->StationControl), and access these devices:

.. image:: jupyter_basic_example.png

You can also use Jupyter Labs integrated console to run your commands (File->New->Console->StationControl) and exploit the ``itango`` suite enhancements:

.. image:: jupyter_console_basic_example.png


Jupyter Lab and Git
------------------------

We provide the ability to interact with git repositories by including the `jupyter-git <https://github.com/jupyterlab/jupyterlab-git>`_ plugin. See their webpage for how to use this plugin.

In our installation, all git commits will be made as a fictive ``JupyterLab on $HOSTNAME`` user. This is because JupyterLab does not know who the user is, and it's preferred to explicitly state at least where the commit comes from, rather than under the name of the last person who told git who they are.

Any problems encountered in git that cannot be solved through the plugin, can be solved by spawning a Terminal and using the ``git`` command-line interface.

.. _pytango-section:

PyTango
------------------------

To access a station from scratch using Python, we need to install some dependencies::

  pip3 install tango

Then, if we know what devices are available on the station, we can access them directly::

  import tango
  import os

  # Tango needs to know where our Tango API is running.
  os.environ["TANGO_HOST"] = "localhost:10000"

  # Construct a remote reference to a specific device.
  # One can also use "tango://localhost:10000/STAT/Boot/1" if TANGO_HOST is not set
  boot_device = tango.DeviceProxy("STAT/Boot/1")

  # Print the device's state.
  print(boot_device.state())

To obtain a list of all devices, we need to access the database::

  import tango

  # Tango needs to know where our Tango API is running.
  import os
  os.environ["TANGO_HOST"] = "localhost:10000"

  # Connect to the database.
  db = tango.Database()

  # Retrieve the available devices, excluding any Tango-internal ones.
  # This returns for example: ['STAT/Boot/1', 'STAT/Docker/1', ...]
  devices = list(db.get_device_exported("STAT/*"))

  # Connect to any of them.
  any_device = tango.DeviceProxy(devices[0])

  # Print the device's state.
  print(any_device.state())

.. _rest-api:

ReST API
------------------------

We also provide a ReST API to allow the station to be controlled without needing to use the Tango API. The root access point is http://localhost:8080/tango/rest/v10/hosts/databaseds;port=10000/ (credentials: tango-cs/tango). This API allows for:

- getting and setting attribute values,
- calling commands,
- retrieving the device state,
- and more.

For example, retrieving http://localhost:8080/tango/rest/v10/hosts/databaseds;port=10000/devices/STAT/SDP/1/state returns the following JSON document::

  {"state":"ON","status":"The device is in ON state."}

For a full description of this API, see https://tango-rest-api.readthedocs.io/en/latest/.
