Broken Hardware
--------------------

Not all hardware is always functional. Broken hardware must be excluded from the signal chain, and in some cases prevented from powering up.

Disabling antennas
``````````````````````````

Not all antennas present in the field are to be used. The AntennaField device exposes the following properties for each of its antennas:

:Antenna_Quality: The condition of the antenna: 0=OK, 1=SUSPICIOUS, 2=BROKEN, 3=BEYOND_REPAIR.

  :type: ``int32[]``

:Antenna_Use: Whether each antenna should be used: 0=AUTO, 1=ON, 2=OFF. In AUTO mode, an antenna is used if its quality is OK or SUSPICIOUS. In ON mode, it is always used. In OFF mode, never.

  :type: ``int32[]``

which can also be queried as ``Antenna_Quality_R`` and ``Antenna_Use_R``.

.. note:: If these properties are updated, you should restart both the AntennaField and DigitalBeam device to propagate their effects.

The above settings result in a subset of the antennas in the AntennaField to be marked as usable. The following property exposes this conclusion:

:Antenna_Usage_Mask_R: Whether antennas will be used, according to their configured state and quality. Antennas which are configured to be BROKEN, BEYOND_REPAIR, or OFF, are not used.

  :type: ``bool[N_tiles]``

Effect on signal chain
""""""""""""""""""""""""""

The DigitalBeam device will only beamform inputs that are enabled in the ``AntennaField.Antenna_Usage_Mask_R`` attribute.


Power distribution
--------------------------

At boot, during hardware initialisation, the following devices toggle power:

* The RECV device turns all RCUs enabled in ``RCU_mask_RW`` OFF and ON,
* The RECV device powers its antennas according to its ``RCU_PWR_ANT_on_RW_default`` property,
* The AntennaField device powers its antennas, if they are:
  * Enabled in ``Antenna_Usage_Mask_R`` attribute, that is, not marked as BROKEN, BEYOND_REPAIR, or OFF,
  * Enabled in the ``Antenna_Needs_Power`` property.

.. note:: Exotic inputs like a noise source must not receive power, even when used. Use the ``Antenna_Needs_Power`` property to configure which antennas should be powered on.

