Signal Chain
====================================

The station hardware collectively processes the analog signals received by the antena dipoles, resulting in either statistics (SST/BST/XST) or beamlets. This signal chain can be monitored as it flows through the hardware as follows:

RECV: Data reception
------------------------------------

The RCU boards can receive input from three sources: an LBA, an HBA tile, and a signal or noise generator.

A typical station has ``rcu == 32`` RCUs, each of which has ``antenna == 3`` inputs.

Input
```````````

* ``recv.RCU_PWR_ANT_on_R[rcu][antenna]`` indicates whether each antenna is powered. If not, the RCU will emit *zeroes* if an LBA or HBA tile is attached.

* ``recv.RCU_PWR_ANALOG_on_R[rcu]`` indicates whether the analog power is enabled to each RCU. If not, the RCU will emit *zeroes* if an LBA or HBA tile is attached.

* ``recv.RCU_PWR_DIGITAL_on_R[rcu]`` indicates whether the digital power is enabled to each RCU. If not, the RCU will emit *zeroes*.

Processing
```````````

* ``recv.RCU_band_select_R[rcu][antenna]`` indicates which band is selected for each antenna (1 = 10MHz, 2 = 30MHz), which affects its sensitivity.

* ``recv.RCU_attenuator_dB_R[rcu][antenna]`` is the attenuation for each antenna, which affects its *amplitude*.

* ``recv.RCU_DTH_ON_R[rcu][antenna]`` indicates whether the dither source is on, which affects the signal quality:

    * ``recv.RCU_DTH_freq_R[rcu][antenna]`` is the frequency of the dither source, in Hz.

SDP: Digital signal processing
------------------------------------

The SDP can process three kinds of input: antenna data, generated waveforms, and no input, and process this into four kinds of output: beamlets, BSTs, SSTs, and XSTs.

A typical station has ``fpga == 16`` FPGAs, each of which has ``input == 12`` inputs.

Input
```````````

* ``sdp.FPGA_wg_enable_R[fpga][input]``, indicates whether waveforms are generated (True) or antenna input is used (False):

    * ``sdp.FPGA_wg_frequency_R[fpga][input]`` indicates the frequency of the generated wave,
    * ``sdp.FPGA_wg_amplitude_R[fpga][input]`` indicates the amplitude of the generated wave,
    * ``sdp.FPGA_wg_phase_R[fpga][input]`` indicates the phase of the generated wave.

* ``sdp.FPGA_signal_input_mean_R[fpga][input]`` shows the input signal strength compared to full scale (FS) = 8192.

* ``sdp.FPGA_signal_input_rms_R[fpga][input]`` shows the root means square of the input.

The signal input mean and rms behave as follows:

+--------------------+----------------+--------------------------+----------------------------+
| Input              | Configuration  | Signal Mean              | Signal RMS                 |
+====================+================+==========================+============================+
| None               |                | 0                        | 0                          |
+--------------------+----------------+--------------------------+----------------------------+
| Waveform Generator | frequency = 0  | amplitude * sin(phase)   | amplitude * 8192 / √2      |
+--------------------+----------------+--------------------------+----------------------------+
| Waveform Generator | frequency > 0  | 0                        | amplitude * 8192 / √2      |
+--------------------+----------------+--------------------------+----------------------------+
| Antenna            |                | > 0                      | > 0                        |
+--------------------+----------------+--------------------------+----------------------------+

Processing
```````````

* ``sdp.FPGA_processing_enable_R[fpga]`` indicates whether the FPGA processes its input. If not, *zeroes* are produced for all outputs.

* ``sdp.FPGA_signal_input_samples_delay_R[fpga][input]`` indicates a per-input delay to be applied, in units of 5 ns. This results in a frequency-dependent *phase* change of the input.

* ``sdp.FPGA_subband_weights_R[fpga][input * subband]`` indicates a per-subband and per-input weight factor. 8192 is unit weight, 0 means the input will be erased. Anything else results in a *phase* and/or *amplitude* change of the input.

SST output
```````````

* ``sst.FPGA_sst_offload_enable_R`` indicates whether SSTs are emitted at all.

* ``sst.nof_valid_payloads_R[fpga]`` is the number of packets received from each FPGA.

* ``sst.sst_R[fpga * input][subband]`` is the *amplitude* of the signal over the configured integration interval:

    * ``sst.FPGA_sst_offload_weighted_subbands_R[fpga * input]`` indicates whether the ``sdp.FPGA_subband_weights_R`` are applied when calculating the SSTs,
    * ``sst.integration_interval_R[fpga * input]`` is the integration interval of the provided SSTs,
    * ``sst.sst_timestamp_R[fpga * input]`` is when the SSTs were received,
    * ``sst.last_packet_timestamp_R`` is when the last SST from any FPGA was received.

If the SSTs are not received, or filled with zeroes, see also :ref:`statistics-debugging`.

XST output
```````````

* ``xst.FPGA_xst_offload_enable_R`` indicates whether XSTs are emitted at all.

* ``xst.FPGA_xst_processing_enable_R`` indicates whether XSTs are computed. If not, *zeroes* are produced.

* ``xst.nof_valid_payloads_R[fpga]`` is the number of packets received from each FPGA.

* ``xst.xst_phase_R[fpga * input][fpga * input]`` is the *phase* angle between each pair of inputs, and is defined only for ``[a][b]`` with ``a <= b``:

    * ``xst.FPGA_xst_subband_select_R[fpga][8]`` contains the subband for which to compute the XSTs. Currently, one subband is supported, which should be on index ``[fpga][1]``,
    * ``xst.FPGA_integration_interval_R[fpga]`` is the integration interval for the XSTs,
    * ``xst.xst_timestamp_R[136]`` is when the XSTs were received, per block (see below),
    * ``xst.last_packet_timestamp_R`` is when the last XST from any FPGA was received.

* ``xst.xst_amplitude_R[fpga * input][fpga * input]`` is the correlated *amplitude* between two inputs, and is subject to the same restrictions as ``xst.xst_phase_R``.

If the XSTs are not received at, or filled with zeroes, see also :ref:`statistics-debugging`.

Each block contains 12x12 XSTs, and are indexed in the same order baselines are, see https://git.astron.nl/lofar2.0/tango/-/blob/master/tangostationcontrol/tangostationcontrol/common/baselines.py on how to convert baseline indices to and from input pairs.
