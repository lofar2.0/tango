Installation
==================

You will need the following dependencies installed:

- docker
- docker-compose
- git
- make

You start with checking out the source code, f.e. the master branch, as well as the git submodules we use::

  git clone https://git.astron.nl/lofar2.0/tango.git
  cd tango
  git submodule init
  git submodule update

Next, we bootstrap the system. This will build our docker images, start key ones, and load the base configuration. This may take a while::

  cd docker-compose
  make bootstrap

If you do have access to LOFAR station hardware, you must upload its configuration to the configuration database. See :doc:`configure_station`.

Now we are ready to start the other containers::

  make start

and make sure they are all up and running::

  make status

You should see all containers either in the ``Up`` state or in ``Exit 0``. If not, you can inspect why with ``docker logs <container>``. Note that the containers will automatically be restarted on failure, and also if you reboot. Stop them explicitly to bring them down (``make stop <container>``).

Post-boot Initialisation
---------------------------

After bootstrapping, and after a reboot, the software and hardware of the station needs to be explicitly initialised. Note that the docker containers do restart automatically at system boot.

The following commands start all the software devices to control the station hardware, and initialise the hardware with the configured default settings. Go to http://localhost:8888, start a new *Station Control* notebook, and initiate the software boot sequence::

  # start and initialise the other devices
  # go through the full startup sequence
  # OFF -> HIBERNATE -> STANDBY -> ON
  stationmanager.station_hibernate()
  stationmanager.station_standby()
  stationmanager.station_on()

Configuration
---------------------------

These sections are optional, to configure specific functionality you may or may not want to use.
