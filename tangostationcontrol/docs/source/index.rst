.. LOFAR2.0 Station Control documentation master file, created by
   sphinx-quickstart on Wed Oct  6 13:31:53 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LOFAR2.0 Station Control's documentation!
====================================================

LOFAR2.0 Station Control is a software stack aimed to monitor, control, and manage a LOFAR2.0 station. In order to do so, it whips up a series of Docker containers, and combines the power of `Tango Controls <https://www.tango-controls.org/>`_, `PyTango <https://pytango.readthedocs.io/en/stable/>`_, `Docker <https://www.docker.com/>`_, `Grafana <https://grafana.com/>`_, `Jupyter Notebook <https://jupyter.org/>`_, and many others to provide a rich and powerful experience in using the station.

Full monitoring and control access to the LOFAR2.0 station hardware is provided, by marshalling their rich `OPC-UA <https://opcfoundation.org/about/opc-technologies/opc-ua/>`_ interfaces. Higher-level logic makes it possible to easily configure and obtain the LOFAR station data products (beamlets, XSTs, SSTs, BSTs) from your local machine using Python, or through one of our provided web interfaces.

Even without having access to any LOFAR2.0 hardware, you can install the full stack on your laptop, and experiment with the software interfaces.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   interfaces/overview
   devices/overview
   devices/using
   devices/afh-afl
   devices/tilebeam-digitalbeam
   devices/beamlet
   devices/recvh-recvl
   devices/sdpfirmware
   devices/sdp
   devices/bst-sst-xst
   devices/station-manager
   devices/docker
   devices/ccd
   devices/ec
   devices/configuration
   devices/temperature-manager
   devices/configure
   configure_station
   protection_control
   observing
   signal_chain
   calibration
   broken_hardware
   developer
   faq


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
