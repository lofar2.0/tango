Observing
=========================

This chapter describes how to start and manage observations.

Starting an observation
-------------------------

To observe with a station, you must construct the observation's specifications, and hand it to the ``DeviceProxy("STAT/ObservationControl/1")`` device to start::

  observation_spec = {
    "observation_id": 12345,
    "start_time": "2106-02-07T00:00:00",
    "stop_time": "2106-02-07T01:00:00",
    "antenna_field": "HBA",
    "antenna_set": "ALL",
    "filter": "HBA_210_250",
    "dithering": {
      "enabled": true,
      "power": -4.0,
      "frequency": 102000000
    },
    "SAPs": [{
          "subbands": [10, 20, 30],
          "pointing": { "angle1": 1.0, "angle2": 0, "direction_type": "J2000" }
    }, {
          "subbands": [40, 50, 60],
          "pointing": { "angle1": 2.0, "angle2": 0, "direction_type": "J2000" }
    }],
    "HBA": {
      "DAB_filter": true,
      "tile_beam": { "angle1": 1.5, "angle2": 0, "direction_type": "J2000" }
    }
  }

  import json
  obs_control = DeviceProxy("STAT/ObservationControl/1")
  obs_control.add_observation(json.dumps(observation_spec))

The above specification contains the following parameters:

+-------------------------+-----------------------------------------------------------------------------------------+
| Parameter               | Description                                                                             |
+=========================+=========================================================================================+
| ``observation_id``      | User-specified unique reference to this observation.                                    |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``start_time``          | automatically start observing when this timestamp is reached. (optional)                |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``stop_time``           | automatically stop observing when this timestamp is reached.                            |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``antenna_field``       | Which antenna field to use (LBA, HBA, HBA0, HBA1).                                      |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``antenna_set``         | Which subset of antennas to use (ALL, INNER, OUTER, EVEN, ODD).                         |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``filter``              | Which band filter to use (LBA_10_90, LBA_30_70, HBA_110_190, HBA_170_230, HBA_210_250). |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``dithering.enabled``   | Whether to add analog dithering noise to increase linearity. (optional)                 |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``dithering.power``     | Power (in dB) to apply for dithering (-4.0 to -25.0). (optional)                        |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``dithering.frequency`` | Dithering frequency (in Hz). (optional)                                                 |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``SAPs``                | List of pointings and frequencies (subbands) to track and beam form.                    |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``HBA.DAB_filter``      | Enable the analog filter on the RCUs for DAB radio frequencies. (optional)              |
+-------------------------+-----------------------------------------------------------------------------------------+
| ``HBA.tile_beam``       | Pointing to track with the HBA tiles (optional). (specify for HBA)                      |
+-------------------------+-----------------------------------------------------------------------------------------+

This will configure the specified antenna field (f.e. ``HBA``) as follows:

* ``STAT/DigitalBeam/HBA`` is configured to beam form the antennas in the specified ``antenna_set``, track all pointings given in ``SAPs[x].pointing``, and produce beamlets for all subbands in ``SAPs[x].subbands``. The beamlets mirror the subbands in the order in which they are specified,
* The ``observation_id`` is used to annotate the beamlet data produced by this observation,
* ``STAT/AFH/HBA`` is configured to use the specified ``filter`` for the RCUs,
* ``STAT/TileBeam/HBA`` is configured to beam form all HBA tiles, tracking the given ``tile_beam`` pointing.

Observation Output
````````````````````````

The effect of the observations can be observed through the following means, all of which are managed independently from the observation:

* The beamlets streaming out of the station towards the processing cluster. The ``Beamlet`` device is responsible for managing and monitoring this data flow,
* The statistics streaming out of the station towards the control softwate. The ``XST/SST/BST`` devices are responsible, and allow inspection of this data flow,
* The various input signal monitoring points available in the ``SDP`` device, such as ``FPGA_input_signal_mean_RW``.

Life cycle
````````````````````````

The ObservationControl device will start each Observation when its start time is reached or past, and will stop it at the specified stop time. You can also force this to happen::

  obs_control = DeviceProxy("STAT/ObservationControl/1")
  obs_control.start_observation_now(12345) # starts observation 12345 now, regardless of its specified start time
  obs_control.stop_observation_now(12345)  # stops observation 12345 now, regardless of its specified stop time

Managing observation(s)
-------------------------

To manage running observations, we can interact with ObservationControl::


  >>> # Check which observations are known (running or yet to run)
  >>> obs_control.observations_R
  array([12345])

  >>> # Check which observations are running
  >>> obs_control.running_observations_R
  array([12345])

  >>> # Stop a running observation
  >>> obs_control.stop_observation_now(12345)

  >>> # Stop all running observations
  >>> obs_control.stop_all_observations_now()

Alternatively, we can inspect a running observation more closely. Each observation is represented by its own device: ``STAT/Observation/$id``, so if observation 12345 has been started, we can do the following::

  observation = DeviceProxy("STAT/Observation/12345")

This device exposes its settings as individual attributes, as well as:

:alive_R: Ever-increasing value as long as the observation is running. Allows one to check whether monitoring has become stale.

  :type: ``int``

:observation_settings_RW: JSON string of the specifications of this observation. NB: This attribute cannot be written once the observation has started.

  :type: ``str``

:observation_id_R: (et al) Each specification parameter can be retrieved individually.

  :type: (depends on specification parameter)
