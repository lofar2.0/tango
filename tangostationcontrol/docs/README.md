The sphinx documentation is build through tox which can be called from
the parent directory using:

```
tox -e docs
```

After which the documentation will be available in html format in the `build/html` directory.
