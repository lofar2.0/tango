# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import urllib.request
import csv
from functools import cache
from typing import List

__all__ = ["HBARotations"]


class HBARotations(dict):
    """Rotations of each HBA field, in degrees. In pairs of (hba0, hba1) or (hba, 0.0).

    Example:

    {
      "CS001": (24.0, 24.0),
      "RS307": (10.0,  0.0),
    }
    """

    def __init__(self):
        self.raw = self.download_raw()
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw() -> List[str]:
        return urllib.request.urlopen(
            f"https://git.astron.nl/ro/lofar/-/raw/master/MAC/Deployment/data/Coordinates/data/hba-rotations.csv?ref_type=heads"
        ).readlines()

    def parse_raw(self):
        hba_rotations_csv = csv.reader((line.decode() for line in self.raw))

        for row in list(hba_rotations_csv)[1:]:
            # station, hba/hba0 rotation, hba1 rotation
            station, rotation_hba0, rotation_hba1 = row
            self[station] = (float(rotation_hba0), float(rotation_hba1 or 0.0))
