# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import urllib.request
from functools import cache
from typing import List


class AntennaCables(dict):
    """Length of the cables to all antennas, in meters.

    Example:

    { 0: { "LBL":  80, "LBH": 80, "HBA": 115 },
      1: { "LBL":  80, "LBH": 80, "HBA": 115 },
      2: { "LBL":  80, "LBH": 80, "HBA": 115 },
      ...
    }
    """

    def __init__(self, station):
        self.station = station

        self.raw = self.download_raw(station)
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw(station) -> List[str]:
        return urllib.request.urlopen(
            f"https://git.astron.nl/ro/lofar/-/raw/master/MAC/Deployment/data/StaticMetaData/CableDelays/{station.upper()}-CableDelays.conf"
        ).readlines()

    def parse_raw(self):
        cables = [line.decode() for line in self.raw]

        for row in cables:
            if "#" in row:
                row = row[: row.find("#")]

            if not row.strip():
                continue

            rcu_nr, cable_length_lbl, _, cable_length_lbh, _, cable_length_hba, _ = (
                row.split()
            )

            self[int(rcu_nr)] = {
                "LBL": cable_length_lbl,
                "LBH": cable_length_lbh,
                "HBA": cable_length_hba,
            }
