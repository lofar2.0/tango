# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import urllib.request

from tangostationcontrol.toolkit.lofar1.calibration_table_LOFAR1 import CalibrationTable

__all__ = ["LOFAR1CalTables"]


class LOFAR1CalTables(dict):
    """LOFAR1 Calibration tables for a station.

    Example:

    {
      "HBA-110_190": CalibrationTable(),
      "HBA-170-230": CalibrationTable(),
      "HBA-210-250": CalibrationTable(),
      "LBA_INNER-10_90": CalibrationTable(),
      "LBA_OUTER-10_90": CalibrationTable(),
    }
    """

    def __init__(self, station, tmpdir="."):
        self.station = station
        self.tmpdir = tmpdir

        self.files = [
            f"CalTable-{self.station_nr:03}-HBA-110_190.dat",
            f"CalTable-{self.station_nr:03}-HBA-170_230.dat",
            f"CalTable-{self.station_nr:03}-HBA-210_250.dat",
            f"CalTable-{self.station_nr:03}-LBA_INNER-10_90.dat",
            f"CalTable-{self.station_nr:03}-LBA_OUTER-10_90.dat",
        ]

        self.download_all()
        self.parse_all()

    @property
    def station_nr(self):
        return int(self.station[2:])

    def download_all(self):
        for filename in self.files:
            with open(f"{self.tmpdir}/{filename}", "wb") as f:
                url = f"https://git.astron.nl/ro/lofar1-caltables/-/raw/main/{self.station.upper()}/{filename}"
                data = urllib.request.urlopen(url).read()
                f.write(data)

    def parse_all(self):
        for f in self.files:
            filename, ext = f.rsplit(".", 1)
            _caltable, _stationnr, antennaset, band = filename.split("-")

            self[f"{antennaset}-{band}"] = CalibrationTable.load_from_file(
                f"{self.tmpdir}/{f}"
            )
