#! /usr/bin/env python3
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import urllib.request
import csv
from functools import cache
from typing import List

__all__ = ["ETRS", "PQRToETRS"]


class ETRS(dict):
    """Positions of all antennas, in ETRS.

    Example:

    { "L0": [ "3826923.503", "460915.488", "5064643.517" ],
      "H0": [ "3826923.503", "460915.488", "5064643.517" ],
      ...
    }
    """

    def __init__(self, station):
        self.station = station

        self.raw = self.download_raw(station)
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw(station) -> List[str]:
        return urllib.request.urlopen(
            f"https://git.astron.nl/ro/lofar/-/raw/master/MAC/Deployment/data/Coordinates/ETRF_FILES/{station.upper()}/{station.lower()}-antenna-positions-etrs.csv?ref_type=heads"
        ).readlines()

    def parse_raw(self):
        etrs_csv = csv.reader((line.decode() for line in self.raw))

        for row in list(etrs_csv)[1:]:
            # antenna, etrs-x, etrs-y, etrs-z, p, q, r, rcu-x, rcu-y
            antenna, etrs_x, etrs_y, etrs_z = row[:4]
            self[antenna] = [etrs_x, etrs_y, etrs_z]


class PQRToETRS(dict):
    """PQR-to-ETRS 3x3 rotation matrices.

    Example:

    { "CS003HBA0": [ -0.1195951054, -0.7919544517, 0.5987530018, 0.9928227484, -0.0954186800, 0.0720990002, 0.0000330969, 0.6030782884, 0.7976820024 ],
      ...
    }
    """

    def __init__(self):
        self.raw = self.download_raw()
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw() -> List[str]:
        return urllib.request.urlopen(
            f"https://git.astron.nl/ro/lofar/-/raw/master/MAC/Deployment/data/Coordinates/data/rotation_matrices.dat"
        ).readlines()

    def parse_raw(self):
        cables = [line.decode() for line in self.raw]

        for row in cables:
            if "#" in row:
                row = row[: row.find("#")]

            if not row.strip():
                continue

            station, antenna_field, px, py, pz, qx, qy, qz, rx, ry, rz = [
                x.strip() for x in row.split(",")
            ]

            self[f"{station.upper()}{antenna_field.upper()}"] = [
                px,
                py,
                pz,
                qx,
                qy,
                qz,
                rx,
                ry,
                rz,
            ]
