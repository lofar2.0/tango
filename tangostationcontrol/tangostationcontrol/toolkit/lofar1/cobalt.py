#! /usr/bin/env python3
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import urllib.request
from functools import cache
import socket
from typing import List

import parse

__all__ = [
    "LiveResolver",
    "OfflineResolver",
    "COBALTPhaseCenters",
    "COBALTStationStreams",
]


class Resolver:
    """Resolve hostnames to IPs and MACs."""

    def host_to_ip(self, hostname: str) -> str:
        raise NotImplementedError

    def host_to_mac(self, hostname: str) -> str:
        raise NotImplementedError


class LiveResolver(Resolver):
    """Resolve using live queries."""

    @staticmethod
    @cache
    def host_to_ip(hostname: str) -> str:
        try:
            return socket.gethostbyname(hostname)
        except socket.gaierror:
            return hostname

    @staticmethod
    @cache
    def host_to_mac(hostname: str) -> str:
        import getmac

        return getmac.get_mac_address(hostname=hostname) or "??:??:??:??:??:??"


class OfflineResolver(Resolver, dict):
    """Resolve using a lookup table."""

    def __init__(self):
        self.raw = self.download_raw()
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw() -> bytes:
        return urllib.request.urlopen(
            f"https://git.astron.nl/ro/lofar/-/raw/master/MAC/Deployment/data/StaticMetaData/COBALT2_IP_MACs.txt"
        ).readlines()

    def parse_raw(self):
        hosts = [line.decode() for line in self.raw]

        for row in hosts:
            if "#" in row:
                row = row[: row.find("#")]

            if not row.strip():
                continue

            hostname, ip, mac = row.split()

            self[hostname] = {"MAC": mac.lower(), "IP": ip}

    def host_to_mac(self, hostname: str) -> str:
        return self.get(hostname, {"MAC": "??"})["MAC"]

    def host_to_ip(self, hostname: str) -> str:
        return self.get(hostname, {"IP": hostname})["IP"]


class COBALTPhaseCenters(dict):
    """Phase centers of all antenna fields, in ITRF.

    Example:

    { "CS001LBA": [
        "3826923.503", "460915.488", "5064643.517"
      ]
    }
    """

    def __init__(self):
        self.raw = self.download_raw()
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw() -> List[str]:
        return urllib.request.urlopen(
            f"https://git.astron.nl/lofar2.0/cobalt/-/raw/main/GPUProc/etc/parset-additions.d/default/StationPositions.parset?ref_type=heads"
        ).readlines()

    def parse_raw(self):
        phase_centers_parset = [line.decode() for line in self.raw]

        for row in phase_centers_parset:
            if "#" in row:
                row = row[: row.find("#")]

            if "=" not in row:
                continue

            # f.e. PIC.Core.CS001LBA.phaseCenter = [3826923.503, 460915.488, 5064643.517]
            k, v = row.split("=", 1)
            k = k.strip()
            v = v.strip()

            kparts = list(parse.parse("PIC.Core.{}.phaseCenter", k) or [])
            vparts = list(parse.parse("[{}, {}, {}]", v) or [])

            if not kparts or not vparts:
                continue

            station_field = kparts[0]
            itrf = list(vparts)

            self[station_field] = itrf


class COBALTStationStreams(dict):
    """COBALT destinations for all stations streams.

    Example:

    { "CS001LBA": [
        { "host": "cbt201-10GB01", "IP": "10.168.160.1", "MAC": "e4:43:4B:3d:89:81", "port": 10001 },
        { "host": "cbt201-10GB01", "IP": "10.168.160.1", "MAC": "e4:43:4B:3d:89:81", "port": 10001 },
        { "host": "cbt201-10GB01", "IP": "10.168.160.1", "MAC": "e4:43:4B:3d:89:81", "port": 10001 },
        { "host": "cbt201-10GB01", "IP": "10.168.160.1", "MAC": "e4:43:4B:3d:89:81", "port": 10001 },
      ]
    }
    """

    def __init__(self, resolver=None):
        self.resolver = resolver or OfflineResolver()

        self.raw = self.download_raw()
        self.parse_raw()

    @staticmethod
    @cache
    def download_raw() -> List[str]:
        return urllib.request.urlopen(
            f"https://git.astron.nl/lofar2.0/cobalt/-/raw/main/GPUProc/etc/parset-additions.d/default/StationStreams.parset?ref_type=heads"
        ).readlines()

    def parse_raw(self):
        phase_centers_parset = [line.decode() for line in self.raw]

        for row in phase_centers_parset:
            if "#" in row:
                row = row[: row.find("#")]

            if "=" not in row:
                continue

            # f.e. PIC.Core.CS001HBA0.RSP.sources    = [udp+sdp:cbt201-10GB01.online.lofar:10010, udp+sdp:cbt201-10GB01.online.lofar:10011, udp+sdp:cbt201-10GB01.online.lofar:10012, udp+sdp:cbt201-10GB01.online.lofar:10013]
            k, v = row.split("=", 1)
            k = k.strip()
            v = v.strip()

            kparts = list(parse.parse("PIC.Core.{}.RSP.sources", k) or [])
            vparts = list(
                parse.parse(
                    "[udp+sdp:{}:{}, udp+sdp:{}:{}, udp+sdp:{}:{}, udp+sdp:{}:{}]", v
                )
                or parse.parse("[udp:{}:{}, udp:{}:{}, udp:{}:{}, udp:{}:{}]", v)
                or []
            )

            if not kparts or not vparts:
                continue

            station_field = kparts[0]
            destinations_host_port = [
                {
                    "hostname": vparts[0],
                    "MAC": self.resolver.host_to_mac(vparts[0]),
                    "IP": self.resolver.host_to_ip(vparts[0]),
                    "port": vparts[1],
                },
                {
                    "hostname": vparts[2],
                    "MAC": self.resolver.host_to_mac(vparts[2]),
                    "IP": self.resolver.host_to_ip(vparts[2]),
                    "port": vparts[3],
                },
                {
                    "hostname": vparts[4],
                    "MAC": self.resolver.host_to_mac(vparts[4]),
                    "IP": self.resolver.host_to_ip(vparts[4]),
                    "port": vparts[5],
                },
                {
                    "hostname": vparts[6],
                    "MAC": self.resolver.host_to_mac(vparts[6]),
                    "IP": self.resolver.host_to_ip(vparts[6]),
                    "port": vparts[7],
                },
            ]

            self[station_field] = destinations_host_port
