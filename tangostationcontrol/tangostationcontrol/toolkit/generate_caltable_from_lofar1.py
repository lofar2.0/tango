#! /usr/bin/env python3
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.toolkit.lofar1.caltable import LOFAR1CalTables

import numpy

from lofar_station_client.file_access import create_hdf5
from tangostationcontrol.common import calibration


class LOFAR2CalTables:
    def __init__(self, station):
        self.station = station

    def write_caltable_lba(self, lofar1_caltables):
        """Construct the LBA calibration table by combining LBA_INNER and LBA_OUTER tables from LOFAR1."""

        filename = f"CalTable-{self.station.upper()}-LBA-50MHz.h5"
        data = numpy.concatenate(
            (
                lofar1_caltables["LBA_INNER-10_90"].data,
                lofar1_caltables["LBA_OUTER-10_90"].data,
            ),
            axis=1,
        )

        self._write_caltable(filename, "LBA", lofar1_caltables["LBA_INNER-10_90"], data)

    def write_caltable_hba(self, lofar1_caltables):
        """Construct all HBA calibration tables 1:1 from their LOFAR1 equivalent."""

        for lofar1_dataset, reference_frequency in [
            ("HBA-110_190", "150MHz"),
            ("HBA-170_230", "200MHz"),
            ("HBA-210_250", "250MHz"),
        ]:
            filename = f"CalTable-{self.station.upper()}-HBA-{reference_frequency}.h5"
            data = lofar1_caltables[lofar1_dataset].data

            self._write_caltable(
                filename, "Tile", lofar1_caltables[lofar1_dataset], data
            )

    def _write_caltable(
        self,
        filename,
        antenna_name_prefix,
        lofar1_caltable_for_metadata,
        lofar1_caltable_data,
    ):
        f = create_hdf5(filename, calibration.CalibrationTable)
        with f as table:
            # convert date from YYYYMMDD to YYYY-MM-DD
            yyyy = lofar1_caltable_for_metadata.calibration_date[0:4]
            mm = lofar1_caltable_for_metadata.calibration_date[4:6]
            dd = lofar1_caltable_for_metadata.calibration_date[6:8]
            table.calibration_date = f"{yyyy}-{mm}-{dd}"

            table.calibration_name = lofar1_caltable_for_metadata.calibration_name
            table.calibration_version = lofar1_caltable_for_metadata.calibration_version

            # construct observation mode as the selected filter, f.e. HBA_110_190
            antenna_type = lofar1_caltable_for_metadata.observation_antennaset.split(
                "_"
            )[0]
            table.observation_mode = (
                f"{antenna_type}_{lofar1_caltable_for_metadata.observation_band}"
            )
            table.observation_source = lofar1_caltable_for_metadata.observation_source
            table.observation_station = self.station.upper()
            table.observation_station_version = "2.0.0"

            # fill the actual calibration values
            table.antennas = {}

            for antenna_nr in range(lofar1_caltable_data.shape[1] // 2):
                # read from LOFAR1
                calvalues_x = lofar1_caltable_data[:, antenna_nr * 2 + 0]
                calvalues_y = lofar1_caltable_data[:, antenna_nr * 2 + 1]

                # validate
                assert (
                    len(calvalues_x) == 512
                ), "Expected calibration values for 512 subbands"
                assert (
                    len(calvalues_y) == 512
                ), "Expected calibration values for 512 subbands"

                # write to LOFAR2
                table.antennas[f"{antenna_name_prefix}{antenna_nr:02}"] = (
                    calibration.CalibrationData()
                )
                table.antennas[f"{antenna_name_prefix}{antenna_nr:02}"].x = calvalues_x
                table.antennas[f"{antenna_name_prefix}{antenna_nr:02}"].y = calvalues_y


def main(**kwargs):
    import argparse
    from pprint import pprint
    from tempfile import TemporaryDirectory

    parser = argparse.ArgumentParser(
        description="Generate calibration tables for specific stations, based on LOFAR1 tables. The latter are downloaded as needed."
    )
    parser.add_argument(
        "-s",
        "--station",
        type=str,
        required=True,
        help="Generate calibration tables for this station",
    )
    parser.add_argument(
        "--no-lba",
        action="store_true",
        default=False,
        help="Do not generate calibration tables for LBA",
    )
    parser.add_argument(
        "--no-hba",
        action="store_true",
        default=False,
        help="Do not generate calibration tables for HBA",
    )
    parser.add_argument(
        "--lofar1",
        action="store_true",
        default=False,
        help="Print the LOFAR1 calibration tables",
    )

    args = parser.parse_args()

    with TemporaryDirectory() as tmpdir:
        lofar1_caltables = LOFAR1CalTables(args.station, tmpdir)
        lofar2_caltables = LOFAR2CalTables(args.station)

        if args.lofar1:
            pprint(lofar1_caltables)

        if not args.no_lba:
            lofar2_caltables.write_caltable_lba(lofar1_caltables)
        if not args.no_hba:
            lofar2_caltables.write_caltable_hba(lofar1_caltables)


if __name__ == "__main__":
    main()
