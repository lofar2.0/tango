#! /usr/bin/env python3
# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Dict, List, Tuple

from tangostationcontrol.toolkit.lofar1.hba_rotations import HBARotations
from tangostationcontrol.toolkit.lofar1.etrs import ETRS, PQRToETRS
from tangostationcontrol.toolkit.lofar1.antenna_cables import AntennaCables
from tangostationcontrol.toolkit.lofar1.cobalt import (
    COBALTPhaseCenters,
    COBALTStationStreams,
)

import mergedeep

N_pol = 2


class CDB(dict):
    """CDB file as a dict."""

    def __init__(self):
        self["servers"] = {}

    def add_device_properties(
        self, domain: str, family: str, member: str, properties: Dict[str, List[str]]
    ):
        dserver = family
        klass = family

        mergedeep.merge(
            self["servers"],
            {
                dserver: {
                    domain: {
                        klass: {
                            f"{domain}/{family}/{member}": {"properties": properties}
                        }
                    }
                }
            },
        )

    @staticmethod
    def elements_per_line(property_name: str) -> int:
        """Number of elements of a list to print on each line for a given property."""

        if property_name.startswith("FPGA_"):
            return 1
        if property_name.endswith("_ETRS") or property_name.endswith("_ITRF"):
            return 3
        if property_name.endswith("_matrix"):
            return 3
        return 6

    def __str__(self) -> str:
        tabsize = 2

        def to_str(x, indent=0, name=None):
            if isinstance(x, dict):
                return dict_to_str(x, indent)
            if isinstance(x, list):
                return list_to_str(x, indent, name)
            return f'"{x}"'

        def dict_to_str(dct, indent=0):
            lines = [
                f'"{k}": {to_str(v, indent + tabsize, k)},' for k, v in dct.items()
            ]
            # strip comma of last line with content
            lines[-1] = lines[-1][:-1]

            # return properly indented
            return "\n".join(
                ["{"]
                + [" " * (indent + tabsize) + line for line in lines]
                + [" " * indent + "}"]
            )

        def list_to_str(lst, indent=0, name=None):
            elements = [f'"{e}",' for e in lst]
            # strip comma of last element with content
            elements[-1] = elements[-1][:-1]

            # group elements per 6 on a line
            lines = []
            nr_per_line = self.elements_per_line(name)
            for line_nr in range((len(elements) + nr_per_line - 1) // nr_per_line):
                lines.append(
                    " ".join(
                        [
                            f"{e:8}"
                            for e in elements[
                                line_nr * nr_per_line : (line_nr + 1) * nr_per_line
                            ]
                        ]
                    )
                )

            # return properly indented
            return "\n".join(
                ["["]
                + [" " * (indent + tabsize) + line for line in lines]
                + [" " * indent + "]"]
            )

        return dict_to_str(self)


class StationData:
    def __init__(self, station, resolver=None):
        self.station = station
        self.etrs = ETRS(station)
        self.hba_rotations = HBARotations()
        self.cobalt_phasecenters = COBALTPhaseCenters()
        self.cobalt_station_streams = COBALTStationStreams(resolver)
        self.antenna_cables = AntennaCables(station)
        self.pqr_to_etrs = PQRToETRS()

    @property
    def station_nr(self) -> int:
        return int(self.station[2:])

    def hba_rotation(self, antenna_field: str) -> float:
        raise NotImplementedError

    def rcu_input(self, antenna_field: str, antenna_nr: int) -> Tuple[int, str]:
        raise NotImplementedError

    def cable_length(self, antenna_field: str, antenna_nr: int) -> str:
        rcu_nr, rcu_input = self.rcu_input(antenna_field, antenna_nr)
        return self.antenna_cables[rcu_nr][rcu_input]

    def properties_StationManager(self) -> Dict[str, List[str]]:
        return {
            "Station_Name": [f"{self.station.upper()}"],
            "Station_Number": [f"{self.station_nr}"],
        }

    def properties_AFH(
        self, antenna_field: str, first_antenna: int, nr_antennas: int
    ) -> Dict[str, List[str]]:
        return {
            "Antenna_Cables": [
                "%sm" % self.cable_length(antenna_field, n) for n in range(nr_antennas)
            ],
            "Antenna_Field_Reference_ITRF": self.cobalt_phasecenters[
                f"{self.station.upper()}{antenna_field.upper()}"
            ],
            "Antenna_Reference_ETRS": sum(
                [self.etrs[f"H{n + first_antenna}"] for n in range(nr_antennas)], []
            ),
            "HBAT_PQR_rotation_angles_deg": [
                f"{self.hba_rotation(antenna_field):.2f}" for _ in range(nr_antennas)
            ],
            "PQR_to_ETRS_rotation_matrix": self.pqr_to_etrs[
                f"{self.station.upper()}{antenna_field.upper()}"
            ],
        }

    def properties_AFL(self, nr_antennas: int) -> Dict[str, List[str]]:
        return {
            "Antenna_Cables": [
                "%sm" % self.cable_length("LBA", n) for n in range(nr_antennas)
            ],
            "Antenna_Field_Reference_ITRF": self.cobalt_phasecenters[
                f"{self.station.upper()}LBA"
            ],
            "Antenna_Reference_ETRS": sum(
                [self.etrs[f"L{n}"] for n in range(nr_antennas)], []
            ),
        }

    def properties_Beamlet(self, antenna_field: str) -> Dict[str, List[str]]:
        cobalt_destinations = self.cobalt_station_streams[
            f"{self.station.upper()}{antenna_field}"
        ]
        return {
            "FPGA_beamlet_output_hdr_eth_source_mac_RW_default": [
                f"00:22:86:08:{n // 4:02}:{n % 4:02}" for n in range(16)
            ],
            "FPGA_beamlet_output_hdr_ip_source_address_RW_default": [
                "10.175.1.1" for n in range(16)
            ],
            "FPGA_beamlet_output_hdr_udp_source_port_RW_default": [
                53248 + n for n in range(16)
            ],
            "FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW_default_shorthand": [
                dest["MAC"] for dest in cobalt_destinations
            ],
            "FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW_default_shorthand": [
                dest["IP"] for dest in cobalt_destinations
            ],
            "FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW_default_shorthand": [
                dest["port"] for dest in cobalt_destinations
            ],
        }

    def CDB(self) -> CDB:
        cdb = CDB()
        cdb.add_device_properties(
            "STAT", "StationManager", "1", self.properties_StationManager()
        )
        return cdb


class CSStationData(StationData):
    def hba_rotation(self, antenna_field: str) -> float:
        if antenna_field == "HBA0":
            return self.hba_rotations[self.station.upper()][0]
        if antenna_field == "HBA1":
            return self.hba_rotations[self.station.upper()][1]

    def rcu_input(self, antenna_field: str, antenna_nr: int) -> Tuple[int, str]:
        if antenna_field == "HBA0":
            return (antenna_nr, "HBA")
        if antenna_field == "HBA1":
            return (antenna_nr + 24, "HBA")
        if antenna_field == "LBA":
            if antenna_nr < 48:
                return (antenna_nr * N_pol, "LBH")
            else:
                return ((antenna_nr - 48) * N_pol, "LBL")

    def CDB(self) -> CDB:
        cdb = super().CDB()
        cdb.add_device_properties(
            "STAT", "Beamlet", "LBA", self.properties_Beamlet("LBA")
        )
        cdb.add_device_properties(
            "STAT", "Beamlet", "HBA0", self.properties_Beamlet("HBA0")
        )
        cdb.add_device_properties(
            "STAT", "Beamlet", "HBA1", self.properties_Beamlet("HBA1")
        )
        cdb.add_device_properties("STAT", "AFL", "LBA", self.properties_AFL(96))
        cdb.add_device_properties(
            "STAT", "AFH", "HBA0", self.properties_AFH("HBA0", 0, 24)
        )
        cdb.add_device_properties(
            "STAT", "AFH", "HBA1", self.properties_AFH("HBA1", 24, 24)
        )
        return cdb


class RSStationData(StationData):
    def hba_rotation(self, antenna_field: str) -> float:
        if antenna_field == "HBA":
            return self.hba_rotations[self.station.upper()][0]

    def rcu_input(self, antenna_field: str, antenna_nr: int) -> Tuple[int, str]:
        if antenna_field == "HBA":
            return (antenna_nr * N_pol, "HBA")
        if antenna_field == "LBA":
            if antenna_nr < 48:
                return (antenna_nr * N_pol, "LBH")
            else:
                return ((antenna_nr - 48) * N_pol, "LBL")

    def CDB(self) -> CDB:
        cdb = super().CDB()
        cdb.add_device_properties(
            "STAT", "Beamlet", "LBA", self.properties_Beamlet("LBA")
        )
        cdb.add_device_properties(
            "STAT", "Beamlet", "HBA", self.properties_Beamlet("HBA")
        )
        cdb.add_device_properties("STAT", "AFL", "LBA", self.properties_AFL(96))
        cdb.add_device_properties(
            "STAT", "AFH", "HBA", self.properties_AFH("HBA", 0, 48)
        )
        return cdb


def StationDataFactory(station) -> StationData:
    station_type = station[:2].upper()

    if station_type == "CS":
        return CSStationData(station)
    if station_type == "RS":
        return RSStationData(station)

    raise ValueError(station)


def main(**kwargs):
    import argparse
    from pprint import pprint

    parser = argparse.ArgumentParser(
        description="Generate CDB files for specific stations, based on LOFAR1 tables. The required input for LOFAR1 is downloaded."
    )
    parser.add_argument(
        "-s",
        "--station",
        type=str,
        required=True,
        help="Print information for this station",
    )
    parser.add_argument(
        "-C", "--cdb", action="store_true", default=False, help="Print the CDB file"
    )
    parser.add_argument(
        "--cobalt-station-streams",
        action="store_true",
        default=False,
        help="Print the COBALTStationStreams dict",
    )
    parser.add_argument(
        "--cobalt-phase-centers",
        action="store_true",
        default=False,
        help="Print the COBALTPhaseCenters dict",
    )
    parser.add_argument(
        "--etrs", action="store_true", default=False, help="Print the ETRS dict"
    )
    parser.add_argument(
        "--hba_rotations",
        action="store_true",
        default=False,
        help="Print the HBARotations dict",
    )
    parser.add_argument(
        "--antenna_cables",
        action="store_true",
        default=False,
        help="Print the AntennaCables dict",
    )
    parser.add_argument(
        "--pqr-to-etrs",
        action="store_true",
        default=False,
        help="Print the PQR-to-ETRS dict",
    )

    args = parser.parse_args()

    if args.cdb:
        station_data = StationDataFactory(args.station)
        print(str(station_data.CDB()))

    if args.cobalt_station_streams:
        pprint(COBALTStationStreams())

    if args.cobalt_phase_centers:
        pprint(COBALTPhaseCenters())

    if args.etrs:
        pprint(ETRS(args.station))

    if args.hba_rotations:
        pprint(HBARotations())

    if args.antenna_cables:
        pprint(AntennaCables(args.station))

    if args.pqr_to_etrs:
        pprint(PQRToETRS())


if __name__ == "__main__":
    main()
