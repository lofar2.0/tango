"""
This program allows the analysis of CDB JSON files that serve as input to dsconfig.
"""

import json


def merge(source: dict, destination: dict) -> dict:
    """Merge source into destination."""

    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value

    return destination


class DsConfigAnalysis:
    def __init__(self, hierarchy_child_property="Power_Children"):
        self.specs = {}
        self.hierarchy_child_property = hierarchy_child_property

    def load_file(self, fname: str):
        """Import an additional dsconfig JSON file."""

        fp = open(fname)
        json_dict = json.load(fp)

        self.load_dict(json_dict)

    def load_dict(self, config: dict):
        """Import an additional dsconfig dict."""

        merge(config, self.specs)

    def device_children(self) -> dict:
        """Return the children of each device."""

        children = {}
        for device_server in self.specs["servers"].values():
            for domain in device_server.values():
                for family in domain.values():
                    for instance, configuration in family.items():
                        children[instance] = configuration.get("properties", {}).get(
                            self.hierarchy_child_property, []
                        )

        return children

    def device_parents(self):
        """Return the parents of each device."""

        parents = {}
        for device, children in self.device_children().items():
            if device not in parents:
                parents[device] = []

            for child in children:
                if child in parents:
                    parents[child].append(device)
                else:
                    parents[child] = [device]

        return parents

    def root_devices(self) -> list:
        """Return all devices that have no parent."""
        return [
            device for device, parents in self.device_parents().items() if not parents
        ]

    def devices_with_multiple_parents(self) -> list:
        """Return all devices that have more than 1 parent."""
        return [
            device
            for device, parents in self.device_parents().items()
            if len(parents) > 1
        ]

    def devices_with_unknown_children(self) -> dict:
        """Return all devices that have children not present in the specification."""
        device_children = self.device_children()
        missing_children = {
            device: [child for child in children if child not in device_children]
            for device, children in device_children.items()
        }

        # only return devices that actually have missing children
        return {
            device: children
            for device, children in missing_children.items()
            if children
        }

    def loops(self) -> list:
        """Find loops in the hierarchy."""

        # Detect from any point, as some loops may cause all devices to have a parent
        return self._loops(self.device_children(), self.device_children().keys(), [])

    def _loops(self, device_children, root_devices, path_so_far=None) -> list:
        loops = []

        if not path_so_far:
            path_so_far = []

        for root in root_devices:
            for child in device_children.get(root, []):
                if child in path_so_far:
                    loops.append(path_so_far)
                else:
                    loops.extend(
                        self._loops(device_children, [child], path_so_far + [root])
                    )

        return loops


def main(**kwargs):
    import argparse
    import pprint
    import sys

    parser = argparse.ArgumentParser(
        description="Analyse and print the device hierarchy based on CDB JSON files fed to dsconfig. Provide any number of JSON files in the order they will be applied."
    )
    parser.add_argument(
        "-H",
        "--hierarchy",
        type=str,
        required=False,
        default="Power",
        choices=["Power", "Control"],
        help="hierarchy to parse",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        default=False,
        help="be quiet (don't print headers)",
    )
    parser.add_argument(
        "-l", "--loops", action="store_true", default=False, help="print detected loops"
    )
    parser.add_argument(
        "-m",
        "--multiple_parents",
        action="store_true",
        default=False,
        help="print devices with multiple parents",
    )
    parser.add_argument(
        "-c",
        "--missing_children",
        action="store_true",
        default=False,
        help="print devices with children that do not exist",
    )
    parser.add_argument(
        "-a",
        "--all_problems",
        action="store_true",
        default=False,
        help="print all problems (-l -m -c)",
    )
    parser.add_argument(
        "-r",
        "--roots",
        action="store_true",
        default=False,
        help="print list of root devices",
    )
    parser.add_argument(
        "-t", "--tree", action="store_true", default=False, help="print device tree"
    )
    parser.add_argument("json", nargs="+")

    args = parser.parse_args()

    found_issues = False

    # load and merge dicts
    analysis = DsConfigAnalysis(f"{args.hierarchy}_Children")
    for fname in args.json:
        if not args.quiet:
            print(f"Reading and parsing {fname}...")
        analysis.load_file(fname)

    if not args.quiet:
        print(f"Analysing hierarchy {args.hierarchy}...")

    if args.tree:
        if not args.quiet:
            print("---------- Device trees below root devices ----------")

        def print_tree(device_children, root, path_so_far=None):
            if not path_so_far:
                path_so_far = []

            indent = len(path_so_far) * 2

            print(" " * indent, "-", root)

            for child in device_children.get(root, []):
                if child in path_so_far:
                    print(" " * (indent + 2), "-", child, " (LOOP!)")
                else:
                    print_tree(device_children, child, path_so_far + [root])

        for root in analysis.root_devices():
            print_tree(analysis.device_children(), root)

    if args.multiple_parents or args.all_problems:
        if not args.quiet:
            print("---------- Issue if >0: Devices with multiple parents ----------")
        multiple_parents = analysis.devices_with_multiple_parents()
        pprint.pprint(multiple_parents)

        if multiple_parents:
            found_issues = True

    if args.loops or args.all_problems:
        if not args.quiet:
            print("---------- Issue if >0: Loops ----------")
        loops = analysis.loops()
        pprint.pprint(loops)

        if loops:
            found_issues = True

    if args.missing_children or args.all_problems:
        if not args.quiet:
            print(
                "---------- Issue if >0: Devices with non-existing children ----------"
            )
        unknown_children = analysis.devices_with_unknown_children()
        pprint.pprint(unknown_children)

        if unknown_children:
            found_issues = True

    if args.roots or args.all_problems:
        if not args.quiet:
            print("---------- Issue if >1: Root devices (without parents) ----------")
        root_devices = analysis.root_devices()
        pprint.pprint(root_devices)

        if len(root_devices) > 1:
            found_issues = True

    sys.exit(1 if found_issues else 0)


if __name__ == "__main__":
    main()
