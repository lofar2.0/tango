#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging
import sys
import time

from prometheus_client import Info, Gauge
from tango import Util
from tango.server import run

from tangostationcontrol import __version__ as version
from tangostationcontrol import devices  # noqa: F401
from tangostationcontrol.common.lofar_logging import configure_logger
from tangostationcontrol.metrics import start_metrics_server

logger = logging.getLogger()

tango_ds_info = Info(
    "tango_ds",
    "Info about the Tango device server",
    ["domain", "family", "member", "device_class"],
)

uptime_ds = Gauge(
    "tango_ds_uptime",
    "Uptime of the Tango device server",
    ["domain", "family", "member", "device_class"],
)


def post_init_callback():
    """This callback runs after the device server was initialised. It sets the
    station control version as the server version, as well as initialises common
    prometheus metrics about the device server."""
    util = Util.instance()
    util.set_server_version(version)
    tango_version = util.get_tango_lib_release()
    start_time = time.time()

    labels = dict(
        domain="dserver",
        family=util.get_ds_exec_name(),
        member=util.get_ds_inst_name(),
        device_class="DServer",
    )

    tango_ds_info.labels(**labels).info(
        dict(
            idl_version=util.get_version_str(),
            server_version=util.get_server_version(),
            lib_version=".".join(
                [
                    str(x)
                    for x in [
                        tango_version // 100,
                        tango_version % 100 // 10,
                        tango_version % 10,
                    ]
                ]
            ),
        )
    )

    uptime_ds.labels(**labels).set_function(lambda: time.time() - start_time)


# ----------
# Run server
# ----------
def main(**kwargs):
    device_class_str = sys.argv[1]
    device_class = getattr(sys.modules["tangostationcontrol.devices"], device_class_str)

    # Remove first argument which is filename
    args = sys.argv[1:]

    # Setup logging
    configure_logger()

    # Setup metrics server (Prometheus endpoint)
    start_metrics_server()

    if device_class_str == "ObservationControl":
        classes = (device_class, devices.ObservationField)
    else:
        classes = (device_class,)

    # Start the device server
    run(classes, args=args, post_init_callback=post_init_callback, **kwargs)
