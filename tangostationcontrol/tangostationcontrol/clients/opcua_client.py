# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
import socket
import textwrap
from typing import Dict, List, Optional

import asyncua
from datetime import datetime
import numpy
from asyncua import Client

from tangostationcontrol.clients.comms_client import AsyncCommClient
from tangostationcontrol.common.lofar_logging import exception_to_str

logger = logging.getLogger()

__all__ = [
    "OPCUAConnection",
    "ProtocolAttribute",
    "OPCUA_connection_tester",
    "OPCUAConnectionStatus",
]

numpy_to_OPCua_dict = {
    numpy.int8: asyncua.ua.VariantType.SByte,
    numpy.uint8: asyncua.ua.VariantType.Byte,
    numpy.int16: asyncua.ua.VariantType.Int16,
    numpy.uint16: asyncua.ua.VariantType.UInt16,
    numpy.int32: asyncua.ua.VariantType.Int32,
    numpy.uint32: asyncua.ua.VariantType.UInt32,
    numpy.int64: asyncua.ua.VariantType.Int64,
    numpy.uint64: asyncua.ua.VariantType.UInt64,
    numpy.float32: asyncua.ua.VariantType.Float,
    numpy.double: asyncua.ua.VariantType.Double,
    numpy.float64: asyncua.ua.VariantType.Double,
    bool: asyncua.ua.VariantType.Boolean,
    str: asyncua.ua.VariantType.String,
}


class OPCUAConnectionStatus:
    def __init__(self):
        self.last_disconnect_exception = None
        self.last_disconnect_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        self.connected = False

    def connection_lost(self):
        raise NotImplementedError


def numpy_to_opcua(numpy_val):
    """Convert a numpy type to a corresponding opcua Variant type."""

    numpy_type = type(numpy_val)

    assert numpy_type not in [list, numpy.array], "Converting arrays not yet supported."

    try:
        ua_type = numpy_to_OPCua_dict[numpy_type]
    except KeyError as e:
        raise TypeError(
            f"Could not convert {numpy_val} (type {type(numpy_val).__name__}) to an OPC UA type."
        ) from e

    return asyncua.ua.uatypes.Variant(Value=numpy_val, VariantType=ua_type)


class OPCUAConnection(AsyncCommClient):
    """
    Connects to OPC-UA in the foreground or background, and sends HELLO
    messages to keep a check on the connection. On connection failure, reconnects once.
    """

    def __init__(
        self,
        address,
        namespace,
        timeout,
        connection_status,
        event_loop=None,
        device=None,
    ):
        """
        Create the OPC ua client and connect() to it and get the object node
        """

        self.address = address
        self.timeout = int(timeout)
        self.namespace = namespace

        self.prot_attrs = {}

        self._clear_client()

        self.io_lock = asyncio.Lock()
        self.device = device

        # prefix path to all nodes with this. this allows the user to switch trees more easily.
        self.node_path_prefix = []

        self.connection_status = connection_status
        self.connection_status.connected = False

        super().__init__(event_loop)

    def _clear_client(self):
        """(Re)initialise members relating to the OPC/UA connection."""

        self.client = None
        self.obj = None

        # cache of looked up child node lists for each comma-separated parent path
        self._node_cache = {}

    def _servername(self):
        return self.client.server_url.geturl()

    async def connect(self):
        """
        Try to connect to the client
        """

        self.client = Client(self.address, int(self.timeout))

        logger.debug(f"Connecting to server {self._servername()}")

        try:
            async with self.io_lock:
                await self.client.connect()
        except (socket.error, IOError, OSError) as e:
            logger.warning(
                f"Could not connect to OPC-UA server {self._servername()}: {exception_to_str(e)}",
            )
            raise IOError(
                f"Could not connect to OPC-UA server {self._servername()}"
            ) from e

        logger.info(f"Connected to OPC-UA server {self._servername()}")

        # determine namespace used
        if type(self.namespace) is str:
            self.name_space_index = await self.client.get_namespace_index(
                self.namespace
            )
        elif type(self.namespace) is int:
            self.name_space_index = self.namespace
        else:
            raise TypeError(
                f"namespace must be of type str or int, but is of type {type(self.namespace).__name__}"
            )

        self.obj = self.client.get_objects_node()

        # updates the protocol attributes after a reconnect
        await self.update_protocol_attributes()

        # connect done
        self.connection_status.connected = True

    async def disconnect(self, reason: Optional[Exception]):
        """
        disconnect from the client
        """
        logger.info(f"Disconnecting to OPC-UA server {self._servername()}")

        self.connection_status.connected = False

        if reason:
            self.connection_status.last_disconnect_exception = reason
            self.connection_status.last_disconnect_time = datetime.now().strftime(
                "%Y-%m-%d %H:%M:%S"
            )
            self.connection_status.connection_lost()

        try:
            async with self.io_lock:
                await self.client.disconnect()
        finally:
            self._clear_client()

    async def ping(self):
        """
        ping the client to make sure the connection with the client is still functional.
        """
        try:
            async with self.io_lock:
                # do a cheap call. NOTE: send_hello is not allowed after establishing a connection,
                # so cannot be used here. see https://reference.opcfoundation.org/v104/Core/docs/Part6/7.1.3/
                _ = await self.client.get_namespace_array()
        except Exception as e:
            raise IOError(f"Lost connection to server {self._servername()}: {e}") from e

    def get_full_node_path(self, path: List[str]):
        """
        Return the path of a node as it will be looked up on the server.
        """

        # add path prefix
        path = self.node_path_prefix + path

        # prepend namespace index for each element if none is given
        path = [
            name if ":" in name else f"{self.name_space_index}:{name}" for name in path
        ]

        return path

    async def get_node(self, path: List[str]):
        """Retrieve an OPC-UA node from either the cache, or the server."""

        if not path:
            return self.obj

        cache_key = ",".join(path)

        # lookup in cache
        if cache_key in self._node_cache:
            return self._node_cache[cache_key]

        # cache it and all of its siblings to save us the round trips for them later on.
        async with self.io_lock:
            parent_path = path[:-1]
            parent_node = (
                await self.obj.get_child(parent_path) if parent_path else self.obj
            )
            child_nodes = await parent_node.get_children_descriptions()

        for child_node in child_nodes:
            # add node to the cache
            child_path = parent_path + [
                f"{self.name_space_index}:{child_node.DisplayName.Text}"
            ]
            self._node_cache[",".join(child_path)] = self.client.get_node(
                child_node.NodeId
            )

        # lookup in cache again. if the name is valid, it should be in there.
        if cache_key in self._node_cache:
            return self._node_cache[cache_key]

        # we couldnt find the requested child, ask server directly to get the appropriate error
        async with self.io_lock:
            return await self.obj.get_child(path)

    async def update_protocol_attributes(self):
        """
        After a reconnect all nodes need to be fetched again, as these are session bound
        This function goes through all the protocol attributes and updates
        their stored nodes.
        """

        for path, prot_attr in self.prot_attrs.items():
            try:
                node = await self.get_node(path.split(","))
            except Exception as e:
                logger.error(
                    f"Could not get node: {path} on server {self._servername()}: {exception_to_str(e)}"
                )
                raise Exception(
                    f"Could not get node: {path} on server {self._servername()}"
                ) from e

            await prot_attr.update_node(node)

    @staticmethod
    def _fix_annotation(annotation) -> Dict[str, object]:
        """Turn shortened/old annotations into a dict."""
        if isinstance(annotation, dict):
            return annotation

        return {"path": annotation}

    async def setup_protocol_attribute(self, annotation, attribute):
        # process the annotation
        annotation = self._fix_annotation(annotation)
        path = self.get_full_node_path(annotation["path"])

        prot_attr = await self._protocol_attribute(
            path,
            attribute.dim_x,
            attribute.dim_y,
            attribute.datatype,
            log_writes=annotation.get("log_writes", True),
        )

        # cache the protocol attributes in case we need to update them after a reconnect
        self.prot_attrs[",".join(path)] = prot_attr

        return prot_attr

    async def _protocol_attribute(
        self, path, dim_x, dim_y, datatype, log_writes: bool = True
    ):

        try:
            node = await self.get_node(path)
        except Exception as e:
            logger.error(
                f"Could not get node: {path} on server {self._servername()}: {exception_to_str(e)}"
            )
            raise Exception(
                f"Could not get node: {path} on server {self._servername()}"
            ) from e

        # get all the necessary data to set up the read/write functions from the AttributeWrapper
        ua_type = numpy_to_OPCua_dict[
            datatype
        ]  # convert the numpy type to a corresponding UA type

        # configure and return the read/write functions
        prot_attr = ProtocolAttribute(
            node,
            self.io_lock,
            dim_x,
            dim_y,
            ua_type,
            name=",".join(path),
            log_writes=log_writes,
        )

        try:
            # NOTE: debug statement tries to get the qualified name, this may not always work. in that case forgo the name and just print the path
            node_name = str(node.get_browse_name())[len("QualifiedName(2:") :]
            logger.debug(
                "connected OPC ua node {} of type {} to attribute with dimensions: {} x {} ".format(
                    str(node_name)[: len(node_name) - 1],
                    str(ua_type)[len("VariantType.") :],
                    dim_x,
                    dim_y,
                )
            )
        except Exception:
            "B001 Do not use bare `except:`, it also catches unexpected events"
            "like memory errors, interrupts, system exit"
            pass

        return prot_attr

    async def setup_attribute(self, annotation, attribute):
        prot_attr = await self.setup_protocol_attribute(annotation, attribute)

        # Tango will call these from a separate polling thread.
        def read_function():
            try:
                if not self.connection_status.connected:
                    raise IOError(
                        f"OPC-UA client is not connected to {self._servername()}"
                    )

                return asyncio.run_coroutine_threadsafe(
                    prot_attr.read_function(), self.event_loop
                ).result()
            except Exception as e:
                logger.error(
                    f"Failed to read attribute {prot_attr.name}: {exception_to_str(e)}"
                )

                raise

        def write_function(value):
            try:
                if not self.connection_status.connected:
                    raise IOError(
                        f"OPC-UA client is not connected to {self._servername()}"
                    )

                asyncio.run_coroutine_threadsafe(
                    prot_attr.write_function(value), self.event_loop
                ).result()
            except Exception as e:
                logger.error(
                    f"Failed to write attribute {prot_attr.name}: {exception_to_str(e)}"
                )

                raise

        # return the read/write functions
        return read_function, write_function

    async def _call_method(self, method_path: List[str], *args):
        method_path = self.get_full_node_path(method_path)

        # convert the arguments to OPC UA types
        args = [numpy_to_opcua(arg) for arg in args]

        try:
            # call method in its parent node
            node = await self.get_node(method_path[:-1])
            result = await node.call_method(method_path[-1], *args)
        except Exception as e:
            logger.error(f"Failed to call method {method_path}: {exception_to_str(e)}")

            raise

        return result

    def call_method(self, method_path, *args):
        return asyncio.run_coroutine_threadsafe(
            self._call_method(method_path, *args), self.event_loop
        ).result()


class ProtocolAttribute:
    """
    This class provides a small wrapper for the OPC ua read/write functions in order to better organise the code
    """

    def __init__(
        self,
        node,
        io_lock,
        dim_x,
        dim_y,
        ua_type,
        name: str = "",
        log_writes: bool = True,
    ):
        self.node = node
        self.io_lock = io_lock
        self.dim_y = dim_y
        self.dim_x = dim_x
        self.ua_type = ua_type
        self.name = name
        self.log_writes = log_writes

    async def update_node(self, node):
        logger.debug(f"node update: {node}")
        self.node = node

    async def read_function(self):
        """
        Read_R function
        """

        async with self.io_lock:
            value = await self.node.get_value()

        try:
            # Pytango strings are Latin-1, and will crash on receiving Unicode strings
            # (see https://gitlab.com/tango-controls/pytango/-/issues/72)
            # So we explicitly convert to Latin-1 and replace errors with '?'.
            #
            # Note that PyTango also accepts byte arrays as strings, so we don't
            # have to actually decode() the result.

            def fix_string(s):
                return s.encode("latin-1", errors="replace").decode("latin-1")

            if type(value) == list and len(value) > 0 and type(value[0]) == str:
                value = [fix_string(v) for v in value]
            elif type(value) == str:
                value = fix_string(value)

            if self.dim_y + self.dim_x == 1:
                # scalar
                return value
            elif self.dim_y != 0:
                # 2D array
                value = numpy.array(
                    numpy.split(numpy.array(value), indices_or_sections=self.dim_y)
                )
            else:
                # 1D array
                value = numpy.array(value)

            return value
        except Exception as e:
            # Log "value" that gave us this issue
            raise ValueError(
                f"Failed to parse atribute value retrieved from OPC-UA: {value}"
            ) from e

    async def write_function(self, value):
        """
        write_RW function
        """

        if self.dim_y != 0:
            # flatten array, convert to python array
            value = numpy.concatenate(value).tolist()
        elif self.dim_x != 1:
            # make sure it is a python array
            value = value.tolist() if type(value) == numpy.ndarray else value

        if self.log_writes:
            logger.info(
                f"OPC-UA write: {self.name} := {textwrap.shorten(str(value), 150)}"
            )

        async with self.io_lock:
            try:
                await self.node.set_data_value(
                    asyncua.ua.uatypes.Variant(Value=value, VariantType=self.ua_type)
                )
            except (TypeError, asyncua.ua.uaerrors.BadTypeMismatch) as e:
                # A type conversion went wrong or there is a type mismatch.
                #
                # This is either the conversion us -> opcua in our client, or client -> server.
                # Report all types involved to allow assessment of the location of the error.
                if type(value) == list:
                    our_type = "list({dtype}) x ({dimensions})".format(
                        dtype=(type(value[0]).__name__ if value else ""),
                        dimensions=len(value),
                    )
                else:
                    our_type = "{dtype}".format(dtype=type(value))

                is_scalar = (self.dim_x + self.dim_y) == 1

                if is_scalar:
                    expected_server_type = "{dtype} (scalar)".format(dtype=self.ua_type)
                else:
                    expected_server_type = "{dtype} x ({dim_x}, {dim_y})".format(
                        dtype=self.ua_type, dim_x=self.dim_x, dim_y=self.dim_y
                    )

                actual_server_type = "{dtype} x {dimensions}".format(
                    dtype=await self.node.read_data_type_as_variant_type(),
                    dimensions=(await self.node.read_array_dimensions())
                    or "(dimensions unknown)",
                )

                attribute_name = (await self.node.read_display_name()).to_string()

                raise TypeError(
                    f"Cannot write value to OPC-UA attribute '{attribute_name}': "
                    f"tried to convert data type {our_type} to expected server type "
                    f"{expected_server_type}, server reports type {actual_server_type}"
                ) from e


async def OPCUA_connection_tester(server, port):
    """
    This function exist in order to test whether an OPC ua server can be reached
    """

    try:
        client = Client(f"opc.tcp://{server}:{port}/", 2)
        await client.connect()
        await client.disconnect()
        return True
    except Exception as e:
        logger.debug("Testing OPCua connection failed", exc_info=True)
        return False
