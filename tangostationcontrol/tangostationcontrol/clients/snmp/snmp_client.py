# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from os import path

from pysnmp import hlapi
from pysnmp.smi import builder, compiler
from tangostationcontrol.clients.comms_client import CommClient

logger = logging.getLogger()

__all__ = ["SNMPClient"]


class SNMPComm:
    """
    Holds information for communicating with the SNMP server
    """

    def __init__(self, community, host, version, port=161):
        if version not in [1, 2, 3]:
            raise ValueError("SNMP version can only be 1, 2 or 3")

        # pysnmp for whatever reason starts counting versions at 0.
        pysnmp_snmp_version = version - 1

        self.port = port
        self.engine = hlapi.SnmpEngine()
        self.community = hlapi.CommunityData(community, mpModel=pysnmp_snmp_version)
        self.transport = hlapi.UdpTransportTarget((host, port))

        # context data sets the version used. Default SNMPv2
        self.ctx_data = hlapi.ContextData()

    def getter(self, objs):
        return next(
            hlapi.getCmd(
                self.engine, self.community, self.transport, self.ctx_data, *objs
            )
        )

    def setter(self, objs):
        return next(
            hlapi.setCmd(
                self.engine, self.community, self.transport, self.ctx_data, *objs
            )
        )


class SNMPClient(CommClient):
    """
    messages to keep a check on the connection. On connection failure, reconnects once.
    """

    def start(self):
        super().start()

    def __init__(
        self,
        community,
        host,
        timeout,
        version,
        attribute_class,
        fault_func,
        try_interval=2,
        port=161,
    ):
        """
        Create the SNMP engine
        """
        super().__init__(fault_func, try_interval)

        logger.debug(
            f"setting up SNMP engine with host: {host} and community: {community}"
        )
        self.SNMP_comm = SNMPComm(community, host, version, port)

        self.SNMP_attribute_class = attribute_class

        # only sets up the engine, doesn't connect
        self.connected = True

    def _process_annotation(self, annotation):
        try:
            mib = annotation["mib"]
            name = annotation["name"]

            # SNMP has tables that require an index number to access them. regular non-table variable have an index of 0
            idx = annotation.get("index", 0)

            # SNMP values like to use weird units like tenths of amps because its all integers. We added a scaling factor to correct for that.
            scaling_factor = annotation.get("scaling_factor", 1)

            return mib, name, idx, scaling_factor
        except KeyError:
            raise ValueError(
                f"SNMP attribute annotation requires a dict argument with both a 'name' and 'mib' key. Instead got: {annotation}"
            )

    def setup_value_conversion(self, attribute):
        """
        gives the client access to the AttributeWrapper object in order to access all data it could potentially need.
        """

        dim_x = attribute.dim_x
        dim_y = attribute.dim_y
        dtype = attribute.datatype

        return dim_x, dim_y, dtype

    def setup_attribute(self, annotation, attribute):
        """
        MANDATORY function: is used by the attribute wrapper to get read/write functions. must return the read and write functions

        Gets called from inside the attribute wrapper. It is provided with the attribute_warpper itself
        and the annotation provided when the AttributeWrapper was declared.
        These parameters can be used to configure a valid read and write function as return values.
        """

        # process the annotation
        mib, name, idx, scaling_factor = self._process_annotation(annotation)

        # get all the necessary data to set up the read/write functions from the AttributeWrapper
        dim_x, dim_y, dtype = self.setup_value_conversion(attribute)
        snmp_attr = self.SNMP_attribute_class(
            self.SNMP_comm, mib, name, idx, dtype, dim_x, dim_y, scaling_factor
        )

        # return the read/write functions
        def read_function():
            return snmp_attr.read_function()

        def write_function(value):
            snmp_attr.write_function(value)

        return read_function, write_function


class MIBLoader:
    def __init__(self, mib_dir: str):
        self.mibBuilder = builder.MibBuilder()
        logger.debug(mib_dir)

        if not path.isabs(mib_dir):
            mib_dir = "/" + mib_dir

        compiler.addMibCompiler(
            self.mibBuilder,
            sources=[
                f"file://{mib_dir}",
            ],
        )
        logger.debug(f"mib sources: {self.mibBuilder.getMibSources()}")

    def load_pymib(self, mib_name):
        self.mibBuilder.loadModules(mib_name)


def test_SNMP_connection(community, host, port=161):
    """
    Attempts to read a single SNMP point with the given parameters
    sysUpTime is chosen because it is pretty much Ubiquitous point.
    """

    iterator = hlapi.getCmd(
        hlapi.SnmpEngine(),
        hlapi.CommunityData(community, mpModel=1),
        hlapi.UdpTransportTarget((host, port), timeout=2, retries=1),
        hlapi.ContextData(),
        hlapi.ObjectType(hlapi.ObjectIdentity("SNMPv2-MIB", "sysUpTime", 0)),
    )

    errorIndication, errorStatus, errorIndex, _ = next(iterator)

    if errorIndication or errorStatus or errorIndex:
        # if not (None, 0, 0) there has been some error
        logger.debug(
            f"Connection test failed. errorIndication: {errorIndication}, errorStatus: {errorStatus}, errorIndex: {errorIndex}"
        )
        return False
    else:
        return True
