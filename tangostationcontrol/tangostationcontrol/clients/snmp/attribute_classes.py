import logging
import numpy
from pysnmp import hlapi

logger = logging.getLogger()


class SNMPAttribute:
    """Define a SNMP attribute with its features"""

    snmp_to_numpy_dict = {
        hlapi.Integer32: numpy.int64,
        hlapi.Integer: numpy.int64,
        hlapi.TimeTicks: numpy.int64,
        hlapi.OctetString: str,
        hlapi.ObjectIdentity: str,
        hlapi.Counter32: numpy.int64,
        hlapi.Gauge32: numpy.int64,
        hlapi.IpAddress: str,
    }

    def __init__(self, comm, mib, name, idx, dtype, dim_x, dim_y, scaling_factor=1):
        self.comm = comm
        self.mib = mib
        self.name = name
        self.idx = idx
        self.dtype = dtype
        self.scaling_factor = scaling_factor

        self.len = self.get_len(dim_x, dim_y)
        self.is_scalar = self.len == 1

        self.objID = tuple(
            hlapi.ObjectIdentity(self.mib, self.name, self.idx + i)
            for i in range(self.len)
        )
        self.objs = tuple(
            (hlapi.ObjectType(i) for i in self.objID),
        )

    def get_len(self, dim_x, dim_y):
        """ ""Small helper function to not clutter the __init__"""

        if dim_x == 0:
            dim_x = 1
        if dim_y == 0:
            dim_y = 1
        return dim_x * dim_y

    def read_function(self):
        """
        Read function we give to the attribute wrapper
        """

        # get all of the values
        error_indication, errorStatus, errorIndex, *var_binds = self.comm.getter(
            self.objs
        )

        if error_indication is not None:
            raise IOError(
                f"An error occurred while attempting to write '{self.name}'. \
                    errorIndication: {error_indication}"
            )

        # get all the values in a list converted to the correct type
        val_lst = self.convert(var_binds)

        # return the list of values
        return val_lst

    def write_function(self, value):
        """
        Write function we give to the attribute wrapper
        """

        if self.is_scalar and type(value) != list:
            value = [value]

        write_obj = tuple(
            hlapi.ObjectType(self.objID[i], value[i]) for i in range(len(self.objID))
        )

        error_indication, errorStatus, errorIndex, *var_binds = self.comm.setter(
            write_obj
        )

        if error_indication is not None:
            raise IOError(
                f"An error occurred while attempting to write '{self.name}'. \
                    errorIndication: {error_indication}"
            )

    def convert(self, var_binds):
        """
        Get all the values in a list, make sure to convert specific types
        that dont want to play nicely
        """
        values = []

        var_binds = var_binds[0]

        for var_bind in var_binds:
            value = self._convert_var_bind_value(var_bind[1])
            values.append(value)

        if self.is_scalar:
            values = values[0]

        return values

    def _convert_var_bind_value(self, value):
        def is_an_hlapi_integer(value):
            return isinstance(value, (hlapi.Integer32, hlapi.Integer))

        def is_an_hlapi_number_type(value):
            return isinstance(
                value,
                (
                    hlapi.TimeTicks,
                    hlapi.Counter32,
                    hlapi.Gauge32,
                    hlapi.Integer32,
                    hlapi.Integer,
                ),
            )

        def is_an_hlapi_string_type(value):
            return isinstance(value, (hlapi.OctetString, hlapi.ObjectIdentity))

        def needs_conversion_from_integer_to_str(value):
            return is_an_hlapi_integer(value) and self.dtype == str

        def needs_conversion_from_ipaddress_to_str(value):
            return isinstance(value, hlapi.IpAddress) and self.dtype == str

        def needs_conversion_from_number_to_int64(value):
            return is_an_hlapi_number_type(value) and self.dtype == numpy.int64

        def needs_conversion_from_string_to_str(value):
            return is_an_hlapi_string_type(value) and self.dtype == str

        def convert_integer_to_str(value):
            if value.namedValues:
                result = value.prettyPrint()
            else:
                result = numpy.int64(value)

            return result

        if needs_conversion_from_ipaddress_to_str(value):
            result = value.prettyPrint()
        elif needs_conversion_from_integer_to_str(value):
            result = convert_integer_to_str(value)
        elif needs_conversion_from_number_to_int64(value):
            result = numpy.int64(value) * self.scaling_factor
        elif needs_conversion_from_string_to_str(value):
            result = str(value)
        else:
            raise TypeError(
                f"Error: did not find a valid snmp type. Got: {type(value)}, \
                    expected one of: '{SNMPAttribute.snmp_to_numpy_dict.keys()}'"
            )

        return result


class PCON_sim:
    """
    This class mimics the desired behavior of the PCON for the points we
    are interested in. It is a replacement for the SNMPAttribute attribute class
    """

    PCON_SCALAR_ATT = "powerSystemStatus"
    PCON_STRING_ATT = "powerSystemCompany"

    def __init__(
        self,
        comm=None,
        mib=None,
        name=None,
        idx=None,
        dtype=None,
        dim_x=None,
        dim_y=None,
        scaling_factor=1,
    ):
        if None in (name, idx, dim_x, dim_y):
            raise Exception("Expected a value for argument name, idx dim_x and dim_y")

        self.name = name

        if dim_x == 0:
            dim_x = 1
        if dim_y == 0:
            dim_y = 1

        self.len = dim_y * dim_x
        self.idx = idx

    def write_function(self, value):
        """Simulated attribute write function"""
        raise NameError("PCOn has no write attributes")

    def read_function(self):
        """Simulated attribute read function"""
        match self.name:
            case self.PCON_SCALAR_ATT:
                return 1
            case self.PCON_STRING_ATT:
                return "Eltek"
