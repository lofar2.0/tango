# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
import time
from abc import ABC, abstractmethod
from typing import Optional

from tangostationcontrol.common.threading import OmniThread
from tangostationcontrol.asyncio import EventLoopThread

logger = logging.getLogger()


class AbstractCommClient(ABC):
    @abstractmethod
    def start(self):
        """Start communication with the client."""

    @abstractmethod
    def stop(self):
        """Stop communication with the client."""

    def ping(self):  # noqa: B027
        """Check whether the connection is still alive.

        Clients that override this method must raise an Exception if the
        connection died."""
        pass

    @abstractmethod
    def setup_attribute(self, annotation, attribute):
        """
        This function returns a (read_function, write_function) tuple for the provided attribute with the provided annotation.

        The setup-attribute has access to the comms_annotation provided to the attribute wrapper to pass along to the comms client
        as well as a reference to the attribute itself.

        The read_function must return a single value, representing the current value of the attribute.

        The write_function must take a single value, write it, and return None.

        Examples:
        - File system:  get_mapping returns functions that read/write a fixed
        number of bytes at a fixed location in a file. (SEEK)
        - OPC-UA:  traverse the OPC-UA tree until the node is found.
        Then return the read/write functions for that node which automatically
        convert values between Python and OPC-UA.
        """


class CommClient(AbstractCommClient, OmniThread):
    """
    Abstracts communication with a client, for instance, over the network, by handling connect(), disconnect(), and ping()
    primitives.
    """

    def __init__(self, fault_func, try_interval=2):
        """ """
        self.fault_func = fault_func
        self.try_interval = try_interval
        self.stopping = False
        self.connected = False

        super().__init__(daemon=True, target=self._run)

    def _connect(self):
        pass

    def _disconnect(self):
        pass

    def connect(self):
        """
        Function used to connect to the client.

        Throws an Exception if the connection cannot be established.
        """

        self._connect()
        self.connected = True

    def disconnect(self):
        """
        Function used to connect to the client.
        """

        self._disconnect()
        self.connected = False

    def _run(self):
        self.stopping = False
        while not self.stopping:
            if not self.connected:
                # we (re)try only once, to catch exotic network issues. if the infra or hardware is down,
                # our device cannot help, and must be reinitialised after the infra or hardware is fixed.
                try:
                    self.connect()
                except Exception as e:
                    logger.exception("Fault condition in communication detected.")
                    self.fault_func(
                        f"FAULT: Connection failed: {e.__class__.__name__}: {e}"
                    )
                    return

            # keep checking if the connection is still alive
            try:
                while not self.stopping:
                    self.ping()
                    time.sleep(self.try_interval)
            except Exception as e:
                logger.exception("Fault condition in communication detected.")

                # technically, we may not have dropped the connection, but encounter a different error. so explicitly disconnect.
                self.disconnect()

                # signal that we're disconnected
                self.fault_func(f"FAULT: Connection lost: {e.__class__.__name__}: {e}")

                # don't enter a spam-connect loop if faults immediately occur
                time.sleep(self.try_interval)

    def ping(self):
        """Check whether the connection is still alive.

        Clients that override this method must raise an Exception if the
        connection died."""
        pass

    def stop(self):
        """
        Stop connecting & disconnect. Can take a few seconds for the timeouts to hit.
        """

        if not self.ident:
            # have not yet been started, so nothing to do
            return

        self.stopping = True
        self.join()

        self.disconnect()


class AsyncCommClient(object):
    """
    Abstracts communication with a client, for instance, over the network, by handling connect(), disconnect(), and ping()
    primitives.

    asyncio version of the CommClient. Also does not reconnect if the connection is lost.
    """

    def __init__(self, event_loop=None, reconnect_retry_time=1):
        """
        Create an Asynchronous communication client.

        fault_func: Function to call to put the device to FAULT if an error is detected.
        event_loop: Aysncio event loop to use. If None, a new event loop is created and
                    run in a separate thread. Only share event loops if any of the functions
                    executed doesn't stall, as asyncio used a cooperative multitasking model.

                    If the executed functions can stall (for a bit), use a dedicated loop to avoid
                    interfering with other users of the event loop.

                    All coroutines need to be executed in this loop, which wil also be stored
                    as the `event_loop` member of this object.
        """
        self.reconnect_retry_time = reconnect_retry_time
        self.running = False

        if event_loop is None:
            # Run a dedicated event loop for communications
            #
            # All co-routines need to be called through this event loop,
            # for example using asyncio.run_coroutine_threadsafe(coroutine, event_loop).
            self.event_loop_thread = EventLoopThread(f"AsyncCommClient {self.name()}")
            self.event_loop = self.event_loop_thread.event_loop
        else:
            self.event_loop_thread = None
            self.event_loop = event_loop

    def name(self):
        """The name of this CommClient, for use in logs."""
        return self.__class__.__name__

    @abstractmethod
    async def connect(self):
        """
        Function used to connect to the client, and any
        post init.
        """

    @abstractmethod
    async def disconnect(self, reason: Optional[Exception]):
        """
        Function used to disconnect from the client.
        """

    async def reconnect(self):
        """
        Reconnect function that keeps attempting to reconnect at a set
        interval whenever the connection is considered lost.
        """

        while self.running:
            try:
                await self.connect()
                logger.debug("Reconnected!")
                return

            except Exception:
                await asyncio.sleep(self.reconnect_retry_time)

    async def watch_connection(self):
        """Notice when the connection goes down, and auto-reconnect."""

        try:
            logger.info(f"[AsyncCommClient {self.name()}] Start watching")

            while self.running:
                # ping will throw in case of connection issues
                try:
                    await self.ping()
                except (OSError, asyncio.TimeoutError) as e:
                    logger.warning(
                        f"[AsyncCommClient {self.name()}] Ping failed: connection considered lost -- reconnecting"
                    )

                    # reset the connection
                    try:
                        await self.disconnect(e)
                    except Exception as e:
                        logger.info(
                            f"[AsyncCommClient {self.name()}] Disconnect failed -- leaking resource"
                        )

                    await self.reconnect()

                # don't spin, sleep for a while
                await asyncio.sleep(2)
        except asyncio.CancelledError as e:
            pass
        except Exception as e:
            # log immediately, or the exception will only be printed once this task is awaited
            logger.exception(
                f"[AsyncCommClient {self.name()}] Exception raised while watching"
            )

            raise
        finally:
            logger.info(f"[AsyncCommClient {self.name()}] Stop watching")

    async def ping(self):
        return

    async def start(self):
        if self.running:
            # already running
            return

        await self.connect()
        self.running = True

        # watch connection
        self.watch_connection_task = asyncio.create_task(
            self.watch_connection(),
            name=f"AsyncCommClient.watch_connection for {self.name()}",
        )

    async def stop(self):
        if not self.running:
            # already stopped
            return

        self.running = False

        try:
            # cancel & reap watcher
            self.watch_connection_task.cancel()

            await self.watch_connection_task
        except asyncio.CancelledError as e:
            pass
        except Exception as e:
            logger.exception(
                f"[AsyncCommClient {self.name()}] Watcher thread raised exception"
            )

            # the task stopped either way, so no need to bother our caller with this

        await self.disconnect(None)

    def sync_stop(self):
        """Synchronous version of stop()."""

        future = asyncio.run_coroutine_threadsafe(self.stop(), self.event_loop)
        return future.result()
