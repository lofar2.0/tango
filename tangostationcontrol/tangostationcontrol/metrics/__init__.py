#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from ._decorators import device_metrics
from ._metrics import (
    device_labels,
    AttributeMetric,
    ScalarAttributeMetric,
    SpectrumAttributeMetric,
    ImageAttributeMetric,
)
from prometheus_client import start_http_server, disable_created_metrics

__all__ = [
    "device_labels",
    "device_metrics",
    "start_metrics_server",
    "AttributeMetric",
    "ScalarAttributeMetric",
    "SpectrumAttributeMetric",
    "ImageAttributeMetric",
]


def start_metrics_server(port: int = 8000):
    # configure
    disable_created_metrics()

    # start server
    start_http_server(port)
