#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import fnmatch
import logging
from typing import List

from tango import Attribute, DevState
from tango.server import Device, attribute
from prometheus_client import Enum
from prometheus_client.core import Info, Counter

from tangostationcontrol import __version__ as version
from tangostationcontrol.common.lofar_logging import exception_to_str

from ._metrics import (
    wrap_method,
    device_labels,
    AttributeMetric,
    ScalarAttributeMetric,
    SpectrumAttributeMetric,
    ImageAttributeMetric,
)

logger = logging.getLogger()


class VersionMetric(AttributeMetric):
    def __init__(self, device: Device):
        super().__init__(
            "software_version",
            "Software version of the device.",
            device_labels(device),
            Info,
        )

        self.set_value(dict(version=version))


class StateMetric(AttributeMetric):
    def __init__(self, device: Device):
        super().__init__(
            "state",
            "State of the device.",
            device.metric_labels,
            Enum,
            metric_class_init_kwargs={"states": list(DevState.names)},
        )

        self.set_value(device.get_state())

        wrap_method(device, device.set_state, self.set_value, post_execute=False)


class AccessCountMetric(AttributeMetric):
    def __init__(self, device: Device):
        super().__init__(
            "access_count",
            "How often this software device was accessed for commands or attributes",
            device.metric_labels,
            Counter,
        )

        self.inc(0)

        wrap_method(device, device.always_executed_hook, self.inc)

    def inc(self, amount: int = 1):
        self.get_metric().inc(amount)


class DeviceMetrics:
    """
    Injects code to provide prometheus metrics for devices annotated with device_metrics
    """

    def __init__(
        self, exclude: List[str] | None = None, include: List[str] | None = None
    ):
        self.exclude = exclude or []
        self.include = include or []

    @staticmethod
    def get_device_class_attributes(cls, device) -> List[Attribute]:
        return [
            getattr(device, name)
            for name, member in cls.__dict__.items()
            # Return only attributes defined in the given class.
            # PyTango likes to redefine them for each class.
            if name not in cls.__bases__[0].__dict__
            and isinstance(
                member,
                (
                    attribute,
                    Attribute,
                ),
            )
        ]

    def exclude_attr(self, name):
        # include overrides exclude
        for p in self.include:
            if fnmatch.fnmatch(name, p):
                yield False
                return

        for p in self.exclude:
            yield fnmatch.fnmatch(name, p)

    @staticmethod
    def make_attribute_metric(
        instance: Device, attribute: Attribute, wrap_read: bool
    ) -> AttributeMetric:
        """Create the right subclass of AttributeMetric for the given
        attribute."""
        if attribute.get_max_dim_y() > 0:
            return ImageAttributeMetric(instance, attribute, wrap_read)
        elif attribute.get_max_dim_x() > 1:
            return SpectrumAttributeMetric(instance, attribute, wrap_read)
        else:
            return ScalarAttributeMetric(instance, attribute, wrap_read)

    def expose_metrics_wrapper(self, cls):
        """Wrap a Device.init_device to add metrics for this class."""

        def post_init_device(instance: Device, *args, **kwargs):
            try:
                if not hasattr(instance, "metrics"):
                    instance.metric_labels = device_labels(instance)

                    # create metrics for non-attribute values
                    instance.metrics = {
                        "__state": StateMetric(instance),
                        "__access_count": AccessCountMetric(instance),
                        "__version": VersionMetric(instance),
                    }

                # create metrics for all attributes of the wrapped class, according to our filters
                for attr in self.get_device_class_attributes(cls, instance):
                    attr_name = attr.get_name()

                    if not any(self.exclude_attr(attr_name)):
                        logger.debug(
                            f"Metrics: publishing metrics for attribute {attr_name} of class {cls.__name__}"
                        )

                        # We do not hook into the read method, as that will not work
                        # for green attributes. Instead, we update the metric
                        # when polling the attribute.
                        instance.metrics[attr_name] = self.make_attribute_metric(
                            instance, attr, False
                        )
            except Exception as e:
                logger.exception(
                    f"Metrics: error publishing metrics of class {cls.__name__}: {exception_to_str(e)}"
                )

                # reraise, causing the device to fail to start
                raise

        return post_init_device

    def poll_attribute_wrapper(self, cls):
        """Wrap Device.init_device to poll attributes for this class."""

        def post_init_device(instance: Device, *args, **kwargs):
            for attr in self.get_device_class_attributes(cls, instance):
                attr_name = attr.get_name()

                if any(self.exclude_attr(attr_name)):
                    continue

                # register to be polled
                instance.attribute_poller.register(
                    attr_name, instance.metrics[attr_name]
                )
                logger.debug(f"Metrics: polling attribute {attr_name}")

        return post_init_device

    def __call__(self, cls):
        # we'll be doing very weird things if this class isn't
        if not issubclass(cls, Device):
            raise ValueError(
                "device_metrics decorator is to be used on Tango Device classes only."
            )

        logger.debug(f"Metrics: hooking into _init_device of class {cls.__name__}")

        # We hook into _init_device as that is called by both synchronous and
        # green devices with the same signature (synchronous).

        wrap_method(
            cls,
            cls._init_device,
            self.expose_metrics_wrapper(cls),
            double_wrap=True,
        )

        wrap_method(
            cls,
            cls._init_device,
            self.poll_attribute_wrapper(cls),
            double_wrap=True,
        )

        return cls


def device_metrics(*args, **kwargs):
    return DeviceMetrics(*args, **kwargs)
