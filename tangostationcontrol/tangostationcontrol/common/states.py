# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tango import DevState

# -----------------------
# Device states
# -----------------------

# The Device states in which we consider our device operational,
# and thus allow interaction.
OPERATIONAL_STATES = [DevState.ON, DevState.ALARM]

# States in which Initialise() has happened, and the hardware
# can thus be configured or otherwise interacted with.
INITIALISED_STATES = OPERATIONAL_STATES + [DevState.STANDBY, DevState.DISABLE]

# States in which most commands are allowed
DEFAULT_COMMAND_STATES = INITIALISED_STATES

# States in which the hardware can be powered on
POWER_ON_COMMAND_STATES = DEFAULT_COMMAND_STATES + [DevState.OFF, DevState.INIT]

# States in which the hardware can be powered off
POWER_OFF_COMMAND_STATES = OPERATIONAL_STATES + [DevState.STANDBY, DevState.DISABLE]
