# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import psutil
from ipaddress import IPv4Address
from socket import AddressFamily

__all__ = ["get_mac", "get_ip"]


def _get_interface_addresses(interface: str) -> list:
    try:
        return psutil.net_if_addrs()[interface]
    except KeyError as e:
        raise ValueError(f"Cannot find interface {interface}") from e


def get_mac(interface: str) -> str:
    """Returns the MAC address of the given interface (f.e. 'eth0')."""

    for address in _get_interface_addresses(interface):
        if address.family == AddressFamily.AF_PACKET:
            return address.address

    raise ValueError(f"Cannot obtain MAC address of interface {interface}")


def get_ip(interface: str) -> IPv4Address:
    """Returns the IPv4 address of the given interface (f.e. 'eth0')."""

    for address in _get_interface_addresses(interface):
        if address.family == AddressFamily.AF_INET:
            return IPv4Address(address.address)

    raise ValueError(f"Cannot obtain IP address of interface {interface}")
