# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass

from .constants import CLK_160_MHZ, CLK_200_MHZ
from .sdp import subband_frequency, are_subbands_decreasing


@dataclass(frozen=True)
class Band:
    """A frequency band supported by LOFAR."""

    name: str  # name of the band, for reverse lookups
    antenna_type: str  # LBA or HBA
    clock: int  # ADC sampling frequency, in Hz
    rcu_band: int  # RCU filter band setting (1, 2, 4)
    nyquist_zone: int  # Nyquist zone index (0, 1, 2)

    @staticmethod
    def lookup_rcu_band(antenna_type: str, clock: int, rcu_band: int) -> str:
        """Return the name of the frequency band matching the given criteria, or "" if no match is found."""

        def is_match(band):
            return (
                band.antenna_type == antenna_type
                and band.clock == clock
                and band.rcu_band == rcu_band
            )

        matching_bands = list(filter(is_match, bands.values()))

        if not matching_bands:
            raise ValueError(
                f"Could not find any frequency band matching antenna_type={antenna_type} clock={clock} rcu_band={rcu_band}."
            )

        # return first match
        return matching_bands[0].name

    @staticmethod
    def lookup_nyquist_zone(antenna_type: str, clock: int, nyquist_zone: int) -> str:
        """Return the name of the frequency band matching the given criteria, or "" if no match is found.

        If multiple bands match, (f.e. LBA_10_90 and LBA_30_90), any is returned."""

        def is_match(band):
            return (
                band.antenna_type == antenna_type
                and band.clock == clock
                and band.nyquist_zone == nyquist_zone
            )

        matching_bands = list(filter(is_match, bands.values()))

        if not matching_bands:
            raise ValueError(
                f"Could not find any frequency band matching antenna_type={antenna_type} clock={clock} nyquist_zone={nyquist_zone}."
            )

        # return first match
        return matching_bands[0].name

    @property
    def spectral_inversion(self) -> bool:
        """Whether the spectrum is inverted, and thus should be inverted back by SDP."""
        return are_subbands_decreasing(self.nyquist_zone, False)

    def subband_frequency(self, subband: int) -> float:
        """The central frequency of the given subband index, if SDP is configured accordingly."""
        return subband_frequency(
            subband, self.clock, self.nyquist_zone, self.spectral_inversion
        )


# Global list of all supported frequency bands
bands = {}
bands["LBA_10_90"] = Band(
    name="LBA_10_90", antenna_type="LBA", clock=CLK_200_MHZ, rcu_band=1, nyquist_zone=0
)
bands["LBA_10_70"] = Band(
    name="LBA_10_70", antenna_type="LBA", clock=CLK_160_MHZ, rcu_band=1, nyquist_zone=0
)
bands["LBA_30_90"] = Band(
    name="LBA_30_90", antenna_type="LBA", clock=CLK_200_MHZ, rcu_band=2, nyquist_zone=0
)
bands["LBA_30_70"] = Band(
    name="LBA_30_70", antenna_type="LBA", clock=CLK_160_MHZ, rcu_band=2, nyquist_zone=0
)
bands["HBA_110_190"] = Band(
    name="HBA_110_190",
    antenna_type="HBA",
    clock=CLK_200_MHZ,
    rcu_band=2,
    nyquist_zone=1,
)
bands["HBA_170_230"] = Band(
    name="HBA_170_230",
    antenna_type="HBA",
    clock=CLK_160_MHZ,
    rcu_band=1,
    nyquist_zone=2,
)
bands["HBA_210_250"] = Band(
    name="HBA_210_250",
    antenna_type="HBA",
    clock=CLK_200_MHZ,
    rcu_band=4,
    nyquist_zone=2,
)
