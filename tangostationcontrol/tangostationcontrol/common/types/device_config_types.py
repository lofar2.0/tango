# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Device proxy config type"""

from typing import List, Union

from tango import DeviceProxy

from lofar_station_client.common import CaseInsensitiveDict

# Dictionary of device class keys with attribute name list
device_config_type = CaseInsensitiveDict[str, List[str]]

# Dictionary of device names with proxies or exceptions about proxy creation
device_proxy_type = CaseInsensitiveDict[str, Union[DeviceProxy, Exception]]


def count_proxies_not_exceptions(proxies: device_proxy_type) -> int:
    """Count the number of proxies that did not generate an exception"""

    count = 0
    for proxy in proxies.values():
        if isinstance(proxy, DeviceProxy):
            count += 1
    return count
