# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Device proxy creation from config"""
import abc

import tango

from lofar_station_client.common import CaseInsensitiveDict

from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.types.device_config_types import (
    device_config_type,
    device_proxy_type,
)


class ICreateProxies(abc.ABC):
    @abc.abstractmethod
    def create_proxies(self) -> device_proxy_type:
        raise NotImplementedError("Interface method create_proxies not implemented")


def create_proxies(
    configuration: device_config_type,
    proxies: device_proxy_type,
    class_device_map: CaseInsensitiveDict = None,
) -> device_proxy_type:
    """Create proxies or store creation exception can be called anytime to retry

    The return of this function can be used to register event subscriptions as soon
    as the creation of a proxy becomes successful. Assuming the return of each call
    is used this will prevent missing subscriptions but also prevent duplicate
    subscriptions.

    Subsequent calls with more and more successful proxy creations will return
    increasingly smaller subset of device creations. The proxies argument is only
    modified for previously unsuccessful proxies.

    :return: A dictionary of newly created devices by name or creation exceptions
    """

    db = tango.Database()
    new_proxies = CaseInsensitiveDict()

    for device_class in configuration.keys():
        devices = db.get_device_exported_for_class(device_class)
        if class_device_map is not None:
            class_device_map[device_class] = devices
        for device in devices:
            if device in proxies and not isinstance(proxies[device], Exception):
                continue

            try:
                proxy = create_device_proxy(device)
                proxies[device] = proxy
                new_proxies[device] = proxy
            except Exception as e:
                proxies[device] = e
                new_proxies[device] = e

    return new_proxies
