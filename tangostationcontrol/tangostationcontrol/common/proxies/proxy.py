#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from typing import Optional

from tango import AccessControlType, DeviceProxy, DevSource


def create_device_proxy(
    dev_name: str, timeout_millis: Optional[int] = None, write_access: bool = False
) -> DeviceProxy:
    proxy = DeviceProxy(dev_name)

    # internal proxies should avoid the cache to avoid
    # race conditions.
    proxy.set_source(DevSource.DEV)

    if timeout_millis:
        proxy.set_timeout_millis(timeout_millis)
    else:
        # we cannot always access attributes, as the device might be down
        timeout_property = "Device_Proxy_Timeout"
        props = proxy.get_property(timeout_property)
        if len(props.get(timeout_property, [])) > 0:
            proxy.set_timeout_millis(int(props[timeout_property][0]))
        else:
            proxy.set_timeout_millis(60_000)

    if not write_access:
        proxy.set_access_control(AccessControlType.ACCESS_READ)

    return proxy
