# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.baselines import nr_baselines

# number of FPGA processing nodes
N_pn = 16

# number of polarisations per antenna (X and y polarisations)
N_pol = 2

# antennas per FPGA
A_pn = 6

# signal inputs per FPGA ( A_pn * N_pol )
S_pn = A_pn * N_pol

# number of FIR (Finite Impulse Response) filter taps
N_tap = 16

# number of FFT blocks
N_fft = 1024

# number of FPGA flash pages size
N_fps = 16384

# Highest number antennas we support
MAX_ANTENNA = N_pn * A_pn

# Maximum number of antenna inputs we support (used to determine array sizes)
MAX_INPUTS = N_pn * S_pn

# Number of tile elements (antenna dipoles) in each HBA tile
N_elements = 16

# number of RCU's per subrack
N_rcu = 32

# Number of antenna inputs per RCU
N_rcu_inp = 3

# number of hops that the data of the stream has traveled to reach the BSN aligner on this node
P_sum = 2

# the number of square correlator cells produced per FPGA for XST's (P_sq := N_pn // 2 + 1)
P_sq = 9

# total number of beamsets
N_beamsets_max = 2
# number of beamsets we control
N_beamsets_ctrl = 1
# The maximum amount of beamlets the SDP (and we) support
N_beamlets_max = 488
# number of actively controlled beamlets
N_beamlets_ctrl = 488
# maximum number of beamlet output destinations
N_bdo_destinations_mm = 32

# Maximum number of subbands we support
N_subbands = 512
# Number of points per subband (the resolution)
N_subband_res = 1024

# main clock frequency's are 200MHz and 160MHz
CLK_200_MHZ = 200_000_000
CLK_160_MHZ = 160_000_000

# Maximum number of subbands for which we collect XSTs simultaneously
MAX_PARALLEL_SUBBANDS = 7
# Expected block for XST's
BLOCK_LENGTH = 12
# Complex values are (real, imag).
VALUES_PER_COMPLEX = 2
# Max blocks for the BST statistics
# We support only one beamset for now, to avoid unnecessary explosion of the matrices
MAX_BEAMSETS = 1
# Expected number of blocks: enough to cover all baselines without the conjugates (that is, the top-left triangle of the matrix).
MAX_BLOCKS = nr_baselines(MAX_INPUTS // BLOCK_LENGTH)

# number of apspus supported by the translator
N_apspu = 2

# UNB2 constants
# number of uniboards in a subrack
N_unb = 2
# number of FPGA's in a uniboard
N_fpga = 4
# number of DDR modules per FPGA
N_ddr = 2
# number of QSFP tranceivers per uniboard
N_qsfp = 24

# the three spatial dimensions XYZ used a lot for PQR and ITRF coordinates.
N_xyz = 3
# amount of parameters needed for a pointing
N_point_prop = 3
# number of values for latitude/longitude coordinates
N_latlong = 2

# default subband we use because of its low RFI
DEFAULT_SUBBAND = 300

# Maximum array size to allocate for beam_device pointings,
MAX_POINTINGS = N_beamlets_max

# max size for a statistic packet
MAX_ETH_FRAME_SIZE = 9000

# The default polling period for polled attributes (in milliseconds)
DEFAULT_POLLING_PERIOD_MS = 1000

# The metadata polling period for polled attributes (in milliseconds)
DEFAULT_POLLING_PERIOD_METADATA_MS = 60000

# The default polling period for attributes polled to update metrics in Prometheus (in milliseconds)
DEFAULT_METRICS_POLLING_PERIOD_MS = 2500

# default numer tiles in a HBA for the non-international stations.
DEFAULT_N_HBA_TILES = 48

# number of tiles in the CS001 station
CS001_TILES = 24

# number of tiles in the RS307 station
RS307_TILES = 48

# The uint32 weight representing 1.0 for the fixed-point math in SDP's packed cint16 weights for subbands and beam forming.
SDP_UNIT_WEIGHT = 2**14

# Number of officially offered set of antennas
# Currently ["INNER", "OUTER", "SPARSE_EVEN", "SPARSE_ODD", "ALL"]
N_ANTENNA_SETS = 5
