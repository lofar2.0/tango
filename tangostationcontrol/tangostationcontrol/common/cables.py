# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass


@dataclass(frozen=True)
class CableType:
    """A cable used in LOFAR, with its properties."""

    name: str
    length: int
    delay: float
    loss: dict

    def speed(self):
        """Return the speed of the signal in this cable, in m/s."""

        return self.length / self.delay

    def get_loss(self, antenna_type: str, rcu_band_select: int) -> float:
        """Get the appropiate loss value (in dB), for the given
        antenna type and RCU band selection."""

        if antenna_type == "LBA":
            if rcu_band_select == 1:
                return self.loss[50]
            elif rcu_band_select == 2:
                return self.loss[50]
            else:
                raise ValueError(
                    f"Unsupported RCU band selection for LBA: {rcu_band_select}"
                )
        elif antenna_type == "HBA":
            if rcu_band_select == 1:
                return self.loss[200]
            elif rcu_band_select == 2:
                return self.loss[150]
            elif rcu_band_select == 4:
                return self.loss[250]
            else:
                raise ValueError(
                    f"Unsupported RCU band selection for HBA: {rcu_band_select}"
                )

        raise ValueError(f"Unsupported antenna type: {antenna_type}")


# Global list of all known cable types.
#
# NB: The LOFAR1 equivalents of these tables are:
#          - MAC/Deployment/data/StaticMetaData/CableDelays/
#          - MAC/Deployment/data/StaticMetaData/CableAttenuation.conf
cable_types = {}
cable_types["0m"] = CableType(
    name="0m",
    length=0,
    delay=000.0000e-9,
    loss={50: 0.00, 150: 0.00, 200: 0.00, 250: 0.00},
)
cable_types["50m"] = CableType(
    name="50m",
    length=50,
    delay=199.2573e-9,
    loss={50: 2.05, 150: 3.64, 200: 4.24, 250: 4.46},
)
cable_types["80m"] = CableType(
    name="80m",
    length=80,
    delay=326.9640e-9,
    loss={50: 3.32, 150: 5.87, 200: 6.82, 250: 7.19},
)
cable_types["85m"] = CableType(
    name="85m",
    length=85,
    delay=342.5133e-9,
    loss={50: 3.53, 150: 6.22, 200: 7.21, 250: 7.58},
)
cable_types["115m"] = CableType(
    name="115m",
    length=115,
    delay=465.5254e-9,
    loss={50: 4.74, 150: 8.35, 200: 9.70, 250: 10.18},
)
cable_types["120m"] = CableType(
    name="120m",
    length=120,
    delay=493.8617e-9,
    loss={50: 4.85, 150: 8.55, 200: 9.92, 250: 10.42},
)  # used on CS030
cable_types["130m"] = CableType(
    name="130m",
    length=130,
    delay=530.6981e-9,
    loss={50: 5.40, 150: 9.52, 200: 11.06, 250: 11.61},
)
