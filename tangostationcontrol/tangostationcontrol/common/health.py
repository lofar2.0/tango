# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
import sys

from tango import DeviceProxy


def main(*args, **kwargs):
    """Main function health check"""

    # Remove first argument as it is the filename
    args = sys.argv[1:]

    try:
        DeviceProxy(args[0]).ping()

        return 0
    except Exception as e:
        logging.getLogger().exception(e)
        return 1
