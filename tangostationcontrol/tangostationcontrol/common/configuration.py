#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import json
import logging
from itertools import islice

from jsonschema import Draft7Validator, FormatChecker, ValidationError
from tango import DeviceProxy, Database, DevFailed, DbDevInfo

from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.configuration import REGISTRY

logger = logging.getLogger()


class StationConfiguration:
    DEVICE_PROPERTIES_QUERY = "SELECT device, property_device.name, property_device.value \
                            FROM property_device \
                            INNER JOIN device ON property_device.device = device.name \
                            WHERE class != 'DServer' \
                            AND property_device.name != '__SubDevices' \
                            ORDER BY device, property_device.name, property_device.count ASC"

    ATTRS_PROPERTIES_QUERY = "SELECT device, attribute, property_attribute_device.name, \
                            property_attribute_device.value \
                            FROM property_attribute_device \
                            INNER JOIN device ON property_attribute_device.device = device.name \
                            WHERE class != 'DServer' \
                            ORDER BY device, property_attribute_device.name, property_attribute_device.count ASC"

    SERVER_QUERY = "SELECT server, class, name FROM device \
                WHERE class != 'DServer' \
                ORDER BY server ASC"

    # Servers that NEVER must be removed
    DEFAULT_SKIP_SERVER_NAMES = [
        "configuration/stat",
        "databaseds/2",
        "tangorestserver/rest",
        "tangotest/test",
        "tangoaccesscontrol/1",
    ]

    def __init__(self, db: Database, tangodb_timeout: int = 10000):
        self.db = db  # TangoDB
        self.dbproxy = create_device_proxy(db.dev_name(), tangodb_timeout)

    @classmethod
    def get_validator(cls):
        """Retrieve the JSON validator from Schemas container"""
        return Draft7Validator(
            REGISTRY["station-configuration"].contents,
            format_checker=FormatChecker(),
            registry=REGISTRY,
        )

    #
    #   DUMPING TANGO DATABASE
    #

    def get_tangodb_data(self) -> dict:
        """Dump a subset of TANGO database into dictionary.

        The dictionary contains the info about all the Devices used in the
        present environment, including their Properties values, their Attribute Properties,
        and the namespace of the DeviceServers which incapsulate each Device.
        """
        # Create empty dictionaries to be populated
        devices_dict = {}
        server_dict = {}

        # Populate devices dictionary from query data
        device_property_result = self._query_tangodb(
            self.dbproxy, self.DEVICE_PROPERTIES_QUERY, 3
        )
        devices_dict = self.add_to_devices_dict(devices_dict, device_property_result)

        # Populate devices dictionary from query data
        attrs_property_result = self._query_tangodb(
            self.dbproxy, self.ATTRS_PROPERTIES_QUERY, 4
        )
        devices_dict = self.add_to_attrs_dict(devices_dict, attrs_property_result)

        # Populate server dictionary from query data and merge it with devices dict
        server_result = self._query_tangodb(self.dbproxy, self.SERVER_QUERY, 3)
        server_dict = self.add_to_server_dict(server_dict, devices_dict, server_result)
        return {"servers": server_dict}

    def _query_tangodb(
        self, dbproxy: DeviceProxy, sql_query: str, num_cols: int
    ) -> list:
        """Query TangoDb with a built-in function and return data as tuples"""
        _, raw_result = dbproxy.command_inout("DbMySqlSelect", sql_query)
        return self.query_to_tuples(raw_result, num_cols)

    def add_to_devices_dict(self, devices_dict: dict, result: list) -> dict:
        """Populate a devices dictionary with the following structure:
        'device_name': { 'properties' : { 'property_name': ['property_value'] } }
        """
        for device, property, value in result:
            # lowercase data
            device = device.lower()
            property = property.lower()
            # model dictionary
            device_data = devices_dict.setdefault(device, {})
            property_data = device_data.setdefault("properties", {})
            value_data = property_data.setdefault(property, [])
            value_data.append(value)
        return devices_dict

    def add_to_attrs_dict(self, devices_dict: dict, result: list) -> dict:
        """Populate a device dictionary with the following structure :
        'device_name': { 'attribute_properties' : { 'attribute_name': {'property_name' : ['property_value'] } } }
        """
        for device, attribute, _property, value in result:
            # lowercase data
            device = device.lower()
            attribute = attribute.lower()
            _property = _property.lower()
            # model dictionary
            device_data = devices_dict.setdefault(device, {})
            property_data = device_data.setdefault("attribute_properties", {})
            attr_data = property_data.setdefault(attribute, {})
            value_data = attr_data.setdefault(_property, [])
            value_data.append(value)
        return devices_dict

    def add_to_server_dict(
        self, server_dict: dict, devices_dict: dict, result: list
    ) -> dict:
        """Populate the server dictionary and merge it with the devices dictionary.
        At the end of the process, the dictionary will have the following structure :
        'server_name' : { 'server_instance' : { 'server_class' :
            'device_name':  { 'properties' : { 'property_name': ['property_value'] } },
                            { 'attribute_properties' : { 'attribute_name': {'property_name' : ['property_value'] } } } } }
        """
        for server, sclass, device in result:
            # lowercase data
            device = device.lower()
            server = server.lower()
            sclass = sclass.lower()
            # model dictionary
            sname, instance = server.split("/")
            device_data = devices_dict.get(device, {})
            server_data = server_dict.setdefault(sname, {})
            instance_data = server_data.setdefault(instance, {})
            class_data = instance_data.setdefault(sclass, {})
            # merge the two dictionaries
            server_dict[sname][instance][sclass][device] = device_data
        return server_dict

    def query_to_tuples(self, result: list, num_cols: int) -> list:
        """Given a query result and its number of columns,
        transforms the raw result in a list of tuples"""
        return list(zip(*[islice(result, i, None, num_cols) for i in range(num_cols)]))

    #
    #   LOADING JSON INTO TANGO DATABASE
    #

    def load_configdb(self, station_configuration: str, update: bool = False):
        """Takes a JSON string which represents the station configuration
        and loads the whole configuration.

        N.B. with flag update=False, it does not update, it loads a full new configuration.
        """
        # Convert json string into dictionary
        try:
            tangodb_dict = json.loads(station_configuration)
        except json.JSONDecodeError as e:
            raise ValueError(f"JSON string not valid") from e
        # Check if json adhere to schema
        try:
            self.get_validator().validate(tangodb_dict)
            logger.info(f"JSON data correctly validated")
        except ValidationError as e:
            raise ValidationError("JSON data does not match schema") from e
        # Select if update or loading configuration from scratch
        if not update:
            # Select the servers to be removed after having built a proper select query
            server_select_query = self.build_select_server_query(
                self.DEFAULT_SKIP_SERVER_NAMES
            )
            servers_to_be_removed = [
                server
                for server, _, _ in self._query_tangodb(
                    self.dbproxy, server_select_query, 3
                )
            ]
            for server in servers_to_be_removed:
                # Remove devices
                self.delete_server(server)
        # Select new configuration and add to DB
        self.add_server(tangodb_dict)

    def build_select_server_query(self, default_skip_server_names: list):
        """Select the servers to be removed except the default ones
        in TangoDB (also ConfigurationDevice) and
        build the relative SQL query.
        """
        sql_query = f"SELECT server, class, name FROM device WHERE class != 'DServer' "
        for server in default_skip_server_names:
            sql_query += f" AND LOWER(server) !='{server.lower()}' "
        sql_query += f"ORDER BY server ASC"
        return sql_query

    def delete_server(self, server: str):
        """Given a list of server names, delete the servers from Tango DB,
            and all their nested data (Devices, Attributes, Properties, Values)
        N.B. This action cannot be undone
        """
        try:
            # https://pytango.readthedocs.io/en/stable/database.html#tango.Database.delete_server
            self.db.delete_server(server)
            logger.info(f"Server {server} has been removed from DB")
        except DevFailed:
            logger.warning(f"[Delete Server] Server {server} not found in DB")

    def add_server(self, tangodb_dict: dict):
        """Given a new TangoDb Configuration as a dictionary,
        create a server object with its own parameters, and add it to the Database.

        N.B. structure is:
        'server_name' : { 'server_instance' : { 'server_class' :
            'device_name':  { 'properties' : { 'property_name': ['property_value'] } },
                            { 'attribute_properties' : { 'attribute_name': {'property_name' : ['property_value'] } } } } }
        """
        configuration_db = tangodb_dict["servers"]
        for server_name, instance_data in configuration_db.items():
            for instance, class_data in instance_data.items():
                # Excluding from update default_skip_servers as well
                if (
                    f"{server_name}/{instance}".lower()
                    not in self.DEFAULT_SKIP_SERVER_NAMES
                ):
                    for _class, device_data in class_data.items():
                        for device_name in device_data:
                            self._insert_server_into_db(
                                server_name, instance, _class, device_data, device_name
                            )

    def _insert_server_into_db(
        self,
        server_name: str,
        instance: str,
        _class: str,
        device_data: str,
        device_name: str,
    ):
        """Insert a new server with all its relative info into the Tango DB"""
        device_info = DbDevInfo()  # Built-in Tango object to interact with DB
        # Set device name
        device_info.name = device_name
        # Set class name
        device_info._class = _class
        # Set server info
        device_info.server = f"{server_name}/{instance}"
        # Add server to Tango DB
        # https://pytango.readthedocs.io/en/stable/database.html#tango.Database.add_server
        self.db.add_server(device_info.server, device_info, with_dserver=False)
        logger.info(f"Server {server_name}/{instance} has been inserted into DB")
        # Add device properties
        device_property_data = device_data.get(device_name, {})
        if "properties" in device_property_data:
            property_data = device_property_data["properties"]
            # https://pytango.readthedocs.io/en/stable/database.html#tango.Database.put_device_property
            self.db.put_device_property(device_name, property_data)
        # Add attribute properties
        if "attribute_properties" in device_property_data:
            attr_property_data = device_property_data["attribute_properties"]
            # https://pytango.readthedocs.io/en/stable/database.html#tango.Database.put_device_attribute_property
            self.db.put_device_attribute_property(device_name, attr_property_data)

    #
    #   UPLOAD JSON INTO TANGO DATABASE
    #

    def update_configdb(self, station_configuration: str):
        """Takes a JSON string which represents the station configuration
        and upload the whole configuration.

        N.B. it does not delete existing devices, it updates overlapping parameters.
        """
        self.load_configdb(station_configuration, update=True)

    #
    #   HELPER METHODS
    #

    def remove_backup_property(self, dbdata: dict) -> dict:
        """Remove from station configuration dictionary the backup_station_configuration property
        to avoid recursive behaviour
        """
        try:
            if "configuration" in dbdata["servers"]:
                device_dict = dbdata["servers"]["configuration"]["stat"][
                    "configuration"
                ]["stat/configuration/1"]
                if "backup_station_configuration" in device_dict["properties"]:
                    del device_dict["properties"]["backup_station_configuration"]
        except KeyError:
            return dbdata
        return dbdata
