#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import textwrap
from datetime import datetime, timedelta
from dataclasses import dataclass
from functools import partial
import logging
import time
from typing import List, Dict, Callable, Optional

from tango import DeviceProxy, EventType, DevFailed
from prometheus_client import Counter

from tangostationcontrol.common.lofar_logging import exception_to_str
from tangostationcontrol.common.constants import (
    DEFAULT_METRICS_POLLING_PERIOD_MS,
)
from lofar_station_client.common import CaseInsensitiveString
from tangostationcontrol.metrics import AttributeMetric

logger = logging.getLogger()

EventCallbackType = Callable[[DeviceProxy, str, object], None]


@dataclass(frozen=False)
class Subscription:
    proxy: DeviceProxy
    attribute_name: CaseInsensitiveString
    event_id: int | None
    callbacks: List[EventCallbackType]
    nr_callbacks_called: int


class EventSubscriptions:
    """Manages a set of subscriptions to Tango events."""

    # Amount of time required for the subscription to propagate
    # to the device in the Device Server.
    SUBSCRIPTION_CONFIRM_TIMEOUT = 5.0

    # Interval at which to check for the subscription
    SUBSCRIPTION_CONFIRM_POLLING_PERIOD = 0.1

    def __init__(self, device_labels: Dict[str, str] | None = None):
        # events we're subscribed to
        self.subscriptions: List[Subscription] = []

        # metric to count events
        self.event_count_metric = AttributeMetric(
            "event_count",
            "Number of events received (including errors)",
            device_labels or {},
            Counter,
            dynamic_labels=["event_device", "event_attribute"],
        )

        # metric to count errors
        self.error_count_metric = AttributeMetric(
            "event_error_count",
            "Number of times received events indicated an error",
            device_labels or {},
            Counter,
            dynamic_labels=["event_device", "event_attribute"],
        )

    def _get_subscription(
        self, dev_name: str, attr_name: str
    ) -> Optional[Subscription]:
        for sub in self.subscriptions:
            if (
                CaseInsensitiveString(sub.proxy.name()) == dev_name
                and sub.attribute_name == attr_name
            ):
                return sub

        return None

    def is_subscribed(self, dev_name: str, attr_name: str) -> bool:
        return self._get_subscription(dev_name, attr_name) is not None

    def _event_callback_wrapper(self, sub, event):
        device = event.device

        # some errors report about the device in general, and thus are not
        # annotated with any attr_value.
        attribute_name = event.attr_value.name if event.attr_value else None

        self.event_count_metric.get_metric([device.name(), str(attribute_name)]).inc()

        # Count reported errors. These can also occur if the device
        # is down, intentional or not. So event errors do not necessarily
        # indicate a problem.
        if event.err:
            self.error_count_metric.get_metric(
                [device.name(), str(attribute_name)]
            ).inc()

            logger.debug(
                "Received attribute change event ERROR from %s for attribute %s: %s",
                device,
                attribute_name,
                event.errors,
            )

            # Little we can do here
            return

        value = event.attr_value.value

        # Log succesful changes
        logger.info(
            "Received attribute change event from %s: %s := %s",
            device,
            attribute_name,
            textwrap.shorten(str(value), width=75),
        )

        # Call requested callback
        for callback in sub.callbacks:
            try:
                callback(device, attribute_name, value)
            except Exception as ex:
                logger.exception(
                    f"Callback {callback} for device {device} attribute {attribute_name} threw an exception: {exception_to_str(ex)}"
                )

        # increase counters
        sub.nr_callbacks_called += 1

    def poll_period(self, proxy: DeviceProxy, attr_name: str) -> int | None:
        """Return the polling period for the given attribute, in milliseconds,
        or None if the attribute is not polled at all."""

        # LofarDevice polls a set of attributes not covered by Tango's poll loop
        try:
            if proxy.is_attribute_polled_by_lofardevice(attr_name):
                return DEFAULT_METRICS_POLLING_PERIOD_MS
        except AttributeError:
            # Not a LofarDevice
            pass

        # Check if polled by Tango
        if proxy.is_attribute_polled(attr_name):
            return proxy.get_attribute_poll_period(attr_name)

        # No polling detected
        return None

    def subscribe_change_event(
        self,
        proxy: DeviceProxy,
        attr_name: str,
        callback: EventCallbackType,
        # a friendly default rate that we tend to poll at anyway
        period: int = DEFAULT_METRICS_POLLING_PERIOD_MS,
        confirm: bool = False,
    ):
        """Subscribe to changes to an attribute of another device.
        Immediately and on change, the provided callback will be called as

          callback(device, attribute_name, value)

        Where the attribute_name can be in either the original or lower case.

        Event errors are logged, counted, and dropped without invoking
        the callback.

        confirm: wait until the subscription actually reaches the device.
        """

        # make sure th attribute is polled often enough
        current_period = self.poll_period(proxy, attr_name)

        if current_period is None or current_period > period:
            raise Exception(
                f"Cannot subscribe to {proxy.name()}/{attr_name} because it is not polled."
            )
        if current_period > period:
            raise Exception(
                f"Cannot subscribe to {proxy.name()}/{attr_name} because it is not polled often enough. Poll rate is {current_period} ms but {period} ms is requested."
            )

        # add or extend subscription to CHANGE_EVENT
        if sub := self._get_subscription(proxy.name(), attr_name):
            # extend existing subscription
            logger.debug(
                f"Adding callback {callback} to CHANGE_EVENT for  {proxy.name()}/{attr_name}"
            )
            sub.callbacks.append(callback)

            # manually trigger first call, mimicking Tango doing so when subscribing
            try:
                value = proxy.read_attribute(attr_name).value

                callback(proxy, attr_name, value)
            except Exception as ex:
                logger.exception(
                    f"Callback {callback} for device {proxy} attribute {attr_name} threw an exception"
                )

        else:
            # make new subscription
            sub = Subscription(
                proxy=proxy,
                attribute_name=CaseInsensitiveString(attr_name),
                event_id=None,
                callbacks=[callback],
                nr_callbacks_called=0,
            )

            # subscribe
            logger.debug(
                f"Subscribing callback {callback} to CHANGE_EVENT on device {proxy} attribute {attr_name}"
            )
            sub.event_id = proxy.subscribe_event(
                attr_name,
                EventType.CHANGE_EVENT,
                partial(self._event_callback_wrapper, sub),
                stateless=True,
            )

            self.subscriptions.append(sub)

        if confirm:
            # wait for the subscription to settle
            logger.debug(
                f"Waiting for subscription on device {proxy} attribute {attr_name} to reach that device"
            )
            deadline = datetime.now() + timedelta(
                seconds=self.SUBSCRIPTION_CONFIRM_TIMEOUT
            )

            # a new subscription results in at least 1 CHANGE_EVENT for the initial value,
            # but only if the device is running and can be reached
            while sub.nr_callbacks_called == 0:
                if datetime.now() > deadline:
                    raise TimeoutError(
                        "Did not receive CHANGE_EVENT to confirm the subscription reached the device"
                    )

                time.sleep(self.SUBSCRIPTION_CONFIRM_POLLING_PERIOD)

            logger.debug(
                f"Subscription on device {proxy} attribute {attr_name} confirmed"
            )

    def unsubscribe_all(self):
        # unsubscribe from all events
        try:
            for sub in self.subscriptions:
                try:
                    sub.proxy.unsubscribe_event(sub.event_id)
                except DevFailed:
                    logger.error(f"Failed to unsubscribe from event {sub}")
        finally:
            self.subscriptions = []
