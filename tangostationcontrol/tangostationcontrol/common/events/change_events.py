#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import numpy

from lofar_station_client.common import CaseInsensitiveString, CaseInsensitiveDict
from tango.server import Device


class ChangeEvents:
    """Manages generating custom CHANGE_EVENTs for attributes, instead of having them be generated from Tango's polling.

    This allows the user to subscribe to these attributes without requiring Tango to poll them.
    """

    def __init__(self, device: Device):
        self.device = device

        # keep track of which attributes we manage
        self.attributes: list[CaseInsensitiveString] = []

        # previous values of attributes, to avoid
        # emitting CHANGE_EVENTs when nothing changed.
        self.prev_values: CaseInsensitiveDict[str, object] = {}

    def is_configured(self, attr_name: str) -> bool:
        return attr_name in self.attributes

    def configure_attribute(self, attr_name: str):
        """Prepares an attribute for emitting custom change events."""

        attr = getattr(self.device, attr_name)

        # tell Tango we will be sending change events ourselves
        # and detecting changes as well.
        # We determine change ourselves, to use Tango
        # for this, abs_change or rel_change needs to
        # be configured for the attribute. We
        # trigger on any change though (rel_change=1.0),
        # so detecting it ourselves is easier.
        self.device.set_change_event(attr_name, True, False)

        self.attributes.append(CaseInsensitiveString(attr_name))

    def send_change_event(self, attr_name: str, value: object | None):
        """Emits a CHANGE_EVENT if the attribute has changed value."""

        try:
            if numpy.equal(self.prev_values.get(attr_name), value).all():
                # no change
                return
        except (ValueError, TypeError):
            # thrown f.e. if the previous and current values have different dimensions or types
            pass

        # avoid sending spurious changes
        self.prev_values[attr_name] = value

        if value is None:
            # could send an ATTR_INVALID change event, if we can somehow
            # generate a default value to send to use
            # self.device.push_change_event(attr_name, value, time.now(), AttrQuality.ATTR_INVALID, 0, 0)
            return

        if (
            self.device.is_attribute_polled(attr_name)
            and self.device.get_attribute_poll_period(attr_name) > 0
        ):
            # Emitting an event now can cause a segfault in devices using the Asyncio green mode,
            # if the Tango polling thread is accessing the same attribute we push an event for.
            #
            # See https://gitlab.com/tango-controls/cppTango/-/merge_requests/1316
            # and https://gitlab.com/tango-controls/pytango/-/merge_requests/729
            raise RuntimeError(
                f"Cannot send change event for attribute {attr_name} of device {self.device} as it is already polled by Tango. Calling push_change_event can cause a Segmentation Fault."
            )

        # emit any change
        self.device.push_change_event(attr_name, value)
