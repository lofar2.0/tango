#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging

from tango import DeviceProxy

from tangostationcontrol.common.constants import DEFAULT_POLLING_PERIOD_METADATA_MS
from tangostationcontrol.common.events import EventSubscriptions
from tangostationcontrol.common.events.subscriptions import EventCallbackType
from tangostationcontrol.common.types.device_config_types import (
    device_config_type,
    device_proxy_type,
)

logger = logging.getLogger()


def register_change_event_subscriptions(
    config: device_config_type,
    proxies: device_proxy_type,
    subscriptions: EventSubscriptions,
    event_callback: EventCallbackType,
):
    """Register change events for working device proxies"""

    for device_name, proxy in proxies.items():
        if not isinstance(proxy, DeviceProxy):
            logger.exception(
                "Failed to subscribe to change events for device %s due to %s",
                device_name,
                proxy,
            )
            continue

        dev_info = proxy.info()
        attributes = config.get(dev_info.dev_class, None)
        if not attributes:
            logger.warning(
                "register_change_event_subscriptions called for device (%s) not "
                "present in confgi, aborting subscription",
                device_name,
            )
            continue

        for attribute_name in attributes:
            try:
                subscriptions.subscribe_change_event(
                    proxy,
                    attribute_name,
                    event_callback,
                    DEFAULT_POLLING_PERIOD_METADATA_MS,
                )
            except Exception as e:
                logger.exception(
                    "Subscription for attribute %s on device %s failed due to %s",
                    attribute_name,
                    device_name,
                    e,
                )
