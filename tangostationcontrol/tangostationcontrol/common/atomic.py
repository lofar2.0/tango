# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Simple synchronization primitive for threads"""

import threading
from typing import TypeVar

T = TypeVar("T")


class Atomic:
    """Synchronization primitive for threads"""

    def __init__(self, initial_value: T):
        self._mutex = threading.Lock()
        self._value: T = initial_value

    @property
    def value(self) -> T:
        with self._mutex:
            return self._value

    @value.setter
    def value(self, value: T):
        with self._mutex:
            self._value = value
