# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""
Baseline calculation functions.
"""

import math


def nr_baselines(nr_inputs: int) -> int:
    """Return the number of baselines (unique pairs) that exist between a given number of inputs."""
    return nr_inputs * (nr_inputs + 1) // 2


"""

 Baselines are ordered like:
   0-0, 1-0, 1-1, 2-0, 2-1, 2-2, ...

 if
   b = baseline
   x = stat1 (major)
   y = stat2 (minor)
   x >= y
 then
   b_xy = x * (x + 1) / 2 + y
 let
   u := b_x0
 then
     u            = x * (x + 1) / 2
     8u           = 4x^2 + 4x
     8u + 1       = 4x^2 + 4x + 1 = (2x + 1)^2
     sqrt(8u + 1) = 2x + 1
                x = (sqrt(8u + 1) - 1) / 2

 Let us define
   x'(b) = (sqrt(8b + 1) - 1) / 2
 which increases monotonically and is a continuation of y(b).

 Because y simply increases by 1 when b increases enough, we
 can just take the floor function to obtain the discrete y(b):
   x(b) = floor(x'(b))
        = floor(sqrt(8b + 1) - 1) / 2)

"""


def baseline_index(major: int, minor: int) -> int:
    """Provide a total ordering of baselines: give the unique array index for the baseline (major,minor),
    with major >= minor."""

    if major < minor:
        raise ValueError(
            f"major < minor: {major} < {minor}. Since we do not store the conjugates this will lead to processing errors."
        )

    return major * (major + 1) // 2 + minor


def baseline_from_index(index: int) -> tuple:
    """Return the (major,minor) input pair given a baseline index."""

    major = int((math.sqrt(float(8 * index + 1)) - 0.99999) / 2)
    minor = index - baseline_index(major, 0)

    return (major, minor)
