#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import collections
import logging
from typing import Iterator

from dns import resolver

logger = logging.getLogger()

Service = collections.namedtuple("Service", ["host", "addr", "port"])


class ServiceLookupException(Exception):
    """Exception that is raised when the service lookup failed."""

    def __str__(self) -> str:
        return "Service lookup failed for %s: %s" % self.args[0:2]


def lookup_service(name: str) -> Iterator[Service]:
    fqdn = f"{name}.service.consul"

    try:
        result: resolver.Answer = resolver.resolve(fqdn, "SRV")
    except (
        resolver.NoAnswer,
        resolver.NoNameservers,
        resolver.NotAbsolute,
        resolver.NoRootSOA,
        resolver.NXDOMAIN,
    ) as error:
        logger.error("Querying SRV %s failed: %r", fqdn, error)
        raise ServiceLookupException(name, error)

    for resource in result:
        host = resource.target.to_text(omit_final_dot=True)
        yield Service(host, host, resource.port)


if __name__ == "__main__":
    lookup_service("s3")
