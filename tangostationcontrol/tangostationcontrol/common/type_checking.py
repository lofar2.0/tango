# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Union

from tango.utils import is_seq
from tango import DeviceProxy


def sequence_not_str(obj):
    """True for sequences that are not str, bytes or bytearray"""
    return is_seq(obj) and not isinstance(obj, (str, bytes, bytearray))


def type_not_sequence(obj):
    """True for types that are not sequences"""
    return not is_seq(obj) and isinstance(obj, type)


def device_name_matches(lhs: str, rhs: str):
    """Returns true if the device names are equivalent"""
    return lhs.casefold() == rhs.casefold()


def device_class_matches(
    device: DeviceProxy, class_name: Union[str, list[str]]
) -> bool:
    """Returns whether the clas of the device represented by `device`
    matches `class_name`, which is either a string or a list of strings."""

    # NB: We'd like to use device.info().dev_class but that requires
    # a connection to the device instead of just to the database.
    # If the pattern does not match, the device might not be needed,
    # so we should not assume the device is actually up.
    #
    # In production, they always should be up, but it grealy helps
    # development and debugging if we can ignore devices that are
    # in the hierarchy but not needed for the requested state
    # transition.
    device_class_name = device.dev_name().split("/")[1].casefold()
    if isinstance(class_name, str):
        return device_class_name == class_name.casefold()
    else:
        return device_class_name in (name.casefold() for name in class_name)
