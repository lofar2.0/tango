#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import numpy

from tango import Util
from lofar_station_client.common import CaseInsensitiveString


def antenna_set_to_mask(
    antenna_set: str,
    nr_antennas: int,
    antenna_setlist: numpy.ndarray,
    antenna_set_masks: numpy.ndarray,
):
    """Translate the antenna set string code into the corresponding
    antenna mask array

    :param antenna_set: name of the antenna set to use for beam forming
    :param nr_antennas: number of configured antennas from the associated antenna field
    :param antenna_setlist: string representation of officially offered set of antennas
    :param antenna_set_masks: string encoding of the corresponding antenna masks
                              for the antennafield
    :raises ValueError: Raised if selected antenna_set is not in antenna_setlist
    """
    try:
        # Retrieve antenna_set entry position
        antenna_index = list(antenna_setlist).index(antenna_set)
        # Return the corresponding antenna_mask array,
        # cut off for our number of antennas if needed, to support generic masks like "ALL"
        return numpy.array(
            [int(x) for x in list(antenna_set_masks[antenna_index])][:nr_antennas],
            dtype=bool,
        )
    except ValueError as exc:
        raise ValueError(
            f"Unsupported antenna mask with the following antenna set: {antenna_set}. \
            Must be one of {antenna_setlist}"
        ) from exc


def antenna_field_family_name(antenna_field_name: str) -> str:
    """Retrieve the correct AntennaField family (class) name for the given antenna field

    :param antenna_field_name: name of an antenna field (f.e. LBA)

    :raises ValueError: raised if antenna_field_name does not equal a valid antenna field

    :return: the AntennaField family name (f.e. AFL)
    """
    match antenna_field_name:
        case "LBA":
            return "AFL"
        case "HBA" | "HBA0" | "HBA1":
            return "AFH"

    raise ValueError(
        f"Invalid value for antenna_field_name parameter: {antenna_field_name}"
    )


def device_member_to_full_device_name(device_member: str) -> str:
    """Retrieve the correct full Tango device name based on the antenna type

    :param device_member: antennafield Tango device member (n.b. domain/family/member)

    :raises ValueError: raised if device_member does not contain the antenna keywords

    :return: a string with the correct Tango device full name
    """
    util = Util.instance()
    if CaseInsensitiveString("LBA") in CaseInsensitiveString(device_member):
        return f"{util.get_ds_inst_name()}/AFL/{device_member}"
    if CaseInsensitiveString("HBA") in CaseInsensitiveString(device_member):
        return f"{util.get_ds_inst_name()}/AFH/{device_member}"
    if CaseInsensitiveString("HBA0") in CaseInsensitiveString(device_member):
        return f"{util.get_ds_inst_name()}/AFH/{device_member}"
    if CaseInsensitiveString("HBA1") in CaseInsensitiveString(device_member):
        return f"{util.get_ds_inst_name()}/AFH/{device_member}"
    raise ValueError(f"Invalid value for antennafield parameter: {device_member}")


def device_member_to_antenna_type(device_member: str) -> str:
    """Determine whether the antennafield device is HBA or LBA

    :param device_member: antennafield Tango device member (n.b. domain/family/member)

    :raises ValueError: raised if device_name does not contain the antenna keywords

    :return: a string with the correct antenna type
    """
    if CaseInsensitiveString("LBA") in CaseInsensitiveString(device_member):
        return "LBA"
    if CaseInsensitiveString("HBA") in CaseInsensitiveString(device_member):
        return "HBA"
    if CaseInsensitiveString("HBA0") in CaseInsensitiveString(device_member):
        return "HBA"
    if CaseInsensitiveString("HBA1") in CaseInsensitiveString(device_member):
        return "HBA"
    raise ValueError(f"Invalid value for antennafield: {device_member}")
