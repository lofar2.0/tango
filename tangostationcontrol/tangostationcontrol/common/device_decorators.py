# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Tango device decorators"""

import logging
import time
import asyncio
from contextlib import contextmanager
from functools import wraps

from tango import DevState, DevFailed, DeviceProxy
from prometheus_client.metrics import Histogram

from tangostationcontrol.common.lofar_logging import (
    exception_to_str,
    get_current_device,
)
from tangostationcontrol.metrics import AttributeMetric

logger = logging.getLogger()

__all__ = [
    "suppress_exceptions",
    "only_in_states",
    "only_when_on",
    "fault_on_error",
    "debugit",
    "DurationMetric",
    "DurationStatistics",
]


def _wrap_sync_or_async(context, func):
    """Wrap a function with a context manager, and keep
    the function synchronous/asynchronous."""

    if asyncio.iscoroutinefunction(func):

        @wraps(func)
        async def wrapper(*args, **kwargs):
            with context(*args, **kwargs):
                return await func(*args, **kwargs)

    else:

        @wraps(func)
        def wrapper(*args, **kwargs):
            with context(*args, **kwargs):
                return func(*args, **kwargs)

    return wrapper


def log_exceptions(logger: logging.Logger = None, suppress: bool = False):
    """Decorator that logs all exceptions that the function raises."""

    def wrapper(func):
        @contextmanager
        def inner(*args, **kwargs):
            try:
                yield
            except Exception as e:
                (logger or logging.getLogger()).exception(
                    f"Unhandled exception: {e.__class__.__name__}: {e}"
                )

                # we can log but we cannot hide
                if not suppress:
                    raise

        return _wrap_sync_or_async(inner, func)

    return wrapper


def suppress_exceptions(suppress: bool = True):
    """Catch all Exceptions and collect them in a list `exceptions` attached
    to the decorated function. If suppress = True, do not reraise them.

    Each element in the `exceptions` list is a (device, exception) tuple,
    where "device" is the parameter to func(device) on which func
    threw the exception.

    Never supresses programming/deployment errors (SyntaxError, etc)."""

    def inner(func):
        @contextmanager
        def wrapper(device: DeviceProxy, *args, **kwargs):
            try:
                yield
            except (SyntaxError, NameError, ImportError) as e:
                # These exceptions we never suppress
                raise
            except Exception as e:
                logger.exception(
                    f"Collected exception during execution of {func.__name__}({device}): {exception_to_str(e)}"
                )

                # collect it
                wrapper.exceptions.append((device, e))

                # reraise if requested
                if not suppress:
                    raise

        # collection of exceptions raised by this function
        wrapper.exceptions = []

        result = _wrap_sync_or_async(wrapper, func)

        # propagate reference to exceptions
        result.exceptions = wrapper.exceptions
        return result

    return inner


def only_in_states(allowed_states, log=True):
    """
    Wrapper to call and return the wrapped function if the device is
    in one of the given states. Otherwise a PyTango exception is thrown.
    """

    def wrapper(func):
        @contextmanager
        def state_check_wrapper(self, *args, **kwargs):
            if self.get_state() not in allowed_states:
                if log:
                    logger.warning(
                        "Illegal command: Function %s can only be called in states %s. "
                        "Current state: %s",
                        func.__name__,
                        allowed_states,
                        self.get_state(),
                    )

                raise Exception(
                    f"IllegalCommand: Function {func.__name__} can only be called in "
                    + f"states {allowed_states}. Current state: {self.get_state()}"
                )

            # if state is allowed, execute wrapped function
            yield

        return _wrap_sync_or_async(state_check_wrapper, func)

    return wrapper


def only_when_on():
    """
    Wrapper to call and return the wrapped function if the device is
    in the ON state. Otherwise None is returned and nothing
    will be called.
    """

    def inner(func):
        @contextmanager
        def when_on_wrapper(self, *args, **kwargs):
            if self.get_state() == DevState.ON:
                yield

        return _wrap_sync_or_async(when_on_wrapper, func)

    return inner


def fault_on_error():
    """
    Wrapper to catch exceptions. Sets the device in a FAULT state if any occurs.
    """

    def inner(func):
        @contextmanager
        def error_wrapper(self, *args, **kwargs):
            try:
                yield
            except DevFailed as _e:
                logger.exception("Function failed.")
                self.Fault(f"FAULT in {func.__name__}: {_e.args[-1].desc}")
                raise
            except Exception as _e:
                logger.exception("Function failed.")
                self.Fault(f"FAULT in {func.__name__}: {_e.__class__.__name__}: {_e}")
                raise

        return _wrap_sync_or_async(error_wrapper, func)

    return inner


def debugit(log_function=logger.debug):
    """
    Clone of tango.DebugIt, but logs to Python's logger instead. Also
    logs the duration of the call.

    Works on both
    """

    def inner(func):
        @contextmanager
        def context(*args, **kwargs):
            try:
                log_function(f">>> {func.__name__}")
                before = time.monotonic()
                yield
            finally:
                after = time.monotonic()
                log_function(f"<<< {func.__name__}, took {(after-before):.2f} s")

        return _wrap_sync_or_async(context, func)

    return inner


def DurationMetric(
    get_metric_labels=lambda _: get_current_device().metric_labels,
    get_metric_name=lambda obj, func: func.__name__,
    buckets=Histogram.DEFAULT_BUCKETS,
):
    """
    Wrapper to expose a Prometheus Histogram traking the duration of calls,
    per object ("self"), called "Duration_{method_name}".

    The labels for this metric is stored per object in "device.metric_labels",
    where "device" is discovered using "get_current_device(self)".

    The `get_metric_labels` parameter can be used to modify this
    behaviour.
    """

    def inner(func):
        metrics = {}

        @contextmanager
        def metric_wrapper(self, *args, **kwargs):
            # obtain Prometheus metric
            if self not in metrics:
                metrics[self] = AttributeMetric(
                    f"duration_{get_metric_name(self, func)}",
                    f"Duration of calls to {get_metric_name(self, func)}",
                    get_metric_labels(self),
                    Histogram,
                    metric_class_init_kwargs={"buckets": buckets},
                )

            metric = metrics[self].get_metric()

            # time function call
            with metric.time():
                yield

        return _wrap_sync_or_async(metric_wrapper, func)

    return inner


def DurationStatistics(
    log_function=None, max_history_len=10, key_function=lambda args, kwargs: args[0]
):
    """
    Wrapper to time calls, per object ("self"). Stores the timing log in a
    <function>.get_statistic(self) function returning a dict with the following
    information:

      "count":        number of times the function was called
      "last":         duration of the last invocation
      "history":      last "max_history_len" durations

    NOTE: If the function called throws an exception, timing information
          is not logged or recorded. Those calls are expected to be
          uncharacteristically cheap, after all.
    """

    def inner(func):
        default_statistic = {
            "count": 0,
            "last": None,
            "history": [],
        }

        statistics = {}

        @contextmanager
        def timer_wrapper(*args, **kwargs):
            # time function call
            before = time.monotonic_ns()
            yield
            after = time.monotonic_ns()

            # maintain statistics per object ("self", so the first argument)
            key = key_function(args, kwargs) if key_function else None
            if key not in statistics:
                statistics[key] = default_statistic.copy()

            # store measurement
            statistics[key]["count"] += 1
            statistics[key]["last"] = (after - before) / 1e9
            statistics[key]["history"] = statistics[key]["history"][
                -(max_history_len - 1) :
            ] + [statistics[key]["last"]]

            if log_function:
                log_function(
                    f"Call to {func.__name__} took {(after - before) / 1e6} ms"
                )

        # propagate get_statistic function to caller
        result = _wrap_sync_or_async(timer_wrapper, func)

        # get the statistics for a specific object
        result.get_statistic = lambda self: statistics.get(
            self, default_statistic.copy()
        )

        return result

    return inner
