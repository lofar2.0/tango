# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from functools import wraps
from typing import List
import logging
import time

from tango import DevFailed, DeviceProxy
from lofar_station_client.common import CaseInsensitiveString


logger = logging.getLogger()


def ensure_device_boots(proxy: DeviceProxy = None):
    """Wrapper to ensure device boots before a test"""

    def inner(func):
        @wraps(func)
        def ensure_device_boots_wrapper(self, *args, **kwargs):
            _proxy = proxy if proxy is not None else self.proxy
            try:
                _proxy.boot()
            except DevFailed as exc:
                logger.error("Failed to boot device: %s", exc)
                raise exc
            # Execute function
            return func(self, *args, **kwargs)

        return ensure_device_boots_wrapper

    return inner


def preserve_off_state_for_devices(devices: List[any]):
    """Wrapper to shutdown selected devices after method execution"""

    def inner(func):
        @wraps(func)
        def preserve_off_state_for_devices_wrapper(self, *args, **kwargs):
            # Execute function
            result = func(self, *args, **kwargs)
            # Shutdown devices
            for dev in devices:
                try:
                    DeviceProxy(CaseInsensitiveString(dev)).Off()
                except DevFailed as exc:
                    # Failing to turn Off devices should not raise errors here
                    logger.error("Failed to turn device %s off: %s", dev, exc)
                    # Wait for 1 second to prevent propagating reconnection errors
                    time.sleep(1)
            return result

        return preserve_off_state_for_devices_wrapper

    return inner


def restore_properties_for_devices(devices: List[any]):
    """Wrapper to restore original properties in a given list of device proxies"""

    def inner(func):
        @wraps(func)
        def restore_properties_for_devices_wrapper(self, *args, **kwargs):
            # Dictionary to be filled with original properties
            properties_dict = {}
            for dev in devices:
                try:
                    device_proxy = DeviceProxy(CaseInsensitiveString(dev))
                except DevFailed as exc:
                    logger.error("Device %s not found", dev)
                    raise exc
                # Current properties of self.proxy
                original_properties = (
                    device_proxy.get_property(device_proxy.get_property_list("*") or {})
                    or {}
                )
                properties_dict[dev] = original_properties
            # Execute function
            result = func(self, *args, **kwargs)
            # Restore properties
            for dev in devices:
                device_proxy = DeviceProxy(CaseInsensitiveString(dev))
                device_proxy.delete_property(device_proxy.get_property_list("*"))
                device_proxy.put_property(properties_dict[dev])
            return result

        return restore_properties_for_devices_wrapper

    return inner
