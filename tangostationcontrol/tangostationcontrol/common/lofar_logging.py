#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging
import socket
import time
import traceback

from tango import DevFailed
from tango.server import Device
from logfmter import Logfmter
from pysnmp.smi.error import SmiError

from tangostationcontrol import __version__ as version


def exception_to_str(ex: Exception) -> str:
    """Convert an exception into a human-readable string."""

    if ex is None:
        return "None"
    elif isinstance(ex, DevFailed):
        return "{klass}: {args}".format(
            klass=ex.__class__.__name__, args=(ex.args[-1].desc if ex.args else "")
        )
    else:
        return "{klass}: {args}".format(
            klass=ex.__class__.__name__, args=": ".join([str(arg) for arg in ex.args])
        )


def get_current_device() -> Device | None:
    """Return the tango Device we're currently executing for, or None if it can't be detected.

    This is derived by traversing the stack and find a Device as 'self' or 'self.device'. In some cases,
    this fails, for example if a separate Thread is started for a certain Device."""

    for frame, _lineno in traceback.walk_stack(f=None):
        _self = frame.f_locals.get("self")

        if _self is not None:
            # return self if it is a Device
            if isinstance(_self, Device):
                return _self
            # return self.device if it is a Device
            try:
                if hasattr(_self, "device"):
                    if isinstance(_self.device, Device):
                        return _self.device
            except SmiError:  # PySMI throws this on hasattr
                pass

    return None


class TangoLoggingHandler(logging.Handler):
    LEVEL_TO_DEVICE_STREAM = {
        logging.DEBUG: Device.debug_stream,
        logging.INFO: Device.info_stream,
        logging.WARN: Device.warn_stream,
        logging.ERROR: Device.error_stream,
        logging.FATAL: Device.fatal_stream,
    }

    def emit(self, record):
        try:
            if record.device is None:
                # log record is not related to any device
                return
        except AttributeError:
            # log record is not annotated with a device
            return

        # determine which log stream to use
        stream = self.LEVEL_TO_DEVICE_STREAM[record.levelno]

        # send the log message to Tango
        try:
            record_msg = record.msg % record.args
            stream(record.device, record.msg, *record.args)
        except TypeError:
            # Tango's logger barfs on mal-formed log lines, f.e. if msg % args is not possible
            record_msg = f"{record.msg} {record.args}".replace("%", "%%")
            stream(record.device, record_msg)

        self.flush()


class LogSuppressErrorSpam(logging.Formatter):
    """
    Suppress specific errors from spamming the logs, by only letting them through periodically.
    """

    def __init__(self, error_suppress_interval=3600):
        """Suppress subsequent errors for `error_suppress_interval` seconds."""

        super().__init__()

        # last time we logged an error
        self.last_error_log_time = 0

        # suppression interval at which we report errors
        self.error_suppress_interval = error_suppress_interval

    def is_error_to_suppress(self, record):
        # Errors occuring by not being able to connect to the log processing container, f.e. because it is down.
        return (
            record.name == "LogProcessingWorker"
            and record.msg == "An error occurred while sending events: %s"
        )

    def filter(self, record):
        if self.is_error_to_suppress(record):
            # filter out error if it occurred within our suppression interval
            now = time.time()

            if now - self.last_error_log_time < self.error_suppress_interval:
                return False

            self.last_error_log_time = now

        return True


class LogAnnotator(logging.Formatter):
    """Annotates log records with:

    record.device: the Tango Device that is executing."""

    def filter(self, record):
        # annotate record with currently executing Tango device, if any
        record.device = get_current_device()

        # annotate record with the current software version
        record.software_version = version

        # we just annotate, we don't filter
        return True


def configure_logger(logger: logging.Logger = None, log_extra=None, debug=False):
    """
    Configure the given logger (or root if None) to:
      - send logs to Loki through Logstash
      - send logs to Tango
      - send logs to stdout
    """

    # NOTE: We have to attach filters to handlers, instead to this logger,
    # in order to have the filters be applied to descendent loggers.

    if logger is None:
        logger = logging.getLogger()

    # By default we want to know everything
    logger.setLevel(logging.DEBUG)

    # remove spam from asyncio
    logging.getLogger("asyncio").setLevel(logging.WARN)

    # remove spam from the OPC-UA client connection
    logging.getLogger("asyncua").setLevel(logging.WARN)

    # don't spam errors for git, as we use it in our log handler, which would result in an infinite loop
    logging.getLogger("git").setLevel(logging.ERROR)

    # don't spam debug messages when fetching URLs
    logging.getLogger("urllib3").setLevel(logging.INFO)

    # don't spam error messages when having connection troubles
    logging.getLogger("LogProcessingWorker").setLevel(logging.CRITICAL)

    # for now, also log to stderr
    # Set up logging in a way that it can be understood by a human reader, be
    # easily grep'ed, be parsed with a couple of shell commands and
    # easily fed into an Kibana/Elastic search system.
    handler = logging.StreamHandler()

    # Always also log the hostname because it makes the origin of the log clear.
    hostname = socket.gethostname()

    formatter = Logfmter(
        keys=["ts", "level", "file", "line", "function"],
        mapping={
            "ts": "asctime",
            "level": "levelname",
            "function": "funcName",
            "file": "filename",
            "line": "lineno",
        },
    )

    handler.setFormatter(formatter)
    handler.addFilter(LogSuppressErrorSpam())
    handler.addFilter(LogAnnotator())

    logger.addHandler(handler)

    # Don't log to Tango to reduce log spam
    """
    # Log to Tango
    try:
        handler = TangoLoggingHandler()
        handler.addFilter(LogSuppressErrorSpam())
        handler.addFilter(LogAnnotator())
        logger.addHandler(handler)
    except Exception:
        logger.exception("Cannot forward logs to Tango.")
    """

    return logger


def device_logging_to_python():
    """Decorator. Call this on a Tango Device instance or class to have your Tango Device log to python instead."""

    def inner(cls):
        # we'll be doing very weird things if this class isnt
        if not issubclass(cls, Device):
            raise ValueError(
                "device_logging_to_python decorator is to be used on Tango Device classes only."
            )

        # Monkey patch the python logger to replace the tango logger
        logger = logging.getLogger()

        # Take a python log function and convert it into a log function
        # that can be plugged into tango.
        def python2tango_logfunc(logfunc):
            def convert(cls, *args, source=None, **kwargs):
                # Tango provides extra parameters that python log functions
                # do not expect: cls, source.

                # Also increase the stacklevel to skip this
                # wrapper when reporting the log's source location.
                return logfunc(*args, **kwargs, stacklevel=2)

            return convert

        cls.debug_stream = python2tango_logfunc(logger.debug)
        cls.info_stream = python2tango_logfunc(logger.info)
        cls.warn_stream = python2tango_logfunc(logger.warning)
        cls.warning_stream = python2tango_logfunc(logger.warning)
        cls.error_stream = python2tango_logfunc(logger.error)
        cls.fatal_stream = python2tango_logfunc(logger.fatal)
        cls.critical_stream = python2tango_logfunc(logger.critical)

        return cls

    return inner
