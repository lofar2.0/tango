#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import logging
import os
import tempfile
from typing import Dict
from urllib.parse import urlparse

import numpy
from lofar_station_client.file_access import member, attribute, read_hdf5
from minio import Minio
from tango import DeviceProxy

from tangostationcontrol.common import consul
from tangostationcontrol.common.constants import (
    N_subbands,
    N_pol,
    N_pn,
    A_pn,
)
from tangostationcontrol.common.sdp import complex_to_weights, are_subbands_decreasing
from tangostationcontrol.common.type_checking import device_name_matches
from lofar_station_client.common import CaseInsensitiveString

logger = logging.getLogger()


class CalibrationData:
    """Class that models calibration data values"""

    x: numpy.ndarray = member()
    y: numpy.ndarray = member()


class CalibrationTable:
    """Class that models calibration table entries"""

    observation_station: str = attribute()
    observation_station_version: str = attribute()
    observation_mode: str = attribute()
    observation_source: str = attribute()
    observation_date: str = attribute()
    calibration_version: int = attribute()
    calibration_name: str = attribute()
    calibration_date: str = attribute()
    antennas: Dict[str, CalibrationData] = member()


class CalibrationManager:
    """Manager class for main calibration operations"""

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, new_url):
        self._url = new_url

    def __init__(self, url: str, station_name: str):
        self._url = url
        self._station_name = station_name
        self._tmp_dir = tempfile.TemporaryDirectory(ignore_cleanup_errors=True)
        self.bucket_name = "caltables"
        self.prefix = self._station_name
        self._init_minio()
        self.sync_calibration_tables()

    def _init_minio(self):
        result = urlparse(self._url)
        bucket_name, prefix, *_ = result.path[1:].split("/", 1) + [""]
        if len(prefix) > 0:
            self.prefix = "/".join([prefix.rstrip("/"), self._station_name])
        if len(bucket_name) > 0:
            self.bucket_name = bucket_name

        logger.info(f"Derived {self.prefix=} {self.bucket_name=} from {self._url=}")

        service: consul.Service = next(consul.lookup_service(result.netloc))
        logger.info("Use service %s:%s", service.host, service.port)
        self._storage = Minio(
            f"{service.addr}:{service.port}",
            access_key=os.getenv("MINIO_ROOT_USER"),
            secret_key=os.getenv("MINIO_ROOT_PASSWORD"),
            secure=result.scheme == "https",
        )

    def sync_calibration_tables(self):
        """Download calibration tables from Minio server"""
        logger.debug(
            "Sync calibration tables from bucket %s with prefix %s/",
            self.bucket_name,
            self.prefix,
        )
        objects = self._storage.list_objects(self.bucket_name, prefix=f"{self.prefix}/")
        for obj in objects:
            filename = os.path.basename(obj.object_name)
            try:
                self._storage.fget_object(
                    self.bucket_name,
                    obj.object_name,
                    os.path.join(self._tmp_dir.name, filename),
                )
            except Exception as ex:
                raise IOError(
                    f"Failed to download {self.bucket=} {obj.object_name=}"
                ) from ex

            logger.info("Downloaded %s from %s", filename, obj.object_name)

    @staticmethod
    def _band_to_reference_frequency(is_hba, rcu_band):
        if is_hba:
            match rcu_band:
                case 1:
                    return "200"
                case 2:
                    return "150"
                case 4:
                    return "250"
        return "50"

    def calibrate_subband_weights(self, antenna_field: DeviceProxy, sdp: DeviceProxy):
        # -----------------------------------------------------------
        #   Compute calibration of subband weights for the remaining
        #   delay and loss corrections.
        # -----------------------------------------------------------

        # Mapping [antenna] -> [fpga][input]
        antenna_to_sdp_mapping = antenna_field.Antenna_to_SDP_Mapping_R

        # read-modify-write on [fpga][(input, polarisation)]
        fpga_subband_weights = sdp.FPGA_subband_weights_RW.reshape(
            (N_pn, A_pn, N_pol, N_subbands)
        )

        antennafield_member = antenna_field.name().split("/")[2]
        antenna_type = antenna_field.antenna_type_R
        rcu_bands = antenna_field.RCU_band_select_RW
        antenna_names = antenna_field.Antenna_Names_R
        fpga_spectral_inversion = sdp.FPGA_spectral_inversion_R
        nyquist_zones = sdp.nyquist_zone_RW

        is_hba = CaseInsensitiveString(antenna_type) == CaseInsensitiveString("HBA")

        def get_antenna_calibration(antenna_name: str, rcu_band: int):
            """Return the calibration values for the given antenna and RCU band."""

            calibration_filename = os.path.join(
                self._tmp_dir.name,
                f"CalTable-{self._station_name}-{antenna_type}"
                f"-{self._band_to_reference_frequency(is_hba, rcu_band)}MHz.h5",
            )
            logging.debug(f"Load calibration file {calibration_filename}")
            f = read_hdf5(calibration_filename, CalibrationTable)
            with f as table:
                # Retrieve data and convert them in the correct Tango attr shape
                if not device_name_matches(
                    table.observation_station, self._station_name
                ):
                    raise ValueError(
                        f"Expected calibration table for station {self._station_name}, but got \
                        {table.observation_station} in calibration file {calibration_filename}"
                    )

                try:
                    data = table.antennas[antenna_name]
                    f.load(data)
                    return data
                except KeyError as exc:
                    raise ValueError(
                        f"Could not find calibration values for field {antennafield_member} \
                        antenna {antenna_name} (index {antenna_nr}) in calibration file \
                        {calibration_filename}"
                    ) from exc

        for antenna_nr, (rcu_band_x, rcu_band_y) in enumerate(rcu_bands):
            fpga_nr, input_nr = antenna_to_sdp_mapping[antenna_nr]
            antenna_name = antenna_names[antenna_nr]

            if input_nr == -1:
                # skip unconnected antennas
                continue

            # set weights, converted from complex to packed uint32
            try:
                weights_x = complex_to_weights(
                    get_antenna_calibration(antenna_name, rcu_band_x).x
                )
                weights_y = complex_to_weights(
                    get_antenna_calibration(antenna_name, rcu_band_y).y
                )
            except Exception:
                logger.error(f"Issue {antenna_name}")
                raise

            # check if subband frequencies are decreasing
            # X pol
            if are_subbands_decreasing(
                nyquist_zones[fpga_nr][input_nr],
                fpga_spectral_inversion[fpga_nr][input_nr],
            ):
                fpga_subband_weights[fpga_nr, input_nr, 0] = numpy.flip(weights_x)
            else:
                fpga_subband_weights[fpga_nr, input_nr, 0] = weights_x
            # Y pol
            if are_subbands_decreasing(
                nyquist_zones[fpga_nr][input_nr * N_pol],
                fpga_spectral_inversion[fpga_nr][input_nr * N_pol],
            ):
                fpga_subband_weights[fpga_nr, input_nr, 1] = numpy.flip(weights_y)
            else:
                fpga_subband_weights[fpga_nr, input_nr, 1] = weights_y

        # TODO(L2SS-1312): This should use atomic_read_modify_write
        sdp.FPGA_subband_weights_RW = fpga_subband_weights.reshape(
            N_pn, A_pn * N_pol * N_subbands
        )


def delay_compensation(delays_seconds: numpy.ndarray, clock: int) -> numpy.ndarray:
    """Return the delay compensation required to line up
    signals that are delayed by "delays" seconds. The returned values
    are the delay to apply, in samples (coarse) and remaining seconds
    (fine), as a tuple (samples, remainder).

    The coarse delay is to be applied in sdp.FPGA_signal_input_delays_RW,
    the fine delay is to be incorporated into sdp.FPGA_subband_weights_RW.

    Note that the remainder is -1/2 * sample <= remainder <= 1/2 * sample.

    Applying this correction equalises the signal across all inputs to be delayed
    max(round(delay_samples)) samples, instead of their value in delay_seconds.
    So we do _not exactly_ delay all signals to match the longest.
    """

    # NB: signal_* are the amount of delay the signal obtained in our processing
    #     chain, while input_* are the amount of (negative) delay to apply
    #     to compensate.

    # compute the correction, in samples
    signal_delays_samples = delays_seconds * clock

    # correct for the coarse delay by delaying the other signals to line up
    # we cannot configure a negative number of samples, so we must delay
    # all of them sufficiently as well.
    #
    # This introduces a constant shift in timing for all samples,
    # as we shift all of them to obtain a non-negative delay.
    input_delays_samples = numpy.round(
        max(signal_delays_samples) - signal_delays_samples
    ).astype(numpy.int32)

    return input_delays_samples


def calibrate_input_samples_delay(
    antenna_field: DeviceProxy, sdpfirmware: DeviceProxy, sdp: DeviceProxy
):
    """SDP input delay calibration"""
    # Mapping [antenna] -> [fpga][input]
    antenna_to_sdp_mapping = antenna_field.Antenna_to_SDP_Mapping_R

    # -----------------------------------------------------------
    #   Set coarse delay compensation by delaying the samples.
    # -----------------------------------------------------------

    # The delay to apply, in samples [antenna]
    # Correct for signal delays in the cables
    signal_delay_seconds = antenna_field.Antenna_Cables_Delay_R

    # compute the required compensation
    clock = sdpfirmware.clock_RW
    input_samples_delay = delay_compensation(signal_delay_seconds, clock)

    # read-modify-write on [fpga][(input, polarisation)]
    fpga_signal_input_samples_delay = sdp.FPGA_signal_input_samples_delay_RW

    for antenna_nr, (fpga_nr, input_nr) in enumerate(antenna_to_sdp_mapping):
        if input_nr == -1:
            # skip unconnected antennas
            continue

        # set for X polarisation
        fpga_signal_input_samples_delay[fpga_nr, input_nr * N_pol + 0] = (
            input_samples_delay[antenna_nr]
        )
        # set for Y polarisation
        fpga_signal_input_samples_delay[fpga_nr, input_nr * N_pol + 1] = (
            input_samples_delay[antenna_nr]
        )

    # TODO(L2SS-1312): This should use atomic_read_modify_write
    sdp.FPGA_signal_input_samples_delay_RW = fpga_signal_input_samples_delay


# RCU calibration


def calibrate_RCU_attenuator_dB(antenna_field: DeviceProxy):
    # -----------------------------------------------------------
    #   Set signal-input attenuation to compensate for
    #   differences in cable length.
    # -----------------------------------------------------------

    # Correct for signal loss in the cables
    signal_delay_loss = antenna_field.Antenna_Cables_Loss_R

    # return coarse attenuation to apply (weakest signal
    # gets 0 attenuation).
    rcu_attenuator_db = loss_compensation(signal_delay_loss)

    # add field-wide attenuation
    rcu_attenuator_db += antenna_field.Field_Attenuation_R

    # apply on antenna field
    antenna_field.RCU_attenuator_dB_RW = rcu_attenuator_db.astype(numpy.int64)


def loss_compensation(losses_dB: numpy.ndarray) -> numpy.ndarray:
    """Return the attenuation required to line up
    signals that are dampened by "lossed_dB" decibel.

    Returned are the signal attenuations in whole dBs (coarse).

    The coarse attenuation is to be applied in recv.RCU_attenuation_dB_RW,
    the remainder is to be incorporated into sdp.FPGA_subband_weights_RW.

    Applying this correction equalises the signal across the inputs
    to be dampened max(round(losses_dB)) instead of their value
    in losses_dB. So we do _not_ fully dampen towards the weakest signal.
    """

    # correct for the coarse loss by dampening the signals to line up.
    input_attenuation_integer_dB = numpy.round(numpy.max(losses_dB) - losses_dB).astype(
        numpy.int32
    )

    return input_attenuation_integer_dB
