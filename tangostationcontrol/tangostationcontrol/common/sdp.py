# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from ctypes import c_short

import numpy
from tangostationcontrol.common.constants import (
    N_subbands,
    N_subband_res,
    VALUES_PER_COMPLEX,
    SDP_UNIT_WEIGHT,
)


def are_subbands_decreasing(nyquist_zone: int, spectral_inversion: bool) -> bool:
    """Check if subsequents subbands decrease in frequency.
    This is True for odd NyQuist zones and spectral inversion not configured."""
    return bool(spectral_inversion) != bool(nyquist_zone % 2)


def subband_frequencies(
    subbands: numpy.ndarray, clock: int, nyquist_zone: int, spectral_inversion: bool
) -> numpy.ndarray:
    """Obtain the frequencies of multiple subbands,
    given a clock and a nyquist zone for each subband."""

    subband_width = clock / N_subband_res
    base_subband = nyquist_zone * N_subbands

    # odd NyQuist zones decrease in frequency for subsequent subbands,
    # and spectral inversion reverses this.
    if are_subbands_decreasing(nyquist_zone, spectral_inversion):
        return (N_subbands - subbands + base_subband) * subband_width
    return (subbands + base_subband) * subband_width


def subband_frequency(
    subband: int, clock: float, nyquist_zone: int, spectral_inversion: bool
) -> int:
    """Obtain the frequencies of a subband, given a clock and a nyquist zone."""

    # just use the interface for multiple subbands to avoid code duplication
    return subband_frequencies(
        numpy.array(subband), clock, nyquist_zone, spectral_inversion
    ).item()


def phases_to_weights(
    phases: numpy.ndarray,
    unit_weight: float = SDP_UNIT_WEIGHT,
    amplitudes: numpy.ndarray = None,
) -> numpy.ndarray:
    """Convert phases (in radians) into FPGA weights (complex numbers packed into uint32)."""

    # The FPGA accepts weights as a 16-bit (imag,real) complex pair packed into an uint32.

    # flatten array and restore its shape later, which makes running over all elements a lot easier
    orig_shape = phases.shape
    phases = phases.flatten()

    # Convert array values in complex numbers
    real = unit_weight * numpy.cos(phases)
    imag = unit_weight * numpy.sin(phases)

    if amplitudes is not None:
        real *= amplitudes
        imag *= amplitudes

    # Interleave into (real, imag) pairs, and store as int16
    # see also https://stackoverflow.com/questions/5347065/interweaving-two-numpy-arrays/5347492
    # Round to nearest integer instead of rounding down
    real_imag = numpy.empty(phases.size * VALUES_PER_COMPLEX, dtype=numpy.int16)
    real_imag[0::2] = numpy.round(real)
    real_imag[1::2] = numpy.round(imag)

    # Cast each (real, imag) pair into an uint32, which brings the array size
    # back to the original.
    weights = real_imag.view(numpy.uint32)

    return weights.reshape(orig_shape)


def real_imag_to_weights(
    real_imag_pairs: numpy.ndarray, unit: int = SDP_UNIT_WEIGHT
) -> numpy.ndarray:
    """Convert complex values (as (real, imag) pairs) into FPGA weights
    (complex numbers packed into uint32)."""

    # The FPGA accepts weights as a 16-bit (imag,real) complex pair packed into an uint32.

    # Interleave into (real, imag) pairs, and store as int16
    # Round to nearest integer instead of rounding down
    real_imag = numpy.round(real_imag_pairs * unit).astype(numpy.int16)

    # Cast each (real, imag) pair into an uint32
    weights = real_imag.view(numpy.uint32)

    return weights


def weight_to_complex(weight: numpy.uint32, unit: int = SDP_UNIT_WEIGHT) -> complex:
    """Unpack an FPGA weight (uint32) into a complex number.

    unit: the weight value representing a weight of 1.0."""

    # A weight is a (real, imag) pair, stored as int16 packed into an uint32 value

    # obtain the upper 16 bits, but make sure we interpret it as c_short to get the correct sign
    imag = c_short(weight >> 16).value
    # isolate the lower 16 bits, and interpret it as c_short to get the correct sign
    real = c_short(weight & 0xFFFF).value

    return (real + 1j * imag) / unit


def weights_to_complex(
    weights: numpy.ndarray, unit: int = SDP_UNIT_WEIGHT
) -> numpy.ndarray:
    """Unpack FPGA weights (uint32) into complex numbers.

    unit: the weight value representing a weight of 1.0."""

    return numpy.vectorize(lambda w: weight_to_complex(w, unit))(weights)


def complex_to_weights(
    complex_weights: numpy.ndarray, unit: int = SDP_UNIT_WEIGHT
) -> numpy.ndarray:
    """Pack an array of complex numbers in FPGA weights (uint32).

    unit: the weight value representing a weight of 1.0."""

    return real_imag_to_weights(
        numpy.array(
            list(zip(complex_weights.flatten().real, complex_weights.flatten().imag))
        ),
        unit,
    ).reshape(complex_weights.shape)


def complex_to_weight(
    complex_weight: complex, unit: int = SDP_UNIT_WEIGHT
) -> numpy.uint32:
    """Pack a complex number in an FPGA weight (uint32).

    unit: the weight value representing a weight of 1.0."""

    return complex_to_weights(numpy.array([complex_weight]), unit)[0]
