import tango
from threading import Thread


class OmniThread(Thread):
    """Threads that interact with Tango will need to be registered in CORBA as OmniORB threads,
    see https://pytango.readthedocs.io/en/stable/howto.html#multithreading-clients-and-servers
    """

    def run(self):
        with tango.EnsureOmniThread():
            super().run()
