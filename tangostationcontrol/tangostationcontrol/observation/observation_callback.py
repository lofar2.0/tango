# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Callable

callback_type = Callable[[int], None]


def default_callback(observation_id: int):
    pass
