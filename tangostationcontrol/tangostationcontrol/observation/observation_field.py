# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

from tango import DevFailed, DevState, Except, Util, DeviceProxy
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.configuration.observation_field_settings import (
    ObservationFieldSettings,
)

logger = logging.getLogger()


class ObservationField(object):
    """Manage specific antenna field for a particular observation id"""

    @property
    def proxy(self) -> DeviceProxy:
        return self._device_proxy

    @property
    def observation_id(self) -> int:
        return self._parameters.observation_id

    @property
    def antenna_field(self) -> str:
        return self._parameters.antenna_field

    @property
    def class_name(self) -> str:
        from tangostationcontrol.devices.observation_field import ObservationField

        return ObservationField.__name__

    @property
    def device_name(self) -> str:
        """Device names property creating unique device name"""
        return (
            f"{self._tango_domain}/{self.class_name}/{self.observation_id}-"
            f"{self.antenna_field}"
        )

    def __init__(self, tango_domain, parameters: ObservationFieldSettings):
        self._device_proxy: DeviceProxy | None = None
        self._event_id: int | None = None
        self._parameters: ObservationFieldSettings = parameters
        self._tango_domain: str = tango_domain

        # The pyTango.Util class is a singleton and every DS can only
        # have one instance of it.
        self._tango_util: Util = Util.instance()

    def create_observation_field_device(self):
        """Instantiate an Observation Device

        :raises DevFailed: when calling :py:func:`create_device` fails
        """
        logger.info("Create device: %s", self.device_name)
        try:
            # Create the Observation device and instantiate it.
            self._tango_util.create_device(self.class_name, f"{self.device_name}")
        except DevFailed as ex:
            logger.exception(ex)
            error_string = (
                f"Cannot create the ObservationField device instance "
                f"{self.device_name} for ID={self.observation_id} and "
                f"field={self.antenna_field} "
            )
            Except.re_throw_exception(ex, "DevFailed", error_string, __name__)

    def destroy_observation_field_device(self):
        try:
            self._tango_util.delete_device(self.class_name, self.device_name)
        except DevFailed:
            logger.exception(
                f"Could not delete device {self.device_name} of class "
                f"{self.class_name} from Tango DB"
            )

    def initialise_observation_field(self):
        """

        raises DevFailed: when an operation on the device proxy fails
        """
        # Instantiate a dynamic Tango Device "Observation".
        self._device_proxy = create_device_proxy(self.device_name, write_access=True)

        # Initialise generic properties
        self.proxy.put_property({"Control_Children": [], "Power_Children": []})

        # Configure the dynamic device its attribute for the observation
        # parameters.
        self.proxy.observation_field_settings_RW = self._parameters.to_json()

        # Take the Observation device through the motions.  Pass the
        # entire JSON set of parameters so that it can pull from it what it
        # needs.
        self.proxy.Initialise()

        logger.info(
            "Successfully initialised an observation field with ID=%s for antenna field=%s",
            self.observation_id,
            self.antenna_field,
        )

    def start(self):
        """

        raises DevFailed: when executing the :py:func:`On` command fails
        """
        self.proxy.On()

    def is_running(self) -> bool:
        """

        :raises DevFailed: when proxy state could not be retrieved
        """
        return self.proxy and self.proxy.state() == DevState.ON

    def stop(self):
        # Check if the device has not terminated itself in the meanwhile.
        try:
            self.proxy.ping()
        except DevFailed:
            logger.error(
                f"Observation field device for ID={self.observation_id} and antenna"
                f"field={self.antenna_field} unexpectedly disappeared."
            )
        else:
            # Tell the ObservationField device to stop the running
            # observation for the given antenna field. This is a synchronous call and
            # the clean-up does not take long.
            self.proxy.Off()

        # Finally remove the device object from the Tango DB.
        self.destroy_observation_field_device()
