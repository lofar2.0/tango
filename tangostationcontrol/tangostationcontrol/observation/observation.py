# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from typing import List
import time

from tango import DeviceProxy, Except
from tango import DevFailed
from tango import DevState
from tangostationcontrol.configuration import ObservationSettings
from tangostationcontrol.configuration import ObservationFieldSettings
from tangostationcontrol.observation.observation_field import ObservationField
from tangostationcontrol.observation.observation_callback import callback_type
from tangostationcontrol.observation.observation_callback import default_callback

logger = logging.getLogger()


class Observation(object):
    """Manage one or more ObservationField devices depending on observation"""

    def is_partially_running(self) -> bool:
        """

        :raises DevFailed: when calling :py:func:`is_running` failed
        """
        return any([field.is_running for field in self._observation_fields])

    def is_running(self) -> bool:
        """

        :raises DevFailed: when calling :py:fund:`is_running` failed
        """
        return all([field.is_running() for field in self._observation_fields])

    @property
    def observation_id(self) -> int:
        return self._parameters.antenna_fields[0].observation_id

    @property
    def antenna_fields(self) -> list[str]:
        return [field.antenna_field for field in self._parameters.antenna_fields]

    def __init__(
        self,
        tango_domain,
        parameters: ObservationSettings,
        start_callback: callback_type = None,
        stop_callback: callback_type = None,
    ):
        self._tango_domain: str = tango_domain
        self._parameters: ObservationSettings = parameters

        self._started_antenna_fields = 0
        self._start_callback = start_callback or default_callback

        self._stopped_antenna_fields = 0
        self._stop_callback = stop_callback or default_callback

        for antenna_field in self._parameters.antenna_fields:
            if antenna_field.observation_id != self.observation_id:
                raise RuntimeError(
                    "Observation configured for different observation IDs across "
                    "antenna fields"
                )

        self._observation_fields: List[ObservationField] = []

    def _update_observation_state(self, device: DeviceProxy):
        """Start / stop the observation managed by the given ObservationField device.

        :raises DevFailed: if either calling :py:func:`~._stop_antenna_field`,
                           :py:func:`~._start_antenna_field` or reading attribute fails
        """

        # Get the antenna field name
        antenna_field = device.antenna_field_R

        # Get the start/stop times from the sending device
        obs_start_time = device.start_time_R
        obs_stop_time = device.stop_time_R

        # Get how much earlier we have to start
        obs_lead_time = device.lead_time_R

        # Obtain the current time ONCE to avoid race conditions
        now = time.time()

        # Manage state transitions
        if now > obs_stop_time:
            # Stop observation
            logger.info("Time: %f now surpassed %f stopping", now, obs_stop_time)
            self._stop_antenna_field(antenna_field)
            if self._stopped_antenna_fields is len(self.antenna_fields):
                logger.debug(
                    "All antenna fields stopped, calling observation controller for "
                    "removal"
                )
                self._stop_callback(self.observation_id)
        elif (
            now >= obs_start_time - obs_lead_time and device.state() == DevState.STANDBY
        ):
            logger.info(
                "Time: %f now surpassed %f starting",
                now,
                obs_start_time - obs_lead_time,
            )
            # Start observation
            self._start_antenna_field(antenna_field)

            if self._started_antenna_fields is len(self.antenna_fields):
                logger.debug("All antenna fields started, triggering start callback")
                self._start_callback(self.observation_id)

    def create_devices(self):
        """Call create_observation_field_device per ObservationField

        :raises DevFailed: Raised when calling :py:func:`~._create_device` fails
        """
        for observation_field in self._parameters.antenna_fields:
            self._create_device(observation_field)

    def destroy_devices(self):
        """Destroy each of the registered ObservationField device"""

        for observation_field in self._observation_fields:
            observation_field.destroy_observation_field_device()

    def _create_device(self, parameters: ObservationFieldSettings):
        """Create singular ObservationField device with ObservationFieldSettings

        :raises DevFailed: Raised when :py:func:`create_observation_field_device` fails
        """
        f = ObservationField(self._tango_domain, parameters)
        f.create_observation_field_device()
        self._observation_fields.append(f)

    def initialise_observation(self):
        """Call initialise_observation per ObservationField

        :raises DevFailed: Raised when :py:func:`initialise_observation_field` fails
        """
        for observation_field in self._observation_fields:
            try:
                observation_field.initialise_observation_field()
            except DevFailed as ex:
                error_string = (
                    f"Failed to initialise observation: "
                    f"{observation_field.observation_id} due to error for antenna "
                    f"field: {observation_field.antenna_field}"
                )
                Except.re_throw_exception(ex, "DevFailed", error_string, __name__)

    def update(self):
        """Call callback per ObservationField device proxy

        :raises DevFailed: If calling :py:func:`~._update_observation_state` fails
        """

        for observation_field in self._observation_fields:
            self._update_observation_state(observation_field.proxy)

    def _start_antenna_field(self, antenna_field: str):
        """

        raises DevFailed: when executing start for the specified observation field fails
        """
        for observation_field in self._observation_fields:
            if observation_field.antenna_field == antenna_field:
                self._started_antenna_fields += 1
                observation_field.start()

    def _stop_antenna_field(self, antenna_field: str):
        """

        raises DevFailed: when executing stop for the specified observation field fails
        """
        for observation_field in self._observation_fields:
            if observation_field.antenna_field == antenna_field:
                self._stopped_antenna_fields += 1
                observation_field.stop()

    def start(self):
        """Call start on each ObservationField

        raises DevFailed: when executing start for a specific observation field fails
        """
        for observation_field in self._observation_fields:
            observation_field.start()

    def stop(self):
        """Call stop on each ObservationField

        raises DevFailed: when executing stop for a specific observation field fails
        """
        for observation_field in self._observation_fields:
            observation_field.stop()
