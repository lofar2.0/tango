# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime
import logging
from typing import Callable, Type

from tango import DevFailed, Util, Database
from tangostationcontrol.configuration import ObservationSettings
from tangostationcontrol.observation.observation import Observation
from tangostationcontrol.observation.observation_callback import callback_type
from tangostationcontrol.observation.observation_callback import default_callback


logger = logging.getLogger()


class ObservationController(dict[int, Observation]):
    """A dictionary of observations. Actively manages the observation state transitions
    (start, stop)."""

    def __init__(
        self,
        tango_domain: str,
        start_callback: callback_type = None,
        stop_callback: callback_type = None,
    ):
        super().__init__()
        self._tango_util = Util.instance()
        self._tango_domain = tango_domain

        self._start_callback = default_callback
        if start_callback:
            self._start_callback = start_callback

        self._stop_callback = default_callback
        if stop_callback:
            self._stop_callback = stop_callback

    def _catch(self, func: Callable, exception: Type[Exception]):
        try:
            return func()
        except exception as e:
            logger.warning("Executing %s failed due to %s", func, e)
            return None

    def _internal_stop_callback(self, observation_id: int):
        """stop callback triggered by Observation class"""
        try:
            logger.debug("ObservationController running stop callback")
            self.pop(observation_id)
            self._stop_callback(observation_id)
        except Exception as e:
            logger.error(
                "Cleanup callback failed for observation %d due to %s",
                observation_id,
                e,
            )

    @property
    def running_observations(self) -> list[int]:
        return [
            obs_id
            for obs_id, observation in self.items()
            if self._catch(observation.is_running, DevFailed)
        ]

    @property
    def active_antenna_fields(self) -> list[str]:
        active_fields = []
        running_field_sets = [
            observation.antenna_fields
            for obs_id, observation in self.items()
            if self._catch(observation.is_running, DevFailed)
        ]
        for running_field_set in running_field_sets:
            active_fields.extend(running_field_set)
        return active_fields

    def add_observation(self, settings: ObservationSettings):
        """Create an Observation which will start 1 or more ObservationField devices

        :raises RuntimeError: if either settings contains multiple observation ids or if
                              the creation and configuration of devices failed
        """

        # Check further properties that cannot be validated through a JSON schema
        # TODO(Corne): Discuss do we want this?
        for observation_field_settings in settings.antenna_fields:
            if (
                datetime.fromisoformat(observation_field_settings.stop_time)
                <= datetime.now()
            ):
                raise ValueError(
                    "Cannot start observation "
                    f"{observation_field_settings.observation_id} because antenna "
                    f"field {observation_field_settings.antenna_field} is already "
                    f"past its stop time {observation_field_settings.stop_time}"
                )

        obs = Observation(
            tango_domain=self._tango_domain,
            parameters=settings,
            start_callback=self._start_callback,
            stop_callback=self._internal_stop_callback,
        )

        try:
            obs.create_devices()
            obs.initialise_observation()
            # Register this observation now that it has been successfully created
            self[obs.observation_id] = obs
            # Make sure the current state is accurate
            obs.update()
        except DevFailed as ex:
            # remove the id from our maintained list if present
            _ = self.pop(obs.observation_id, None)

            # Remove the devices again if creation, proxies or subscriptions fail.
            obs.destroy_devices()

            raise RuntimeError(
                f"Observation: {settings.antenna_fields[0].observation_id} failed, "
                "destroying devices..."
            ) from ex

    def start_observation(self, obs_id: int):
        """Start the observation with the given ID

        :raises DevFailed: if executing :py:func:`start` fails
        :raises KeyError: if observation id is unknown
        """
        try:
            observation = self[obs_id]
        except KeyError as _exc:
            raise KeyError(f"Unknown observation: {obs_id}")

        observation.start()

    def stop_observation_now(self, obs_id: int):
        """Stop the observation with the given ID

        :raises DevFailed: if executing :py:func:`stop` fails
        :raises KeyError: if observation id is unknown
        """
        try:
            observation = self.pop(obs_id)
        except KeyError as _exc:
            raise KeyError(f"Unknown observation: {obs_id}")

        observation.stop()

    def stop_all_observations_now(self):
        """Stop all observations (running or to be run)"""
        for obs_id in list(self):  # draw a copy as we modify the list
            try:
                self.stop_observation_now(obs_id)
            except DevFailed as ex:
                logger.exception(ex)

        self._destroy_all_observation_field_devices()

    @staticmethod
    def _destroy_all_observation_field_devices():
        """Prevent any lingering observation field devices, remove all from database"""

        # import here to avoid circular imports
        from tangostationcontrol.devices.observation_field import ObservationField

        db = Database()
        devices = db.get_device_exported_for_class(ObservationField.__name__)
        for device in devices:
            try:
                db.delete_device(device)
                logger.warning("Destroyed lingering device: %s", device)
            except Exception as ex:
                logger.exception(f"Failed to destroy ObservationField device: {ex}")

    def update_all_observations(self):
        """Check for status updates for all registered observations."""
        for obsid, observation in list(self.items()):
            try:
                observation.update()
            except Exception as ex:
                logger.exception(
                    f"Failed to update status for observation {obsid}: {ex}"
                )
