# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
import copy

from tangostationcontrol.configuration import (
    Dithering,
    ObservationFieldSettings,
    ObservationSettings,
    Pointing,
    Sap,
    HBA,
    SST,
    XST,
)

SETTINGS_TWO_FIELDS_CORE = ObservationSettings(
    "cs001",
    [
        ObservationFieldSettings(
            5,
            "2022-10-26T11:35:54.704150",
            "2022-10-27T11:35:54.704150",
            "HBA0",
            "ALL",
            "filter settings",
            [
                Sap([3, 2], Pointing(1.2, 2.1, "LMN")),
                Sap([1], Pointing(3.3, 4.4, "MOON")),
            ],
        ),
        ObservationFieldSettings(
            5,
            "2022-10-26T11:35:54.704150",
            "2022-10-27T11:35:54.704150",
            "LBA",
            "ALL",
            "filter settings",
            [
                Sap([3, 2], Pointing(1.2, 2.1, "LMN")),
                Sap([1], Pointing(3.3, 4.4, "MOON")),
            ],
        ),
    ],
)

SETTINGS_HBA_CORE_IMMEDIATE = ObservationSettings(
    "cs001",
    [
        ObservationFieldSettings(
            observation_id=12345,
            start_time=None,
            stop_time="2106-02-07T00:00:00",
            antenna_field="HBA0",
            antenna_set="ALL",
            filter="HBA_110_190",
            SAPs=[
                Sap([10, 20, 30], Pointing(0.0261799, 0, "J2000")),
            ],
            HBA=HBA(
                tile_beam=Pointing(0.0261799, 0, "J2000"),
                DAB_filter=True,
                element_selection="GENERIC_201512",
            ),
            dithering=Dithering(enabled=True, power=-10, frequency=123000000),
            SST=SST(subbands_calibrated=False),
            XST=XST(
                subbands=[1, 2, 3, 4, 5], subbands_step=5, integration_interval=1.0
            ),
        ),
    ],
)


def get_observation_settings_two_fields_core() -> ObservationSettings:
    """Get an observation with two antenna fields"""
    return copy.deepcopy(SETTINGS_TWO_FIELDS_CORE)


def get_observation_settings_hba_core_immediate() -> ObservationSettings:
    """Get an observation with one antenna field and no start time"""
    return copy.deepcopy(SETTINGS_HBA_CORE_IMMEDIATE)
