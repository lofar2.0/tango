# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
import logging
from tango import DevFailed
from tangostationcontrol.states.station_state import StationState
from tangostationcontrol.states.station_state_enum import StationStateEnum

__all__ = ["StandbyState"]
logger = logging.getLogger()


class StandbyState(StationState):
    """STANDBY station state"""

    def __init__(self, station_manager, power_hierarchy) -> None:
        super().__init__(StationStateEnum.STANDBY, station_manager, power_hierarchy)

    async def station_off(self, timeout: int):
        return self.disallowed_transition_error(StationStateEnum.OFF)

    async def station_standby(self, timeout: int):
        self.get_transition_current_state_error()
        return

    async def station_hibernate(self, timeout: int):
        """Transition STANDBY -> HIBERNATE"""
        from tangostationcontrol.states.hibernate import HibernateState

        target_state = StationStateEnum.HIBERNATE
        try:
            await self._transition(
                target_state,
                self._power_hierarchy.standby_to_hibernate,
                timeout=timeout,
            )
        except DevFailed as exc:
            self.generate_transition_error(target_state, logger, exc)
        self._station_manager.set_station_state(
            HibernateState(self._station_manager, self._power_hierarchy)
        )

    async def station_on(self, timeout: int):
        """Transition STANDBY -> ON"""
        from tangostationcontrol.states.on import OnState

        target_state = StationStateEnum.ON
        try:
            await self._transition(
                target_state, self._power_hierarchy.standby_to_on, timeout=timeout
            )
        except DevFailed as exc:
            self.generate_transition_error(target_state, logger, exc)
        self._station_manager.set_station_state(
            OnState(self._station_manager, self._power_hierarchy)
        )
