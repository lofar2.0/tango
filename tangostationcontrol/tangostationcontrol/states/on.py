# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
import logging
from tango import DevFailed
from tangostationcontrol.states.station_state import StationState
from tangostationcontrol.states.station_state_enum import StationStateEnum


__all__ = ["OnState"]
logger = logging.getLogger()


class OnState(StationState):
    """ON station state"""

    def __init__(self, station_manager, power_hierarchy) -> None:
        super().__init__(StationStateEnum.ON, station_manager, power_hierarchy)

    async def station_off(self, timeout: int):
        return self.disallowed_transition_error(StationStateEnum.ON)

    async def station_hibernate(self, timeout: int):
        return self.disallowed_transition_error(StationStateEnum.HIBERNATE)

    async def station_on(self, timeout: int):
        self.get_transition_current_state_error()
        return

    async def station_standby(self, timeout: int):
        """Transition ON -> STANDBY"""
        from tangostationcontrol.states.standby import StandbyState

        target_state = StationStateEnum.STANDBY
        try:
            await self._transition(
                target_state, self._power_hierarchy.on_to_standby, timeout=timeout
            )
        except DevFailed as exc:
            self.generate_transition_error(target_state, logger, exc)
        self._station_manager.set_station_state(
            StandbyState(self._station_manager, self._power_hierarchy)
        )
