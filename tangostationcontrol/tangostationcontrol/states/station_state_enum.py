# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from enum import IntEnum

__all__ = ["StationStateEnum"]


class StationStateEnum(IntEnum):
    """Station states enumeration"""

    OFF = 0
    HIBERNATE = 1
    STANDBY = 2
    ON = 3

    @staticmethod
    def keys():
        return [s.name for s in StationStateEnum]
