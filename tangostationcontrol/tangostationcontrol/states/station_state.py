# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
from abc import ABC, abstractmethod
from typing import Callable, Awaitable
from functools import wraps

import tango
from tango import DevState, DeviceProxy, Except, DevFailed

from tangostationcontrol.states.station_state_enum import StationStateEnum

__all__ = ["StationState"]
logger = logging.getLogger()


def run_if_device_on_in_station_state(
    target_state: StationStateEnum,
):
    """Decorator for a function func(device).
    It only executes the decorated function if the device should be turned ON in the target_state.
    Returns None otherwise."""

    def inner(func):
        @wraps(func)
        def wrapper(device: DeviceProxy, *args, **kwargs):
            def should_execute():
                available_in_state = device.available_in_power_state_R
                if available_in_state in StationStateEnum.keys():
                    return target_state == StationStateEnum[available_in_state]
                return False

            # execute function if we should in the configured state
            return func(device, *args, **kwargs) if should_execute() else None

        return wrapper

    return inner


class StationState(ABC):
    """Abstract class for station state"""

    def __init__(
        self, state: StationStateEnum, station_manager, power_hierarchy
    ) -> None:
        self._state = state
        self._station_manager = station_manager
        self._power_hierarchy = power_hierarchy
        super().__init__()

    @property
    def state(self) -> StationStateEnum:
        """Name of the current state"""
        return self._state

    @state.setter
    def state(self, state: StationStateEnum) -> None:
        self._state = state

    def generate_transition_error(
        self, target_state: StationStateEnum, log: logging.Logger, exc: DevFailed
    ):
        """Generate standardized error for state transistions"""
        error_string = f"Station {self._station_manager.Station_Name} \
            can not transition to {target_state.name} state. \
            Current state is {self.state.name}"
        log.exception(error_string)
        Except.re_throw_exception(
            exc, "DevFailed", error_string, str(self._station_manager)
        )

    def get_transition_current_state_error(self) -> None:
        """Log a warning message after performing a transition to
        the already current state"""
        logger.warning(
            "Requested to go to %s state, but am already in %s state.",
            self.state.name,
            self.state.name,
        )

    def disallowed_transition_error(
        self, target_state: StationStateEnum, raise_exception: bool = True
    ) -> None:
        """Raise an exception or log a warning if the requested transition
        is not allowed"""
        error_message = (
            f"State transition to {target_state.name} state "
            f"not allowed in current state: {self.state.name}"
        )
        if raise_exception:
            raise Exception(error_message)
        logger.warning(error_message)

    async def _transition(
        self,
        target_state: StationStateEnum,
        transition_func: Callable[[], Awaitable[None]],
        timeout: int,
    ):
        """Transition to a station state using `transition_func`.

        :param target_state : transition to be performed
        :param transition_func : function that implements the transition
        """

        # StationManager device must be in ON state
        if self._station_manager.get_state() != DevState.ON:
            raise Exception(
                f"Station Manager must be in ON state. "
                f"Current state is {self._station_manager.get_state()}"
            )

        logger.info(
            "Station %s requested to perform the %s -> %s Power Sequence",
            self._station_manager.Station_Name,
            self.state.name,
            target_state.name,
        )

        try:
            self._station_manager.requested_station_state = target_state
            self._station_manager.last_requested_transition_exceptions = (
                await self.power_transition(
                    target_state, transition_func, timeout=timeout
                )
            )
        except Exception as ex:
            # unsuppressed exception
            self._station_manager.last_requested_transition_exceptions = [(None, ex)]
            raise

        logger.info(
            "Station %s has correctly completed the %s -> %s Power Sequence",
            self._station_manager.Station_Name,
            self.state.name,
            target_state.name,
        )

    @abstractmethod
    async def station_off(self):
        """Abstract method for OFF transitions"""
        raise NotImplementedError

    @abstractmethod
    async def station_hibernate(self, timeout: int):
        """Abstract method for HIBERNATE transitions"""
        raise NotImplementedError

    @abstractmethod
    async def station_standby(self, timeout: int):
        """Abstract method for STANDBY transitions"""
        raise NotImplementedError

    @abstractmethod
    async def station_on(self, timeout: int):
        """Abstract method for ON transitions"""
        raise NotImplementedError

    async def power_transition(self, target_state, pwr_func, timeout: int = 60):
        """Trigger the Power Hierarchy device to perform the transition

        :param target_state : transition to be performed
        :param pwr_func : power hierarchy function that implements the transition
        :param timeout: Timeout in seconds
        """
        try:
            with tango.EnsureOmniThread():
                return await asyncio.wait_for(
                    asyncio.to_thread(pwr_func),
                    timeout=timeout,
                )
        except asyncio.TimeoutError as exc:
            raise TimeoutError(
                f"Transition to {target_state.name} state timed out."
            ) from exc
