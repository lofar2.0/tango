#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import datetime
import logging
from functools import lru_cache
from concurrent.futures import ProcessPoolExecutor
from typing import TypedDict

import casacore.measures
import casacore.tables
import numpy
import tango

"""
The 'measures' tables contain the ephemerides and geodetic calibrations
as input for its time and space calculations. These tables are externally
available at ftp://ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar.

Casacore is expected to be configured to look in /opt/IERS for
its 'measures' tables, through setting this in the ~/.casarc file as:

    measures.directory: /opt/IERS

This can be verified by running the 'findmeastable' utility, which
is part of the 'casacore-tools' package.

The measures
"""

logger = logging.getLogger()

# Where to store the measures table sets
IERS_ROOTDIR = "/opt/IERS"


def get_IERS_timestamp() -> datetime.datetime:
    """Return the date of the currently installed IERS tables."""

    with casacore.tables.table(f"{IERS_ROOTDIR}/geodetic/TAI_UTC") as t:
        return datetime.datetime.strptime(t.VS_DATE, "%Y/%m/%d/%H:%M").replace(
            tzinfo=datetime.timezone.utc
        )


class CasacoreQuantum(TypedDict):
    """A casacore::Quantum object as returned by casacore.measures."""

    unit: str
    value: float


class CasacoreMDirection(TypedDict):
    """A casacore::MDirection object as returned by casacore.measures."""

    type: str
    refer: str
    m1: CasacoreQuantum
    m2: CasacoreQuantum


@lru_cache
def pointing_to_str(pointing: tuple[str, str, str]) -> str:
    """Convert a pointing tuple (direction_type, angle0, angle1) into a coincise string to display."""

    direction_type, angle0, angle1 = pointing

    def display_angle(angle: str) -> str:
        """Cut off lengthy precision, if possible."""

        if angle.endswith("rad"):
            # Radians, round to 5 digits
            try:
                angle_amount = float(angle[:-3])
            except ValueError:
                return angle

            return "{0:.5}rad".format(angle_amount)

        if angle.endswith("deg"):
            # Degrees, round to 3 digits
            try:
                angle_amount = float(angle[:-3])
            except ValueError:
                return angle

            return "{0:.3}deg".format(angle_amount)

        return angle

    return "{direction_type} ({angle0}, {angle1})".format(
        direction_type=direction_type,
        angle0=display_angle(angle0),
        angle1=display_angle(angle1),
    )


@lru_cache
def is_valid_pointing(pointing: tuple[str, str, str]) -> bool:
    """Check validity of the direction measure"""

    try:
        if pointing[0] == "None":
            # uninitialised direction
            return True

        measure = casacore.measures.measures()

        # if this raises, the direction is invalid
        _ = measure.direction(*pointing)

        return True
    except (RuntimeError, TypeError, KeyError, IndexError) as e:
        return False


def subtract(a, b) -> numpy.ndarray:
    return numpy.array([x - y for x, y in zip(a, b)])


class Delays:
    # process-local casacore.measures.measures instance
    measure = None

    @staticmethod
    def _init_process():
        """Initialise the process that will be used to query measures."""

        # cleanup any Tango and CORBA state for this process,
        # see https://pytango.readthedocs.io/en/latest/how-to/multiprocessing.html#using-clients-with-multiprocessing
        tango.ApiUtil.cleanup()

        # create a dedicated Measures object for this process
        Delays.measure = casacore.measures.measures()

    def __init__(self, reference_itrf: tuple[float, float, float] | None = None):
        self.pool = None

        logger.debug("Starting Delays process pool")

        # start a dedicated process
        self.pool = ProcessPoolExecutor(
            max_workers=1,
            initializer=self._init_process,
        )

        try:
            # process a command to make sure the pool process is initialised
            _ = self.set_measure_time(datetime.datetime.now())

            logger.info("Started Delays process pool")

            # set default reference position
            if reference_itrf is not None:
                _ = self.set_reference_itrf(reference_itrf)
        except Exception:
            # destruct properly
            self.stop()
            raise

    @property
    def running(self) -> bool:
        return self.pool is not None

    def __del__(self):
        if self.running:
            logger.warning(
                "stop() was never called. __del__ now will but this cannot be counted on."
            )
            self.stop()

    def stop(self):
        if not self.running:
            return

        logger.debug("Stopping Delays process pool")

        self.pool.shutdown()
        self.pool = None

        logger.info("Stopped Delays process pool")

    @staticmethod
    def _set_measure_time(utc_time: datetime.datetime):
        utc_time_str = utc_time.isoformat(" ")
        frame_time = Delays.measure.epoch("UTC", utc_time_str)

        if not Delays.measure.do_frame(frame_time):
            raise ValueError(f"measure.do_frame failed for UTC time {utc_time_str}")

    def set_measure_time(self, utc_time: datetime.datetime):
        """Configure the measure object for the specified time."""

        assert self.running, "Pool not running."

        future = self.pool.submit(self._set_measure_time, utc_time)
        _ = future.result()

    @staticmethod
    def _set_reference_itrf(reference_itrf: tuple[float, float, float]):
        frame_location = Delays.measure.position(
            "ITRF", *[f"{x}m" for x in reference_itrf]
        )

        if not Delays.measure.do_frame(frame_location):
            raise ValueError(
                f"measure.do_frame failed for ITRF location {reference_itrf}"
            )

    def set_reference_itrf(self, reference_itrf: tuple[float, float, float]):
        """Configure the measure object for the specified reference location."""

        assert self.running, "Pool not running."

        future = self.pool.submit(self._set_reference_itrf, reference_itrf)
        _ = future.result()

    def get_direction_vector(self, pointing: list[str]) -> numpy.ndarray:
        """Compute direction vector for a given pointing, relative to the measure."""

        return self.get_direction_vector_bulk([pointing]).flatten()

    @staticmethod
    def _get_direction_vector_bulk(pointings: list[list[str]]):
        """Process that provides casacore functions. Multiprocessing is needed
        to avoid casacore from blocking itself in a multithreaded environment."""

        angles = numpy.zeros((2, len(pointings)), dtype=numpy.float64)

        for idx, pointing in enumerate(pointings):
            if pointing[0] == "None":
                # uninitialised direction
                angles[:, idx] = (0, 0)
            else:
                direction = Delays.measure.direction(*pointing)
                m_angles = Delays.measure.measure(direction, "ITRF")
                angles[:, idx] = (m_angles["m0"]["value"], m_angles["m1"]["value"])

        # Convert polar to carthesian coordinates
        # see also https://github.com/casacore/casacore/blob/e793b3d5339d828a60339d16476bf688a19df3ec/casa/Quanta/MVDirection.cc#L67
        direction_vectors = numpy.array(
            [
                numpy.cos(angles[0]) * numpy.cos(angles[1]),
                numpy.sin(angles[0]) * numpy.cos(angles[1]),
                numpy.sin(angles[1]),
            ]
        )

        return direction_vectors.T

    def get_direction_vector_bulk(self, pointings: list[list[str]]) -> numpy.ndarray:
        """Compute direction vectors for the given pointings, relative to the measure."""

        assert self.running, "Pool not running."

        future = self.pool.submit(self._get_direction_vector_bulk, pointings)
        return future.result()

    def delays(
        self,
        pointing: list[str],
        antenna_relative_itrf: list[tuple[float, float, float]],
    ) -> numpy.ndarray:
        """Get the delays for a direction and relative antenna positions.

        These are the delays that have to be applied to the signal chain in order to line up the signal.
        Positions closer to the source will result in a positive delay.

        Returns delays[antenna]."""

        return self.delays_bulk(
            numpy.array([pointing]),
            numpy.array(antenna_relative_itrf),
        ).flatten()

    def delays_bulk(
        self, pointings: numpy.ndarray, antenna_relative_itrfs: numpy.ndarray
    ) -> numpy.ndarray:
        """Get the delays for each direction and each *relative* antenna position.

        These are the delays that have to be applied to the signal chain in order to line up the signal.
        Positions closer to the source will result in a positive delay.

        Returns delays[antenna][direction]."""

        # obtain the direction vector for each pointing
        direction_vectors = self.get_direction_vector_bulk(pointings)

        # compute the corresponding delays for all directions
        def get_delay_all_directions(relative_itrf):
            # Dot product between relative position and angle vector determines
            # distance. Divide by speed of light to obtain delay.
            return numpy.inner(relative_itrf, direction_vectors) / 299792458.0

        delays = numpy.apply_along_axis(
            get_delay_all_directions, 1, antenna_relative_itrfs
        )

        return delays
