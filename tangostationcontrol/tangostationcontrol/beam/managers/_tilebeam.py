#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import datetime
import logging

import numpy

from tangostationcontrol.common.constants import (
    N_elements,
    N_pol,
    MAX_ANTENNA,
)
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.device_decorators import (
    DurationMetric,
    DurationStatistics,
)
from tangostationcontrol.metrics import device_labels
from ._base import AbstractBeamManager

logger = logging.getLogger()


class TileBeamManager(AbstractBeamManager):
    def __init__(self, device):
        super().__init__()

        self.HBAT_antenna_positions = []
        self.HBAT_reference_positions = []
        self.delay_calculator = None
        self.nr_tiles = 0
        self.device = device

        # Labels for DurationMetric
        self.metric_labels = device_labels(device)

    @DurationMetric()
    def delays(self, pointing_direction: numpy.array, timestamp: datetime.datetime):
        """
        Calculate the delays (in seconds) based on the pointing list and the timestamp.

        Returns delays[tile][element]
        """

        delays = numpy.zeros((self.nr_tiles, N_elements), dtype=numpy.float64)

        d = self.delay_calculator

        for tile in range(self.nr_tiles):
            # initialise delay calculator for this tile
            d.set_reference_itrf(self.HBAT_reference_positions[tile])
            d.set_measure_time(timestamp)

            # calculate the delays based on the set reference position, the set time and
            # now the set direction and antenna positions
            delays[tile] = d.delays(
                pointing_direction[tile],
                self.HBAT_antenna_positions[tile] - self.HBAT_reference_positions[tile],
            )

        return delays

    @DurationMetric()
    def compute_weights(
        self, pointing_direction: numpy.array, timestamp: datetime.datetime
    ) -> numpy.array:
        """
        Uploads beam weights based on a given pointing direction 2D array (96 tiles x 3 parameters)
        """

        # Retrieve delays from casacore
        delays = self.delays(pointing_direction, timestamp)

        # Convert delays into beam weights
        delays = delays.flatten().reshape(self.nr_tiles, N_elements)

        result_values = numpy.zeros(
            (self.nr_tiles, N_elements * N_pol), dtype=numpy.int64
        )
        control_mapping = numpy.reshape(
            self.device.control.read_parent_attribute("Control_to_RECV_mapping_R"),
            (-1, N_pol),
        )

        recv_devices = self.device.control.read_parent_attribute("RECV_Devices_R")
        recv_proxies = [
            create_device_proxy(recv, write_access=True) for recv in recv_devices
        ]

        for recv_idx, recv_proxy in enumerate(recv_proxies):
            # collect all delays for this recv_proxy
            recv_result_indices = numpy.where(control_mapping[:, 0] == (recv_idx + 1))
            recv_delays = delays[recv_result_indices]

            if not recv_result_indices:
                # no RCUs are actually used from this recv_proxy
                continue

            # convert them into delay steps
            flatten_delay_steps = numpy.array(
                recv_proxy.calculate_HBAT_bf_delay_steps(recv_delays.flatten()),
                dtype=numpy.int64,
            )
            delay_steps = numpy.reshape(flatten_delay_steps, (-1, N_elements * N_pol))

            # write back into same positions we collected them from
            result_values[recv_result_indices] = delay_steps

        bf_delay_steps = result_values.flatten()

        return bf_delay_steps

    @DurationStatistics()
    @DurationMetric()
    def apply_weights(
        self,
        pointing_direction: numpy.array,
        timestamp: datetime.datetime,
        bf_delay_steps: numpy.array,
    ):
        control_mapping = numpy.reshape(
            self.device.control.read_parent_attribute("Control_to_RECV_mapping_R"),
            (-1, N_pol),
        )

        recv_devices = self.device.control.read_parent_attribute("RECV_Devices_R")
        recv_proxies = [
            create_device_proxy(recv, write_access=True) for recv in recv_devices
        ]

        mapped_values = []

        for _ in range(len(recv_proxies)):
            mapped_values.append(numpy.full((MAX_ANTENNA, N_elements * N_pol), None))

        mapped_values = numpy.array(mapped_values)

        bf_delay_steps2 = bf_delay_steps.reshape(self.nr_tiles, N_elements * N_pol)

        for idx, mapping in enumerate(control_mapping):
            recv = mapping[0]
            rcu = mapping[1]
            if recv > 0:
                mapped_values[recv - 1, rcu] = bf_delay_steps2[idx]

        mapped_values = mapped_values.reshape(
            (len(recv_proxies), MAX_ANTENNA, N_elements * N_pol)
        )

        # Stage the new delays for periodic writes that combine
        # delays for other antenna fields, and only applies when the
        # value changes.
        for idx, recv_proxy in enumerate(recv_proxies):
            self.device.atomic_read_modify_write_attribute(
                mapped_values[idx],
                recv_proxy,
                "HBAT_bf_delay_steps_stage_RW",
                cast_type=numpy.int64,
            )

        # Record where we now point to, now that we've updated the weights.
        # Only the entries within the mask have been updated
        mask = self.device.control.read_parent_attribute("ANT_mask_RW")
        for rcu in range(self.nr_tiles):
            if mask[rcu]:
                beam_update_error = (
                    datetime.datetime.now() - timestamp
                ).total_seconds() + self.beam_tracking_application_offset
                self.current_pointing_direction[rcu] = pointing_direction[rcu]
                self.current_pointing_timestamp[rcu] = timestamp.timestamp()
                self.current_pointing_error[rcu] = beam_update_error

                # Determine how far away from ideal apply time

        logger.info(
            "Pointing direction update submitted (check recvh logs for when it"
            + "gets applied), error: %s",
            beam_update_error,
        )
