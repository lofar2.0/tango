#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import datetime
import logging

import numpy

from tangostationcontrol.common.constants import (
    N_point_prop,
    N_beamlets_ctrl,
    N_pn,
    A_pn,
)
from tangostationcontrol.common.device_decorators import (
    DurationMetric,
    DurationStatistics,
)
from tangostationcontrol.metrics import device_labels
from ._base import AbstractBeamManager

logger = logging.getLogger()


class DigitalBeamManager(AbstractBeamManager):
    def __init__(self, device):
        super().__init__()

        self.delay_calculator = None
        self.relative_antenna_positions = None
        self.device = device

        # Labels for DurationMetric
        self.metric_labels = device_labels(device)

    def antennas_used(self) -> list[bool]:
        """
        Return the set of antennas actually used in the beamforming, after
        all masks and selectionis have been applied.
        """

        # Use zero weights for antennas that we should not use:
        #  - if the antenna has no input mapped on the SDP
        #  - if the antenna is bad (False in Antenna_Usage_Mask_R)
        #  - if the antenna is not in the antenna set (False in Antenna_Mask_RW)
        antenna_usage_mask = self.device.control.read_parent_attribute(
            "Antenna_Usage_Mask_R"
        )
        antenna_mask = self.device.read_Antenna_Mask_R()
        antenna_to_sdp_mapping = self.device.control.read_parent_attribute(
            "Antenna_to_SDP_Mapping_R"
        )

        antennas_used = [False] * len(antenna_mask)

        for antenna_nr, (_fpga_nr, input_nr) in enumerate(antenna_to_sdp_mapping):
            antennas_used[antenna_nr] = (
                input_nr >= 0
                and antenna_usage_mask[antenna_nr]
                and antenna_mask[antenna_nr]
            )

        return antennas_used

    @DurationMetric()
    def delays(self, pointing_direction: numpy.array, timestamp: datetime.datetime):
        """
        Calculate the delays (in seconds) based on the pointing list and the timestamp,

        Returns delays[N_pn][A_pn][len(pointing_direction)]
        """

        d = self.delay_calculator
        d.set_measure_time(timestamp)

        # calculate the delays based on the set reference position,
        # the set time and now the set direction and antenna positions
        delays = d.delays_bulk(pointing_direction, self.relative_antenna_positions)

        return delays

    @DurationMetric()
    def compute_weights(
        self, pointing_direction: numpy.array, timestamp: datetime.datetime
    ) -> numpy.array:
        """
        Uploads beam weights based on a given pointing direction 2D array
        (488 beamlets x 3 parameters)
        """

        assert pointing_direction.shape == (
            N_beamlets_ctrl,
            N_point_prop,
        ), f"{pointing_direction.shape}"

        # Retrieve delays from casacore, returns [antenna][beamlet]
        antenna_delays = self.delays(pointing_direction, timestamp)

        # Map the antennas onto the SDP input. Inputs for which we do not have an
        # antenna are given a delay of 0. They actually need a weight of 0, but no
        # delay can accomplish that. We zero out weights later.
        beam_delays = numpy.zeros((N_pn, A_pn, N_beamlets_ctrl), dtype=numpy.float32)
        for antenna_nr, (fpga_nr, input_nr) in enumerate(
            self.device.control.read_parent_attribute("Antenna_to_SDP_Mapping_R")
        ):
            if input_nr >= 0:
                beam_delays[fpga_nr, input_nr, :] = antenna_delays[antenna_nr, :]

        # Turn the delays into weights
        beam_weights = self.device.beamlet.calculate_bf_weights(beam_delays.flatten())
        beam_weights = beam_weights.reshape(N_pn, A_pn, N_beamlets_ctrl)

        # Use zero weights for antennas that we should not use
        antennas_used = self.antennas_used()
        zeroes_for_all_beamlets = numpy.array([0] * N_beamlets_ctrl, dtype=numpy.uint32)

        for antenna_nr, (fpga_nr, input_nr) in enumerate(
            self.device.control.read_parent_attribute("Antenna_to_SDP_Mapping_R")
        ):
            if not antennas_used[antenna_nr]:
                beam_weights[fpga_nr, input_nr, :] = zeroes_for_all_beamlets

        return beam_weights

    @DurationStatistics()
    @DurationMetric()
    def apply_weights(
        self,
        pointing_direction: numpy.array,
        timestamp: datetime.datetime,
        beam_weights: numpy.array,
    ):
        """
        Uploads beam weights based on a given pointing direction 2D array
        (488 beamlets x 3 parameters)
        """

        assert beam_weights.shape == (
            N_pn,
            A_pn,
            N_beamlets_ctrl,
        ), f"{beam_weights.shape}"

        self.device.beamlet.FPGA_bf_weights_pp_RW = beam_weights.reshape(
            N_pn, A_pn * N_beamlets_ctrl
        )

        # Determine how far away from ideal apply time
        beam_update_error = (
            datetime.datetime.now() - timestamp
        ).total_seconds() + self.beam_tracking_application_offset
        logger.info("Pointing direction updated, error: %s", beam_update_error)

        # Record where we now point to, now that we've updated the weights.
        # Only record pointings per beamlet, not which antennas took part
        self.current_pointing_direction = pointing_direction
        self.current_pointing_timestamp[:] = timestamp.timestamp()
        self.current_pointing_error[:] = beam_update_error
