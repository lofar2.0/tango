#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import abc
import asyncio
import datetime
import logging
from statistics import median

import numpy

logger = logging.getLogger()


class AbstractBeamManager:
    def __init__(self):
        self.new_pointing_direction = []
        self.current_pointing_direction = []
        self.current_pointing_timestamp = []
        self.current_pointing_error = []
        self.beam_tracking_application_offset = None

    async def wait_to_apply_weights(self, timestamp: datetime.datetime):
        # expected time required to upload weights to hardware
        # (use last 10 measured durations)
        expected_application_time = median(
            self.apply_weights.get_statistic(self)["history"][-10:] or [0.1]
        )

        # wait until provided time occurs, but don't start sleeping long here
        sleep_time = (
            (timestamp - datetime.datetime.now()).total_seconds()
            - expected_application_time
            - self.beam_tracking_application_offset
        )
        if sleep_time > 1:
            raise ValueError(
                f"Provided timestamp is too far into the future \
                to apply at real time: {sleep_time} seconds from now."
            )
        elif sleep_time < 0:
            logger.warning(
                "Insufficient prepare time to apply weights: %s too late",
                abs(sleep_time - expected_application_time),
            )

        await asyncio.sleep(max(0, sleep_time))

    async def update_pointing(self, timestamp: datetime.datetime):
        """Update the weights for the configured pointings,
        for the given timestamp, at the given timestamp."""
        await self.set_pointing(self.new_pointing_direction, timestamp)

    async def set_pointing(
        self, pointing_direction: numpy.array, timestamp: datetime.datetime
    ):
        """
        Calculate and Upload the hardware-specific delay weights
        based on the 2D pointing list (num_pointings x 3) and the timestamp

        Also updates _pointing_direction_r and _pointing_timestamp_r
        according to which antennas got their pointing updated.
        """

        # prepare weights
        weights = self.compute_weights(pointing_direction, timestamp)

        # wait until we can apply them
        await self.wait_to_apply_weights(timestamp)

        # upload weights
        self.apply_weights(pointing_direction, timestamp, weights)

    # -----------------
    # Abstract Methods
    # -----------------
    @abc.abstractmethod
    def compute_weights(
        self, pointing_direction: numpy.array, timestamp: datetime.datetime
    ) -> numpy.array:
        raise NotImplementedError

    @abc.abstractmethod
    def delays(self, pointing_direction: numpy.array, timestamp: datetime.datetime):
        """Extract text from the data set"""
        raise NotImplementedError

    @abc.abstractmethod
    def apply_weights(
        self,
        pointing_direction: numpy.array,
        timestamp: datetime.datetime,
        beam_weights: numpy.array,
    ):
        """Extract text from the data set"""
        raise NotImplementedError
