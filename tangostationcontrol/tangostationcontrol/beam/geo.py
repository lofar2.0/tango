# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import math

import etrsitrs
import geohash
import lofarantpos.geo
import numpy

"""
   LOFAR station positions are measured in ETRS89,
   which are the coordinates of the position as it would be in 1989.

   These coordinates are carthesian (X, Y, Z), 
   with (0, 0, 0) being the center of the Earth.

   The ETRS89 positions differ from the current due to tectonic movements.
   Periodically, these differences are modelled
   as position offsets and velocities.
   For example, ITRF2005 represents these offsets updated to 2005. We can
   furthermore extrapolate these models to later dates, such as 2015.5.

   By periodically extrapolating to later dates, or by using later models,
   we can obtain more precise positions of our
   antennas without having to remeasure them.

   The ETRSitrs package does all the transformation calculations for us.
"""


def _apply_fn_on_one_element_or_array(fn, array: numpy.array) -> numpy.array:
    if array.ndim == 1:
        # convert a single coordinate triple
        return fn(array)
    else:
        # convert each coordinate triple
        return numpy.apply_along_axis(fn, 1, array)


def ETRS_to_ITRF(
    ETRS_coordinates: numpy.array,
    ITRF_reference_frame: str = "ITRF2005",
    ITRF_reference_epoch: float = 2015.5,
) -> numpy.array:
    """Convert an array of coordinate triples from ETRS to ITRF, in the given reference frame and epoch."""

    # fetch converter
    ETRS_to_ITRF_fn = etrsitrs.convert_fn(
        "ETRF2000", ITRF_reference_frame, ITRF_reference_epoch
    )

    return _apply_fn_on_one_element_or_array(ETRS_to_ITRF_fn, ETRS_coordinates)


def ETRS_to_GEO(ETRS_coordinates: numpy.array) -> numpy.array:
    """Convert an array of coordinate triples from ETRS to latitude/longitude (degrees)."""

    def ETRS_to_GEO_fn(etrs_coords):
        geo_coords = lofarantpos.geo.geographic_from_xyz(etrs_coords)

        return numpy.array(
            [
                geo_coords["lat_rad"] * 180 / math.pi,
                geo_coords["lon_rad"] * 180 / math.pi,
            ]
        )

    return _apply_fn_on_one_element_or_array(ETRS_to_GEO_fn, ETRS_coordinates)


# Geo coordinates are only used for rough positioning.
# The difference between ITRF and ETRS matters little here
ITRF_to_GEO = ETRS_to_GEO


def GEO_to_GEOHASH(GEO_coordinates: numpy.array) -> numpy.array:
    """Convert an array of latitude/longitude (degrees) tuples to geohash strings."""

    def GEO_to_GEOHASH_fn(geo_coords):
        return geohash.encode(geo_coords[0], geo_coords[1], precision=16)

    return _apply_fn_on_one_element_or_array(GEO_to_GEOHASH_fn, GEO_coordinates)
