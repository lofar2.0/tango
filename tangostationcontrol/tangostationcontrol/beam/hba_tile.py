# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from math import sin, cos

import numpy


class HBATAntennaOffsets(object):
    """
        This class helps calculate the absolute offsets of the antennas within a tile,
        based on their relative orientation to ETRS.

        These offsets are Known in LOFAR1 as "iHBADeltas".

        Within LOFAR, we use a relative "PQR" coordinate system for each station:

        * The origin is the same as the ETRS reference position of the station,
        * "Q" points to true North as seen from CS002LBA, the center of LOFAR,
        * "P" points (roughly) east,
        * "R" points (roughly) up,
        * All antennas are positioned in the same plane (R=0 with an error < 3cm).

        With the PQR->ETRS rotation matrix (or "ROTATION_MATRIX" in LOFAR1),
        we can thus convert from PQR to (relative) ETRS coordinates.

        Below, the rotation of the HBA tiles is provided with respect to this PQR frame.
        Along with the PQR->ETRS rotation matrix, this allows us to calculate the
    `   relative positions of each antenna element within a tile in ETRS space.

        The relative ITRF positions are subsequently equal to the relative ETRS positions.

        For reference, see:
            https://git.astron.nl/RD/lofar-referentie-vlak/-/blob/master/data/dts/dts.ipynb
            https://git.astron.nl/ro/lofar/-/blob/master/MAC/Deployment/data/Coordinates/calc_hba_deltas.py
            https://github.com/brentjens/lofar-antenna-positions/blob/master/lofarantpos/db.py#L208

    """

    """ Model of the HBAT1 tile, as offsets of each antenna with respect to the reference center, in metres. """
    HBAT1_BASE_ANTENNA_OFFSETS = (
        numpy.array(
            [
                [-1.5, +1.5, 0.0],
                [-0.5, +1.5, 0.0],
                [+0.5, +1.5, 0.0],
                [+1.5, +1.5, 0.0],
                [-1.5, +0.5, 0.0],
                [-0.5, +0.5, 0.0],
                [+0.5, +0.5, 0.0],
                [+1.5, +0.5, 0.0],
                [-1.5, -0.5, 0.0],
                [-0.5, -0.5, 0.0],
                [+0.5, -0.5, 0.0],
                [+1.5, -0.5, 0.0],
                [-1.5, -1.5, 0.0],
                [-0.5, -1.5, 0.0],
                [+0.5, -1.5, 0.0],
                [+1.5, -1.5, 0.0],
            ]
        )
        * 1.25
    )

    @staticmethod
    def rotation_matrix(rad: float) -> numpy.array:
        """Return a rotation matrix for coordinates for a given number of radians."""

        rotation_matrix = numpy.array(
            [[cos(rad), sin(rad), 0], [-sin(rad), cos(rad), 0], [0, 0, 1]]
        )

        return rotation_matrix

    @staticmethod
    def ITRF_offsets(
        base_antenna_offsets: numpy.array,
        PQR_rotation: float,
        PQR_to_ETRS_rotation_matrix: numpy.array,
    ) -> numpy.array:
        """Return the antenna offsets in ITRF, given:

        :param: base_antenna_offsets:        antenna offsets within an unrotated tile (16x3).
        :param: PQR_rotation:                rotation of the tile(s) in PQR space (radians).
        :param: PQR_to_ETRS_rotation_matrix: rotation matrix for PQR -> ETRS conversion (3x3).
        """

        # Offsets in PQR are derived by rotating the base tile by the specified number of radians
        PQR_offsets = numpy.inner(
            base_antenna_offsets, HBATAntennaOffsets.rotation_matrix(PQR_rotation)
        )

        # The PQR->ETRS mapping is a rotation as well
        ETRS_offsets = numpy.inner(PQR_offsets, PQR_to_ETRS_rotation_matrix)

        # The ITRF offsets are the same as the ETRS offsets
        return ETRS_offsets
