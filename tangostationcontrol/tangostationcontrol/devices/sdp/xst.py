#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""XST Device Server for LOFAR2.0"""

import datetime
import numpy
from typing import Dict, List, Tuple

from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType

# PyTango imports
from tango.server import device_property, attribute

from tangostationcontrol.clients.opcua_client import OPCUAConnection
from tangostationcontrol.common.constants import (
    N_pn,
    P_sq,
    DEFAULT_SUBBAND,
    MAX_PARALLEL_SUBBANDS,
    MAX_INPUTS,
)
from tangostationcontrol.metrics import device_metrics

# Additional import
from tangostationcontrol.devices.sdp.statistics import Statistics

__all__ = ["XST"]


@device_metrics(
    exclude=[
        "FPGA_xst_ring_*_bsn_R",
        "FPGA_xst_ring_*_nof_packets_R",
        "FPGA_xst_ring_*_nof_valid_R",
        "FPGA_xst_ring_*_latency_R",
        "FPGA_xst_ring_*_clear_total_counts_*",
        "FPGA_xst_ring_nof_transport_hops_*",
        "FPGA_xst_rx_align_*",
        "FPGA_xst_offload_hdr_*",
        "FPGA_xst_subband_select_*",
        "xst_real_R",
        "xst_imag_R",
        "*_RW",
    ]
)
class XST(Statistics):
    """XST Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    FPGA_xst_processing_enable_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[True] * N_pn
    )

    FPGA_xst_ring_nof_transport_hops_RW_default = device_property(
        dtype="DevVarUShortArray", mandatory=False, default_value=[8] * N_pn
    )

    FPGA_xst_subband_select_RW_default = device_property(
        dtype="DevVarULongArray",
        mandatory=False,
        default_value=[[0, DEFAULT_SUBBAND, 0, 0, 0, 0, 0, 0]] * N_pn,
    )

    FPGA_xst_integration_interval_RW_default = device_property(
        dtype="DevVarDoubleArray", mandatory=False, default_value=[1.0] * N_pn
    )

    FPGA_xst_offload_enable_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[True] * N_pn
    )

    FIRST_DEFAULT_SETTINGS = [
        "FPGA_xst_subband_select_RW",
        "FPGA_xst_integration_interval_RW",
        "FPGA_xst_ring_nof_transport_hops_RW",
        # enable only after the offloading is configured correctly
        "FPGA_xst_offload_enable_RW",
    ]

    # ----------
    # Attributes
    # ----------

    # FPGA control points for XSTs
    FPGA_xst_integration_interval_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_integration_interval_RW"],
        datatype=numpy.double,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_integration_interval_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_integration_interval_R"],
        datatype=numpy.double,
        dims=(N_pn,),
    )
    FPGA_xst_offload_enable_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_enable_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_offload_enable_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_enable_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    FPGA_xst_offload_hdr_eth_destination_mac_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_hdr_eth_destination_mac_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_offload_hdr_eth_destination_mac_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_hdr_eth_destination_mac_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_xst_offload_hdr_ip_destination_address_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_hdr_ip_destination_address_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_offload_hdr_ip_destination_address_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_hdr_ip_destination_address_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_xst_offload_hdr_udp_destination_port_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_hdr_udp_destination_port_RW"],
        datatype=numpy.uint16,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_offload_hdr_udp_destination_port_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_hdr_udp_destination_port_R"],
        datatype=numpy.uint16,
        dims=(N_pn,),
    )
    FPGA_xst_input_bsn_at_sync_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_input_bsn_at_sync_R"],
        datatype=numpy.int64,
        dims=(N_pn,),
    )
    FPGA_xst_output_sync_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_output_sync_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn,),
    )
    FPGA_xst_offload_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn,),
    )
    FPGA_xst_processing_enable_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_processing_enable_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_processing_enable_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_processing_enable_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    FPGA_xst_subband_select_RW = AttributeWrapper(
        doc="Which subbands to emit XSTs for. The first element is the step size, allowing different subbands to be emitted every interval.",
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_subband_select_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, MAX_PARALLEL_SUBBANDS + 1),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_subband_select_R = AttributeWrapper(
        doc="Which subbands to emit XSTs for. The first element is the step size, allowing different subbands to be emitted every interval.",
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_subband_select_R"],
        datatype=numpy.uint32,
        dims=(N_pn, MAX_PARALLEL_SUBBANDS + 1),
    )
    FPGA_xst_start_time_RW = AttributeWrapper(
        doc="Start generating XSTs at this timestamp, or as soon as possible. Use this to synchronise the XSTs from different FPGAs.",
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_start_time_RW"],
        datatype=numpy.float64,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
        unit="s",
    )
    FPGA_xst_start_time_R = AttributeWrapper(
        doc="Start generating XSTs at this timestamp, or as soon as possible. Use this to synchronise the XSTs from different FPGAs.",
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_start_time_R"],
        datatype=numpy.float64,
        dims=(N_pn,),
        unit="s",
    )

    FPGA_xst_offload_nof_crosslets_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_nof_crosslets_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_offload_nof_crosslets_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_offload_nof_crosslets_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )

    FPGA_xst_ring_nof_transport_hops_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_nof_transport_hops_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_ring_nof_transport_hops_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_nof_transport_hops_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )

    FPGA_xst_offload_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_xst_offload_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_xst_offload_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_xst_offload_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )

    FPGA_xst_ring_rx_clear_total_counts_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_clear_total_counts_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_ring_rx_clear_total_counts_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_clear_total_counts_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    FPGA_xst_rx_align_stream_enable_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_stream_enable_RW"],
        datatype=bool,
        dims=(N_pn, P_sq),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_xst_rx_align_stream_enable_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_stream_enable_R"],
        datatype=bool,
        dims=(N_pn, P_sq),
    )
    FPGA_xst_ring_rx_total_nof_packets_received_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_total_nof_packets_received_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_xst_ring_rx_total_nof_packets_discarded_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_total_nof_packets_discarded_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_xst_ring_rx_total_nof_sync_received_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_total_nof_sync_received_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_xst_ring_rx_total_nof_sync_discarded_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_total_nof_sync_discarded_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_xst_ring_rx_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_ring_rx_nof_packets_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_ring_rx_nof_valid_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_ring_rx_latency_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_rx_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_rx_align_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, P_sq),
    )
    FPGA_xst_rx_align_nof_packets_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, P_sq),
    )
    FPGA_xst_rx_align_nof_valid_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, P_sq),
    )
    FPGA_xst_rx_align_latency_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, P_sq),
    )
    FPGA_xst_rx_align_nof_replaced_packets_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_rx_align_nof_replaced_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, P_sq),
    )
    FPGA_xst_aligned_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_aligned_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn,),
    )
    FPGA_xst_aligned_nof_packets_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_aligned_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_xst_aligned_nof_valid_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_aligned_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_xst_aligned_latency_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_aligned_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_xst_ring_tx_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_tx_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_ring_tx_nof_packets_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_tx_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_ring_tx_nof_valid_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_tx_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_pn),
    )
    FPGA_xst_ring_tx_latency_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_xst_ring_tx_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_pn),
    )

    @attribute(
        max_dim_x=MAX_INPUTS,
        max_dim_y=MAX_INPUTS,
        dtype=((numpy.float32,),),
        doc="Last received XST matrix (real component)",
    )
    def xst_real_R(self):
        return numpy.array(self.last_xst["data_real"], dtype=numpy.float32)

    @attribute(
        max_dim_x=MAX_INPUTS,
        max_dim_y=MAX_INPUTS,
        dtype=((numpy.float32,),),
        doc="Last received XST matrix (imaginary component)",
    )
    def xst_imag_R(self):
        return numpy.array(self.last_xst["data_imag"], dtype=numpy.float32)

    @attribute(dtype=numpy.float64)
    def xst_timestamp_R(self):
        try:
            return self.last_xst["timestamp"].timestamp()
        except ValueError:
            return 0.0

    @attribute(dtype=numpy.int32)
    def xst_subband_R(self):
        return self.last_xst["subband"]

    @attribute(dtype=numpy.float32)
    def xst_integration_interval_R(self):
        return self.last_xst["integration_interval"]

    # ----------
    # Summarising Attributes
    # ----------
    FPGA_processing_error_R = attribute(
        dtype=(bool,), max_dim_x=N_pn, fisallowed="is_attribute_access_allowed"
    )

    def read_FPGA_processing_error_R(self):
        return self.control.read_parent_attribute(
            "TR_fpga_mask_R",
        ) & (
            ~self.read_attribute("FPGA_xst_offload_enable_R")
            | ~self.read_attribute("FPGA_xst_processing_enable_R")
        )

    # --------
    # Overloaded functions
    # --------

    def __init__(self, cl, name):
        super().__init__(cl, name)

        self.last_xst = {
            "timestamp": datetime.datetime.min,
            "subband": -1,
            "data_real": [],
            "data_imag": [],
            "integration_interval": 0.0,
        }

    def handle_statistics_message(self, topic, message):
        self.last_xst["timestamp"] = datetime.datetime.fromisoformat(
            message["timestamp"]
        )
        self.last_xst["subband"] = message["subband"]
        self.last_xst["integration_interval"] = message["integration_interval"]
        self.last_xst["data_real"] = message["xst_data_real"]
        self.last_xst["data_imag"] = message["xst_data_imag"]

    def _get_defaults(self, properties: Dict[str, object]) -> List[Tuple[str, object]]:
        return self._statistics_defaults("xst", properties) + super()._get_defaults(
            properties
        )

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""

        processing_enabled = self.read_attribute("FPGA_xst_processing_enable_R")
        offload_enabled = self.read_attribute("FPGA_xst_offload_enable_R")

        mask = self.control.read_parent_attribute("TR_fpga_mask_R")

        try:
            # "powered" means processing and offload is as expected for the FPGAs required
            return numpy.count_nonzero(
                processing_enabled & offload_enabled & mask
            ) / numpy.count_nonzero(mask)
        except ZeroDivisionError:
            return 1.0

    def _power_hardware_on(self):
        self.proxy.write_attribute(
            "FPGA_xst_processing_enable_RW", self.FPGA_xst_processing_enable_RW_default
        )
        self.proxy.write_attribute(
            "FPGA_xst_offload_enable_RW", self.FPGA_xst_offload_enable_RW_default
        )

    def _power_hardware_off(self):
        self.proxy.write_attribute("FPGA_xst_offload_enable_RW", [False] * N_pn)
        self.proxy.write_attribute("FPGA_xst_processing_enable_RW", [False] * N_pn)

    # --------
    # Commands
    # --------
