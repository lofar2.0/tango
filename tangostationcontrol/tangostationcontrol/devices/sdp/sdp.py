#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""SDP Device Server for LOFAR2.0"""
import time
from typing import Dict, List, Tuple

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType

# PyTango imports
from tango.server import device_property, attribute

# Additional import
from tangostationcontrol.common.constants import (
    S_pn,
    N_pn,
    CLK_200_MHZ,
    N_subband_res,
    N_subbands,
    DEFAULT_SUBBAND,
    SDP_UNIT_WEIGHT,
)
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.common.sdp import subband_frequencies
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.common.device_decorators import DurationMetric
from tangostationcontrol.metrics import device_metrics

__all__ = ["SDP"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "FPGA_sdp_info_*",
        "FPGA_subband_weights_*",
        "FPGA_signal_input_samples_delay_*",
        "FPGA_jesd204b_csr_*",
        "FPGA_wg_amplitude_*",
        "FPGA_wg_frequency_*",
        "FPGA_wg_phase_*",
        "subband_frequency_R",
        "*_RW",
    ],
    include=[
        "FPGA_jesd204b_rx_err0_sum_R",
        "FPGA_jesd204b_rx_err1_sum_R",
    ],
)
class SDP(OPCUADevice):
    """SDP Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    # Do not enable processing before:
    #  * the ring is configured by this device
    #  * the (number of) beamlet output destinations is configured by the beamlet device
    # Changing these values after processing has been enabled leads to unstable results!
    FPGA_processing_enable_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[True] * N_pn
    )

    FPGA_ring_node_offset_RW_default = device_property(
        dtype="DevVarUShortArray", mandatory=False, default_value=[0] * N_pn
    )

    FPGA_ring_nof_nodes_RW_default = device_property(
        dtype="DevVarUShortArray", mandatory=False, default_value=[16] * N_pn
    )

    FPGA_ring_use_cable_to_next_rn_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[False] * N_pn
    )

    FPGA_ring_use_cable_to_previous_rn_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[False] * N_pn
    )

    FPGA_beamlet_output_nof_destinations_RW_default = device_property(
        doc="Number of output streams. Will be set to this value for all FPGAs",
        dtype="DevVarUShortArray",
        mandatory=False,
        default_value=[4] * N_pn,
    )

    FPGA_wg_enable_RW_default = device_property(
        dtype="DevVarBooleanArray",
        mandatory=False,
        default_value=[[False] * S_pn] * N_pn,
    )

    # If we enable the waveform generator, we want some sane defaults.

    FPGA_wg_amplitude_RW_default = device_property(
        dtype="DevVarDoubleArray", mandatory=False, default_value=[[0.1] * S_pn] * N_pn
    )

    FPGA_wg_frequency_RW_default = device_property(
        dtype="DevVarDoubleArray",
        mandatory=False,
        # Emit a signal on subband 102
        default_value=[[DEFAULT_SUBBAND * CLK_200_MHZ / N_subband_res] * S_pn] * N_pn,
    )

    FPGA_wg_phase_RW_default = device_property(
        dtype="DevVarDoubleArray", mandatory=False, default_value=[[0.0] * S_pn] * N_pn
    )

    FPGA_sdp_info_station_id_RW_default = device_property(
        dtype="DevVarULongArray", mandatory=False, default_value=[0] * N_pn
    )

    FPGA_sdp_info_antenna_band_index_RW_default = device_property(
        dtype="DevVarULongArray", mandatory=False, default_value=[0] * N_pn
    )

    FPGA_sdp_info_antenna_field_index_RW_default = device_property(
        dtype="DevVarULongArray", mandatory=False, default_value=[0] * N_pn
    )

    FPGA_signal_input_samples_delay_RW_default = device_property(
        dtype="DevVarULongArray", mandatory=False, default_value=[[0] * S_pn] * N_pn
    )

    FPGA_subband_weights_RW_default = device_property(
        dtype="DevVarULongArray",
        mandatory=False,
        default_value=[[SDP_UNIT_WEIGHT] * S_pn * N_subbands] * N_pn,
    )

    FPGA_subband_weights_cross_RW_default = device_property(
        dtype="DevVarULongArray",
        mandatory=False,
        default_value=[[0] * S_pn * N_subbands] * N_pn,
    )

    FIRST_DEFAULT_SETTINGS = [
        # These settings must be configured before FPGA_processing_enable_RW is set to True
        "FPGA_ring_node_offset_RW",
        "FPGA_ring_nof_nodes_RW",
        "FPGA_ring_use_cable_to_next_rn_RW",
        "FPGA_ring_use_cable_to_previous_rn_RW",
        "FPGA_beamlet_output_nof_destinations_RW",
    ]

    def _get_defaults(self, properties: Dict[str, object]) -> List[Tuple[str, object]]:
        # Disable FPGA processing before configuring the ring, and to explicitly toggle it after
        # powering the RCUs.
        #
        # The defaults for FPGA_processing_enable_RW can reenable it afterwards.
        return [("FPGA_processing_enable_RW", [False] * N_pn)] + super()._get_defaults(
            properties
        )

    # ----------
    # Attributes
    # ----------

    FPGA_processing_enable_R = AttributeWrapper(
        comms_annotation=["FPGA_processing_enable_R"], datatype=bool, dims=(N_pn,)
    )
    FPGA_processing_enable_RW = AttributeWrapper(
        comms_annotation=["FPGA_processing_enable_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_ring_node_offset_R = AttributeWrapper(
        comms_annotation=["FPGA_ring_node_offset_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_ring_node_offset_RW = AttributeWrapper(
        comms_annotation=["FPGA_ring_node_offset_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_ring_nof_nodes_R = AttributeWrapper(
        comms_annotation=["FPGA_ring_nof_nodes_R"], datatype=numpy.uint32, dims=(N_pn,)
    )
    FPGA_ring_nof_nodes_RW = AttributeWrapper(
        comms_annotation=["FPGA_ring_nof_nodes_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_ring_use_cable_to_next_rn_R = AttributeWrapper(
        comms_annotation=["FPGA_ring_use_cable_to_next_rn_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    FPGA_ring_use_cable_to_next_rn_RW = AttributeWrapper(
        comms_annotation=["FPGA_ring_use_cable_to_next_rn_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_ring_use_cable_to_previous_rn_R = AttributeWrapper(
        comms_annotation=["FPGA_ring_use_cable_to_previous_rn_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    FPGA_ring_use_cable_to_previous_rn_RW = AttributeWrapper(
        comms_annotation=["FPGA_ring_use_cable_to_previous_rn_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_beamlet_output_nof_destinations_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_nof_destinations_R"],
        datatype=numpy.uint8,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_nof_destinations_RW = AttributeWrapper(
        doc="Number of UDP streams to create for beamlets. NB: Cannot be configured once FPGA_processing_enable_R has been True.",
        comms_annotation=["FPGA_beamlet_output_nof_destinations_RW"],
        datatype=numpy.uint8,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_sdp_info_antenna_band_index_R = AttributeWrapper(
        doc="Antenna band number to put in packet headers. 0=LB, 1=HB",
        comms_annotation=["FPGA_sdp_info_antenna_band_index_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_antenna_band_index_RW = AttributeWrapper(
        doc="Antenna band number to put in packet headers. 0=LB, 1=HB",
        comms_annotation=["FPGA_sdp_info_antenna_band_index_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_sdp_info_antenna_field_index_R = AttributeWrapper(
        doc="Antenna field number to put in packet headers. 0=LBA/HBA0/HBA, 1=HBA1",
        comms_annotation=["FPGA_sdp_info_antenna_field_index_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_antenna_field_index_RW = AttributeWrapper(
        doc="Antenna field number to put in packet headers. 0=LBA/HBA0/HBA, 1=HBA1",
        comms_annotation=["FPGA_sdp_info_antenna_field_index_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_sdp_info_block_period_R = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_block_period_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_f_adc_R = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_f_adc_R"], datatype=numpy.uint32, dims=(N_pn,)
    )
    FPGA_sdp_info_fsub_type_R = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_fsub_type_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_nyquist_sampling_zone_index_R = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_nyquist_sampling_zone_index_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_nyquist_sampling_zone_index_RW = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_nyquist_sampling_zone_index_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_sdp_info_observation_id_R = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_observation_id_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_observation_id_RW = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_observation_id_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_sdp_info_station_id_R = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_station_id_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_sdp_info_station_id_RW = AttributeWrapper(
        comms_annotation=["FPGA_sdp_info_station_id_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_spectral_inversion_R = AttributeWrapper(
        comms_annotation=["FPGA_spectral_inversion_R"],
        datatype=bool,
        dims=(N_pn, S_pn),
    )
    FPGA_spectral_inversion_RW = AttributeWrapper(
        comms_annotation=["FPGA_spectral_inversion_RW"],
        datatype=bool,
        dims=(N_pn, S_pn),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_subband_weights_R = AttributeWrapper(
        comms_annotation=["FPGA_subband_weights_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn, N_subbands),
    )
    FPGA_subband_weights_RW = AttributeWrapper(
        comms_annotation=["FPGA_subband_weights_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn, N_subbands),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_subband_weights_cross_R = AttributeWrapper(
        comms_annotation=["FPGA_subband_weights_cross_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn, N_subbands),
    )
    FPGA_subband_weights_cross_RW = AttributeWrapper(
        comms_annotation=["FPGA_subband_weights_cross_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn, N_subbands),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_wg_amplitude_R = AttributeWrapper(
        comms_annotation=["FPGA_wg_amplitude_R"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
    )
    FPGA_wg_amplitude_RW = AttributeWrapper(
        comms_annotation=["FPGA_wg_amplitude_RW"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_wg_enable_R = AttributeWrapper(
        comms_annotation=["FPGA_wg_enable_R"], datatype=bool, dims=(N_pn, S_pn)
    )
    FPGA_wg_enable_RW = AttributeWrapper(
        comms_annotation=["FPGA_wg_enable_RW"],
        datatype=bool,
        dims=(N_pn, S_pn),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_wg_frequency_R = AttributeWrapper(
        comms_annotation=["FPGA_wg_frequency_R"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
    )
    FPGA_wg_frequency_RW = AttributeWrapper(
        comms_annotation=["FPGA_wg_frequency_RW"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_wg_phase_R = AttributeWrapper(
        comms_annotation=["FPGA_wg_phase_R"], datatype=numpy.double, dims=(N_pn, S_pn)
    )
    FPGA_wg_phase_RW = AttributeWrapper(
        comms_annotation=["FPGA_wg_phase_RW"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
        access=AttrWriteType.READ_WRITE,
    )

    # OPC-UA MP only points for AIT
    FPGA_signal_input_mean_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_mean_R"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
    )
    FPGA_signal_input_rms_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_rms_R"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
    )
    FPGA_signal_input_std_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_std_R"],
        datatype=numpy.double,
        dims=(N_pn, S_pn),
    )

    FPGA_jesd204b_csr_rbd_count_R = AttributeWrapper(
        comms_annotation=["FPGA_jesd204b_csr_rbd_count_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn),
    )
    FPGA_jesd204b_csr_dev_syncn_R = AttributeWrapper(
        comms_annotation=["FPGA_jesd204b_csr_dev_syncn_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn),
    )

    FPGA_jesd204b_rx_err0_R = AttributeWrapper(
        comms_annotation=["FPGA_jesd204b_rx_err0_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn),
    )

    FPGA_jesd204b_rx_err1_R = AttributeWrapper(
        comms_annotation=["FPGA_jesd204b_rx_err1_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn),
    )

    @attribute(
        doc="FPGA_jesd204b_rx_err0_R aggregated per FPGA",
        dtype=(numpy.uint32,),
        max_dim_x=N_pn,
    )
    def FPGA_jesd204b_rx_err0_sum_R(self):
        return numpy.sum(self.read_attribute("FPGA_jesd204b_rx_err0_R"), axis=1)

    @attribute(
        doc="FPGA_jesd204b_rx_err1_R aggregated per FPGA",
        dtype=(numpy.uint32,),
        max_dim_x=N_pn,
    )
    def FPGA_jesd204b_rx_err1_sum_R(self):
        return numpy.sum(self.read_attribute("FPGA_jesd204b_rx_err1_R"), axis=1)

    FPGA_jesd_ctrl_R = AttributeWrapper(
        comms_annotation=["FPGA_jesd_ctrl_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_jesd_ctrl_RW = AttributeWrapper(
        comms_annotation=["FPGA_jesd_ctrl_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_signal_input_bsn_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_bsn_R"], datatype=numpy.int64, dims=(N_pn,)
    )
    FPGA_signal_input_nof_blocks_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_nof_blocks_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_signal_input_nof_samples_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_nof_samples_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_signal_input_samples_delay_R = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_samples_delay_R"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn),
    )
    FPGA_signal_input_samples_delay_RW = AttributeWrapper(
        comms_annotation=["FPGA_signal_input_samples_delay_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, S_pn),
        access=AttrWriteType.READ_WRITE,
    )

    nyquist_zone_RW = attribute(
        doc="Nyquist zone of each input.",
        dtype=((numpy.uint32,),),
        max_dim_y=N_pn,
        max_dim_x=S_pn,
        access=AttrWriteType.READ_WRITE,
        fisallowed="is_attribute_access_allowed",
    )
    subband_frequency_R = attribute(
        doc="Frequency of each subband for each input",
        unit="Hz",
        dtype=((numpy.float64,),),
        max_dim_y=N_pn * S_pn,
        max_dim_x=N_subbands,
        fisallowed="is_attribute_access_allowed",
    )

    def read_nyquist_zone_RW(self):
        return self._nyquist_zone

    def write_nyquist_zone_RW(self, value):
        # use numpy for easy processing
        value = numpy.array(value)

        # validate shape
        if value.shape != (N_pn, S_pn):
            raise ValueError(
                f"Dimension mismatch. Expected ({N_pn}, {S_pn}), got {value.shape}."
            )

        # adopt new value
        self._nyquist_zone = value

        # propagate the new value

        # We assume the first Nyquist zone of each FPGA is representative for the whole FPGA,
        # that is, the whole FPGA is assumed to have the same Nyquist zone.
        nyquist_zone_per_fpga = self._nyquist_zone[:, 0]

        # Update the packet headers.
        self.proxy.FPGA_sdp_info_nyquist_sampling_zone_index_RW = nyquist_zone_per_fpga

        # Use the correct spectral inversion setting,
        # to make sure the subbands are ascending in frequency
        self.proxy.FPGA_spectral_inversion_RW = self._nyquist_zone % 2

    @staticmethod
    def _subband_frequencies(
        clock: int, nyquist_zone: numpy.ndarray, spectral_inversion: numpy.ndarray
    ) -> numpy.ndarray:
        subband_list = numpy.array(range(N_subbands))

        def frequencies_per_input(fpga_nr, input_nr):
            """Return the frequencies for all subbands of a given input."""
            return subband_frequencies(
                subband_list,
                clock,
                nyquist_zone[fpga_nr, input_nr],
                spectral_inversion[fpga_nr, input_nr],
            )

        # compute all frequencies for all inputs.
        frequencies = numpy.array(
            [
                frequencies_per_input(fpga_nr, input_nr)
                for fpga_nr in range(N_pn)
                for input_nr in range(S_pn)
            ],
            dtype=numpy.float64,
        )

        return frequencies.reshape(N_pn * S_pn, N_subbands)

    @DurationMetric()
    def read_subband_frequency_R(self):
        clock = self.control.read_parent_attribute("clock_RW")  # scalar
        nyquist_zone = numpy.array(
            self.read_attribute("nyquist_zone_RW")
        )  # N_pn x S_pn
        spectral_inversion = self.read_attribute(
            "FPGA_spectral_inversion_RW"
        )  # N_pn x S_pn

        return self._subband_frequencies(clock, nyquist_zone, spectral_inversion)

    # ----------
    # Summarising Attributes
    # ----------
    FPGA_processing_error_R = attribute(
        dtype=(bool,), max_dim_x=N_pn, fisallowed="is_attribute_access_allowed"
    )
    FPGA_input_error_R = attribute(
        dtype=(bool,), max_dim_x=N_pn, fisallowed="is_attribute_access_allowed"
    )

    def read_FPGA_processing_error_R(self):
        return self.control.read_parent_attribute(
            "TR_fpga_mask_R",
        ) & (
            ~self.read_attribute("FPGA_processing_enable_R")
            | (self.control.read_parent_attribute("FPGA_boot_image_R") <= 0)
        )

    def read_FPGA_input_error_R(self):
        return self.control.read_parent_attribute(
            "TR_fpga_mask_R",
        ) & (
            self.read_attribute("FPGA_wg_enable_R").any(axis=1)
            | (self.read_attribute("FPGA_signal_input_rms_R") == 0).any(axis=1)
        )

    # --------
    # overloaded functions
    # --------

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""

        mask = self.control.read_parent_attribute("TR_fpga_mask_R")
        powered = self.read_attribute("FPGA_processing_enable_R")

        try:
            return numpy.count_nonzero(powered & mask) / numpy.count_nonzero(mask)
        except ZeroDivisionError:
            return 1.0

    def _set_defaults(self):
        super()._set_defaults()

        # wait 2s for the BSN to propagate through the FPGAs after
        # enabling FPGA_processing_enable_RW.
        time.sleep(2.0)

    def _init_device(self):
        super()._init_device()

        # Poll and send change events for attributes that device_metrics
        # does not poll, but which we do want to allow subscriptions on.
        self.attribute_poller.register("subband_frequency_R", metric=None)

    def configure_for_initialise(self):
        super().configure_for_initialise()

        # Store which band filter is connected to each input.
        #
        # We need to be told this by AntennaField (write_Frequency_Band_RW)
        self._nyquist_zone = numpy.array([[0] * S_pn] * N_pn, dtype=numpy.uint32)

    # --------
    # Commands
    # --------

    # --------
    # Support functions
    # --------
