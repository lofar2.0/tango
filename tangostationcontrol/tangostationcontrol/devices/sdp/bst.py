#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""BST Device Server for LOFAR2.0"""

import datetime
import numpy
from typing import Dict, List, Tuple

from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType
from tango.server import device_property, attribute
from tangostationcontrol.clients.opcua_client import OPCUAConnection
from tangostationcontrol.common.constants import N_pn, N_pol, N_beamlets_max
from tangostationcontrol.metrics import device_metrics

# Own imports
from tangostationcontrol.devices.sdp.statistics import Statistics

__all__ = ["BST"]


@device_metrics(
    exclude=[
        "FPGA_bst_offload_hdr_*",
        "bst_R",
        "*_RW",
    ]
)
class BST(Statistics):
    """BST Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    FPGA_bst_offload_enable_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[True] * N_pn
    )

    FIRST_DEFAULT_SETTINGS = [
        # enable only after the offloading is configured correctly
        "FPGA_bst_offload_enable_RW",
    ]

    # ----------
    # Attributes
    # ----------

    # FPGA control points for BSTs
    FPGA_bst_offload_enable_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_enable_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bst_offload_enable_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_enable_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    FPGA_bst_offload_hdr_eth_destination_mac_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_hdr_eth_destination_mac_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bst_offload_hdr_eth_destination_mac_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_hdr_eth_destination_mac_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_bst_offload_hdr_ip_destination_address_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_hdr_ip_destination_address_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bst_offload_hdr_ip_destination_address_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_hdr_ip_destination_address_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_bst_offload_hdr_udp_destination_port_RW = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_hdr_udp_destination_port_RW"],
        datatype=numpy.uint16,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bst_offload_hdr_udp_destination_port_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_hdr_udp_destination_port_R"],
        datatype=numpy.uint16,
        dims=(N_pn,),
    )
    FPGA_bst_offload_bsn_R = AttributeWrapper(
        comms_id=OPCUAConnection,
        comms_annotation=["FPGA_bst_offload_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn,),
    )

    FPGA_bst_offload_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_bst_offload_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )
    FPGA_bst_offload_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_bst_offload_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
    )

    @attribute(
        max_dim_x=N_pol,
        max_dim_y=N_beamlets_max,
        dtype=((numpy.float32,),),
        doc="Last received BST matrix",
    )
    def bst_R(self):
        return numpy.array(self.last_bst["data"], dtype=numpy.float32)

    @attribute(dtype=numpy.float64)
    def bst_timestamp_R(self):
        try:
            return self.last_bst["timestamp"].timestamp()
        except ValueError:
            return 0.0

    @attribute(dtype=numpy.float32)
    def bst_integration_interval_R(self):
        return self.last_bst["integration_interval"]

    # ----------
    # Summarising Attributes
    # ----------
    FPGA_processing_error_R = attribute(
        dtype=(bool,), max_dim_x=N_pn, fisallowed="is_attribute_access_allowed"
    )

    def read_FPGA_processing_error_R(self):
        return self.control.read_parent_attribute(
            "TR_fpga_mask_RW",
        ) & (~self.read_attribute("FPGA_bst_offload_enable_R"))

    # --------
    # Overloaded functions
    # --------

    def __init__(self, cl, name):
        super().__init__(cl, name)

        self.last_bst = {
            "timestamp": datetime.datetime.min,
            "data": [],
            "integration_interval": 0.0,
        }

    def handle_statistics_message(self, topic, message):
        self.last_bst["timestamp"] = datetime.datetime.fromisoformat(
            message["timestamp"]
        )
        self.last_bst["integration_interval"] = message["integration_interval"]
        self.last_bst["data"] = message["bst_data"]

    def _get_defaults(self, properties: Dict[str, object]) -> List[Tuple[str, object]]:
        return self._statistics_defaults("bst", properties) + super()._get_defaults(
            properties
        )

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""
        return 1.0 * any(self.read_attribute("FPGA_bst_offload_enable_R"))

    def _power_hardware_on(self):
        self.proxy.write_attribute(
            "FPGA_bst_offload_enable_RW", self.FPGA_bst_offload_enable_RW_default
        )

    def _power_hardware_off(self):
        self.proxy.write_attribute("FPGA_bst_offload_enable_RW", [False] * N_pn)

    # --------
    # Commands
    # --------
