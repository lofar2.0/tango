#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""SDP Firmware Device Server for LOFAR2.0"""

import logging

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType

# PyTango imports
from tango.server import device_property, attribute

# Additional import
from tangostationcontrol.common.constants import (
    N_pn,
    S_pn,
    CLK_200_MHZ,
    CLK_160_MHZ,
    N_subbands,
)
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.devices.types import DeviceTypes
from tangostationcontrol.metrics import device_metrics

logger = logging.getLogger()

__all__ = ["SDPFirmware"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "FPGA_scrap_*",
        "FPGA_flash_*",
        "TR_ucp_reset_counters_*",
        "*_RW",
    ],
    include=[
        "clock_RW",
        "TR_fpga_mask_RW",
    ],
)
class SDPFirmware(OPCUADevice):
    """SDP Firmware Device server for LOFAR 2.0"""

    # -----------------
    # Device Properties
    # -----------------

    TR_fpga_mask_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[True] * N_pn
    )
    clock_RW_default = device_property(
        dtype="DevULong", mandatory=False, default_value=CLK_200_MHZ
    )

    # ----- Timing values

    Firmware_Boot_timeout = device_property(
        doc="Maximum amount of time to wait for FPGAs to boot.",
        dtype="DevFloat",
        mandatory=False,
        default_value=60.0,
    )

    TRANSLATOR_DEFAULT_SETTINGS = ["TR_fpga_mask_RW"]

    # ----------
    # Attributes
    # ----------

    # The SDP Firmware device offers all monitoring & control points
    # available in the Board Support Package (BSP) that is supported
    # by both the user and factory images.

    # Translator
    TR_fpga_mask_R = AttributeWrapper(
        comms_annotation=["TR_fpga_mask_R"], datatype=bool, dims=(N_pn,)
    )
    TR_fpga_mask_RW = AttributeWrapper(
        comms_annotation=["TR_fpga_mask_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    TR_fpga_communication_error_R = AttributeWrapper(
        comms_annotation=["TR_fpga_communication_error_R"], datatype=bool, dims=(N_pn,)
    )
    TR_sdp_config_first_fpga_nr_R = AttributeWrapper(
        comms_annotation=["TR_sdp_config_first_fpga_nr_R"], datatype=numpy.uint32
    )
    TR_sdp_config_nof_beamsets_R = AttributeWrapper(
        comms_annotation=["TR_sdp_config_nof_beamsets_R"], datatype=numpy.uint32
    )
    TR_sdp_config_nof_fpgas_R = AttributeWrapper(
        comms_annotation=["TR_sdp_config_nof_fpgas_R"], datatype=numpy.uint32
    )
    TR_software_version_R = AttributeWrapper(
        comms_annotation=["TR_software_version_R"], datatype=str
    )
    TR_start_time_R = AttributeWrapper(
        comms_annotation=["TR_start_time_R"], datatype=numpy.int64
    )
    TR_tod_R = AttributeWrapper(
        doc="Time-of-day of SDPTR",
        comms_annotation=["TR_tod_R"],
        datatype=numpy.int64,
        dims=(2,),
    )  # struct of (time_t, int64)
    TR_tod_sync_status_R = AttributeWrapper(
        doc="Time-of-day synchronisation status with PTP/NTP (True=synchronised)",
        comms_annotation=["TR_tod_sync_status_R"],
        datatype=bool,
    )
    TR_tod_pps_delta_R = AttributeWrapper(
        comms_annotation=["TR_tod_pps_delta_R"], datatype=numpy.double
    )
    TR_pps_monitor_delta_R = AttributeWrapper(
        comms_annotation=["TR_pps_monitor_delta_R"], datatype=numpy.double
    )

    # Firmware & flash
    FPGA_boot_image_R = AttributeWrapper(
        comms_annotation=["FPGA_boot_image_R"],
        datatype=numpy.int32,
        dims=(N_pn,),
        doc="Active FPGA image (0=factory, 1=user)",
    )
    FPGA_boot_image_RW = AttributeWrapper(
        comms_annotation=["FPGA_boot_image_RW"],
        datatype=numpy.int32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_flash_addr_R = AttributeWrapper(
        comms_annotation=["FPGA_flash_addr_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_flash_addr_RW = AttributeWrapper(
        comms_annotation=["FPGA_flash_addr_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_flash_protect_R = AttributeWrapper(
        comms_annotation=["FPGA_flash_protect_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        doc="Protect (0) or unprotect (1) writing the factory image",
    )
    FPGA_flash_protect_RW = AttributeWrapper(
        comms_annotation=["FPGA_flash_protect_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
        doc="Protect (0) or unprotect (1) writing the factory image",
    )
    FPGA_flash_erase_R = AttributeWrapper(
        comms_annotation=["FPGA_flash_erase_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_flash_erase_RW = AttributeWrapper(
        comms_annotation=["FPGA_flash_erase_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_scrap_R = AttributeWrapper(
        comms_annotation=["FPGA_scrap_R"],
        datatype=numpy.uint32,
        dims=(N_pn, N_subbands),
        doc="FPGA scratch memory",
    )
    FPGA_scrap_RW = AttributeWrapper(
        comms_annotation=["FPGA_scrap_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, N_subbands),
        access=AttrWriteType.READ_WRITE,
        doc="FPGA scratch memory",
    )

    # Configuration
    FPGA_firmware_version_R = AttributeWrapper(
        comms_annotation=["FPGA_firmware_version_R"], datatype=str, dims=(N_pn,)
    )
    FPGA_hardware_version_R = AttributeWrapper(
        comms_annotation=["FPGA_hardware_version_R"], datatype=str, dims=(N_pn,)
    )
    FPGA_global_node_index_R = AttributeWrapper(
        comms_annotation=["FPGA_global_node_index_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )

    # PPS
    FPGA_pps_present_R = AttributeWrapper(
        comms_annotation=["FPGA_pps_present_R"], datatype=bool, dims=(N_pn,)
    )
    FPGA_pps_capture_cnt_R = AttributeWrapper(
        comms_annotation=["FPGA_pps_capture_cnt_R"], datatype=numpy.uint32, dims=(N_pn,)
    )
    FPGA_pps_error_cnt_R = AttributeWrapper(
        comms_annotation=["FPGA_pps_error_cnt_R"], datatype=numpy.uint32, dims=(N_pn,)
    )
    FPGA_pps_expected_cnt_R = AttributeWrapper(
        comms_annotation=["FPGA_pps_expected_cnt_R"],
        datatype=numpy.uint32,
        dims=(N_pn,),
    )
    FPGA_pps_expected_cnt_RW = AttributeWrapper(
        comms_annotation=["FPGA_pps_expected_cnt_RW"],
        datatype=numpy.uint32,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_time_since_last_pps_R = AttributeWrapper(
        comms_annotation=["FPGA_time_since_last_pps_R"],
        datatype=numpy.double,
        dims=(N_pn,),
    )
    FPGA_monitor_pps_offset_time_R = AttributeWrapper(
        comms_annotation=["FPGA_monitor_pps_offset_time_R"],
        datatype=numpy.double,
        dims=(N_pn,),
    )

    # UCP (Uniboard Control Protocol)
    TR_ucp_disable_communication_R = AttributeWrapper(
        comms_annotation=["TR_ucp_disable_communication_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    TR_ucp_disable_communication_RW = AttributeWrapper(
        comms_annotation=["TR_ucp_disable_communication_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    TR_ucp_reset_counters_R = AttributeWrapper(
        comms_annotation=["TR_ucp_reset_counters_R"],
        datatype=bool,
        dims=(N_pn,),
    )
    TR_ucp_reset_counters_RW = AttributeWrapper(
        comms_annotation=["TR_ucp_reset_counters_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    TR_ucp_nof_reads_R = AttributeWrapper(
        comms_annotation=["TR_ucp_nof_reads_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )
    TR_ucp_nof_read_retries_R = AttributeWrapper(
        comms_annotation=["TR_ucp_nof_read_retries_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )
    TR_ucp_nof_read_failures_R = AttributeWrapper(
        comms_annotation=["TR_ucp_nof_read_failures_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )
    TR_ucp_nof_writes_R = AttributeWrapper(
        comms_annotation=["TR_ucp_nof_writes_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )
    TR_ucp_nof_write_retries_R = AttributeWrapper(
        comms_annotation=["TR_ucp_nof_write_retries_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )
    TR_ucp_nof_write_failures_R = AttributeWrapper(
        comms_annotation=["TR_ucp_nof_write_failures_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )

    # Monitoring
    FPGA_temp_R = AttributeWrapper(
        comms_annotation=["FPGA_temp_R"], datatype=numpy.double, dims=(N_pn,)
    )

    @attribute(
        access=AttrWriteType.READ,
        dtype=str,
        fisallowed="is_attribute_access_allowed",
    )
    def SDP_device_R(self):
        return self.control.child(DeviceTypes.SDP).dev_name()

    clock_RW = attribute(
        doc="Configured sampling clock (Hz)",
        unit="Hz",
        dtype=numpy.uint32,
        access=AttrWriteType.READ_WRITE,
        fisallowed="is_attribute_access_allowed",
    )

    def read_clock_RW(self):
        # We can only return a single value, so we assume the FPGA is configured coherently.
        # Which is something that is to be checked by an independent monitoring system anyway.
        mask = self.read_attribute(
            "TR_fpga_mask_RW",
        )
        clocks = self.read_attribute("FPGA_pps_expected_cnt_RW")
        clocks_in_mask = [clock for idx, clock in enumerate(clocks) if mask[idx]]

        # We return first setting within the mask.
        # If there are no FPGAs selected at all, just return a sane default.
        return (
            numpy.uint32(clocks_in_mask[0]) if clocks_in_mask else self.clock_RW_default
        )

    def write_clock_RW(self, clock):
        if clock not in (CLK_160_MHZ, CLK_200_MHZ):
            raise ValueError(f"Unsupported clock frequency: {clock}")

        # Tell all FPGAs to use this clock
        self.proxy.FPGA_pps_expected_cnt_RW = [clock] * N_pn

    # ----------
    # Summarising Attributes
    # ----------

    @attribute(
        doc="Number of signal inputs processed by the FPGAs",
        dtype=numpy.uint32,
        fisallowed="is_attribute_access_allowed",
    )
    def nr_signal_inputs_R(self):
        return numpy.uint32(self.read_attribute("TR_sdp_config_nof_fpgas_R") * S_pn)

    @attribute(
        doc="The signal index of the first signal input processed by the FPGAs",
        dtype=numpy.uint32,
        fisallowed="is_attribute_access_allowed",
    )
    def first_signal_input_index_R(self):
        return numpy.uint32(
            self.read_attribute("TR_sdp_config_first_fpga_nr_R") % N_pn * S_pn
        )

    FPGA_error_R = attribute(
        dtype=(bool,), max_dim_x=N_pn, fisallowed="is_attribute_access_allowed"
    )

    def read_FPGA_error_R(self):
        return self.read_attribute("TR_fpga_mask_R") & (
            self.read_attribute("TR_fpga_communication_error_R")
            | (self.read_attribute("FPGA_firmware_version_R") == "")
            # we cannot assume all inputs of an FPGA are working until we have a mask for it
            # | (self.read_attribute("FPGA_jesd204b_csr_dev_syncn_R") == 0).any(axis=1)
        )

    # --------
    # overloaded functions
    # --------

    def _boot_to_image(self, image_nr):
        # FPGAs that are actually reachable and we care about
        wait_for = self.read_attribute("TR_fpga_mask_R")

        # Wait for the current image to be booted (in case we connected to SDPTR before even the current
        # image finished booting).
        self.wait_attribute(
            "TR_fpga_communication_error_R",
            lambda attr: (~attr | ~wait_for).all(),
            self.Firmware_Boot_timeout,
        )

        # Order the correct firmare to be loaded
        self.proxy.FPGA_boot_image_RW = [image_nr] * N_pn

        # Wait for the firmware to be loaded (ignoring masked out elements)
        self.wait_attribute(
            "FPGA_boot_image_R",
            lambda attr: ((attr == image_nr) | ~wait_for).all(),
            self.Firmware_Boot_timeout,
        )

        # Wait for the new image to be booted
        self.wait_attribute(
            "TR_fpga_communication_error_R",
            lambda attr: (~attr | ~wait_for).all(),
            self.Firmware_Boot_timeout,
        )

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""

        boot_image = self.read_attribute("FPGA_boot_image_R")
        mask = self.read_attribute("TR_fpga_mask_R")

        try:
            return numpy.count_nonzero((boot_image == 1) & mask) / numpy.count_nonzero(
                mask
            )
        except ZeroDivisionError:
            return 1.0

    def _power_hardware_on(self):
        """Boot the SDP Firmware user image"""

        self._boot_to_image(1)

    def _power_hardware_off(self):
        """Use the SDP Firmware factory image"""

        self._boot_to_image(0)

    # --------
    # Commands
    # --------
