#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""DigitalBeam Device Server for LOFAR2.0"""

import logging

import numpy

# PyTango imports
from tango.server import attribute, AttrWriteType
from tangostationcontrol.beam.delays import Delays
from tangostationcontrol.beam.managers import DigitalBeamManager
from tangostationcontrol.common.antennas import antenna_set_to_mask
from tangostationcontrol.common.constants import (
    N_beamlets_ctrl,
    N_xyz,
    MAX_ANTENNA,
)

# Additional import
from tangostationcontrol.common.device_decorators import log_exceptions
from tangostationcontrol.devices.base_device_classes.beam_device import BeamDevice
from tangostationcontrol.devices.types import DeviceTypes
from tangostationcontrol.metrics import device_metrics

logger = logging.getLogger()

__all__ = ["DigitalBeam"]


@device_metrics(
    exclude=[
        "*_RW",
    ]
)
class DigitalBeam(BeamDevice):
    """Manages Digital Beamforming on the SDP, in which the FPGA combines
    its digital inputs from the RCUs into one or more beamlets, that
    are emitted through the Beamlet device.

    We distinguish:
      * antennas:         the elements in the AntennaField that feeds the FPGA,
      * inputs:           the antenna slots of the FPGAs (covering both X and Y as one),
      * polarised_inputs: all the input slots of the FPGAs (separating X and Y).

    The antennas are drawn from an AntennaField device. Only the antennas enabled
    in the AntennaField's Antenna_Usage_Mask will be used in beamforming. Those
    disabled in the mask will get a weight of 0.

    We always control all of the beamlets of the target Beamlet device.

    This device using asyncio greenmode take care with async methods and awaiting them
    see :py:class:`BeamDevice` and :py:class:`AsyncDevice` for details.
    """

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    Antenna_Set_RW = attribute(
        doc="Name of the antenna set to use for beam forming",
        dtype=str,
        access=AttrWriteType.READ_WRITE,
    )

    Antenna_Mask_R = attribute(
        doc="Antennas selected for beam forming through Antenna_Set_RW",
        dtype=(bool,),
        max_dim_x=MAX_ANTENNA,
    )

    Antenna_Usage_Mask_R = attribute(
        doc="Antennas used for beam forming. Excludes antennas outside "
        "the antenna set, broken antennas, unconnected antennas, etc.",
        dtype=(bool,),
        max_dim_x=MAX_ANTENNA,
        fisallowed="is_attribute_access_allowed",
        fget=lambda self: self._beam_manager.antennas_used(),
    )

    nr_antennas_R = attribute(
        doc="Number of configured antennas from the associated antenna field.",
        dtype=numpy.uint32,
        fget="nr_antennas",
    )

    nr_beamlets_R = attribute(
        doc="Number of controlled beamlets",
        dtype=numpy.uint32,
        fget="nr_beamlets",
    )

    subband_select_RW = attribute(
        dtype=(numpy.uint32,),
        max_dim_x=N_beamlets_ctrl,
        access=AttrWriteType.READ_WRITE,
        fisallowed="is_attribute_access_allowed",
        abs_change="1",
    )

    def read_subband_select_RW(self):
        return self.beamlet.subband_select_RW

    def write_subband_select_RW(self, subbands):
        self.beamlet.subband_select_RW = subbands

    def nr_antennas(self):
        """Return the number of configured antennas."""

        return self.control.read_parent_attribute("nr_antennas_R")

    def nr_beamlets(self):
        """Return the number of controlled beamlets."""

        return N_beamlets_ctrl

    def antenna_setlist(self):
        """Return the string representation of officially offered set of antennas"""

        return self.control.read_parent_attribute("Antenna_Sets_R")

    def antenna_set_masks(self):
        """Return string encoding of the corresponding antenna masks for the antenna field"""

        return self.control.read_parent_attribute("Antenna_Set_Masks_R")

    def read_Antenna_Set_RW(self):
        return self._antenna_set

    def write_Antenna_Set_RW(self, antenna_set: str):
        # make sure this antenna set is supported
        _ = antenna_set_to_mask(
            antenna_set,
            self.nr_antennas(),
            self.antenna_setlist(),
            self.antenna_set_masks(),
        )

        self._antenna_set = antenna_set

    def read_Antenna_Mask_R(self):
        return antenna_set_to_mask(
            self._antenna_set,
            self.nr_antennas(),
            self.antenna_setlist(),
            self.antenna_set_masks(),
        )

    # ----------
    # Summarising Attributes
    # ----------

    # --------
    # Overloaded functions
    # --------

    def __init__(self, cl, name):
        self.parent = None
        self.beamlet = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    def _init_device(self):
        super()._init_device(DigitalBeamManager(self))

    @log_exceptions()
    def configure_for_initialise(self):
        super().configure_for_initialise(self.nr_beamlets())

        self.parent = self.control.parent()
        self.beamlet = self.control.child(DeviceTypes.Beamlet)

        # Use all antennas by default
        self.write_Antenna_Set_RW("ALL")

        # Retrieve positions from RECV device
        reference_itrf = self.control.read_parent_attribute(
            "Antenna_Field_Reference_ITRF_R"
        )
        antenna_itrf = self.control.read_parent_attribute(
            "Antenna_Reference_ITRF_R"
        ).reshape(-1, N_xyz)

        # a delay calculator
        self._beam_manager.delay_calculator = Delays(reference_itrf)

        # relative positions of each antenna
        self._beam_manager.relative_antenna_positions = antenna_itrf - reference_itrf

    def configure_for_off(self):
        # Turn off BeamTracker first
        super().configure_for_off()

        if self._beam_manager and self._beam_manager.delay_calculator:
            self._beam_manager.delay_calculator.stop()
