#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Beamlet Device Server for LOFAR2.0"""

import logging
from functools import lru_cache
from typing import List, Dict, Tuple

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import (
    AttrWriteType,
    DevVarFloatArray,
    DevVarULongArray,
    Util,
)

# PyTango imports
from tango.server import device_property, command, attribute
from tangostationcontrol.common.constants import (
    N_pn,
    A_pn,
    N_pol,
    N_beamlets_ctrl,
    N_beamsets_ctrl,
    N_bdo_destinations_mm,
    P_sum,
    DEFAULT_SUBBAND,
    N_subbands,
)

# Additional import
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.sdp import phases_to_weights
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.metrics import device_metrics

__all__ = ["Beamlet"]

logger = logging.getLogger()


@device_metrics(
    exclude=[
        "beamlet_frequency_R",
        "FPGA_beamlet_subband_select_*",
        "FPGA_bf_weights_*",
        "FPGA_beamlet_output_hdr_*",
        "FPGA_beamlet_output_multiple_hdr_*",
        "*_RW",
    ]
)
class Beamlet(OPCUADevice):
    """Beamlet Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    FPGA_beamlet_output_hdr_eth_source_mac_RW_default = device_property(
        dtype="DevVarStringArray", mandatory=True
    )

    FPGA_beamlet_output_hdr_ip_source_address_RW_default = device_property(
        dtype="DevVarStringArray", mandatory=True
    )

    FPGA_beamlet_output_hdr_udp_source_port_RW_default = device_property(
        dtype="DevVarUShortArray", mandatory=True
    )

    FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW_default_shorthand = device_property(
        doc="MAC addresses for each output stream. Will be set to this value for all FPGAs, augmented with blank values.",
        dtype="DevVarStringArray",
        mandatory=True,
    )

    FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW_default_shorthand = device_property(
        doc="IP addresses for each output stream. Will be set to this value for all FPGAs, augmented with blank values.",
        dtype="DevVarStringArray",
        mandatory=True,
    )

    FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW_default_shorthand = device_property(
        doc="UDP ports for each output stream. Will be set to this value for all FPGAs, augmented with blank values.",
        dtype="DevVarUShortArray",
        mandatory=True,
    )

    FPGA_beamlet_output_enable_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[False] * N_pn
    )

    FPGA_beamlet_output_scale_RW_default = device_property(
        dtype="DevVarDoubleArray", mandatory=False, default_value=[1.0] * N_pn
    )

    FPGA_bf_ring_nof_transport_hops_RW_default = device_property(
        dtype="DevVarUShortArray", mandatory=False, default_value=[1] * N_pn
    )

    FPGA_bf_weights_xy_yx_RW_default = device_property(
        dtype="DevVarULongArray",
        mandatory=False,
        default_value=[0] * N_pn * A_pn * N_pol * N_beamlets_ctrl,
    )

    subband_select_RW_default = device_property(
        dtype="DevVarULongArray",
        mandatory=False,
        default_value=[DEFAULT_SUBBAND] * N_beamlets_ctrl,
    )

    SDP_device = device_property(
        dtype=str,
        doc="Which SDP device subscribed for this beamlet",
        mandatory=False,
        default_value="STAT/SDP/HBA",
    )

    FIRST_DEFAULT_SETTINGS = [
        "FPGA_beamlet_output_hdr_eth_source_mac_RW",
        "FPGA_beamlet_output_hdr_ip_source_address_RW",
        "FPGA_beamlet_output_hdr_udp_source_port_RW",
        "FPGA_beamlet_output_enable_RW",
        "FPGA_bf_weights_xy_yx_RW",
    ]

    # ----------
    # Attributes
    # ----------

    FPGA_beamlet_output_enable_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_enable_R"], datatype=bool, dims=(N_pn,)
    )
    FPGA_beamlet_output_enable_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_enable_RW"],
        datatype=bool,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_beamlet_output_hdr_eth_source_mac_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_eth_source_mac_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_hdr_eth_source_mac_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_eth_source_mac_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_hdr_ip_source_address_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_ip_source_address_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_hdr_ip_source_address_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_ip_source_address_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_hdr_udp_source_port_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_udp_source_port_R"],
        datatype=numpy.uint16,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_hdr_udp_source_port_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_udp_source_port_RW"],
        datatype=numpy.uint16,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_beamlet_output_hdr_eth_destination_mac_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_eth_destination_mac_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_hdr_eth_destination_mac_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_eth_destination_mac_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_hdr_ip_destination_address_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_ip_destination_address_R"],
        datatype=str,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_hdr_ip_destination_address_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_ip_destination_address_RW"],
        datatype=str,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_hdr_udp_destination_port_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_udp_destination_port_R"],
        datatype=numpy.uint16,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_hdr_udp_destination_port_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_hdr_udp_destination_port_RW"],
        datatype=numpy.uint16,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_beamlet_output_nof_destinations_act_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_nof_destinations_act_R"],
        datatype=numpy.uint8,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_nof_destinations_max_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_nof_destinations_max_R"],
        datatype=numpy.uint8,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_nof_blocks_per_packet_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_nof_blocks_per_packet_R"],
        datatype=numpy.uint8,
        dims=(N_pn,),
    )

    FPGA_beamlet_output_multiple_hdr_eth_destination_mac_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_multiple_hdr_eth_destination_mac_R"],
        datatype=str,
        dims=(N_pn, N_bdo_destinations_mm),
    )
    FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW"],
        datatype=str,
        dims=(N_pn, N_bdo_destinations_mm),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_multiple_hdr_ip_destination_address_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_multiple_hdr_ip_destination_address_R"],
        datatype=str,
        dims=(N_pn, N_bdo_destinations_mm),
    )
    FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW"],
        datatype=str,
        dims=(N_pn, N_bdo_destinations_mm),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_multiple_hdr_udp_destination_port_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_multiple_hdr_udp_destination_port_R"],
        datatype=numpy.uint16,
        dims=(N_pn, N_bdo_destinations_mm),
    )
    FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW"],
        datatype=numpy.uint16,
        dims=(N_pn, N_bdo_destinations_mm),
        access=AttrWriteType.READ_WRITE,
    )

    FPGA_beamlet_output_scale_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_scale_R"],
        datatype=numpy.double,
        dims=(N_pn,),
    )
    FPGA_beamlet_output_scale_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_scale_RW"],
        datatype=numpy.double,
        dims=(N_pn,),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_beamlet_output_bsn_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_beamsets_ctrl),
    )

    FPGA_beamlet_output_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_beamlet_output_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )

    # boolean[N_pn][N_beamsets_ctrl]
    FPGA_beamlet_output_ready_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_ready_R"],
        datatype=bool,
        dims=(N_pn, N_beamsets_ctrl),
    )
    # boolean[N_pn][N_beamsets_ctrl]
    FPGA_beamlet_output_xon_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_output_xon_R"],
        datatype=bool,
        dims=(N_pn, N_beamsets_ctrl),
    )

    FPGA_beamlet_output_10gbe_tx_nof_frames_R = AttributeWrapper(
        doc="Number of frames emitted over 10GbE",
        comms_annotation=["FPGA_beamlet_output_10gbe_tx_nof_frames_R"],
        datatype=numpy.uint64,
        dims=(N_pn,),
    )

    # uint16[N_pn][A_PN][N_POL][N_beamsets_ctrl]
    # Select subband per dual-polarisation beamlet.
    # 0 for antenna polarization X in beamlet polarization X,
    # 1 for antenna polarization Y in beamlet polarization Y.
    FPGA_beamlet_subband_select_R = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_subband_select_R"],
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_pol, N_beamlets_ctrl),
    )
    FPGA_beamlet_subband_select_RW = AttributeWrapper(
        comms_annotation=["FPGA_beamlet_subband_select_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_pol, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # uint32[N_pn][N_beamset_ctrl]
    FPGA_bf_ring_nof_transport_hops_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_nof_transport_hops_R"],
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_nof_transport_hops_RW = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_nof_transport_hops_RW"],
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bf_ring_rx_clear_total_counts_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_clear_total_counts_R"],
        datatype=bool,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_clear_total_counts_RW = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_clear_total_counts_RW"],
        datatype=bool,
        dims=(N_pn, N_beamsets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # Beam Former Weights raw data
    FPGA_bf_weights_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_weights_R"],
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl, N_pol, A_pn, N_pol, N_beamlets_ctrl),
    )
    FPGA_bf_weights_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl, N_pol, A_pn, N_pol, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # cint16[N_pn][A_pn][N_pol][N_beamlets_ctrl]
    # Co-polarization BF weights. The N_pol = 2 parameter index is:
    # 0 for antenna polarization X in beamlet polarization X,
    # 1 for antenna polarization Y in beamlet polarization Y.
    FPGA_bf_weights_xx_yy_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xx_yy_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_pol, N_beamlets_ctrl),
    )
    FPGA_bf_weights_xx_yy_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xx_yy_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_pol, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # cint16[N_pn][A_pn][N_pol][N_beamlets_ctrl]
    # Cross-polarization BF weights. The N_pol = 2 parameter index is
    # (note that index pol in range 0:N_pol-1 is the antenna polarization,
    # so index !pol is the beamlet polarization):
    # 0 for antenna polarization X in beamlet polarization Y,
    # 1 for antenna polarization Y in beamlet polarization X.
    FPGA_bf_weights_xy_yx_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xy_yx_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(A_pn * N_pol * N_beamlets_ctrl, N_pn),
    )
    FPGA_bf_weights_xy_yx_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xy_yx_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(A_pn * N_pol * N_beamlets_ctrl, N_pn),
        access=AttrWriteType.READ_WRITE,
    )

    # cint16[N_pn][N_pol][A_pn][N_pol][N_beamlets_ctrl]
    # Full Jones matrix of BF weights.
    FPGA_bf_weights_xx_xy_yx_yy_R = AttributeWrapper(
        comms_annotation={
            "path": ["FPGA_bf_weights_xx_xy_yx_yy_R"],
            "log_writes": False,
        },
        datatype=numpy.uint32,
        dims=(N_pn, N_pol, A_pn, N_pol, N_beamlets_ctrl),
    )
    FPGA_bf_weights_xx_xy_yx_yy_RW = AttributeWrapper(
        comms_annotation={
            "path": ["FPGA_bf_weights_xx_xy_yx_yy_RW"],
            "log_writes": False,
        },
        datatype=numpy.uint32,
        dims=(N_pn, N_pol, A_pn, N_pol, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # cint16[N_pn][A_pn][N_beamlets_ctrl]
    # BF weights for separate access to respectively w_xx, w_xy, w_yx, and w_yy.
    FPGA_bf_weights_xx_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xx_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
    )
    FPGA_bf_weights_xx_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xx_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn * N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bf_weights_xy_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xy_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
    )
    FPGA_bf_weights_xy_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_xy_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bf_weights_yx_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_yx_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
    )
    FPGA_bf_weights_yx_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_yx_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )
    FPGA_bf_weights_yy_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_yy_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
    )
    FPGA_bf_weights_yy_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_yy_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # Co-polarization BF weights, using same weights for
    # both X-polarization and Y-polarization, so w_xx = w_yy = w_pp
    FPGA_bf_weights_pp_R = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_pp_R"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
    )
    FPGA_bf_weights_pp_RW = AttributeWrapper(
        comms_annotation={"path": ["FPGA_bf_weights_pp_RW"], "log_writes": False},
        datatype=numpy.uint32,
        dims=(N_pn, A_pn, N_beamlets_ctrl),
        access=AttrWriteType.READ_WRITE,
    )

    # boolean[N_pn][N_beamsets_ctrl][P_sum]
    FPGA_bf_rx_align_stream_enable_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_stream_enable_R"],
        datatype=bool,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
    )
    FPGA_bf_rx_align_stream_enable_RW = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_stream_enable_RW"],
        datatype=bool,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
        access=AttrWriteType.READ_WRITE,
    )

    # int64[N_pn][N_beamsets_ctrl][P_sum]
    FPGA_bf_rx_align_bsn_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
    )

    # int32[N_pn][N_beamsets_ctrl][P_sum]
    FPGA_bf_rx_align_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
    )
    FPGA_bf_rx_align_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
    )
    FPGA_bf_rx_align_latency_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
    )
    FPGA_bf_rx_align_nof_replaced_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_rx_align_nof_replaced_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl, P_sum),
    )

    FPGA_bf_aligned_bsn_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_aligned_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_aligned_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_aligned_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_aligned_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_aligned_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_aligned_latency_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_aligned_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_tx_bsn_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_tx_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_tx_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_tx_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_tx_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_tx_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_tx_latency_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_tx_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )

    FPGA_bf_ring_rx_total_nof_packets_received_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_total_nof_packets_received_R"],
        datatype=numpy.uint64,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_total_nof_packets_discarded_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_total_nof_packets_discarded_R"],
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_total_nof_sync_received_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_total_nof_sync_received_R"],
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_total_nof_sync_discarded_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_total_nof_sync_discarded_R"],
        datatype=numpy.uint32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_bsn_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_bsn_R"],
        datatype=numpy.int64,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_nof_packets_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_nof_packets_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_nof_valid_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_nof_valid_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )
    FPGA_bf_ring_rx_latency_R = AttributeWrapper(
        comms_annotation=["FPGA_bf_ring_rx_latency_R"],
        datatype=numpy.int32,
        dims=(N_pn, N_beamsets_ctrl),
    )

    subband_select_RW = attribute(
        dtype=(numpy.uint32,),
        max_dim_x=N_beamlets_ctrl,
        access=AttrWriteType.READ_WRITE,
        fisallowed="is_attribute_access_allowed",
    )

    beamlet_frequency_R = attribute(
        doc="Frequency of each beamlet for each input",
        unit="Hz",
        dtype=((numpy.float64,),),
        max_dim_y=N_pn * A_pn,
        max_dim_x=N_beamlets_ctrl,
        fisallowed="is_attribute_access_allowed",
    )

    def _get_defaults(self, properties: Dict[str, object]) -> List[Tuple[str, object]]:
        return super()._get_defaults(properties) + self._beamlet_output_defaults(
            properties
        )

    @staticmethod
    def _beamlet_output_defaults(properties) -> Dict[str, object]:
        """Return the defaults for FPGA_beamlet_output_multiple_hdr_*
        based on shorthand defaults."""

        # --- Configure FPGA destination streams equally for all 16 FPGAs
        default_settings = []

        # Set MAC, IP, port
        for setting, value_if_undefined in [
            (
                "FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW",
                "00:00:00:00:00:00",
            ),
            ("FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW", "0.0.0.0"),
            ("FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW", 0),
        ]:
            # obtain shorthand description (first values per fpga)
            shorthand_value = properties[f"{setting}_default_shorthand"]

            # construct value for all FPGAs, extending the given values with blanks
            # to obtain the required array shape.
            value_per_fpga = [value_if_undefined] * N_bdo_destinations_mm
            value_per_fpga[: len(shorthand_value)] = shorthand_value
            value = [value_per_fpga] * N_pn

            # add the value to set
            default_settings.append((setting, value))

        return default_settings

    def read_subband_select_RW(self):
        # We can only return a single value, so we assume the FPGA is configured coherently.
        # Which is something that is to be checked by an independent monitoring system anyway.
        mask = self.sdpfirmware_proxy.TR_fpga_mask_RW
        subbands = self.read_attribute("FPGA_beamlet_subband_select_RW")
        subbands_in_mask = [s for idx, s in enumerate(subbands) if mask[idx]]

        # If there are no FPGAs selected at all, just return a sane default.
        if not subbands_in_mask:
            return self.subband_select_RW_default

        # We return the first setting within the mask. Convert into actual shape for a single FPGA
        mask_for_all_inputs = subbands_in_mask[0].reshape(A_pn, N_pol, N_beamlets_ctrl)

        # Return the first setting (antenna) within this FPGA
        return mask_for_all_inputs[0, 0]

    def write_subband_select_RW(self, subbands):
        # Use the same subband for all inputs and polarisations of a beamlet
        self.proxy.FPGA_beamlet_subband_select_RW = numpy.tile(
            subbands, (N_pn, A_pn * N_pol)
        )

        self.cache_clear()

    # ----------
    # Summarising Attributes
    # ----------

    # --------
    # Overloaded functions
    # --------

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""
        return 1.0 * any(self.read_attribute("FPGA_beamlet_output_enable_R"))

    def configure_for_initialise(self):
        super().configure_for_initialise()

        # TODO(L2SS-1364): Ensure beamlet does not need to read sdpfirmware attribute
        util = Util.instance()
        instance_type = self.get_name().split("/")[2]
        self.sdpfirmware_proxy = create_device_proxy(
            f"{util.get_ds_inst_name()}/SDPFirmware/{instance_type}"
        )

        # TODO(L2SS-1364): Ensure beamlet does not need to subscribe sdp attribute
        self.sdp_proxy = create_device_proxy(self.SDP_device)

        # subscribe to events to notice setting changes in SDP that determine the input frequency
        self.events.subscribe_change_event(
            self.sdp_proxy, "subband_frequency_R", self._frequency_change_event
        )

    # --------
    # internal functions
    # --------

    def _frequency_change_event(self, device, attribute_name, value):
        """Trigger on external changes in frequency settings."""

        # invalidate caches for frequency-dependent values
        self.cache_clear()

    @command()
    def cache_clear(self):
        """Explicitly clear any caches."""

        self.beamlet_frequencies.cache_clear()

    """
       The SDP FPGAs correct for signal-delay differences by rotating the phases of the antenna signals. A delay
       is converted to a phase difference as follows (NB: applying a delay means rotating the signal backwards):

           phase = -2 * pi * frequency * delay

       where 'frequency' is the subband frequency:

           LBA: frequency = (subband_nr +   0) * clock / 1024
           HBA: frequency = (subband_nr + 512) * clock / 1024

       The beamformer combines a set of antennas for each beamlet, and each beamlet can have a different pointing
       and subband selected.

       This results in an array of phase[antenna][beamlet] adjustments to be sent to the FPGA.The FPGA accepts
       weights as a 16-bit (imag,real) complex pair packed into an uint32.

       The phases, delays, and final beam weights, all have shape (fpga_nr, [input_nr][pol][beamlet_nr]).
    """

    @staticmethod
    def _beamlet_frequencies(
        beamlet_subbands: numpy.ndarray, subband_frequencies: numpy.ndarray
    ) -> numpy.ndarray:
        def frequencies_per_input(fpga_nr, antenna_nr):
            """Return the frequencies for the selected subbands of the given input."""
            return numpy.take(
                subband_frequencies[fpga_nr, antenna_nr, 0, :],
                beamlet_subbands[
                    fpga_nr, antenna_nr, 0, :
                ],  # we use X pol for reference
            )

        # compute all frequencies for all inputs.
        # NB: since we assume both polarisations of each antenna have the same frequency
        # in order to use the FPGA_bf_weights_pp_RW, we use the X polarisation for reference
        frequencies = numpy.array(
            [
                frequencies_per_input(fpga_nr, antenna_nr)
                for fpga_nr in range(N_pn)
                for antenna_nr in range(A_pn)
            ],
            dtype=numpy.float64,
        )

        return frequencies.reshape(N_pn, A_pn, N_beamlets_ctrl)

    @lru_cache()  # this function requires large hardware reads for values that don't change often
    def beamlet_frequencies(self):
        """Obtain the frequencies (in Hz) of each subband
        that is selected for each antenna and beamlet.

        Returns shape (fpga_nr, input_nr, beamlet_nr), so one value per antenna, not per input.
        This makes the result directly usable for FPGA_bf_weights_pp_RW.
        """

        # obtain which subband is selected for each input and beamlet
        beamlet_subbands = self.read_attribute(
            "FPGA_beamlet_subband_select_RW"
        ).reshape(
            N_pn, A_pn, N_pol, N_beamlets_ctrl
        )  # orig: (fpga_nr, [input_nr][pol][beamlet_nr])
        subband_frequencies = self.sdp_proxy.subband_frequency_R.reshape(
            N_pn, A_pn, N_pol, N_subbands
        )  # orig: ([fpga_nr][input_nr], subband_nr)

        return self._beamlet_frequencies(beamlet_subbands, subband_frequencies)

    @staticmethod
    def _calculate_bf_weights(
        delays: numpy.ndarray, beamlet_frequencies: numpy.ndarray
    ):
        """Helper function that converts a series of delays into FPGA_bf_weights_pp.

        All input and output arrays have the same dimensionality.
        """

        # compute the phases

        # correcting for a delay means rotating *backwards*
        beamlet_phases = (-2.0 * numpy.pi) * beamlet_frequencies * delays

        # convert to weights
        bf_weights = phases_to_weights(beamlet_phases)

        return bf_weights

    def read_beamlet_frequency_R(self):
        return self.beamlet_frequencies().reshape(N_pn * A_pn, N_beamlets_ctrl)

    # --------
    # Commands
    # --------

    @command(dtype_in=DevVarFloatArray, dtype_out=DevVarULongArray)
    def calculate_bf_weights(self, delays: numpy.ndarray):
        """converts a difference in delays (in seconds) to a FPGA weight (in complex number)"""

        # Calculate the FPGA weight array
        delays = delays.reshape(N_pn, A_pn, N_beamlets_ctrl)
        beamlet_frequencies = self.beamlet_frequencies()
        bf_weights = self._calculate_bf_weights(delays, beamlet_frequencies)

        return bf_weights.flatten()
