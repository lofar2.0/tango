#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""PCON Device Server for LOFAR2.0"""

import logging

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tangostationcontrol.clients.snmp.attribute_classes import PCON_sim

# Additional import
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)
from tangostationcontrol.devices.base_device_classes.snmp_device import SNMPDevice
from tangostationcontrol.metrics import device_metrics

logger = logging.getLogger()

__all__ = ["PCON"]


@device_logging_to_python()
@device_metrics()
class PCON(SNMPDevice):
    """PCON Device Server for LOFAR2.0"""

    powerSystemStatus_R = AttributeWrapper(
        comms_annotation={"mib": "SP2-MIB", "name": "powerSystemStatus"},
        datatype=numpy.int32,
    )

    powerSystemCompany_R = AttributeWrapper(
        comms_annotation={"mib": "SP2-MIB", "name": "powerSystemCompany"},
        datatype=str,
    )

    # --------
    # overloaded functions
    # --------

    def configure_for_initialise(self):
        """user code here. is called when the state is set to STANDBY"""

        super().configure_for_initialise(simulator_class=PCON_sim)
