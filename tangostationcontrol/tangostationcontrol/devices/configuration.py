#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Configuration Device Server for LOFAR2.0

Handles and exposes the station configuration

"""
import json
import logging

# PyTango imports
from tango import AttrWriteType, Database, DebugIt, DevString
from tango.server import attribute, command, device_property

# Additional import
from tangostationcontrol.common.configuration import StationConfiguration
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.common.device_decorators import only_in_states

logger = logging.getLogger()

__all__ = ["Configuration"]


@device_logging_to_python()
class Configuration(LOFARDevice):
    """Manages the main operations relative to the Station Configuration
    which is stored in the TangoDB"""

    # -----------------
    # Device Properties
    # -----------------

    Backup_Station_Configuration = device_property(
        doc="Backup of the previous station configuration, as a JSON string",
        dtype="DevString",
        mandatory=False,
        default_value="""{ "servers": {} }""",
    )

    # ----------
    # Attributes
    # ----------
    station_configuration_RW = attribute(
        dtype=str,
        access=AttrWriteType.READ_WRITE,
        doc="The Tango properties of all the devices in this station, as a JSON string.",
    )

    backup_station_configuration_R = attribute(
        dtype=str,
        access=AttrWriteType.READ,
        doc="The backup of the previous station configuration, as a JSON string",
    )

    def read_station_configuration_RW(self):
        return self._dump_configdb()

    def write_station_configuration_RW(self, station_configuration):
        self._backup_configdb()
        # Remove property to prevent recursive behaviour
        station_configuration_dict = self.station_configuration.remove_backup_property(
            json.loads(station_configuration)
        )
        station_configuration = json.dumps(
            station_configuration_dict,
            ensure_ascii=False,
            separators=(",", ":"),
            sort_keys=True,
        )
        self._load_configdb(station_configuration)

    def read_backup_station_configuration_R(self):
        return self.Backup_Station_Configuration

    def _dump_configdb(self):
        """Returns the TangoDB station configuration as a JSON string"""
        dbdata = self.station_configuration.get_tangodb_data()
        # Remove property to prevent recursive behaviour
        dbdata = self.station_configuration.remove_backup_property(dbdata)
        return json.dumps(dbdata, ensure_ascii=False, indent=4, sort_keys=True)

    def _load_configdb(self, station_configuration):
        """Takes a JSON string which represents the station configuration
        and loads the whole configuration from scratch.

        N.B. it does not update, it loads a full new configuration.
        """
        self.station_configuration.load_configdb(station_configuration, update=False)

    def _backup_configdb(self):
        """Saves a backup of the actual station configuration as a JSON string,
        before any load/update operations
        """
        station_configuration = self.read_attribute("station_configuration_RW")
        self._write_backup_station_configuration(station_configuration)

    def _write_backup_station_configuration(self, backup_configuration):
        """Write the backup station configuration property"""
        # Remove property to prevent recursive behaviour
        backup_dict = self.station_configuration.remove_backup_property(
            json.loads(backup_configuration)
        )
        self.Backup_Station_Configuration = json.dumps(
            backup_dict, ensure_ascii=False, separators=(",", ":"), sort_keys=True
        )
        self.proxy.put_property(
            {"backup_station_configuration": self.Backup_Station_Configuration}
        )

    # --------
    # Commands
    # --------
    @command(dtype_in=DevString)
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def update_station_configuration(self, station_configuration: str):
        """Takes a JSON string which represents the station configuration
        and upload the whole configuration.

        N.B. it does not delete existing devices, it updates overlapping parameters.
        """
        self._backup_configdb()
        self.station_configuration.update_configdb(station_configuration)

    @command
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def restore_station_configuration(self):
        """
        Restore the station configuration as saved in the backup attribute
        """
        backup_configuration = self.read_attribute("backup_station_configuration_R")
        self.station_configuration.load_configdb(backup_configuration, update=False)

    # --------
    # overloaded functions
    # --------
    def configure_for_initialise(self):
        super().configure_for_initialise()
        self.station_configuration = StationConfiguration(db=Database())
        # Save a configuration backup copy
        tango_db_configuration = json.dumps(
            self.station_configuration.get_tangodb_data(),
            ensure_ascii=False,
            separators=(",", ":"),
            sort_keys=True,
        )
        self._write_backup_station_configuration(tango_db_configuration)
