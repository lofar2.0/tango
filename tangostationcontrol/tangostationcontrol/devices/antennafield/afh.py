# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""High Band Antenna Device Server for LOFAR2.0"""

from math import pi
import numpy

# PyTango imports
from tango import (
    AttrWriteType,
    DevVarFloatArray,
    DevVarLongArray,
)
from tango.server import device_property, attribute, command

from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AF,
)
from tangostationcontrol.beam.hba_tile import HBATAntennaOffsets
from tangostationcontrol.common.constants import (
    N_elements,
    MAX_ANTENNA,
    N_pol,
    N_xyz,
)
from tangostationcontrol.devices.base_device_classes.mapper import (
    MappedAttribute,
)
from tangostationcontrol.metrics import device_metrics

__all__ = ["AFH"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "HBAT_antenna_ITRF_offsets_R",
        "HBAT_BF_delay_steps_*",
        "HBAT_LED_on_*",
        "HBAT_single_element_selection_GENERIC_201512_R",
        "*_RW",
    ]
)
class AFH(AF):
    """
    AntennaField HBAT implements the logic of LOFAR2 High Band Antennas
    """

    ANTENNA_TYPE = "HBA"

    # ----- HBA properties

    HBAT_PQR_rotation_angles_deg = device_property(
        doc='Rotation of each tile in the PQ plane ("horizontal") in degrees.',
        dtype="DevVarFloatArray",
        mandatory=False,
        default_value=[0.0] * MAX_ANTENNA,
    )

    HBAT_base_antenna_offsets = device_property(
        doc="Offsets of the antennas in a HBAT, with respect to its reference "
        "center (16x3).",
        dtype="DevVarFloatArray",
        mandatory=False,
        default_value=HBATAntennaOffsets.HBAT1_BASE_ANTENNA_OFFSETS.flatten(),
    )

    # see also https://github.com/lofar-astron/lofarimaging/blob/9672b52bb9be8f3405e6e3f85701175bdc4bf211/lofarimaging/singlestationutil.py#L43
    HBAT_single_element_selection_GENERIC_201512 = device_property(
        doc="Which element to select when using a single dipole per tile "
        "in the 'GENERIC_201512' strategy",
        dtype="DevVarLongArray",
        mandatory=False,
        default_value=[
            0,
            10,
            4,
            3,
            14,
            0,
            5,
            5,
            3,
            13,
            10,
            3,
            12,
            2,
            7,
            15,
            6,
            14,
            7,
            5,
            7,
            9,
            0,
            15,
        ],
    )

    # ----- RECV mapping

    Power_to_RECV_mapping = device_property(
        dtype=(numpy.int32,),
        doc="The mapping of Antenna power lines to RECV mapping. Each RECV can "
        "handle 96 inputs. The Antenna number is the index and the value shows "
        "to which receiver device it is connected and on which input. The "
        "first integer is the input. The second integer is the RECV id. "
        "Example: [0, 3] = first receiver of property `Control_Children` with "
        "input 3. -1 means that the Antenna is not connected. The property is "
        "stored in a one dimensional structure. It needs to be reshaped to a "
        "list of lists of two items.",
        mandatory=False,
        default_value=[-1] * MAX_ANTENNA * 2,
    )

    # ----- Generic information

    @attribute(
        access=AttrWriteType.READ,
        dtype=(numpy.int32,),
        max_dim_x=MAX_ANTENNA,
    )
    def HBAT_single_element_selection_GENERIC_201512_R(self):
        return numpy.array(
            self.HBAT_single_element_selection_GENERIC_201512, dtype=numpy.int32
        )

    # ----- Attributes mapped on RECV

    HBAT_BF_delay_steps_R = MappedAttribute(
        "HBAT_BF_delay_steps_R",
        dtype=((numpy.int64,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
    )
    HBAT_BF_delay_steps_RW = MappedAttribute(
        "HBAT_BF_delay_steps_RW",
        dtype=((numpy.int64,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
        access=AttrWriteType.READ_WRITE,
    )
    HBAT_BF_delay_steps_stage_RW = MappedAttribute(
        "HBAT_BF_delay_steps_stage_RW",
        dtype=((numpy.int64,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
        access=AttrWriteType.READ_WRITE,
    )
    HBAT_LED_on_R = MappedAttribute(
        "HBAT_LED_on_R",
        dtype=((bool,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
    )
    HBAT_LED_on_RW = MappedAttribute(
        "HBAT_LED_on_RW",
        dtype=((bool,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
        access=AttrWriteType.READ_WRITE,
    )
    HBAT_PWR_LNA_on_R = MappedAttribute(
        "HBAT_PWR_LNA_on_R",
        dtype=((bool,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
    )
    HBAT_PWR_LNA_on_RW = MappedAttribute(
        "HBAT_PWR_LNA_on_RW",
        dtype=((bool,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
        access=AttrWriteType.READ_WRITE,
    )
    HBAT_PWR_on_R = MappedAttribute(
        "HBAT_PWR_on_R",
        dtype=((bool,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
    )
    HBAT_PWR_on_RW = MappedAttribute(
        "HBAT_PWR_on_RW",
        dtype=((bool,),),
        max_dim_x=N_elements * N_pol,
        max_dim_y=MAX_ANTENNA,
        access=AttrWriteType.READ_WRITE,
    )

    # ----- Position information

    HBAT_antenna_ITRF_offsets_R = attribute(
        access=AttrWriteType.READ,
        doc="For each tile, the offsets of the antennas within that, "
        'in ITRF ("iHBADeltas"). True shape: nrtiles x 16 x 3.',
        dtype=((numpy.float64,),),
        max_dim_x=MAX_ANTENNA * N_xyz,
        max_dim_y=MAX_ANTENNA,
    )

    def read_HBAT_antenna_ITRF_offsets_R(self):
        """Returns the ITRF differences between the center of the tile and its
        individual elements, which is a (nrtiles)x16x3 matrix (16 elements with 3
        ITRF coordinates each), returned as a (nrtiles)x48 matrix.

        This takes the relative offsets between the elements in the tiles as
        described in HBAT_base_antenna_offsets. These offsets are in PQR space,
        which is the plane of the station. The tiles are rotated locally in this
        space according to the HBAT_PQR_rotation_angles_deg, and finally translated
        into global ETRS coordinates using the PQR_to_ETRS_rotation_matrix.

        The relative ITRF offsets are the same as relative ETRS offsets.

        NB: In all of this, the absolute position of each tile is actually
        irrelevant, as all the tiles lie on the same plane in ITRF."""

        # the relative offsets between the elements is fixed in
        # HBAT_base_antenna_offsets
        base_antenna_offsets = numpy.array(self.HBAT_base_antenna_offsets).reshape(
            N_elements, N_xyz
        )

        pqr_to_etrs_rotation_matrix = numpy.array(
            self.PQR_to_ETRS_rotation_matrix
        ).reshape(N_xyz, N_xyz)

        # each tile has its own rotation angle, resulting in different offsets per tile
        all_offsets = numpy.array(
            [
                HBATAntennaOffsets.ITRF_offsets(
                    base_antenna_offsets,
                    angle_deg * pi / 180,
                    pqr_to_etrs_rotation_matrix,
                )
                for angle_deg in self.HBAT_PQR_rotation_angles_deg
            ]
        )

        return all_offsets.reshape(-1, N_elements * N_xyz)

    # --------
    # Overloaded functions
    # --------

    # --------
    # Commands
    # --------

    @command(dtype_in=DevVarFloatArray, dtype_out=DevVarLongArray)
    def calculate_HBAT_bf_delay_steps(self, delays: numpy.ndarray):
        num_tiles = self.read_nr_antennas_R()

        delays = delays.reshape(num_tiles, N_elements)

        result_values = numpy.zeros((num_tiles, N_elements * N_pol), dtype=numpy.int64)
        control_mapping = numpy.reshape(self.Control_to_RECV_mapping, (-1, N_pol))

        for recv_idx, recv_proxy in enumerate(self.recv_proxies):
            # collect all delays for this recv_proxy
            recv_result_indices = numpy.where(control_mapping[:, 0] == (recv_idx + 1))
            recv_delays = delays[recv_result_indices]

            if not recv_result_indices:
                # no RCUs are actually used from this recv_proxy
                continue

            # convert them into delay steps
            flatten_delay_steps = numpy.array(
                recv_proxy.calculate_HBAT_bf_delay_steps(recv_delays.flatten()),
                dtype=numpy.int64,
            )
            delay_steps = numpy.reshape(flatten_delay_steps, (-1, N_elements * N_pol))

            # write back into same positions we collected them from
            result_values[recv_result_indices] = delay_steps

        return result_values.flatten()
