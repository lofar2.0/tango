# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Low Band Antenna Device Server for LOFAR2.0"""
import numpy
from tango.server import device_property

from tangostationcontrol.common.lofar_logging import device_logging_to_python

from tangostationcontrol.common.constants import MAX_ANTENNA
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AF,
)
from tangostationcontrol.metrics import device_metrics

__all__ = ["AFL"]


@device_logging_to_python()
@device_metrics()
class AFL(AF):
    """
    AntennaField LBA implements the logic of LOFAR2 Low Band Antennas
    """

    ANTENNA_TYPE = "LBA"

    # ----- RECV mapping

    Power_to_RECV_mapping = device_property(
        dtype=(numpy.int32,),
        doc="The mapping of Antenna power lines to RECV mapping. Each RECV can "
        "handle 96 inputs. The Antenna number is the index and the value shows "
        "to which receiver device it is connected and on which input. The "
        "first integer is the input. The second integer is the RECV id. "
        "Example: [0, 3] = first receiver of property `Control_Children` with "
        "input 3. -1 means that the Antenna is not connected. The property is "
        "stored in a one dimensional structure. It needs to be reshaped to a "
        "list of lists of two items.",
        mandatory=False,
        default_value=[-1] * MAX_ANTENNA * 2,
    )

    # --------
    # Overloaded functions
    # --------
