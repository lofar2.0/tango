#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""CCD Device Server for LOFAR2.0"""

import logging

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType

# PyTango imports
from tango import DebugIt
from tango.server import command, attribute, device_property
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.common.device_decorators import only_in_states
from tangostationcontrol.metrics import device_metrics

# Additional import

logger = logging.getLogger()

__all__ = ["CCD"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "*_RW",
    ]
)
class CCD(OPCUADevice):
    """CCD Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    CCDTR_monitor_rate_RW_default = device_property(
        dtype="DevLong64", mandatory=False, default_value=1
    )

    # ----- Timing values

    CCD_On_Off_timeout = device_property(
        doc="Maximum amount of time to wait after turning CCD on or off",
        dtype="DevFloat",
        mandatory=False,
        default_value=10.0,
    )

    # ----------
    # Attributes
    # ----------
    TR_software_version_R = AttributeWrapper(
        comms_annotation=["TR_software_version_R"], datatype=str
    )
    CCDTR_FAN_I2C_error_R = AttributeWrapper(
        doc="0=good, >0 indicates an I2C communication error",
        comms_annotation=["CCDTR_FAN_I2C_error_R"],
        datatype=numpy.int64,
    )
    CCDTR_I2C_error_R = AttributeWrapper(
        doc="0=good, >0 indicates an I2C communication error",
        comms_annotation=["CCDTR_I2C_error_R"],
        datatype=numpy.int64,
    )
    CCDTR_monitor_rate_R = AttributeWrapper(
        comms_annotation=["CCDTR_monitor_rate_R"],
        datatype=numpy.int64,
        unit="s",
    )
    CCDTR_monitor_rate_RW = AttributeWrapper(
        comms_annotation=["CCDTR_monitor_rate_RW"],
        datatype=numpy.int64,
        access=AttrWriteType.READ_WRITE,
        unit="s",
    )
    CCDTR_translator_busy_R = AttributeWrapper(
        comms_annotation=["CCDTR_translator_busy_R"], datatype=bool
    )
    CCD_BACK_ID_R = AttributeWrapper(
        comms_annotation=["CCD_BACK_ID_R"], datatype=numpy.int64
    )
    CCD_clear_lock_R = AttributeWrapper(
        comms_annotation=["CCD_clear_lock_R"], datatype=bool
    )
    CCD_clear_lock_RW = AttributeWrapper(
        comms_annotation=["CCD_clear_lock_RW"],
        datatype=bool,
        access=AttrWriteType.READ_WRITE,
    )
    CCD_FAN_RPM_R = AttributeWrapper(
        comms_annotation=["CCD_FAN_RPM_R"], datatype=numpy.float64
    )
    CCD_INPUT_10MHz_good_R = AttributeWrapper(
        comms_annotation=["CCD_INPUT_10MHz_good_R"], datatype=bool
    )
    CCD_INPUT_PPS_good_R = AttributeWrapper(
        comms_annotation=["CCD_INPUT_PPS_good_R"], datatype=bool
    )
    CCD_loss_lock_R = AttributeWrapper(
        comms_annotation=["CCD_loss_lock_R"], datatype=bool
    )
    CCD_PCB_ID_R = AttributeWrapper(
        comms_annotation=["CCD_PCB_ID_R"], datatype=numpy.int64
    )
    CCD_PCB_number_R = AttributeWrapper(
        comms_annotation=["CCD_PCB_number_R"], datatype=str
    )
    CCD_PCB_version_R = AttributeWrapper(
        comms_annotation=["CCD_PCB_version_R"], datatype=str
    )
    CCD_PLL_locked_R = AttributeWrapper(
        comms_annotation=["CCD_PLL_locked_R"], datatype=bool
    )
    CCD_PWR_CLK_DIST_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_CLK_DIST_3V3_R"], datatype=numpy.float64
    )
    CCD_PWR_CLK_INPUT_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_CLK_INPUT_3V3_R"], datatype=numpy.float64
    )
    CCD_PWR_CTRL_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_CTRL_3V3_R"], datatype=numpy.float64
    )
    CCD_PWR_OCXO_INPUT_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_OCXO_INPUT_3V3_R"], datatype=numpy.float64
    )
    CCD_PWR_on_R = AttributeWrapper(comms_annotation=["CCD_PWR_on_R"], datatype=bool)
    CCD_PWR_PLL_INPUT_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_PLL_INPUT_3V3_R"], datatype=numpy.float64
    )
    CCD_PWR_PPS_INPUT_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_PPS_INPUT_3V3_R"], datatype=numpy.float64
    )
    CCD_PWR_PPS_OUTPUT_3V3_R = AttributeWrapper(
        comms_annotation=["CCD_PWR_PPS_OUTPUT_3V3_R"], datatype=numpy.float64
    )
    CCD_TEMP_R = AttributeWrapper(
        comms_annotation=["CCD_TEMP_R"], datatype=numpy.float64
    )

    @attribute(dtype=bool, fisallowed="is_attribute_access_allowed")
    def CCD_lock_R(self):
        return not self.read_attribute("CCD_loss_lock_R")

    # ----------
    # Summarising Attributes
    # ----------
    CCD_error_R = attribute(dtype=bool, fisallowed="is_attribute_access_allowed")

    def read_CCD_error_R(self):
        errors = [
            self.read_attribute("CCDTR_I2C_error_R") > 0,
            self.alarm_val("CCD_loss_lock_R"),
            self.read_attribute("CCD_INPUT_10MHz_good_R"),
            not self.read_attribute("CCD_INPUT_10MHz_good_R"),
            not self.read_attribute("CCD_INPUT_PPS_good_R")
            and not self.read_attribute("CCD_clear_lock_R"),
            not self.read_attribute("CCD_PLL_locked_R"),
        ]
        return any(errors)

    CCD_TEMP_error_R = attribute(
        dtype=bool,
        fisallowed="is_attribute_access_allowed",
    )
    CCD_VOUT_error_R = attribute(dtype=bool, fisallowed="is_attribute_access_allowed")

    def read_CCD_TEMP_error_R(self):
        return self.alarm_val("CCD_TEMP_R")

    def read_CCD_VOUT_error_R(self):
        return (
            self.alarm_val("CCD_PWR_CLK_DIST_3V3_R")
            or self.alarm_val("CCD_PWR_CLK_INPUT_3V3_R")
            or self.alarm_val("CCD_PWR_CTRL_3V3_R")
            or self.alarm_val("CCD_PWR_OCXO_INPUT_3V3_R")
            or self.alarm_val("CCD_PWR_PLL_INPUT_3V3_R")
            or self.alarm_val("CCD_PWR_PPS_INPUT_3V3_R")
            or self.alarm_val("CCD_PWR_PPS_OUTPUT_3V3_R")
            or (not self.read_attribute("CCD_PWR_on_R"))
        )

    # --------
    # overloaded functions
    # --------

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def reset_hardware(self):
        """Initialise the CCD hardware."""

        # Cycle clock. Quickly toggling the heater should not cool the heater down too much.
        self.CCD_off()
        self.wait_attribute("CCDTR_translator_busy_R", False, self.CCD_On_Off_timeout)
        self.CCD_on()
        self.wait_attribute("CCDTR_translator_busy_R", False, self.CCD_On_Off_timeout)

        if not self.read_attribute("CCD_PLL_locked_R"):
            if self.read_attribute("CCDTR_I2C_error_R"):
                raise Exception("I2C is not working.")
            logger.warning(
                "CCD not locked, this may indicate the clock has not yet warmed up"
            )

    def _disable_hardware(self):
        """Disable the CCD hardware.
        WARNING: The CCD contains a heater that takes about 15 minutes
        to fully heat up from a cold start.
        """

        # Turn off the CCD
        self.CCD_off()
        self.wait_attribute("CCDTR_translator_busy_R", False, self.CCD_On_Off_timeout)
        logger.debug("Put CCD in off state")

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""
        return 1.0 * self.read_attribute("CCD_PWR_on_R")

    def _power_hardware_on(self):
        """Forward the clock signal."""

        # CCD is powered on by default when it
        # starts up.
        pass

    def _power_hardware_off(self):
        """Stop forwarding the clock signal."""

        # CCD does not have to be powered off
        # during shutdown sequence.
        pass

    # --------
    # Commands
    # --------

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def CCD_off(self):
        """

        :return:None
        """
        self.opcua_connection.call_method(["CCD_off"])

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def CCD_on(self):
        """

        :return:None
        """
        self.opcua_connection.call_method(["CCD_on"])
