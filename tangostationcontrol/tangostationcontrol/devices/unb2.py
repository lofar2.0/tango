#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""UNB2 Device Server for LOFAR2.0"""

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType, DebugIt

# PyTango imports
from tango.server import command, attribute, device_property
from tangostationcontrol.common.constants import (
    N_unb,
    N_fpga,
    N_ddr,
    N_qsfp,
)
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.common.device_decorators import only_in_states
from tangostationcontrol.metrics import device_metrics

# Additional import

__all__ = ["UNB2"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "UNB2_*_error_R",  # too expensive as they read a lot from hardware
        "*_RW",
    ],
    include=[
        "UNB2_mask_RW",
    ],
)
class UNB2(OPCUADevice):
    """UNB2 Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    UNB2_mask_RW_default = device_property(
        dtype="DevVarBooleanArray", mandatory=False, default_value=[True] * N_unb
    )

    UNB2TR_monitor_rate_RW_default = device_property(
        dtype="DevLong64", mandatory=False, default_value=1
    )

    # ----- Timing values

    UNB2_On_Off_timeout = device_property(
        doc="Maximum amount of time to wait after turning Uniboard(s) on or off",
        dtype="DevFloat",
        mandatory=False,
        default_value=10.0,
    )

    # ----------
    # Attributes
    # ----------

    TRANSLATOR_DEFAULT_SETTINGS = ["UNB2_mask_RW", "UNB2TR_monitor_rate_RW"]

    TR_software_version_R = AttributeWrapper(
        comms_annotation=["TR_software_version_R"], datatype=str
    )
    UNB2TR_I2C_bus_DDR4_error_R = AttributeWrapper(
        comms_annotation=["UNB2TR_I2C_bus_DDR4_error_R"],
        datatype=numpy.int64,
        dims=(N_unb, N_fpga),
    )
    UNB2TR_I2C_bus_error_R = AttributeWrapper(
        comms_annotation=["UNB2TR_I2C_bus_error_R"], datatype=numpy.int64, dims=(N_unb,)
    )
    UNB2TR_I2C_bus_FPGA_PS_error_R = AttributeWrapper(
        comms_annotation=["UNB2TR_I2C_bus_FPGA_PS_error_R"],
        datatype=numpy.int64,
        dims=(N_unb, N_fpga),
    )
    UNB2TR_I2C_bus_PS_error_R = AttributeWrapper(
        comms_annotation=["UNB2TR_I2C_bus_PS_error_R"],
        datatype=numpy.int64,
        dims=(N_unb,),
    )
    UNB2TR_I2C_bus_QSFP_error_R = AttributeWrapper(
        comms_annotation=["UNB2TR_I2C_bus_QSFP_error_R"],
        datatype=numpy.int64,
        dims=(N_unb, N_qsfp),
    )
    UNB2TR_monitor_rate_RW = AttributeWrapper(
        comms_annotation=["UNB2TR_monitor_rate_RW"],
        datatype=numpy.int64,
        access=AttrWriteType.READ_WRITE,
    )
    UNB2TR_translator_busy_R = AttributeWrapper(
        comms_annotation=["UNB2TR_translator_busy_R"], datatype=bool
    )
    UNB2_DC_DC_48V_12V_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_DC_DC_48V_12V_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_DC_DC_48V_12V_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_DC_DC_48V_12V_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_DC_DC_48V_12V_VIN_R = AttributeWrapper(
        comms_annotation=["UNB2_DC_DC_48V_12V_VIN_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_DC_DC_48V_12V_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_DC_DC_48V_12V_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_FPGA_DDR4_SLOT_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_DDR4_SLOT_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga * N_ddr),
    )
    UNB2_FPGA_POL_CORE_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_CORE_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_CORE_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_CORE_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_CORE_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_CORE_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_ERAM_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_ERAM_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_ERAM_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_ERAM_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_ERAM_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_ERAM_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_HGXB_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_HGXB_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_HGXB_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_HGXB_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_HGXB_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_HGXB_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_PGM_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_PGM_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_PGM_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_PGM_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_PGM_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_PGM_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_RXGXB_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_RXGXB_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_RXGXB_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_RXGXB_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_RXGXB_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_RXGXB_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_TXGXB_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_TXGXB_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_TXGXB_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_TXGXB_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_POL_TXGXB_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_POL_TXGXB_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_fpga),
    )
    UNB2_FPGA_QSFP_CAGE_LOS_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_QSFP_CAGE_LOS_R"],
        datatype=numpy.int64,
        dims=(N_unb, N_qsfp),
    )
    UNB2_FPGA_QSFP_CAGE_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_FPGA_QSFP_CAGE_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb, N_qsfp),
    )
    UNB2_Front_Panel_LED_colour_R = AttributeWrapper(
        comms_annotation=["UNB2_Front_Panel_LED_colour_R"],
        datatype=numpy.int64,
        dims=(N_unb,),
    )
    UNB2_Front_Panel_LED_colour_RW = AttributeWrapper(
        comms_annotation=["UNB2_Front_Panel_LED_colour_RW"],
        datatype=numpy.int64,
        dims=(N_unb,),
        access=AttrWriteType.READ_WRITE,
    )
    UNB2_I2C_enabled_R = AttributeWrapper(
        comms_annotation=["UNB2_I2C_enabled_R"], datatype=bool, dims=(N_unb,)
    )
    UNB2_I2C_reset_R = AttributeWrapper(
        comms_annotation=["UNB2_I2C_reset_R"], datatype=bool, dims=(N_unb,)
    )
    UNB2_I2C_reset_RW = AttributeWrapper(
        comms_annotation=["UNB2_I2C_reset_RW"],
        datatype=bool,
        dims=(N_unb,),
        access=AttrWriteType.READ_WRITE,
    )
    UNB2_mask_RW = AttributeWrapper(
        comms_annotation=["UNB2_mask_RW"],
        datatype=bool,
        dims=(N_unb,),
        access=AttrWriteType.READ_WRITE,
    )
    UNB2_PCB_ID_R = AttributeWrapper(
        comms_annotation=["UNB2_PCB_ID_R"], datatype=numpy.int64, dims=(N_unb,)
    )
    UNB2_PCB_number_R = AttributeWrapper(
        comms_annotation=["UNB2_PCB_number_R"], datatype=str, dims=(N_unb,)
    )
    UNB2_PCB_version_R = AttributeWrapper(
        comms_annotation=["UNB2_PCB_version_R"], datatype=str, dims=(N_unb,)
    )
    UNB2_POL_CLOCK_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_CLOCK_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_CLOCK_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_CLOCK_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_CLOCK_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_CLOCK_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_QSFP_N01_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_QSFP_N01_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_QSFP_N01_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_QSFP_N01_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_QSFP_N01_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_QSFP_N01_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_QSFP_N23_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_QSFP_N23_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_QSFP_N23_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_QSFP_N23_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_QSFP_N23_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_QSFP_N23_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_SWITCH_1V2_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_SWITCH_1V2_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_SWITCH_1V2_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_SWITCH_1V2_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_SWITCH_1V2_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_SWITCH_1V2_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_SWITCH_PHY_IOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_SWITCH_PHY_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_SWITCH_PHY_TEMP_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_SWITCH_PHY_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_POL_SWITCH_PHY_VOUT_R = AttributeWrapper(
        comms_annotation=["UNB2_POL_SWITCH_PHY_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_unb,),
    )
    UNB2_PWR_on_R = AttributeWrapper(
        comms_annotation=["UNB2_PWR_on_R"], datatype=bool, dims=(N_unb,)
    )

    # ----------
    # Summarising Attributes
    # ----------
    UNB2_error_R = attribute(
        dtype=(bool,), max_dim_x=N_unb, fisallowed="is_attribute_access_allowed"
    )

    def read_UNB2_error_R(self):
        return self.read_attribute("UNB2_mask_RW") & (
            (self.read_attribute("UNB2TR_I2C_bus_error_R") > 0)
            | self.alarm_val("UNB2_PCB_ID_R")
            | (self.read_attribute("UNB2TR_I2C_bus_DDR4_error_R") > 0).any(axis=1)
            | (self.read_attribute("UNB2TR_I2C_bus_FPGA_PS_error_R") > 0).any(axis=1)
            | (self.read_attribute("UNB2TR_I2C_bus_QSFP_error_R") > 0).any(axis=1)
        )

    UNB2_IOUT_error_R = attribute(
        dtype=(bool,), max_dim_x=N_unb, fisallowed="is_attribute_access_allowed"
    )
    UNB2_TEMP_error_R = attribute(
        dtype=(bool,),
        max_dim_x=N_unb,
        fisallowed="is_attribute_access_allowed",
    )
    UNB2_VOUT_error_R = attribute(
        dtype=(bool,), max_dim_x=N_unb, fisallowed="is_attribute_access_allowed"
    )

    def read_UNB2_IOUT_error_R(self):
        return self.read_attribute("UNB2_mask_RW") & (
            self.alarm_val("UNB2_DC_DC_48V_12V_IOUT_R")
            | self.alarm_val("UNB2_FPGA_POL_CORE_IOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_ERAM_IOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_HGXB_IOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_PGM_IOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_RXGXB_IOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_TXGXB_IOUT_R").any(axis=1)
            | self.alarm_val("UNB2_POL_CLOCK_IOUT_R")
            | self.alarm_val("UNB2_POL_QSFP_N01_IOUT_R")
            | self.alarm_val("UNB2_POL_QSFP_N23_IOUT_R")
            | self.alarm_val("UNB2_POL_SWITCH_1V2_IOUT_R")
            | self.alarm_val("UNB2_POL_SWITCH_PHY_IOUT_R")
        )

    def read_UNB2_TEMP_error_R(self):
        # Don't apply the mask here --- we always want to know if things get too hot!
        return (
            self.alarm_val("UNB2_DC_DC_48V_12V_TEMP_R")
            | self.alarm_val("UNB2_FPGA_POL_CORE_TEMP_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_ERAM_TEMP_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_HGXB_TEMP_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_PGM_TEMP_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_RXGXB_TEMP_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_TXGXB_TEMP_R").any(axis=1)
            | self.alarm_val("UNB2_POL_CLOCK_TEMP_R")
            | self.alarm_val("UNB2_POL_QSFP_N01_TEMP_R")
            | self.alarm_val("UNB2_POL_QSFP_N23_TEMP_R")
            | self.alarm_val("UNB2_POL_SWITCH_1V2_TEMP_R")
            | self.alarm_val("UNB2_POL_SWITCH_PHY_TEMP_R")
        )

    def read_UNB2_VOUT_error_R(self):
        return self.read_attribute("UNB2_mask_RW") & (
            self.alarm_val("UNB2_DC_DC_48V_12V_VOUT_R")
            | self.alarm_val("UNB2_FPGA_POL_CORE_VOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_ERAM_VOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_HGXB_VOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_PGM_VOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_RXGXB_VOUT_R").any(axis=1)
            | self.alarm_val("UNB2_FPGA_POL_TXGXB_VOUT_R").any(axis=1)
            | self.alarm_val("UNB2_POL_CLOCK_VOUT_R")
            | self.alarm_val("UNB2_POL_QSFP_N01_VOUT_R")
            | self.alarm_val("UNB2_POL_QSFP_N23_VOUT_R")
            | self.alarm_val("UNB2_POL_SWITCH_1V2_VOUT_R")
            | self.alarm_val("UNB2_POL_SWITCH_PHY_VOUT_R")
        )

    # --------
    # overloaded functions
    # --------

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""

        mask = self.read_attribute("UNB2_mask_RW")
        powered = self.read_attribute("UNB2_PWR_on_R")

        try:
            return numpy.count_nonzero(powered & mask) / numpy.count_nonzero(mask)
        except ZeroDivisionError:
            return 1.0

    def _power_hardware_on(self):
        """Power the Uniboards."""

        self.UNB2_on()
        self.wait_attribute("UNB2TR_translator_busy_R", False, self.UNB2_On_Off_timeout)

        self.wait_attribute(
            "hardware_powered_fraction_R", 1.0, self.UNB2_On_Off_timeout
        )

    def _power_hardware_off(self):
        """Disable the Uniboards."""

        # Save actual mask values
        UNB2_mask = self.proxy.UNB2_mask_RW
        # Set the mask to all Trues
        self.UNB2_mask_RW = [True] * N_unb
        # Turn off the uniboards
        self.UNB2_off()
        self.wait_attribute("UNB2TR_translator_busy_R", False, self.UNB2_On_Off_timeout)
        # Restore the mask
        self.UNB2_mask_RW = UNB2_mask

    # --------
    # Commands
    # --------

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def UNB2_off(self):
        """

        :return:None
        """
        self.opcua_connection.call_method(["UNB2_off"])

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def UNB2_on(self):
        """

        :return:None
        """
        self.opcua_connection.call_method(["UNB2_on"])
