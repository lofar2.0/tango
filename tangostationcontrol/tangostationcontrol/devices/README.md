# Devices

This directory contains the sources for our custom Tango devices.

## Adding a new device

If a new device is added, it will (likely) need to be referenced in several places. Adjust or add the following files (referenced from the repository root), following the pattern shown by the devices already there:

- Adjust `CDB/stations/common.json` to create the device in the Tango device database,
- Add the device hierarchies to define the power and control configuration in `common.json`
- Adjust `docker/jupyter-lab/ipython-profiles/stationcontrol-jupyter/startup/01-devices.py` to make an alias for it available in Jupyter-Lab,
- Add to `infra/env.yaml` the device class.
- Adjust `tangostationcontrol/devices/__init__.py` to include the new device class and add to `__all__`
- Add the device class to `/sbin/run_integration_test.sh` list of devices
- Create `tangostationcontrol/tangostationcontrol/integration_test/default/devices/test_device_xxx.py` to add an integration test,
- Add to `tangostationcontrol/docs/source/devices/` to mention the device in the end-user documentation.
- Adjust `tangostationcontrol/docs/source/index.rst` to include the newly created file in `docs/source/devices/`.
