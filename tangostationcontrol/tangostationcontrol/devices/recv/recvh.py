#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Receiver Unit High Device Server for LOFAR2.0"""
import logging

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType, DevVarFloatArray

# PyTango imports
from tango.server import command, device_property, attribute

# Additional import
from tangostationcontrol.asyncio import PeriodicTask
from tangostationcontrol.common.constants import (
    N_rcu,
    N_elements,
    N_pol,
    N_rcu_inp,
)
from tangostationcontrol.common.device_decorators import DurationMetric
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
    exception_to_str,
)
from tangostationcontrol.devices.base_device_classes.recv_device import RECVDevice
from tangostationcontrol.metrics import device_metrics

__all__ = ["RECVH"]

logger = logging.getLogger()


@device_logging_to_python()
@device_metrics(
    exclude=[
        "HBAT_BF_delay_steps_R",
        "HBAT_PWR_on_R",
        "HBAT_PWR_LNA_on_R",
        "*_RW",
    ]
)
class RECVH(RECVDevice):
    """
    RECVH implements the logic of LOFAR2 Receiver Unit
    for High Band Antennas
    """

    # -----------------
    # Device Properties
    # -----------------

    HBAT_BF_delay_steps_update_interval = device_property(
        dtype="DevFloat",
        doc="Interval with which to update HBAT_BF_delay_steps_RW [seconds]",
        mandatory=False,
        default_value=10.0,
    )

    # ----- Default settings

    HBAT_PWR_on_RW_default = device_property(
        dtype="DevVarBooleanArray",
        mandatory=False,
        default_value=[True] * N_rcu * N_rcu_inp * N_elements * N_pol,
    )

    HBAT_PWR_LNA_on_RW_default = device_property(
        dtype="DevVarBooleanArray",
        mandatory=False,
        default_value=[True] * N_rcu * N_rcu_inp * N_elements * N_pol,
    )

    HBAT_LED_on_RW_default = device_property(
        dtype="DevVarBooleanArray",
        mandatory=False,
        default_value=[False] * N_rcu * N_rcu_inp * N_elements * N_pol,
    )

    # ----- Calibration values

    HBAT_bf_delay_step_delays = device_property(
        dtype="DevVarFloatArray",
        mandatory=False,
        default_value=numpy.array(
            [
                0.0,
                0.5228e-9,
                0.9797e-9,
                1.4277e-9,
                1.9055e-9,
                2.4616e-9,
                2.9539e-9,
                3.4016e-9,
                3.8076e-9,
                4.3461e-9,
                4.9876e-9,
                5.4894e-9,
                5.7973e-9,
                6.2707e-9,
                6.8628e-9,
                7.3989e-9,
                8.0673e-9,
                8.6188e-9,
                9.1039e-9,
                9.5686e-9,
                10.0463e-9,
                10.5774e-9,
                11.0509e-9,
                11.5289e-9,
                11.9374e-9,
                12.4524e-9,
                13.0842e-9,
                13.5936e-9,
                13.9198e-9,
                14.4087e-9,
                14.9781e-9,
                15.5063e-9,
            ],
            dtype=numpy.float64,
        ),
    )

    HBAT_signal_input_delays = device_property(
        doc="Signal input delay calibration values for the elements within a tile.",
        dtype="DevVarFloatArray",
        mandatory=False,
        default_value=numpy.zeros((N_rcu,), dtype=numpy.float64),
    )

    # ----------
    # Attributes
    # ----------

    bf_delay_steps_thread_running_R = attribute(
        doc="Whether the HBAT_BF_delay_steps_RW is being updated.",
        dtype=bool,
        fget=lambda self: (
            self.event_loop_thread
            and self.event_loop_thread.is_running()
            and self.bf_delay_steps_update_task
            and self.bf_delay_steps_update_task.is_running()
        ),
    )

    RCU_firmware_version_R = AttributeWrapper(
        comms_annotation=["RCU_firmware_version_R"],
        datatype=numpy.int64,
        dims=(N_rcu,),
    )

    RCU_DAB_filter_on_R = AttributeWrapper(
        comms_annotation=["RCU_DAB_filter_on_R"],
        datatype=bool,
        dims=(N_rcu, N_rcu_inp),
    )
    RCU_DAB_filter_on_RW = AttributeWrapper(
        comms_annotation=["RCU_DAB_filter_on_RW"],
        datatype=bool,
        access=AttrWriteType.READ_WRITE,
        dims=(N_rcu, N_rcu_inp),
    )
    RCU_ADC_sync_R = AttributeWrapper(
        doc="Whether the ADC is in sync after RCU_on",
        comms_annotation=["RCU_ADC_sync_R"],
        datatype=bool,
        dims=(N_rcu, N_rcu_inp),
    )

    # The HBAT beamformer delays represent 32 delays for each of the 96 inputs.
    # The 32 delays deconstruct as delays[polarisation][dipole],
    # and each delay is the number of 'delay steps' to apply (0.5ns for HBAT1).
    HBAT_BF_delay_steps_R = AttributeWrapper(
        comms_annotation=["HBAT_BF_delay_steps_R"],
        datatype=numpy.int64,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
    )
    HBAT_BF_delay_steps_RW = AttributeWrapper(
        doc="HBAT beamformer delay steps. The hardware supports writing this value only once every 10s. This device will write the value of HBAT_BF_delay_steps_stage_RW every HBAT_BF_delay_steps_update_interval seconds.",
        comms_annotation=["HBAT_BF_delay_steps_RW"],
        datatype=numpy.int64,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
        access=AttrWriteType.READ_WRITE,
    )

    # Multiple TileBeams want to read-modify-write HBAT_BF_delay_steps to
    # interleave the delays for their respective antenna fields. Yet we are only
    # allowed to write HBAT_BF_delay_steps_RW every 10s. This "stage" attribute
    # allows collecting changes, which are periodically applied through
    # the PeriodicTask "_update_bf_delay_steps".
    @attribute(
        doc="HBAT beamformer delay steps staged for writing at the next interval.",
        dtype=((numpy.int64,),),
        max_dim_y=N_rcu * N_rcu_inp,
        max_dim_x=N_elements * N_pol,
        access=AttrWriteType.READ_WRITE,
    )
    def HBAT_BF_delay_steps_stage_RW(self):
        return self._HBAT_BF_delay_steps_stage_RW

    @HBAT_BF_delay_steps_stage_RW.write
    def HBAT_BF_delay_steps_stage_RW(self, value):
        self._HBAT_BF_delay_steps_stage_RW = value

    HBAT_LED_on_R = AttributeWrapper(
        comms_annotation=["HBAT_LED_on_R"],
        datatype=bool,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
    )
    HBAT_LED_on_RW = AttributeWrapper(
        comms_annotation=["HBAT_LED_on_RW"],
        datatype=bool,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
        access=AttrWriteType.READ_WRITE,
    )
    HBAT_PWR_LNA_on_R = AttributeWrapper(
        comms_annotation=["HBAT_PWR_LNA_on_R"],
        datatype=bool,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
    )
    HBAT_PWR_LNA_on_RW = AttributeWrapper(
        comms_annotation=["HBAT_PWR_LNA_on_RW"],
        datatype=bool,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
        access=AttrWriteType.READ_WRITE,
    )
    HBAT_PWR_on_R = AttributeWrapper(
        comms_annotation=["HBAT_PWR_on_R"],
        datatype=bool,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
    )
    HBAT_PWR_on_RW = AttributeWrapper(
        comms_annotation=["HBAT_PWR_on_RW"],
        datatype=bool,
        dims=(N_rcu * N_rcu_inp, N_elements, N_pol),
        access=AttrWriteType.READ_WRITE,
    )

    @DurationMetric()
    async def _update_bf_delay_steps(self):
        try:
            staged_value = await self.async_read_attribute(
                "HBAT_BF_delay_steps_stage_RW"
            )
            last_written_value = await self.async_read_attribute(
                "HBAT_BF_delay_steps_RW"
            )

            if (staged_value != last_written_value).any():
                logger.debug(
                    "Writing HBAT_BF_delay_steps_RW := HBAT_BF_delay_steps_stage_RW after detecting changes"
                )
                self.proxy.write_attribute("HBAT_BF_delay_steps_RW", staged_value)
            else:
                logger.debug(
                    "Not writing HBAT_BF_delay_steps_RW := HBAT_BF_delay_steps_stage_RW as it remains unchanged"
                )
        except Exception as ex:
            logger.error(
                f"Failed to update HBAT_BF_delay_steps_RW: {exception_to_str(ex)}"
            )

    # --------
    # overloaded functions
    # --------
    def __init__(self, cl, name):
        self.bf_delay_steps_update_task = None
        self._HBAT_BF_delay_steps_stage_RW = numpy.zeros(
            (N_rcu * N_rcu_inp, N_elements * N_pol), dtype=numpy.int64
        )

        super().__init__(cl, name)

    def configure_for_on(self):
        super().configure_for_on()

        self.bf_delay_steps_update_task = PeriodicTask(
            self.event_loop_thread.event_loop,
            self._update_bf_delay_steps,
            self.HBAT_BF_delay_steps_update_interval,
        )

    def configure_for_off(self):
        # stop writing delays (gracefully, as it is fast)
        if self.bf_delay_steps_update_task:
            self.bf_delay_steps_update_task.stop()

        super().configure_for_off()

    def properties_changed(self):
        super().properties_changed()

        # The HBAT can only apply positive delays, yet we want to apply a delay
        # relative to the center of the tile, which typically requires negative
        # delays for half of the elements.
        #
        # We circumvent this issue by increasing the delays for all elements
        # by a fixed amount, the average of all steps. Doing so should result
        # in positive delays regardless of the pointing direction.
        self.HBAT_bf_delay_offset = numpy.mean(self.HBAT_bf_delay_step_delays)

    # --------
    # internal functions
    # --------
    def _calculate_HBAT_bf_delay_steps(self, delays: numpy.ndarray):
        """
        Helper function that converts a signal path delay (in seconds) to an analog beam weight,
        which is a value per tile per dipole per polarisation.
        """
        # Duplicate delay values per polarisation
        polarised_delays = numpy.repeat(delays, N_pol, axis=1)  # output dims -> 96x32

        # Add signal input delay
        calibrated_delays = numpy.add(polarised_delays, self.HBAT_signal_input_delays)

        # Find the right delay step by looking for the closest match
        # in property RECV-> HBAT_bf_delay_step_delays
        def nearest_delay_step(delay):
            # We want the index in the HBAT_bf_delay_steps_delay array
            # which is closest to the given delay,
            # shifted by HBAT_bf_delay_offset to obtain strictly positive delays.
            return (
                numpy.abs(
                    self.HBAT_bf_delay_step_delays - (delay + self.HBAT_bf_delay_offset)
                )
            ).argmin()

        # Apply to all elements to convert each delay into the number of delay steps
        return numpy.vectorize(nearest_delay_step)(calibrated_delays)

    # --------
    # Commands
    # --------
    @command(dtype_in=DevVarFloatArray, dtype_out=DevVarFloatArray)
    def calculate_HBAT_bf_delay_steps(self, delays: numpy.ndarray):
        """converts a signal path delay (in seconds) to an analog beam weight"""

        # Reshape the flattened input array, into whatever how many tiles we get
        delays = numpy.array(delays).reshape(-1, N_elements)

        # Calculate the beam weight array
        HBAT_bf_delay_steps = self._calculate_HBAT_bf_delay_steps(delays)

        return HBAT_bf_delay_steps.flatten()
