#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Receiver Unit Low Device Server for LOFAR2.0"""

from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.devices.base_device_classes.recv_device import RECVDevice
from tangostationcontrol.metrics import device_metrics

__all__ = ["RECVL"]


@device_logging_to_python()
@device_metrics()
class RECVL(RECVDevice):
    """
    RECVL implements the logic of LOFAR2 Receiver Unit
    for Low Band Antennas
    """

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------
