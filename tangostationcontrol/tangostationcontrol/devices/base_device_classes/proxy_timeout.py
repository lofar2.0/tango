# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""DeviceProxy timeouts for PyTango devices"""
from tango.server import attribute, Device, class_property

__all__ = ["device_proxy_timeout"]


class _ProxyTimeoutDevice(Device):
    """Device Proxy Timeout"""

    # -----------------
    # Device Properties
    # -----------------
    Device_Proxy_Timeout = class_property(
        dtype="DevULong", default_value=10000, doc="DeviceProxy timeout (ms)"
    )

    @attribute(dtype="DevULong", doc="DeviceProxy timeout (ms)")
    def device_proxy_timeout_R(self):
        return self.Device_Proxy_Timeout


def device_proxy_timeout(cls):
    if not issubclass(cls, Device):
        raise TypeError("{cls.__name__} needs to be a subclass of tango.server.Device")

    return type(
        cls.__name__,
        (cls,) + cls.__bases__,
        dict(_ProxyTimeoutDevice.__dict__),
    )
