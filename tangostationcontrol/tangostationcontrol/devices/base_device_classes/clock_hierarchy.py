# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Clock Hierarchy for PyTango devices"""

from typing import Dict
from typing import Optional

from tango import DeviceProxy

from tangostationcontrol.devices.base_device_classes.hierarchy_device import (
    AbstractHierarchyDevice,
)


class ClockHierarchyDevice(AbstractHierarchyDevice):
    CLOCK_CHILD_PROPERTY = "clock_children"
    CLOCK_PARENT_PROPERTY = "clock_parent"

    def init(
        self,
        device_name: str,
        proxies: Optional[Dict[str, DeviceProxy]] = None,
    ):
        super().init(
            device_name, self.CLOCK_CHILD_PROPERTY, self.CLOCK_PARENT_PROPERTY, proxies
        )
