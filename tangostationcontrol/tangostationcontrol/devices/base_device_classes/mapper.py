#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Mapper for TANGO devices that share a common attribute"""
from abc import ABC, abstractmethod
from enum import Enum
from typing import List, Optional, Dict
from collections.abc import Callable
import numpy

# PyTango imports
from tango import AttrWriteType, DeviceProxy
from tango.server import attribute

from tangostationcontrol.common.constants import (
    N_elements,
    MAX_ANTENNA,
    N_pol,
    N_rcu,
    N_rcu_inp,
    N_pn,
)
from tangostationcontrol.common.type_checking import type_not_sequence
from tangostationcontrol.devices.recv.recvh import RECVH
from tangostationcontrol.devices.sdp.sdp import SDP

__all__ = [
    "AntennaMapper",
    "AntennaToRecvMapper",
    "AntennaToSdpMapper",
    "AFHToRecvMapper",
    "AFLToRecvMapper",
]


class MappingKeys(str, Enum):
    """Types of Mapping"""

    POWER = "POWER"
    CONTROL = "CONTROL"
    FPGA = "FPGA"


class MappedAttribute(attribute):
    """An extension of Tango attribute"""

    def __init__(
        self,
        mapping_attribute,
        dtype,
        max_dim_x,
        max_dim_y=0,
        access=AttrWriteType.READ,
        mapping_device="RECV",
        **kwargs,
    ):
        if access == AttrWriteType.READ_WRITE:

            def write_func_wrapper(device, value):
                cast_type = dtype
                while not type_not_sequence(cast_type):
                    cast_type = cast_type[0]
                _write_func = device.set_mapped_attribute(
                    mapping_attribute, value, cast_type, mapping_device
                )

            self.fset = write_func_wrapper

        def read_func_wrapper(device):
            return device.get_mapped_attribute(mapping_attribute, mapping_device)

        self.fget = read_func_wrapper
        self.mapping_device = mapping_device

        super().__init__(
            dtype=dtype,
            max_dim_y=max_dim_y,
            max_dim_x=max_dim_x,
            access=access,
            fisallowed="is_attribute_access_allowed",
            **kwargs,
        )


class AntennaMapper:
    """AntennaMapper superclass"""

    def __init__(
        self,
        mapping_dict: Dict[str, numpy.ndarray],
        num_devices: Optional[int] = 1,
    ) -> None:
        self._mapping_dict = mapping_dict
        self._num_devices = num_devices
        self._value_mapper = {}
        self._default_value_mapping_read = {}
        self._masked_value_mapping_write = {}
        self._reshape_attributes_in = {}
        self._reshape_attributes_out = {}
        self._fill_attributes_in = {}
        self._empty_attributes_out = {}
        self._pwr_mapping = []

    def map_read(
        self, mapped_attribute: str, device_values: List[any]
    ) -> numpy.ndarray:
        """Perform a mapped read for the attribute using the device_results

        :param mapped_attribute: attribute identifier as present in
                                 py:attribute:`~_default_value_mapping_read`
        :param device_values: Results as gathered by appending attributes from device
        :return: device_results as mapped given attribute dimensions and control mapping
        """
        default_values = self._default_value_mapping_read[mapped_attribute]
        device_values = self._reshape_r_values(mapped_attribute, device_values)
        mapped_values = numpy.array(default_values)
        value_mapping = self._value_mapper[mapped_attribute]
        # If mapping is based on only one map, then insert the value
        # provided by the map
        mapping = value_mapping[0]
        for idx, (dev_conn, dev_input) in enumerate(mapping):
            # Differentiate operations between RECVs and FPGAs
            if dev_conn > 0 and MappingKeys.CONTROL in list(self._mapping_dict.keys()):
                mapped_values[idx] = device_values[dev_conn - 1][dev_input]
            elif dev_input > -1 and MappingKeys.FPGA in list(self._mapping_dict.keys()):
                mapped_values[idx] = device_values[dev_conn]

        return mapped_values

    def _reshape_r_values(self, mapped_attribute: str, device_values: List[any]):
        """Attributes which need to be reshaped with a copy of their values,
        because Device original dimension < AntennaField mapped dimension"""
        if mapped_attribute in self._fill_attributes_in:
            device_values = [
                [x] * self._fill_attributes_in[mapped_attribute] for x in device_values
            ]

        if mapped_attribute in self._reshape_attributes_in:
            device_values = numpy.reshape(
                device_values,
                (self._num_devices,) + self._reshape_attributes_in[mapped_attribute],
            )
        return device_values

    def map_write(self, mapped_attribute: str, set_values: List[any]) -> numpy.ndarray:
        """Perform a mapped write for the attribute using the set_values

        :param mapped_attribute: attribute identifier as present in
                                 py:attribute:`~_default_value_mapping_write`
        :param set_values: The values to be set for the specified attribute
        :return: set_values as mapped given attribute dimensions and control mapping
        """
        default_values = self._masked_value_mapping_write[mapped_attribute]
        value_mapping = self._value_mapper[mapped_attribute]

        if MappingKeys.CONTROL in list(self._mapping_dict.keys()):
            mapped_values = []
            for _ in range(self._num_devices):
                mapped_values.append(default_values)
            mapped_values = numpy.array(mapped_values)
        elif MappingKeys.FPGA in list(self._mapping_dict.keys()):
            mapped_values = numpy.array(default_values)

        if len(value_mapping) == 1:
            mapping = value_mapping[0]
            for idx, (dev_conn, dev_input) in enumerate(mapping):
                # Differentiate operations between RECVs and FPGAs
                if dev_conn > 0 and MappingKeys.CONTROL in list(
                    self._mapping_dict.keys()
                ):
                    mapped_values[dev_conn - 1, dev_input] = set_values[idx]
                elif dev_input > -1 and MappingKeys.FPGA in list(
                    self._mapping_dict.keys()
                ):
                    mapped_values[dev_conn] = set_values[idx]

        return self._reshape_rw_values(mapped_attribute, mapped_values)

    def _reshape_rw_values(self, mapped_attribute: str, mapped_values: numpy.ndarray):
        """Attributes which need to be reshaped with removing a part of their duplicated
        values because Device original dimension < Antennafield mapped dimension"""
        if mapped_attribute in self._empty_attributes_out:
            mapped_values = numpy.reshape(
                mapped_values,
                (self._num_devices,) + self._empty_attributes_out[mapped_attribute],
            )
            # Remove second (last) dimension
            mapped_values = mapped_values[:, :, 0]

        # Only RECV mapping may have more than one devices by now
        if mapped_attribute in self._reshape_attributes_out:
            mapped_values = numpy.reshape(
                mapped_values,
                (self._num_devices,) + self._reshape_attributes_out[mapped_attribute],
            )

        return mapped_values

    def _init_reshape_out(self, mapped_attributes: List[str], device_type: str):
        """Initialise the _reshape_attributes_out dictionary, setting the
        original dimension for each mapped attribute"""
        device_class_map = {"RECV": RECVH, "SDP": SDP}
        device_class = device_class_map[device_type]
        reshape_out = {}
        for attr in mapped_attributes:
            # Retrieve the original attribute dimension from class attributes
            attr_instance = getattr(device_class, attr)
            dim_x, dim_y = attr_instance.dim_x, attr_instance.dim_y
            # Insert the dimension(s) in the map
            reshape_out[attr] = (dim_x,) if dim_y == 0 else (dim_y, dim_x)
        return reshape_out


class AntennaToSdpMapper(AntennaMapper):
    """Class that sets a mapping between SDP and AntennaField attributes"""

    _VALUE_MAP_NONE_16 = numpy.full(N_pn, None)

    def __init__(
        self,
        mapping_dict: Dict[str, numpy.ndarray],
        mapped_dimensions: Dict[str, tuple],
    ) -> None:
        super().__init__(mapping_dict)

        self._fpga_mapping = self._mapping_dict[MappingKeys.FPGA]
        number_of_antennas = len(self._fpga_mapping)
        value_map_fpga_info_int = numpy.zeros(number_of_antennas, dtype=numpy.uint32)

        self._mapped_attributes = list(mapped_dimensions)

        self._value_mapper = {}
        for attr in self._mapped_attributes:
            self._value_mapper[attr] = [self._fpga_mapping]

        self._default_value_mapping_read = {}
        for attr in self._mapped_attributes:
            self._default_value_mapping_read[attr] = value_map_fpga_info_int

        self._masked_value_mapping_write = {}
        for attr in self._mapped_attributes:
            # Only read/write attributes
            if attr.endswith("RW"):
                self._masked_value_mapping_write[attr] = (
                    AntennaToSdpMapper._VALUE_MAP_NONE_16
                )


class AntennaToRecvMapper(ABC, AntennaMapper):
    """Class that sets a mapping between RECVs and AntennaField attributes"""

    _VALUE_MAP_NONE_96 = numpy.full(MAX_ANTENNA, None)
    _VALUE_MAP_NONE_96_32 = numpy.full((MAX_ANTENNA, N_elements * N_pol), None)
    _VALUE_MAP_NONE_96_2 = numpy.full((MAX_ANTENNA, 2), None)

    def __init__(
        self,
        mapping_dict: Dict[str, numpy.ndarray],
        mapped_dimensions: Dict[str, tuple],
        number_of_receivers: Optional[int] = 1,
    ):
        super().__init__(mapping_dict, number_of_receivers)

        self._power_mapping = self._mapping_dict[MappingKeys.POWER]
        self._control_mapping = self._mapping_dict[MappingKeys.CONTROL]
        number_of_antennas = len(self._control_mapping)

        # Reduce memory footprint of default values by creating single instance of
        # common fields
        value_map_ant_32_int = numpy.zeros(
            [number_of_antennas, N_elements * N_pol], dtype=numpy.int64
        )
        value_map_ant_32_bool = numpy.full(
            (number_of_antennas, N_elements * N_pol), False
        )
        value_map_ant_bool = numpy.full(number_of_antennas, False)

        self._mapped_attributes = list(mapped_dimensions)

        self._default_value_mapping_read = {
            "ANT_mask_RW": value_map_ant_bool,
            "HBAT_BF_delay_steps_R": value_map_ant_32_int,
            "HBAT_BF_delay_steps_RW": value_map_ant_32_int,
            "HBAT_BF_delay_steps_stage_RW": value_map_ant_32_int,
            "HBAT_LED_on_R": value_map_ant_32_bool,
            "HBAT_LED_on_RW": value_map_ant_32_bool,
            "HBAT_PWR_LNA_on_R": value_map_ant_32_bool,
            "HBAT_PWR_LNA_on_RW": value_map_ant_32_bool,
            "HBAT_PWR_on_R": value_map_ant_32_bool,
            "HBAT_PWR_on_RW": value_map_ant_32_bool,
            "RCU_PWR_ANT_on_R": value_map_ant_bool,
            "RCU_PWR_ANT_on_RW": value_map_ant_bool,
            "RCU_band_select_R": numpy.zeros(
                (number_of_antennas, N_pol), dtype=numpy.int64
            ),
            "RCU_band_select_RW": numpy.zeros(
                (number_of_antennas, N_pol), dtype=numpy.int64
            ),
            "RCU_attenuator_dB_R": numpy.zeros(
                (number_of_antennas, N_pol), dtype=numpy.int64
            ),
            "RCU_attenuator_dB_RW": numpy.zeros(
                (number_of_antennas, N_pol), dtype=numpy.int64
            ),
            "RCU_DTH_freq_R": numpy.zeros(number_of_antennas, dtype=numpy.float64),
            "RCU_DTH_freq_RW": numpy.zeros(number_of_antennas, dtype=numpy.float64),
            "RCU_DTH_on_R": value_map_ant_bool,
            "RCU_DTH_PWR_R": numpy.zeros(number_of_antennas, dtype=numpy.float64),
            "RCU_DTH_PWR_RW": numpy.zeros(number_of_antennas, dtype=numpy.float64),
            "RCU_DAB_filter_on_R": value_map_ant_bool,
            "RCU_DAB_filter_on_RW": value_map_ant_bool,
            "RCU_PCB_ID_R": numpy.zeros((number_of_antennas, 2), dtype=numpy.int64),
            "RCU_PCB_version_R": numpy.full((number_of_antennas, 2), "", dtype=str),
        }
        self._masked_value_mapping_write = self._init_masked_value_write(
            mapped_dimensions
        )

        self._double_mapping = [
            "RCU_attenuator_dB_R",
            "RCU_attenuator_dB_RW",
            "RCU_band_select_R",
            "RCU_band_select_RW",
            "RCU_PCB_ID_R",
            "RCU_PCB_version_R",
        ]
        self._pwr_mapping = ["RCU_PWR_ANT_on_R", "RCU_PWR_ANT_on_RW"]

        # Dict of reshaped mapped attribute dimensions with special cases
        self._reshape_attributes_in = mapped_dimensions
        self._reshape_attributes_in["RCU_attenuator_dB_R"] = ((MAX_ANTENNA * N_pol),)
        self._reshape_attributes_in["RCU_attenuator_dB_RW"] = ((MAX_ANTENNA * N_pol),)
        self._reshape_attributes_in["RCU_band_select_R"] = ((MAX_ANTENNA * N_pol),)
        self._reshape_attributes_in["RCU_band_select_RW"] = ((MAX_ANTENNA * N_pol),)
        self._reshape_attributes_in["RCU_PCB_ID_R"] = (MAX_ANTENNA,)
        self._reshape_attributes_in["RCU_PCB_version_R"] = (MAX_ANTENNA,)

        # Dict of reshaped attribute dimensions following its device
        self._reshape_attributes_out = self._init_reshape_out(
            self._mapped_attributes,
            "RECV",
        )

        # Attributes which need to be reshaped with a copy of their values,
        # because RECV original dimension < AntennaField mapped dimension
        self._fill_attributes_in = {
            "RCU_band_select_R": N_pol,
            "RCU_band_select_RW": N_pol,
            "RCU_attenuator_dB_R": N_pol,
            "RCU_attenuator_dB_RW": N_pol,
            "RCU_PCB_ID_R": N_rcu_inp,
            "RCU_PCB_version_R": N_rcu_inp,
        }

        # Attributes which need to be reshaped with removing a part of their duplicated
        # values because RECV original dimension < Antennafield mapped dimension
        self._empty_attributes_out = {
            "RCU_band_select_RW": (N_rcu * N_rcu_inp, N_pol),
            "RCU_attenuator_dB_RW": (N_rcu * N_rcu_inp, N_pol),
        }

    def _init_masked_value_write(
        self, mapped_dimensions: Dict[str, tuple]
    ) -> Dict[str, numpy.ndarray]:
        """Create the masked value mapping dictionary for write attributes

        :param mapped_dimensions: a dictionary[str,tuple] which maps each
                                    attribute with its RECV dimension
        :return a dictionary[str, numpy.ndarray] which maps each attribute with
                its AntennaField masked array dimension
        """
        special_cases = ["RCU_PCB_ID_R", "RCU_PCB_version_R"]
        masked_write = {}
        for attr, dim in mapped_dimensions.items():
            if attr.casefold().endswith("rw") or attr in special_cases:
                if len(dim) == 1 and dim[0] == 96:
                    masked_write[attr] = AntennaToRecvMapper._VALUE_MAP_NONE_96
                elif len(dim) == 2 and dim[1] == 2:
                    masked_write[attr] = AntennaToRecvMapper._VALUE_MAP_NONE_96_2
                elif len(dim) == 2 and dim[1] == 32:
                    masked_write[attr] = AntennaToRecvMapper._VALUE_MAP_NONE_96_32
        return masked_write

    def map_read(
        self, mapped_attribute: str, device_values: List[any]
    ) -> numpy.ndarray:
        """Perform a mapped read for the attribute using the device_results

        :param mapped_attribute: attribute identifier as present in
                                 py:attribute:`~_default_value_mapping_read`
        :param device_values: Results as gathered by appending attributes from device
        :return: device_results as mapped given attribute dimensions and control mapping
        """
        if len(self._value_mapper[mapped_attribute]) == 1:
            return super().map_read(mapped_attribute, device_values)
        if len(self._value_mapper[mapped_attribute]) == 2:
            return self.map_read_double(mapped_attribute, device_values)
        raise ValueError(
            f"Mapping dims must be <=2, while it is {len(self._value_mapper[mapped_attribute])}"
        )

    def map_write(self, mapped_attribute: str, set_values: List[any]) -> numpy.ndarray:
        """Perform a mapped write for the attribute using the set_values

        :param mapped_attribute: attribute identifier as present in
                                 py:attribute:`~_default_value_mapping_write`
        :param set_values: The values to be set for the specified attribute
        :return: set_values as mapped given attribute dimensions and control mapping
        """
        if len(self._value_mapper[mapped_attribute]) == 1:
            return super().map_write(mapped_attribute, set_values)
        if len(self._value_mapper[mapped_attribute]) == 2:
            return self.map_write_double(mapped_attribute, set_values)
        raise ValueError(
            f"Mapping dims must be <=2, while it is {len(self._value_mapper[mapped_attribute])}"
        )

    def map_read_double(
        self, mapped_attribute: str, device_values: List[any]
    ) -> numpy.ndarray:
        """Perform a mapped read for the attribute using the device_results,
        only when the mapping is based on TWO maps (CTRL and PWR)

        :param mapped_attribute: attribute identifier as present in
                                 py:attribute:`~_default_value_mapping_read`
        :param device_values: Results as gathered by appending attributes from device
        :return: mapped_values as a numpy array containing the mapped values for the read
                    operation based on both CTRL and PWR mapping
        """
        default_values = self._default_value_mapping_read[mapped_attribute]
        device_values = super()._reshape_r_values(mapped_attribute, device_values)
        value_mapping = self._value_mapper[mapped_attribute]
        mapped_values = numpy.array(default_values)
        return self._mapped_r_values(
            value_mapping, mapped_values, mapped_attribute, device_values
        )

    def map_write_double(
        self, mapped_attribute: str, set_values: List[any]
    ) -> numpy.ndarray:
        """Perform a mapped write for the attribute using the set_values
        only when the mapping is based on TWO maps (CTRL and PWR)

        :param mapped_attribute: attribute identifier as present in
                                 py:attribute:`~_default_value_mapping_read`
        :param set_values: The values to be set for the specified attribute
        :return: mapped_values as a numpy array containing the mapped values for the write
                    operation based on both CTRL and PWR mapping
        """
        default_values = self._masked_value_mapping_write[mapped_attribute]
        mapped_values = []
        for _ in range(self._num_devices):
            mapped_values.append(default_values)
        mapped_values = numpy.array(mapped_values)
        mapped_values = self._mapped_rw_values(
            mapped_values, mapped_attribute, set_values
        )
        return super()._reshape_rw_values(mapped_attribute, mapped_values)

    @abstractmethod
    def _mapped_r_values(
        self,
        value_mapping: List[any],
        mapped_values: numpy.ndarray,
        mapped_attribute: str,
        device_values: List[any],
    ) -> numpy.ndarray:
        """Insert in the mapped values array the read attribute values following
        the rules of the specific mapping

        :param value_mapping: list of the available mappings (e.g. PWR and CTRL)
        :param mapped_values: array with default values to be updated with the mapped ones
        :param mapped_attribute: attribute name to be mapped
        :param device_values: results as gathered by appending attribute values from device
        :return: mapped_values as a numpy array containing the mapped values for the read
                    operation
        """
        raise NotImplementedError("Subclasses must implement this method")

    @abstractmethod
    def _mapped_rw_values(
        self,
        mapped_values: numpy.ndarray,
        mapped_attribute: str,
        set_values: List[any],
    ) -> numpy.ndarray:
        """Insert in the mapped values array the read-write attribute values following
        the rules of the specific mapping

        :param mapped_values: array with default values to be updated with the mapped ones
        :param mapped_attribute: attribute name to be mapped
        :param set_values: the values to be set for the specified attribute
        :return: mapped_values as a numpy array containing the mapped values for the write
                    operation
        """
        raise NotImplementedError("Subclasses must implement this method")


class AFLToRecvMapper(AntennaToRecvMapper):
    """Class that sets a mapping between RECVs and LBA attributes"""

    def __init__(
        self,
        mapping_dict: Dict[str, numpy.ndarray],
        mapped_dimensions: Dict[str, tuple],
        number_of_receivers: Optional[int] = 1,
    ):
        super().__init__(
            mapping_dict,
            mapped_dimensions,
            number_of_receivers,
        )

        self._value_mapper = self._init_value_mapper()

    def _init_value_mapper(self):
        """Create the value mapping dictionary"""
        value_mapper = {}
        for attr in self._mapped_attributes:
            if attr in self._double_mapping:
                value_mapper[attr] = [self._power_mapping, self._control_mapping]
            elif attr in self._pwr_mapping:
                value_mapper[attr] = [self._power_mapping, self._control_mapping]
            else:
                value_mapper[attr] = [self._control_mapping]
        return value_mapper

    def _mapped_rw_values(
        self,
        mapped_values: numpy.ndarray,
        mapped_attribute: str,
        set_values: List[any],
    ) -> numpy.ndarray:
        """Insert in the mapped values array the read-write attribute values following
        the rules of the specific mapping for AntennaField-Low

        :param mapped_values: array with default values to be updated with the mapped ones
        :param mapped_attribute: attribute name to be mapped
        :param set_values: the values to be set for the specified attribute
        :return: mapped_values as a numpy array containing the mapped values for the write
                    operation
        """
        [power_mapping, control_mapping] = self._value_mapper[mapped_attribute]
        for idx, (
            (dev_power, dev_input_power),
            (dev_control, dev_input_control),
        ) in enumerate(zip(power_mapping, control_mapping)):
            if mapped_attribute in self._pwr_mapping:
                if dev_power > 0 and dev_control > 0:
                    mapped_values[dev_power - 1, dev_input_power] = set_values[idx]
                    mapped_values[dev_control - 1, dev_input_control] = set_values[idx]
            else:
                if dev_power > 0:
                    mapped_values[dev_power - 1, dev_input_power] = set_values[idx][0]
                if dev_control > 0:
                    mapped_values[dev_control - 1, dev_input_control] = set_values[idx][
                        1
                    ]
        return mapped_values

    def _mapped_r_values(
        self,
        value_mapping: List[any],
        mapped_values: numpy.ndarray,
        mapped_attribute: str,
        device_values: List[any],
    ) -> numpy.ndarray:
        """Insert in the mapped values array the read attribute values following
        the rules of the specific mapping for the AntennaField-Low

        :param value_mapping: list of the available mappings (e.g. PWR and CTRL)
        :param mapped_values: array with default values to be updated with the mapped ones
        :param mapped_attribute: attribute name to be mapped
        :param device_values: results as gathered by appending attribute values from device
        :return: mapped_values as a numpy array containing the mapped values for the read
                    operation
        """
        # Assuming mapper lists are always in the following order:
        # [Power_Mapping, Control_Mapping]
        [power_mapping, control_mapping] = value_mapping
        for idx, (
            (dev_power, dev_input_power),
            (dev_control, dev_input_control),
        ) in enumerate(zip(power_mapping, control_mapping)):
            if mapped_attribute in self._pwr_mapping:
                # We should report the value of both mappings for each antenna (2 values),
                # but for now we pick only one to keep the resulting array of similar
                # dimension compared to HBA
                if dev_power > 0 and dev_control > 0:
                    mapped_values[idx] = device_values[dev_power - 1][dev_input_power]
            else:
                # Insert the two values in the mapped array
                if dev_power > 0:
                    mapped_values[idx][0] = device_values[dev_power - 1][
                        dev_input_power
                    ]
                if dev_control > 0:
                    mapped_values[idx][1] = device_values[dev_control - 1][
                        dev_input_control
                    ]
        return mapped_values


class AFHToRecvMapper(AntennaToRecvMapper):
    """Class that sets a mapping between RECVs and HBA attributes"""

    def __init__(
        self,
        mapping_dict: Dict[str, numpy.ndarray],
        mapped_dimensions: Dict[str, tuple],
        number_of_receivers: Optional[int] = 1,
    ):
        super().__init__(
            mapping_dict,
            mapped_dimensions,
            number_of_receivers,
        )

        self._value_mapper = self._init_value_mapper()

    def _init_value_mapper(self):
        """Create the value mapping dictionary"""
        value_mapper = {}
        for attr in self._mapped_attributes:
            if attr in self._double_mapping:
                value_mapper[attr] = [self._power_mapping, self._control_mapping]
            elif attr in self._pwr_mapping:
                value_mapper[attr] = [self._power_mapping]
            else:
                value_mapper[attr] = [self._control_mapping]
        return value_mapper

    def _mapped_rw_values(
        self,
        mapped_values: numpy.ndarray,
        mapped_attribute: str,
        set_values: List[any],
    ) -> numpy.ndarray:
        """Insert in the mapped values array the read-write attribute values following
        the rules of the specific mapping for AntennaField-High

        :param mapped_values: array with default values to be updated with the mapped ones
        :param mapped_attribute: attribute name to be mapped
        :param set_values: the values to be set for the specified attribute
        :return: mapped_values as a numpy array containing the mapped values for the write
                    operation
        """
        [power_mapping, control_mapping] = self._value_mapper[mapped_attribute]
        for idx, (
            (dev_power, dev_input_power),
            (dev_control, dev_input_control),
        ) in enumerate(zip(power_mapping, control_mapping)):
            if dev_power > 0:
                mapped_values[dev_power - 1, dev_input_power] = set_values[idx][0]
            if dev_control > 0:
                mapped_values[dev_control - 1, dev_input_control] = set_values[idx][1]
        return mapped_values

    def _mapped_r_values(
        self,
        value_mapping: List[any],
        mapped_values: numpy.ndarray,
        mapped_attribute: str,
        device_values: List[any],
    ) -> numpy.ndarray:
        """Insert in the mapped values array the read attribute values following
        the rules of the specific mapping for the AntennaField-High

        :param value_mapping: list of the available mappings (e.g. PWR and CTRL)
        :param mapped_values: array with default values to be updated with the mapped ones
        :param mapped_attribute: attribute name to be mapped
        :param device_values: results as gathered by appending attribute values from device
        :return: mapped_values as a numpy array containing the mapped values for the read
                    operation
        """
        # Assuming mapper lists are always in the following order:
        # [Power_Mapping, Control_Mapping]
        [power_mapping, control_mapping] = value_mapping
        for idx, (
            (dev_power, dev_input_power),
            (dev_control, dev_input_control),
        ) in enumerate(zip(power_mapping, control_mapping)):
            # Insert the two values in the mapped array
            if dev_power > 0:
                mapped_values[idx][0] = device_values[dev_power - 1][dev_input_power]
            if dev_control > 0:
                mapped_values[idx][1] = device_values[dev_control - 1][
                    dev_input_control
                ]
        return mapped_values


class RecvDeviceWalker:
    """Walks over child devices with the appropriate mask set to affect the hardware
    under the parent's control.."""

    def __init__(
        self, control_to_recv_mapping: numpy.ndarray, antenna_usage_mask: list[bool]
    ):
        self.control_to_recv_mapping = control_to_recv_mapping
        self.antenna_usage_mask = antenna_usage_mask

    def recv_ant_masks(self) -> numpy.ndarray:
        """Return the antenna mask for the control inputs of the antennas enabled
        in Antenna_Usage_Mask_R."""

        nr_recv_devices = max(self.control_to_recv_mapping[:, 0])

        recv_ant_masks = numpy.zeros((nr_recv_devices, N_rcu, N_rcu_inp), dtype=bool)

        for use, (recv, rcu_input) in zip(
            self.antenna_usage_mask, self.control_to_recv_mapping
        ):
            if not use:
                continue
            if recv <= 0:
                continue

            recv_ant_masks[recv - 1][rcu_input // N_rcu_inp][
                rcu_input % N_rcu_inp
            ] = True

        return recv_ant_masks

    def walk_receivers(
        self,
        recv_proxies: list[DeviceProxy],
        visitor_func: Callable[[DeviceProxy], None],
    ):
        """Walk over all RECV devices with the mask set for our antennas."""
        ant_masks = self.recv_ant_masks()

        for recv_proxy, ant_mask in zip(recv_proxies, ant_masks):
            # configure RECV to address only our antennas. Save existing mask.
            old_mask = recv_proxy.ANT_mask_RW
            recv_proxy.ANT_mask_RW = ant_mask

            try:
                visitor_func(recv_proxy)
            finally:
                # restore mask
                recv_proxy.ANT_mask_RW = old_mask
