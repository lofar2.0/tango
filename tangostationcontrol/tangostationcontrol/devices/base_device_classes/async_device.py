# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Async Device"""

from tango import GreenMode, DevState
from tango.server import Device, command

from tangostationcontrol.common.device_decorators import (
    debugit,
    log_exceptions,
    fault_on_error,
    only_in_states,
)
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.asyncio import MonitoredLock
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice

__all__ = ["AsyncDevice"]


class AsyncDevice(LOFARDevice):
    """Inherit LOFARDevice and make it use asyncio greenmode

    It is _NOT_ necessary to redefine the :py:attr:`~green_mode` attribute again in
    child classes of :py:class:`~AsyncDevice`.

    It is recommended to be conservative when making methods and commands async.

    :warning: You can _NOT_ execute any command from another command when using asyncio
              greenmode! Create a non-command alias of the function instead.
    :warning: Ensure all calls to async methods use `await`!
    :warning: Do not call async methods from non-async methods,
              they will not be executed (this happens silently)!
    """

    green_mode = GreenMode.Asyncio

    async def init_device(self):
        """Do not use super as calling LOFARDevice.init_device directly breaks async.

        In green devices, init_device must be async, but LOFARDevice is not.
        Yet LOFARDevice.init_device calls Device.init_device, which is async."""

        await Device.init_device(self)
        self._init_device()

        # Asyncio mode does not have a monitor lock, and thus allows concurrent
        # command execution. We implement our own lock for commands that cannot
        # not run concurrently.
        self.async_command_lock = MonitoredLock(
            "async_command_lock", self.metric_labels
        )

    async def delete_device(self):
        """Call super as LOFARDevice.delete_device never calls Device.delete_device"""
        await super().delete_device()

    def read_attribute(self, attr_name: str):
        raise NotImplementedError("Use async_read_attribute instead!")

    @command()
    async def poll_attributes(self):
        async with self.async_command_lock:
            await self.attribute_poller.poll()

    @command()
    @debugit()
    @only_in_states([DevState.OFF])
    @fault_on_error()
    @log_exceptions()
    async def Initialise(self):
        async with self.async_command_lock:
            super()._Initialise()

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @fault_on_error()
    @log_exceptions()
    async def On(self):
        async with self.async_command_lock:
            self._On()

    @command()
    @debugit()
    @log_exceptions()
    async def Off(self):
        async with self.async_command_lock:
            self._Off()

    @command()
    @debugit()
    @log_exceptions()
    async def Fault(self, new_status="Device is in the FAULT state."):
        async with self.async_command_lock:
            self._Fault(new_status)

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @log_exceptions()
    async def set_defaults(self):
        async with self.async_command_lock:
            self._set_defaults()

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @log_exceptions()
    async def power_hardware_on(self):
        async with self.async_command_lock:
            self._power_hardware_on()

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @log_exceptions()
    async def power_hardware_off(self):
        async with self.async_command_lock:
            self._power_hardware_off()
