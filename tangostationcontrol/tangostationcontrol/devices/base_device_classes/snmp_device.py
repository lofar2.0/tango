# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""SNMP Device Server for LOFAR2.0"""

import logging
import os

try:
    from importlib.resources import files
except ImportError:
    from importlib_resources import files  # type: ignore
from pysmi import debug
from tango.server import device_property, attribute
from tangostationcontrol.clients.snmp.attribute_classes import SNMPAttribute

from tangostationcontrol.clients.snmp.snmp_client import (
    SNMPClient,
    MIBLoader,
    test_SNMP_connection,
)
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)

# Additional import
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.metrics import device_metrics

logger = logging.getLogger()

__all__ = ["SNMPDevice"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        # connection tests can easily timeout, and thus block
        # polling threads
        "can_connect_R",
    ]
)
class SNMPDevice(LOFARDevice):
    """SNMP Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------
    SNMP_community = device_property(dtype="DevString", mandatory=True)

    SNMP_host = device_property(dtype="DevString", mandatory=True)

    SNMP_mib_dir = device_property(dtype="DevString", mandatory=True)

    SNMP_timeout = device_property(dtype="DevDouble", mandatory=True)

    SNMP_version = device_property(dtype="DevULong", mandatory=True)

    SNMP_use_simulators = device_property(dtype="DevBoolean", mandatory=True)

    can_connect_R = attribute(
        dtype=bool,
    )

    def read_can_connect_R(self):
        """
        Test whether we can connect with the SNMP server
        """
        return test_SNMP_connection(self.SNMP_community, self.SNMP_host)

    def __init__(self, cl, name):
        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

        # Configure PySMI debug logging
        debug.setLogger(
            debug.Debug(
                "searcher", "compiler", "borrower", "reader", loggerName="pysmi"
            )
        )

    def configure_for_initialise(self, simulator_class):
        if self.SNMP_use_simulators:
            self.SNMP_attribute_class = simulator_class
        else:
            self.SNMP_attribute_class = SNMPAttribute

        # set up the SNMP client
        self.snmp_manager = SNMPClient(
            self.SNMP_community,
            self.SNMP_host,
            self.SNMP_timeout,
            self.SNMP_version,
            self.SNMP_attribute_class,
            self.Fault,
            self,
        )

        # map an access helper class
        for i in self.attr_list():
            try:
                i.set_comm_client(self, self.snmp_manager)
            except Exception as exc:
                # use the pass function instead of setting read/write fails
                i.set_pass_func(self)
                logger.warning(
                    "error while setting the SNMP attribute %s read/write function. %s",
                    i,
                    exc,
                )

        self.snmp_manager.start()

        super().configure_for_initialise()

    def configure_for_on(self):
        super().configure_for_on()

    def configure_for_off(self):
        super().configure_for_off()

    def get_mib_dir(self):
        """Get MIB files location path"""
        return os.path.dirname(files("tangostationcontrol") / self.SNMP_mib_dir)

    def init_device(self):
        super().init_device()

        # create the MIBLoader and set the mib path
        self.loader = MIBLoader(self.get_mib_dir())

        for i in self.attr_list():
            try:
                # for all of the attributes attempt to load the pre-compiled MIB.
                # Skips already loaded ones
                self.loader.load_pymib(i.comms_annotation["mib"])
            except Exception as exc:
                raise Exception(
                    f"Failed to load MIB file: {i.comms_annotation.get('mib')} \
                        for attribute {i.name} in directory {self.get_mib_dir()} "
                ) from exc
