#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Abstract Hierarchy Device for PyTango devices"""

import logging
import fnmatch
from enum import Enum
from typing import Dict, List, Optional, Callable, Union

from tango import Database, DevState, DeviceProxy

from lofar_station_client.common import CaseInsensitiveDict, CaseInsensitiveString
from tangostationcontrol.common.type_checking import device_name_matches
from tangostationcontrol.common.proxies.proxy import create_device_proxy

logger = logging.getLogger()


class HierarchyMatchingFilter(Enum):
    """Select to filter by exact match, substring or regex"""

    EXACT = 0
    FIND = 1
    REGEX = 2


class NotFoundException(Exception):
    """The search for a node in the hierarchy turned up empty"""


class AbstractHierarchyDevice:
    """AbstractHierarchyDevice

    :warning: Do not actually use ABC to make this an abstract class as it will
              cause conflicting metaclasses with PyTango Device servers
    """

    children_type = Dict[str, Dict[str, Union[DeviceProxy, "children_type"]]]
    child_filter_func_type = Callable[[str, str], bool]
    child_filter_input_type = Union[str, Enum]

    def __init__(self):
        self._children = CaseInsensitiveDict()
        self._parent = None
        self._proxies = CaseInsensitiveDict()

    def __str__(self) -> str:
        return f"Hierarchy of {self._child_property_name}"

    @staticmethod
    def _find_parent(device_name: str, child_property: str):
        """Find parent for the given device_name and child property"""

        db = Database()

        # Get servers: ['stationmanager/STAT', 'digitalbeam/STAT', ...]
        servers = db.get_server_list()

        devices = []

        # Find each device through devices per server: ['STAT/DigitalBeam/HBA', ... ]
        for server in servers:
            devices.extend(db.get_device_name(server, server.split("/")[0]))

        # Iterate over each device getting its children property and determining if we
        # are one of its children, by induction that makes them our parent.
        for device in devices:
            children = db.get_device_property(device, child_property)[child_property]
            for child in children:
                if device_name_matches(child, device_name):
                    return device

        return None

    def init(
        self,
        device_name: str,
        child_property: str,
        proxies: Optional[Dict[str, DeviceProxy]] = None,
    ):
        """Initialize the hierarchy for the device

        :param device_name: Full name of device inheriting AbstractHierarchyDevice
        :param child_property: name of the PyTango device property to identify children
        :param proxies: Optional shared dictionary of device proxies for when device
                        implements multiple hierarchies
        :raises tango.ConnectionFailed: Raised if connecting to the tango database
                                        failed
        :raises tango.CommunicationFailed: Raised if communication with the
                                           tango database failed
        :raises tango.DevFailed: Raised if the tango database encounters an
                                 error
        """

        db = Database()
        children = db.get_device_property(device_name, child_property)[child_property]
        parent = self._find_parent(device_name, child_property)

        if not children and not parent:
            logger.warning(
                "Device: %s has empty hierarchy, %s property is empty and there are no "
                "parents",
                device_name,
                child_property,
                exc_info=True,
            )

        self._child_property_name = child_property
        self._children = CaseInsensitiveDict()
        self._parent = None

        # Store proxies internally upon creation and only pass references to
        # them. Ensures only single instance of DeviceProxy is created per
        # device.
        self._proxies = proxies
        if proxies and not isinstance(proxies, CaseInsensitiveDict):
            self._proxies = CaseInsensitiveDict(proxies)
        else:
            self._proxies = CaseInsensitiveDict()

        if parent:
            self._parent = create_device_proxy(parent)

        if not children:
            children = []
        for child in children:
            self._children[child] = None

    @staticmethod
    def _get_filter(filter_type: HierarchyMatchingFilter) -> child_filter_func_type:
        """Get specific filter function for exact or contains matching

        These functions ensure the matching is case-insensitive

        :param exact: Bool to determine type of filter function
        :return: Function to match exactly or by contains
        """

        def f_regex(name: CaseInsensitiveString, filter_str: str) -> bool:
            return fnmatch.fnmatch(name, filter_str.casefold())

        def f_exact(name: CaseInsensitiveString, filter_str: str) -> bool:
            return name == filter_str

        def f_find(name: CaseInsensitiveString, filter_str: str) -> bool:
            return name.find(filter_str.casefold()) >= 0

        match = f_find
        if filter_type == HierarchyMatchingFilter.EXACT:
            match = f_exact
        elif filter_type == HierarchyMatchingFilter.REGEX:
            match = f_regex

        return match

    def _get_or_create_proxy(self, device: str) -> DeviceProxy:
        """Create a proxy if it does not yet exist otherwise pass reference

        :param device: full device name
        :return: Reference to DeviceProxy from internal cache
        """

        if not self._proxies.get(device):
            self._proxies[device] = create_device_proxy(
                device, 60_000, write_access=True
            )

        return self._proxies[device]

    def _get_children(self, child: str, depth: int) -> children_type:
        """Recursively create dict structure of DeviceProxy and children

        Built a depth-first recursive dict structure terminating at
        :attr:`~depth` by reading the :attr:`~self._child_property` property
        of each proxy. depth may be set to -1 for indefinite recursion

        Resulting datastructure of format
            _children_ = {
                device_string: {
                    'proxy': DeviceProxy(device_string),
                    'children': _children_
                },
                ...
            }

        :warning: Makes no attempt to detect cycles in the tree and if they
                  exist will never terminate and consume infinite memory!
        :param child: full device name string from the current child
        :param depth: Maximum depth to recurse to, -1 for indefinite
        :return: recursive datastructure of proxies and children as described
        """

        proxy = self._get_or_create_proxy(child)

        # TODO(Corne): Assert if this value changes even if the property
        #              has become persistent / immutable with the original value
        #              for the given device. If so resolve potential issues
        children = proxy.get_property(self._child_property_name)[
            self._child_property_name
        ]

        if len(children) == 0 or depth == 0:
            return {"proxy": proxy, "children": CaseInsensitiveDict()}

        # Perform depth-first recursion to build tree of children and their
        # children
        proxies = CaseInsensitiveDict()
        for child in children:
            try:
                proxies[child] = self._get_children(child, depth - 1)
            except Exception as exc:
                raise NotFoundException(
                    f"Could not obtain proxy to child {child} of parent {proxy.dev_name()}"
                ) from exc

        return {"proxy": proxy, "children": proxies}

    def children(self, depth: int = 1) -> children_type:
        """Retrieve DeviceProxies of children up to depth

        :param depth: Maximum steps of traversing children, -1 for unlimited
        :raises tango.NonDbDevice: Raised if the child device does not exist in
                                   the tango database
        :raises tango.ConnectionFailed: Raised if connecting to the tango
                                        database failed
        :raises tango.CommunicationFailed: Raised if communication with the
                                           tango database failed
        :raises tango.DevFailed: Raised if the tango database encounters an
                                 error
        :return: Dict of DeviceProxies, children and grandchildren up to
                 :attr:`~depth` of recursive structure _children_ = {
                    device_string: {
                        'proxy': DeviceProxy(device_string),
                        'children': _children_
                    },
                    ...
                 }
        """

        children = CaseInsensitiveDict()
        for child in self._children:
            children[child] = self._get_children(child, depth - 1)

        return children

    def _children_names(
        self,
        child_filter_str: str,
        child_filter_func: child_filter_func_type,
        children: children_type,
    ) -> List[str]:
        """Return children names matching child_filter substring

        :param child_filter_str: Substring of the device to retrieve device names list
                                 for
        :param children: Recursive dictionary of children of the caller hierarchy
                         device
        """

        device_names = []

        for name, data in children.items():
            if child_filter_func(name, child_filter_str):
                device_names.append(name)

            if not data["children"]:
                continue

            device_names.extend(
                self._children_names(
                    child_filter_str, child_filter_func, data["children"]
                )
            )

        return device_names

    def children_names(
        self,
        child_filter: child_filter_input_type,
        matching_filter: HierarchyMatchingFilter = HierarchyMatchingFilter.FIND,
    ) -> List[str]:
        """Retrieve Device children names matching child_filter substring

        :param child_filter: Substring of the device to retrieve device names list
                                 for
        :param filter_type: Type of filter such as exact, substring or regex
        :return A list of device names matching the child filter substring
        """
        if isinstance(child_filter, Enum):
            child_filter = child_filter.value

        return self._children_names(
            child_filter, self._get_filter(matching_filter), self.children(depth=-1)
        )

    def _child(
        self,
        child_filter_str: str,
        child_filter_func: child_filter_func_type,
        children: children_type,
    ) -> Optional[DeviceProxy]:
        """Recurse :attr:`~children` to find device and return it

        Only returns single device or None
        """

        for name, data in children.items():
            if child_filter_func(name, child_filter_str):
                return data["proxy"]

            if not data["children"]:
                continue

            result = self._child(child_filter_str, child_filter_func, data["children"])
            if result:
                return result

    def child(
        self,
        child_filter: child_filter_input_type,
        matching_filter: HierarchyMatchingFilter = HierarchyMatchingFilter.FIND,
    ) -> Optional[DeviceProxy]:
        """Retrieve DeviceProxy of child matching full name :attr:`~filter`

        :param child_filter: Full name of the device to retrieve DeviceProxy for
        :param filter_type: Type of filter such as exact, substring or regex
        :raises ValueError: Raised if the child could not be found in the
                            hierarchy
        :raises tango.NonDbDevice: Raised if the child device does not exist in
                                   the tango database
        :raises tango.ConnectionFailed: Raised if connecting to the tango
                                        database failed
        :raises tango.CommunicationFailed: Raised if communication with the
                                           tango database failed
        :raises tango.DevFailed: Raised if the tango database encounters an
                                 error
        :return: Return DeviceProxy of child device
        """
        if isinstance(child_filter, Enum):
            child_filter = child_filter.value

        child = self._child(
            child_filter, self._get_filter(matching_filter), self.children(depth=-1)
        )

        if not child:
            raise NotFoundException(
                (
                    f"Could not find child in {self} matching {child_filter} "
                    f"using filter {matching_filter.name}"
                )
            )
        return child

    def branch_child(
        self,
        child_filter: child_filter_input_type,
        matching_filter: HierarchyMatchingFilter = HierarchyMatchingFilter.FIND,
    ) -> Optional[DeviceProxy]:
        """Retrieve Device child matching child_filter substring located in
        the same branch of the hierarchy

        :param child_filter: Substring of the device to retrieve device names list
                                 for
        :param matching_filter: Type of filter such as exact, substring or regex
        :return A device proxy whose name matches the child_filter substring
        """

        def _branch_child(hierarchy_dev=self, depth=0, direct_parent=""):
            """Recursive helper function of branch_child method
            :param hierarchy_dev: The current tree element of the recursion
            :param depth: Depth of the actual recursion
            :param direct_parent: Direct parent of the first level node, which must be excluded
                                    from the results
            """
            if depth == 0:
                direct_parent = hierarchy_dev.parent()

            print(depth)
            print(direct_parent)
            print(hierarchy_dev._child_property_name)
            print(hierarchy_dev._parent)
            print(hierarchy_dev is None)
            print(hierarchy_dev.parent())
            print()

            try:
                child = hierarchy_dev.child(child_filter, matching_filter)
                # Prevent access to direct parent
                if CaseInsensitiveString(child.dev_name()) == CaseInsensitiveString(
                    direct_parent
                ):
                    pass
                else:
                    return child
            except NotFoundException:
                pass

            _hierarchy = AbstractHierarchyDevice()
            _hierarchy.init(hierarchy_dev.parent(), self._child_property_name)

            # Recursive call with updated depth and direct_parent
            return _branch_child(_hierarchy, depth + 1, direct_parent)

        return _branch_child()

    def branch_children_names(
        self,
        child_filter: child_filter_input_type,
        matching_filter: HierarchyMatchingFilter = HierarchyMatchingFilter.FIND,
    ) -> List[str]:
        """Retrieve Device children names matching child_filter substring located in
        the same branch of the hierarchy

        :param child_filter: Substring of the device to retrieve device names list
                                 for
        :param matching_filter: Type of filter such as exact, substring or regex
        :return A list of device names matching the child filter substring
        """

        def _branch_children_names(hierarchy_dev=self, depth=0, direct_parent=""):
            """Recursive helper function of branch_children method
            :param hierarchy_dev: The current tree element of the recursion
            :param depth: Depth of the actual recursion
            :param direct_parent: Direct parent of the first level node, which must be excluded
                                    from the results
            """
            if depth == 0:
                try:
                    direct_parent = hierarchy_dev.parent()
                except NotFoundException:
                    return []

            children = hierarchy_dev.children_names(child_filter, matching_filter)
            # Prevent access to direct parent
            try:
                children.remove(CaseInsensitiveString(direct_parent))
            except ValueError:
                pass

            if len(children) > 0:
                return children
            if len(children) == 0 and not hierarchy_dev._parent:
                return []

            _hierarchy = AbstractHierarchyDevice()
            _hierarchy.init(hierarchy_dev.parent(), self._child_property_name)

            # Recursive call with updated depth and direct_parent
            return _branch_children_names(_hierarchy, depth + 1, direct_parent)

        if isinstance(child_filter, Enum):
            child_filter = child_filter.value

        return _branch_children_names()

    def parent(self) -> str:
        """Return the parent device name. Requires the parent to be unique.

        :return: The device name of the parent if any
        """

        if self._parent:
            return self._parent.dev_name().casefold()

        raise NotFoundException(f"Could not find parent for {self}")

    def read_parent_attribute(self, attribute: str) -> any:
        """Allow to read attribute from parent without direct access

        :param attribute: The attribute to read from the parent, can be RW
        :return: The data from the attribute
        :raises tango.DevFailed: The exception from the DeviceProxy if raised
        """
        if not self._parent:
            raise NotFoundException(f"Could not find parent for {self}")

        return getattr(self._parent, attribute)

    def parent_state(self) -> Optional[DevState]:
        """Return the state of the parent without direct access

        :return: The state of the parent if there is one
        :raises tango.DevFailed: The exception from the DeviceProxy if raised
        """
        if not self._parent:
            raise NotFoundException(f"Could not find parent for {self}")

        return self._parent.state()

    def walk_down(self, func, depth: int = -1):
        """Execute the given function on every node in the tree downwards from the root,
        depth first."""

        def _walk(children, func):
            for child in children.values():
                func(child["proxy"])
                _walk(child["children"], func)

        _walk(self.children(depth), func)

    def walk_up(self, func, depth: int = -1):
        """Execute the given function on every node in the tree upwards from the leaves,
        depth first."""

        def _walk(children, func):
            for child in reversed(children.values()):
                _walk(child["children"], func)
                func(child["proxy"])

        _walk(self.children(depth), func)
