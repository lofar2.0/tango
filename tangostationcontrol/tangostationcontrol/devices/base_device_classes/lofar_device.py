#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Hardware Device Server for LOFAR2.0"""
import json
import logging
import math
import pprint
import textwrap
import time
import os
import sys
import asyncio
from pathlib import Path

from typing import List, Dict, Tuple, Union
from functools import partial
from inspect import isawaitable

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from prometheus_client import generate_latest
from tango import (
    AttReqType,
    AttrDataFormat,
    Attribute,
    AutoTangoAllowThreads,
    DeviceProxy,
    DevFailed,
    DevState,
)
from tango._tango import AttrWriteType

# PyTango imports
from tango.server import attribute, command, class_property, Device, device_property

# Additional import
from tangostationcontrol import __version__ as version
from tangostationcontrol.asyncio import EventLoopThread, PeriodicTask
from tangostationcontrol.common.device_decorators import (
    only_in_states,
    fault_on_error,
    debugit,
    log_exceptions,
    DurationMetric,
)
from tangostationcontrol.common.constants import (
    DEFAULT_METRICS_POLLING_PERIOD_MS,
    DEFAULT_POLLING_PERIOD_MS,
)
from lofar_station_client.common import CaseInsensitiveDict
from tangostationcontrol.common.events import EventSubscriptions, ChangeEvents
from tangostationcontrol.common.json_encoder import JsonEncoder
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
    exception_to_str,
)
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.common.type_checking import sequence_not_str
from tangostationcontrol.devices.base_device_classes.control_hierarchy import (
    ControlHierarchyDevice,
)
from tangostationcontrol.devices.base_device_classes.power_hierarchy import (
    power_hierarchy,
)
from tangostationcontrol.devices.base_device_classes.proxy_timeout import (
    device_proxy_timeout,
)
from tangostationcontrol.metrics import device_metrics, AttributeMetric, device_labels

__all__ = ["LOFARDevice"]

logger = logging.getLogger()


def restart_python():
    """Force a restart this python program.

    This function does not return."""

    exe_path = Path(sys.executable)

    # NOTE: Python 3.4+ closes all file descriptors > 2 automatically, see https://www.python.org/dev/peps/pep-0446/
    os.execv(exe_path, [exe_path.name] + sys.argv)


class AttributePoller:
    """Manages polling of a set of attributes,
    in order to trigger periodic reads from
    hardware or any derived attributes.

    We provide our own polling mechanism in
    favour of Tango's native one, because:

    * It allows full control of reading
      a set of attributes, and therefor
      aspects like timing (avoiding long
      stalls).
    * It allows better tracking of polling
      behaviour.
    * It avoids any multithreading and locking
      complexities inside Tango, which requires
      more care and is thus more complex to
      work with.
    """

    def __init__(self, device: Device):
        self.device = device

        # attributes to poll. This dict is case sensitive
        # because we use it for functions that need the
        # right casing for the attribute name, f.e.
        # read_attribute().
        self._poll_list: Dict[str, Dict[str, object]] = {}

        # change event system
        self.change_events = ChangeEvents(self.device)

    def register(
        self,
        attr_name: str,
        metric: AttributeMetric | None = None,
        send_change_events: bool = True,
    ):
        self._poll_list[attr_name] = {
            "metric": metric,
            "send_change_events": send_change_events,
        }

        if send_change_events:
            self.change_events.configure_attribute(attr_name)

    def is_registered(self, attr_name: str) -> bool:
        # allow case-insensitive checks. we still want to keep the _poll_list
        # cased properly so we explicitly casefold here
        return attr_name in CaseInsensitiveDict(self._poll_list)

    @DurationMetric()
    async def _read_attribute(self, attr_name: str):
        return await self.device.async_read_attribute(attr_name)

    def clear_all(self):
        """Clear all metrics to prevent maintaining stale values."""
        for attr_data in self._poll_list.values():
            if attr_data["metric"]:
                attr_data["metric"].clear()

    async def _read_attribute_nothrow(self, attr_name: str) -> object:
        """Read the attribute without raising an exception."""
        try:
            # try to read the attribute
            return await self._read_attribute(attr_name)
        except asyncio.CancelledError as e:
            logger.info(f"Polling cancelled during read of {attr_name}")
            raise
        except (
            asyncio.TimeoutError,
            OSError,
            DevFailed,
        ) as e:
            # Connection issues are handled through the OPCUAConnection object. Do
            # not complain too hard that we cannot poll some attributes when f.e.
            # we are not connected.
            logger.debug(f"Failed to poll attribute {attr_name}: {exception_to_str(e)}")
        except Exception as e:
            logger.exception(f"Failed to poll attribute {attr_name}")

        return None

    def _update_metric(self, metric: AttributeMetric, value: object | None):
        """Update the given metric with the given value. Clear the metric if the value is None."""

        try:
            if value is None:
                metric.clear()
            else:
                metric.set_value(value)
        except Exception as e:
            logger.exception(f"Failed to update metric {metric}")

    def _send_change_event(self, attr_name: str, value: object | None):
        """Emit a Tango CHANGE_EVENT for the attribute."""
        try:
            self.change_events.send_change_event(attr_name, value)
        except DevFailed as e:
            logger.exception(f"Failed to emit change event for {attr_name}")

    def polling_allowed(self) -> bool:
        # TODO(JDM): Poll attributes based on their individual is_allowed states
        return self.device.is_attribute_access_allowed(AttReqType.READ_REQ)

    @DurationMetric()
    async def _poll(self):
        # NB: The metrics are exposed asynchronously to Prometheus.
        # We must make sure not to leave a metric cleared when we're
        # reading it. If a metric is cleared, it will considered
        # by Prometheus not to exist, leading to gaps in the metric
        # even if it functions correctly.

        for attr_name, attr_data in list(self._poll_list.items()):
            # stop polling if we turned to OFF/FAULT during this loop
            if not self.polling_allowed():
                return

            value = await self._read_attribute_nothrow(attr_name)

            # stop polling if we turned to OFF/FAULT during this loop
            if not self.polling_allowed():
                return

            # Update Prometheus metric, if any
            if attr_data["metric"]:
                self._update_metric(attr_data["metric"], value)

            # Emit Tango CHANGE_EVENT, if configured to
            if attr_data["send_change_events"]:
                if self.device.is_attribute_polled(attr_name):
                    # ChangeEvents.send_change_event is not allowed for attributes already pollled
                    # by Tango. Also, because they're already polled, Tango will emit the change
                    # events for us. So we simply filter those attributes out.
                    logger.warning(
                        f"Not emitting change event for {attr_name} anymore as it is already polled by Tango (which will emit change events instead)."
                    )
                else:
                    self._send_change_event(attr_name, value)

    async def poll(self):
        if not self.polling_allowed():
            # invalidate all metrics
            self.clear_all()

            return

        return await self._poll()


@power_hierarchy
@device_proxy_timeout
@device_logging_to_python()
@device_metrics(
    exclude=[
        # these are exposed by the metrics decorator already,
        # to update them efficiently.
        "version_R",
        "uptime_R",
        "access_count_R",
        "available_in_power_state_R",
        "ds_prometheus_metrics_R",
        "ds_tracemalloc_R",
    ]
)
class LOFARDevice(Device):
    """

    **Properties:**

    States are as follows:
        INIT    = Device is initialising.
        STANDBY = Device is initialised, but pends external configuration and an explicit turning on,
        ON      = Device is fully configured, functional, controls the hardware, and is possibly actively running,
        FAULT   = Device detected an unrecoverable error, and is thus malfunctional,
        OFF     = Device is turned off, drops connection to the hardware,

    The following state transitions are implemented:
        boot -> OFF:        Triggered by tango.  Device will be instantiated,
        OFF  -> INIT:       Triggered by device. Device will initialise (connect to hardware, other devices),
        INIT -> STANDBY:    Triggered by device. Device is initialised, and is ready for additional configuration by the user,
        STANDBY -> ON:      Triggered by user.   Device reports to be functional,
        * -> FAULT:         Triggered by device. Device has degraded to malfunctional, for example because the connection to the hardware is lost,
        * -> FAULT:         Triggered by user.   Emulate a forced malfunction for integration testing purposes,
        * -> OFF:           Triggered by user.   Device is turned off. Triggered by the Off() command,
        FAULT -> INIT:      Triggered by user.   Device is reinitialised to recover from an error,

        The user triggers their transitions by the commands reflecting the target state (Initialise(), On(), Fault()).
    """

    # -----------------
    # Device Properties
    # -----------------

    Power_Children = device_property(dtype="DevVarStringArray", mandatory=False)

    Control_Children = device_property(dtype="DevVarStringArray", mandatory=False)

    Debug = class_property(
        dtype=bool,
        default_value=True,
        doc="The initial debug state when the device is started",
    )

    # ----------
    # Attributes
    # ----------

    version_R = attribute(
        doc="Version of this software device", dtype=str, fget=lambda self: version
    )

    uptime_R = attribute(
        doc="How long this software device has been running (wall-clock time, in seconds)",
        unit="s",
        dtype=numpy.int64,
        fget=lambda self: numpy.int64(time.time() - self.device_start_time),
    )

    @attribute(
        access=AttrWriteType.READ_WRITE,
        dtype=bool,
        doc="Runtime configuration to enable / disable debug functionality",
    )
    def Debug_RW(self):
        return self._debug

    def get_attribute_list(self) -> list[str]:
        return [
            attr
            for attr in dir(self)
            if isinstance(getattr(self, attr), (attribute, Attribute))
        ]

    @Debug_RW.write
    def Debug_RW(self, value):
        self._debug = value
        self._set_log_level(self._debug)

    access_count_R = attribute(
        doc="How often this software device was accessed for commands or attributes",
        dtype=numpy.int64,
        fget=lambda self: numpy.int64(self._access_count),
    )

    hardware_powered_fraction_R = attribute(
        doc="Fraction of hardware that is powered on (0 = nothing, 1 = all).",
        dtype=numpy.float64,
        fget=lambda self: self._read_hardware_powered_fraction_R(),
        fisallowed="is_attribute_access_allowed",
    )

    event_thread_running_R = attribute(
        doc="Whether the event thread is running." "",
        dtype=bool,
        fget=lambda self: self.event_loop_thread
        and self.event_loop_thread.is_running(),
    )

    poll_thread_running_R = attribute(
        doc="Whether the attributes are being polled.",
        dtype=bool,
        fget=lambda self: (
            self.event_loop_thread
            and self.event_loop_thread.is_running()
            and self.poll_task
            and self.poll_task.is_running()
        ),
    )

    @attribute(
        dtype=(str,),
        max_dim_x=10240,
        doc="Prometheus metrics as they are currently exposed on this DeviceServer.",
    )
    def ds_prometheus_metrics_R(self):
        return numpy.array(generate_latest().decode().split("\n"), dtype=str)

    # Only return the top N tracemalloc statistics
    TOPN_TRACEMALLOC_STATISTICS = 16

    @attribute(
        dtype=(str,),
        max_dim_x=TOPN_TRACEMALLOC_STATISTICS,
        doc="Python source-code lines that allocated the most memory",
    )
    def ds_tracemalloc_R(self):
        import tracemalloc

        # start tracing in case we didn't
        tracemalloc.start()

        # obtain and return the top statistics per file
        snapshot = tracemalloc.take_snapshot()
        stats = snapshot.statistics("lineno")
        return [str(x) for x in stats[: self.TOPN_TRACEMALLOC_STATISTICS]]

    # list of translator property names to be set by set_translator_defaults
    TRANSLATOR_DEFAULT_SETTINGS = []

    # list of hardware property names to be set first by set_defaults
    FIRST_DEFAULT_SETTINGS = []

    def __str__(self):
        return self.get_name()

    @classmethod
    def attr_list(cls):
        """Return a list of all the AttributeWrapper members of this class."""
        return [v for k, v in cls.__dict__.items() if isinstance(v, AttributeWrapper)]

    def setup_attribute_wrapper(self):
        """prepare the caches for attribute wrapper objects"""

        self._attribute_wrapper_io = {}

    def is_attribute_access_allowed(self, req_type: AttReqType):
        """Returns whether an attribute wrapped by the AttributeWrapper be accessed."""

        return self.get_state() in DEFAULT_COMMAND_STATES

    # TODO(Corne): Actually implement locking in L2SS-940
    def atomic_read_modify_write_attribute(
        self,
        values: numpy.ndarray,
        proxy: DeviceProxy,
        attribute: str,
        mask_or_sparse=None,
        cast_type=None,
    ):
        """Automatically read-modify-write the attribute on the given proxy

        :param values: New values to write
        :param proxy: Device to write the values to
        :param attribute: Attribute of the device to write
        :param mask_or_sparse: The value or mask used to replace elements in
                               values with current attribute values
        :param cast_type: type to cast numpy array to for delimited merge_writes

        #"""

        # proxy.lock()
        current_values = proxy.read_attribute(attribute).value
        merged_values = self.merge_write(
            values, current_values, mask_or_sparse, cast_type
        )

        if merged_values.dtype.type == numpy.str_:
            # writing string numpy arrays leads to a crash, see
            # https://gitlab.com/tango-controls/pytango/-/issues/492
            merged_values = merged_values.tolist()

        proxy.write_attribute(attribute, merged_values)
        # proxy.unlock()

    def merge_write(
        self,
        merge_values: numpy.ndarray,
        current_values: List[any],
        mask_or_sparse=None,
        cast_type=None,
    ) -> numpy.ndarray:
        """Merge values with current_values retrieved from attribute by mask / sparse

        To be used by the :py:func:`~atomic_read_modify_write_attribute`

        :param merge_values: New values to write and results of merge
        :param current_values: values retrieved from an attribute
        :param mask_or_sparse: The value or mask used to replace elements in
                               merge_values with current_values
        :param cast_type: type to cast numpy array to for delimited merge_writes
        :return:

        """

        # Create shallow copy of merge_values, use native numpy copy as it works
        # for N dimensionality. (native copy.copy() only copies outermost dim)
        merge_values = merge_values.copy()

        if mask_or_sparse is not None and sequence_not_str(mask_or_sparse):
            self._merge_write_mask(merge_values, current_values, mask_or_sparse)
            return merge_values
        else:
            if cast_type is None:
                raise AttributeError("dtype can not be None for sparse merge_write")

            self._merge_write_delimiter(merge_values, current_values, mask_or_sparse)
            return merge_values.astype(dtype=cast_type)

    def _merge_write_delimiter(
        self,
        merge_values: numpy.ndarray,
        current_values: List[any],
        sparse=None,
    ):
        """Merge merge_values and current_values by replacing elements by sparse

        For every element in merge_values where the value is equal to sparse
        replace it by the element in current_values.

        The result can be obtained in merge_values as the list is modified
        in-place (and passed by reference).
        """

        for idx, value in enumerate(merge_values):
            if sequence_not_str(value):
                self._merge_write_delimiter(
                    merge_values[idx], current_values[idx], sparse
                )
            elif value == sparse:
                merge_values[idx] = current_values[idx]

    def _merge_write_mask(
        self, merge_values: List[any], current_values: List[any], mask: List[any]
    ):
        """Merge merge_values and current_values by replacing elements by mask

        For every element in merge_values where the element in mask is false
        replace it by the element in current_values.

        The result can be obtained in merge_values as the list is modified
        in-place (and passed by reference).
        """

        for idx, value in enumerate(merge_values):
            if sequence_not_str(value):
                self._merge_write_mask(
                    merge_values[idx], current_values[idx], mask[idx]
                )
            elif not mask[idx]:
                merge_values[idx] = current_values[idx]

    @command()
    def poll_attributes(self):
        future = self.event_loop_thread.run_coroutine_threadsafe(
            self.attribute_poller.poll()
        )

        with AutoTangoAllowThreads(self):
            # release lock to allow event_loop to use this device
            # to read attributes
            _ = future.result()

    @command(dtype_in=str, dtype_out=bool)
    def is_attribute_polled_by_lofardevice(self, attr_name: str) -> bool:
        return self.attribute_poller.is_registered(attr_name)

    def __init__(self, cl, name):
        self._debug = False

        # a proxy to ourself. can only be constructed in or after init_device
        # is called, during super().__init__().
        self.proxy = None

        self.poll_task: Union[PeriodicTask, None] = None
        self.event_loop_thread = None

        # @device_metrics() will register attributes here
        # after init_device.
        self.attribute_poller = AttributePoller(self)

        super().__init__(cl, name)

        # record how many time this device was accessed
        self._access_count = 0

        # record when this device was started
        self.device_start_time = time.time()

        self.events = EventSubscriptions(device_labels=device_labels(self))

    def _init_device(self):
        logger.debug("[LOFARDevice] init_device")

        self._debug = self.Debug
        self._set_log_level(self._debug)

        self.properties_changed()

        self.set_state(DevState.OFF)
        self.set_status("Device is in the OFF state.")

        # register a proxy to ourselves, to interact with
        # our attributes and commands as a client would.
        #
        # this is required to get/set attributes.
        #
        # we cannot write directly to our attribute, as that would not

        self.proxy = create_device_proxy(self.get_name(), self.Device_Proxy_Timeout)

    def _tango_polled_attributes(self):
        return [
            attr for attr in self.get_attribute_list() if self.is_attribute_polled(attr)
        ]

    def reset_tango_polling(self):
        """Reset the Tango polling configuration.

        Do NOT call this after the device initialisation, as the polling thread might be
        about to block on the GIL to read an attribute (including state/status!). This
        causes any interaction with the poller to block in turn, leading to a deadlock.
        """

        # stop polling everything
        for attr_name in self._tango_polled_attributes():
            logger.info(f"Stop polling {attr_name}")
            try:
                self.stop_poll_attribute(attr_name)
            except DevFailed as ex:
                logger.warning(
                    f"Cannot stop polling {attr_name}: {exception_to_str(ex)}"
                )

        # make sure we poll these as they're not really attributes
        # but some devices want to subscribe to events anyway
        for attr_name in ["State", "Status"]:
            logger.debug(f"Polling {attr_name}")
            try:
                self.poll_attribute(attr_name, DEFAULT_POLLING_PERIOD_MS)
            except DevFailed as ex:
                logger.warning(f"Cannot poll {attr_name}: {exception_to_str(ex)}")

    def _set_log_level(self, debug: bool):
        if debug:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

    @command(dtype_out=str)
    def get_children(self):
        return pprint.pformat(self.control.children(-1))

    @command(dtype_out=str)
    def get_parent(self):
        return pprint.pformat(self.control.parent(), depth=1, indent=4, width=60)

    @log_exceptions()
    def init_device(self):
        """Instantiates the device in the OFF state."""
        # NOTE: Will delete_device first, if necessary
        super().init_device()

        self.reset_tango_polling()

        self._init_device()

    @log_exceptions()
    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command (a Tango built-in).
        """
        logger.info("Shutting down...")

        self._Off()

        logger.info("Shut down.  Good bye.")

    # --------
    # Commands
    # --------

    @fault_on_error()
    def _Initialise(self):
        """
        Command to ask for initialisation of this device. Can only be called in OFF state.

        :return:None
        """

        self.set_state(DevState.INIT)
        self.set_status("Device is in the INIT state.")

        self.setup_attribute_wrapper()

        # reload our class & device properties from the Tango database
        self.get_device_properties()
        self.properties_changed()

        # a thread running to schedule work in. This thread is active
        # from configure_for_initialise up to and including configure_for_off.
        self.event_loop_thread = EventLoopThread(f"LOFARDevice {self.get_name()}")

        # Constructing the control hierarchy can not be part of init_device due to
        # use of `.off(); .put_property(); .boot()` pattern in integration tests!
        self.control = ControlHierarchyDevice()
        self.control.init(self.get_name())

        self.configure_for_initialise()

        # start polling
        self.poll_task = PeriodicTask(
            self.event_loop_thread.event_loop,
            self.attribute_poller.poll,
            DEFAULT_METRICS_POLLING_PERIOD_MS / 1000.0,
        )

        # WARNING: any values read so far are stale.
        # Proxies either need to wait for the next poll round, or
        # use proxy.set_source(DevSource.DEV) to avoid the cache
        # alltogether.
        #
        # Actually clearing the polling cache runs into performance
        # problems if a lot of attributes are polled, and into race
        # conditions with the polling thread itself.

        self.set_state(DevState.STANDBY)
        self.set_status("Device is in the STANDBY state.")

    @command()
    @debugit()
    @only_in_states([DevState.OFF])
    @fault_on_error()
    @log_exceptions()
    def Initialise(self):
        self._Initialise()

    def _On(self):
        if self.get_state() == DevState.ON:
            # Already on. Don't complain.
            logger.warning("Requested to go to ON state, but am already in ON state.")
            return

        self.configure_for_on()

        self.set_state(DevState.ON)
        self.set_status("Device is in the ON state.")

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @fault_on_error()
    @log_exceptions()
    def On(self):
        """
        Command to ask for initialisation of this device. Can only be called in FAULT or OFF state.

        :return:None
        """
        self._On()

    def _Off(self):
        if self.get_state() == DevState.OFF:
            # Already off. Don't complain.
            return

        # Turn off
        self.set_state(DevState.OFF)
        self.set_status("Device is in the OFF state.")

        logger.info("Unsubscribing events from other devices")
        self.events.unsubscribe_all()

        # stop polling (ungracefully, as it may take too long)
        if self.poll_task:
            logger.info("Cancelling AttributePoller task")
            self.poll_task.cancel()
            logger.info("Waiting for AttributePoller task")
            self.poll_task.join()

        # clear metrics, as they will all be stale
        logger.info("Clearing metrics")
        self.attribute_poller.clear_all()

        logger.info(">>> configure_for_off()")
        self.configure_for_off()
        logger.info("<<< configure_for_off()")

        # stop event thread
        try:
            if self.event_loop_thread:
                logger.info("Stopping EventLoop thread")
                self.event_loop_thread.stop()
        finally:
            self.event_loop_thread = None

        # Turn off again, in case of race conditions through reconnecting
        self.set_state(DevState.OFF)
        self.set_status("Device is in the OFF state.")

    @command()
    @debugit()
    @log_exceptions()
    def Off(self):
        """
        Command to ask for shutdown of this device.

        :return:None
        """
        self._Off()

    def _Fault(self, new_status):
        if self.get_state() == DevState.OFF:
            # Spurious FAULT
            logger.warning(
                "Requested to go to FAULT state, but am already in OFF state."
            )
            return

        if self.get_state() == DevState.FAULT:
            # Already faulting. Don't complain.
            logger.warning(
                "Requested to go to FAULT state, but am already in FAULT state."
            )
            return

        self.set_state(DevState.FAULT)
        self.set_status(new_status)

        self.configure_for_fault()

    @command()
    @debugit()
    @log_exceptions()
    def Fault(self, new_status="Device is in the FAULT state."):
        """
        FAULT state is used to indicate the device broke down, and partially or fully
        stopped functioning. Remaining functionality is undefined.

        new_status: The status to set when going to FAULT.

        :return:None
        """
        self._Fault(new_status)

    # functions that can or must be overloaded
    def configure_for_fault(self):
        """Overloadable function called in Fault after state is set to FAULT"""
        pass

    def configure_for_off(self):
        """Overloadable function called in Off after state is set to OFF"""
        pass

    def configure_for_on(self):
        """Overloadable function called in On BEFORE state is set to ON"""
        pass

    def configure_for_initialise(self):
        """Overloadable function called in initialise with state INIT, STANDBY after call"""
        pass

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed or attribute is accessed."""

        # increase the number of accesses
        self._access_count += 1

    def _read_hardware_powered_fraction_R(self):
        """Overloadable function called to ask whether all hardware is powered."""

        # If device backs no hardware, assume it's powered
        return 1.0

    def properties_changed(self):
        pass

    def _reshape_value_for_attribute(self, attribute: str, value: object):
        """Reshape the value to fit the dimensions of the given attribute.
        This is useful for 2D arrays as properties are flattened into 1D."""

        # properties are always 0D or 1D, but arrays can be 2D.
        # in the latter case, we need to reshape the default.
        attr = getattr(self, attribute)
        max_dim_x, max_dim_y = attr.get_max_dim_x(), attr.get_max_dim_y()

        if max_dim_y > 0:
            # 2D array -> reshape 1D default
            # This is needed because Tango properties cannot be 2D, so we
            # cannot specify them in their actual shape.
            #
            # We assume the length of the y dimension to be variable,
            # as some attributes (and thus defaults) have a varying size.
            return numpy.array(value).reshape(-1, max_dim_x)

        # 1D/scalar can be set as is
        return value

    @command(
        dtype_out=str,
        doc_out="Return all defaults set by set_defaults, as a JSON string",
    )
    def get_defaults(self):
        properties = self.get_properties()
        defaults_list = self._get_defaults(properties)
        defaults_dict = {k: v for k, v in defaults_list}

        return json.dumps(defaults_dict, cls=JsonEncoder)

    def _get_defaults(self, properties: Dict[str, object]) -> List[Tuple[str, object]]:
        """Return a list of attribute names and their default values, in the order in
        which they must be applied, as a list of pairs (attribute_name, default_value).
        """

        # get a list of (attribute, value) pairs for which defaults are provided
        attributes_with_defaults = [
            (
                name,
                self._reshape_value_for_attribute(name, properties[f"{name}_default"]),
            )
            # iterator over all our attributes
            for name in dir(self)
            if isinstance(getattr(self, name), Attribute)
            # ignore attributes for which no default setting exists in the properties,
            # and those for which the default is None
            if properties.get(f"{name}_default", None) is not None
        ]

        # order them according to their application:
        # pull TRANSLATOR_DEFAULT_SETTINGS and FIRST_DEFAULT_SETTINGS to the head of the list.
        attributes_with_defaults.sort(
            key=lambda x: (
                x[0] not in self.TRANSLATOR_DEFAULT_SETTINGS,
                x[0] not in self.FIRST_DEFAULT_SETTINGS,
            )
        )

        # return the defaults to set in the required order
        return attributes_with_defaults

    def get_properties(self) -> Dict[str, object]:
        """Return a dictionary of all properties for this device."""
        return {k: v[2] for k, v in self.device_property_list.items()}

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @log_exceptions()
    def set_defaults(self):
        # Defer to undecorated version that might be overloaded
        self._set_defaults()

    def _set_defaults(self):
        """Set hardware points to their default value.

        A hardware point XXX is set to the value of the object member named XXX_default,
        if it exists.
        XXX_default can be f.e. a constant, or a device_property.

        The points are set in the following order:
            1) The python class property 'FIRST_DEFAULT_SETTINGS' is read,
               as an array of strings denoting property names.
               Each property is set in that order.
            2) Any remaining default properties are set, except the translators
               (those in 'TRANSLATOR_DEFAULT_SETTINGS').
        """

        # get all properties
        properties = self.get_properties()

        # get default values for attributes
        for attribute_name, value in self._get_defaults(properties):
            try:
                logger.debug(
                    textwrap.shorten(
                        f"Setting attribute {attribute_name} to {value}", 150
                    )
                )

                # write the default value
                self.proxy.write_attribute(attribute_name, value)
            except Exception as _e:
                # log which attribute we're addressing
                raise Exception(
                    f"Cannot assign default to attribute {attribute}"
                ) from _e

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @log_exceptions()
    def power_hardware_on(self):
        """Power the hardware related to the device."""

        self._power_hardware_on()

    @command()
    @debugit()
    @only_in_states(DEFAULT_COMMAND_STATES)
    @log_exceptions()
    def power_hardware_off(self):
        """Disable the hardware related to the device."""
        self._power_hardware_off()

    @command()
    @debugit()
    @log_exceptions()
    def ds_restart_device_server(self):
        """Restart the device server. Needed to reload casacore, and maybe for nasty bugs. This function does not return."""

        logger.warning("Restarting Device Server")
        restart_python()
        logger.error("Failed to restart Device Server")

    @command()
    def ds_debug_pycharm(self):
        """Activate connection to pycharm debugger"""
        import pydevd_pycharm

        if os.environ.get("DEBUG_HOST") and os.environ.get("DEBUG_HOST") != "":
            pydevd_pycharm.settrace(
                os.environ["DEBUG_HOST"],
                port=12345,
                stdoutToServer=True,
                stderrToServer=True,
            )
        else:
            raise RuntimeError(
                "Failed no ip to connect, need DEBUG_HOST environment variables"
            )

    @only_in_states([DevState.OFF])
    def _boot(self):
        # setup connections
        self._Initialise()

        # initialise settings
        self.set_defaults()

        # make device available
        self._On()

    @command()
    def boot(self):
        self._boot()

    def _power_hardware_on(self):
        """Override this method to load any firmware or power cycle components
        before configuring the hardware."""
        pass

    def _power_hardware_off(self):
        """Override this method to disable any hardware related to the device."""
        pass

    def _read_attribute(self, attr_name: str):
        """Get bound method to read certain attribute (directly from the hardware).

        :warning: Calling this method does not work for async greenmode devices!
        """

        # obtain the class information of this attribute, effectively equal
        # to getattr(self, attr_name), but this also makes sure we actually
        # address an attribute.
        class_attribute = self.get_device_attr().get_attr_by_name(attr_name)

        # obtain the read function (unwrap PyTango's wrapper to avoid interacting
        # with PyTango internals in there).
        read_wrapper = getattr(self, f"__read_{attr_name}_wrapper__")
        while hasattr(read_wrapper, "__wrapped__"):
            read_wrapper = read_wrapper.__wrapped__

        return partial(read_wrapper, self)

    def read_attribute(self, attr_name):
        """Read the value of a certain attribute (directly from the hardware).

        :warning: Async greenmode devices override this with an async version!
        """

        func = self._read_attribute(attr_name)

        if isawaitable(func):
            return asyncio.run(func())
        else:
            result = func()

            return asyncio.run(result) if isawaitable(result) else result

    async def async_read_attribute(self, attr_name):
        """Read the value of a certain attribute (directly from the hardware).

        Provides an asyncio interface.
        """

        func = self._read_attribute(attr_name)

        if isawaitable(func):
            return await func()
        else:
            # yield while running the read_attribute function
            # this also provides a point where CancelledError
            # can be thrown.
            result = await asyncio.to_thread(func)

            return await result if isawaitable(result) else result

    def wait_attribute(self, attr_name, value, timeout=10, pollperiod=0.2):
        """Wait until the given attribute obtains the given value.

        Raises an Exception if it has not after the timeout.

        value:       The value that needs to be matched, or a function
                     that needs to evaluate to True given the attribute.
        timeout:     time until an Exception is raised, in seconds.
        pollperiod:  how often to check the attribute, in seconds.
        """
        if type(value) == type(lambda x: True):
            # evaluate function
            is_correct = value
        else:
            # compare to value
            is_correct = lambda x: x == value

        # Poll every half a second
        for _ in range(math.ceil(timeout / pollperiod)):
            if is_correct(self.read_attribute(attr_name)):
                return

            time.sleep(pollperiod)

        raise Exception(f"{attr_name} != {value} after {timeout} seconds still.")

    def alarm_val(self, attr_name):
        """Returns whether min_alarm < attr_value < max_alarm for the given attribute,
        if these values are set. For arrays, an array of booleans of the same shape
        is returned."""

        # fetch attribute configuration
        attr_config = self.proxy.get_attribute_config(attr_name)
        alarms = attr_config.alarms
        is_scalar = attr_config.data_format == AttrDataFormat.SCALAR

        # fetch attribute value as an array
        value = self.read_attribute(attr_name)
        if is_scalar:
            value = numpy.array(value)  # this stays a scalar in numpy

        # construct alarm state, in the same shape as the attribute
        alarm_state = numpy.zeros(value.shape, dtype=bool)

        if alarms.max_alarm != "Not specified":
            alarm_state |= value >= value.dtype.type(alarms.max_alarm)

        if alarms.min_alarm != "Not specified":
            alarm_state |= value <= value.dtype.type(alarms.min_alarm)

        # return alarm state, with the same dimensions as the attribute
        if is_scalar:
            return alarm_state.item()
        else:
            return alarm_state
