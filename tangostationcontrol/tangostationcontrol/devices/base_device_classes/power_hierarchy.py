# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Power Hierarchy for PyTango devices"""

from typing import Dict, Optional
import logging

from tango import DeviceProxy, DevState
from tango.server import attribute, Device, class_property

from tangostationcontrol.devices.base_device_classes.hierarchy_device import (
    AbstractHierarchyDevice,
)
from tangostationcontrol.states.station_state import run_if_device_on_in_station_state
from tangostationcontrol.states.station_state_enum import StationStateEnum
from tangostationcontrol.common.type_checking import device_class_matches
from tangostationcontrol.common.device_decorators import suppress_exceptions

logger = logging.getLogger()

__all__ = ["PowerHierarchyControlDevice", "power_hierarchy", "power_hierarchy_control"]


class _PowerHierarchyDevice(Device):
    """Power Hierarchy"""

    # -----------------
    # Device Properties
    # -----------------
    Power_Available_In_State = class_property(dtype="DevString", default_value="ON")

    @attribute(dtype=str)
    def available_in_power_state_R(self):
        return self.Power_Available_In_State


def power_hierarchy(cls):
    if not issubclass(cls, Device):
        raise TypeError("{cls.__name__} needs to be a subclass of tango.server.Device")

    return type(
        cls.__name__,
        (cls,) + cls.__bases__,
        dict(_PowerHierarchyDevice.__dict__),
    )


class PowerHierarchyControlDevice(AbstractHierarchyDevice):
    """Power Hierarchy"""

    POWER_CHILD_PROPERTY = "Power_Children"

    def init(
        self,
        device_name: str,
        continue_on_failure: bool = False,
        proxies: Optional[Dict[str, DeviceProxy]] = None,
    ):
        AbstractHierarchyDevice.init(
            self, device_name, self.POWER_CHILD_PROPERTY, proxies
        )

        self.continue_on_failure = continue_on_failure

    def _boot_device(self, device: DeviceProxy):
        """Default sequence of device booting operations"""

        logger.info("Booting %s: off()", device)
        device.off()
        logger.info("Booting %s: initialise()", device)
        device.initialise()
        logger.info("Booting %s: set_defaults()", device)
        device.set_defaults()
        logger.info("Booting %s: on()", device)
        device.on()
        logger.info("Booting %s: Succesful: state=%s", device, device.state())

    def _shutdown_device(self, device: DeviceProxy):
        """Default sequence of device turning-off operations"""
        if device.state() == DevState.OFF:
            logger.info("Shutting down %s: Succesful: It's already OFF?", device)
            return

        logger.info("Shutting down %s: off()", device)
        device.off()
        logger.info("Shutting down %s: Succesful: state=%s", device, device.state())

    def off_to_hibernate(self):
        """Manage the device operations involved in the OFF -> HIBERNATE state transition.
        Only minimal hardware is powered."""

        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.HIBERNATE)
        def boot_to_hibernate(device: DeviceProxy):
            # TODO(JDM): wait for CCDTR to be powered on before booting it?
            self._boot_device(device)

            # CCD: Power on clock
            if device_class_matches(device, "CCD"):
                logger.info("Powering on %s: Clock", device)
                device.power_hardware_on()
                logger.info("Powering on %s: Succesful: Clock", device)

        self.walk_down(boot_to_hibernate, -1)

        # Return the suppressed exceptions
        return boot_to_hibernate.exceptions

    def hibernate_to_standby(self):
        """Manage the device operations involved in the HIBERNATE -> STANDBY state transition.
        Powers hardware except antennas and firmware.
        """

        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.STANDBY)
        def boot_to_standby(device: DeviceProxy):
            self._boot_device(device)

            # UNB2: Power on the Uniboards
            if device_class_matches(device, "UNB2"):
                logger.info("Powering on %s: Uniboards", device)
                device.power_hardware_on()
                logger.info("Powering on %s: Succesful: Uniboards", device)

        self.walk_down(boot_to_standby, -1)

        # Return the suppressed exceptions
        return boot_to_standby.exceptions

    def standby_to_hibernate(self):
        """Manage the device operations involved in the STANDBY -> HIBERNATE state transition."""

        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.STANDBY)
        def power_off_from_standby(device: DeviceProxy):
            # UNB2: Power off the Uniboards
            if device_class_matches(device, "UNB2"):
                logger.info("Powering off %s: Uniboards", device)
                device.power_hardware_off()
                logger.info("Powering off %s: Succesful: Uniboards", device)

        self.walk_up(power_off_from_standby, -1)

        # now transition to hibernate
        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.STANDBY)
        def shutdown_to_hibernate(device: DeviceProxy):
            self._shutdown_device(device)

        self.walk_up(shutdown_to_hibernate, -1)

        # Return the suppressed exceptions
        return power_off_from_standby.exceptions + shutdown_to_hibernate.exceptions

    def standby_to_on(self):
        """Manage the device operations involved in the STANDBY -> ON state transition.
        Powers power-hungry devices (SDP, antennas).
        """

        # first, power on additional (power-hungry) hardware
        @suppress_exceptions(self.continue_on_failure)
        def power_on(device: DeviceProxy):
            # APSCT: Select 200 MHz clock
            if device_class_matches(device, "APSCT"):
                logger.info("Powering on %s: 200MHz clock", device)
                device.power_hardware_on()
                logger.info("Powering on %s: Succesful: 200MHz clock", device)

            # RECV: Power on RCUs
            if device_class_matches(device, ["RECVH", "RECVL"]):
                logger.info("Powering on %s: RCUs", device)
                device.power_hardware_on()
                logger.info("Powering on %s: Succesful: RCUs", device)

            # SDPFirmware: Flash user image
            if device_class_matches(device, "SDPFirmware"):
                logger.info("Powering on %s: User image", device)
                device.power_hardware_on()
                logger.info("Powering on %s: Succesful: User image", device)

        self.walk_down(power_on, -1)

        # now transition to on
        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.ON)
        def boot_to_on(device: DeviceProxy):
            self._boot_device(device)

        self.walk_down(boot_to_on, -1)

        # power on antennas (now that AntennaField is booted)
        @suppress_exceptions(self.continue_on_failure)
        def power_antennas_on(device: DeviceProxy):
            # AntennaField: Power on used antennas
            if device_class_matches(device, ("AFL", "AFH")):
                logger.info("Powering on %s: Antennas", device)
                device.power_hardware_on()
                # TODO(JDM): Report which antennas
                logger.info("Powering on %s: Succesful: Antennas", device)

        self.walk_down(power_antennas_on, -1)

        # Return the suppressed exceptions
        return (
            power_on.exceptions + boot_to_on.exceptions + power_antennas_on.exceptions
        )

    def on_to_standby(self):
        """Manage the device operations involved in the ON -> STANDBY state transition."""

        # turn off power to hardware we also will turn off the software device for
        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.ON)
        def power_off_from_on(device: DeviceProxy):
            # AntennaField: Power off all antennas
            if device_class_matches(device, ("AFL", "AFH")):
                logger.info("Powering off %s: Antennas", device)
                device.power_hardware_off()
                # TODO(JDM): Report which antennas
                logger.info("Powering off %s: Succesful: Antennas", device)

        self.walk_up(power_off_from_on, -1)

        # turn off software devices
        @suppress_exceptions(self.continue_on_failure)
        @run_if_device_on_in_station_state(StationStateEnum.ON)
        def shutdown_to_standby(device: DeviceProxy):
            self._shutdown_device(device)

        self.walk_up(shutdown_to_standby, -1)

        # now turn off power to power-hungry hardware
        @suppress_exceptions(self.continue_on_failure)
        def power_off(device: DeviceProxy):
            # APSCT: Turn off clock
            if device_class_matches(device, "APSCT"):
                logger.info("Powering off %s: Clock", device)
                device.power_hardware_off()
                logger.info("Powering off %s: Succesful: Clock", device)

            # RECV: Power off RCUs
            if device_class_matches(device, ["RECVH", "RECVL"]):
                logger.info("Powering off %s: RCUs", device)
                device.power_hardware_off()
                logger.info("Powering off %s: Succesful: RCUs", device)

            # SDPFirmware: Flash factory image
            if device_class_matches(device, "SDPFirmware"):
                logger.info("Powering off %s: Factory image", device)
                device.power_hardware_off()
                logger.info("Powering off %s: Succesful: Factory image", device)

        self.walk_up(power_off, -1)

        # Return the suppressed exceptions
        return (
            power_off_from_on.exceptions
            + shutdown_to_standby.exceptions
            + power_off.exceptions
        )


def power_hierarchy_control(cls):
    return type(
        cls.__name__,
        (cls,) + cls.__bases__ + PowerHierarchyControlDevice.__bases__,
        dict(PowerHierarchyControlDevice.__dict__),
    )
