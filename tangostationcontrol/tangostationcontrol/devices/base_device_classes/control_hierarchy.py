# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Control Hierarchy for PyTango devices"""
from typing import Dict
from typing import Optional

from tango import DeviceProxy

from tangostationcontrol.devices.base_device_classes.hierarchy_device import (
    AbstractHierarchyDevice,
)


class ControlHierarchyDevice(AbstractHierarchyDevice):
    CONTROL_CHILD_PROPERTY = "Control_Children"

    def init(
        self,
        device_name: str,
        proxies: Optional[Dict[str, DeviceProxy]] = None,
    ):
        super().init(
            device_name,
            self.CONTROL_CHILD_PROPERTY,
            proxies,
        )
