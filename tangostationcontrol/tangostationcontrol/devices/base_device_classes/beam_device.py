#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Beam Abstract Device Server for LOFAR2.0"""

import asyncio
import datetime
import logging
import time
from json import loads
from threading import Lock, Condition
from typing import Awaitable
from typing import Callable

import numpy
from tango import (
    AttrWriteType,
    DebugIt,
    DevVarStringArray,
    DevVarDoubleArray,
    DevString,
)

# PyTango imports
from tango.server import attribute, command, device_property

from tangostationcontrol.common.atomic import Atomic
from tangostationcontrol.beam.delays import (
    pointing_to_str,
    get_IERS_timestamp,
    is_valid_pointing,
)
from tangostationcontrol.beam.managers import AbstractBeamManager
from tangostationcontrol.common.constants import MAX_POINTINGS, N_point_prop
from tangostationcontrol.common.device_decorators import (
    only_in_states,
    fault_on_error,
    log_exceptions,
)

# Additional import
from tangostationcontrol.common.lofar_logging import exception_to_str
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.common.threading import OmniThread
from tangostationcontrol.devices.base_device_classes.async_device import AsyncDevice
from tangostationcontrol.metrics import device_metrics

__all__ = ["BeamDevice", "BeamTracker"]

logger = logging.getLogger()


@device_metrics(
    exclude=[
        "Last_update_pointing_exception_R",
        "Pointing_direction_R",
        "Pointing_direction_RW",
    ]
)
class BeamDevice(AsyncDevice):
    """BeamDevice is a PyTango Asyncio greenmode device

    :warning: You can _NOT_ execute any command from another command when using asyncio
              greenmode! Create a non-command alias of the function instead.
    :warning: Ensure all calls to async methods use `await`!
    :warning: Do not call async methods from non-async methods,
              they will not be executed (this happens silently)!
    """

    def __init__(self, cl, name):
        self._beam_manager = None
        self._num_pointings = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    # -----------------
    # Device Properties
    # -----------------

    Beam_tracking_interval = device_property(
        dtype="DevFloat",
        doc="Beam weights updating interval time [seconds]",
        mandatory=False,
        default_value=1.0,
    )

    Beam_tracking_application_offset = device_property(
        dtype="DevFloat",
        doc="Amount of time to send the weights earlier than the interval, \
            to allow the hardware to get apply them in time [seconds]",
        mandatory=False,
        default_value=0.05,
    )

    Beam_tracking_preparation_period = device_property(
        dtype="DevFloat",
        doc="Preparation time [seconds] needed before starting update operation",
        mandatory=False,
        default_value=0.4,
    )

    Tracking_enabled_RW_default = device_property(
        dtype="DevBoolean", mandatory=False, default_value=True
    )

    # ----------
    # Attributes
    # ----------

    # The actual number of pointings for this device
    # will be stored as self._num_pointings.

    Pointing_direction_R = attribute(
        access=AttrWriteType.READ,
        dtype=((str,),),
        max_dim_x=N_point_prop,
        max_dim_y=MAX_POINTINGS,
        fget=lambda self: self._beam_manager.current_pointing_direction,
        fisallowed="is_attribute_access_allowed",
    )

    Pointing_direction_RW = attribute(
        access=AttrWriteType.READ_WRITE,
        dtype=((str,),),
        max_dim_x=N_point_prop,
        max_dim_y=MAX_POINTINGS,
        fget=lambda self: self._beam_manager.new_pointing_direction,
        fisallowed="is_attribute_access_allowed",
    )

    Pointing_direction_str_R = attribute(
        access=AttrWriteType.READ,
        doc="Pointing direction as a formatted string",
        dtype=(str,),
        max_dim_x=MAX_POINTINGS,
        fget=lambda self: [
            pointing_to_str(tuple(x))
            for x in self._beam_manager.current_pointing_direction
        ],
        fisallowed="is_attribute_access_allowed",
    )

    Pointing_timestamp_R = attribute(
        access=AttrWriteType.READ,
        dtype=(numpy.double,),
        max_dim_x=MAX_POINTINGS,
        fget=lambda self: self._beam_manager.current_pointing_timestamp,
        fisallowed="is_attribute_access_allowed",
    )

    # Deviation from ideal pointing update moment
    Pointing_error_R = attribute(
        access=AttrWriteType.READ,
        dtype=(numpy.double,),
        max_dim_x=MAX_POINTINGS,
        fget=lambda self: self._beam_manager.current_pointing_error,
        fisallowed="is_attribute_access_allowed",
    )

    Tracking_enabled_R = attribute(
        access=AttrWriteType.READ,
        doc="Whether the beam is updated periodically",
        dtype=bool,
        fget=lambda self: bool(self.Beam_tracker and self.Beam_tracker.is_alive()),
    )

    Tracking_enabled_RW = attribute(
        access=AttrWriteType.READ_WRITE,
        doc="Whether the beam should be updated periodically",
        dtype=bool,
        fget=lambda self: self._tracking_enabled_rw,
    )

    Nr_update_pointing_exceptions_R = attribute(
        access=AttrWriteType.READ,
        doc="Number of exceptions thrown when updating the pointing in the Beam_tracker",
        dtype=numpy.int32,
        fget=lambda self: (
            self.Beam_tracker.nr_update_exceptions if self.Beam_tracker else 0
        ),
        fisallowed="is_attribute_access_allowed",
    )

    Last_update_pointing_exception_R = attribute(
        access=AttrWriteType.READ,
        doc="Last thrown exception when updating the pointing in the Beam_tracker",
        dtype=str,
        fget=lambda self: (
            exception_to_str(self.Beam_tracker.last_update_exception)
            if self.Beam_tracker and self.Beam_tracker.last_update_exception
            else ""
        ),
        fisallowed="is_attribute_access_allowed",
    )

    Last_update_pointing_exception_timestamp_R = attribute(
        access=AttrWriteType.READ,
        doc="When last thrown exception was thrown when updating the pointing in the Beam_tracker",
        unit="s",
        dtype=numpy.float64,
        fget=lambda self: (
            self.Beam_tracker.last_update_exception_timestamp.timestamp()
            if self.Beam_tracker and self.Beam_tracker.last_update_exception_timestamp
            else 0.0
        ),
        fisallowed="is_attribute_access_allowed",
    )

    def write_Pointing_direction_RW(self, value):
        """Setter method for attribute Pointing_direction_RW"""
        # verify whether values are valid
        if len(value) != self._num_pointings:
            raise ValueError(
                f"Expected {self._num_pointings} directions, got {len(value)}"
            )

        for pointing in value:
            if not is_valid_pointing(tuple(pointing)):
                raise ValueError(f"Invalid direction: {pointing}")

        # store the new values
        self._beam_manager.new_pointing_direction = numpy.array(value, dtype="<U32")

        # force update if pointing changes
        self.Beam_tracker.force_update()
        logger.info("Pointing direction update requested")

    def write_Tracking_enabled_RW(self, value):
        self._tracking_enabled_rw = value

        if value:
            self.Beam_tracker.start()
        else:
            self.Beam_tracker.stop()

    # --------
    # overloaded functions
    # --------

    async def _read_hardware_powered_fraction_R(self):
        # We consider 'powered' if the beam tracker is running
        return 1.0 * await self.async_read_attribute("Tracking_enabled_R")

    def _init_device(self, beam_manager: AbstractBeamManager):
        super()._init_device()

        self._beam_manager: AbstractBeamManager = beam_manager

        # Initialise pointing array data and attribute
        self._tracking_enabled_rw = False

        # thread to perform beam tracking
        self.Beam_tracker = None

    # Derived  classes will override this with a non-parameterised
    # version that lofar_device will call.
    @log_exceptions()
    def configure_for_initialise(self, num_pointings):
        super().configure_for_initialise()

        if not (0 < num_pointings <= MAX_POINTINGS):
            raise ValueError(
                f"beam_device is configured to support 0 - {MAX_POINTINGS} pointings, \
                but {num_pointings} were requested"
            )

        # Initialise tracking control
        self._num_pointings = num_pointings
        self._beam_manager.current_pointing_timestamp = numpy.zeros(
            num_pointings, dtype=numpy.double
        )
        self._beam_manager.current_pointing_error = numpy.zeros(
            num_pointings, dtype=numpy.double
        )
        self._beam_manager.current_pointing_direction = numpy.zeros(
            (num_pointings, N_point_prop), dtype="<U32"
        )
        self._beam_manager.new_pointing_direction = numpy.array(
            [["None", "", ""]] * num_pointings, dtype="<U32"
        )

        self._beam_manager.beam_tracking_application_offset = (
            self.Beam_tracking_application_offset
        )

        self._tracking_enabled_rw = self.Tracking_enabled_RW_default

        # Create a thread object to update beam weights
        self.Beam_tracker = BeamTracker(
            self.Beam_tracking_interval,
            self.Beam_tracking_preparation_period,
            self._beam_manager.update_pointing,
        )

    @log_exceptions()
    def configure_for_on(self):
        super().configure_for_on()

        # Start beam tracking thread
        if self._tracking_enabled_rw:
            self.Beam_tracker.start()

    @log_exceptions()
    def configure_for_off(self):
        if self.Beam_tracker:
            # Stop thread object
            self.Beam_tracker.stop()

        super().configure_for_off()

    # --------
    # Commands
    # --------

    @command(dtype_in=DevVarStringArray)
    @DebugIt()
    @log_exceptions()
    @only_in_states(DEFAULT_COMMAND_STATES)
    async def set_pointing(self, pointing_direction: list):
        """
        Compute and uploads the hardware-specific delays based on a given pointing direction
        2D array (num_pointings x 3 parameters).
        """

        # Reshape the flatten input array
        pointing_direction = numpy.array(pointing_direction).reshape(
            self._num_pointings, N_point_prop
        )

        await self._beam_manager.set_pointing(
            pointing_direction, datetime.datetime.now()
        )

    @command(dtype_in=DevString)
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    async def set_pointing_for_specific_time(self, parameters: DevString = None):
        """
        Compute and uploads the hardware-specific delays based on a given pointing direction
        2D array (num_pointings x 3 parameters) for the given timestamp

        Parameters are given in a json document.
        pointing_direction: array of 96 casacore pointings (3 string parameters)
        timestamp: POSIX timestamp
        """
        pointing_parameters = loads(parameters)

        pointing_direction = list(pointing_parameters["pointing_direction"])
        timestamp_param = float(pointing_parameters["timestamp"])
        timestamp = datetime.datetime.fromtimestamp(timestamp_param)

        # Reshape the flatten pointing array
        pointing_direction = numpy.array(pointing_direction).reshape(
            self._num_pointings, N_point_prop
        )

        await self._beam_manager.set_pointing(pointing_direction, timestamp)

    @command(dtype_in=DevVarStringArray, dtype_out=DevVarDoubleArray)
    @DebugIt()
    @log_exceptions()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def delays(self, pointing_direction: numpy.array):
        """
        Calculate the delays based on the pointing list and the timestamp
        """

        pointing_direction = numpy.array(pointing_direction).reshape(
            self._num_pointings, N_point_prop
        )

        delays = self._beam_manager.delays(pointing_direction, datetime.datetime.now())

        return delays.flatten()

    # --------
    # Measures
    # --------

    # Directory where the casacore measures that we use, reside. We configure ~/.casarc to
    # use the symlink /opt/IERS, which we switch to the actual set of files to use.
    IERS_timestamp_R = attribute(
        doc="Timestamp of the IERS tables used.",
        dtype=numpy.float64,
        access=AttrWriteType.READ,
        fget=lambda self: get_IERS_timestamp().timestamp(),
    )


# ----------
# Beam Tracker
# ----------


class BeamTracker:
    """Object encapsulates a Thread, responsible for beam tracking

    BeamTracker should update at regular predictable intervals accounting
    for :py:attr:`~preparation_time`. At these intervals the
    :py:attr:`~update_pointing_callback` will be called.

    Operational rules:
    1. If we miss an interval we should still update as soon as possible
    2. If we estimate to miss an interval (due to preparation time),
       still update as soon as possible.
    3. If we update because of a stale pointing we should still update
       at the next interval (even though the interval between those
       updates is shorter)
    """

    JOIN_TIMEOUT = 3.0

    def __init__(
        self,
        interval: float,
        preparation_time: float,
        update_pointing_callback: Callable[[datetime.datetime], Awaitable[None]],
    ):
        """"""

        self.done = Atomic(False)
        self.thread = None

        self.interval = interval
        self.preparation_time = preparation_time

        self.update_pointing_callback = update_pointing_callback

        # Condition to trigger a forced update or early abort
        self.update_lock = Lock()
        self.update_condition = Condition(self.update_lock)

        # Number of times we couldn't update the pointing for some reason
        self.nr_update_exceptions = 0
        self.last_update_exception = None
        self.last_update_exception_timestamp = None

        # Whether the pointing has to be forced updated
        self.stale_pointing = True

    def start(self):
        """Starts the Beam Tracking thread"""
        if self.thread:
            # already started
            return

        self.done.value = False
        self.thread = OmniThread(
            target=self._update_pointing_direction,
            name="BeamTracker",
            args=(self.done,),
        )
        self.thread.start()

        logger.info("[BeamTracking] Thread started")

    def is_alive(self) -> bool:
        """True just before and after run method starts and terminates"""
        return self.thread and self.thread.is_alive()

    def force_update(self):
        """Force the pointing to be updated."""

        self.stale_pointing = True
        self.notify_thread()

    def notify_thread(self):
        """Wake up :py:method:`~_update_pointing_direction` thread"""
        with self.update_lock:
            self.update_condition.notify()

    def stop(self):
        """Stops the Beam Tracking loop"""

        if not self.thread:
            return

        logger.info("[BeamTracking] Thread stopping")

        self.done.value = True
        self.force_update()

        # wait for thread to finish
        self.thread.join(self.JOIN_TIMEOUT)

        if self.is_alive():
            logger.error("[BeamTracking] Thread did not properly terminate")

        self.thread = None

        logger.info("[BeamTracking] Thread stopped")

    def _get_sleep_time(self, next_update_in: datetime.datetime) -> float:
        """Compute the sleep time before beginning next update

        :return: Sleep time in seconds
        """

        # Computes the needed sleep time before the next update
        now = datetime.datetime.now().timestamp()
        sleep_time = (
            abs((next_update_in.timestamp() - now) % self.interval)
            - self.preparation_time
        )

        if sleep_time < 0:
            sleep_time = 0

        if now > next_update_in.timestamp() - self.preparation_time:
            logger.warning(
                "[BeamTracking] Can not keep up, sleep time expired "
                "before await condition! increase interval time"
            )
            return sleep_time

        return sleep_time

    def _update_pointing(self, timestamp: datetime.datetime):
        """Wrap the update_pointing_callback to prevent propagating fleeting errors
        when updating the pointing."""

        try:
            asyncio.run(self.update_pointing_callback(timestamp))
        except Exception as excep:
            # Tango communication errors and timeouts shouldn't permanently stop the tracking
            logger.exception("Could not update pointing")

            self.last_update_exception = excep
            self.last_update_exception_timestamp = datetime.datetime.now()
            self.nr_update_exceptions += 1

    def get_next_update_in(self, now: datetime.datetime):
        """Find the next interval

        :param now: current datetime
        """

        return (now + datetime.timedelta(seconds=self.interval)) - (
            datetime.timedelta(seconds=now.timestamp() % self.interval)
        )

    @log_exceptions()
    @fault_on_error()
    def _update_pointing_direction(self, done: Atomic):
        """Updates the beam weights using a fixed interval of time

        Do not execute directly, run on separate thread see :py:method:`~start`
        """

        last_update = datetime.datetime.min
        logger.debug("[BeamTracker] Successfully became alive :)")

        with self.update_lock:
            while True:
                # apply next pointing at next interval,
                now = datetime.datetime.now()

                # If last update returns early sleep to prevent two updates
                # in single interval
                if now <= last_update:
                    # Determine how much earlier or later returned then expected
                    error = last_update - now
                    logger.debug(
                        "[BeamTracker] Last update returned early sleeping for %s",
                        error,
                    )
                    time.sleep(error.total_seconds())
                    now = datetime.datetime.now()

                # normalized interval function, will generate sequence
                # at the rate of the given interval at predictable moments
                # (interval = 10 results in 0.000, 10.000, 20.000)
                next_update_in = self.get_next_update_in(now)

                # sleep until the next update, or when interrupted
                # (this releases the lock, allowing for notification)
                # note that we need wait_for as conditions can be triggered
                # multiple times in succession
                self.update_condition.wait_for(
                    lambda: done.value or self.stale_pointing,
                    self._get_sleep_time(next_update_in),
                )

                # Terminate thread when done
                if done.value:
                    break

                # Update stale pointing immediately
                if self.stale_pointing:
                    self.stale_pointing = False
                    self._update_pointing(datetime.datetime.now())
                    continue

                self._update_pointing(next_update_in)

                # Track when next_update_in should have happened
                last_update = next_update_in
