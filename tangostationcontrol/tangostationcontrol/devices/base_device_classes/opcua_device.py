#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Generic OPC-UA Device Server for LOFAR2.0"""

import asyncio
import logging
from datetime import datetime

import numpy

# PyTango imports
from tango.server import device_property, attribute, Device
from tangostationcontrol.clients.opcua_client import (
    OPCUAConnection,
    OPCUA_connection_tester,
    OPCUAConnectionStatus,
)
from tangostationcontrol.common.lofar_logging import exception_to_str
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.metrics import device_metrics, AttributeMetric, device_labels
from prometheus_client import Counter

# Additional import

logger = logging.getLogger()

__all__ = ["OPCUADevice"]


@device_metrics(
    exclude=[
        "last_disconnect_exception_R",
        "opcua_missing_attributes_R",
        # connection tests can easily timeout, and thus block
        # polling threads
        "can_connect_R",
    ]
)
class OPCUADevice(LOFARDevice):
    """

    **Properties:**

    - Device Property
        OPC_Server_Name
            - Type:'DevString'
        OPC_Server_Port
            - Type:'DevULong'
        OPC_Time_Out
            - Type:'DevDouble'
    """

    class MonitoredOPCUAConnectionStatus(OPCUAConnectionStatus):
        def __init__(self, device: Device):
            self.last_disconnect_exception = None
            self.last_disconnect_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self._connected = False
            self._device = device

            self.ds_opcua_nr_connection_losses = AttributeMetric(
                "opcua_connection_losses",
                "Number of connection losses to the OPCUA server",
                device_labels(device),
                Counter,
            ).get_metric()
            self.ds_opcua_connected = AttributeMetric(
                "opcua_connected",
                "Is connected to OPCUA server",
                device_labels(device),
            ).get_metric()

            # provide an initial value for the metrics (or they won't be exposed)
            self.ds_opcua_nr_connection_losses.inc(0)
            self.ds_opcua_connected.set(False)

        def connection_lost(self):
            self.ds_opcua_nr_connection_losses.inc()

        @property
        def connected(self):
            return self._connected

        @connected.setter
        def connected(self, value):
            self._connected = value
            self.ds_opcua_connected.set(value)

    # -----------------
    # Device Properties
    # -----------------

    OPC_Server_Name = device_property(dtype="DevString", mandatory=True)

    OPC_Server_Port = device_property(dtype="DevULong", mandatory=True)

    OPC_Time_Out = device_property(dtype="DevDouble", mandatory=True)

    OPC_namespace = device_property(
        dtype="DevString", mandatory=False, default_value="http://lofar.eu"
    )

    # Add these elements to the OPC-UA node path.
    OPC_Node_Path_Prefix = device_property(
        dtype="DevVarStringArray", mandatory=False, default_value=[]
    )

    # ----------
    # Attributes
    # ----------

    opcua_missing_attributes_R = attribute(
        max_dim_x=128,
        dtype=(str,),
        fget=lambda self: numpy.array(self.opcua_missing_attributes, dtype=str),
        doc="OPC-UA attributes that this device requested, but which are not exposed on the server. These attributes are replaced with a no-op and thus do not function as expected.",
    )

    # Connection status attributes
    last_disconnect_exception_R = attribute(
        dtype=str,
        fget=lambda self: exception_to_str(
            self.opcua_connection.connection_status.last_disconnect_exception
        ),
        fisallowed="is_attribute_access_allowed",
    )
    last_disconnect_time_R = attribute(
        dtype=str,
        fget=lambda self: self.opcua_connection.connection_status.last_disconnect_time,
        fisallowed="is_attribute_access_allowed",
    )
    connected_R = attribute(
        dtype=bool,
        fget=lambda self: self.opcua_connection.connection_status.connected,
        fisallowed="is_attribute_access_allowed",
    )

    @attribute(dtype=bool)
    def can_connect_R(self):
        """
        Test whether we can connect with the OPC-ua server.
        Sets up an OPC ua connection from scratch, as it cannot be assumed
        that self.opcua_connection has been created or functions
        """
        try:
            loop = asyncio.get_running_loop()
        except RuntimeError as _e:
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
        return loop.run_until_complete(
            OPCUA_connection_tester(self.OPC_Server_Name, self.OPC_Server_Port)
        )

    # --------
    # overloaded functions
    # --------

    def init_device(self):
        super().init_device()

        self.opcua_connection_status = self.MonitoredOPCUAConnectionStatus(self)

    def configure_for_initialise(self):
        """user code here. is called when the state is set to INIT"""

        # set up the OPC ua client
        self.opcua_connection = OPCUAConnection(
            f"opc.tcp://{self.OPC_Server_Name}:{self.OPC_Server_Port}/",
            self.OPC_namespace,
            self.OPC_Time_Out,
            self.opcua_connection_status,
            # TODO(JDM): This results in a deadlock when polling attributes and mixing green/non-green functionality
            # event_loop=self.event_loop_thread.event_loop,
            device=self,
        )
        self.opcua_connection.node_path_prefix = self.OPC_Node_Path_Prefix

        self.opcua_missing_attributes = []

        # schedule the opc-ua initialisation, and wait for it to finish
        future = asyncio.run_coroutine_threadsafe(
            self._connect_opcua(), self.opcua_connection.event_loop
        )
        _ = future.result()

    async def _connect_opcua(self):
        # connect
        await self.opcua_connection.start()

        # map an access helper class
        for i in self.attr_list():
            try:
                if not i.comms_id or i.comms_id == OPCUAConnection:
                    await i.async_set_comm_client(self, self.opcua_connection)
            except Exception as e:
                # use the pass function instead of setting read/write fails
                i.set_pass_func(self)
                annotation = self.opcua_connection._fix_annotation(i.comms_annotation)

                self.opcua_missing_attributes.append(
                    ",".join(
                        self.opcua_connection.get_full_node_path(annotation["path"])
                    )
                )

                logger.warning(
                    f"Error while setting the attribute {i.comms_annotation} read/write function.",
                    exc_info=True,
                )

    def configure_for_off(self):
        """user code here. is called when the state is set to OFF"""
        try:
            # disconnect
            self.opcua_connection.sync_stop()
        except Exception as e:
            logger.warning(
                "Exception while stopping OPC ua connection in configure_for_off function: {}. Exception ignored".format(
                    e
                )
            )
