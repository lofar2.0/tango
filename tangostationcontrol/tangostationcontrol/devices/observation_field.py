# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime

import logging
from itertools import chain
from time import monotonic
from typing import Optional, List

import numpy
from jsonschema.exceptions import ValidationError

# PyTango imports
from tango import AttrWriteType, DeviceProxy, DevState, Util
from tango.server import attribute
from tangostationcontrol.common.constants import (
    MAX_ANTENNA,
    MAX_PARALLEL_SUBBANDS,
    N_beamlets_ctrl,
    N_elements,
    N_pn,
    N_point_prop,
    N_pol,
)
from tangostationcontrol.common.device_decorators import log_exceptions
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.antennas import device_member_to_full_device_name
from tangostationcontrol.configuration import ObservationFieldSettings
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.common.device_decorators import fault_on_error
from tangostationcontrol.common.device_decorators import only_in_states
from tangostationcontrol.metrics import device_metrics


logger = logging.getLogger()

__all__ = ["ObservationField"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "observation_field_settings_RW",
        "saps_pointing_R",
        "saps_subband_R",
    ]
)
class ObservationField(LOFARDevice):
    """ObservationField Device for LOFAR2.0

    This Tango device is responsible for the set-up of hardware for a
    specific observation and antenna field pair.  It will, if necessary keep tabs on
    HW MPs to signal issues that are not caught by MPs being outside their nominal
    range.

    The lifecycle of instances of this device is controlled by ObservationControl.

    Settings are written to observation_field_settings_RW, which can only be done
    in the OFF state.
    """

    # Attributes
    @attribute(
        doc="Observation ID",
        dtype=numpy.int64,
    )
    def observation_id_R(self):
        return self._observation_field_settings.observation_id

    @attribute(
        doc="Which antenna field this ObservationField configures",
        dtype=str,
        fisallowed="is_attribute_access_allowed",
    )
    def antenna_field_R(self):
        return self._observation_field_settings.antenna_field

    @attribute(
        doc="Observation start time (seconds since 1970), or 0 if immediate.",
        unit="s",
        dtype=numpy.float64,
    )
    def start_time_R(self):
        return (
            datetime.fromisoformat(
                self._observation_field_settings.start_time
            ).timestamp()
            if self._observation_field_settings.start_time
            else 0.0
        )

    @attribute(
        doc="Observation stop time (seconds since 1970).",
        unit="s",
        dtype=numpy.float64,
    )
    def stop_time_R(self):
        return datetime.fromisoformat(
            self._observation_field_settings.stop_time
        ).timestamp()

    @attribute(
        doc="Seconds to be on sky before the observation start time, to fill buffers "
        "and allow for negative geometric delay compensation downstream.",
        unit="s",
        dtype=numpy.float64,
    )
    def lead_time_R(self):
        return self._observation_field_settings.lead_time or 0.0

    @attribute(
        doc="Which antenna set this ObservationField configures",
        dtype=str,
        fisallowed="is_attribute_access_allowed",
    )
    def antenna_set_R(self):
        return self._observation_field_settings.antenna_set

    @attribute(
        doc="Whether to add dithering to the signal to increase its linearity",
        dtype=bool,
        fisallowed="is_attribute_access_allowed",
    )
    def dithering_enabled_R(self):
        try:
            return self._observation_field_settings.dithering.enabled or False
        except AttributeError:
            return False

    @attribute(
        doc="Power of dithering signal, in dBm",
        unit="dBm",
        dtype=numpy.float64,
        fisallowed="is_attribute_access_allowed",
    )
    def dithering_power_R(self):
        try:
            return self._observation_field_settings.dithering.power or -4.0
        except AttributeError:
            return -4.0

    @attribute(
        doc="Frequency of dithering signal, in Hz",
        unit="Hz",
        dtype=numpy.int64,
        fisallowed="is_attribute_access_allowed",
    )
    def dithering_frequency_R(self):
        try:
            return self._observation_field_settings.dithering.frequency or 102_000_000
        except AttributeError:
            return 102_000_000

    @attribute(
        doc="Which band filter to use for all antennas",
        dtype=str,
        fisallowed="is_attribute_access_allowed",
    )
    def filter_R(self):
        return self._observation_field_settings.filter

    @attribute(
        doc="Which subbands to beamform for each beamlet.",
        dtype=(numpy.uint32,),
        max_dim_x=N_beamlets_ctrl,
        fisallowed="is_attribute_access_allowed",
    )
    def saps_subband_R(self):
        return numpy.array(
            list(
                chain(*[sap.subbands for sap in self._observation_field_settings.SAPs])
            ),
            dtype=numpy.uint32,
        )

    @attribute(
        doc="Which pointing to beamform towards for each beamlet.",
        dtype=((str,),),
        max_dim_x=N_point_prop,
        max_dim_y=N_beamlets_ctrl,
    )
    def saps_pointing_R(self):
        saps_pointing = []
        for sap in self._observation_field_settings.SAPs:
            for _ in sap.subbands:
                saps_pointing.append(
                    (
                        sap.pointing.direction_type,
                        f"{sap.pointing.angle1}rad",
                        f"{sap.pointing.angle2}rad",
                    ),
                )
        return saps_pointing

    @attribute(
        doc="Beamlet index of the FPGA output, at which to start mapping the beamlets "
        "of this observation.",
        dtype=numpy.uint64,
        fisallowed="is_attribute_access_allowed",
    )
    def first_beamlet_R(self):
        return self._observation_field_settings.first_beamlet

    @attribute(
        doc="Which pointing to beamform all HBA tiles to (if any).",
        dtype=(str,),
        max_dim_x=N_point_prop,
    )
    def HBA_tile_beam_R(self):
        try:
            if self._observation_field_settings.HBA.tile_beam is None:
                return []
        except AttributeError:
            return []

        pointing_direction = self._observation_field_settings.HBA.tile_beam
        return [
            str(pointing_direction.direction_type),
            f"{pointing_direction.angle1}rad",
            f"{pointing_direction.angle2}rad",
        ]

    @attribute(
        doc="Whether to enable the DAB filter",
        dtype=bool,
        fisallowed="is_attribute_access_allowed",
    )
    def HBA_DAB_filter_R(self):
        try:
            return self._observation_field_settings.HBA.DAB_filter or False
        except AttributeError:
            return False

    @attribute(
        doc="Which element(s) to enable in each tile",
        dtype=str,
        fisallowed="is_attribute_access_allowed",
    )
    def HBA_element_selection_R(self):
        try:
            return self._observation_field_settings.HBA.element_selection or "ALL"
        except AttributeError:
            return "ALL"

    @attribute(
        doc="",
        dtype=bool,
        fisallowed="is_attribute_access_allowed",
    )
    def SST_subbands_calibrated_R(self):
        try:
            return self._observation_field_settings.SST.subbands_calibrated
        except AttributeError:
            return True

    @attribute(
        doc="The indices of the subbands to emit every integration interval.",
        dtype=(numpy.uint32,),
        max_dim_x=MAX_PARALLEL_SUBBANDS,
    )
    def XST_subbands_R(self):
        try:
            return numpy.array(
                self._observation_field_settings.XST.subbands,
                dtype=numpy.uint32,
            )
        except AttributeError:
            return numpy.array([300], dtype=numpy.uint32)

    @attribute(
        doc="The subband indices are increased with this index every interval, causing different subbands to be emitted.",
        dtype=numpy.uint32,
    )
    def XST_subbands_step_R(self):
        try:
            return self._observation_field_settings.XST.subbands_step
        except AttributeError:
            return 0

    @attribute(
        doc="",
        dtype=numpy.float64,
        fisallowed="is_attribute_access_allowed",
    )
    def XST_integration_interval_R(self):
        try:
            return self._observation_field_settings.XST.integration_interval
        except AttributeError:
            return 1.0

    observation_field_settings_RW = attribute(
        dtype=str, access=AttrWriteType.READ_WRITE
    )

    def __init__(self, cl, name):
        self._last_time = None
        self.antennafield_proxy: Optional[DeviceProxy] = None
        self.beamlet_proxy: Optional[DeviceProxy] = None
        self.digitalbeam_proxy: Optional[DeviceProxy] = None
        self.tilebeam_proxy: Optional[DeviceProxy] = None
        self._observation_field_settings: Optional[ObservationFieldSettings] = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    def init_device(self):
        """Setup some class member variables for observation state"""
        logger.debug("[ObservationField] init device")
        super().init_device()

    def configure_for_initialise(self):
        """Load the JSON from the attribute and configure member variables"""

        if self._observation_field_settings is None:
            raise RuntimeError("Device can not be initialized without configuration")

        super().configure_for_initialise()

        logger.info(
            "Initialising observation with ID=%s with settings:%s.",
            self._observation_field_settings.observation_id,
            self._observation_field_settings.to_json(),
        )

        self._prepare_observation()

        logger.info(
            "The observation with ID=%s is initialised to run between %s and %s.",
            self._observation_field_settings.observation_id,
            self._observation_field_settings.start_time,
            self._observation_field_settings.stop_time,
        )

    def configure_for_on(self):
        """Indicate the observation has started"""

        super().configure_for_on()

        self._start_observation()

        logger.info(
            "Started the observation with ID=%s for antenna field=%s.",
            self._observation_field_settings.observation_id,
            self._observation_field_settings.antenna_field,
        )

    def configure_for_off(self):
        """Indicate the observation has stopped"""

        super().configure_for_off()

        logger.info(
            "Stopped the observation with ID=%s for antenna field=%s.",
            {
                (
                    self._observation_field_settings.observation_id
                    if self._observation_field_settings
                    else None
                )
            },
            {
                (
                    self._observation_field_settings.antenna_field
                    if self._observation_field_settings
                    else None
                )
            },
        )

    @log_exceptions()
    def _prepare_observation(self):
        """Setup proxies and other preparations that can be done ahead of
        the observation start time without changing any station
        settings."""

        # ObservationControl takes already good care of checking that the
        # parameters are in order and sufficient.  It is therefore unnecessary
        # at the moment to check the parameters here again.
        # This could change when the parameter check becomes depending on
        # certain aspects that only an Observation device can know.

        util = Util.instance()
        antennafield = self._observation_field_settings.antenna_field
        self.antennafield_proxy = create_device_proxy(
            device_member_to_full_device_name(antennafield), write_access=True
        )

        # Set a reference of Beamlet device that is correlated to this device
        self.beamlet_proxy = create_device_proxy(
            f"{util.get_ds_inst_name()}/Beamlet/{antennafield}", write_access=True
        )

        # Set a reference of DigitalBeam device that is correlated to this device
        self.digitalbeam_proxy = create_device_proxy(
            f"{util.get_ds_inst_name()}/DigitalBeam/{antennafield}", write_access=True
        )

        if self._is_HBA():
            # Set a reference of TileBeam device that is correlated to this device
            self.tilebeam_proxy = create_device_proxy(
                f"{util.get_ds_inst_name()}/TileBeam/{antennafield}", write_access=True
            )

        self.sst_proxy = create_device_proxy(
            f"{util.get_ds_inst_name()}/SST/{antennafield}", write_access=True
        )

        self.xst_proxy = create_device_proxy(
            f"{util.get_ds_inst_name()}/XST/{antennafield}", write_access=True
        )

    def _log_time(self, method: str):
        current_time = monotonic()
        logger.debug(
            "Took %.4f seconds to initialize %s", current_time - self._last_time, method
        )
        self._last_time = current_time

    @log_exceptions()
    def _start_observation(self):
        """Configure the station for this observation and antenna field."""
        self._last_time = monotonic()

        # Apply ObservationID
        self.antennafield_proxy.FPGA_sdp_info_observation_id_RW = (
            self._apply_observation_id(self.read_attribute("observation_id_R"))
        )
        self._log_time("Antennafield observation id")

        # Apply Antenna Set
        self.digitalbeam_proxy.Antenna_Set_RW = self.read_attribute("antenna_set_R")
        self._log_time("Digitalbeam antenna set")

        (frequency_band,) = self._apply_antennafield_settings(
            self.read_attribute("filter_R")
        )
        self.antennafield_proxy.Frequency_Band_RW = frequency_band
        self._log_time("Antennafield frequency band")

        # Apply dithering configuration
        if self.read_attribute("dithering_enabled_R"):
            self.antennafield_proxy.RCU_DTH_freq_RW = [
                self.read_attribute("dithering_frequency_R")
            ] * self.antennafield_proxy.nr_antennas_R

            self.antennafield_proxy.RCU_DTH_PWR_RW = [
                self.read_attribute("dithering_power_R")
            ] * self.antennafield_proxy.nr_antennas_R

            self.antennafield_proxy.RCU_DTH_on()
        else:
            self.antennafield_proxy.RCU_DTH_off()
        self._log_time("Antennafield dithering")

        # Apply Beamlet configuration
        self.beamlet_proxy.subband_select_RW = self._apply_saps_subbands(
            self.read_attribute("saps_subband_R")
        )
        self._log_time("Beamlet subband saps")
        self.digitalbeam_proxy.Pointing_direction_RW = self._apply_saps_pointing(
            self.read_attribute("saps_pointing_R")
        )
        self._log_time("Digitalbeam saps pointing")

        if self._is_HBA():
            # Apply Tile Beam pointing direction
            # NB: These settings are applied to all tiles, not just the ones
            #     in the selected antenna set.
            tile_beam = self.read_attribute("HBA_tile_beam_R")

            self.tilebeam_proxy.Pointing_direction_RW = [
                tuple(tile_beam)
            ] * self.antennafield_proxy.nr_antennas_R
            self._log_time("Tilebeam pointing")

            # Apply DAB filter setting
            DAB_filter = self.read_attribute("HBA_DAB_filter_R")

            self.antennafield_proxy.RCU_DAB_filter_on_RW = [
                DAB_filter
            ] * self.antennafield_proxy.nr_antennas_R
            self._log_time("Antennafield DAB filter")

            # Apply HBAT element selection
            element_selection = self.read_attribute("HBA_element_selection_R")

            self.antennafield_proxy.HBAT_PWR_LNA_on_RW = self._apply_element_selection(
                element_selection
            )
            self.antennafield_proxy.HBAT_PWR_on_RW = self._apply_element_selection(
                element_selection
            )
            self._log_time("Antennafield element selection")

        # Configure SST
        subbands_calibrated = self.read_attribute("SST_subbands_calibrated_R")
        self.sst_proxy.FPGA_sst_offload_weighted_subbands_RW = [
            subbands_calibrated
        ] * N_pn

        # Toggle statistics to activate new settings
        self.sst_proxy.power_hardware_off()
        self.sst_proxy.power_hardware_on()
        self._log_time("SST subband calibration")

        # Configure XST
        subbands = self.read_attribute("XST_subbands_R")
        subbands_step = self.read_attribute("XST_subbands_step_R")
        integration_interval = self.read_attribute("XST_integration_interval_R")

        self.xst_proxy.FPGA_xst_subband_select_RW = self._apply_xst_subband_select(
            subbands.tolist(),
            subbands_step,
        )
        self.xst_proxy.FPGA_xst_integration_interval_RW = [integration_interval] * N_pn
        self.xst_proxy.FPGA_xst_offload_nof_crosslets_RW = [len(subbands)] * N_pn

        # Toggle statistics to activate new settings
        self.xst_proxy.power_hardware_off()
        self.xst_proxy.power_hardware_on()
        self._log_time("XST subbands and integration interval")

    @fault_on_error()
    @log_exceptions()
    def read_observation_field_settings_RW(self):
        """Return current observation_parameters string"""
        return (
            None
            if self._observation_field_settings is None
            else self._observation_field_settings.to_json()
        )

    @only_in_states([DevState.OFF])
    @fault_on_error()
    @log_exceptions()
    def write_observation_field_settings_RW(self, parameters: str):
        """No validation on configuring parameters as task of control device"""
        try:
            self._observation_field_settings = ObservationFieldSettings.from_json(
                parameters
            )
        except ValidationError as ex:
            self._observation_field_settings = None
            raise ex

    def _is_HBA(self):
        """Return whether this observation should control a TileBeam device."""
        return self._observation_field_settings.antenna_field.startswith("HBA")

    def _apply_xst_subband_select(
        self, subbands: List[int], subbands_step: int
    ) -> numpy.ndarray:

        if len(subbands) > MAX_PARALLEL_SUBBANDS:
            raise ValueError(
                f"Requested more than {MAX_PARALLEL_SUBBANDS} subbands for the XSTs: {subbands}"
            )

        return numpy.array(
            [[subbands_step] + subbands + [0] * (MAX_PARALLEL_SUBBANDS - len(subbands))]
            * N_pn,
            dtype=numpy.uint32,
        )

    def _apply_antennafield_settings(self, filter_name: str):
        """Retrieve the RCU band from filter name, returning the correct format for
        AntennaField device
        """

        nr_antennas = self.antennafield_proxy.nr_antennas_R
        return (numpy.array([[filter_name] * N_pol] * nr_antennas),)

    def _apply_saps_subbands(self, sap_subbands: list):
        """Convert an array of subbands into the correct format for Beamlet device"""
        subband_select = self.beamlet_proxy.subband_select_RW
        first_beamlet = self.read_attribute("first_beamlet_R")

        # Insert subband values starting from the first beamlet
        subband_select[first_beamlet : first_beamlet + len(sap_subbands)] = sap_subbands

        return subband_select

    def _apply_saps_pointing(self, sap_pointing: list):
        """Convert an array of string directions into the correct format for DigitalBeam device"""
        pointing_direction = list(self.digitalbeam_proxy.Pointing_direction_RW)
        first_beamlet = self.read_attribute("first_beamlet_R")

        # Insert pointing values starting from the first beamlet
        pointing_direction[first_beamlet : first_beamlet + len(sap_pointing)] = (
            sap_pointing
        )

        return tuple(pointing_direction)

    def _apply_observation_id(self, observation_id: numpy.int64):
        """Convert the observation id value into the correct format for Antennafield device"""
        return numpy.array([observation_id] * MAX_ANTENNA, dtype=numpy.uint32)

    def _apply_element_selection(self, element_selection: str):
        """Convert the element selection strategy to a boolean array
        per element per polarisation per tile."""

        nr_antennas = self.antennafield_proxy.nr_antennas_R

        selection = numpy.zeros((nr_antennas, N_elements, N_pol), dtype=bool)

        if element_selection == "ALL":
            selection[:, :, :] = True
        elif element_selection == "FIRST":
            selection[:, 0, :] = True
        elif element_selection == "GENERIC_201512":
            # select a specific element for each tile
            element_per_tile = (
                self.antennafield_proxy.HBAT_single_element_selection_GENERIC_201512_R
            )
            for antenna, element in enumerate(element_per_tile):
                selection[antenna, element, :] = True
        else:
            raise ValueError(f"Unsupported element selection: {element_selection}")

        return selection.reshape(nr_antennas, N_elements * N_pol)
