#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""EC (Environmental Control) Device Server for LOFAR2.0"""

import logging

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType

# PyTango imports
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.metrics import device_metrics

# Additional import

logger = logging.getLogger()

__all__ = ["EC"]


@device_logging_to_python()
@device_metrics()
class EC(OPCUADevice):
    """EC Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    Temperature_Cab0_0_R = AttributeWrapper(
        comms_annotation=["Temperature_Cab0.0_R"], datatype=numpy.float64
    )
    Temperature_Cab0_1_R = AttributeWrapper(
        comms_annotation=["Temperature_Cab0.1_R"], datatype=numpy.float64
    )
    Temperature_Cab1_0_R = AttributeWrapper(
        comms_annotation=["Temperature_Cab1.0_R"], datatype=numpy.float64
    )
    Temperature_Cab1_1_R = AttributeWrapper(
        comms_annotation=["Temperature_Cab1.1_R"], datatype=numpy.float64
    )
    Door_Closed_Cab0_0_R = AttributeWrapper(
        comms_annotation=["Door_Closed_Cab0.0_R"], datatype=bool
    )
    Door_Closed_Cab0_1_R = AttributeWrapper(
        comms_annotation=["Door_Closed_Cab0.1_R"], datatype=bool
    )
    Door_Closed_Cab1_0_R = AttributeWrapper(
        comms_annotation=["Door_Closed_Cab1.0_R"], datatype=bool
    )
    Control_Relais_48V_RW = AttributeWrapper(
        comms_annotation=["Control_Relais_48V_RW"],
        datatype=bool,
        access=AttrWriteType.READ_WRITE,
    )
    Control_Relais_Heater_RW = AttributeWrapper(
        comms_annotation=["Control_Relais_Heater_RW"],
        datatype=bool,
        access=AttrWriteType.READ_WRITE,
    )
    Control_Relais_PSOC_RW = AttributeWrapper(
        comms_annotation=["Control_Relais_PSOC_RW"],
        datatype=bool,
        access=AttrWriteType.READ_WRITE,
    )
    Trigger_Shut_Off_48V_R = AttributeWrapper(
        comms_annotation=["Trigger_Shut_Off_48V_R"], datatype=bool
    )
    Trigger_Shut_Off_PSOC_R = AttributeWrapper(
        comms_annotation=["Trigger_Shut_Off_PSOC_R"], datatype=bool
    )
    Setpoint_Temperature_PID_RW = AttributeWrapper(
        comms_annotation=["Setpoint_Temperature_PID_RW"],
        datatype=numpy.float64,
        access=AttrWriteType.READ_WRITE,
    )
    Fan_Speed_Door_Closed_RW = AttributeWrapper(
        comms_annotation=["Fan_Speed_Door_Open_RW"], datatype=numpy.float64
    )
    First_Trip_Temperature_RW = AttributeWrapper(
        comms_annotation=["First_Trip_Temperature_RW"],
        datatype=numpy.float64,
        access=AttrWriteType.READ_WRITE,
    )
    Second_Trip_Temperature_RW = AttributeWrapper(
        comms_annotation=["Second_Trip_Temperature_RW"],
        datatype=numpy.float64,
        access=AttrWriteType.READ_WRITE,
    )
    InnerFans_Cab0_RPM_RW = AttributeWrapper(
        comms_annotation=["InnerFans_Cab0_RPM_RW"],
        datatype=numpy.float64,
        access=AttrWriteType.READ_WRITE,
    )
    InnerFans_Cab0_RPM_R = AttributeWrapper(
        comms_annotation=["InnerFans_Cab0_RPM_R"], datatype=numpy.float64
    )

    # ----------
    # Summarising Attributes
    # ----------

    # --------
    # overloaded functions
    # --------

    def _read_hardware_powered_fraction_R(self):
        # the device and translator are one, so if
        # the translator is reachable, the hardware
        # is powered.
        return 1.0 * self.read_attribute("connected_R")

    # --------
    # Commands
    # --------
