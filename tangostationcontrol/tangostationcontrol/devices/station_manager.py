#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""StationManager Device Server for LOFAR2.0"""
import asyncio
import logging

import numpy
from tango import AttrWriteType
from tango.server import attribute, command, device_property

# Additional import
from tangostationcontrol.common.device_decorators import debugit, log_exceptions
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.common.lofar_logging import exception_to_str

from tangostationcontrol.states.station_state_enum import StationStateEnum
from tangostationcontrol.devices.base_device_classes.power_hierarchy import (
    power_hierarchy_control,
)
from tangostationcontrol.devices.base_device_classes.async_device import AsyncDevice
from tangostationcontrol.metrics import device_metrics
from tangostationcontrol.states.off import OffState

logger = logging.getLogger()

__all__ = ["StationManager"]


@power_hierarchy_control
@device_logging_to_python()
@device_metrics(
    exclude=[
        "last_requested_transition_exceptions_R",
    ]
)
class StationManager(AsyncDevice):
    """StationManager Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    Station_Name = device_property(
        doc="Name of the station, f.e. CS001",
        dtype="DevString",
        mandatory=False,
        default_value="DevStation",
    )
    Station_Number = device_property(
        doc="Number of the station, f.e. CS001 has number 1",
        dtype="DevLong64",
        mandatory=False,
        default_value=999,
    )
    Suppress_State_Transition_Failures = device_property(
        doc="Allow state transitions to continue even if a device fails to initialise.",
        dtype="DevBoolean",
        mandatory=False,
        default_value=True,
    )
    Hibernate_Transition_Timeout = device_property(
        doc="Timeout in seconds to transition to hibernate",
        dtype="DevLong64",
        mandatory=False,
        default_value=60,
    )
    Standy_Transition_Timeout = device_property(
        doc="Timeout in seconds to transition to standby",
        dtype="DevLong64",
        mandatory=False,
        default_value=300,
    )
    On_Transition_Timeout = device_property(
        doc="Timeout in seconds to transition to on",
        dtype="DevLong64",
        mandatory=False,
        default_value=600,
    )

    # ----------
    # Attributes
    # ----------

    @attribute(dtype=str, fisallowed="is_attribute_access_allowed")
    def station_name_R(self):
        return self.Station_Name

    @attribute(dtype=StationStateEnum, fisallowed="is_attribute_access_allowed")
    def station_state_R(self):
        return self.station_state_name

    @attribute(dtype=StationStateEnum, fisallowed="is_attribute_access_allowed")
    def requested_station_state_R(self):
        return self.requested_station_state

    @attribute(dtype=bool, fisallowed="is_attribute_access_allowed")
    def station_state_transitioning_R(self):
        return self.transition_lock and self.transition_lock.locked()

    @attribute(dtype=(str,), max_dim_x=1024, fisallowed="is_attribute_access_allowed")
    def last_requested_transition_exceptions_R(self):
        return [
            f"{device}: {exception_to_str(ex)}"
            for (device, ex) in self.last_requested_transition_exceptions
        ][:1024]

    @attribute(dtype=bool, fisallowed="is_attribute_access_allowed")
    def last_requested_transition_ok_R(self):
        return not self.last_requested_transition_exceptions

    @attribute(
        access=AttrWriteType.READ_WRITE,
        dtype=numpy.int64,
    )
    def hibernate_transition_timeout_RW(self):
        return self._hibernate_transition_timeout

    @hibernate_transition_timeout_RW.write
    def hibernate_transition_timeout_RW(self, value):
        self._hibernate_transition_timeout = value

    @attribute(
        access=AttrWriteType.READ_WRITE,
        dtype=numpy.int64,
        min_value="0",
    )
    def standby_transition_timeout_RW(self):
        return self._standby_transition_timeout

    @standby_transition_timeout_RW.write
    def standby_transition_timeout_RW(self, value):
        self._standby_transition_timeout = value

    @attribute(
        access=AttrWriteType.READ_WRITE,
        dtype=numpy.int64,
        min_value="0",
    )
    def on_transition_timeout_RW(self):
        return self._on_transition_timeout

    @on_transition_timeout_RW.write
    def on_transition_timeout_RW(self, value):
        self._on_transition_timeout = value

    @attribute(
        access=AttrWriteType.READ_WRITE,
        dtype=bool,
    )
    def protection_lock_RW(self):
        return self._protection_lock

    @protection_lock_RW.write
    def protection_lock_RW(self, value):
        self._protection_lock = value

    # --------
    # overloaded functions
    # --------
    def __init__(self, cl, name):
        self.init_station_state = OffState(self, self)
        self.set_station_state(self.init_station_state)
        self.requested_station_state = self.init_station_state.state
        self.last_requested_transition_exceptions = []
        self.transition_lock = asyncio.Lock()

        self._hibernate_transition_timeout = 0
        self._standby_transition_timeout = 0
        self._on_transition_timeout = 0

        self._protection_lock = False

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    def _init_device(self):
        super()._init_device()
        # always turn on automatically, so the user doesn't have to boot the
        # StationManager device.

        self._hibernate_transition_timeout = self.Hibernate_Transition_Timeout
        self._standby_transition_timeout = self.Standy_Transition_Timeout
        self._on_transition_timeout = self.On_Transition_Timeout

        """Alternative method"""
        # loop = asyncio.get_running_loop()
        # loop.create_task(self.Initialise())
        # loop.create_task(self.On())

        """Works"""
        self._Initialise()
        self._On()

    def configure_for_initialise(self):
        super().configure_for_initialise()

        # Create the state transition lock
        if self.transition_lock is None:
            self.transition_lock = asyncio.Lock()

        # Initialise power hierarchy based on current settings
        self._initialise_power_hierarchy()

        # Set the station state to off
        self.set_station_state(OffState(self, self))

    # --------
    # internal functions
    # --------

    def set_station_state(self, station_state):
        """Change the Station state"""
        self.station_state = station_state
        self.station_state_name = StationStateEnum[station_state.state.name]

    def _initialise_power_hierarchy(self):
        """Create and initialise the PowerHierarchy to manage the power sequence"""
        # create a power hierarchy device instance
        self.init(
            self.get_name(), continue_on_failure=self.Suppress_State_Transition_Failures
        )

    def _check_protective_lock(
        self, current_state: StationStateEnum, requested_state: StationStateEnum
    ):
        """Check against the protective lock being engaged"""
        if self._protection_lock and requested_state >= current_state:
            raise RuntimeError(
                "Station transitions locked by protective mechanisms. Can not "
                f"transition from {current_state} to {requested_state}. If you are "
                "absolutely certain the protection (against overvoltage, overcurrent, "
                "overtemperature) can be lifted you can write "
                "`stationmanager.protection_lock_RW = False` please note that if the "
                "protective conditions persist this lock will be reengaged by "
                "ProtectionControl at any time! Unless, protectioncontrol is "
                "explicitly turned off `protectioncontrol.Off()`. No further "
                "software protection mechanisms exist if protectioncontrol is "
                "turned off!!!"
            )
        elif self._protection_lock:
            logger.info(
                "Protective lock engaged but transitioning to protective state from %s"
                "to %s, allowing",
                current_state,
                requested_state,
            )

    # --------
    # Commands
    # --------

    @command()
    @debugit()
    @log_exceptions()
    async def station_off(self):
        """
        Switch the station into OFF state.
        It can only be executed from state HIBERNATE.
        """
        async with self.transition_lock:
            # Off is always allowed
            await self.station_state.station_off(0)

    @command()
    @debugit()
    @log_exceptions()
    async def station_hibernate(self):
        """
        Switch the station into HIBERNATE state.
        It can only be executed from either state OFF or STANDBY.
        """
        async with self.transition_lock:
            self._check_protective_lock(
                self.station_state_name, StationStateEnum.HIBERNATE
            )
            await self.station_state.station_hibernate(
                timeout=self._hibernate_transition_timeout
            )

    @command()
    @debugit()
    @log_exceptions()
    async def station_standby(self):
        """
        Switch the station into STANDBY state.
        It can only be executed from either state HIBERNATE or ON.
        """
        async with self.transition_lock:
            self._check_protective_lock(
                self.station_state_name, StationStateEnum.STANDBY
            )
            await self.station_state.station_standby(
                timeout=self._standby_transition_timeout
            )

    @command()
    @debugit()
    @log_exceptions()
    async def station_on(self):
        """
        Switch the station into ON state.
        It can only be executed from state STANDBY.
        """
        async with self.transition_lock:
            self._check_protective_lock(self.station_state_name, StationStateEnum.ON)
            await self.station_state.station_on(timeout=self._on_transition_timeout)
