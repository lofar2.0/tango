#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""APS Device Server for LOFAR2.0"""

# PyTango imports
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.metrics import device_metrics

# Additional import

__all__ = ["APS"]


@device_logging_to_python()
@device_metrics()
class APS(LOFARDevice):
    pass
    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    # --------
    # overloaded functions
    # --------

    # --------
    # Commands
    # --------
