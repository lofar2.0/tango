#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Calibration Device Server for LOFAR2.0"""
import logging

from prometheus_client import Counter
from tango import Database
from tango.server import device_property, command, attribute
from tangostationcontrol.common.calibration import (
    CalibrationManager,
    calibrate_RCU_attenuator_dB,
    calibrate_input_samples_delay,
)
from lofar_station_client.common import (
    CaseInsensitiveDict,
    CaseInsensitiveString,
)
from tangostationcontrol.common.device_decorators import only_in_states, debugit
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.common.type_checking import device_name_matches
from tangostationcontrol.devices.antennafield.afh import AFH
from tangostationcontrol.devices.antennafield.afl import AFL
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.devices.sdp.firmware import SDPFirmware
from tangostationcontrol.devices.sdp.sdp import SDP
from tangostationcontrol.metrics import device_metrics, AttributeMetric, device_labels

logger = logging.getLogger()
__all__ = ["Calibration"]


@device_logging_to_python()
@device_metrics()
class Calibration(LOFARDevice):
    """Manages the calibration of antenna field, SDP and RECV devices."""

    def __init__(self, cl, name):
        self._calibration_manager: CalibrationManager = None
        self.sdpfirmware_proxies: CaseInsensitiveDict = CaseInsensitiveDict()
        self.sdp_proxies: CaseInsensitiveDict = CaseInsensitiveDict()
        self.hba_proxies: CaseInsensitiveDict = CaseInsensitiveDict()
        self.lba_proxies: CaseInsensitiveDict = CaseInsensitiveDict()
        self.ant_proxies: CaseInsensitiveDict = CaseInsensitiveDict()

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

        self.calibration_count_metric = AttributeMetric(
            "calibration_count",
            "Number of times calibration has been triggered for each AntennaField device",
            device_labels(self),
            Counter,
            dynamic_labels=["antennafield"],
        )

    def _calibrate_antenna_field(self, device: str):
        """Recalibrate a specific AntennaField."""

        ant_proxy = self.ant_proxies[device]

        if ant_proxy.state() not in DEFAULT_COMMAND_STATES:
            logger.warning(f"Device {device} not active. Forgoing calibration.")
            return

        logger.info("Re-calibrate antenna field %s", device)

        self.calibrate_recv(device)
        self.calibrate_sdp(device)

        # get device member in its original casing
        antenna_field_name = device.split("/")[2]

        self.calibration_count_metric.get_metric([antenna_field_name]).inc()

    def _antennafield_changed_event(self, device, attribute_name, value):
        """Trigger on key external changes in AntennaField settings."""

        if self.dev_state() not in DEFAULT_COMMAND_STATES:
            logger.warning("Device not active. Ignore AntennaField changed event")
            return

        # make sure we have the latest tables
        logger.debug("Syncing calibration tables")
        self._calibration_manager.sync_calibration_tables()

        # frequencies changed, so we need to recalibrate
        self._calibrate_antenna_field(device.name())

    def _clock_changed_event(self, device, attribute_name, value):
        """Trigger on external changes in frequency settings."""

        if self.dev_state() not in DEFAULT_COMMAND_STATES:
            logger.warning("Device not active. Ignore clock changed event")
            return

        # make sure we have the latest tables
        logger.debug("Syncing calibration tables")
        self._calibration_manager.sync_calibration_tables()

        found = False
        for k, ant in self.ant_proxies.items():
            # Recalibrate associated AntennaField
            sdpfirmware_device = ant.SDPFirmware_device_R

            if device_name_matches(sdpfirmware_device, device.name()):
                self._calibrate_antenna_field(k)
                found = True
                break

        if not found:
            logger.warning(
                f"Could not find any AntennaField to calibrate for clock change event from {device}"
            )

    Calibration_Table_Base_URL = device_property(
        doc="Base URL of the calibration tables",
        dtype="DevString",
        mandatory=False,
        update_db=True,
        default_value="http://s3/caltables",
    )

    @attribute(dtype=(str,), max_dim_x=20)
    def AntennaFields_Monitored_R(self):
        return list(self.ant_proxies.keys())

    @attribute(dtype=(str,), max_dim_x=20)
    def SDPs_Monitored_R(self):
        return list(self.sdp_proxies.keys())

    @debugit()
    @command()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def download_calibration_tables(self):
        """Download the latest calibration tables and apply them."""

        self._calibration_manager.sync_calibration_tables()

        # Apply downloaded tables
        self.calibrate_all()

    @debugit()
    @command(dtype_in=str)
    @only_in_states(DEFAULT_COMMAND_STATES)
    def calibrate_recv(self, device: str):
        """Calibrate RECV for our antennas.

        Run whenever the following changes:
            sdpfirmware.clock_RW
            antennafield.RCU_band_select_RW
        """

        # -----------------------------------------------------------
        #   Set signal-input attenuation to compensate for
        #   differences in cable length.
        # -----------------------------------------------------------

        if device not in self.ant_proxies:
            logger.error("Could not find %s", device)
            return

        calibrate_RCU_attenuator_dB(self.ant_proxies[device])

    @debugit()
    @command(dtype_in=str)
    @only_in_states(DEFAULT_COMMAND_STATES)
    def calibrate_sdp(self, device: str):
        """Calibrate SDP for our antennas.

        Run whenever the following changes:
            sdpfirmware.clock_RW
            sdpfirmware.FPGA_boot_image_R
            antennafield.RCU_band_select_RW
        """

        if device not in self.ant_proxies:
            raise ValueError(f"Could not find {device} in {self.ant_proxies}")

        ant_proxy = self.ant_proxies[device]
        sdpfirmware_device = ant_proxy.SDPFirmware_device_R

        if sdpfirmware_device not in self.sdpfirmware_proxies:
            raise ValueError(
                f"Could not find {sdpfirmware_device} in {self.sdpfirmware_proxies}"
            )

        sdpfirmware_proxy = self.sdpfirmware_proxies[sdpfirmware_device]

        sdp_device = sdpfirmware_proxy.SDP_device_R

        if sdp_device not in self.sdp_proxies:
            raise ValueError(f"Could not find {sdp_device} in {self.sdp_proxies}")

        sdp_proxy = self.sdp_proxies[sdp_device]

        calibrate_input_samples_delay(ant_proxy, sdpfirmware_proxy, sdp_proxy)

        self._calibration_manager.calibrate_subband_weights(ant_proxy, sdp_proxy)

    @command()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def calibrate_all(self):
        for k in self.ant_proxies.keys():
            logger.info("Re-calibrate antenna field %s", k)

            try:
                self._calibrate_antenna_field(k)
            except Exception as exc:
                logger.exception("Could not calibrate antenna field %s", k)

    # --------
    # Overloaded functions
    # --------

    def configure_for_initialise(self):
        super().configure_for_initialise()

        station_name = self.control.read_parent_attribute("station_name_R")

        self._calibration_manager = CalibrationManager(
            self.Calibration_Table_Base_URL, station_name
        )

        db = Database()
        devices = db.get_device_exported_for_class(AFH.__name__)
        self.hba_proxies = {
            CaseInsensitiveString(d): create_device_proxy(d, write_access=True)
            for d in devices
        }
        for d in devices:
            logger.debug("found HBA antenna field device %s", str(d))

        devices = db.get_device_exported_for_class(AFL.__name__)
        self.lba_proxies = {
            CaseInsensitiveString(d): create_device_proxy(d, write_access=True)
            for d in devices
        }
        for d in devices:
            logger.debug("found LBA antenna field device %s", str(d))

        self.ant_proxies = CaseInsensitiveDict({**self.hba_proxies, **self.lba_proxies})

        devices = db.get_device_exported_for_class(SDPFirmware.__name__)
        for d in devices:
            logger.debug("Found SDP firmware device %s", str(d))

            self.sdpfirmware_proxies[d] = create_device_proxy(d)

        devices = db.get_device_exported_for_class(SDP.__name__)
        for d in devices:
            logger.debug("Found SDP device %s", (d))

            self.sdp_proxies[d] = create_device_proxy(d, write_access=True)

        # subscribe to events to notice setting changes in SDP that determine the
        # input frequency
        for prx in self.ant_proxies.values():
            self.events.subscribe_change_event(
                prx, "Frequency_Band_RW", self._antennafield_changed_event
            )
            self.events.subscribe_change_event(
                prx, "State", self._antennafield_changed_event
            )

        for prx in self.sdpfirmware_proxies.values():
            self.events.subscribe_change_event(
                prx, "clock_RW", self._clock_changed_event
            )
            self.events.subscribe_change_event(
                prx, "FPGA_boot_image_R", self._clock_changed_event
            )

    def configure_for_on(self):
        # (Re)calibrate all antennafields, as we did not receive
        # any events yet.
        self.download_calibration_tables()

        super().configure_for_on()
