#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""APSPU Device Server for LOFAR2.0"""

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper

# PyTango imports
from tango import AttrWriteType
from tango.server import attribute, device_property

from tangostationcontrol.common.constants import N_apspu
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.metrics import device_metrics

# Additional import

__all__ = ["APSPU"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "APSPU_*_error_R",  # too expensive as they read a lot from hardware
        "*_RW",
    ]
)
class APSPU(OPCUADevice):
    # -----------------
    # Device Properties
    # -----------------

    APSPUTR_monitor_rate_RW_default = device_property(
        dtype="DevLong64", mandatory=False, default_value=1
    )

    TRANSLATOR_DEFAULT_SETTINGS = ["APSPUTR_monitor_rate_RW"]

    # ----------
    # Attributes
    # ----------

    TR_software_version_R = AttributeWrapper(
        comms_annotation=["TR_software_version_R"], datatype=str
    )
    APSPUTR_I2C_error_R = AttributeWrapper(
        comms_annotation=["APSPUTR_I2C_error_R"],
        datatype=numpy.int64,
        dims=(N_apspu,),
    )
    APSPUTR_monitor_rate_RW = AttributeWrapper(
        comms_annotation=["APSPUTR_monitor_rate_RW"],
        datatype=numpy.int64,
        access=AttrWriteType.READ_WRITE,
    )
    APSPUTR_translator_busy_R = AttributeWrapper(
        comms_annotation=["APSPUTR_translator_busy_R"], datatype=bool
    )
    APSPU_FAN1_RPM_R = AttributeWrapper(
        comms_annotation=["APSPU_FAN1_RPM_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_FAN2_RPM_R = AttributeWrapper(
        comms_annotation=["APSPU_FAN2_RPM_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_FAN3_RPM_R = AttributeWrapper(
        comms_annotation=["APSPU_FAN3_RPM_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_LBA_IOUT_R = AttributeWrapper(
        comms_annotation=["APSPU_LBA_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_LBA_TEMP_R = AttributeWrapper(
        comms_annotation=["APSPU_LBA_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_LBA_VOUT_R = AttributeWrapper(
        comms_annotation=["APSPU_LBA_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_PCB_ID_R = AttributeWrapper(
        comms_annotation=["APSPU_PCB_ID_R"],
        datatype=numpy.int64,
        dims=(N_apspu,),
    )
    APSPU_PCB_number_R = AttributeWrapper(
        comms_annotation=["APSPU_PCB_number_R"],
        datatype=str,
        dims=(N_apspu,),
    )
    APSPU_PCB_version_R = AttributeWrapper(
        comms_annotation=["APSPU_PCB_version_R"],
        datatype=str,
        dims=(N_apspu,),
    )
    APSPU_RCU2A_IOUT_R = AttributeWrapper(
        comms_annotation=["APSPU_RCU2A_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_RCU2A_TEMP_R = AttributeWrapper(
        comms_annotation=["APSPU_RCU2A_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_RCU2A_VOUT_R = AttributeWrapper(
        comms_annotation=["APSPU_RCU2A_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_RCU2D_IOUT_R = AttributeWrapper(
        comms_annotation=["APSPU_RCU2D_IOUT_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_RCU2D_TEMP_R = AttributeWrapper(
        comms_annotation=["APSPU_RCU2D_TEMP_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )
    APSPU_RCU2D_VOUT_R = AttributeWrapper(
        comms_annotation=["APSPU_RCU2D_VOUT_R"],
        datatype=numpy.float64,
        dims=(N_apspu,),
    )

    # ----------
    # Summarising Attributes
    # ----------

    @attribute(dtype=bool, fisallowed="is_attribute_access_allowed")
    def APSPU_error_R(self):
        return (
            (self.read_attribute("APSPUTR_I2C_error_R") > 0).any()
            or self.alarm_val("APSPU_PCB_ID_R").any()
            or self.alarm_val("APSPU_FAN1_RPM_R").any()
            or self.alarm_val("APSPU_FAN2_RPM_R").any()
            or self.alarm_val("APSPU_FAN3_RPM_R").any()
        )

    APSPU_IOUT_error_R = attribute(dtype=bool, fisallowed="is_attribute_access_allowed")
    APSPU_TEMP_error_R = attribute(
        dtype=bool,
        fisallowed="is_attribute_access_allowed",
    )
    APSPU_VOUT_error_R = attribute(dtype=bool, fisallowed="is_attribute_access_allowed")

    def read_APSPU_IOUT_error_R(self):
        return (
            self.alarm_val("APSPU_LBA_IOUT_R").any()
            or self.alarm_val("APSPU_RCU2A_IOUT_R").any()
            or self.alarm_val("APSPU_RCU2D_IOUT_R").any()
        )

    def read_APSPU_TEMP_error_R(self):
        return (
            self.alarm_val("APSPU_LBA_TEMP_R").any()
            or self.alarm_val("APSPU_RCU2A_TEMP_R").any()
            or self.alarm_val("APSPU_RCU2D_TEMP_R").any()
        )

    def read_APSPU_VOUT_error_R(self):
        return (
            self.alarm_val("APSPU_LBA_VOUT_R").any()
            or self.alarm_val("APSPU_RCU2A_VOUT_R").any()
            or self.alarm_val("APSPU_RCU2D_VOUT_R").any()
        )

    # --------
    # overloaded functions
    # --------

    # --------
    # Commands
    # --------
