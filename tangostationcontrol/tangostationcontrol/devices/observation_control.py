#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging

import numpy
from tango import (
    AutoTangoAllowThreads,
    Util,
    DevBoolean,
    DevString,
)
from tango.server import command, attribute, device_property
from tangostationcontrol.observation.observation_controller import ObservationController
from tangostationcontrol.asyncio import PeriodicTask
from tangostationcontrol.common.device_decorators import DurationMetric, log_exceptions
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)
from tangostationcontrol.configuration import ObservationSettings
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.devices.observation_field import ObservationField
from tangostationcontrol.devices.types import DeviceTypes
from tangostationcontrol.common.device_decorators import only_when_on, debugit
from tangostationcontrol.metrics import device_metrics


logger = logging.getLogger()

__all__ = ["ObservationControl"]


@device_logging_to_python()
@device_metrics()
class ObservationControl(LOFARDevice):
    """Observation Control Device Server for LOFAR2.0
    The ObservationControl Tango device controls the instantiation of a Tango Dynamic
    Device from the ObservationField class.
    ObservationControl then keeps a record of the ObservationField devices and if they
    are still alive.

    At the end of an observation ObservationControl checks if the respective
    ObservationField devices have stopped its execution and releases it. If the
    ObservationField devices have not stopped its execution yet, it is attempted to
    forcefully stop the execution of them.

    The ObservationField devices are responsible for the "real" execution of an
    observation. They get references to the hardware devices that are needed to set
    values in the relevant Control Points. The ObservationField device performs only a
    check if enough parameters are available to perform the set-up.

    Essentially this is what happens:
    Somebody calls ObservationControl.add_observation(parameters).
    Then ObservationControl will perform:
    - Creates a new instances of an ObservationField devices in the Tango DB
      (one per antenna field)
    - Call Initialise(parameters)
    - Wait for initialise to return
    - Check status()
    - If status() is NOT STANDBY, abort with an exception
    - Wait for start_time - lead_time
    - Call On()
    - Register the observation in the dict self.running_observations[ID]
    - The Observation updates the MP every second with the current time
    - The update callback gets called periodically.
      It checks if MP value > stop (stored in the dict under the obs IDS.
      By this it can determine if the observation is done.
    - if MP value > observation end
        - Remove observation ID from running obs dict
        - Unsubscribe from the MP's event
        - Call off()
        - Remove the device from the Tango DB which will make the device disappear

    MPs
    - string version
    """

    # -----------------
    # Device Properties
    # -----------------

    Observation_Update_Interval = device_property(
        dtype="DevFloat",
        doc="Interval with which check for updates for observation states.",
        mandatory=False,
        default_value=1.0,
    )

    # Attributes
    @attribute(
        doc="List of registered observations.",
        dtype=(numpy.int64,),
        max_dim_x=1000,
        fisallowed="is_attribute_access_allowed",
    )
    def observations_R(self):
        return list(self._observation_controller)

    @attribute(
        doc="List of registered observations that are running.",
        dtype=(numpy.int64,),
        max_dim_x=1000,
        fisallowed="is_attribute_access_allowed",
    )
    def running_observations_R(self):
        return self._observation_controller.running_observations

    @attribute(
        doc="List of active antenna fields.",
        dtype=(str,),
        max_dim_x=1000,
        fisallowed="is_attribute_access_allowed",
    )
    def active_antenna_fields_R(self):
        return self._observation_controller.active_antenna_fields

    observation_update_thread_running_R = attribute(
        doc="Whether the observation states are being updated.",
        dtype=bool,
        fget=lambda self: (
            self.event_loop_thread
            and self.event_loop_thread.is_running()
            and self.observation_update_task
            and self.observation_update_task.is_running()
        ),
    )

    def __init__(self, cl, name):
        self.observation_update_task = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

        # The top level tango domain is the left-most part of a
        # device's name.
        self.metadata = None
        self.myTangoDomain: str = self.get_name().split("/")[0]

        self._observation_controller: ObservationController = ObservationController(
            tango_domain=self.myTangoDomain,
            start_callback=self._observation_callback,
            stop_callback=self._observation_callback,
        )

        self._stop_all_observations_now()

    def _observation_callback(self, observation_id: int):
        """Callback for when an observation is started, use to emit metadata"""

        try:
            logger.info("Triggering metadata update")

            # trigger asynchronous call, we do not care about the result,
            # since we cannot afford to wait delaying the observation setup
            self.metadata.command_inout_asynch("send_metadata", None, True)
        except Exception as e:
            logger.exception(
                f"Observation callback for observation {observation_id} "
                f"failed due to {e}"
            )

    # Core functions
    async def _update_observations(self):
        with AutoTangoAllowThreads(self):
            self._observation_controller.update_all_observations()

    def configure_for_initialise(self):
        super().configure_for_initialise()

        self.metadata = self.control.child(DeviceTypes.Metadata)

        self.observation_update_task = PeriodicTask(
            self.event_loop_thread.event_loop,
            self._update_observations,
            self.Observation_Update_Interval,
        )

    # Lifecycle functions
    def configure_for_off(self):
        super().configure_for_off()

        self._stop_all_observations_now()

    @command(dtype_in=DevString)
    @only_when_on()
    @debugit()
    @log_exceptions()
    @DurationMetric()
    def add_observation(self, parameters: DevString = None):
        """Add an observation. Start/stop it at the specified start/stop times."""

        with AutoTangoAllowThreads(self):
            self._observation_controller.add_observation(
                ObservationSettings.from_json(parameters)
            )

    @command(dtype_in=numpy.int64)
    @only_when_on()
    @debugit()
    @log_exceptions()
    def start_observation_now(self, obs_id: numpy.int64 = 0):
        """Force an observation to start now."""
        logger.info("Force starting the observation with ID=%s.", obs_id)

        with AutoTangoAllowThreads(self):
            self._observation_controller.start_observation(obs_id)

    @command(dtype_in=numpy.int64)
    @only_when_on()
    @log_exceptions()
    def stop_observation_now(self, obs_id: numpy.int64):
        """Force an observation to stop now."""
        logger.info("Force stopping the observation with ID=%s.", obs_id)

        with AutoTangoAllowThreads(self):
            self._observation_controller.stop_observation_now(obs_id)

    def _stop_all_observations_now(self):
        logger.info("Force stopping all observations!")

        with AutoTangoAllowThreads(self):
            self._observation_controller.stop_all_observations_now()

    @command()
    @only_when_on()
    @debugit()
    @log_exceptions()
    def stop_all_observations_now(self):
        """Force all observations to stop now."""
        self._stop_all_observations_now()

    @command(dtype_in=numpy.int64, dtype_out=DevBoolean)
    @only_when_on()
    @log_exceptions()
    def is_observation_running(self, obs_id: numpy.int64) -> DevBoolean:
        return obs_id in self._observation_controller.running_observations

    @command(dtype_out=DevBoolean)
    @only_when_on()
    @log_exceptions()
    def is_any_observation_running(self) -> DevBoolean:
        return len(self._observation_controller.running_observations) > 0

    @command(dtype_in=DevString, dtype_out=DevBoolean)
    @only_when_on()
    @log_exceptions()
    def is_antenna_field_active(self, antenna_field: DevString = None) -> DevBoolean:
        return antenna_field in self._observation_controller.active_antenna_fields

    TEST_OBS_FIELD_NAME = "STAT/ObservationField/1"

    @command()
    def create_test_device(self):
        Util.instance().create_device(
            ObservationField.__name__, self.TEST_OBS_FIELD_NAME
        )

    @command()
    def destroy_test_device(self):
        Util.instance().delete_device(
            ObservationField.__name__, self.TEST_OBS_FIELD_NAME
        )
