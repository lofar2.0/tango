#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""TileBeam Device Server for LOFAR2.0"""

import logging

# Additional import
from tangostationcontrol.beam.delays import Delays
from tangostationcontrol.beam.managers import TileBeamManager
from tangostationcontrol.common.constants import N_xyz, N_elements
from tangostationcontrol.common.device_decorators import log_exceptions
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)
from tangostationcontrol.devices.base_device_classes.beam_device import BeamDevice
from tangostationcontrol.metrics import device_metrics

logger = logging.getLogger()

__all__ = ["TileBeam"]


@device_logging_to_python()
@device_metrics()
class TileBeam(BeamDevice):
    """Tracks a Tile Beam for all tiles in a single AntennaField.

    This device using asyncio greenmode take care with async methods and awaiting them
    see :py:class:`BeamDevice` and :py:class:`AsyncDevice` for details.
    """

    # --------
    # overloaded functions
    # --------

    def __init__(self, cl, name):
        self.antennafield_proxy = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    def _init_device(self):
        super()._init_device(TileBeamManager(self))

    @log_exceptions()
    def configure_for_initialise(self):
        # We maintain the same number of tiles as the AntennaField
        self._beam_manager.nr_tiles = self.control.read_parent_attribute(
            "nr_antennas_R"
        )
        super().configure_for_initialise(self._beam_manager.nr_tiles)

        # Retrieve positions from AntennaField device
        antenna_reference_itrf = self.control.read_parent_attribute(
            "Antenna_Reference_itrf_R"
        )
        hbat_antenna_itrf_offsets = self.control.read_parent_attribute(
            "HBAT_antenna_itrf_offsets_R"
        ).reshape(self._beam_manager.nr_tiles, N_elements, N_xyz)

        # a delay calculator
        self._beam_manager.delay_calculator = Delays()

        # absolute positions of each tile
        self._beam_manager.HBAT_reference_positions = antenna_reference_itrf

        # absolute positions of each antenna element
        self._beam_manager.HBAT_antenna_positions = [
            antenna_reference_itrf[tile] + hbat_antenna_itrf_offsets[tile]
            for tile in range(self._beam_manager.nr_tiles)
        ]

    def configure_for_off(self):
        # Turn off BeamTracker first
        super().configure_for_off()

        if self._beam_manager and self._beam_manager.delay_calculator:
            self._beam_manager.delay_calculator.stop()
