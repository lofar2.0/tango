# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Metadata device to emit various data"""

# Additional import
import logging
import time
from typing import Any

import numpy
from tango import DebugIt, DeviceProxy
from tango.server import device_property, attribute, command

# PyTango imports
from lofar_station_client.common import CaseInsensitiveDict
from lofar_station_client.zeromq.publisher import ZeroMQPublisher

from tangostationcontrol.asyncio import PeriodicTask
from tangostationcontrol.common.device_decorators import only_in_states, log_exceptions
from tangostationcontrol.common.events.register_subscriptions import (
    register_change_event_subscriptions,
)
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.common.types.device_config_types import (
    device_config_type,
    device_proxy_type,
)
from tangostationcontrol.metadata.metadata_organizer import MetadataOrganizer
from tangostationcontrol.metrics import device_metrics


logger = logging.getLogger()

__all__ = ["Metadata"]


@device_metrics()
class Metadata(LOFARDevice):
    """Metadata device to emit data about relevant to statistics and observations"""

    # -----------------
    # Device Properties
    # -----------------

    metadata_protocol = device_property(dtype="DevString", default_value="tcp")

    metadata_bind = device_property(dtype="DevString", default_value="*")

    metadata_port = device_property(dtype="DevUShort", mandatory=True)

    metadata_topic = device_property(dtype="DevString", default_value="metadata")

    metadata_periodic_publish_interval = device_property(
        dtype="DevFloat",
        doc="Interval with which to force publication of all metadata.",
        mandatory=False,
        default_value=600.0,
    )

    # ----------
    # Attributes
    # ----------

    @attribute(
        doc="Whether the metadata is periodically published",
        dtype=bool,
    )
    def metadata_periodic_publish_thread_running_R(self):
        return (
            self.event_loop_thread
            and self.event_loop_thread.is_running()
            and self.metadata_periodic_publish_task
            and self.metadata_periodic_publish_task.is_running()
        )

    @attribute(
        doc="Is the metadata publisher running",
        dtype=bool,
    )
    def is_publisher_running_R(self):
        return self._publisher.is_running if self._publisher else False

    @attribute(
        doc="Is the metadata publisher stopping",
        dtype=bool,
    )
    def is_publisher_stopping_R(self):
        return self._publisher.is_stopping if self._publisher else False

    @attribute(
        doc="Fraction of queue filled between metadata organizer and metadata publisher",
        dtype=numpy.float64,
    )
    def publisher_queue_fill_fraction_R(self):
        return (
            self._publisher.queue_fill / self._publisher.queue_size
            if self._publisher
            else 0.0
        )

    @attribute(
        doc="Number of messages sent to the publisher since device was started",
        dtype=numpy.int64,
    )
    def messages_published_R(self):
        return self._num_published

    # --------
    # Overloaded functions
    # --------

    STOP_TIME_LIMIT = 10

    METADATA_CONFIG: device_config_type = CaseInsensitiveDict(
        {
            "AFH": [
                "Antenna_to_SDP_Mapping_R",
                "Antenna_Names_R",
                "RCU_PCB_ID_R",
                "RCU_PCB_version_R",
                "Antenna_Usage_Mask_R",
                "Antenna_Reference_ITRF_R",
                "Frequency_Band_RW",
                "RCU_attenuator_dB_R",
                "RCU_DTH_on_R",
                "RCU_DTH_freq_R",
                "HBAT_PWR_on_R",
                "Antenna_Status_R",
            ],
            "AFL": [
                "Antenna_to_SDP_Mapping_R",
                "Antenna_Names_R",
                "RCU_PCB_ID_R",
                "RCU_PCB_version_R",
                "Antenna_Usage_Mask_R",
                "Antenna_Reference_ITRF_R",
                "Frequency_Band_RW",
                "RCU_attenuator_dB_R",
                "RCU_DTH_on_R",
                "RCU_DTH_freq_R",
                "Antenna_Status_R",
            ],
            "SDP": ["subband_frequency_R"],
            "TileBeam": ["Pointing_direction_str_R", "Tracking_enabled_R"],
            "DigitalBeam": [
                "Pointing_direction_str_R",
                "Tracking_enabled_R",
                "subband_select_RW",
            ],
            "SDPFirmware": [
                "FPGA_firmware_version_R",
                "FPGA_hardware_version_R",
                "nr_signal_inputs_R",
                "first_signal_input_index_R",
                "clock_RW",
            ],
            "ObservationControl": ["running_observations_R", "active_antenna_fields_R"],
        }
    )

    def __init__(self, cl, name):
        self._num_published = 0
        self._publisher = None
        self._organizer = None
        self.metadata_periodic_publish_task = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    def _wait_for_done(self):
        """Spin until publisher is done"""
        accumulate = 0
        while not self._publisher.is_done and accumulate < self.STOP_TIME_LIMIT:
            logger.debug("Waiting for publisher thread to stop..")
            time.sleep(0.1)
            accumulate += 0.1

        if accumulate > self.STOP_TIME_LIMIT:
            raise TimeoutError("Failed to stop publisher thread!")

    def configure_for_off(self):
        """user code here. is called when the state is set to OFF"""
        super().configure_for_off()

        # Delete so that event subscriptions are recreated
        if self._organizer is not None:
            del self._organizer
            self._organizer = None

        if self._publisher is not None:
            self._publisher.shutdown()
            try:
                self._wait_for_done()
            finally:
                self._publisher = None

    @log_exceptions()
    def configure_for_initialise(self):
        """Initialises the attributes and properties of the statistics device."""
        super().configure_for_initialise()

        if not self._publisher:
            self._publisher = ZeroMQPublisher(
                ZeroMQPublisher.contstruct_bind_uri(
                    self.metadata_protocol, self.metadata_bind, self.metadata_port
                ),
                [self.metadata_topic],
            )

        if not self._organizer:
            self._organizer = MetadataOrganizer(config=self.METADATA_CONFIG)

    @log_exceptions()
    def configure_for_on(self):
        """Initialises the attributes and properties of the statistics device."""
        super().configure_for_on()

        proxies = self._organizer.create_proxies()
        self.register_change_event_subscriptions(proxies)

        self.metadata_periodic_publish_task = PeriodicTask(
            self.event_loop_thread.event_loop,
            self._send_metadata_async,
            self.metadata_periodic_publish_interval,
        )

    def register_change_event_subscriptions(self, proxies: device_proxy_type):
        """Register change events for working device proxies"""

        register_change_event_subscriptions(
            self.METADATA_CONFIG, proxies, self.events, self.change_event_handler
        )

    def change_event_handler(
        self, device: DeviceProxy, attribute_name: str, value: Any
    ):
        self._organizer.partial_update(device, attribute_name, value)
        self._send_metadata()

    def _send_metadata(self):
        """"""
        self._publisher.send(self._organizer.get_json())
        self._num_published += 1

    async def _send_metadata_async(self):
        """"""
        self._send_metadata()

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def send_metadata(self):
        """Gather and publish metadata attempt to connect failed proxies"""
        new_proxies = self._organizer.create_proxies()
        self.register_change_event_subscriptions(new_proxies)
        self._organizer.gather_metadata()
        self._send_metadata()
