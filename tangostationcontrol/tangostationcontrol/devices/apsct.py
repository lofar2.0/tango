#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""APSCT Device Server for LOFAR2.0"""

import logging

import numpy
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from tango import AttrWriteType

# PyTango imports
from tango import DebugIt
from tango.server import command, attribute, device_property
from tangostationcontrol.common.lofar_logging import device_logging_to_python
from tangostationcontrol.common.states import DEFAULT_COMMAND_STATES
from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice
from tangostationcontrol.common.device_decorators import only_in_states
from tangostationcontrol.metrics import device_metrics

# Additional import

logger = logging.getLogger()

__all__ = ["APSCT"]


@device_logging_to_python()
@device_metrics(
    exclude=[
        "APSCT_error_R",  # too expensive as they read a lot from hardware
        "APSCT_TEMP_error_R",  # too expensive as they read a lot from hardware
        "APSCT_VOUT_error_R",  # too expensive as they read a lot from hardware
        "*_RW",
    ]
)
class APSCT(OPCUADevice):
    """APSCT Device Server for LOFAR2.0"""

    # -----------------
    # Device Properties
    # -----------------

    APSCTTR_monitor_rate_RW_default = device_property(
        dtype="DevLong64", mandatory=False, default_value=1
    )

    # ----- Timing values

    APSCT_On_Off_timeout = device_property(
        doc="Maximum amount of time to wait after turning APSCT on or off",
        dtype="DevFloat",
        mandatory=False,
        default_value=10.0,
    )

    TRANSLATOR_DEFAULT_SETTINGS = ["APSCTTR_monitor_rate_RW"]

    # ----------
    # Attributes
    # ----------

    TR_software_version_R = AttributeWrapper(
        comms_annotation=["TR_software_version_R"], datatype=str
    )
    APSCTTR_I2C_error_R = AttributeWrapper(
        comms_annotation=["APSCTTR_I2C_error_R"], datatype=numpy.int64
    )
    APSCTTR_monitor_rate_RW = AttributeWrapper(
        comms_annotation=["APSCTTR_monitor_rate_RW"],
        datatype=numpy.int64,
        access=AttrWriteType.READ_WRITE,
    )
    APSCTTR_translator_busy_R = AttributeWrapper(
        comms_annotation=["APSCTTR_translator_busy_R"], datatype=bool
    )
    LMP_ID_R = AttributeWrapper(
        comms_annotation=["LMP_ID_R"],
        datatype=numpy.int64,
        doc="Midplane ID (updated on startup or any method call)",
    )
    APSCT_INPUT_10MHz_good_R = AttributeWrapper(
        comms_annotation=["APSCT_INPUT_10MHz_good_R"], datatype=bool
    )
    APSCT_INPUT_PPS_good_R = AttributeWrapper(
        comms_annotation=["APSCT_INPUT_PPS_good_R"], datatype=bool
    )
    APSCT_PCB_ID_R = AttributeWrapper(
        comms_annotation=["APSCT_PCB_ID_R"], datatype=numpy.int64
    )
    APSCT_PCB_number_R = AttributeWrapper(
        comms_annotation=["APSCT_PCB_number_R"], datatype=str
    )
    APSCT_PCB_version_R = AttributeWrapper(
        comms_annotation=["APSCT_PCB_version_R"], datatype=str
    )
    APSCT_PLL_160MHz_error_R = AttributeWrapper(
        comms_annotation=["APSCT_PLL_160MHz_error_R"], datatype=bool
    )
    APSCT_PLL_160MHz_locked_R = AttributeWrapper(
        comms_annotation=["APSCT_PLL_160MHz_locked_R"], datatype=bool
    )
    APSCT_PLL_200MHz_error_R = AttributeWrapper(
        comms_annotation=["APSCT_PLL_200MHz_error_R"], datatype=bool
    )
    APSCT_PLL_200MHz_locked_R = AttributeWrapper(
        comms_annotation=["APSCT_PLL_200MHz_locked_R"], datatype=bool
    )
    APSCT_PPS_ignore_R = AttributeWrapper(
        comms_annotation=["APSCT_PPS_ignore_R"], datatype=bool
    )
    APSCT_PPS_ignore_RW = AttributeWrapper(
        comms_annotation=["APSCT_PPS_ignore_RW"],
        datatype=bool,
        access=AttrWriteType.READ_WRITE,
    )
    APSCT_PWR_CLKDIST1_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_CLKDIST1_3V3_R"], datatype=numpy.float64
    )
    APSCT_PWR_CLKDIST2_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_CLKDIST2_3V3_R"], datatype=numpy.float64
    )
    APSCT_PWR_CTRL_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_CTRL_3V3_R"], datatype=numpy.float64
    )
    APSCT_PWR_INPUT_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_INPUT_3V3_R"], datatype=numpy.float64
    )
    APSCT_PWR_on_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_on_R"], datatype=bool
    )
    APSCT_PWR_PLL_160MHz_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_PLL_160MHz_3V3_R"], datatype=numpy.float64
    )
    APSCT_PWR_PLL_160MHz_on_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_PLL_160MHz_on_R"], datatype=bool
    )
    APSCT_PWR_PLL_200MHz_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_PLL_200MHz_3V3_R"], datatype=numpy.float64
    )
    APSCT_PWR_PLL_200MHz_on_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_PLL_200MHz_on_R"], datatype=bool
    )
    APSCT_PWR_PPSDIST_3V3_R = AttributeWrapper(
        comms_annotation=["APSCT_PWR_PPSDIST_3V3_R"], datatype=numpy.float64
    )
    APSCT_TEMP_R = AttributeWrapper(
        comms_annotation=["APSCT_TEMP_R"], datatype=numpy.float64
    )

    # ----------
    # Summarising Attributes
    # ----------
    APSCT_error_R = attribute(dtype=bool, fisallowed="is_attribute_access_allowed")

    def read_APSCT_error_R(self):
        errors = [
            self.read_attribute("APSCTTR_I2C_error_R") > 0,
            self.alarm_val("APSCT_PCB_ID_R"),
            not self.read_attribute("APSCT_INPUT_10MHz_good_R"),
            not self.read_attribute("APSCT_INPUT_PPS_good_R")
            and not self.read_attribute("APSCT_PPS_ignore_R"),
            not self.read_attribute("APSCT_PLL_160MHz_locked_R")
            and not self.read_attribute("APSCT_PLL_200MHz_locked_R"),
            self.read_attribute("APSCT_PLL_200MHz_locked_R")
            and self.read_attribute("APSCT_PLL_200MHz_error_R"),
            self.read_attribute("APSCT_PLL_160MHz_locked_R")
            and self.read_attribute("APSCT_PLL_160MHz_error_R"),
        ]
        return any(errors)

    APSCT_TEMP_error_R = attribute(
        dtype=bool,
        fisallowed="is_attribute_access_allowed",
    )
    APSCT_VOUT_error_R = attribute(dtype=bool, fisallowed="is_attribute_access_allowed")

    def read_APSCT_TEMP_error_R(self):
        return self.alarm_val("APSCT_TEMP_R")

    def read_APSCT_VOUT_error_R(self):
        return (
            self.alarm_val("APSCT_PWR_PPSDIST_3V3_R")
            or self.alarm_val("APSCT_PWR_CLKDIST1_3V3_R")
            or self.alarm_val("APSCT_PWR_CLKDIST2_3V3_R")
            or self.alarm_val("APSCT_PWR_CTRL_3V3_R")
            or self.alarm_val("APSCT_PWR_INPUT_3V3_R")
            or (
                self.read_attribute("APSCT_PWR_PLL_160MHz_on_R")
                and self.alarm_val("APSCT_PWR_PLL_160MHz_3V3_R")
            )
            or (
                self.read_attribute("APSCT_PWR_PLL_200MHz_on_R")
                and self.alarm_val("APSCT_PWR_PLL_200MHz_3V3_R")
            )
            or (not self.read_attribute("APSCT_PWR_on_R"))
        )

    # --------
    # overloaded functions
    # --------

    def _read_hardware_powered_fraction_R(self):
        """Read attribute which monitors the power"""
        return 1.0 * self.read_attribute("APSCT_PWR_on_R")

    def _power_hardware_on(self):
        """Turns on the 200MHz clock."""

        # Turn on 200 MHz clock
        self.APSCT_200MHz_on()
        self.wait_attribute(
            "APSCTTR_translator_busy_R", False, self.APSCT_On_Off_timeout
        )

        self.wait_attribute("APSCT_PWR_on_R", True, self.APSCT_On_Off_timeout)

        if not self.read_attribute("APSCT_PLL_200MHz_locked_R"):
            if self.read_attribute("APSCTTR_I2C_error_R"):
                raise Exception(
                    "I2C is not working. "
                    + "Maybe power cycle subrack to restart CLK board and translator?"
                )
            else:
                raise Exception(
                    "200MHz signal is not locked. "
                    + "The subrack probably do not receive clock input or the CLK PCB is broken?"
                )

    def _power_hardware_off(self):
        """Turns off the clock."""

        # Turn off the APSCT
        self.APSCT_off()
        self.wait_attribute(
            "APSCTTR_translator_busy_R", False, self.APSCT_On_Off_timeout
        )

        self.wait_attribute("APSCT_PWR_on_R", False, self.APSCT_On_Off_timeout)

    # --------
    # Commands
    # --------

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def APSCT_off(self):
        """
        Turn off the clock.

        Warning: This stops the user image in SDP, making
                 it unresponsive. The factory image is
                 not affected.

        :return:None
        """
        self.opcua_connection.call_method(["APSCT_off"])

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def APSCT_200MHz_on(self):
        """
        Set the clock to 200 MHz.

        :return:None
        """
        self.opcua_connection.call_method(["APSCT_200MHz_on"])

    @command()
    @DebugIt()
    @only_in_states(DEFAULT_COMMAND_STATES)
    def APSCT_160MHz_on(self):
        """

        :return:None
        """
        self.opcua_connection.call_method(["APSCT_160MHz_on"])
