#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Protection management Device Server for LOFAR2.0"""

import logging
from typing import Any

import numpy
from tango import (
    DebugIt,
    DeviceProxy,
)
from tango.server import attribute, device_property
from tango.server import command

# Additional import
from lofar_station_client.common import CaseInsensitiveDict

from tangostationcontrol.asyncio import PeriodicTask
from tangostationcontrol.common.types.device_config_types import (
    device_proxy_type,
    count_proxies_not_exceptions,
)
from tangostationcontrol.common.events.register_subscriptions import (
    register_change_event_subscriptions,
)
from tangostationcontrol.common.lofar_logging import (
    device_logging_to_python,
)
from tangostationcontrol.devices.base_device_classes.lofar_device import LOFARDevice
from tangostationcontrol.metrics import device_metrics
from tangostationcontrol.protection.config_types import (
    convert_protection_to_device_config,
    generate_device_attribute_mapping,
    get_number_of_device_attribute_pairs,
    protection_config_type,
)
from tangostationcontrol.protection.metrics import ProtectionMetrics
from tangostationcontrol.protection.protection_manager import ProtectionManager
from tangostationcontrol.protection.state import ProtectionStateEnum
from tangostationcontrol.protection.threshold import (
    FilteredNumberProtectionThreshold,
)


logger = logging.getLogger()

__all__ = ["ProtectionManager"]


@device_logging_to_python()
@device_metrics()
class ProtectionControl(LOFARDevice):
    """Station protection control Device for LOFAR2.0"""

    # START PROTECTION_CONFIG (needed for sphinx)
    PROTECTION_CONFIG: protection_config_type = CaseInsensitiveDict(
        {
            "UNB2": CaseInsensitiveDict(
                {
                    "UNB2_FPGA_POL_CORE_TEMP_R": FilteredNumberProtectionThreshold(
                        minimal=0.0, maximum=90.0, lower_limit=-128.0, upper_limit=350.0
                    )
                }
            ),
            "SDPFirmware": CaseInsensitiveDict(
                {
                    "FPGA_temp_R": FilteredNumberProtectionThreshold(
                        minimal=0.0, maximum=90.0, lower_limit=-128.0, upper_limit=350.0
                    )
                }
            ),
            "RECVL": CaseInsensitiveDict(
                {
                    "RCU_TEMP_R": FilteredNumberProtectionThreshold(
                        minimal=0.0, maximum=90.0, lower_limit=-128.0, upper_limit=350.0
                    )
                }
            ),
            "RECVH": CaseInsensitiveDict(
                {
                    "RCU_TEMP_R": FilteredNumberProtectionThreshold(
                        minimal=0.0, maximum=90.0, lower_limit=-128.0, upper_limit=350.0
                    )
                }
            ),
            "APSCT": CaseInsensitiveDict(
                {
                    "APSCT_TEMP_R": FilteredNumberProtectionThreshold(
                        minimal=0.0, maximum=90.0, lower_limit=-128.0, upper_limit=350.0
                    )
                }
            ),
            "APSPU": CaseInsensitiveDict(
                {
                    "APSPU_RCU2A_TEMP_R": FilteredNumberProtectionThreshold(
                        minimal=0.0,
                        maximum=100.0,
                        lower_limit=-128.0,
                        upper_limit=350.0,
                    )
                }
            ),
        }
    )
    # END PROTECTION_CONFIG (needed for sphinx)

    # Generated one time at process start
    METRIC_MAPPING = generate_device_attribute_mapping(PROTECTION_CONFIG)
    METRIC_MAPPING_LENGTH = get_number_of_device_attribute_pairs(PROTECTION_CONFIG)

    # -----------------
    # Device Properties
    # -----------------

    periodic_reattempt_interval = device_property(
        dtype="DevFloat",
        doc="Interval with which to reattempt creating failed device proxies.",
        mandatory=False,
        default_value=600.0,
    )

    # ----------
    # Attributes
    # ----------

    @attribute(dtype=ProtectionStateEnum)
    def protection_state_R(self):
        if self._protection_manager:
            return self._protection_manager.state
        else:
            return ProtectionStateEnum.DEACTIVATED

    @attribute(
        doc="Whether the station is periodically evaluated against damage",
        dtype=bool,
    )
    def protection_monitor_thread_running_R(self):
        try:
            return (
                self.event_loop_thread
                and self.event_loop_thread.is_running()
                and self._protection_periodic_task
                and self._protection_periodic_task.is_running()
            )
        except Exception:
            logger.exception("Failed to determine event loop or task status")
            return False

    @attribute(
        doc="The number connected devices for event subscriptions",
        dtype=int,
        fisallowed="is_attribute_access_allowed",
    )
    def number_of_connected_devices_R(self):
        return self._metrics.number_of_connected_devices

    @attribute(
        doc="The number discovered devices for event subscriptions",
        dtype=int,
        fisallowed="is_attribute_access_allowed",
    )
    def number_of_discovered_devices_R(self):
        return self._metrics.number_of_discovered_devices

    @attribute(
        doc="Return the timestamp of the last change event per device attribute pair",
        dtype=(numpy.int32,),
        max_dim_x=METRIC_MAPPING_LENGTH,
    )
    def time_last_change_events_R(self):
        return self._metrics.time_last_change_events

    @attribute(
        doc="Return the devices and each of their attributes identified from the"
        "database. This list matches the order of time_last_change_events_R",
        dtype=(str,),
        max_dim_x=METRIC_MAPPING_LENGTH,
    )
    def identified_device_attribute_pairs_R(self):
        pairs = []
        for device_name, attributes in self.METRIC_MAPPING.items():
            for attribute_name in attributes:
                pairs.append(f"{device_name}.{attribute_name}")
        return pairs

    @attribute(
        doc="The last device attribute pair change event that was above the threshold,"
        "this value can be extremely stale!",
        dtype=str,
    )
    def last_threshold_device_attribute_R(self):
        return self._metrics.threshold_device_attribute

        # --------
        # Overloaded functions
        # --------

    def __init__(self, cl, name):
        self._metrics = None
        self._protection_periodic_task = None
        self._protection_manager = None

        # Super must be called after variable assignment due to executing init_device!
        super().__init__(cl, name)

    def configure_for_off(self):
        """user code here. is called when the state is set to OFF"""
        super().configure_for_off()

        if self._metrics is not None:
            logger.info("Destroying protection metrics instance")
            del self._metrics
            self._metrics = None

        if self._protection_manager is not None:
            logger.info("Destroying protection manager instance")
            del self._protection_manager
            self._protection_manager = None

    def configure_for_initialise(self):
        """Initialises the attributes and properties of the statistics device."""
        super().configure_for_initialise()

        self._metrics = ProtectionMetrics(self.METRIC_MAPPING)
        self._protection_manager = ProtectionManager(self.PROTECTION_CONFIG)

    def configure_for_on(self):
        """Initialises the attributes and properties of the statistics device."""
        super().configure_for_on()

        proxies = self._protection_manager.create_proxies()
        self.register_change_event_subscriptions(proxies)
        self._metrics.update_device_metrics(proxies)

        self._protection_periodic_task = PeriodicTask(
            self.event_loop_thread.event_loop,
            self.reattempt_proxy_connections,
            self.periodic_reattempt_interval,
        )

    def register_change_event_subscriptions(self, proxies: device_proxy_type):
        """Register change events for working device proxies"""

        register_change_event_subscriptions(
            convert_protection_to_device_config(self.PROTECTION_CONFIG),
            proxies,
            self.events,
            self.change_event_handler,
        )

    def change_event_handler(
        self, device: DeviceProxy, attribute_name: str, value: Any
    ):
        try:
            if self._protection_manager.evaluate(
                device.info().dev_class, attribute_name, value
            ):
                logger.error(
                    "Device: %s has attribute: %s that is above threshold",
                    device.name(),
                    attribute_name,
                )
            self._metrics.update_change_event(device.name(), attribute_name)
        except Exception:
            logger.exception(
                "Failed to evaluate new value %s:%s", device.name(), attribute_name
            )

    async def reattempt_proxy_connections(self):
        logger.debug("Reattempting failed proxy connections")
        new_proxies = self._protection_manager.create_proxies()

        if count_proxies_not_exceptions(new_proxies) <= 0:
            return

        logger.info("Established new proxy connections: %s", new_proxies)

        self.register_change_event_subscriptions(new_proxies)
        self._metrics.update_device_metrics(new_proxies)

    def _protective_shutdown(self, reason: str):
        """Manually triggered protective shutdown"""

        logger.warning("Manually triggering protective shutdown because of: %s", reason)
        self._protection_manager.protective_shutdown()

    @command(dtype_in=str)
    @DebugIt()
    def protective_shutdown(self, reason: str):
        """Trigger protective shutdown"""
        self._protective_shutdown(reason)
