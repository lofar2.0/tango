# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import List

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase


class XST(_ConfigurationBase):
    def __init__(
        self,
        integration_interval: float,
        subbands: List[int],
        subbands_step: int | None = None,
    ):
        self.subbands = subbands
        self.subbands_step = subbands_step
        self.integration_interval = integration_interval

    def __iter__(self):
        yield "integration_interval", self.integration_interval
        yield "subbands", self.subbands

        if self.subbands_step is not None:
            yield "subbands_step", self.subbands_step

    @staticmethod
    def to_object(json_dct) -> "XST":
        return XST(
            json_dct["integration_interval"],
            json_dct["subbands"],
            json_dct.get("subbands_step"),
        )
