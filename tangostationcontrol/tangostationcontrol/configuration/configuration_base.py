#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import json
import re
from abc import ABC, abstractmethod
from typing import TypeVar, Type

import jsonschema
from jsonschema import Draft7Validator, FormatChecker, ValidationError

from tangostationcontrol.configuration import REGISTRY
from tangostationcontrol.configuration._json_parser import _from_json_hook_t

T = TypeVar("T")


def _is_object(_, instance):
    return isinstance(instance, dict) or issubclass(type(instance), _ConfigurationBase)


jsonschema.validators.Draft7Validator.TYPE_CHECKER = (
    Draft7Validator.TYPE_CHECKER.redefine(
        "object",
        _is_object,
    )
)


class _ConfigurationBase(ABC):
    @staticmethod
    def _class_to_url(cls_name: str) -> str:
        """Class name to json schema file conversion name"""
        cls_name = cls_name.replace("HBA", "hba")
        cls_name = cls_name.replace("LBA", "lba")
        cls_name = cls_name.replace("SST", "sst")
        cls_name = cls_name.replace("XST", "xst")
        return re.sub(r"(?<!^)(?=[A-Z])", "-", cls_name).lower()

    @classmethod
    def get_validator(cls) -> Draft7Validator:
        """Retrieve schema validation file and return Draft7Validator instance

        Schema file name is derived from class name see :py:func:`~._class_to_url` for
        conversion
        """
        return Draft7Validator(
            REGISTRY[_ConfigurationBase._class_to_url(cls.__name__)].contents,
            format_checker=FormatChecker(),
            registry=REGISTRY,
        )

    @abstractmethod
    def __iter__(self):
        pass

    def __str__(self):
        try:
            return json.dumps(dict(self), ensure_ascii=False)
        except TypeError:
            # This happens if we want to dump a type that cannot be represented in JSON
            return str(dict(self))

    def __repr__(self):
        return self.__str__()

    # required for jsonschema validation
    def __getitem__(self, item):
        return getattr(self, item)

    # required for jsonschema validation
    def __contains__(self, item):
        return hasattr(self, item) and getattr(self, item) is not None

    def to_json(self) -> str:
        return self.__str__()

    @staticmethod
    @abstractmethod
    def to_object(json_dct) -> T:
        pass

    @classmethod
    def from_json(cls: Type[T], data: str) -> T:
        try:
            s = json.loads(data, object_hook=_from_json_hook_t(cls))
        except json.decoder.JSONDecodeError as ex:
            raise ValueError(f"Error decoding JSON {data}") from ex

        if not isinstance(s, cls):
            raise ValidationError(
                f"Unexpected type: expected <{cls.__class__.__name__}>, got "
                f"<{type(s).__name__}>: {s}"
            )
        return s
