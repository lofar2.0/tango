# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime
from typing import Sequence

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase
from tangostationcontrol.configuration.dithering import Dithering
from tangostationcontrol.configuration.hba import HBA
from tangostationcontrol.configuration.sap import Sap
from tangostationcontrol.configuration.sst import SST
from tangostationcontrol.configuration.xst import XST


class ObservationFieldSettings(_ConfigurationBase):
    def __init__(
        self,
        observation_id: int,
        start_time: str | datetime | None,
        stop_time: str | datetime,
        antenna_field: str,
        antenna_set: str,
        filter: str,
        SAPs: Sequence[Sap],
        HBA: HBA | None = None,
        first_beamlet: int = 0,
        lead_time: float | None = None,
        dithering: Dithering | None = None,
        SST: SST | None = None,
        XST: XST | None = None,
    ):
        self.observation_id = observation_id
        self.start_time = self._parse_and_convert_datetime(start_time)
        self.stop_time = self._parse_and_convert_datetime(stop_time)
        self.antenna_field = antenna_field
        self.antenna_set = antenna_set
        self.filter = filter
        self.SAPs = SAPs
        self.HBA = HBA
        self.first_beamlet = first_beamlet
        self.lead_time = lead_time
        self.dithering = dithering
        self.SST = SST
        self.XST = XST

    @staticmethod
    def _parse_and_convert_datetime(time: str | datetime | None):
        """Transparently convert datetime to string in isoformat"""
        if time and not isinstance(time, datetime):
            try:
                datetime.fromisoformat(time)
            except ValueError as ex:
                raise ex
        if time and isinstance(time, datetime):
            return time.isoformat()

        return time

    def __iter__(self):
        yield "observation_id", self.observation_id
        if self.start_time:
            yield "start_time", self.start_time
        yield from {
            "stop_time": self.stop_time,
            "antenna_field": self.antenna_field,
            "antenna_set": self.antenna_set,
            "filter": self.filter,
            "SAPs": [dict(s) for s in self.SAPs],
        }.items()
        if self.HBA:
            yield "HBA", dict(self.HBA)
        yield "first_beamlet", self.first_beamlet
        if self.lead_time is not None:
            yield "lead_time", self.lead_time
        if self.dithering is not None:
            yield "dithering", dict(self.dithering)
        if self.SST is not None:
            yield "SST", dict(self.SST)
        if self.XST is not None:
            yield "XST", dict(self.XST)

    @staticmethod
    def to_object(json_dct) -> "ObservationFieldSettings":
        return ObservationFieldSettings(
            json_dct["observation_id"],
            json_dct["start_time"] if "start_time" in json_dct else None,
            json_dct["stop_time"],
            json_dct["antenna_field"],
            json_dct["antenna_set"],
            json_dct["filter"],
            json_dct["SAPs"],
            json_dct.get("HBA"),
            json_dct.get("first_beamlet", 0),
            json_dct.get("lead_time"),
            json_dct.get("dithering"),
            json_dct.get("SST"),
            json_dct.get("XST"),
        )
