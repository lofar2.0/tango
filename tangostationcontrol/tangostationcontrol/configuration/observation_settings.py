# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Sequence

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase
from tangostationcontrol.configuration.observation_field_settings import (
    ObservationFieldSettings,
)


class ObservationSettings(_ConfigurationBase):
    def __init__(
        self, station: str, antenna_fields: Sequence[ObservationFieldSettings]
    ):
        self.station = station
        self.antenna_fields = antenna_fields

    def __iter__(self):
        yield "station", self.station
        yield "antenna_fields", [dict(s) for s in self.antenna_fields]

    @staticmethod
    def to_object(json_dct) -> "ObservationSettings":
        return ObservationSettings(json_dct["station"], json_dct["antenna_fields"])
