# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase
from tangostationcontrol.configuration.pointing import Pointing


class HBA(_ConfigurationBase):
    def __init__(
        self,
        tile_beam: Pointing,
        DAB_filter: bool | None = None,
        element_selection: str | None = "ALL",
    ):
        self.tile_beam = tile_beam
        self.DAB_filter = DAB_filter
        self.element_selection = element_selection

    def __iter__(self):
        yield from {
            "tile_beam": dict(self.tile_beam),
        }.items()

        if self.DAB_filter is not None:
            yield "DAB_filter", self.DAB_filter
        if self.element_selection is not None:
            yield "element_selection", self.element_selection

    @staticmethod
    def to_object(json_dct) -> "HBA":
        return HBA(
            json_dct["tile_beam"],
            json_dct.get("DAB_filter"),
            json_dct.get("element_selection"),
        )
