# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Type

from jsonschema.exceptions import ValidationError


def _from_json_hook_t(primary: Type):
    from tangostationcontrol.configuration import (
        Pointing,
        Sap,
        ObservationSettings,
        ObservationFieldSettings,
        HBA,
        Dithering,
        SST,
        XST,
    )

    def actual_hook(json_dct):
        """Validate json_dct for each schema layer up to the primary type"""
        primary_ex = None
        # Order is critical, must match inheritance, deepest layers first
        for t in [
            SST,
            XST,
            Pointing,
            Sap,
            HBA,
            Dithering,
            ObservationFieldSettings,
            ObservationSettings,
        ]:
            try:
                t.get_validator().validate(json_dct)
            except ValidationError as ex:
                if t is primary:
                    primary_ex = ex
                pass
            else:
                return t.to_object(json_dct)
        if primary_ex:
            raise primary_ex
        return None

    return actual_hook
