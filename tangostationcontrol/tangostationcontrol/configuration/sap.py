# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase
from tangostationcontrol.configuration.pointing import Pointing


class Sap(_ConfigurationBase):
    def __init__(self, subbands: [int], pointing: Pointing):
        self.subbands = subbands
        self.pointing = pointing

    def __iter__(self):
        yield from {"subbands": self.subbands, "pointing": dict(self.pointing)}.items()

    @staticmethod
    def to_object(json_dct) -> "Sap":
        return Sap(json_dct["subbands"], json_dct["pointing"])
