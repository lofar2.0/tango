# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase


class Pointing(_ConfigurationBase):
    VALIDATOR = None

    def __init__(
        self,
        angle1=0.6624317181687094,
        angle2=1.5579526427549426,
        direction_type="J2000",
    ):
        self.angle1 = angle1
        self.angle2 = angle2
        self.direction_type = direction_type

    def __iter__(self):
        yield from {
            "angle1": self.angle1,
            "angle2": self.angle2,
            "direction_type": self.direction_type,
        }.items()

    @staticmethod
    def to_object(json_dct) -> "Pointing":
        return Pointing(
            json_dct["angle1"], json_dct["angle2"], json_dct["direction_type"]
        )
