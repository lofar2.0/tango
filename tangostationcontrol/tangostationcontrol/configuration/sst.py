# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase


class SST(_ConfigurationBase):
    def __init__(
        self,
        subbands_calibrated: bool,
    ):
        self.subbands_calibrated = subbands_calibrated

    def __iter__(self):
        yield "subbands_calibrated", self.subbands_calibrated

    @staticmethod
    def to_object(json_dct) -> "SST":
        return SST(
            json_dct["subbands_calibrated"],
        )
