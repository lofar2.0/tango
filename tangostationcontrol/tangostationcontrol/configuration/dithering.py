# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.configuration.configuration_base import _ConfigurationBase


class Dithering(_ConfigurationBase):
    VALIDATOR = None

    def __init__(
        self,
        enabled: bool,
        power: float | None,
        frequency: int | None,
    ):
        self.enabled = enabled
        self.power = power
        self.frequency = frequency

    def __iter__(self):
        yield from {
            "enabled": self.enabled,
        }.items()
        if self.power is not None:
            yield "power", self.power
        if self.frequency is not None:
            yield "frequency", self.frequency

    @staticmethod
    def to_object(json_dct) -> "Dithering":
        return Dithering(
            json_dct["enabled"],
            json_dct.get("power"),
            json_dct.get("frequency"),
        )
