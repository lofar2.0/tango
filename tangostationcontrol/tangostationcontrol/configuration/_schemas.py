#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import json

try:
    from importlib.resources import files
except ImportError:
    from importlib_resources import files  # type: ignore

from referencing import Resource
from referencing import Registry as _Registry
from referencing.jsonschema import SchemaRegistry as _SchemaRegistry


def _schemas():
    for schema in files(__package__).joinpath("schemas").glob("*.json"):
        try:
            contents = json.loads(schema.read_text(encoding="utf-8"))
        except json.decoder.JSONDecodeError as ex:
            raise ValueError(f"Error decoding JSON schema {schema}") from ex

        yield Resource.from_contents(contents)


#: A `referencing.jsonschema.SchemaRegistry` containing all of the official
#: meta-schemas and vocabularies.
REGISTRY: _SchemaRegistry = (_schemas() @ _Registry()).crawl()
