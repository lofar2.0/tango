"""Station Control software related to Tango devices"""

# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from importlib import metadata

__version__ = metadata.version("tangostationcontrol")


def print_version(*args, **kwargs):
    """Output version of tangostationcontrol"""
    print(__version__)
