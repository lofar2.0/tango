# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
from contextlib import suppress
from concurrent.futures import CancelledError
from typing import Callable

from tangostationcontrol.common.atomic import Atomic

logger = logging.getLogger()


class PeriodicTask:
    """Provide a periodic call to a coroutine."""

    def __init__(
        self,
        event_loop,
        func: Callable[[], None],
        interval: float = 1.0,
    ):
        self.event_loop = event_loop
        self.func = func
        self.interval = interval

        self.done = Atomic(False)
        self.task = None

        future = asyncio.run_coroutine_threadsafe(
            self._schedule_call_periodically(), self.event_loop
        )
        _ = future.result()

    async def _schedule_call_periodically(self):
        self.task = asyncio.create_task(
            self._call_periodically(), name=f"PeriodicTask for {self.func}"
        )

    async def _call_periodically(self):
        while not self.done.value:
            try:
                await self.func()
            except (CancelledError, asyncio.CancelledError):
                raise
            except Exception as ex:
                logger.exception(f"Periodic task for {self.func} raised an exception")

            # TODO(JDM): Calculate how long to sleep to have the runtime of
            #            func be subtracted.
            await asyncio.sleep(self.interval)

    def join(self):
        """Wait for the periodic task to stop or throw an exception, and reap that result."""

        if self.event_loop.is_closed():
            return

        async def wait_for_task():
            with suppress(asyncio.CancelledError):
                await self.task

        future = asyncio.run_coroutine_threadsafe(wait_for_task(), self.event_loop)
        _ = future.result()

    def stop(self):
        """Stop gracefully, to avoid cancelling self.func(), breaking their state."""

        self.done.value = True

    def cancel(self):
        """Stop non-gracefully."""
        if not self.event_loop.is_closed():
            self.event_loop.call_soon_threadsafe(self.task.cancel)
        self.stop()

    def is_running(self):
        """Return whether the periodic call is still scheduled."""
        return self.task and not self.task.done() and not self.done.value
