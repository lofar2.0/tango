# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
from concurrent.futures import Future

from tangostationcontrol.common.threading import OmniThread

logger = logging.getLogger()


class EventLoopThread:
    """A forever running thread that keeps executing the given event_loop."""

    def __init__(self, name: str = "anonymous", event_loop=None):
        self.name = name
        self.event_loop = event_loop or asyncio.new_event_loop()

        self.event_loop_thread = OmniThread(
            target=self._run_loop,
            args=(self.event_loop, self.name),
            name=f"{self.name} event loop",
            daemon=True,
        )
        self.event_loop_thread.start()

    def call_soon_threadsafe(self, coro, *args) -> asyncio.Handle:
        """Schedules the coroutine to run (fire & forget). Return a Handle to allow cancelling."""
        return self.event_loop.call_soon_threadsafe(coro, *args)

    def run_coroutine_threadsafe(self, coro) -> Future:
        """Schedules the coroutine to run and returns a Future for the result."""
        return asyncio.run_coroutine_threadsafe(coro, self.event_loop)

    def stop(self):
        if self.event_loop_thread is not None:
            # signal our event loop thread to stop
            self.event_loop.call_soon_threadsafe(self.event_loop.stop)

            # reap our event loop thread once it is done processing tasks
            self.event_loop_thread.join()
            self.event_loop_thread = None

    def is_running(self):
        return self.event_loop.is_running()

    def __del__(self):
        self.stop()

    @staticmethod
    def _run_loop(loop: asyncio.AbstractEventLoop, name: str) -> None:
        asyncio.set_event_loop(loop)

        try:
            logger.debug(f"EventLoop {name}: starting")
            loop.run_forever()
            logger.debug(f"EventLoop {name}: loop stopped gracefully")
        finally:
            # stop any tasks still running
            for task in asyncio.all_tasks(loop):
                logger.debug(f"EventLoop {name}: cancelling {task}")
                task.cancel()

            # properly clean up, see https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.shutdown_asyncgens
            logger.debug(f"EventLoop {name}: finishing cancelled tasks")
            loop.run_until_complete(loop.shutdown_asyncgens())
            logger.debug(f"EventLoop {name}: closing")
            loop.close()
            logger.debug(f"EventLoop {name}: done")
