# Copyright (C) 2025 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime, timedelta, timezone
from math import log
from typing import Dict, Tuple
import logging

import numpy

from tangostationcontrol.common.constants import N_pol, N_subbands
from tangostationcontrol.rpc._proto import statistics_pb2
from tangostationcontrol.rpc._proto import statistics_pb2_grpc
from tangostationcontrol.rpc.common import (
    call_exception_metrics,
)

logger = logging.getLogger()


def dB(x: float) -> float:
    return 10.0 * log(x, 10) if x > 0.0 else 0.0


class NotAvailableError(RuntimeError):
    """The requested statistics are not available."""

    pass


class TooOldError(RuntimeError):
    """The requested statistics are older than requested."""

    pass


class LastStatisticsMessagesMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.last_messages: Dict[Tuple[str, str], dict] = {}

    def handle_statistics_message(self, topic, timestamp, msg):
        """Process a single ZMQ message."""

        type_, antenna_field, station = topic.split("/")
        self.last_messages[(type_, antenna_field)] = msg

    def get_last_message(self, type_: str, antenna_field: str) -> dict:
        """Return the last ZMQ message for the given type and antenna field.

        Raises NotAvailableError if no message has arrived yet."""

        try:
            return self.last_messages[(type_, antenna_field)]
        except KeyError as e:
            raise NotAvailableError(
                f"Statistics not available of type {type_} for antenna field '{antenna_field}'"
            )


class Statistics(statistics_pb2_grpc.StatisticsServicer, LastStatisticsMessagesMixin):
    def _check_message_age(self, message: dict, maxage: int | None):
        """Raises TooOldError if the message is older than maxage (in seconds)."""

        if not maxage:
            return

        msg_timestamp = datetime.fromisoformat(message["timestamp"])
        now = datetime.now(tz=timezone.utc)

        if now - msg_timestamp > timedelta(seconds=maxage):
            raise TooOldError(
                f"Message generated at {msg_timestamp} is older than {maxage} seconds."
            )

    @staticmethod
    def _message_to_frequency_band(message: dict) -> statistics_pb2.FrequencyBand:
        """Extract a FrequencyBand out of a ZMQ message, originating from the packet header."""

        antenna_band_to_type = {0: "LBA", 1: "HBA"}

        return statistics_pb2.FrequencyBand(
            antenna_type=antenna_band_to_type[
                message["source_info"]["antenna_band_index"]
            ],
            clock=message["f_adc"] * 1_000_000,
            nyquist_zone=message["source_info"]["nyquist_zone_index"],
        )

    @call_exception_metrics("Statistics")
    def Bst(self, request: statistics_pb2.BstRequest, context):
        bst_message = self.get_last_message("bst", request.antenna_field)
        self._check_message_age(bst_message, request.maxage)

        beamlets = [
            statistics_pb2.BstResult.BstBeamlet(
                beamlet=beamlet_nr,
                x_power_db=dB(beamlet_data[0]),
                y_power_db=dB(beamlet_data[1]),
            )
            for beamlet_nr, beamlet_data in enumerate(bst_message["bst_data"])
        ]

        return statistics_pb2.BstReply(
            result=statistics_pb2.BstResult(
                timestamp=datetime.fromisoformat(bst_message["timestamp"]),
                frequency_band=self._message_to_frequency_band(bst_message),
                integration_interval=bst_message["integration_interval"],
                beamlets=beamlets,
            ),
        )

    @call_exception_metrics("Statistics")
    def Sst(self, request: statistics_pb2.SstRequest, context):
        sst_message = self.get_last_message("sst", request.antenna_field)
        self._check_message_age(sst_message, request.maxage)

        sst_data = numpy.array(sst_message["sst_data"], dtype=numpy.float32)
        sst_data = sst_data.reshape((-1, N_pol, N_subbands))

        subbands = [
            statistics_pb2.SstResult.SstSubband(
                subband=subband_nr,
                antennas=[
                    statistics_pb2.SstResult.SstSubband.SstAntenna(
                        antenna=antenna_nr,
                        x_power_db=dB(antenna_data[0][subband_nr]),
                        y_power_db=dB(antenna_data[1][subband_nr]),
                    )
                    for antenna_nr, antenna_data in enumerate(sst_data)
                ],
            )
            for subband_nr in range(1, N_subbands)
        ]

        return statistics_pb2.SstReply(
            result=statistics_pb2.SstResult(
                timestamp=datetime.fromisoformat(sst_message["timestamp"]),
                frequency_band=self._message_to_frequency_band(sst_message),
                integration_interval=sst_message["integration_interval"],
                subbands=subbands,
            ),
        )

    @call_exception_metrics("Statistics")
    def Xst(self, request: statistics_pb2.XstRequest, context):
        xst_message = self.get_last_message("xst", request.antenna_field)
        self._check_message_age(xst_message, request.maxage)

        # turn real & imaginary data from message into
        # power and angle arrays of the right shape

        xst_data_real = numpy.array(xst_message["xst_data_real"], dtype=numpy.float32)
        xst_data_imag = numpy.array(xst_message["xst_data_imag"], dtype=numpy.float32)
        xst_data = xst_data_real + xst_data_imag * 1j

        nr_inputs = xst_data.shape[0]
        nr_antennas = nr_inputs // N_pol
        xst_data = xst_data.reshape((nr_antennas, N_pol, nr_antennas, N_pol))
        xst_power = numpy.abs(xst_data)
        xst_phase = numpy.angle(xst_data)

        baselines = [
            statistics_pb2.XstResult.XstBaseline(
                antenna1=antenna1,
                antenna2=antenna2,
                xx=statistics_pb2.XstResult.XstBaseline.XstValue(
                    power_db=dB(xst_power[antenna1, 0, antenna2, 0]),
                    phase=xst_phase[antenna1, 0, antenna2, 0],
                ),
                xy=statistics_pb2.XstResult.XstBaseline.XstValue(
                    power_db=dB(xst_power[antenna1, 0, antenna2, 1]),
                    phase=xst_phase[antenna1, 0, antenna2, 1],
                ),
                yx=statistics_pb2.XstResult.XstBaseline.XstValue(
                    power_db=dB(xst_power[antenna1, 1, antenna2, 0]),
                    phase=xst_phase[antenna1, 1, antenna2, 0],
                ),
                yy=statistics_pb2.XstResult.XstBaseline.XstValue(
                    power_db=dB(xst_power[antenna1, 1, antenna2, 1]),
                    phase=xst_phase[antenna1, 1, antenna2, 1],
                ),
            )
            for antenna1 in range(nr_antennas)
            for antenna2 in range(nr_antennas)
            if antenna1 <= antenna2
        ]

        return statistics_pb2.XstReply(
            result=statistics_pb2.XstResult(
                timestamp=datetime.fromisoformat(xst_message["timestamp"]),
                frequency_band=self._message_to_frequency_band(xst_message),
                integration_interval=xst_message["integration_interval"],
                subband=xst_message["subband"],
                baselines=baselines,
            ),
        )
