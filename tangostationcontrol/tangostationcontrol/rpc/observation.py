# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from tangostationcontrol.rpc._proto import observation_pb2
from tangostationcontrol.rpc._proto import observation_pb2_grpc
from tangostationcontrol.rpc.common import (
    call_exception_metrics,
    reply_on_exception,
)
from tangostationcontrol.common.proxies.proxy import create_device_proxy

logger = logging.getLogger()


class Observation(observation_pb2_grpc.ObservationServicer):
    TIMEOUT = 5000

    @reply_on_exception(observation_pb2.ObservationReply)
    @call_exception_metrics("Observation")
    def StartObservation(
        self, request: observation_pb2.StartObservationRequest, context
    ):
        observation_control = create_device_proxy(
            "STAT/ObservationControl/1", self.TIMEOUT
        )
        observation_control.add_observation(request.configuration)
        return observation_pb2.ObservationReply(success=True)

    @reply_on_exception(observation_pb2.ObservationReply)
    @call_exception_metrics("Observation")
    def StopObservation(self, request: observation_pb2.StopObservationRequest, context):
        logger.info(f"Stopping observation {request.observation_id}")
        observation_control = create_device_proxy(
            "STAT/ObservationControl/1", self.TIMEOUT
        )
        observation_control.stop_observation_now(request.observation_id)
        return observation_pb2.ObservationReply(success=True)
