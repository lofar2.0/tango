"""This module handles antenna field control via GRPC"""

import logging

import tango
from tango import DeviceProxy

from tangostationcontrol.common.antennas import antenna_field_family_name
from tangostationcontrol.rpc._proto import antennafield_pb2_grpc

from tangostationcontrol.rpc._proto.antennafield_pb2 import (
    AntennaReply,
    AntennaResult,
    GetAntennaRequest,
    Identifier,
    SetAntennaStatusRequest,
    SetAntennaUseRequest,
)
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.rpc.common import (
    call_exception_metrics,
    reply_on_exception,
)

logger = logging.getLogger()


class AntennaNotFoundException(ValueError):
    """Exception raised when an antenna is not found in the device."""


class AntennaField(antennafield_pb2_grpc.AntennafieldServicer):
    """Represents an antenna field in the TangoStationControl system.

    This class provides methods to control and manage antennas remotely.
    """

    def _create_antennafield_device_proxy(
        self, identifier: Identifier, write_access: bool = False
    ) -> DeviceProxy:
        """Get the device proxy for a antenna in a antennafield, given that it exist."""
        try:
            family = antenna_field_family_name(identifier.antennafield_name)
            db = tango.Database()
            device_exported = db.get_device_exported_for_class(family)

            for device_name in device_exported:
                _, _, exported_antenna_name = device_name.split("/")
                if (
                    exported_antenna_name.casefold()
                    == identifier.antenna_name.casefold()
                ):
                    return create_device_proxy(device_name, write_access=write_access)
        except Exception as ex:
            logger.exception(
                "Failed to create device proxy",
                extra={
                    "antennafield_name": identifier.antennafield_name,
                    "antenna_name": identifier.antenna_name,
                },
            )
            raise IOError(
                f"Failed to create device proxy to '{identifier.antennafield_name}'"
            ) from ex
        else:
            raise ValueError(
                f"Antenna in Antenna field not found '{identifier.antennafield_name} '"
            )

    def _get_antenna_index(
        self, identifier: Identifier, antennafield_device: DeviceProxy
    ) -> int:
        """Returns the current antenna index."""
        try:
            return antennafield_device.Antenna_Names_R.index(identifier.antenna_name)
        except ValueError as exc:
            logger.warning(
                "Antenna not found",
                extra={
                    "antenna_name": identifier.antenna_name,
                    "antennafield_name": identifier.antennafield_name,
                },
            )
            raise AntennaNotFoundException(
                f"Antenna '{identifier.antenna_name}' "
                f"not found in device '{identifier.antennafield_name}'."
            ) from exc

    def _get_antenna_reply(
        self,
        identifier: Identifier,
        antenna_index: int,
        antennafield_device: DeviceProxy,
    ):
        """Returns the  Message for  the caller of the GRPC."""
        try:
            antenna_status = antennafield_device.Antenna_Status_R[antenna_index]
            antenna_use = antennafield_device.Antenna_Use_R[antenna_index]
        except (AttributeError, IndexError) as ex:
            raise ValueError(
                f"Could not access device attribute for antenna "
                f"{identifier.antenna_name=} "
                f"{identifier.antenna_index}"
            ) from ex

        return AntennaReply(
            success=True,
            exception="",
            result=AntennaResult(
                identifier=identifier,
                antenna_use=antenna_use,
                antenna_status=antenna_status,
            ),
        )

    @reply_on_exception(AntennaReply)
    @call_exception_metrics("AntennaField")
    def GetAntenna(self, request: GetAntennaRequest, context):
        """Gets Correct Device, then returns the Message for  the caller of the GRPC."""
        antennafield_device = self._create_antennafield_device_proxy(request.identifier)
        antenna_index = self._get_antenna_index(request.identifier, antennafield_device)

        return self._get_antenna_reply(
            request.identifier, antenna_index, antennafield_device
        )

    def _apply_changes(self, antenna_field):
        """Apply Changes to the System. It is required to bring it off first"""
        antenna_field.Off()
        antenna_field.Initialise()
        antenna_field.On()

    def _update_antenna_property(
        self,
        identifier: Identifier,
        attribute_name: str,
        property_name: str,
        new_value: int,
    ) -> AntennaReply:
        """Update the antenna Property"""
        antennafield_device = self._create_antennafield_device_proxy(identifier, True)
        antenna_index = self._get_antenna_index(identifier, antennafield_device)

        property_value = list(antennafield_device.read_attribute(attribute_name).value)
        property_value[antenna_index] = new_value

        antennafield_device.put_property({property_name: property_value})
        self._apply_changes(antennafield_device)

        return self._get_antenna_reply(identifier, antenna_index, antennafield_device)

    @reply_on_exception(AntennaReply)
    @call_exception_metrics("AntennaField")
    def SetAntennaStatus(self, request: SetAntennaStatusRequest, context):
        """Set the new Antenna  Status"""
        return self._update_antenna_property(
            request.identifier,
            "Antenna_Status_R",
            "Antenna_Status",
            request.antenna_status,
        )

    @reply_on_exception(AntennaReply)
    @call_exception_metrics("AntennaField")
    def SetAntennaUse(self, request: SetAntennaUseRequest, context):
        """Set the new Antenna  Use"""
        return self._update_antenna_property(
            request.identifier, "Antenna_Use_R", "Antenna_Use", request.antenna_use
        )
