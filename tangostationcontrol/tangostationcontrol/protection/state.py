# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from enum import IntEnum

__all__ = ["ProtectionStateEnum"]


class ProtectionStateEnum(IntEnum):
    """Protection states enumeration

    Valid transitions:
    off -> armed
    armed -> active
    active -> transitioning
    active -> fault
    active -> aborted
    active -> off
    transitioning -> active
    """

    DEACTIVATED = 0  # protective control measures are disabled outright
    OFF = 1  # No protective shutdown is ongoing
    ARMED = 2  # A protective shutdown is being prepared
    ACTIVE = 3  # A protective shutdown is ongoing and the stationmanager is locked against state transitions
    TRANSITIONING = 4  # Protective shutdown transitions are taking place, the shutdown is ongoing the stationmanager is locked
    FAULT = 5  # The protective shutdown encountered an unrecoverable exception
    ABORTED = 6  # If the protective shutdown was aborted before completion

    """"""

    @staticmethod
    def keys():
        return [s.name for s in ProtectionStateEnum]
