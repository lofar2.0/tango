# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import abc
import numbers
from typing import Any

import numpy

from tangostationcontrol.common.type_checking import sequence_not_str


class IProtectionThreshold(abc.ABC):
    @abc.abstractmethod
    def evaluate(self, value: Any) -> bool:
        """Returns true if threshold / condition is exceeded / met"""
        raise NotImplementedError("Interface method not implemented by child class")


class NumberProtectionThreshold(IProtectionThreshold):

    def __init__(self, minimal: numbers.Real, maximum: numbers.Real):
        self._min = minimal
        self._max = maximum

    @staticmethod
    def _evaluate(value: Any, minimal: numbers.Real, maximum: numbers.Real):
        """Iterate collections infinitely and evaluate .any() against thresholds

        :return: returns true if any value is above or below threshold
        """
        if isinstance(value, numpy.ndarray):
            return numpy.any(value > maximum)  # or numpy.any(value < minimal)
        elif sequence_not_str(value) and len(value) > 0:
            for element in value:
                if NumberProtectionThreshold._evaluate(element, minimal, maximum):
                    return True
        elif value > maximum:  # or value < minimal:
            return True
        return False

    def evaluate(self, value: Any) -> bool:
        """Iterate collections infinitely and evaluate .any() against thresholds

        :return: returns true if any value is above or below threshold
        """
        return self._evaluate(value, self._min, self._max)


class FilteredNumberProtectionThreshold(NumberProtectionThreshold):
    """Evaluate if the value is within min / max but ignore values beyond limits"""

    def __init__(
        self,
        minimal: numbers.Real,
        maximum: numbers.Real,
        lower_limit: numbers.Real,
        upper_limit: numbers.Real,
    ):
        super().__init__(minimal, maximum)

        self._lower_limit = lower_limit
        self._upper_limit = upper_limit

    def evaluate(self, value: Any) -> bool:
        """Filter values beyond limit and evaluate"""

        if self._evaluate(value, self._lower_limit, self._upper_limit):
            return False

        return super().evaluate(value)
