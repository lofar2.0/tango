# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Protection management class"""

import logging
import time
from concurrent.futures import ThreadPoolExecutor
from typing import Any

import tango
from lofar_station_client.common import CaseInsensitiveDict
from tango import DevFailed

from tangostationcontrol.common.atomic import Atomic
from tangostationcontrol.common.proxies.create_proxies import (
    create_proxies,
    ICreateProxies,
)
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.types.device_config_types import (
    device_proxy_type,
)
from tangostationcontrol.protection.config_types import (
    protection_config_type,
    convert_protection_to_device_config,
)
from tangostationcontrol.protection.state import ProtectionStateEnum
from tangostationcontrol.states.station_state_enum import StationStateEnum

logger = logging.getLogger()


class ProtectionManager(ICreateProxies):
    """Protecting station against damage"""

    def __init__(
        self,
        config: protection_config_type,
        station_manager_name: str = "stat/stationmanager/1",
    ):
        self._threading = ThreadPoolExecutor(max_workers=1)
        self._cancelled = Atomic(False)
        self._future = None
        self._state = ProtectionStateEnum.OFF

        self._station_manager = create_device_proxy(
            station_manager_name, write_access=True
        )
        self._proxies: device_proxy_type = CaseInsensitiveDict()
        self._config: protection_config_type = config

    @property
    def state(self) -> ProtectionStateEnum:
        """get the current state of the protection manager"""
        return self._state

    def create_proxies(self) -> device_proxy_type:
        """Transparently wrap stateless create_proxies method"""

        return create_proxies(
            convert_protection_to_device_config(self._config), self._proxies
        )

    def evaluate(self, device_class: str, attribute_name: str, value: Any) -> bool:
        """Evaluate if the attribute value should trigger a protective shutdown"""

        result = self._config[device_class][attribute_name].evaluate(value)
        if result:
            self.protective_shutdown()
        return result

    def _transition_minus_one(self, reference_state: StationStateEnum):
        try:
            if reference_state == StationStateEnum.ON:
                self._state = ProtectionStateEnum.TRANSITIONING
                self._station_manager.station_standby()
            elif reference_state == StationStateEnum.STANDBY:
                self._state = ProtectionStateEnum.TRANSITIONING
                self._station_manager.station_hibernate()
            else:
                logger.debug("Nothing to do for state: %s", reference_state)
        except DevFailed:
            logger.exception("Executing transition raised exception")
        finally:
            self._state = ProtectionStateEnum.ACTIVE

    def _protective_shutdown(self, cancelled: Atomic):
        """Protective shutdown sequence run on separate thread

        Do not call this method directly, only call `protective_shutdown` instead
        """
        with tango.EnsureOmniThread():
            try:
                logger.info("Locking station manager due to protective shutdown")
                self._station_manager.protection_lock_RW = True
                self._state = ProtectionStateEnum.ACTIVE

                if (
                    self._station_manager.station_state_R <= StationStateEnum.HIBERNATE
                    and self._station_manager.requested_station_state_R
                    <= StationStateEnum.HIBERNATE
                ):
                    logger.info("Station in state HIBERNATE or lower, nothing to do")
                    return

                logger.info("Current state above hibernate, triggering shutdown")

                anticipated_state = self._station_manager.station_state_R
                if self._station_manager.station_state_transitioning_R:
                    logger.info(
                        "Ongoing transition to: %s, anticipating...",
                        self._station_manager.requested_station_state_R,
                    )
                    anticipated_state = self._station_manager.requested_station_state_R

                while (
                    self._station_manager.station_state_R != StationStateEnum.HIBERNATE
                    and not cancelled.value
                ):
                    self._transition_minus_one(anticipated_state)

                    while (
                        self._station_manager.station_state_transitioning_R is True
                        and not cancelled.value
                    ):
                        logger.warning(
                            "Previous state transition hasn't completed, waiting..."
                        )
                        time.sleep(2)

                    anticipated_state = self._station_manager.station_state_R

                self._state = ProtectionStateEnum.OFF
            except Exception:
                self._state = ProtectionStateEnum.FAULT
                logger.exception(
                    "Protective shutdown encountered an unexpected exception! shutdown "
                    "process can not continue"
                )
                raise
            finally:
                if cancelled.value:
                    logger.info("Aborting protective shutdown due to user request")
                    self._state = ProtectionStateEnum.ABORTED

    def abort(self):
        """Force the abortion of an ongoing protective shutdown"""
        timeout = 90  # give time for ongoing state transition to complete or timeout
        count = 0
        step = 2

        if (
            self._state == ProtectionStateEnum.OFF
            or self._state >= ProtectionStateEnum.FAULT
        ):
            logger.info("Nothing to abort")
            return

        logger.info("Requested abort of ongoing protective shutdown")
        self._cancelled.value = True

        logger.info("Waiting for protective shutdown to abort")
        while self._state != ProtectionStateEnum.ABORTED and count < timeout:
            logger.info("Waiting ...")
            count += step
            time.sleep(step)

        if count >= timeout:
            logger.warning(
                "Failed to abort ongoing protective shutdown, cleaning up threadpool"
            )
            self._threading = ThreadPoolExecutor(max_workers=1)

    def protective_shutdown(self):
        """Trigger station protection shutdown, this method is none blocking"""

        if (
            self._state == ProtectionStateEnum.OFF
            or self._state >= ProtectionStateEnum.FAULT
        ):
            logger.warning("Starting protective shutdown")
            self._state = ProtectionStateEnum.ARMED
            self._cancelled.value = False
            self._future = self._threading.submit(
                self._protective_shutdown, self._cancelled
            )
        else:
            logger.info("Protective shutdown ongoing")
