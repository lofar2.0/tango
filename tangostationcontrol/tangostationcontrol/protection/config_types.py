# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import copy
import logging

import tango
from lofar_station_client.common import CaseInsensitiveDict
from tango import DevFailed

from tangostationcontrol.common.types.device_config_types import device_config_type
from tangostationcontrol.protection.threshold import IProtectionThreshold

# Dictionary of device class keys with key value pairs of attributes and their
# protection thresholds
protection_config_type = CaseInsensitiveDict[
    str, CaseInsensitiveDict[str, IProtectionThreshold]
]

# Dictionary of device name keys and key value pairs of attributes and their
# Tango attribute dim_x indices.
protection_metric_mapping_type = CaseInsensitiveDict[str, CaseInsensitiveDict[str, int]]

logger = logging.getLogger()


def convert_protection_to_device_config(
    protection_config: protection_config_type,
) -> device_config_type:
    clone: device_config_type = copy.copy(protection_config)
    for device_class, attributes in protection_config.items():
        clone[device_class] = list(attributes.keys())

    return clone


def get_number_of_attributes(config: protection_config_type) -> int:
    """Statically count the number of attributes defined in the protection config"""

    count = 0
    for _, attributes in config.items():
        count += len(attributes)
    return count


def get_number_of_device_attribute_pairs(config: protection_config_type) -> int:
    """Dynamically determine the number of device attribute combinations from tangodb

    Makes no attempt to determine if the specified attributes for a given device exist
    """
    count = 0

    try:
        db = tango.Database()
    except DevFailed:
        logger.exception("Could not determine number of device attribute pairs")
        return 1

    for device_class, attributes in config.items():
        devices = db.get_device_exported_for_class(device_class)
        count += len(devices) * len(attributes)

    return count


def generate_device_attribute_mapping(
    config: protection_config_type,
) -> protection_metric_mapping_type:
    mapping = CaseInsensitiveDict()

    try:
        db = tango.Database()
    except DevFailed:
        logger.exception("Could not determine device attribute mapping")
        return mapping

    count = 0
    for device_class, attributes in config.items():
        devices = db.get_device_exported_for_class(device_class)
        for device_name in devices:
            mapping[device_name] = CaseInsensitiveDict()
            for attribute in attributes:
                mapping[device_name][attribute] = count
                count += 1

    return mapping
