# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Protection metric collection class"""

from datetime import datetime
import logging
from typing import List

from tangostationcontrol.common.types.device_config_types import (
    device_proxy_type,
    count_proxies_not_exceptions,
)
from tangostationcontrol.protection.config_types import protection_metric_mapping_type

logger = logging.getLogger()


class ProtectionMetrics:
    """Collecting and serving metrics about protecting station against damage"""

    def __init__(self, mapping: protection_metric_mapping_type):
        self._threshold_device_attribute = ""
        self._number_of_discovered_devices = 0
        self._number_of_connected_devices = 0
        self._device_names: List[str] = []
        self._device_attribute_mapping: protection_metric_mapping_type = mapping

        # Get the last value in the nested dictionary as it is equal the size of the
        # mapping
        if len(self._device_attribute_mapping) > 0:
            self._time_last_change_events: List[int] = [0] * (
                list(list(self._device_attribute_mapping.values())[-1].values())[-1] + 1
            )
        else:
            logger.warning("Device attribute mapping is empty")
            self._time_last_change_events: List[int] = [0]

    @property
    def number_of_discovered_devices(self) -> int:
        return self._number_of_discovered_devices

    @property
    def number_of_connected_devices(self) -> int:
        return self._number_of_connected_devices

    @property
    def time_last_change_events(self) -> List[int]:
        return self._time_last_change_events

    @property
    def threshold_device_attribute(self) -> str:
        return self._threshold_device_attribute

    def update_device_metrics(self, proxies: device_proxy_type):
        """Update statistics about device connection metrics"""

        for device_name in proxies.keys():
            if device_name not in self._device_names:
                self._device_names.append(device_name)
                self._number_of_discovered_devices += 1

        # no list needs to be maintained of connected device names as create_proxies
        # only returns new connections once per device.
        self._number_of_connected_devices += count_proxies_not_exceptions(proxies)

    def update_change_event(self, device_name: str, attribute_name: str):
        try:
            self._time_last_change_events[
                self._device_attribute_mapping[device_name][attribute_name]
            ] = int(datetime.now().timestamp())
        except IndexError:
            logger.exception(
                "Failed to update change event time for %s.%s",
                device_name,
                attribute_name,
            )

    def update_threshold_device_attribute(self, device_name: str, attribute_name: str):
        self._threshold_device_attribute = f"{device_name}.{attribute_name}"
