# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Metadata class organize gathering and managing metadata"""
from datetime import datetime, timezone
import json
from typing import Any

import numpy
from tango import DeviceProxy

from lofar_station_client.common import CaseInsensitiveDict
from tangostationcontrol.common.lofar_logging import exception_to_str
from tangostationcontrol.common.proxies.create_proxies import (
    create_proxies,
    ICreateProxies,
)
from tangostationcontrol.common.types.device_config_types import (
    device_config_type,
    device_proxy_type,
)


class MetadataOrganizer(ICreateProxies):
    """Organize maintaining of metadata"""

    def __init__(self, config: device_config_type):
        self._data = CaseInsensitiveDict()
        self._proxies: device_proxy_type = CaseInsensitiveDict()
        self._config: device_config_type = config
        self._class_device_map = CaseInsensitiveDict()

        # self.create_proxies()

    def create_proxies(self) -> device_proxy_type:
        """Transparently wrap stateless create_proxies method"""

        return create_proxies(self._config, self._proxies, self._class_device_map)

    @staticmethod
    def _is_serializable(obj: Any) -> bool:
        """Test if any object is serializable"""

        try:
            json.dumps(obj)
        except TypeError:
            return False

        return True

    @staticmethod
    def _convert_object(obj: Any) -> Any:
        """"""

        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        raise TypeError("Failed to convert object of type %s", type(obj))

    def _convert_if_necessary(self, obj: Any) -> Any:
        """"""

        if not self._is_serializable(obj):
            return self._convert_object(obj)
        return obj

    def gather_metadata(self):
        """Scrape proxies and ensure all metadata is uptodate"""

        for device_class, attributes in self._config.items():
            for device_str in self._class_device_map[device_class]:
                device_proxy = self._proxies[device_str]
                if isinstance(device_proxy, Exception):
                    self._data[device_str] = exception_to_str(device_proxy)
                    continue

                self._data[device_str] = CaseInsensitiveDict()

                for attribute in attributes:
                    try:
                        self._data[device_str][attribute] = self._convert_if_necessary(
                            getattr(device_proxy, attribute)
                        )
                    except Exception as e:
                        self._data[device_str][attribute] = exception_to_str(e)

    def partial_update(self, device: DeviceProxy, attribute_name: str, value: Any):
        """Partially update internally retained data"""

        device_str = device.name()
        self._data.setdefault(device_str, CaseInsensitiveDict())
        self._data[device_str][attribute_name] = self._convert_if_necessary(value)

    def get_json(self) -> str:
        """Convert current data to json"""
        data = {
            key: dict(value) if isinstance(value, CaseInsensitiveDict) else value
            for key, value in self._data.items()
        }

        data["ts"] = datetime.now(tz=timezone.utc).isoformat()

        return json.dumps(data)
