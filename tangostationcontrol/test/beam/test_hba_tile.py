# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from math import pi

import numpy.testing

from tangostationcontrol.beam.hba_tile import HBATAntennaOffsets

from test import base


class TestHBATAntennaOffsets(base.TestCase):
    def test_verify_CS001_HBA0(self):
        """Verify if the calculated HBAT Antenna Offsets match those calculated in LOFAR1."""

        CS001_HBA0_rotation_angle_deg = 24
        CS001_PQR_to_ETRS_rotation_matrix = numpy.array(
            [
                [-0.1195951054, -0.7919544517, 0.5987530018],
                [0.9928227484, -0.0954186800, 0.0720990002],
                [0.0000330969, 0.6030782884, 0.7976820024],
            ]
        )

        # recalculate the ITRF offsets
        ITRF_offsets = HBATAntennaOffsets.ITRF_offsets(
            HBATAntennaOffsets.HBAT1_BASE_ANTENNA_OFFSETS,
            CS001_HBA0_rotation_angle_deg * pi / 180,
            CS001_PQR_to_ETRS_rotation_matrix,
        )

        # verify against LOFAR1 (MAC/Deployment/data/StaticMetaData/iHBADeltas/CS001-iHBADeltas.conf)
        LOFAR1_CS001_HBA0_ITRF_offsets = numpy.array(
            [
                [-1.847, -1.180, 1.493],
                [-1.581, 0.003, 1.186],
                [-1.315, 1.185, 0.880],
                [-1.049, 2.367, 0.573],
                [-0.882, -1.575, 0.804],
                [-0.616, -0.393, 0.498],
                [-0.350, 0.789, 0.191],
                [-0.083, 1.971, -0.116],
                [0.083, -1.971, 0.116],
                [0.350, -0.789, -0.191],
                [0.616, 0.393, -0.498],
                [0.882, 1.575, -0.804],
                [1.049, -2.367, -0.573],
                [1.315, -1.185, -0.880],
                [1.581, -0.003, -1.186],
                [1.847, 1.180, -1.493],
            ]
        )

        numpy.testing.assert_almost_equal(
            ITRF_offsets, LOFAR1_CS001_HBA0_ITRF_offsets, decimal=3
        )
