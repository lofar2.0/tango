# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from test import base
import numpy.testing

from tangostationcontrol.beam.geo import ETRS_to_ITRF, ETRS_to_GEO, GEO_to_GEOHASH


class TestETRSToITRF(base.TestCase):
    def test_convert_single_coordinate(self):
        """Convert a single coordinate."""
        ETRS_coords = numpy.array([1.0, 1.0, 1.0])
        ITRF_coords = ETRS_to_ITRF(ETRS_coords, "ITRF2005", 2015.5)

        self.assertEqual(ETRS_coords.shape, ITRF_coords.shape)

    def test_convert_array(self):
        """Convert an array of coordinates."""
        ETRS_coords = numpy.array([[1.0, 1.0, 1.0], [2.0, 2.0, 2.0]])
        ITRF_coords = ETRS_to_ITRF(ETRS_coords, "ITRF2005", 2015.5)

        self.assertEqual(ETRS_coords.shape, ITRF_coords.shape)

    def test_verify_CS001_LBA(self):
        """Verify if the calculated CS001LBA phase center matches those calculated in LOFAR1."""

        # See CLBA in MAC/Deployment/data/Coordinates/ETRF_FILES/CS001/CS001-antenna-positions-ETRS.csv
        CS001_LBA_ETRS = [3826923.942, 460915.117, 5064643.229]

        # Convert to ITRF
        CS001_LBA_ITRF = ETRS_to_ITRF(numpy.array(CS001_LBA_ETRS), "ITRF2005", 2015.5)

        # verify against LOFAR1 (MAC/Deployment/data/StaticMetaData/AntennaFields/CS001-AntennaField.conf)
        LOFAR1_CS001_LBA_ITRF = [3826923.50275, 460915.488115, 5064643.517]

        numpy.testing.assert_almost_equal(
            CS001_LBA_ITRF, LOFAR1_CS001_LBA_ITRF, decimal=1.5
        )


class TestETRSToGEO(base.TestCase):
    def test_convert_single_coordinate(self):
        """Convert a single coordinate."""
        ETRS_coords = numpy.array([1.0, 1.0, 1.0])
        GEO_coords = ETRS_to_GEO(ETRS_coords)

        self.assertEqual((2,), GEO_coords.shape)

    def test_convert_array(self):
        """Convert an array of coordinates."""
        ETRS_coords = numpy.array([[1.0, 1.0, 1.0], [2.0, 2.0, 2.0], [3.0, 3.0, 3.0]])
        GEO_coords = ETRS_to_GEO(ETRS_coords)

        self.assertEqual((3, 2), GEO_coords.shape)

    def test_verify_CS001_LBA(self):
        """Verify if the calculated CS001LBA phase center matches those calculated in LOFAR1."""

        # See CLBA in MAC/Deployment/data/Coordinates/ETRF_FILES/CS001/CS001-antenna-positions-ETRS.csv
        CS001_LBA_ETRS = [3826923.942, 460915.117, 5064643.229]

        # Convert to GEO
        CS001_LBA_GEO = ETRS_to_GEO(numpy.array(CS001_LBA_ETRS))

        # verify against actual position
        LOFAR1_CS001_LBA_GEO = [52.911, 6.868]

        numpy.testing.assert_almost_equal(
            CS001_LBA_GEO, LOFAR1_CS001_LBA_GEO, decimal=3
        )


class TestGEOToGEOHASH(base.TestCase):
    def test_convert_single_coordinate(self):
        """Convert a single coordinate."""
        GEO_coords = numpy.array([1.0, 1.0])
        GEOHASH_coords = GEO_to_GEOHASH(GEO_coords)

        self.assertEqual(str, type(GEOHASH_coords))

    def test_convert_array(self):
        """Convert an array of coordinates."""
        GEO_coords = numpy.array([[1.0, 1.0], [2.0, 2.0], [3.0, 3.0]])
        GEOHASH_coords = GEO_to_GEOHASH(GEO_coords)

        self.assertEqual((3,), GEOHASH_coords.shape)

    def test_CS001_LBA_regression(self):
        """Verify if the calculated CS001LBA phase center match fixed values, to detect changes in computation."""

        CS001_LBA_GEO = [52.911, 6.868]

        # Convert to GEO
        CS001_LBA_GEOHASH = GEO_to_GEOHASH(numpy.array(CS001_LBA_GEO))

        # verify against precomputed value
        self.assertEqual("u1kvh21hgvrcpm28", CS001_LBA_GEOHASH)
