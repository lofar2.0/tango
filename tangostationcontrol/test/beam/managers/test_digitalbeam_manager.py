#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
import datetime
from unittest.mock import MagicMock, patch

import numpy.testing

from tangostationcontrol.beam.managers import DigitalBeamManager
from tangostationcontrol.common import device_decorators
from tangostationcontrol.common.constants import (
    N_elements,
    N_beamlets_ctrl,
    A_pn,
    N_pn,
    N_point_prop,
)
from test import base


class TestDigitalBeamManager(base.TestCase):
    @patch.object(device_decorators, "get_current_device")
    def test_delays(self, mock_get_current_device):
        dt = datetime.datetime.now()

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.metric_labels = {"foo": "bar"}

        mock_get_current_device.return_value = device_mock

        sut = DigitalBeamManager(device_mock)

        sut.delay_calculator = MagicMock()
        sut.delay_calculator.delays_bulk.return_value = numpy.full(
            N_elements, 8, dtype=numpy.float64
        )
        sut.relative_antenna_positions = "ant positions"

        pointings = [i**2 for i in range(10)]

        # verify delays method returns the correct dimensions
        delays = sut.delays(pointings, dt).flatten()
        numpy.testing.assert_array_equal(
            delays, numpy.full(N_elements, 8, dtype=numpy.float64)
        )
        sut.delay_calculator.set_measure_time.assert_called_with(dt)
        sut.delay_calculator.delays_bulk.assert_called_with(pointings, "ant positions")

    @patch.object(device_decorators, "get_current_device")
    def test_compute_weights(self, mock_get_current_device):
        dt = datetime.datetime.now()
        pointing_direction = numpy.full((N_beamlets_ctrl, N_point_prop), 1)

        def read_parent_attribute(attr):
            match attr:
                case "Antenna_to_SDP_Mapping_R":
                    return [[0, 0], [0, 1], [0, 2]]
                case "Antenna_Usage_Mask_R":
                    return [True, False, True]

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.metric_labels = {"foo": "bar"}

        mock_get_current_device.return_value = device_mock

        device_mock.read_Antenna_Mask_R = MagicMock(return_value=([True, True, False]))

        device_mock.control = MagicMock()
        device_mock.control.read_parent_attribute = MagicMock()
        device_mock.control.read_parent_attribute.side_effect = read_parent_attribute

        device_mock.beamlet = MagicMock()
        device_mock.beamlet.calculate_bf_weights = MagicMock()
        device_mock.beamlet.calculate_bf_weights.return_value = numpy.full(
            (N_pn, A_pn, N_beamlets_ctrl), 7
        )

        sut = DigitalBeamManager(device_mock)
        sut.delays = MagicMock(return_value=numpy.full((3, 1), 2))

        beam_weights = sut.compute_weights(pointing_direction, dt)

        numpy.testing.assert_array_equal(
            beam_weights,
            numpy.array(
                [
                    numpy.array(
                        [
                            numpy.full(N_beamlets_ctrl, 7),
                            numpy.full(N_beamlets_ctrl, 0),
                            numpy.full(N_beamlets_ctrl, 0),
                            numpy.full(N_beamlets_ctrl, 7),
                            numpy.full(N_beamlets_ctrl, 7),
                            numpy.full(N_beamlets_ctrl, 7),
                        ]
                    )
                ]
                + ([numpy.full((A_pn, N_beamlets_ctrl), 7)] * 15)
            ),
        )

    @patch.object(device_decorators, "get_current_device")
    def test_apply_weights(self, mock_get_current_device):
        dt = datetime.datetime.now()
        pointing_direction = numpy.array([1])
        beam_weights = numpy.full((N_pn, A_pn, N_beamlets_ctrl), 1)

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.metric_labels = {"foo": "bar"}

        mock_get_current_device.return_value = device_mock

        sut = DigitalBeamManager(device_mock)
        sut.beam_tracking_application_offset = 0.05
        sut.current_pointing_timestamp = numpy.zeros(1, dtype=numpy.double)
        sut.current_pointing_error = numpy.zeros(1, dtype=numpy.double)

        with patch(
            "tangostationcontrol.beam.managers._digitalbeam.datetime", wraps=datetime
        ) as mock:
            mock.datetime.now.return_value = dt + datetime.timedelta(0, 1)
            sut.apply_weights(pointing_direction, dt, beam_weights)

        numpy.testing.assert_array_equal(
            sut.current_pointing_direction, pointing_direction
        )
        numpy.testing.assert_array_equal(
            sut.current_pointing_timestamp, numpy.full(1, dt.timestamp())
        )
        numpy.testing.assert_array_equal(
            sut.current_pointing_error, numpy.full(1, 1.05)
        )


class TestDigitalBeamManagerAntennasUsed(base.TestCase):
    def _test_antennas_used(
        self,
        antenna_to_sdp_mapping: list[list[int]],
        antenna_usage_mask: list[bool],
        antenna_mask: list[bool],
        expected_antennas_used: list[bool],
    ):
        """Generic test function."""

        def read_parent_attribute(attr):
            match attr:
                case "Antenna_to_SDP_Mapping_R":
                    return antenna_to_sdp_mapping
                case "Antenna_Usage_Mask_R":
                    return antenna_usage_mask

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.read_Antenna_Mask_R = MagicMock(return_value=antenna_mask)

        device_mock.control = MagicMock()
        device_mock.control.read_parent_attribute = MagicMock()
        device_mock.control.read_parent_attribute.side_effect = read_parent_attribute

        sut = DigitalBeamManager(device_mock)
        self.assertListEqual(sut.antennas_used(), expected_antennas_used)

    def test_antennas_used_single_antenna(self):
        # Test simple case: single antenna
        self._test_antennas_used(
            antenna_to_sdp_mapping=[[0, 0]],
            antenna_usage_mask=[True],
            antenna_mask=[True],
            expected_antennas_used=[True],
        )

    def test_antennas_used_unconnected_antenna(self):
        # Exclude antenna because it is not connected
        self._test_antennas_used(
            antenna_to_sdp_mapping=[[0, -1]],
            antenna_usage_mask=[True],
            antenna_mask=[True],
            expected_antennas_used=[False],
        )

    def test_antennas_used_unusable_antenna(self):
        # Exclude antenna because it is not to be used
        self._test_antennas_used(
            antenna_to_sdp_mapping=[[0, 0]],
            antenna_usage_mask=[False],
            antenna_mask=[True],
            expected_antennas_used=[False],
        )

    def test_antennas_used_unselected_antenna(self):
        # Exclude antenna because it is not in the selected antenna set
        self._test_antennas_used(
            antenna_to_sdp_mapping=[[0, 0]],
            antenna_usage_mask=[True],
            antenna_mask=[False],
            expected_antennas_used=[False],
        )

    def test_antennas_used_multiple_antennas(self):
        # All combinations in one go to test multiple antennas
        self._test_antennas_used(
            antenna_to_sdp_mapping=[[0, 0], [0, 1], [0, 2], [0, -1]],
            antenna_usage_mask=[True, False, True, True],
            antenna_mask=[True, True, False, True],
            expected_antennas_used=[True, False, False, False],
        )
