#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import datetime
from unittest.mock import MagicMock, patch, call
from test import base
import numpy.testing

from tangostationcontrol.beam.managers import TileBeamManager, _tilebeam
from tangostationcontrol.common import device_decorators
from tangostationcontrol.common.constants import (
    DEFAULT_N_HBA_TILES,
    N_elements,
    N_point_prop,
    N_pol,
)


class TestTileBeamManager(base.TestCase):
    """Test class for TileBeam manager"""

    @patch.object(device_decorators, "get_current_device")
    def test_delays(self, mock_get_current_device):
        """Verify delays are retrieved with correct dimensions"""
        _dt = datetime.datetime.now()

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.metric_labels = {"foo": "bar"}

        mock_get_current_device.return_value = device_mock

        sut = TileBeamManager(device_mock)

        sut.nr_tiles = DEFAULT_N_HBA_TILES
        sut.delay_calculator = MagicMock()
        sut.delay_calculator.delays.side_effect = [
            numpy.full(N_elements, i, dtype=numpy.float64)
            for i in range(DEFAULT_N_HBA_TILES)
        ]

        sut.HBAT_reference_positions = numpy.array(
            [[-x] for x in range(DEFAULT_N_HBA_TILES, 0, -1)]
        )
        sut.HBAT_antenna_positions = numpy.array(
            [[x] for x in range(DEFAULT_N_HBA_TILES, 0, -1)]
        )

        pointings = [i**2 for i in range(DEFAULT_N_HBA_TILES)]

        # verify delays method returns the correct dimensions
        delays = sut.delays(pointings, _dt).flatten()
        numpy.testing.assert_array_equal(
            delays,
            numpy.concatenate(
                [
                    numpy.full(N_elements, i, dtype=numpy.float64)
                    for i in range(DEFAULT_N_HBA_TILES)
                ]
            ),
        )

        for i in range(DEFAULT_N_HBA_TILES):
            sut.delay_calculator.set_measure_time.assert_has_calls([call(_dt)])
            sut.delay_calculator.delays.assert_has_calls(
                [call(pointings[i], (DEFAULT_N_HBA_TILES - i) * 2)]
            )

    @patch.object(_tilebeam, "create_device_proxy")
    @patch.object(device_decorators, "get_current_device")
    def test_compute_weights(self, mock_get_current_device, mock_create_device_proxy):
        """Verify computed weights are retrieved correctly"""
        _dt = datetime.datetime.now()
        pointing_direction = numpy.full(1, 1)

        def read_parent_attribute(attr):
            match attr:
                case "Control_to_RECV_mapping_R":
                    return numpy.array([[1, x] for x in range(0, DEFAULT_N_HBA_TILES)])
                case "RECV_Devices_R":
                    return [recv_mock.name]

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.metric_labels = {"foo": "bar"}

        mock_get_current_device.return_value = device_mock

        device_mock.control = MagicMock()
        device_mock.control.read_parent_attribute = MagicMock()
        device_mock.control.read_parent_attribute.side_effect = read_parent_attribute
        device_mock.control.branch_children_names = MagicMock(
            return_value=["1", "2", "3"]
        )
        device_mock.control.branch_child = MagicMock()
        recv_mock = MagicMock()
        recv_mock.calculate_HBAT_bf_delay_steps = MagicMock()
        recv_mock.calculate_HBAT_bf_delay_steps.side_effect = lambda a: [a] * 2
        device_mock.control.branch_child.return_value = recv_mock

        mock_create_device_proxy.return_value = recv_mock

        sut = TileBeamManager(device_mock)
        sut.nr_tiles = DEFAULT_N_HBA_TILES
        sut.delays = MagicMock(
            return_value=numpy.array(
                [numpy.full(N_elements, i) for i in range(DEFAULT_N_HBA_TILES)]
            )
        )

        bf_delay_steps = sut.compute_weights(pointing_direction, _dt)

        numpy.testing.assert_array_equal(
            bf_delay_steps,
            numpy.concatenate(
                [numpy.full(N_elements, i) for i in range(DEFAULT_N_HBA_TILES)] * N_pol
            ),
        )

    @patch.object(_tilebeam, "create_device_proxy")
    @patch.object(device_decorators, "get_current_device")
    def test_apply_weights(self, mock_get_current_device, mock_create_device_proxy):
        """Verify beam weights are applied correctly"""
        _dt = datetime.datetime.now()
        pointing_direction = numpy.array(range(DEFAULT_N_HBA_TILES))
        bf_delay_steps = numpy.concatenate(
            [numpy.full(N_elements, i) for i in range(DEFAULT_N_HBA_TILES)] * N_pol
        )

        def read_parent_attribute(attr):
            match attr:
                case "Control_to_RECV_mapping_R":
                    return numpy.array([[1, x] for x in range(0, DEFAULT_N_HBA_TILES)])
                case "ANT_mask_RW":
                    return [True, False] * (DEFAULT_N_HBA_TILES // 2)
                case "RECV_Devices_R":
                    return [recv_mock.name]

        device_mock = MagicMock()
        device_mock.get_name = MagicMock(return_value="domain/family/member")
        device_mock.metric_labels = {"foo": "bar"}

        mock_get_current_device.return_value = device_mock

        device_mock.control = MagicMock()
        device_mock.control.read_parent_attribute = MagicMock()
        device_mock.control.read_parent_attribute.side_effect = read_parent_attribute
        device_mock.control.branch_children_names = MagicMock(
            return_value=["1", "2", "3"]
        )
        device_mock.control.branch_child = MagicMock()
        recv_mock = MagicMock()
        recv_mock.calculate_HBAT_bf_delay_steps = MagicMock()
        recv_mock.calculate_HBAT_bf_delay_steps.side_effect = lambda a: [a] * 2
        device_mock.control.branch_child.return_value = recv_mock
        mock_create_device_proxy.return_value = recv_mock

        # Tilebeam configuration
        sut = TileBeamManager(device_mock)
        sut.nr_tiles = DEFAULT_N_HBA_TILES
        sut.current_pointing_timestamp = numpy.zeros(
            DEFAULT_N_HBA_TILES, dtype=numpy.double
        )
        sut.current_pointing_direction = numpy.zeros(
            (DEFAULT_N_HBA_TILES, N_point_prop), dtype="<U32"
        )
        sut.current_pointing_error = numpy.zeros(
            DEFAULT_N_HBA_TILES, dtype=numpy.double
        )
        sut.beam_tracking_application_offset = 0.05

        # Apply operation
        sut.apply_weights(pointing_direction, _dt, bf_delay_steps)

        # Test results
        numpy.testing.assert_array_equal(
            sut.current_pointing_timestamp,
            numpy.array([_dt.timestamp(), 0] * (DEFAULT_N_HBA_TILES // 2)),
        )
        numpy.testing.assert_array_equal(
            sut.current_pointing_direction,
            numpy.concatenate(
                [
                    numpy.array([[i, i, i], ["", "", ""]])
                    for i in range(0, DEFAULT_N_HBA_TILES, 2)
                ]
            ),
        )
