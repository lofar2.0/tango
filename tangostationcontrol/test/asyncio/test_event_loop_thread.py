# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
from unittest.mock import MagicMock

from tangostationcontrol.asyncio import (
    EventLoopThread,
)

from test import base


class TestEventLoopThread(base.TestCase):
    def test_clean_construct_teardown(self):
        """Whether the EventLoopThread cleanly constructs and tears down"""

        elt = EventLoopThread()
        elt.stop()

    def test_call_soon_threadsafe(self):
        """Whether the call_soon_threadsafe function indeed executes the given function."""

        # a callback that tracks how it was called
        callback = MagicMock()

        elt = EventLoopThread()
        handle = elt.call_soon_threadsafe(callback)
        elt.stop()

        # check that the callback was indeed called
        callback.assert_called()

    def test_run_coroutine_threadsafe(self):
        """Whether the run_coroutine_threadsafe function indeed executes the given function."""

        async def callback():
            # make sure we yield to force asyncio magic
            await asyncio.sleep(0.0)

            return 42

        elt = EventLoopThread()
        future = elt.run_coroutine_threadsafe(callback())
        self.assertEqual(42, future.result())
        elt.stop()
