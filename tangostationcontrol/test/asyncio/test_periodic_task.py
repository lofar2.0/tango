# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.asyncio import (
    EventLoopThread,
    PeriodicTask,
)

from test import base


class TestPeriodicTask(base.TestCase):
    def test(self):
        """Test whether PeriodicTask calls the given function repeatingly."""

        call_counter = [0]

        async def callback():
            call_counter[0] += 1
            if call_counter[0] >= 5:
                pt.cancel()
            return

        elt = EventLoopThread()
        pt = PeriodicTask(elt.event_loop, callback, interval=0.01)

        # callback cancels the future, so if future is cancelled,
        # we know the callback was indeed called often enough.
        pt.join()

        self.assertEqual(5, call_counter[0])

        pt.stop()
        elt.stop()
