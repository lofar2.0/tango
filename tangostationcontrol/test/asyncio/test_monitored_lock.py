# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
from prometheus_client import generate_latest

import asyncio

from tangostationcontrol.asyncio import (
    MonitoredLock,
)

from test import base


class TestMonitoredLock(base.TestCase):
    def test_locks(self):
        """Test basic locking functionality."""

        lock = MonitoredLock("my_lock", {"foo": "bar"})

        async def test_fun():
            self.assertFalse(lock.locked())

            async with lock:
                self.assertTrue(lock.locked())

            self.assertFalse(lock.locked())

        asyncio.run(test_fun())

    def test_metrics(self):
        """Test whether MonitoredLock exposes metrics."""

        lock = MonitoredLock("my_lock", {"foo": "bar"})

        async def test_fun():
            async with lock:
                pass

        # generate some metrics
        asyncio.run(test_fun())

        # check for metrics
        metric_output = generate_latest().decode()
        self.assertTrue(
            any(
                [
                    metric.startswith("ds_duration_my_lock_acquire_bucket")
                    for metric in metric_output.split()
                ]
            ),
            msg=metric_output,
        )
        self.assertTrue(
            any(
                [
                    metric.startswith("ds_duration_my_lock_locked_bucket")
                    for metric in metric_output.split()
                ]
            ),
            msg=metric_output,
        )
