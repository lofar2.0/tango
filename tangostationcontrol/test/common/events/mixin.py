# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from threading import Semaphore
import logging

from tangostationcontrol.common.events import EventSubscriptions

logger = logging.getLogger()


class EventSubscriptionMixin:
    def setUp(self):
        self.events = EventSubscriptions()

        self.callback_called_values = []

        # tokens are inserted on each call. start with 0.
        self.callback_call_counter = Semaphore(0)

    def tearDown(self):
        self.events.unsubscribe_all()

    def callback(self, device, attribute_name, value):
        logger.info(f"Callback received: {attribute_name} := {value}")
        self.callback_called_values.append((attribute_name, value))

        # another token is added
        self.callback_call_counter.release()

    def wait_for_callback(self, timeout: float = 2.0):
        self.assertTrue(self.callback_call_counter.acquire(timeout=timeout))

    def wait_for_no_callback(self, timeout: float = 2.0):
        self.assertFalse(self.callback_call_counter.acquire(timeout=timeout))
