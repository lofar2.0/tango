# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from unittest import mock
from unittest.mock import call

import tango
from lofar_station_client.common import CaseInsensitiveDict
from tango import DeviceProxy, DevFailed

from tangostationcontrol.common.constants import DEFAULT_POLLING_PERIOD_METADATA_MS
from tangostationcontrol.common.events import EventSubscriptions
from tangostationcontrol.common.events import register_subscriptions

from test import base

logger = logging.getLogger()


class TestRegisterSubscriptions(base.TestCase):

    def setUp(self):
        super(TestRegisterSubscriptions, self).setUp()

        self.m_proxy = self.proxy_patch(tango, "DeviceProxy", spec=tango.DeviceProxy)

    @staticmethod
    def callback(proxy: DeviceProxy, name: str, data: object):
        pass

    def register_change_event_test_base(self, subscription_mock):
        self.m_proxy["mock"].return_value.info.return_value = mock.Mock(dev_class="AFH")

        test_config = CaseInsensitiveDict(
            {"AFH": ["Test_attribute_R", "Test_attribute_2_R"]}
        )

        m_proxy = self.m_proxy["object"]()
        m_proxy_config = CaseInsensitiveDict({"AFH": m_proxy})

        register_subscriptions.register_change_event_subscriptions(
            test_config, m_proxy_config, subscription_mock, self.callback
        )

        call1 = call(
            m_proxy,
            test_config["AFH"][0],
            self.callback,
            DEFAULT_POLLING_PERIOD_METADATA_MS,
        )
        call2 = call(
            m_proxy,
            test_config["AFH"][1],
            self.callback,
            DEFAULT_POLLING_PERIOD_METADATA_MS,
        )
        subscription_mock.subscribe_change_event.assert_has_calls([call1, call2])

    def test_register_change_event_subscriptions(self):
        """Test registering change event subscriptions"""

        m_subscriptions = mock.Mock(spec=EventSubscriptions)
        self.register_change_event_test_base(m_subscriptions)

    def test_register_change_event_subscriptions_exception(self):
        """Test registering change event subscriptions robustness against exceptions"""

        m_subscriptions = mock.Mock(spec=EventSubscriptions)
        m_subscriptions.subscribe_change_event.side_effect = [Exception("error")]
        self.register_change_event_test_base(m_subscriptions)

    def test_register_change_event_subscription_proxy_exception(self):
        """Test registering change event is skipped for failed device proxies"""

        m_subscriptions = mock.Mock(spec=EventSubscriptions)

        test_config = CaseInsensitiveDict(
            {"AFH": ["Test_attribute_R", "Test_attribute_2_R"]}
        )

        m_proxy = DevFailed()
        m_proxy_config = CaseInsensitiveDict({"AFH": m_proxy})

        register_subscriptions.register_change_event_subscriptions(
            test_config, m_proxy_config, m_subscriptions, self.callback
        )

        self.assertEqual(0, m_subscriptions.subscribe_change_event.call_count)
