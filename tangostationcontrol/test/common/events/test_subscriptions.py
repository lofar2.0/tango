# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from unittest.mock import patch

from test.common.events.mixin import EventSubscriptionMixin

from tango.server import Device, attribute, AttrWriteType, command
from tango.test_context import DeviceTestContext

from test import base


class TestEventSubscriptions(EventSubscriptionMixin, base.TestCase):
    class TestDevice(Device):
        """A device with a writable and polled attribute, needed for generating change events."""

        A = attribute(
            dtype=int,
            access=AttrWriteType.READ_WRITE,
            polling_period=1000,
            abs_change="1",
        )

        def init_device(self):
            super().init_device()

            self._A = 0

        def read_A(self):
            return self._A

        def write_A(self, value):
            self._A = value

        @command(dtype_in=str, dtype_out=bool)
        def is_attribute_polled_by_lofardevice(self, attr_name):
            # used by EventSubscriptions. we poll nothing ourselves
            return False

    def test_initial_event(self):
        """Do we receive the initial event emitted when subscribing?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(proxy, "A", self.callback)

            # wait for change events
            self.wait_for_callback()

            # check call sequence
            self.assertListEqual([("A", 0)], self.callback_called_values)

    def test_is_subscribed(self):
        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            self.assertFalse(self.events.is_subscribed(proxy.name(), "A"))

            self.events.subscribe_change_event(proxy, "A", self.callback)

            self.assertTrue(self.events.is_subscribed(proxy.name(), "A"))

            self.events.unsubscribe_all()

            self.assertFalse(self.events.is_subscribed(proxy.name(), "A"))

    def test_confirm(self):
        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            self.events.subscribe_change_event(proxy, "A", self.callback, confirm=True)

            self.assertTrue(self.events.is_subscribed(proxy.name(), "A"))

            self.events.unsubscribe_all()

    @patch("tango.DeviceProxy.subscribe_event")
    @patch("tango.DeviceProxy.unsubscribe_event")
    def test_confirm_timeout(self, m_unsubscribe_event, m_subscribe_event):
        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            with self.assertRaises(TimeoutError):
                self.events.subscribe_change_event(
                    proxy, "A", self.callback, confirm=True
                )

        # explicitly unsubscribe using the mocked unsubscribe_event
        self.events.unsubscribe_all()

    def test_change_event(self):
        """Do we also receive an event emitted after the value changes (plus the initial)?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(proxy, "A", self.callback)

            # change (triggering a change event)
            proxy.A = 42

            # wait for 2 change events
            self.wait_for_callback()
            self.wait_for_callback()

            # check call sequence
            self.assertListEqual([("A", 0), ("a", 42)], self.callback_called_values)

    def test_multiple_callbacks(self):
        """Can we register multiple callbacks on the same attribute?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(proxy, "A", self.callback)

            # subscribe again (triggering a change event)
            self.events.subscribe_change_event(proxy, "A", self.callback)

            # change (triggering a change event)
            proxy.A = 42

            # wait for 4 change events
            self.wait_for_callback()
            self.wait_for_callback()
            self.wait_for_callback()
            self.wait_for_callback()

            # check call sequence
            self.assertListEqual(
                [("A", 0), ("A", 0), ("a", 42), ("a", 42)], self.callback_called_values
            )

    def test_unsubscribe(self):
        """Do we stop receiving if we unsubscribe?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(proxy, "A", self.callback)

            # unsubscribe
            self.events.unsubscribe_all()

            # change (triggering a change event, but aren't subscribed)
            proxy.A = 42

            # wait for 1 change event
            self.wait_for_callback()

            # fail to wait for another change event
            self.wait_for_no_callback()

            # check call sequence
            self.assertListEqual([("A", 0)], self.callback_called_values)

    def test_event_errors(self):
        """Are events describing errors handled?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(proxy, "A", self.callback)

            # wait for 1 change event
            self.wait_for_callback()

            # stop polling attribute! this emits an event error
            proxy.stop_poll_attribute("A")

            # change the value anyway, this should not result in a change event
            proxy.A = 42

            # fail to wait for another change event
            self.wait_for_no_callback()

            # check if error registered
            metric = self.events.error_count_metric.metric.collect()[0]
            self.assertEqual(1.0, metric.samples[0].value)
            self.assertDictEqual(
                {"event_device": "test/nodb/testdevice", "event_attribute": "None"},
                metric.samples[0].labels,
            )
