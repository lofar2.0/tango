# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from test.common.events.mixin import EventSubscriptionMixin
from tangostationcontrol.common.events import (
    ChangeEvents,
)

from tango.server import Device, attribute, AttrWriteType, command
from tango.test_context import DeviceTestContext
from tango import DevFailed

from test import base


class TestChangeEvents(EventSubscriptionMixin, base.TestCase):
    class TestDevice(Device):
        """A device with a writable attribute, needed for generating change events."""

        bool_attr = attribute(
            dtype=bool,
            access=AttrWriteType.READ_WRITE,
        )

        int_attr = attribute(
            dtype=int,
            access=AttrWriteType.READ_WRITE,
        )

        def init_device(self):
            super().init_device()

            self._bool = False
            self._int = 0
            self.custom_change_events = ChangeEvents(self)

            self.custom_change_events.configure_attribute("bool_attr")
            self.custom_change_events.configure_attribute("int_attr")

        def read_bool_attr(self):
            return self._bool

        def write_bool_attr(self, value):
            self._bool = value
            self.custom_change_events.send_change_event("bool_attr", value)

        def read_int_attr(self):
            return self._int

        def write_int_attr(self, value):
            self._int = value
            self.custom_change_events.send_change_event("int_attr", value)

        @command(dtype_in=str, dtype_out=bool)
        def is_attribute_polled_by_lofardevice(self, attr_name):
            # used by EventSubscriptions. we poll everything ourselves
            return True

    def test_bool_change_event(self):
        """Do we also receive an event emitted after the value changes (plus the initial)?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(
                proxy, "bool_attr", self.callback, confirm=True
            )

            # change (triggering a change event)
            proxy.bool_attr = True

            # wait for 2 change events
            self.wait_for_callback()
            self.wait_for_callback()

            # check call sequence
            self.assertListEqual(
                [("bool_attr", False), ("bool_attr", True)], self.callback_called_values
            )

    def test_int_change_event(self):
        """Do we also receive an event emitted after the value changes (plus the initial)?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(
                proxy, "int_attr", self.callback, confirm=True
            )

            # change (triggering a change event)
            proxy.int_attr = 42

            # wait for 2 change events
            self.wait_for_callback()
            self.wait_for_callback()

            # check call sequence
            self.assertListEqual(
                [("int_attr", 0), ("int_attr", 42)], self.callback_called_values
            )

    def test_no_change_event(self):
        """Do we also receive no event emitted when the value does not change when set (except the initial)?"""

        with DeviceTestContext(self.TestDevice, properties={}, process=True) as proxy:
            # subscribe (triggering a change event)
            self.events.subscribe_change_event(
                proxy, "int_attr", self.callback, confirm=True
            )

            # initial set (triggering a change event)
            proxy.int_attr = 0

            # wait for 2 change events
            self.wait_for_callback()
            self.wait_for_callback()

            # second set to the same value (triggering no change event)
            proxy.int_attr = 0

            # this should not result in a change event
            self.wait_for_no_callback()

    def test_no_change_event_for_polled_attributes(self):
        """Test whether we allow push_change_event for attributes polled by Tango."""

        class PolledTestDevice(self.TestDevice):
            @attribute(dtype=int, polling_period=1000)
            def polled_attr(self):
                return 42

            def init_device(self):
                super().init_device()

                self.custom_change_events.configure_attribute("polled_attr")

            @command()
            def generate_change_event(self):
                self.custom_change_events.send_change_event("polled_attr", 42)

        with DeviceTestContext(PolledTestDevice, properties={}, process=True) as proxy:
            # try to send a change event
            with self.assertRaises(DevFailed):
                proxy.generate_change_event()
