# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import numpy
from tango.utils import is_seq

from tangostationcontrol.common import type_checking

from test import base


class TestTypeChecking(base.TestCase):
    @staticmethod
    def subscriptable(obj):
        return hasattr(obj, "__getitem__")

    @staticmethod
    def iterable(obj):
        return hasattr(obj, "__iter__")

    @staticmethod
    def positional_ordering(obj):
        try:
            obj[0]
            return True
        except Exception:
            return False

    def sequence_test(self, obj):
        """Test object is sequence based on properties and verify is_sequence

        Recover alternative is_sequence method from commit 73177e9 if tests fail
        """

        result = (
            self.subscriptable(obj) & self.iterable(obj) & self.positional_ordering(obj)
        )

        self.assertEqual(result, is_seq(obj), f"Test failed for type {type(obj)}")

    def test_is_sequence_for_types(self):
        """Types to be tested by is_sequence

        Recover alternative is_seq method from commit 73177e9 if tests fail
        """

        test_types = [
            (False,),
            {"test": "test"},
            [False],
            {"test"},
            numpy.array([1, 2, 3]),
        ]

        for test in test_types:
            self.sequence_test(test)

    def test_is_sequence_not_str(self):
        """Types test for sequence_not_str, must be false"""

        t_bytearray = bytearray([0, 5, 255])
        test_types = [str(""), bytes(t_bytearray), t_bytearray]

        for test in test_types:
            self.assertFalse(type_checking.sequence_not_str(test))

    def test_type_not_sequence(self):
        test = [str]
        self.assertFalse(type_checking.type_not_sequence(test))
        self.assertTrue(type_checking.type_not_sequence(test[0]))
