# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
import time

from tangostationcontrol.common.device_decorators import (
    suppress_exceptions,
    DurationStatistics,
    log_exceptions,
)

from test import base


class TestSuppressExceptions(base.TestCase):
    def test_sync(self):
        call_count = [0]

        @suppress_exceptions()
        def func_raising_exception(device, call_count):
            call_count[0] += 1
            raise Exception("exception")

        # this should NOT raise
        func_raising_exception(None, call_count)

        # function should have been called
        self.assertEqual(1, call_count[0])

        # exception should have been recorded
        self.assertEqual(1, len(func_raising_exception.exceptions))

    def test_async(self):
        call_count = [0]

        @suppress_exceptions()
        async def func_raising_exception(device, call_count):
            # make sure we yield, forcing a full async run
            # of this function before the exception is raised
            await asyncio.sleep(0)

            call_count[0] += 1
            raise Exception("exception")

        # this should NOT raise
        asyncio.run(func_raising_exception(None, call_count))

        # function should have been called
        self.assertEqual(1, call_count[0])

        # exception should have been recorded
        self.assertEqual(1, len(func_raising_exception.exceptions))


class TestDurationStatistics(base.TestCase):
    KEY = 42

    def test_sync(self):
        @DurationStatistics()
        def func(key):
            time.sleep(0.1)

        for _ in range(3):
            func(self.KEY)

        statistics = func.get_statistic(self.KEY)

        # verify statistics got recorded
        self.assertEqual(3, statistics["count"])
        self.assertEqual(3, len(statistics["history"]))

        # verify statistics caught the actual call duration
        self.assertAlmostEqual(0.1, statistics["history"][0], places=2)
        self.assertAlmostEqual(0.1, statistics["history"][1], places=2)
        self.assertAlmostEqual(0.1, statistics["history"][2], places=2)

    def test_async(self):
        @DurationStatistics()
        async def func(key):
            await asyncio.sleep(0.1)

        for _ in range(3):
            asyncio.run(func(self.KEY))

        statistics = func.get_statistic(self.KEY)

        # verify statistics got recorded
        self.assertEqual(3, statistics["count"])
        self.assertEqual(3, len(statistics["history"]))

        # verify statistics caught the actual call duration
        self.assertAlmostEqual(0.1, statistics["history"][0], places=2)
        self.assertAlmostEqual(0.1, statistics["history"][1], places=2)
        self.assertAlmostEqual(0.1, statistics["history"][2], places=2)


class TestLogExceptions(base.TestCase):
    """Test class for logging methods"""

    def setUp(self):
        super().setUp()

        # reset logging system
        root_logger = logging.getLogger()
        root_logger.filters = []
        root_logger.handlers = []
        root_logger.manager.loggerDict = {}

        # record everything we log in memory so we can inspect it
        class MemoryHandler(logging.Handler):
            """Handler that provides access to the records emitted."""

            def __init__(self):
                super().__init__()
                self.records = []

            def emit(self, record):
                self.records.append(record)

        self.memory_handler = MemoryHandler()
        root_logger.addHandler(self.memory_handler)

    def test(self):
        """Test whether log_exceptions actually logs and reraises exceptions."""

        class Foo:
            @log_exceptions()
            def exceptionalFunction(self):
                raise RuntimeError("test")

        with self.assertRaises(
            RuntimeError, msg="log_exceptions did not reraise exception"
        ):
            f = Foo()
            f.exceptionalFunction()

        self.assertEqual(
            1,
            len(self.memory_handler.records),
            msg="log_exceptions did not log exception",
        )
