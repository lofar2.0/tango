# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import numpy

from tangostationcontrol.common.constants import CLK_200_MHZ, CLK_160_MHZ
from tangostationcontrol.common.sdp import (
    phases_to_weights,
    subband_frequencies,
    subband_frequency,
    weight_to_complex,
    complex_to_weights,
)

from test import base


class TestSDPCommon(base.TestCase):
    UNIT_WEIGHT = 8192

    def test_subband_frequencies(self):
        subbands = numpy.array(
            [
                [0, 1, 102],
            ]
        )

        # for reference values, see https://proxy.lofar.eu/rtsm/tests/

        lba_frequencies = subband_frequencies(subbands, CLK_160_MHZ, 0, False)
        self.assertAlmostEqual(lba_frequencies[0][0], 0.0000000e6)
        self.assertAlmostEqual(lba_frequencies[0][1], 0.1562500e6)
        self.assertAlmostEqual(lba_frequencies[0][2], 15.9375000e6)

        lba_frequencies = subband_frequencies(subbands, CLK_200_MHZ, 0, False)
        self.assertAlmostEqual(lba_frequencies[0][0], 0.0000000e6)
        self.assertAlmostEqual(lba_frequencies[0][1], 0.1953125e6)
        self.assertAlmostEqual(lba_frequencies[0][2], 19.9218750e6)

        # Nyquist zone 1 is not used in 160 MHz

        hba_low_frequencies = subband_frequencies(subbands, CLK_200_MHZ, 1, True)
        self.assertAlmostEqual(hba_low_frequencies[0][0], 100.0000000e6)
        self.assertAlmostEqual(hba_low_frequencies[0][1], 100.1953125e6)
        self.assertAlmostEqual(hba_low_frequencies[0][2], 119.9218750e6)

        hba_high_frequencies = subband_frequencies(subbands, CLK_160_MHZ, 2, False)
        self.assertAlmostEqual(hba_high_frequencies[0][0], 160.0000000e6)
        self.assertAlmostEqual(hba_high_frequencies[0][1], 160.1562500e6)
        self.assertAlmostEqual(hba_high_frequencies[0][2], 175.9375000e6)

        hba_high_frequencies = subband_frequencies(subbands, CLK_200_MHZ, 2, False)
        self.assertAlmostEqual(hba_high_frequencies[0][0], 200.0000000e6)
        self.assertAlmostEqual(hba_high_frequencies[0][1], 200.1953125e6)
        self.assertAlmostEqual(hba_high_frequencies[0][2], 219.9218750e6)

    def test_subband_frequency(self):
        # most coverage is in test_subband_frequencies, we only need to test
        # whether the right parameter types are accepted.
        hba_low_frequency = subband_frequency(102, CLK_200_MHZ, 1, True)
        self.assertAlmostEqual(hba_low_frequency, 119.9218750e6)

    def test_subband_frequency_spectral_inversion(self):
        # test for an inverted band (nyquist zone 1)
        freq_subband_0_normal = subband_frequency(0, CLK_200_MHZ, 1, True)
        freq_subband_512_reverse = subband_frequency(512, CLK_200_MHZ, 1, False)
        self.assertAlmostEqual(freq_subband_0_normal, freq_subband_512_reverse)

        # test for a regulat band (nyquist zone 2)
        freq_subband_0_normal = subband_frequency(0, CLK_200_MHZ, 2, False)
        freq_subband_512_reverse = subband_frequency(512, CLK_200_MHZ, 2, True)
        self.assertAlmostEqual(freq_subband_0_normal, freq_subband_512_reverse)

    def test_phases_to_weights(self):
        # offer nice 0, 90, 180, 270, 360 degrees
        phases = numpy.array(
            [0.0, numpy.pi / 2, numpy.pi, numpy.pi * 1.5, numpy.pi * 2]
        )

        sdp_weights = phases_to_weights(phases, self.UNIT_WEIGHT)

        # check whether the complex representation is also along the right axes and
        # has the right amplitude
        self.assertEqual(
            weight_to_complex(sdp_weights[0], self.UNIT_WEIGHT),
            1 + 0j,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertEqual(
            weight_to_complex(sdp_weights[1], self.UNIT_WEIGHT),
            0 + 1j,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertEqual(
            weight_to_complex(sdp_weights[2], self.UNIT_WEIGHT),
            -1 + 0j,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertEqual(
            weight_to_complex(sdp_weights[3], self.UNIT_WEIGHT),
            0 - 1j,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertEqual(
            weight_to_complex(sdp_weights[4], self.UNIT_WEIGHT),
            1 + 0j,
            msg=f"sdp_weights = {sdp_weights}",
        )

    def test_phases_to_weights_with_amplitude(self):
        # offer nice 0, 90, 180, 270, 360 degrees
        phases = numpy.array(
            [0.0, numpy.pi / 2, numpy.pi, numpy.pi * 1.5, numpy.pi * 2]
        )
        amplitudes = numpy.array([0.1, 0.2, 0.3, 0.4, 0.5])

        sdp_weights = phases_to_weights(phases, self.UNIT_WEIGHT, amplitudes)

        # check whether the complex representation is also along the right axes and
        # has the right amplitude
        self.assertAlmostEqual(
            weight_to_complex(sdp_weights[0], self.UNIT_WEIGHT),
            0.1 + 0j,
            places=3,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertAlmostEqual(
            weight_to_complex(sdp_weights[1], self.UNIT_WEIGHT),
            0 + 0.2j,
            places=3,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertAlmostEqual(
            weight_to_complex(sdp_weights[2], self.UNIT_WEIGHT),
            -0.3 + 0j,
            places=3,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertAlmostEqual(
            weight_to_complex(sdp_weights[3], self.UNIT_WEIGHT),
            0 - 0.4j,
            places=3,
            msg=f"sdp_weights = {sdp_weights}",
        )
        self.assertAlmostEqual(
            weight_to_complex(sdp_weights[4], self.UNIT_WEIGHT),
            0.5 + 0j,
            places=3,
            msg=f"sdp_weights = {sdp_weights}",
        )

    def test_weight_to_complex(self):
        self.assertEqual(0, weight_to_complex(0, self.UNIT_WEIGHT))
        self.assertEqual(1, weight_to_complex(self.UNIT_WEIGHT, self.UNIT_WEIGHT))
        self.assertEqual(
            1j, weight_to_complex(self.UNIT_WEIGHT << 16, self.UNIT_WEIGHT)
        )

    def test_complex_to_weights_1D_input(self):
        values = numpy.ones(3) + 1j * numpy.zeros(3)
        weights = complex_to_weights(values, self.UNIT_WEIGHT)
        self.assertEqual(values.shape, weights.shape)
        self.assertListEqual(
            [self.UNIT_WEIGHT] * weights.size, weights.flatten().tolist()
        )

    def test_complex_to_weights_2D_input(self):
        values = numpy.ones((3, 2)) + 1j * numpy.zeros((3, 2))
        weights = complex_to_weights(values, self.UNIT_WEIGHT)
        self.assertEqual(values.shape, weights.shape)
        self.assertListEqual(
            [self.UNIT_WEIGHT] * weights.size, weights.flatten().tolist()
        )

    def test_complex_to_weights_3D_input(self):
        values = numpy.ones((3, 2, 4)) + 1j * numpy.zeros((3, 2, 4))
        weights = complex_to_weights(values, self.UNIT_WEIGHT)
        self.assertEqual(values.shape, weights.shape)
        self.assertListEqual(
            [self.UNIT_WEIGHT] * weights.size, weights.flatten().tolist()
        )

    def test_weight_to_complex_vs_complex_to_weights(self):
        def complex_to_weight(c):
            return complex_to_weights(
                numpy.array([c], dtype=numpy.complex128), self.UNIT_WEIGHT
            )[0]

        self.assertEqual(-1, weight_to_complex(complex_to_weight(-1), self.UNIT_WEIGHT))
        self.assertEqual(
            -1j, weight_to_complex(complex_to_weight(-1j), self.UNIT_WEIGHT)
        )
        self.assertEqual(
            2 - 3j, weight_to_complex(complex_to_weight(2 - 3j), self.UNIT_WEIGHT)
        )
        self.assertEqual(
            0.5 - 0.25j,
            weight_to_complex(complex_to_weight(0.5 - 0.25j), self.UNIT_WEIGHT),
        )
