# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common import cables

from test import base


class TestCables(base.TestCase):
    def test_cable_names(self):
        """Test whether cable names match their key in the cable_types dictionary."""

        for name, cable in cables.cable_types.items():
            self.assertEqual(name, cable.name)

    def test_cable_speeds(self):
        """Test whether cables are transporting signals at 80% - 100% the speed of light,
        which is a property of all our cables."""

        speed_of_light = 299_792_458

        for cable in cables.cable_types.values():
            if cable.length > 0:
                self.assertLess(
                    80, cable.speed() / speed_of_light * 100, msg=f"Cable {cable.name}"
                )
                self.assertGreater(
                    100, cable.speed() / speed_of_light * 100, msg=f"Cable {cable.name}"
                )

    def test_cable_loss_increases_with_frequency(self):
        """Test whether cable losses increase with frequency for each cable."""

        for cable in cables.cable_types.values():
            if cable.length == 0:
                self.assertEqual(0.0, cable.loss[50])
                self.assertEqual(0.0, cable.loss[150])
                self.assertEqual(0.0, cable.loss[200])
                self.assertEqual(0.0, cable.loss[250])
            else:
                self.assertLess(
                    cable.loss[50], cable.loss[150], msg=f"Cable {cable.name}"
                )
                self.assertLess(
                    cable.loss[150], cable.loss[200], msg=f"Cable {cable.name}"
                )
                self.assertLess(
                    cable.loss[200], cable.loss[250], msg=f"Cable {cable.name}"
                )
