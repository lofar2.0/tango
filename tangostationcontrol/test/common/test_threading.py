# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import tango

from tangostationcontrol.common.threading import OmniThread

from test import base


class TestOmniThread(base.TestCase):
    def test_runs(self):
        """Tests whether the target function is called at all."""

        tracer = [False]

        def _run():
            tracer[0] = True

        ot = OmniThread(target=_run)
        ot.start()
        ot.join()

        self.assertTrue(tracer[0])

    def test_is_omnithread(self):
        """Tests whether the spawned thread is turned into an OmniThread."""

        result = [False]

        def _run():
            result[0] = tango.is_omni_thread()

        ot = OmniThread(target=_run)
        ot.start()
        ot.join()

        self.assertTrue(result[0])
