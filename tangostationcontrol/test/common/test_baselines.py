# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common import baselines
from tangostationcontrol.common.constants import MAX_INPUTS

from test import base


class TestBaselines(base.TestCase):
    def test_nr_baselines(self):
        # no baselines if no antennas
        self.assertEqual(0, baselines.nr_baselines(0))
        # one antenna only has autocorrelation
        self.assertEqual(1, baselines.nr_baselines(1))
        # two antennas each have autocorrelations + a baseline between each other
        self.assertEqual(3, baselines.nr_baselines(2))

    def test_baseline_indices(self):
        """Test whether baseline_from_index and baseline_index line up."""

        for major in range(MAX_INPUTS):
            for minor in range(major + 1):
                idx = baselines.baseline_index(major, minor)
                self.assertEqual(
                    (major, minor),
                    baselines.baseline_from_index(idx),
                    msg=f"baseline_index({major},{minor}) resulted in {idx}, and should match baseline_from_index({idx})",
                )
