#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging

from test import base
from tango import DevFailed
from tango.test_context import DeviceTestContext
from tangostationcontrol.common import lofar_logging
from tangostationcontrol.devices.antennafield import afh


class TestExceptionToStr(base.TestCase):
    """Test class for logging strings check"""

    def test_normal_exception(self):
        """Test whether logging strings work with normal exceptions"""
        ex = Exception()
        self.assertNotEqual("", lofar_logging.exception_to_str(ex))

    def test_tango_exception(self):
        """Test whether logging strings work with Tango exceptions"""
        ex = DevFailed()
        self.assertNotEqual("", lofar_logging.exception_to_str(ex))

        # Simulate DevFailed exception
        try:
            with DeviceTestContext(afh.AFH, properties={}, process=True) as proxy:
                proxy.boot()
        except DevFailed as ex:
            original_exc = str(ex)
            logged_exc = lofar_logging.exception_to_str(ex)
            # Verbose original exception vs readable logged exception
            self.assertTrue("PyDs_PythonError" in original_exc, msg=original_exc)
            self.assertFalse("PyDs_PythonError" in logged_exc, msg=logged_exc)
            self.assertTrue(len(original_exc) > len(logged_exc))


class TestLofarLogging(base.TestCase):
    """Test class for logging methods"""

    def setUp(self):
        super(TestLofarLogging, self).setUp()

        # reset logging system
        root_logger = logging.getLogger()
        root_logger.filters = []
        root_logger.handlers = []
        root_logger.manager.loggerDict = {}

        # record everything we log in memory so we can inspect it
        class MemoryHandler(logging.Handler):
            """Handler that provides access to the records emitted."""

            def __init__(self):
                super().__init__()
                self.records = []

            def emit(self, record):
                self.records.append(record)

        self.memory_handler = MemoryHandler()
        root_logger.addHandler(self.memory_handler)

    def test_configure_logging_basic_usage(self):
        """Test whether configure_logger indeed runs smoothly."""

        logger = lofar_logging.configure_logger(debug=True)

        logger.debug("test debug")
        logger.info("test info")
        logger.warning("test warning")
        logger.error("test error")
        logger.fatal("test fatal")

    def test_configure_logging_log_annotation(self):
        """Test whether log records get annotated after using configure_logger()."""

        logger = lofar_logging.configure_logger(debug=True)

        logger.info("test")
        self.assertIn("device", self.memory_handler.records[0].__dict__)
        self.assertIn("software_version", self.memory_handler.records[0].__dict__)
