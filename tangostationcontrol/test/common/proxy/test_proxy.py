# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

from tango import DevSource, AccessControlType

from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.common.proxies import proxy

from test import base

logger = logging.getLogger()


class TestCreateProxy(base.TestCase):

    def setUp(self):
        super(TestCreateProxy, self).setUp()

        self.m_device_proxy = self.proxy_patch(proxy, "DeviceProxy", autospec=True)

    def test_create_proxy(self):
        """Test creating proxy from device name"""

        t_device_name = "stat/stationmanager/1"

        create_device_proxy(t_device_name)

        self.m_device_proxy["mock"].return_value.set_source.assert_called_with(
            DevSource.DEV
        )

        self.m_device_proxy["mock"].return_value.set_access_control.assert_called_with(
            AccessControlType.ACCESS_READ
        )

    def test_create_proxy_set_timeout(self):
        """Test creating proxy and configure timeout"""
        t_timeout = 60000

        t_device_name = "stat/stationmanager/1"
        create_device_proxy(t_device_name, timeout_millis=t_timeout)

        self.m_device_proxy["mock"].return_value.set_timeout_millis.assert_called_with(
            t_timeout
        )

    def test_create_proxy_timeout_property(self):
        """Test creating proxy from device name"""
        t_property_timeout = 90000

        self.m_device_proxy["mock"].return_value.get_property.return_value = {
            "Device_Proxy_Timeout": [str(t_property_timeout)]
        }

        t_device_name = "stat/stationmanager/1"

        create_device_proxy(t_device_name)

        self.m_device_proxy["mock"].return_value.set_source.assert_called_with(
            DevSource.DEV
        )
        self.m_device_proxy["mock"].return_value.set_timeout_millis.assert_called_with(
            t_property_timeout
        )

    def test_creat_proxy_write_access(self):
        """Test creating proxy with write access"""

        t_device_name = "stat/stationmanager/1"

        create_device_proxy(t_device_name, write_access=True)

        self.m_device_proxy["mock"].return_value.set_source.assert_called_with(
            DevSource.DEV
        )

        self.m_device_proxy["mock"].return_value.set_access_control.assert_not_called()
