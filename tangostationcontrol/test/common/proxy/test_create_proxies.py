# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from unittest import mock

import tango
from lofar_station_client.common import CaseInsensitiveDict

from tangostationcontrol.common.types.device_config_types import (
    device_config_type,
    device_proxy_type,
)
from tangostationcontrol.common.proxies import create_proxies

from test import base

logger = logging.getLogger()


class TestCreateProxies(base.TestCase):

    def setUp(self):
        super(TestCreateProxies, self).setUp()

        self.m_create_proxy = self.proxy_patch(
            create_proxies, "create_device_proxy", autospec=True
        )

        self.m_database = self.proxy_patch(create_proxies.tango, "Database")

        self.test_config: device_config_type = CaseInsensitiveDict({"AFH": [""]})

    def test_create_proxies(self):
        """Test creating proxies from device classes"""

        self.m_database[
            "mock"
        ].return_value.get_device_exported_for_class.return_value = [
            "stat/afh/hba0",
            "stat/afh/hba1",
        ]

        m_proxy = mock.Mock(spec=tango.DeviceProxy)
        self.m_create_proxy["mock"].return_value = m_proxy

        current_proxies: device_proxy_type = CaseInsensitiveDict()

        create_proxies.create_proxies(self.test_config, current_proxies)

        self.assertEqual(current_proxies["stat/afh/hba0"], m_proxy)
        self.assertEqual(current_proxies["stat/afh/hba1"], m_proxy)

    def test_create_proxies_exception(self):
        """Test creating proxies based on classes but some return exceptions"""

        self.m_database[
            "mock"
        ].return_value.get_device_exported_for_class.return_value = [
            "stat/afh/hba0",
            "stat/afh/hba1",
        ]

        m_proxy = mock.Mock(spec=tango.DeviceProxy)
        self.m_create_proxy["mock"].side_effect = [m_proxy, tango.DevFailed]

        current_proxies: device_proxy_type = CaseInsensitiveDict()

        create_proxies.create_proxies(self.test_config, current_proxies)

        self.assertEqual(current_proxies["stat/afh/hba0"], m_proxy)
        self.assertIsInstance(current_proxies["stat/afh/hba1"], tango.DevFailed)

    def test_create_proxies_return_new_only(self):
        """Test that calls to create_proxies only return newly created"""

        self.m_database[
            "mock"
        ].return_value.get_device_exported_for_class.return_value = [
            "stat/afh/hba0",
            "stat/afh/hba1",
        ]

        m_proxy = mock.Mock(spec=tango.DeviceProxy)
        self.m_create_proxy["mock"].return_value = m_proxy

        current_proxies: device_proxy_type = CaseInsensitiveDict(
            {"stat/afh/hba0": m_proxy, "stat/afh/hba1": tango.DevFailed()}
        )

        result = create_proxies.create_proxies(self.test_config, current_proxies)
        self.assertEqual(result["stat/afh/hba1"], m_proxy)
        self.assertNotIn("stat/afh/hba0", result)

    def test_create_proxies_class_device_map(self):
        """Test data being added to class device map"""

        device_list = [
            "stat/afh/hba0",
            "stat/afh/hba1",
        ]

        self.m_database[
            "mock"
        ].return_value.get_device_exported_for_class.return_value = device_list

        current_proxies: device_proxy_type = CaseInsensitiveDict()
        class_map = CaseInsensitiveDict()
        create_proxies.create_proxies(self.test_config, current_proxies, class_map)

        self.assertIn("AFH", class_map)
        self.assertEqual(class_map["AFH"], device_list)
