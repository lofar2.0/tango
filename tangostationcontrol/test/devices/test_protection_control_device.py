# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import time
from unittest.mock import patch

from requests.structures import CaseInsensitiveDict
from tango.test_context import DeviceTestContext

from tangostationcontrol.common.constants import DEFAULT_POLLING_PERIOD_MS
from tangostationcontrol.devices import protection_control
from tangostationcontrol.protection.state import ProtectionStateEnum

from test.devices import device_base


@patch("tango.Database")
class TestProtectionControlDevice(device_base.DeviceTestCase):
    def setUp(self):
        super(TestProtectionControlDevice, self).setUp()

        self.m_protection = self.proxy_patch(
            protection_control, "ProtectionManager", autospec=True
        )
        self.m_protection["mock"].return_value.state = ProtectionStateEnum.OFF

    def test_start_stop_event_loop(self, _database):
        with DeviceTestContext(
            protection_control.ProtectionControl,
            process=False,
            timeout=10,
        ) as proxy:
            proxy.initialise()
            proxy.on()

            time.sleep(DEFAULT_POLLING_PERIOD_MS / 1000)

            self.assertTrue(proxy.protection_monitor_thread_running_R)

    def test_protection_control_state(self, _database):

        with DeviceTestContext(
            protection_control.ProtectionControl,
            process=False,
            timeout=10,
        ) as proxy:

            self.assertEqual(ProtectionStateEnum.DEACTIVATED, proxy.protection_state_R)

            proxy.initialise()
            proxy.on()

            self.assertEqual(ProtectionStateEnum.OFF, proxy.protection_state_R)

    def test_periodic_reattempt(self, _database):
        with DeviceTestContext(
            protection_control.ProtectionControl,
            properties={"periodic_reattempt_interval": 5},
            process=False,
            timeout=10,
        ) as proxy:
            proxy.initialise()
            proxy.on()

            self.assertEqual(
                2, self.m_protection["mock"].return_value.create_proxies.call_count
            )

            time.sleep(6)

            self.assertTrue(
                self.m_protection["mock"].return_value.create_proxies.call_count > 2
            )

    def test_manual_shutdown(self, _database):
        with DeviceTestContext(
            protection_control.ProtectionControl,
            process=False,
            timeout=10,
        ) as proxy:

            proxy.initialise()
            proxy.on()

            proxy.protective_shutdown("Everything is on fire")

            self.assertEqual(
                1, self.m_protection["mock"].return_value.protective_shutdown.call_count
            )

    def test_metrics(self, _database):
        """Test that metric mappings works as best as possible in unit tests"""

        original_mapping = protection_control.ProtectionControl.METRIC_MAPPING
        try:
            protection_control.ProtectionControl.METRIC_MAPPING = CaseInsensitiveDict(
                {"stat/device_1/1": CaseInsensitiveDict({"attribute_1": 0})}
            )

            with DeviceTestContext(
                protection_control.ProtectionControl,
                process=False,
                timeout=10,
            ) as proxy:

                proxy.initialise()
                proxy.on()

                self.assertIn(
                    "stat/device_1/1.attribute_1",
                    proxy.identified_device_attribute_pairs_R,
                )
                self.assertEqual(0, proxy.time_last_change_events_R[0])
        finally:
            protection_control.ProtectionControl.METRIC_MAPPING = original_mapping
