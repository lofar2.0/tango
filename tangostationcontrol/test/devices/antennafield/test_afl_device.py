#  Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# invalid-name
# pylint: disable=C0103

import logging
from test import base

import numpy

from tangostationcontrol.common.constants import (
    N_rcu,
    DEFAULT_N_HBA_TILES,
    MAX_ANTENNA,
)
from tangostationcontrol.devices.antennafield.afl import AFL
from tangostationcontrol.devices.base_device_classes.mapper import (
    MappingKeys,
    AFLToRecvMapper,
)

logger = logging.getLogger()

RECV_MAPPED_ATTRS = AFL.get_mapped_dimensions("RECV")
DEFAULT_N_LBA = DEFAULT_N_HBA_TILES  # 48 antennas


class TestAFLToRecvMapper(base.TestCase):
    """Test class for AntennaField-LBA to RECV Mapper"""

    # A mapping where Antennas are all not mapped to power RCUs
    POWER_NOT_CONNECTED = [[-1, -1]] * 2 * DEFAULT_N_LBA
    # A mapping where Antennas are all not mapped to control RCUs
    CONTROL_NOT_CONNECTED = [[-1, -1]] * 2 * DEFAULT_N_LBA
    # A mapping where first two Antennas are mapped on the first Receiver.
    # The first Antenna control line on RCU 1 and the second Antenna control line
    # on RCU 0.
    POWER_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1 = (
        [[1, 1], [1, 0]]
        + [[-1, -1]] * (DEFAULT_N_LBA - 2)
        + [[2, 1], [2, 0]]
        + [[-1, -1]] * (DEFAULT_N_LBA - 2)
    )
    CONTROL_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1 = (
        POWER_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1
    )

    def test_map_write_rcu_pwr_ant_on_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFLToRecvMapper(
            recv_mapping,
            RECV_MAPPED_ATTRS,
            1,
        )

        set_values = [None] * DEFAULT_N_LBA
        expected = [[[None, None, None]] * N_rcu]
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_pwr_ant_on_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFLToRecvMapper(
            recv_mapping,
            RECV_MAPPED_ATTRS,
            2,
        )

        set_values = [None] * DEFAULT_N_LBA
        expected = [[[None, None, None]] * N_rcu] * 2
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_pwr_ant_on_lba_0_and_1_on_rcu_1_and_0_of_recv_1_and_no_ctrl(
        self,
    ):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFLToRecvMapper(
            recv_mapping,
            RECV_MAPPED_ATTRS,
            1,
        )

        set_values = [1, 0] + [None] * (DEFAULT_N_LBA - 2)
        expected = [[[None, None, None]] + [[None, None, None]] * (N_rcu - 1)]
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_pwr_ant_on_lba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFLToRecvMapper(
            recv_mapping,
            RECV_MAPPED_ATTRS,
            2,
        )

        receiver_values = [[False, True] + [False] * (MAX_ANTENNA - 2)] * 2
        expected = ([True, False] + [False] * (int(MAX_ANTENNA / 2) - 2)) * 2
        actual = mapper.map_read("RCU_PWR_ANT_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_pwr_ant_on_lba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_LBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFLToRecvMapper(
            recv_mapping,
            RECV_MAPPED_ATTRS,
            1,
        )

        set_values = [1, 0] + [None] * (DEFAULT_N_LBA - 2)
        expected = [[[None, None, None]] * N_rcu]
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)
