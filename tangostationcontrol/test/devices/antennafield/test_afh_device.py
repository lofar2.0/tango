#  Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# invalid-name
# pylint: disable=C0103

import logging
from test import base
from test.devices import device_base

import numpy
from tango.test_context import DeviceTestContext

from tangostationcontrol.common.constants import (
    MAX_ANTENNA,
    N_rcu,
    N_rcu_inp,
    DEFAULT_N_HBA_TILES,
    N_pn,
)
from tangostationcontrol.devices.antennafield import afh
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AntennaStatus,
    AntennaUse,
)
from tangostationcontrol.devices.antennafield.afh import AFH
from tangostationcontrol.devices.base_device_classes.mapper import (
    MappingKeys,
    AFHToRecvMapper,
    AntennaToSdpMapper,
)

logger = logging.getLogger()

SDP_MAPPED_ATTRS = AFH.get_mapped_dimensions("SDP")
RECV_MAPPED_ATTRS = AFH.get_mapped_dimensions("RECV")

DEFAULT_N_LBA = DEFAULT_N_HBA_TILES  # 48 antennas


class TestAntennaToSdpMapper(base.TestCase):
    """Test class for AntennaToSDPMapper"""

    # A mapping where Antennas are all not mapped to SDP FPGAs
    FPGA_NOT_CONNECTED = numpy.reshape(
        numpy.array([[x, -1] * 6 for x in range(0, 8)]), (DEFAULT_N_HBA_TILES * 2,)
    )  # 6 inputs, 8 fpgas
    # A mapping where first two Antennas are mapped on the second and first FPGA respectively
    FPGA_HBA_0_AND_1_ON_FPGA_1_AND_0 = numpy.reshape(
        numpy.array([[1, 0, 0, 0] + [0, -1] * 4] + [[x, -1] * 6 for x in range(0, 7)]),
        (DEFAULT_N_HBA_TILES * 2,),
    )

    def test_fpga_sdp_info_observation_id_no_mapping(self):
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(self.FPGA_NOT_CONNECTED, (-1, 2)).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        sdp_values = list(range(1, N_pn + 1))
        expected = [0] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("FPGA_sdp_info_observation_id_R", sdp_values)
        numpy.testing.assert_equal(expected, actual)

    def test_fpga_sdp_info_antenna_band_index_no_mapping(self):
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(self.FPGA_NOT_CONNECTED, (-1, 2)).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        # all HBAs
        sdp_values = [1] * N_pn
        # expected zeros because not connected
        expected = [0] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("FPGA_sdp_info_antenna_band_index_R", sdp_values)
        numpy.testing.assert_equal(expected, actual)

    def test_fpga_sdp_info_observation_id_HBA_0_AND_1_ON_FPGA_1_AND_0(self):
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(
                self.FPGA_HBA_0_AND_1_ON_FPGA_1_AND_0, (-1, 2)
            ).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        sdp_values = list(range(1, N_pn + 1))  # [1,2,3,...] (12,)
        expected = [2] + [1] + [0] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("FPGA_sdp_info_observation_id_R", sdp_values)
        numpy.testing.assert_equal(expected, actual)

    def test_fpga_sdp_info_antenna_band_index_HBA_0_AND_1_ON_FPGA_1_AND_0(self):
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(
                self.FPGA_HBA_0_AND_1_ON_FPGA_1_AND_0, (-1, 2)
            ).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        # all HBAs
        sdp_values = [0] + [1] + [1] * (N_pn - 2)
        # expected zeros because not connected
        expected = [1] + [0] + [0] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("FPGA_sdp_info_antenna_band_index_R", sdp_values)
        numpy.testing.assert_equal(actual, expected)

    def test_map_write_fpga_sdp_info_observation_id_no_mapping(self):
        """Verify results None without fpga connection and array sizes"""
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(self.FPGA_NOT_CONNECTED, (-1, 2)).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [None] * N_pn
        actual = mapper.map_write("FPGA_sdp_info_observation_id_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_fpga_sdp_info_observation_id_hba_0_and_1_on_fpga_1_and_0(self):
        """Verify results fpga connections and array sizes"""
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(
                self.FPGA_HBA_0_AND_1_ON_FPGA_1_AND_0, (-1, 2)
            ).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        set_values = [2] + [1] + [-1] * (DEFAULT_N_HBA_TILES - 2)
        expected = [1] + [2] + [None] * (N_pn - 2)
        actual = mapper.map_write("FPGA_sdp_info_observation_id_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_fpga_sdp_info_antenna_band_index_no_mapping(self):
        """Verify results None without fpga connection and array sizes"""
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(self.FPGA_NOT_CONNECTED, (-1, 2)).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [None] * N_pn
        actual = mapper.map_write("FPGA_sdp_info_antenna_band_index_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_fpga_sdp_info_antenna_band_index_hba_0_and_1_on_fpga_1_and_0(
        self,
    ):
        """Verify results fpga connections and array sizes"""
        fpga_mapping = {
            MappingKeys.FPGA: numpy.reshape(
                self.FPGA_HBA_0_AND_1_ON_FPGA_1_AND_0, (-1, 2)
            ).tolist()
        }
        mapper = AntennaToSdpMapper(fpga_mapping, SDP_MAPPED_ATTRS)
        set_values = [1] + [0] + [1] * (DEFAULT_N_HBA_TILES - 2)
        expected = [0] + [1] + [None] * (N_pn - 2)
        actual = mapper.map_write("FPGA_sdp_info_antenna_band_index_RW", set_values)
        numpy.testing.assert_equal(expected, actual)


class TestAFHToRecvMapper(base.TestCase):
    """Test class for AntennaField-HBA to RECV Mapper"""

    # A mapping where Antennas are all not mapped to power RCUs
    POWER_NOT_CONNECTED = [[-1, -1]] * DEFAULT_N_HBA_TILES
    # A mapping where Antennas are all not mapped to control RCUs
    CONTROL_NOT_CONNECTED = [[-1, -1]] * DEFAULT_N_HBA_TILES
    # A mapping where first two Antennas are mapped on the first Receiver.
    # The first Antenna control line on RCU 1 and the second Antenna control line
    # on RCU 0.
    POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1 = [[1, 1], [1, 0]] + [[-1, -1]] * (
        DEFAULT_N_HBA_TILES - 2
    )
    CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1 = [[1, 1], [1, 0]] + [[-1, -1]] * (
        DEFAULT_N_HBA_TILES - 2
    )
    CONTROL_HBA_0_AND_1_ON_RCU_3_AND_2_OF_RECV_1 = [[1, 3], [1, 2]] + [[-1, -1]] * (
        DEFAULT_N_HBA_TILES - 2
    )
    ANTENNA_TYPE = "HBA"

    def test_ant_read_mask_r_no_mapping(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [False] * MAX_ANTENNA,
            [False] * MAX_ANTENNA,
            [False] * MAX_ANTENNA,
        ]
        expected = [False] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("ANT_mask_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_ant_read_mask_r_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [False, True, False] + [False, False, False] * (N_rcu - 1),
            [False] * MAX_ANTENNA,
            [False] * MAX_ANTENNA,
        ]
        expected = [True, False] + [False] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("ANT_mask_RW", receiver_values)

        numpy.testing.assert_equal(expected, actual)

    def test_rcu_band_select_no_mapping(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)
        receiver_values = [[0] * MAX_ANTENNA, [0] * MAX_ANTENNA, [0] * MAX_ANTENNA]
        expected = [[0, 0]] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("RCU_band_select_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_bf_read_delay_steps_r_no_mapping(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[0] * N_rcu] * MAX_ANTENNA,
            [[0] * N_rcu] * MAX_ANTENNA,
            [[0] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[0] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_BF_delay_steps_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_bf_read_delay_steps_r_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[2] * N_rcu, [1] * N_rcu] + [[0] * N_rcu] * (MAX_ANTENNA - 2),
            [[0] * N_rcu] * MAX_ANTENNA,
            [[0] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[1] * N_rcu, [2] * N_rcu] + [[0] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_BF_delay_steps_R", receiver_values)

        numpy.testing.assert_equal(expected, actual)

    def test_bf_read_delay_steps_rw_no_mapping(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[0] * N_rcu] * MAX_ANTENNA,
            [[0] * N_rcu] * MAX_ANTENNA,
            [[0] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[0] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_BF_delay_steps_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_bf_read_delay_steps_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[2] * N_rcu, [1] * N_rcu] + [[0] * N_rcu] * (MAX_ANTENNA - 2),
            [[0] * N_rcu] * MAX_ANTENNA,
            [[0] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[1] * N_rcu, [2] * N_rcu] + [[0] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_BF_delay_steps_RW", receiver_values)

        numpy.testing.assert_equal(expected, actual)

    def test_map_read_led_on_r_unmapped(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[False] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_LED_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_led_on_r_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False, True] * 16, [True, False] * 16]
            + [[False] * N_rcu] * (MAX_ANTENNA - 2),
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]

        expected = [[True, False] * 16, [False, True] * 16] + [[False] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_LED_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_led_on_rw_unmapped(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[False] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_LED_on_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_led_on_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False, True] * 16, [True, False] * 16]
            + [[False] * N_rcu] * (MAX_ANTENNA - 2),
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]

        expected = [[True, False] * 16, [False, True] * 16] + [[False] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_LED_on_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_lna_on_r_unmapped(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[False] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_PWR_LNA_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_lna_on_r_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False, True] * 16, [True, False] * 16]
            + [[False] * N_rcu] * (MAX_ANTENNA - 2),
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]

        expected = [[True, False] * 16, [False, True] * 16] + [[False] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_PWR_LNA_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_lna_on_rw_unmapped(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[False] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_PWR_LNA_on_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_lna_on_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False, True] * 16, [True, False] * 16]
            + [[False] * N_rcu] * (MAX_ANTENNA - 2),
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]

        expected = [[True, False] * 16, [False, True] * 16] + [[False] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_PWR_LNA_on_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_on_r_unmapped(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[False] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_PWR_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_on_r_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False, True] * 16, [True, False] * 16]
            + [[False] * N_rcu] * (MAX_ANTENNA - 2),
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]

        expected = [[True, False] * 16, [False, True] * 16] + [[False] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_PWR_on_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_on_rw_unmapped(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]
        expected = [[False] * N_rcu] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("HBAT_PWR_on_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_pwr_on_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)

        receiver_values = [
            [[False, True] * 16, [True, False] * 16]
            + [[False] * N_rcu] * (MAX_ANTENNA - 2),
            [[False] * N_rcu] * MAX_ANTENNA,
            [[False] * N_rcu] * MAX_ANTENNA,
        ]

        expected = [[True, False] * 16, [False, True] * 16] + [[False] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        actual = mapper.map_read("HBAT_PWR_on_RW", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_id_unmapped(self):
        """Test whether RCU_PCB_ID_R is correctly read with no mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)
        receiver_values = [list(range(32)), [0] * 32, [0] * 32]
        expected = [[0] * 2] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("RCU_PCB_ID_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_id_control_connected_and_power_disconnected(self):
        """Test whether RCU_PCB_ID_R is correctly read with control mapping
        and no power mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_3_AND_2_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)
        receiver_values = [list(range(32)), [0] * 32, [0] * 32]
        expected = [[0, 3], [0, 2]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("RCU_PCB_ID_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_id_control_disconnected_and_power_connected(self):
        """Test whether RCU_PCB_ID_R is correctly read with power mapping
        and no control mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)
        receiver_values = [list(range(32)), [0] * 32, [0] * 32]
        expected = [[1, 0], [0, 0]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("RCU_PCB_ID_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_id_control_and_power_connected(self):
        """Test whether RCU_PCB_ID_R is correctly read with control mapping
        and power mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_3_AND_2_OF_RECV_1,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)
        receiver_values = [list(range(32)), [0] * 32, [0] * 32]
        expected = [[1, 3], [0, 2]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("RCU_PCB_ID_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_attenuator_unmapped(self):
        """Test whether RCU_attenuator_dB_R is correctly read with no mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 3)
        receiver_values = [
            list(range(MAX_ANTENNA)),
            [0] * MAX_ANTENNA,
            [0] * MAX_ANTENNA,
        ]
        expected = [[0] * 2] * DEFAULT_N_HBA_TILES
        actual = mapper.map_read("RCU_attenuator_dB_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_attenuator_control_connected_and_power_disconnected(self):
        """Test whether RCU_attenuator_dB_R is correctly read with control mapping
        and no power mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)
        receiver_values = [list(range(MAX_ANTENNA))]
        expected = [[0, 1], [0, 0]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("RCU_attenuator_dB_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_attenuator_control_disconnected_and_power_connected(self):
        """Test whether RCU_attenuator_dB_R is correctly read with power mapping
        and no control mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)
        receiver_values = [list(range(MAX_ANTENNA))]
        expected = [[1, 0], [0, 0]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("RCU_attenuator_dB_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_read_rcu_attenuator_control_and_power_connected(self):
        """Test whether RCU_attenuator_dB_R is correctly read with both
        control mapping and power mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_3_AND_2_OF_RECV_1,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)
        receiver_values = [list(range(MAX_ANTENNA))]
        expected = [[1, 3], [0, 2]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        actual = mapper.map_read("RCU_attenuator_dB_R", receiver_values)
        numpy.testing.assert_equal(expected, actual)

    # Rename to write

    def test_map_write_ant_mask_rw_no_mapping_and_one_receiver(self):
        """Verify results None without control and array sizes"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [[[None, None, None]] * N_rcu]
        actual = mapper.map_write("ANT_mask_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_ant_mask_rw_no_mapping_and_two_receivers(self):
        """Verify results None without control and array sizes"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [[[None, None, None]] * N_rcu] * 2
        actual = mapper.map_write("ANT_mask_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_ant_mask_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [True, False] + [None] * (DEFAULT_N_HBA_TILES - 2)
        expected = [[[False, True, None]] + [[None, None, None]] * (N_rcu - 1)]
        actual = mapper.map_write("ANT_mask_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_pwr_ant_on_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [[[None, None, None]] * N_rcu]
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_pwr_ant_on_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [[[None, None, None]] * N_rcu] * 2
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_pwr_ant_on_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [1, 0] + [None] * (DEFAULT_N_HBA_TILES - 2)
        expected = [[[0, 1, None]] + [[None, None, None]] * (N_rcu - 1)]
        actual = mapper.map_write("RCU_PWR_ANT_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_band_select_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [[[None, None, None]] * N_rcu]
        actual = mapper.map_write("RCU_band_select_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_band_select_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [None] * DEFAULT_N_HBA_TILES
        expected = [[[None, None, None]] * N_rcu] * 2
        actual = mapper.map_write("RCU_band_select_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_band_select_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[1, 1], [0, 0]] + [[None, None]] * (DEFAULT_N_HBA_TILES - 2)
        expected = [[[0, 1, None]] + [[None, None, None]] * (N_rcu - 1)]
        actual = mapper.map_write("RCU_band_select_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_bf_delay_steps_rw_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[1] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_BF_delay_steps_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_bf_delay_steps_rw_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [[1] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA, [[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_BF_delay_steps_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_bf_delay_steps_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[1] * N_rcu, [2] * N_rcu] + [[None] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        expected = [[[2] * N_rcu, [1] * N_rcu] + [[None] * N_rcu] * (MAX_ANTENNA - 2)]
        actual = mapper.map_write("HBAT_BF_delay_steps_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_led_on_rw_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[None] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_LED_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_led_on_rw_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [[None] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA, [[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_LED_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_led_on_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[False, True] * 16, [True, False] * 16] + [[None] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        expected = [
            [[True, False] * 16, [False, True] * 16]
            + [[None] * N_rcu] * (MAX_ANTENNA - 2)
        ]
        actual = mapper.map_write("HBAT_LED_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_pwr_lna_on_rw_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[None] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_PWR_LNA_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_pwr_lna_on_rw_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [[None] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA, [[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_PWR_LNA_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_pwr_lna_on_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[False, True] * 16, [True, False] * 16] + [[None] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        expected = [
            [[True, False] * 16, [False, True] * 16]
            + [[None] * N_rcu] * (MAX_ANTENNA - 2)
        ]
        actual = mapper.map_write("HBAT_PWR_LNA_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_pwr_on_rw_no_mapping_and_one_receiver(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)
        set_values = [[None] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_PWR_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_lna_on_rw_no_mapping_and_two_receivers(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 2)

        set_values = [[None] * N_rcu] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu] * MAX_ANTENNA, [[None] * N_rcu] * MAX_ANTENNA]
        actual = mapper.map_write("HBAT_PWR_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_pwr_on_rw_hba_0_and_1_on_rcu_1_and_0_of_recv_1(self):
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[False, True] * 16, [True, False] * 16] + [[None] * N_rcu] * (
            DEFAULT_N_HBA_TILES - 2
        )
        expected = [
            [[True, False] * 16, [False, True] * 16]
            + [[None] * N_rcu] * (MAX_ANTENNA - 2)
        ]
        actual = mapper.map_write("HBAT_PWR_on_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_attenuator_unmapped(self):
        """Test whether RCU_attenuator_dB_RW is correctly written with no mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[1, 1]] * DEFAULT_N_HBA_TILES
        expected = [[[None] * N_rcu_inp] * N_rcu]

        actual = mapper.map_write("RCU_attenuator_dB_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_attenuator_control_connected_and_power_disconnected(self):
        """Test whether RCU_attenuator_dB_RW is correctly written with control mapping
        and no power mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
            MappingKeys.POWER: self.POWER_NOT_CONNECTED,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = [[1, 2], [3, 4]] + [[0, 0]] * (DEFAULT_N_HBA_TILES - 2)
        expected = [[[4, 2, None]] + [[None] * N_rcu_inp] * (N_rcu - 1)]

        actual = mapper.map_write("RCU_attenuator_dB_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_attenuator_control_disconnected_and_power_connected(self):
        """Test whether RCU_attenuator_dB_RW is correctly written with power mapping
        and no control mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_NOT_CONNECTED,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = set_values = [[1, 2], [3, 4]] + [[0, 0]] * (
            DEFAULT_N_HBA_TILES - 2
        )
        expected = [[[3, 1, None]] + [[None] * N_rcu_inp] * (N_rcu - 1)]

        actual = mapper.map_write("RCU_attenuator_dB_RW", set_values)
        numpy.testing.assert_equal(expected, actual)

    def test_map_write_rcu_attenuator_control_connected_and_power_connected(self):
        """Test whether RCU_attenuator_dB_RW is correctly written with power mapping
        and no control mapping"""
        recv_mapping = {
            MappingKeys.CONTROL: self.CONTROL_HBA_0_AND_1_ON_RCU_3_AND_2_OF_RECV_1,
            MappingKeys.POWER: self.POWER_HBA_0_AND_1_ON_RCU_1_AND_0_OF_RECV_1,
        }
        mapper = AFHToRecvMapper(recv_mapping, RECV_MAPPED_ATTRS, 1)

        set_values = set_values = [[1, 2], [3, 4]] + [[0, 0]] * (
            DEFAULT_N_HBA_TILES - 2
        )
        expected = [[[3, 1, 4], [2, None, None]] + [[None] * N_rcu_inp] * (N_rcu - 2)]

        actual = mapper.map_write("RCU_attenuator_dB_RW", set_values)
        numpy.testing.assert_equal(expected, actual)


class TestHBADevice(device_base.DeviceTestCase):
    """Test class for HBA device"""

    # some dummy values for mandatory properties
    AT_PROPERTIES = {
        "OPC_Server_Name": "example.com",
        "OPC_Server_Port": 4840,
        "OPC_Time_Out": 5.0,
        "Antenna_Field_Reference_ITRF": [3.0, 3.0, 3.0],
        "Antenna_Field_Reference_ETRS": [7.0, 7.0, 7.0],
        "RECV_Devices": ["STAT/RECVH/H0"],
    }
    ANTENNA_PROPERTIES = {
        "Antenna_Sets": ["INNER", "OUTER", "SPARSE_EVEN", "SPARSE_ODD", "ALL"],
        "Antenna_Set_Masks": [
            "111111111111111111111111111111111111111111111111"
            + "000000000000000000000000000000000000000000000000",
            "000000000000000000000000000000000000000000000000"
            + "111111111111111111111111111111111111111111111111",
            "101010101010101010101010101010101010101010101010"
            + "101010101010101010101010101010101010101010101010",
            "010101010101010101010101010101010101010101010101"
            + "010101010101010101010101010101010101010101010101",
            "111111111111111111111111111111111111111111111111"
            + "111111111111111111111111111111111111111111111111",
        ],
    }

    def setUp(self):
        # DeviceTestCase setUp patches lofar_device DeviceProxy
        super(TestHBADevice, self).setUp()

    def test_read_Antenna_Field_Reference(self):
        """Verify if Antenna coordinates are correctly provided"""
        # Device uses ITRF coordinates by default
        with DeviceTestContext(
            afh.AFH, properties=self.AT_PROPERTIES, process=True
        ) as proxy:
            self.assertEqual(3.0, proxy.Antenna_Field_Reference_ITRF_R[0])
        # Device derives coordinates from ETRS if ITRF ones are not found
        at_properties_v2 = {
            "OPC_Server_Name": "example.com",
            "OPC_Server_Port": 4840,
            "OPC_Time_Out": 5.0,
            "Antenna_Field_Reference_ETRS": [7.0, 7.0, 7.0],
            "RECV_Devices": ["STAT/RECVH/H0"],
        }

        with DeviceTestContext(
            afh.AFH, properties=at_properties_v2, process=True
        ) as proxy:
            self.assertNotEqual(
                3.0, proxy.Antenna_Field_Reference_ITRF_R[0]
            )  # value = 6.948998835785814

    def test_read_Antenna_Status(self):
        """Verify if Antenna_Status_R is correctly retrieved"""
        antenna_qualities = numpy.array([AntennaStatus.OK] * MAX_ANTENNA)
        with DeviceTestContext(
            afh.AFH, properties=self.AT_PROPERTIES, process=False
        ) as proxy:
            numpy.testing.assert_equal(antenna_qualities, proxy.Antenna_Status_R)

    def test_read_Antenna_Use(self):
        """Verify if Antenna_Use_R is correctly retrieved"""
        antenna_use = numpy.array([AntennaUse.AUTO] * MAX_ANTENNA)
        with DeviceTestContext(
            afh.AFH, properties=self.AT_PROPERTIES, process=False
        ) as proxy:
            numpy.testing.assert_equal(antenna_use, proxy.Antenna_Use_R)

    def test_read_Antenna_Usage_Mask(self):
        """Verify if Antenna_Usage_Mask_R is correctly retrieved"""
        antenna_qualities = numpy.array([AntennaStatus.OK] * MAX_ANTENNA)
        antenna_use = numpy.array(
            [AntennaUse.ON] + [AntennaUse.AUTO] * (MAX_ANTENNA - 1)
        )
        antenna_properties = {
            "Antenna_Status": antenna_qualities.tolist(),
            "Antenna_Use": antenna_use.tolist(),
        }
        with DeviceTestContext(
            afh.AFH,
            properties={**self.AT_PROPERTIES, **antenna_properties},
            process=False,
        ) as proxy:
            numpy.testing.assert_equal(
                numpy.array([True] * MAX_ANTENNA), proxy.Antenna_Usage_Mask_R
            )

    def test_read_Antenna_Usage_Mask_only_one_functioning_antenna(self):
        """Verify if Antenna_Usage_Mask_R (only first antenna is OK) is correctly retrieved"""
        antenna_qualities = numpy.array(
            [AntennaStatus.OK] + [AntennaStatus.BROKEN] * (MAX_ANTENNA - 1)
        )
        antenna_use = numpy.array(
            [AntennaUse.ON] + [AntennaUse.AUTO] * (MAX_ANTENNA - 1)
        )
        antenna_properties = {
            "Antenna_Status": antenna_qualities.tolist(),
            "Antenna_Use": antenna_use.tolist(),
        }
        with DeviceTestContext(
            afh.AFH,
            properties={**self.AT_PROPERTIES, **antenna_properties},
            process=False,
        ) as proxy:
            numpy.testing.assert_equal(
                numpy.array([True] + [False] * (MAX_ANTENNA - 1)),
                proxy.Antenna_Usage_Mask_R,
            )

    def test_read_Antenna_Names(self):
        """Verify if Antenna_Names_R is correctly retrieved"""
        antenna_names = ["C0", "C1", "C2", "C3", "C4"]
        antenna_properties = {"Antenna_Names": antenna_names}
        with DeviceTestContext(
            afh.AFH,
            properties={**self.AT_PROPERTIES, **antenna_properties},
            process=False,
        ) as proxy:
            for i in range(len(antenna_names)):
                self.assertTrue(proxy.Antenna_Names_R[i] == f"C{i}")

    def test_Antenna_Mask_all_antennas(self):
        """Verify if Antenna_Mask_R is correctly retrieved"""
        with DeviceTestContext(
            afh.AFH,
            properties={**self.AT_PROPERTIES, **self.ANTENNA_PROPERTIES},
            process=False,
        ) as proxy:
            expected = [True] * MAX_ANTENNA
            numpy.testing.assert_equal(expected, proxy.antenna_set_to_mask("ALL"))

    def test_Antenna_Mask_half_antennas(self):
        """Verify if Antenna_Mask_R is correctly retrieved"""
        with DeviceTestContext(
            afh.AFH,
            properties={**self.AT_PROPERTIES, **self.ANTENNA_PROPERTIES},
            process=False,
        ) as proxy:
            expected = [True] * int(MAX_ANTENNA / 2) + [False] * int(MAX_ANTENNA / 2)
            numpy.testing.assert_equal(expected, proxy.antenna_set_to_mask("INNER"))
