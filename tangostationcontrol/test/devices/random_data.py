# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from numpy import random, double

# PyTango imports
from tango import DevState
from tango.server import run, Device, attribute

__all__ = ["RandomData", "main"]


class RandomData(Device):
    """
    Random data monitor point device
    """

    DIM_ARRAY = 1024  # x-axis dimension of a random values array

    def read(self):
        return random.random()

    def read_array(self):
        return random.rand(self.DIM_ARRAY).astype(double)

    # Attributes
    rnd1 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd2 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd3 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd4 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd5 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd6 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd7 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd8 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd9 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd10 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd11 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd12 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd13 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd14 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd15 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd16 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd17 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd18 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd19 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd20 = attribute(
        dtype="DevDouble",
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read,
    )

    rnd21 = attribute(
        dtype=("DevDouble",),
        max_dim_x=DIM_ARRAY,
        max_dim_y=1,
        polling_period=1000,
        period="1000",
        rel_change="0.1",
        abs_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        archive_abs_change="0.1",
        max_value="1.0",
        min_value="0.0",
        max_alarm="1.0",
        min_alarm="0.99",
        max_warning="0.99",
        min_warning="0.98",
        fget=read_array,
    )

    # General methods
    def init_device(self):
        """
        Initialises the attributes and properties of the RandomData device.
        """
        Device.init_device(self)
        self.set_state(DevState.OFF)

        self.rnd1.set_data_ready_event(True)
        self.set_change_event("rnd1", True, True)
        self.set_archive_event("rnd1", True, True)
        self.rnd2.set_data_ready_event(True)
        self.set_change_event("rnd2", True, True)
        self.set_archive_event("rnd2", True, True)
        self.rnd3.set_data_ready_event(True)
        self.set_change_event("rnd3", True, True)
        self.set_archive_event("rnd3", True, True)
        self.rnd4.set_data_ready_event(True)
        self.set_change_event("rnd4", True, True)
        self.set_archive_event("rnd4", True, True)
        self.rnd5.set_data_ready_event(True)
        self.set_change_event("rnd5", True, True)
        self.set_archive_event("rnd5", True, True)
        self.rnd6.set_data_ready_event(True)
        self.set_change_event("rnd6", True, True)
        self.set_archive_event("rnd6", True, True)
        self.rnd7.set_data_ready_event(True)
        self.set_change_event("rnd7", True, True)
        self.set_archive_event("rnd7", True, True)
        self.rnd8.set_data_ready_event(True)
        self.set_change_event("rnd8", True, True)
        self.set_archive_event("rnd8", True, True)
        self.rnd9.set_data_ready_event(True)
        self.set_change_event("rnd9", True, True)
        self.set_archive_event("rnd9", True, True)
        self.rnd10.set_data_ready_event(True)
        self.set_change_event("rnd10", True, True)
        self.set_archive_event("rnd10", True, True)
        self.rnd11.set_data_ready_event(True)
        self.set_change_event("rnd11", True, True)
        self.set_archive_event("rnd11", True, True)
        self.rnd12.set_data_ready_event(True)
        self.set_change_event("rnd12", True, True)
        self.set_archive_event("rnd12", True, True)
        self.rnd13.set_data_ready_event(True)
        self.set_change_event("rnd13", True, True)
        self.set_archive_event("rnd13", True, True)
        self.rnd14.set_data_ready_event(True)
        self.set_change_event("rnd14", True, True)
        self.set_archive_event("rnd14", True, True)
        self.rnd15.set_data_ready_event(True)
        self.set_change_event("rnd15", True, True)
        self.set_archive_event("rnd15", True, True)
        self.rnd16.set_data_ready_event(True)
        self.set_change_event("rnd16", True, True)
        self.set_archive_event("rnd16", True, True)
        self.rnd17.set_data_ready_event(True)
        self.set_change_event("rnd17", True, True)
        self.set_archive_event("rnd17", True, True)
        self.rnd18.set_data_ready_event(True)
        self.set_change_event("rnd18", True, True)
        self.set_archive_event("rnd18", True, True)
        self.rnd19.set_data_ready_event(True)
        self.set_change_event("rnd19", True, True)
        self.set_archive_event("rnd19", True, True)
        self.rnd20.set_data_ready_event(True)
        self.set_change_event("rnd20", True, True)
        self.set_archive_event("rnd20", True, True)
        self.rnd21.set_data_ready_event(True)
        self.set_change_event("rnd21", True, True)
        self.set_archive_event("rnd21", True, True)
        self.set_state(DevState.ON)

    def delete_device(self):
        self.set_state(DevState.OFF)


def main(args=None, **kwargs):
    """
    Main function of the RandomData module.
    """
    return run((RandomData,), args=args, **kwargs)
