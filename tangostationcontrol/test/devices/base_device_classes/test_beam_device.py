# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Awaitable
from typing import Callable
from typing import Optional
import datetime
import logging
import time

import numpy
import timeout_decorator
from tango import DevFailed

from tangostationcontrol.common.atomic import Atomic
from tangostationcontrol.devices.base_device_classes import beam_device

from unittest import mock
from test import base
from test.devices import device_base

logger = logging.getLogger()


class TestBeamDevice(device_base.DeviceTestCase):
    def setUp(self):
        # DeviceTestCase setUp patches lofar_device DeviceProxy
        super(TestBeamDevice, self).setUp()


async def default_update_callback(next_update):
    pass


class TestBeamTracker(base.TestCase):
    def setUp(self):
        super(TestBeamTracker, self).setUp()

    def get_sleep_time_test_single(
        self,
        interval: float,
        step: float,
        pre_sleep: float,
        next_update: datetime.datetime,
    ):
        """Take single result from get_sleep_time for interval test"""
        t_tracker = beam_device.BeamTracker(interval, 0, default_update_callback)

        time.sleep(pre_sleep)
        result = t_tracker._get_sleep_time(next_update)
        self.assertTrue(
            interval - step < result < interval,
            f"Result {result} not between {interval - step} and {interval}",
        )

    def test_get_sleep_time_expired(self):
        """Ensure sleep time remains below interval when target expired"""
        interval = 1
        step = interval / 4

        self.get_sleep_time_test_single(
            interval=interval,
            step=step,
            pre_sleep=interval,
            next_update=datetime.datetime.now() + datetime.timedelta(seconds=interval),
        )

    def test_get_sleep_time_distant_past(self):
        """Test sleep time within interval for extreme delta"""
        interval = 1
        step = interval / 4
        # Timestamp from epoch 0
        next_update = datetime.datetime.fromtimestamp(
            0
            # Reference next_update to current microseconds
            # Since interval is 1 second
        ) + datetime.timedelta(microseconds=datetime.datetime.now().microsecond)

        self.get_sleep_time_test_single(
            interval=interval, step=step, pre_sleep=0, next_update=next_update
        )

    def test_get_sleep_time_decrease(self):
        """Ensure sleep time decreases when nearing :py:attr:`next_update_in`

        :warning: If system jitter is above :py:attr:`~step` tests will
                  fail!
        """
        interval = 1
        step = interval / 4

        t_tracker = beam_device.BeamTracker(interval, 0, default_update_callback)

        next_update = datetime.datetime.now() + datetime.timedelta(seconds=interval)
        # Test for each sleep of size step the next_update time decreases
        # by step over the complete (non overflowing) range of interval
        for i in numpy.arange(interval, 0, -step):
            result = t_tracker._get_sleep_time(next_update)
            self.assertTrue(
                i - step < result < i, f"Result {result} not between {i-step} and {i}"
            )
            time.sleep(step)

    @timeout_decorator.timeout(5)
    def test_beamtracker_start_stop(self):
        """Test repeatedly starting and stopping the beamtracking

        Validates the beamtracker stops cleanly without error and that it starts
        every time
        """
        attempts = 5
        interval = 1
        step = interval / 4

        logger_patcher = mock.patch.object(beam_device, "logger")
        self.addCleanup(logger_patcher.stop)

        t_tracker = beam_device.BeamTracker(step, 0, default_update_callback)

        for _ in range(attempts):
            t_tracker.start()

            # Poll thread, start is none blocking
            self.assertIsNotNone(t_tracker.thread)
            while not t_tracker.is_alive():
                time.sleep(0.2)
            self.assertTrue(t_tracker.is_alive())

            m_logger = logger_patcher.start()

            # Stop is blocking, can check immediately
            t_tracker.stop()
            m_logger.error.assert_not_called()
            self.assertFalse(t_tracker.is_alive())

            logger_patcher.stop()

    def beamtracker_base(
        self,
        interval: float = 10,
        preparation_time: float = 0,
        setup: Optional[Callable[[beam_device.BeamTracker], None]] = None,
        test: Optional[Callable[[beam_device.BeamTracker], None]] = None,
        update: Callable[
            [datetime.datetime], Awaitable[None]
        ] = default_update_callback,
    ):
        """Parameterized base to perform beamtracker tests

        1. Initializes a beamtracker given interval, preparation_time and update and
           fault callbacks.
        2. Blocks the tracker from starting until setup method has been executed
        3. Execute test method
        4. Cleanup the beamtracker in a finally block
        """

        t_tracker = beam_device.BeamTracker(interval, preparation_time, update)
        try:
            with t_tracker.update_lock:
                t_tracker.start()
                if setup:
                    setup(t_tracker)

            while not t_tracker.is_alive():
                time.sleep(0.1)

            if test:
                test(t_tracker)
        finally:
            t_tracker.stop()

    def early_update_base(
        self,
        interval: float,
        last_update: datetime,
        datetime_now: datetime,
        test: Callable[[beam_device.BeamTracker], None],
    ):
        """Parameterized test for early pointing updates"""

        datetime_patcher = self.proxy_patch(beam_device, "datetime", autospec=True)
        m_datetime = datetime_patcher["mock"]

        m_datetime.datetime.min = last_update
        m_datetime.datetime.now.return_value = datetime_now

        def setup(beamtracker: beam_device.BeamTracker):
            beamtracker.done = Atomic(True)
            beamtracker.update_condition = mock.Mock()
            beamtracker._get_sleep_time = mock.Mock()

        self.beamtracker_base(interval, setup=setup, test=test)

    @timeout_decorator.timeout(5)
    def test_update_pointing_early(self):
        """Test that early updates sleep until the intended update time has passed"""

        interval = 1.0
        step = interval / 2

        start_time = datetime.datetime.now()
        last_update = start_time + datetime.timedelta(seconds=step)

        time_patcher = self.proxy_patch(beam_device, "time", autospec=True)
        m_time = time_patcher["mock"]

        def test(beamtracker: beam_device.BeamTracker):
            beamtracker.stop()
            self.assertFalse(beamtracker.is_alive())
            m_time.sleep.assert_called_with(step)

        self.early_update_base(
            interval=interval,
            last_update=last_update,
            datetime_now=start_time,
            test=test,
        )

    @timeout_decorator.timeout(5)
    def test_update_pointing_late(self):
        """Test that late updates do not cause any additional sleep"""

        interval = 1.0
        step = interval / 2

        last_update = datetime.datetime.now()
        datetime_now = last_update + datetime.timedelta(seconds=step)

        time_patcher = self.proxy_patch(beam_device, "time", autospec=True)
        m_time = time_patcher["mock"]

        def test(beamtracker: beam_device.BeamTracker):
            beamtracker.stop()
            self.assertFalse(beamtracker.is_alive())
            m_time.sleep.assert_not_called()

        self.early_update_base(
            interval=interval,
            last_update=last_update,
            datetime_now=datetime_now,
            test=test,
        )

    @timeout_decorator.timeout(5)
    def test_update_pointing_stale(self):
        """Test the :attr:`stale_pointing` functionality for beamtracking"""
        test_value = "update-pointing-stale"
        interval = 1.0

        datetime_patcher = self.proxy_patch(beam_device, "datetime", autospec=True)
        m_datetime = datetime_patcher["mock"]

        current_time = datetime.datetime.now()
        m_datetime.datetime.min = current_time
        m_datetime.datetime.now.side_effect = [
            current_time + datetime.timedelta(seconds=interval),
            test_value,
        ]

        m_update_pointing = mock.Mock()
        m_update_pointing.side_effect = [
            RuntimeError()  # Raise exception to stop thread
        ]

        def setup(beamtracker: beam_device.BeamTracker):
            beamtracker.done = Atomic(False)
            beamtracker.update_condition = mock.Mock()
            beamtracker._get_sleep_time = mock.Mock()
            beamtracker._update_pointing = m_update_pointing

        def test(beamtracker: beam_device.BeamTracker):
            while beamtracker.stale_pointing:
                time.sleep(0.1)

            m_update_pointing.assert_called_with(test_value)
            self.assertFalse(beamtracker.stale_pointing)

        self.beamtracker_base(interval, setup=setup, test=test)

    def next_update_in_base(self, interval: float, steps: int):
        """Test interval generation for next_update_in in micro steps (< 1 interval)

        :param interval: Interval for beam tracker updates in seconds
        :param steps: Number of steps to test,
        """

        step_time = interval / steps

        t_tracker = beam_device.BeamTracker(interval, 0.0, default_update_callback)

        # Test that next_update_in stays interval * 1 until, interval is reached
        now = datetime.datetime(2022, 1, 1, 12, 30, 0)
        for i in range(1, steps):
            now = now + datetime.timedelta(seconds=step_time)
            self.assertEqual(
                (
                    now
                    + datetime.timedelta(seconds=interval)
                    - (datetime.timedelta(seconds=step_time) * i)
                ),
                t_tracker.get_next_update_in(now),
            )

        # Last step should finally generate next_update_in time of interval * 2
        now = now + datetime.timedelta(seconds=step_time)
        self.assertEqual(
            now + (datetime.timedelta(seconds=interval)),
            t_tracker.get_next_update_in(now),
        )

    def test_next_update_in_second(self):
        self.next_update_in_base(1.0, 10)

    def test_next_update_in_10_seconds(self):
        self.next_update_in_base(10, 10)

    def test_monotonic_next_update(self):
        """Ensure next_update monotonically increases with intervals (>1 step)"""
        interval = 10
        t_tracker = beam_device.BeamTracker(interval, 0.0, default_update_callback)

        now = datetime.datetime(2022, 1, 1, 12, 30, 0)
        for _ in range(1, 30):
            now = now + datetime.timedelta(seconds=interval)
            self.assertEqual(
                now + (datetime.timedelta(seconds=interval)),
                t_tracker.get_next_update_in(now),
            )

    def test_update_throws_tango_exception(self):
        """Test if the beam tracker is resilient if update throws a Tango exception."""

        async def update_raise_fault(timestamp: datetime.datetime):
            raise DevFailed("foo")

        t_tracker = beam_device.BeamTracker(1.0, 0.0, update_raise_fault)

        t_tracker.Fault = mock.Mock()
        t_tracker._update_pointing(datetime.datetime.now())
        self.assertEqual(1, t_tracker.nr_update_exceptions)
        t_tracker.Fault.assert_not_called()
