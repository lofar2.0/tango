# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

import numpy
import time
from tango import DevFailed
from tango.test_context import DeviceTestContext
from attribute_wrapper.attribute_wrapper import AttributeWrapper
import timeout_decorator

from tangostationcontrol.devices.base_device_classes import opcua_device

from test.devices import device_base
from test.opcua import RunOPCUAServer

logger = logging.getLogger()


class RunOPCUADevice(device_base.DeviceTestCase):
    class TestDevice(opcua_device.OPCUADevice):
        float_R = AttributeWrapper(comms_annotation=["float_R"], datatype=numpy.float32)

        PROPERTIES = {
            "OPC_Server_Name": "127.0.0.1",
            "OPC_Server_Port": 12345,
            "OPC_Time_Out": 10,
            "OPC_Node_Path_Prefix": ["2:ServerInterfaces", "2:TestDevice"],
        }

    def setUp(self):
        super(RunOPCUADevice, self).setUp()

        self.test_device = opcua_device.OPCUADevice

    @timeout_decorator.timeout(10)
    def test_connect(self):
        """Test if device connects to the OPC-UA server."""

        with RunOPCUAServer(12345, "http://lofar.eu") as opcua_server:
            with DeviceTestContext(
                self.TestDevice, properties=self.TestDevice.PROPERTIES, process=True
            ) as proxy:
                proxy.initialise()

                self.assertTrue(proxy.connected_R)
                self.assertEqual(20.0, proxy.float_R)

                proxy.off()

    @timeout_decorator.timeout(30)
    def test_read_after_disconnect(self):
        """Test if reads fail after disconnecting from the OPC-UA server."""

        with DeviceTestContext(
            self.TestDevice, properties=self.TestDevice.PROPERTIES, process=True
        ) as proxy:
            with RunOPCUAServer(12345, "http://lofar.eu") as opcua_server:
                proxy.initialise()

            while proxy.connected_R:
                logger.debug("Sleeping to wait for disconnect")
                time.sleep(0.5)

            with self.assertRaises(DevFailed):
                _ = proxy.float_R

    @timeout_decorator.timeout(30)
    def test_read_after_reconnect(self):
        """Test if reads succeeds after reconnecting to the OPC-UA server."""

        with DeviceTestContext(
            self.TestDevice, properties=self.TestDevice.PROPERTIES, process=True
        ) as proxy:
            with RunOPCUAServer(12345, "http://lofar.eu") as opcua_server:
                proxy.initialise()

            while proxy.connected_R:
                logger.debug("Sleeping to wait for disconnect")
                time.sleep(0.5)

            with RunOPCUAServer(12345, "http://lofar.eu") as opcua_server:
                while not proxy.connected_R:
                    logger.debug("Sleeping to wait for reconnect")
                    time.sleep(0.5)

                self.assertEqual(20.0, proxy.float_R)
