# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from test import base
import numpy

from tangostationcontrol.common.constants import N_rcu
from tangostationcontrol.devices.base_device_classes.mapper import RecvDeviceWalker


class TestRecvDeviceWalker(base.TestCase):
    """Test class for RecvDeviceWalker"""

    class MockDeviceProxy:
        """Mock of DeviceProxy that simulates recv.ANT_mask_RW."""

        @property
        def ANT_mask_RW(self):
            return self.ant_mask

        @ANT_mask_RW.setter
        def ANT_mask_RW(self, value):
            self.ant_mask = value

        def __init__(self, index):
            self.visited = False
            self.index = index

            # fill with a value we don't use, to make sure
            # there will be a difference when set
            self.ant_mask = numpy.array([[False, True, True]] * N_rcu)

            # save the value we originally use
            self.original_ant_mask = self.ant_mask

    def test_recv_masks_identity_mapping(self):
        """Test whether a straight setup works."""

        control_to_recv_mapping = numpy.array([[1, 0], [1, 1], [1, 2]])
        antenna_usage_mask = numpy.array([True, True, True])

        sut = RecvDeviceWalker(control_to_recv_mapping, antenna_usage_mask)

        expected = numpy.array(
            [[[True, True, True]] + [[False, False, False]] * (N_rcu - 1)]
        )
        numpy.testing.assert_equal(expected, sut.recv_ant_masks())

    def test_recv_masks_antenna_usage_mask(self):
        """Test whether the antenna_usage_mask is respected."""

        control_to_recv_mapping = numpy.array([[1, 0], [1, 1], [1, 2]])
        antenna_usage_mask = numpy.array([False, True, False])

        sut = RecvDeviceWalker(control_to_recv_mapping, antenna_usage_mask)

        expected = numpy.array(
            [[[False, True, False]] + [[False, False, False]] * (N_rcu - 1)]
        )
        numpy.testing.assert_equal(expected, sut.recv_ant_masks())

    def test_recv_masks_control_to_recv_mapping(self):
        """Test whether control_to_recv_mapping is respected."""

        control_to_recv_mapping = numpy.array([[1, 0], [2, 1], [1, 2]])
        antenna_usage_mask = numpy.array([True, True, True])

        sut = RecvDeviceWalker(control_to_recv_mapping, antenna_usage_mask)

        expected = numpy.array(
            [
                [[True, False, True]] + [[False, False, False]] * (N_rcu - 1),
                [[False, True, False]] + [[False, False, False]] * (N_rcu - 1),
            ]
        )
        numpy.testing.assert_equal(expected, sut.recv_ant_masks())

    def test_walk_receivers(self):
        """Test walk_receivers on multiple recv_proxies."""

        control_to_recv_mapping = numpy.array([[1, 0], [2, 1], [1, 2]])
        antenna_usage_mask = numpy.array([True, True, True])

        sut = RecvDeviceWalker(control_to_recv_mapping, antenna_usage_mask)

        recv_proxies = [TestRecvDeviceWalker.MockDeviceProxy(n + 1) for n in range(2)]

        def visitor(recv_proxy):
            recv_proxy.visited = True

            # is our mask set correctly?
            if recv_proxy.index == 1:
                expected = numpy.array(
                    [[True, False, True]] + [[False, False, False]] * (N_rcu - 1)
                )
            elif recv_proxy.index == 2:
                expected = numpy.array(
                    [[False, True, False]] + [[False, False, False]] * (N_rcu - 1)
                )

            numpy.testing.assert_equal(expected, recv_proxy.ANT_mask_RW)

        sut.walk_receivers(recv_proxies, visitor)

        # make sure both recv_proxies were visited
        self.assertTrue(recv_proxies[0].visited)
        self.assertTrue(recv_proxies[1].visited)

        # make sure both masks were restored
        numpy.testing.assert_equal(
            recv_proxies[0].original_ant_mask, recv_proxies[0].ant_mask
        )
        numpy.testing.assert_equal(
            recv_proxies[1].original_ant_mask, recv_proxies[1].ant_mask
        )

    def test_walk_receivers_restores_mask_on_exception(self):
        """Test whether walk_receivers() also restores the recv_proxy.ANT_mask_RW
        if the visitor function throws."""
        control_to_recv_mapping = numpy.array([[1, 0], [2, 1], [1, 2]])
        antenna_usage_mask = numpy.array([True, True, True])

        sut = RecvDeviceWalker(control_to_recv_mapping, antenna_usage_mask)

        recv_proxies = [TestRecvDeviceWalker.MockDeviceProxy(n + 1) for n in range(2)]

        class MyException(Exception):
            """A exception noone can raise but us."""

        def visitor(recv_proxy):
            raise MyException("foo")

        with self.assertRaises(MyException):
            sut.walk_receivers(recv_proxies, visitor)

        # make sure no mask was disturbed
        numpy.testing.assert_equal(
            recv_proxies[0].original_ant_mask, recv_proxies[0].ant_mask
        )
        numpy.testing.assert_equal(
            recv_proxies[1].original_ant_mask, recv_proxies[1].ant_mask
        )
