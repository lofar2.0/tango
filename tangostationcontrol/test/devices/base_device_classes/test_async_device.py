# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import numpy
from tango.server import attribute
from tango.server import device_property
from tango import DevSource

from tango.test_context import DeviceTestContext

from tangostationcontrol.devices.base_device_classes import async_device

from test.devices.base_device_classes.test_lofar_device import TestLofarDevice


class TestAsyncDevice(TestLofarDevice):
    """Inherits the tests from TestLofarDevice"""

    property_length = 96

    def setUp(self):
        # DeviceTestCase setUp patches lofar_device DeviceProxy
        super(TestAsyncDevice, self).setUp()

        # override
        self.test_device = async_device.AsyncDevice

    def test_read_attribute_derived_property(self):
        """Assert properties can be read in attribute read functions

        This is critical for AsyncDevice as this is known to break when an `incorrect`
        inheritance chain is used.
        """

        class MyAsyncDevice(self.test_device):
            example_property = device_property(
                dtype="DevVarBooleanArray",
                doc="Test Reading from attribute read function",
                mandatory=True,
            )

            example_attribute = attribute(
                doc="Attribute with fget read function that reads a property",
                dtype=numpy.uint32,
                fget="get_example_attribute",
            )

            def get_example_attribute(self):
                return len(self.example_property)

        property_value = [False] * self.property_length
        db_properties = {"example_property": property_value}
        with DeviceTestContext(
            MyAsyncDevice,
            properties={**db_properties},
            process=False,
        ) as proxy:
            self.assertEqual(proxy.example_attribute, self.property_length)

    def test_poll_attribute(self):
        """Test whether poll_attribute really polls registered attributes."""

        class MyAsyncDevice(self.test_device):
            async def init_device(self):
                await super().init_device()

                self._A_read_counter = 0

                self.attribute_poller.register("A")

            @attribute(dtype=float)
            async def A(self):
                self._A_read_counter += 1
                return 42.0

            @attribute(dtype=int)
            async def A_read_counter(self):
                return self._A_read_counter

        with DeviceTestContext(MyAsyncDevice, process=False) as proxy:
            # make sure we don't get a cached result if the
            # poll_attribute was already called by Tango's polling loop.
            proxy.set_source(DevSource.DEV)

            proxy.initialise()
            proxy.on()

            # force poll
            proxy.poll_attributes()

            # check whether A was read. It could have been read by a periodic
            # call from poll_attributes, but that's what we're testing so it's ok.
            self.assertGreaterEqual(proxy.A_read_counter, 1)

    def test_async_read_attribute(self):
        """Assess whether async_read_attribute works on synchronous and green attributes."""

        class MyAsyncDevice(self.test_device):
            # attributes that provide values to read
            @attribute(dtype=numpy.uint32, read_green_mode=False)
            def synchronous_attribute(self):
                return 42

            @attribute(dtype=numpy.uint32, read_green_mode=True)
            async def green_attribute(self):
                return 43

            # attributes that read the values provided
            @attribute(dtype=numpy.uint32)
            async def read_synchronous_attribute(self):
                return await self.async_read_attribute("synchronous_attribute")

            @attribute(dtype=numpy.uint32)
            async def read_green_attribute(self):
                return await self.async_read_attribute("green_attribute")

        with DeviceTestContext(MyAsyncDevice, process=False) as proxy:
            # verify that the attributes propagate through async_read_attribute
            self.assertEqual(proxy.read_synchronous_attribute, 42)
            self.assertEqual(proxy.read_green_attribute, 43)


class TestAsyncReadAttribute(TestLofarDevice):
    """Assess whether async_read_attribute works if we interact with the attribute in other ways."""

    def setUp(self):
        # DeviceTestCase setUp patches lofar_device DeviceProxy
        super(TestAsyncReadAttribute, self).setUp()

        # override
        self.test_device = async_device.AsyncDevice

    class MyAsyncDevice(async_device.AsyncDevice):
        async def init_device(self):
            await super().init_device()

            self.value = 1

        @attribute(dtype=numpy.uint32, read_green_mode=True)
        async def green_attribute(self):
            return self.value

        @green_attribute.setter
        async def write_green_attribute(self, value):
            self.value = value

        @attribute(dtype=numpy.uint32)
        async def read_green_attribute(self):
            return await self.async_read_attribute("green_attribute")

    def test_after_write(self):
        with DeviceTestContext(self.MyAsyncDevice, process=False) as proxy:
            # check for change after write, without reading first
            proxy.green_attribute = 2
            self.assertEqual(proxy.read_green_attribute, 2)

    def test_after_read(self):
        with DeviceTestContext(self.MyAsyncDevice, process=False) as proxy:
            # check for change after read
            self.assertEqual(proxy.green_attribute, 1)
            self.assertEqual(proxy.read_green_attribute, 1)

    def test_after_write_read(self):
        with DeviceTestContext(self.MyAsyncDevice, process=False) as proxy:
            # check for change after write & read
            proxy.green_attribute = 3
            self.assertEqual(proxy.green_attribute, 3)
            self.assertEqual(proxy.read_green_attribute, 3)
