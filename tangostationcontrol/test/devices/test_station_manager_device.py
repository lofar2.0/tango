# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
import time

from timeout_decorator import timeout_decorator

from unittest import mock

from test.devices import device_base

from tango import DevState, DevFailed
from tango.test_context import DeviceTestContext

from tangostationcontrol.devices import station_manager
from tangostationcontrol.states.station_state_enum import StationStateEnum


class TestStationManagerDevice(device_base.DeviceTestCase):
    """Unit test class for device Station Manager"""

    def setUp(self):
        super(TestStationManagerDevice, self).setUp()

        # self.m_offstate = self.proxy_patch(off, "OffState")['mock']

    def test_init_on(self):
        """Test whether ON state is reached at device initialisation"""
        with DeviceTestContext(
            station_manager.StationManager, process=False, timeout=10
        ) as proxy:
            proxy.init()

            self.assertEqual(proxy.state(), DevState.ON)

    @staticmethod
    def block():
        time.sleep(30)

    @staticmethod
    def patched__initialise_power_hierarchy(self):
        self.init(
            self.get_name(), continue_on_failure=self.Suppress_State_Transition_Failures
        )
        self.off_to_hibernate = lambda: time.sleep(30)

    @timeout_decorator.timeout(10)
    def test_state_transition_timeout(self):
        timeout = 1

        with mock.patch.object(
            station_manager.StationManager, "_initialise_power_hierarchy", autospec=True
        ) as m_off:
            m_off.side_effect = self.patched__initialise_power_hierarchy
            with DeviceTestContext(
                station_manager.StationManager, process=False, timeout=60
            ) as proxy:
                proxy.hibernate_transition_timeout_RW = timeout
                self.assertEqual(timeout, proxy.hibernate_transition_timeout_RW)
                self.assertRaises(DevFailed, proxy.station_hibernate)

    def test_transitions_lock(self):
        """Test whether the lock mechanism ensure only one transition
        at a time is executed"""

        async def dummy_station_hibernate(lock, shared_var):
            """Dummy method to simulate station_hibernate lock"""
            async with lock:
                shared_var += 1
                await asyncio.sleep(0.1)  # Simulate some asynchronous operation
                shared_var = 99
                await asyncio.sleep(0.1)  # Simulate some asynchronous operation
                shared_var -= 1

        async def dummy_station_standby(lock, shared_var):
            """Dummy method to simulate station_standby lock"""
            async with lock:
                shared_var += 2
                await asyncio.sleep(0.1)  # Simulate some asynchronous operation
                shared_var = 99
                await asyncio.sleep(0.1)  # Simulate some asynchronous operation
                shared_var -= 2

        async def test_concurrent_transitions(lock, shared_var):
            """Execute the two mocked coroutines"""
            await dummy_station_hibernate(lock, shared_var)
            await dummy_station_standby(lock, shared_var)

        def test_lock_serialization(lock, shared_var):
            """Test whether lock serialization is correctly working"""
            # Execute concurrent transitions multiple times to test serialization
            for _ in range(10):
                shared_var = 0  # reset variable
                asyncio.run(test_concurrent_transitions(lock, shared_var))
                self.assertEqual(
                    shared_var,
                    0,
                    msg=f"Lock serialization failed. Shared variable={shared_var}",
                )

        with DeviceTestContext(
            station_manager.StationManager, process=False, timeout=10
        ) as proxy:
            proxy.init()

            shared_var = 0  # Initialize shared variable
            lock = asyncio.Lock()  # Create a lock

            test_lock_serialization(lock, shared_var)

            # Verify that the device is in the correct state after the transitions
            self.assertEqual(proxy.state(), DevState.ON)

    def test_transitions_protection(self):
        """Test that the protection prevents state transitions"""

        with DeviceTestContext(
            station_manager.StationManager, process=False, timeout=10
        ) as proxy:
            proxy.init()
            proxy.protection_lock_RW = True

            self.assertRaisesRegex(  # Check once for presence of correct error message
                DevFailed, "protection_lock_RW", proxy.station_hibernate
            )
            self.assertRaises(DevFailed, proxy.station_standby)
            self.assertRaises(DevFailed, proxy.station_on)

            proxy.protection_lock_RW = False

            proxy.station_hibernate()
            self.assertEqual(StationStateEnum.HIBERNATE, proxy.station_state_R)
            proxy.station_standby()
            self.assertEqual(StationStateEnum.STANDBY, proxy.station_state_R)
            proxy.station_on()
            self.assertEqual(StationStateEnum.ON, proxy.station_state_R)

            proxy.protection_lock_RW = True

            proxy.station_standby()
            self.assertRaises(DevFailed, proxy.station_on)

            proxy.station_hibernate()
            self.assertRaises(DevFailed, proxy.station_standby)
            self.assertRaises(DevFailed, proxy.station_on)

            proxy.station_off()

            self.assertRaises(DevFailed, proxy.station_hibernate)
            self.assertRaises(DevFailed, proxy.station_standby)
            self.assertRaises(DevFailed, proxy.station_on)
