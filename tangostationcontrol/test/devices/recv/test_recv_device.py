# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from test.devices import device_base
import numpy
from tango.test_context import DeviceTestContext
from tango import DevFailed

from tangostationcontrol.common.constants import N_rcu, N_rcu_inp, N_elements
from tangostationcontrol.devices.recv import recvh


class TestRecvDevice(device_base.DeviceTestCase):
    # some dummy values for mandatory properties
    RECV_PROPERTIES = {
        "OPC_Server_Name": "example.com",
        "OPC_Server_Port": 4840,
        "OPC_Time_Out": 5.0,
    }

    def setUp(self):
        # DeviceTestCase setUp patches lofar_device DeviceProxy
        super(TestRecvDevice, self).setUp()

    def test_calculate_HBAT_bf_delay_steps(self):
        """Verify HBAT beamforming calculations are correctly executed"""
        with DeviceTestContext(
            recvh.RECVH, properties=self.RECV_PROPERTIES, process=False
        ) as proxy:
            delays = numpy.random.rand(N_rcu * N_rcu_inp, N_elements).flatten()
            HBAT_bf_delay_steps = proxy.calculate_HBAT_bf_delay_steps(delays)
            self.assertEqual(3072, len(HBAT_bf_delay_steps))  # 96x32=3072

    def test_get_rcu_band_from_filter(self):
        """Verify filter lookup table values are correctly retrieved"""
        with DeviceTestContext(
            recvh.RECVH, properties=self.RECV_PROPERTIES, process=False
        ) as proxy:
            filter_name = "HBA_170_230"
            self.assertEqual(1, proxy.get_rcu_band_from_filter(filter_name))

            with self.assertRaises(DevFailed):
                _ = proxy.get_rcu_band_from_filter("MOCK_FILTER")
