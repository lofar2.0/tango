# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

# TODO(Corne): Remove sys.path.append hack once packaging is in place!
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.append(parentdir)

import time
import numpy
from tango import DevState, Util
from tango.server import run, Device, attribute

__all__ = ["MonitoringPerformanceDevice", "main"]

POLLING_THREADS = 100
ARRAY_SIZE = 2000000


class MonitoringPerformanceDevice(Device):
    global ARRAY_SIZE

    def read_array(self):
        print("{} {}".format(time.time(), self.get_name()))
        return self._array

    array1_r = attribute(
        dtype=(numpy.double,),
        max_dim_x=ARRAY_SIZE,
        period="1000",
        rel_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        max_value="1.0",
        min_value="0.0",
        fget=read_array,
    )

    array2_r = attribute(
        dtype=(numpy.double,),
        max_dim_x=ARRAY_SIZE,
        period="1000",
        rel_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        max_value="1.0",
        min_value="0.0",
        fget=read_array,
    )

    array3_r = attribute(
        dtype=(numpy.double,),
        max_dim_x=ARRAY_SIZE,
        period="1000",
        rel_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        max_value="1.0",
        min_value="0.0",
        fget=read_array,
    )

    array4_r = attribute(
        dtype=(numpy.double,),
        max_dim_x=ARRAY_SIZE,
        period="1000",
        rel_change="0.1",
        archive_period="1000",
        archive_rel_change="0.1",
        max_value="1.0",
        min_value="0.0",
        fget=read_array,
    )

    def init_device(self):
        Device.init_device(self)

        util = Util.instance()
        print(
            "Current polling thread pool size = {}".format(
                util.get_polling_threads_pool_size()
            )
        )
        util.set_polling_threads_pool_size(POLLING_THREADS)
        print(
            "New polling thread pool size = {}".format(
                util.get_polling_threads_pool_size()
            )
        )
        print("Array size = {}".format(ARRAY_SIZE))

        self.set_state(DevState.OFF)

        self._array = numpy.zeros(ARRAY_SIZE)

        self.array1_r.set_data_ready_event(True)
        self.set_change_event("array1_r", True, True)
        self.set_archive_event("array1_r", True, True)

        self.array2_r.set_data_ready_event(True)
        self.set_change_event("array2_r", True, True)
        self.set_archive_event("array2_r", True, True)

        self.array3_r.set_data_ready_event(True)
        self.set_change_event("array3_r", True, True)
        self.set_archive_event("array3_r", True, True)

        self.array4_r.set_data_ready_event(True)
        self.set_change_event("array4_r", True, True)
        self.set_archive_event("array4_r", True, True)

        self.set_state(DevState.ON)

    def delete_device(self):
        self.set_state(DevState.OFF)


def main(args=None, **kwargs):
    return run((MonitoringPerformanceDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
