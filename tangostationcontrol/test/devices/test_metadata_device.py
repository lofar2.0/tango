# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from unittest.mock import ANY, patch
from unittest import mock
from unittest.mock import call

from tango import DevFailed, DeviceProxy
from tango.test_context import DeviceTestContext

from tangostationcontrol.devices.base_device_classes import lofar_device
from tangostationcontrol.devices import metadata

from test.devices import device_base


@patch("tango.Database")
class TestMetadataDevice(device_base.DeviceTestCase):

    TEST_PROPERTIES = {
        "metadata_port": "6001",
    }

    def setUp(self):
        super(TestMetadataDevice, self).setUp()

    def test_start_stop(self, tango_db):
        with DeviceTestContext(
            metadata.Metadata,
            properties=self.TEST_PROPERTIES,
            process=False,
            timeout=10,
        ) as proxy:
            proxy.initialise()

            # NB: This turns on periodic publishing of metadata,
            #     resulting in elements on the publisher queue.
            proxy.on()

            self.assertTrue(proxy.is_publisher_running_R)
            self.assertFalse(proxy.is_publisher_stopping_R)

            proxy.off()

            self.assertFalse(proxy.is_publisher_running_R)
            self.assertFalse(proxy.is_publisher_stopping_R)
            self.assertAlmostEqual(0.0, proxy.publisher_queue_fill_fraction_R)

    @mock.patch.object(metadata, "ZeroMQPublisher", autospec=True)
    def test_stop_timeout_exception(self, m_publisher, tango_db):
        """Test that publisher threads failing to stop don't block forever"""

        m_publisher.return_value.is_done = False

        with DeviceTestContext(
            metadata.Metadata,
            properties=self.TEST_PROPERTIES,
            process=False,
            timeout=10,
        ) as proxy:
            proxy.initialise()
            proxy.on()

            self.assertRaises(DevFailed, proxy.off)

    @mock.patch.object(lofar_device, "EventSubscriptions", autospec=True)
    def test_register_change_events(self, m_events, tango_db):
        """Test that for each device attribute pair change events are registered"""

        tango_db.return_value.get_device_exported_for_class.side_effect = lambda cls: [
            f"stat/{cls.casefold()}/1"
        ]

        m_devices = []
        t_calls = []
        for device_cls, attributes in metadata.Metadata.METADATA_CONFIG.items():
            m_device = mock.Mock(spec_set=DeviceProxy)
            m_device.name.return_value = f"stat/{device_cls.casefold()}/1"
            m_device.get_property.return_value = {}
            m_device.info.return_value.dev_class = device_cls
            m_devices.append(m_device)
            for attribute_name in attributes:
                t_calls.append(call(m_device, attribute_name, ANY, ANY))

        with DeviceTestContext(
            metadata.Metadata,
            properties=self.TEST_PROPERTIES,
            process=False,
            timeout=10,
        ) as proxy:
            self.device_proxy_mock["object"].side_effect = m_devices

            proxy.boot()

            m_events.return_value.subscribe_change_event.assert_has_calls(t_calls)

            proxy.send_metadata()
