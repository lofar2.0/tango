# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import numpy
import numpy.testing

# Internal test imports
from test.devices import device_base

from tangostationcontrol.common.constants import (
    CLK_160_MHZ,
    CLK_200_MHZ,
    N_pn,
    S_pn,
    N_subbands,
)
from tangostationcontrol.common.sdp import subband_frequency
from tangostationcontrol.devices.sdp.sdp import SDP


class TestSDPDevice(device_base.DeviceTestCase):
    def _verify_subband_frequencies(self, clock, nyquist_zone, spectral_inversion):
        subband_frequencies = SDP._subband_frequencies(
            clock, nyquist_zone, spectral_inversion
        )

        # explicitly construct expected output. We can count on the subband_frequency
        # to work as that is tested elsewhere.
        expected_frequencies = numpy.zeros(
            (N_pn, S_pn, N_subbands), dtype=numpy.float64
        )
        for sb in range(N_subbands):
            for fpga_nr in range(N_pn):
                for antenna_nr in range(S_pn):
                    expected_frequencies[fpga_nr, antenna_nr, sb] = subband_frequency(
                        sb,
                        clock,
                        nyquist_zone[fpga_nr, antenna_nr],
                        spectral_inversion[fpga_nr, antenna_nr],
                    )

        numpy.testing.assert_array_almost_equal(
            subband_frequencies, expected_frequencies.reshape(subband_frequencies.shape)
        )

    def test_subband_frequencies_200mhz(self):
        clock = CLK_200_MHZ
        nyquist_zone = numpy.array([[1] * S_pn] * N_pn)
        spectral_inversion = numpy.array([[True] * S_pn] * N_pn)

        self._verify_subband_frequencies(clock, nyquist_zone, spectral_inversion)

    def test_subband_frequencies_160mhz(self):
        clock = CLK_160_MHZ
        nyquist_zone = numpy.array([[1] * S_pn] * N_pn)
        spectral_inversion = numpy.array([[True] * S_pn] * N_pn)

        self._verify_subband_frequencies(clock, nyquist_zone, spectral_inversion)

    def test_subband_frequencies_mixed_fpga_settings(self):
        """Test different settings for the FPGAs to verify the ordering."""

        clock = CLK_200_MHZ
        nyquist_zone = numpy.array([[1] * S_pn] * N_pn)
        spectral_inversion = numpy.array([[True] * S_pn] * N_pn)

        # change settings for FPGAs beyond 4
        nyquist_zone[4:, :] = 0
        spectral_inversion[4:, :] = False

        self._verify_subband_frequencies(clock, nyquist_zone, spectral_inversion)

    def test_subband_frequencies_mixed_input_settings(self):
        """Test different settings for the inputs of the FPGAs to verify the ordering."""

        clock = CLK_200_MHZ
        nyquist_zone = numpy.array([[1] * S_pn] * N_pn)
        spectral_inversion = numpy.array([[True] * S_pn] * N_pn)

        # change settings for inputs beyond 4
        nyquist_zone[:, 4:] = 0
        spectral_inversion[:, 4:] = False

        self._verify_subband_frequencies(clock, nyquist_zone, spectral_inversion)
