# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

# Internal test imports
from test.devices import device_base


class TestDigitalBeamDevice(device_base.DeviceTestCase):
    def setUp(self):
        super(TestDigitalBeamDevice, self).setUp()
