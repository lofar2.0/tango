# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import datetime
import json
import time
import zmq
from unittest.mock import patch

from tango import server
from tango.test_context import DeviceTestContext

from test.devices import device_base

from tangostationcontrol.devices.sdp.statistics import Statistics


class TestStatisticsDevice(device_base.DeviceTestCase):
    def setUp(self):
        super(TestStatisticsDevice, self).setUp()

    def test_python_bug(self):
        """Python is bugged and super() calls in ctypes fail

        This is the same type of issues that prevents combining metaclasses
        DeviceMeta and ABCMeta

        https://github.com/waveform80/picamera/issues/355
        https://bugs.python.org/issue29270
        """
        import ctypes as ct

        try:

            class TestSubclass(ct.c_uint32):
                def __repr__(self):
                    return super().__repr__()

        except Exception as e:
            self.assertIsInstance(e, TypeError)

    def test_instance_statistics(self):
        """Test that we can create a statistics device"""

        with DeviceTestContext(
            Statistics,
            process=True,
            properties={
                "OPC_Server_Name": "localhost",
                "OPC_Server_Port": 0,
                "OPC_Time_Out": 300,
                "Statistics_Client_UDP_Port": 0,
                "Statistics_Client_Hostname": "127.0.0.1",
            },
        ) as proxy:
            self.assertTrue(proxy.ping() >= 0)

    @patch(
        "tangostationcontrol.devices.base_device_classes.opcua_device.OPCUADevice._connect_opcua"
    )
    def test_zmq_teardown_no_connect(self, m_connect_opcua):
        """Test whether we can tear down if ZMQ does not connect."""

        with DeviceTestContext(
            Statistics,
            process=True,
            properties={
                "OPC_Server_Name": "127.0.0.1",
                "OPC_Server_Port": 12345,
                "OPC_Time_Out": 300,
                "Statistics_Client_UDP_Port": 0,
                "Statistics_Client_Hostname": "127.0.0.1",
            },
        ) as proxy:
            proxy.initialise()
            proxy.off()

    @patch(
        "tangostationcontrol.devices.base_device_classes.opcua_device.OPCUADevice._connect_opcua"
    )
    @patch(
        "tangostationcontrol.devices.sdp.statistics.AsyncZeroMQSubscriber.async_recv"
    )
    def test_zmq_messages(self, m_async_recv, m_connect_opcua):
        """Test whether ZMQ messages are received and provided to handle_statistics_message."""

        topic = "foo/bar"
        message_payload = {"a": "b"}

        m_async_recv.side_effect = [
            [topic, datetime.datetime.now(), json.dumps(message_payload).encode()],
            zmq.ContextTerminated,
        ]

        class TestStatistics(Statistics):
            def __init__(self, cl, name):
                super().__init__(cl, name)

                self.topics = []
                self.messages = []

            def handle_statistics_message(self, topic: str, message: dict):
                self.topics.append(topic)
                self.messages.append(message)

            @server.command()
            def test(device):
                self.assertListEqual([topic], device.topics)
                self.assertListEqual([message_payload], device.messages)

        with DeviceTestContext(
            TestStatistics,
            process=True,
            properties={
                "OPC_Server_Name": "127.0.0.1",
                "OPC_Server_Port": 12345,
                "OPC_Time_Out": 300,
                "Statistics_Client_UDP_Port": 0,
                "Statistics_Client_Hostname": "127.0.0.1",
            },
        ) as proxy:
            proxy.initialise()

            # the zmq.ContextTerminated will kill the zmq thread, so
            # if that dies the messages were received
            while proxy.zmq_thread_running_R:
                time.sleep(0.1)

            # check for arrival
            proxy.test()

            proxy.off()
