# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import datetime
import json
import numpy
import time
import zmq
from unittest.mock import patch

from tango.test_context import DeviceTestContext

from test.devices import device_base

from tangostationcontrol.common.constants import MAX_INPUTS
from tangostationcontrol.devices.sdp.xst import XST


class TestXSTDevice(device_base.DeviceTestCase):
    def setUp(self):
        super(TestXSTDevice, self).setUp()

    @patch(
        "tangostationcontrol.devices.base_device_classes.opcua_device.OPCUADevice._connect_opcua"
    )
    @patch(
        "tangostationcontrol.devices.sdp.statistics.AsyncZeroMQSubscriber.async_recv"
    )
    def test_zmq_messages(self, m_async_recv, m_connect_opcua):
        """Test whether ZMQ messages are received and provided to handle_statistics_message."""

        topic = "xst/lba/cs001"

        # test payload
        xst_data = numpy.ones((MAX_INPUTS, MAX_INPUTS), dtype=numpy.complex64) + 1.3j

        timestamp = datetime.datetime.now()
        integration_interval = 1.23
        subband = 300
        message_payload = {
            "timestamp": timestamp.isoformat(),
            "station": "cs001",
            "antenna_field": "lba",
            "type": "xst",
            "xst_data_real": xst_data.real.tolist(),
            "xst_data_imag": xst_data.imag.tolist(),
            "subband": 300,
            "integration_interval": integration_interval,
        }

        m_async_recv.side_effect = [
            [topic, datetime.datetime.now(), json.dumps(message_payload).encode()],
            zmq.ContextTerminated,
        ]

        with DeviceTestContext(
            XST,
            process=True,
            properties={
                "OPC_Server_Name": "127.0.0.1",
                "OPC_Server_Port": 12345,
                "OPC_Time_Out": 300,
                "Statistics_Client_UDP_Port": 0,
                "Statistics_Client_Hostname": "127.0.0.1",
            },
        ) as proxy:
            proxy.initialise()

            # the zmq.ContextTerminated will kill the zmq thread, so
            # if that dies the messages were received
            while proxy.zmq_thread_running_R:
                time.sleep(0.1)

            # check if statistics arrived and are exposed
            self.assertAlmostEqual(timestamp.timestamp(), proxy.xst_timestamp_r)
            self.assertAlmostEqual(
                integration_interval, proxy.xst_integration_interval_r
            )
            self.assertEqual(subband, proxy.xst_subband_r)
            numpy.testing.assert_equal(xst_data.real, proxy.xst_real_r)
            numpy.testing.assert_equal(xst_data.imag, proxy.xst_imag_r)

            proxy.off()
