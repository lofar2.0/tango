# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import numpy
import numpy.testing

from tangostationcontrol.common.constants import (
    CLK_200_MHZ,
    A_pn,
    N_pn,
    N_pol,
    N_beamlets_ctrl,
    N_bdo_destinations_mm,
    N_subbands,
)
from tangostationcontrol.common.sdp import weight_to_complex
from tangostationcontrol.devices.sdp.beamlet import Beamlet

from test import base


class TestBeamletDevice(base.TestCase):
    def test_calculate_bf_weights_small_numbers(self):
        # 2 beamlets, 3 antennas. The antennas are 1 second apart.
        delays = numpy.array(
            [
                [1.0, 2.0, 3.0],
                [1.0, 2.0, 3.0],
            ]
        )

        # the frequency of the signal is 1.0 Hz and 0.5 Hz respectively,
        # so the antennas will be either in phase or in opposite phase
        beamlet_frequencies = numpy.array(
            [
                [1.0, 1.0, 1.0],
                [0.5, 0.5, 0.5],
            ]
        )

        bf_weights = Beamlet._calculate_bf_weights(delays, beamlet_frequencies)

        self.assertEqual(delays.shape, bf_weights.shape)

        self.assertEqual(
            weight_to_complex(bf_weights[0][0]),
            1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[0][1]),
            1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[0][2]),
            1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[1][0]),
            -1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[1][1]),
            1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[1][2]),
            -1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )

    def test_calculate_bf_weights_actual_numbers(self):
        # we test phase offsets with 90 degree increments to weed out:
        #  - sign errors (-1-2j instead of 1+2j == 180 degree offset)
        #  - complex conjugation errors (1-2j instead of 1+2j == 180 degree offset)
        #  - real/imaginary swap errors (2+1j instead of 1+2j == 90 degree offset)
        #
        # NB: -180 degree rotation == +180 degree rotation, so we need 90 degree
        #     offsets to detect 180 degree errors as well.

        # 2 beamlets, 3 antennas. The antennas are 1 second apart.
        delays = numpy.array([[0.0, 1.25e-9, 2.5e-9, 3.75e-9, 5.0e-9]])

        beamlet_frequencies = numpy.array(
            [[CLK_200_MHZ, CLK_200_MHZ, CLK_200_MHZ, CLK_200_MHZ, CLK_200_MHZ]]
        )

        bf_weights = Beamlet._calculate_bf_weights(delays, beamlet_frequencies)

        self.assertEqual(delays.shape, bf_weights.shape)

        self.assertEqual(
            weight_to_complex(bf_weights[0][0]),
            1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[0][1]),
            0 - 1j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[0][2]),
            -1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[0][3]),
            0 + 1j,
            msg=f"bf_weights = {bf_weights}",
        )
        self.assertEqual(
            weight_to_complex(bf_weights[0][4]),
            1 + 0j,
            msg=f"bf_weights = {bf_weights}",
        )

    def test_beamlet_output_defaults_4_streams(self):
        MACs = [
            "00:00:00:00:00:01",
            "00:00:00:00:00:02",
            "00:00:00:00:00:03",
            "00:00:00:00:00:04",
        ]
        IPs = ["1.1.1.1", "1.1.1.2", "1.1.1.3", "1.1.1.4"]
        PORTs = [1001, 1002, 1003, 1004]
        properties = {
            "FPGA_beamlet_output_nof_destinations_RW_default_shorthand": 4,
            "FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW_default_shorthand": MACs,
            "FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW_default_shorthand": IPs,
            "FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW_default_shorthand": PORTs,
        }

        defaults = dict(Beamlet._beamlet_output_defaults(properties))

        self.assertListEqual(
            defaults["FPGA_beamlet_output_multiple_hdr_eth_destination_mac_RW"],
            [MACs + ["00:00:00:00:00:00"] * (N_bdo_destinations_mm - 4)] * N_pn,
        )
        self.assertListEqual(
            defaults["FPGA_beamlet_output_multiple_hdr_ip_destination_address_RW"],
            [IPs + ["0.0.0.0"] * (N_bdo_destinations_mm - 4)] * N_pn,
        )
        self.assertListEqual(
            defaults["FPGA_beamlet_output_multiple_hdr_udp_destination_port_RW"],
            [PORTs + [0] * (N_bdo_destinations_mm - 4)] * N_pn,
        )

    def _verify_beamlet_frequencies(self, beamlet_subbands, subband_frequencies):
        beamlet_frequencies = Beamlet._beamlet_frequencies(
            beamlet_subbands, subband_frequencies
        )

        # explicitly construct expected output. We can count on the subband_frequency
        # to work as that is tested elsewhere.
        expected_frequencies = numpy.zeros(
            (N_pn, A_pn, N_beamlets_ctrl), dtype=numpy.float64
        )
        for beamlet_nr in range(N_beamlets_ctrl):
            for fpga_nr in range(N_pn):
                for antenna_nr in range(A_pn):
                    pol_nr = 0
                    subband_nr = beamlet_subbands[
                        fpga_nr, antenna_nr, pol_nr, beamlet_nr
                    ]
                    expected_frequencies[fpga_nr, antenna_nr, beamlet_nr] = (
                        subband_frequencies[fpga_nr, antenna_nr, pol_nr, subband_nr]
                    )

        numpy.testing.assert_array_almost_equal(
            beamlet_frequencies, expected_frequencies.reshape(beamlet_frequencies.shape)
        )

    def test_beamlet_frequencies_basic(self):
        beamlet_subbands = numpy.array(
            [
                [[[beamlet % N_subbands for beamlet in range(N_beamlets_ctrl)]] * N_pol]
                * A_pn
            ]
            * N_pn,
            dtype=numpy.uint32,
        )
        subband_indices = numpy.array(
            [[[list(range(N_subbands))] * N_pol] * A_pn] * N_pn, dtype=numpy.uint32
        )
        subband_frequencies = subband_indices * (CLK_200_MHZ / N_subbands)

        self._verify_beamlet_frequencies(beamlet_subbands, subband_frequencies)

    def test_beamlet_frequencies_mixed_fpga_settings(self):
        """Test different settings for the FPGAs to verify the ordering."""

        beamlet_subbands = numpy.array(
            [
                [[[beamlet % N_subbands for beamlet in range(N_beamlets_ctrl)]] * N_pol]
                * A_pn
            ]
            * N_pn,
            dtype=numpy.uint32,
        )
        subband_indices = numpy.array(
            [[[list(range(N_subbands))] * N_pol] * A_pn] * N_pn, dtype=numpy.uint32
        )
        subband_frequencies = subband_indices * (CLK_200_MHZ / N_subbands)

        # change settings for FPGAs beyond 4
        beamlet_subbands[4:, :, :, :] = 0

        self._verify_beamlet_frequencies(beamlet_subbands, subband_frequencies)

    def test_beamlet_frequencies_mixed_input_settings(self):
        """Test different settings for the inputs of the FPGAs to verify the ordering."""

        beamlet_subbands = numpy.array(
            [
                [[[beamlet % N_subbands for beamlet in range(N_beamlets_ctrl)]] * N_pol]
                * A_pn
            ]
            * N_pn,
            dtype=numpy.uint32,
        )
        subband_indices = numpy.array(
            [[[list(range(N_subbands))] * N_pol] * A_pn] * N_pn, dtype=numpy.uint32
        )
        subband_frequencies = subband_indices * (CLK_200_MHZ / N_subbands)

        # change settings for inputs beyond 4
        beamlet_subbands[:, 4:, :, :] = 0

        self._verify_beamlet_frequencies(beamlet_subbands, subband_frequencies)

    def test_beamlet_frequencies_unordered_subbands(self):
        """Test a shuffled order of subbands."""

        beamlet_subbands = numpy.array(
            [
                [[[beamlet % N_subbands for beamlet in range(N_beamlets_ctrl)]] * N_pol]
                * A_pn
            ]
            * N_pn,
            dtype=numpy.uint32,
        )
        subband_indices = numpy.array(
            [[[list(range(N_subbands))] * N_pol] * A_pn] * N_pn, dtype=numpy.uint32
        )

        # shuffle subbands (assuming 7 is coprime to N_subbands)
        subband_indices = (subband_indices * 7) % N_subbands
        subband_frequencies = subband_indices * (CLK_200_MHZ / N_subbands)

        self._verify_beamlet_frequencies(beamlet_subbands, subband_frequencies)
