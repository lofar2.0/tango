# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from numpy import testing

from tango import DevState
from tango.test_context import DeviceTestContext

from tangostationcontrol.devices import observation_control
from tangostationcontrol.devices.base_device_classes import lofar_device
from tangostationcontrol.observation import observation_controller

from test.devices import device_base


class TestObservationControlDevice(device_base.DeviceTestCase):
    """Unit test class for device ObservationControl"""

    def setUp(self):
        super(TestObservationControlDevice, self).setUp()

        proxy_patcher = self.proxy_patch(
            lofar_device, "ControlHierarchyDevice", autospec=True
        )
        self.m_control = proxy_patcher["mock"]

        self.m_db = self.database_proxy_patch(observation_controller)

    def test_init_on(self):
        """Test device initialization and defaults"""

        with DeviceTestContext(
            observation_control.ObservationControl,
            process=False,
            timeout=10,
        ) as proxy:
            proxy.boot()

            self.assertEqual(proxy.state(), DevState.ON)

            testing.assert_equal([], proxy.observations_R)
            testing.assert_equal([], proxy.running_observations_R)
            testing.assert_equal([], proxy.active_antenna_fields_R)

            self.assertFalse(proxy.is_any_observation_running())
            self.assertFalse(proxy.is_antenna_field_active("HBA"))
            self.assertFalse(proxy.is_antenna_field_active("HBA0"))
            self.assertFalse(proxy.is_antenna_field_active("HBA1"))
            self.assertFalse(proxy.is_antenna_field_active("LBA"))
