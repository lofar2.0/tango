# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.proxies import proxy
from tangostationcontrol.devices.base_device_classes import hierarchy_device

from test import base


class DeviceTestCase(base.TestCase):
    """BaseClass for device test cases to perform common DeviceProxy patching

    Only to be used on devices inheriting lofar_device, use device_proxy_patch
    to patch additional devices their proxy.
    """

    def setUp(self):
        super(DeviceTestCase, self).setUp()

        # Patch DeviceProxy to allow making the proxies during initialisation
        # that we otherwise avoid using
        self.device_proxy_mock = self.device_proxy_patch(proxy)

        # Patch Database to allow calling database during initialisation
        for database in [hierarchy_device]:
            self.database_proxy_patch(database)

    def database_proxy_patch(self, device_module):
        """Patch a Python module using DeviceProxy to mock its behavior"""

        return self.proxy_patch(device_module, "Database")

    def device_proxy_patch(self, device_module):
        """Patch a Python module using DeviceProxy to mock its behavior"""

        return self.proxy_patch(device_module, "DeviceProxy")
