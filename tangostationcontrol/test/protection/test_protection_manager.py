# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from unittest import mock
from unittest.mock import PropertyMock

import tango
from lofar_station_client.common import CaseInsensitiveDict
from tango import DevFailed
from timeout_decorator import timeout_decorator

from tangostationcontrol.common.constants import N_pn
from tangostationcontrol.protection.threshold import NumberProtectionThreshold
from tangostationcontrol.protection.config_types import protection_config_type
from tangostationcontrol.states.station_state_enum import StationStateEnum
from tangostationcontrol.protection import protection_manager
from tangostationcontrol.protection.protection_manager import ProtectionManager
from tangostationcontrol.protection.state import ProtectionStateEnum

from test import base

logger = logging.getLogger()


class TestProtectionManager(base.TestCase):

    def setUp(self):
        super(TestProtectionManager, self).setUp()

        self.m_create_proxy = self.proxy_patch(
            protection_manager, "create_device_proxy"
        )

    def test_transition_minus_one(self):
        """Test evaluating transition goals"""

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        t_manager._transition_minus_one(StationStateEnum.ON)

        self.m_create_proxy["mock"].return_value.station_standby.assert_called_once()

        t_manager._transition_minus_one(StationStateEnum.STANDBY)

        self.m_create_proxy["mock"].return_value.station_hibernate.assert_called_once()

        t_manager._transition_minus_one(StationStateEnum.HIBERNATE)
        self.m_create_proxy["mock"].return_value.station_standby.assert_called_once()
        self.m_create_proxy["mock"].return_value.station_hibernate.assert_called_once()

    def test_mutex_prevent_multiple_shutdown(self):
        """Test mutex preventing multiple shutdowns"""

        m_thread = self.proxy_patch(
            protection_manager, "ThreadPoolExecutor", autospec=True
        )

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        t_manager.protective_shutdown()

        self.assertEqual(t_manager._state, ProtectionStateEnum.ARMED)
        m_thread["mock"].return_value.submit.assert_called_once()

        t_manager.protective_shutdown()
        m_thread["mock"].return_value.submit.assert_called_once()

        t_manager._state = ProtectionStateEnum.OFF

        t_manager.protective_shutdown()

        self.assertEqual(2, m_thread["mock"].return_value.submit.call_count)

    def test_evaluate(self):
        """"""
        t_config: protection_config_type = CaseInsensitiveDict(
            {
                "SDPFirmware": CaseInsensitiveDict(
                    {
                        "FPGA_temp_R": NumberProtectionThreshold(
                            minimal=0.0, maximum=90.0
                        )
                    }
                ),
                "UNB2": CaseInsensitiveDict(
                    {
                        "UNB2_FPGA_POL_CORE_TEMP_R": NumberProtectionThreshold(
                            minimal=0.0, maximum=90.0
                        )
                    }
                ),
            }
        )

        m_shutdown = self.proxy_patch(
            protection_manager.ProtectionManager, "_protective_shutdown"
        )

        t_manager = ProtectionManager(t_config)

        t_manager.evaluate("sdpfirmware", "fpga_temp_r", [80.4] * N_pn)
        m_shutdown["mock"].assert_not_called()

        t_manager.evaluate("sdpfirmware", "fpga_temp_r", [91.4] * N_pn)
        m_shutdown["mock"].assert_called_once()

    @timeout_decorator.timeout(10)
    def test_protective_shutdown_already_protected(self):
        """Test the iterative shutdown sequence"""

        self.m_create_proxy["mock"].return_value = mock.Mock(
            protection_lock_RW=False,
            station_state_R=StationStateEnum.HIBERNATE,
            requested_station_state_R=StationStateEnum.HIBERNATE,
            spec=tango.DeviceProxy,
        )

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        t_manager._state = ProtectionStateEnum.ARMED
        t_manager._protective_shutdown(t_manager._cancelled)

        self.assertTrue(self.m_create_proxy["mock"].return_value.protection_lock_RW)

    @timeout_decorator.timeout(10)
    def test_protective_shutdown_transition_once(self):
        """Test having to transition once for protective shutdown"""
        m_transition_hibernate = mock.Mock()
        m_proxy_mock = mock.Mock(
            protection_lock_RW=False,
            station_hibernate=m_transition_hibernate,
            spec=tango.DeviceProxy,
        )
        type(m_proxy_mock).station_state_R = PropertyMock(
            side_effect=[
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.HIBERNATE,
                StationStateEnum.HIBERNATE,
            ]
        )
        type(m_proxy_mock).requested_station_state_R = PropertyMock(
            side_effect=[StationStateEnum.STANDBY]
        )
        type(m_proxy_mock).station_state_transitioning_R = PropertyMock(
            side_effect=[False, False]
        )
        self.m_create_proxy["mock"].return_value = m_proxy_mock

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        t_manager._state = ProtectionStateEnum.ARMED
        t_manager._protective_shutdown(t_manager._cancelled)

        self.assertTrue(self.m_create_proxy["mock"].return_value.protection_lock_RW)
        m_transition_hibernate.assert_called_once()
        self.assertEqual(t_manager._state, ProtectionStateEnum.OFF)

    @timeout_decorator.timeout(10)
    def test_protective_shutdown_transition_await(self):
        """Test awaiting state transition completion"""
        m_transition_hibernate = mock.Mock()
        m_proxy_mock = mock.Mock(
            protection_lock_RW=False,
            station_hibernate=m_transition_hibernate,
            spec=tango.DeviceProxy,
        )
        type(m_proxy_mock).station_state_R = PropertyMock(
            side_effect=[
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.HIBERNATE,
                StationStateEnum.HIBERNATE,
            ]
        )
        type(m_proxy_mock).requested_station_state_R = PropertyMock(
            side_effect=[StationStateEnum.STANDBY]
        )
        type(m_proxy_mock).station_state_transitioning_R = PropertyMock(
            side_effect=[False, True, False]
        )
        self.m_create_proxy["mock"].return_value = m_proxy_mock

        m_time = self.proxy_patch(protection_manager.time, "sleep", autospec=True)

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        t_manager._state = ProtectionStateEnum.ARMED
        t_manager._protective_shutdown(t_manager._cancelled)

        self.assertTrue(self.m_create_proxy["mock"].return_value.protection_lock_RW)
        m_transition_hibernate.assert_called_once()
        m_time["mock"].assert_called_once()
        self.assertEqual(t_manager._state, ProtectionStateEnum.OFF)

    @timeout_decorator.timeout(10)
    def test_protective_shutdown_anticipate_transition(self):
        """Test protective shutdown anticipate ongoing transition"""

        m_transition_standby = mock.Mock()
        m_transition_hibernate = mock.Mock()
        m_proxy_mock = mock.Mock(
            protection_lock_RW=False,
            station_standby=m_transition_standby,
            station_hibernate=m_transition_hibernate,
            spec=tango.DeviceProxy,
        )
        type(m_proxy_mock).station_state_R = PropertyMock(
            side_effect=[
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.STANDBY,
                StationStateEnum.HIBERNATE,
                StationStateEnum.HIBERNATE,
            ]
        )
        type(m_proxy_mock).requested_station_state_R = PropertyMock(
            side_effect=[
                StationStateEnum.ON,
                StationStateEnum.ON,
                StationStateEnum.ON,
            ]
        )
        type(m_proxy_mock).station_state_transitioning_R = PropertyMock(
            side_effect=[True, False, False]
        )
        self.m_create_proxy["mock"].return_value = m_proxy_mock

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        t_manager._state = ProtectionStateEnum.ARMED
        t_manager._protective_shutdown(t_manager._cancelled)

        m_transition_standby.assert_called_once()
        m_transition_hibernate.assert_called_once()
        self.assertEqual(t_manager._state, ProtectionStateEnum.OFF)

    def test_protective_shutdown_exception_state(self):
        """Test that exceptions in the shutdown sequence results in fault state"""

        m_exception = PropertyMock(side_effect=DevFailed)
        m_proxy_mock = mock.Mock(spec=tango.DeviceProxy)
        type(m_proxy_mock).protection_lock_RW = m_exception
        self.m_create_proxy["mock"].return_value = m_proxy_mock

        t_manager = ProtectionManager(CaseInsensitiveDict({}))
        t_manager._state = ProtectionStateEnum.ARMED
        self.assertRaises(
            DevFailed, t_manager._protective_shutdown, t_manager._cancelled
        )
        self.assertEqual(t_manager._state, ProtectionStateEnum.FAULT)

    def test_shutdown_threading(self):
        m_transition_standby = mock.Mock()
        m_transition_hibernate = mock.Mock()
        m_proxy_mock = mock.Mock(
            protection_lock_RW=False,
            station_standby=m_transition_standby,
            station_hibernate=m_transition_hibernate,
            spec=tango.DeviceProxy,
        )
        type(m_proxy_mock).station_state_R = PropertyMock(
            return_value=StationStateEnum.STANDBY
        )
        type(m_proxy_mock).requested_station_state_R = PropertyMock(
            return_value=StationStateEnum.ON,
        )

        # Gives 4x2 seconds of sleep should be plenty
        type(m_proxy_mock).station_state_transitioning_R = PropertyMock(
            side_effect=[True, True, True, True]
        )
        self.m_create_proxy["mock"].return_value = m_proxy_mock

        t_manager = ProtectionManager(CaseInsensitiveDict({}))

        self.assertEqual(ProtectionStateEnum.OFF, t_manager.state)

        try:
            t_manager.protective_shutdown()

            self.assertTrue(
                t_manager.state == ProtectionStateEnum.ARMED
                or t_manager.state == ProtectionStateEnum.ACTIVE
            )
        finally:
            t_manager.abort()
            self.assertEqual(t_manager._state, ProtectionStateEnum.ABORTED)
