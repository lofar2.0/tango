# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from unittest import mock

from lofar_station_client.common import CaseInsensitiveDict

from tangostationcontrol.devices import ProtectionControl
from tangostationcontrol.protection import config_types
from tangostationcontrol.protection.config_types import (
    convert_protection_to_device_config,
    generate_device_attribute_mapping,
    get_number_of_attributes,
    get_number_of_device_attribute_pairs,
    protection_config_type,
)
from tangostationcontrol.protection.threshold import NumberProtectionThreshold

from test import base

logger = logging.getLogger()


class TestConfigTypes(base.TestCase):

    TEST_CONFIG: protection_config_type = CaseInsensitiveDict(
        {
            "DEVICE_1": CaseInsensitiveDict(
                {
                    "attribute_1": NumberProtectionThreshold(minimal=0.0, maximum=90.0),
                    "attribute_2": NumberProtectionThreshold(minimal=0.0, maximum=90.0),
                }
            ),
            "device_2": CaseInsensitiveDict(
                {"attribute_3": NumberProtectionThreshold(minimal=0.0, maximum=90.0)}
            ),
        }
    )

    def setUp(self):
        super(TestConfigTypes, self).setUp()

        patch = self.proxy_patch(config_types, "tango", autospec=True)
        self.m_tango = patch["mock"]

    def test_convert_protection_to_device_config(self):
        """Test converting data"""

        result = convert_protection_to_device_config(
            ProtectionControl.PROTECTION_CONFIG
        )

        for device, attributes in ProtectionControl.PROTECTION_CONFIG.items():
            self.assertEqual(len(attributes), len(list(result.values())[0]))
            for y, attribute in enumerate(attributes.keys()):
                self.assertEqual(attribute, result[device][y])

    def test_get_number_of_attributes(self):
        """Test determining number of attributes from protection_config_type object"""

        self.assertEqual(0, get_number_of_attributes(CaseInsensitiveDict({})))
        self.assertEqual(3, get_number_of_attributes(self.TEST_CONFIG))

    def test_get_number_of_device_attribute_pairs(self):
        """Test dynamically determining number of device attribute pairs from tangodb"""

        m_database = mock.Mock()
        m_database.get_device_exported_for_class.side_effect = [
            [],  # first call, device 1
            [],  # first call, device 2
            ["stat/device_1/1", "stat/device_1/2"],  # second call, device 1
            [],  # second call, device 2
            [],  # third call, device 1
            ["stat/device_1/1"],  # third call, device 2
        ]

        self.m_tango.Database.return_value = m_database

        self.assertEqual(0, get_number_of_device_attribute_pairs(self.TEST_CONFIG))

        # 2 devices * 2 attributes = 4
        self.assertEqual(4, get_number_of_device_attribute_pairs(self.TEST_CONFIG))

        # 1 devices * 1 attribute = 1
        self.assertEqual(1, get_number_of_device_attribute_pairs(self.TEST_CONFIG))

    def test_generate_device_attribute_mapping(self):
        """Test creating a per device attribute pair mapping"""

        m_database = mock.Mock()
        m_database.get_device_exported_for_class.side_effect = [
            ["stat/device_1/1", "stat/device_1/2"],  # first call, device 1
            ["stat/device_2/1"],  # first call, device 2
        ]
        self.m_tango.Database.return_value = m_database

        result = generate_device_attribute_mapping(self.TEST_CONFIG)

        for device, attributes in self.TEST_CONFIG.items():
            for attribute in attributes:
                if device == "DEVICE_1":
                    self.assertTrue(attribute in result["stat/device_1/1"])
                    self.assertTrue(attribute in result["stat/device_1/2"])
                elif device == "device_2":
                    self.assertTrue(attribute in result["stat/device_2/1"])

        self.assertEqual(0, result["stat/device_1/1"]["attribute_1"])
        self.assertEqual(1, result["stat/device_1/1"]["attribute_2"])
        self.assertEqual(2, result["stat/device_1/2"]["attribute_1"])
        self.assertEqual(3, result["stat/device_1/2"]["attribute_2"])
        self.assertEqual(4, result["stat/device_2/1"]["attribute_3"])
