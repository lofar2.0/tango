# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from unittest import mock

from lofar_station_client.common import CaseInsensitiveDict
from tango import DeviceProxy, DevFailed

from tangostationcontrol.protection.config_types import (
    protection_metric_mapping_type,
)
from tangostationcontrol.protection.metrics import ProtectionMetrics

from test import base

logger = logging.getLogger()


class TestProtectionMetrics(base.TestCase):

    TEST_MAPPING: protection_metric_mapping_type = CaseInsensitiveDict(
        {
            "stat/device_1/1": CaseInsensitiveDict(
                {"attribute_1": 0, "attribute_2": 1}
            ),
            "stat/device_1/2": CaseInsensitiveDict(
                {"attribute_1": 2, "attribute_2": 3}
            ),
            "stat/device_2/1": CaseInsensitiveDict(
                {
                    "attribute_3": 4,
                }
            ),
        }
    )

    def setUp(self):
        super(TestProtectionMetrics, self).setUp()

    def test_update_device_metrics(self):
        """Test updating metrics and reported changes"""

        m_proxy = mock.Mock(spec=DeviceProxy)
        m_devfailed = mock.Mock(spec=DevFailed)

        t_device_proxies = CaseInsensitiveDict(
            {"stat/device_1/1": m_proxy, "stat/device_1/2": m_devfailed}
        )

        t_metrics = ProtectionMetrics(self.TEST_MAPPING)

        self.assertEqual(0, t_metrics.number_of_discovered_devices)
        self.assertEqual(0, t_metrics.number_of_connected_devices)

        t_metrics.update_device_metrics(t_device_proxies)
        self.assertEqual(2, t_metrics.number_of_discovered_devices)
        self.assertEqual(1, t_metrics.number_of_connected_devices)

        t_device_proxies = CaseInsensitiveDict({"stat/device_1/2": m_devfailed})

        t_metrics.update_device_metrics(t_device_proxies)
        self.assertEqual(2, t_metrics.number_of_discovered_devices)
        self.assertEqual(1, t_metrics.number_of_connected_devices)

        t_device_proxies = CaseInsensitiveDict({"stat/device_1/2": m_proxy})

        t_metrics.update_device_metrics(t_device_proxies)
        self.assertEqual(2, t_metrics.number_of_discovered_devices)
        self.assertEqual(2, t_metrics.number_of_connected_devices)

    def test_update_change_event(self):

        t_metrics = ProtectionMetrics(self.TEST_MAPPING)

        t_first_device = list(self.TEST_MAPPING.keys())[0]
        t_first_attribute = list(list(self.TEST_MAPPING.values())[0].keys())[0]

        self.assertEqual(0, t_metrics._time_last_change_events[0])
        t_metrics.update_change_event(t_first_device, t_first_attribute)
        self.assertTrue(t_metrics._time_last_change_events[0] > 0)
