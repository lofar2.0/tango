# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

import numpy

from tangostationcontrol.states.station_state_enum import StationStateEnum
from tangostationcontrol.protection.threshold import NumberProtectionThreshold

from test import base

logger = logging.getLogger()


class TestProtectionThreshold(base.TestCase):

    def setUp(self):
        super(TestProtectionThreshold, self).setUp()

    def test_number_threshold_scalar(self):
        """Test evaluating a NumberProtectionThreshold for simple scalars"""

        test = NumberProtectionThreshold(minimal=0, maximum=90.0)

        # self.assertTrue(test.evaluate(-1))
        # self.assertTrue(test.evaluate(-0.1))
        self.assertTrue(test.evaluate(91))
        self.assertTrue(test.evaluate(90.1))

        self.assertFalse(test.evaluate(0))
        self.assertFalse(test.evaluate(0.0))
        self.assertFalse(test.evaluate(89.9))

    def test_number_threshold_list(self):
        test = NumberProtectionThreshold(minimal=0, maximum=90.0)

        # self.assertTrue(
        #     test.evaluate([0, 0, 0, 0, -1]),
        #     "Single dimensionsal list below threshold failed",
        # )
        self.assertTrue(
            test.evaluate(
                [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 91, 0, 0], [0, 0, 0, 0, 0]]
            ),
            "Multidimensional list above threshold failed",
        )

        self.assertFalse(
            test.evaluate([0, 0, 0, 0, 0]),
            "Single dimensional list within threshold failed",
        )
        self.assertFalse(
            test.evaluate(
                [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
            ),
            "Multidimensional list within threshold failed",
        )

    def test_number_threshold_tuple(self):
        test = NumberProtectionThreshold(minimal=0, maximum=90.0)

        # self.assertTrue(
        #     test.evaluate(
        #         (
        #             0,
        #             0,
        #             0,
        #             0,
        #             -1,
        #         )
        #     ),
        #     "Single dimensionsal tuple below threshold failed",
        # )
        self.assertTrue(
            test.evaluate(
                (
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                    (
                        0,
                        0,
                        91,
                        0,
                        0,
                    ),
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                )
            ),
            "Multidimensional tuple above threshold failed",
        )

        self.assertFalse(
            test.evaluate(
                (
                    0,
                    0,
                    0,
                    0,
                    0,
                )
            ),
            "Single dimensional tuple within threshold failed",
        )
        self.assertFalse(
            test.evaluate(
                (
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                    (
                        0,
                        0,
                        0,
                        0,
                        0,
                    ),
                )
            ),
            "Multidimensional tuple within threshold failed",
        )

    def test_number_threshold_numpy(self):
        test = NumberProtectionThreshold(minimal=0, maximum=90.0)

        self.assertTrue(
            test.evaluate(numpy.array([0] * 3 + [91], dtype=numpy.int8)),
            "Single dimensional numpy array int8 above threshold failed",
        )

        self.assertTrue(
            test.evaluate(numpy.array([[0] * 3 + [91]] + [[0] * 4], dtype=numpy.int8)),
            "Multi dimensional numpy array int8 above threshold failed",
        )

        self.assertTrue(
            test.evaluate(numpy.array([0] * 3 + [91], dtype=numpy.double)),
            "Single dimensional numpy array double above threshold failed",
        )

        self.assertTrue(
            test.evaluate(
                numpy.array([[0] * 3 + [91]] + [[0] * 4], dtype=numpy.double)
            ),
            "Multi dimensional numpy array double above threshold failed",
        )

        self.assertFalse(
            test.evaluate(numpy.array([[0] * 3 + [2]] + [[0] * 4], dtype=numpy.double)),
            "Multi dimensional numpy array double within threshold failed",
        )

    def test_number_threshold_enum(self):
        test = NumberProtectionThreshold(
            minimal=StationStateEnum.HIBERNATE, maximum=StationStateEnum.STANDBY
        )

        # self.assertTrue(
        #     test.evaluate(StationStateEnum.OFF), "Enum scalar below threshold failed"
        # )

        self.assertTrue(
            test.evaluate(StationStateEnum.ON), "Enum scalar above threshold failed"
        )

        self.assertFalse(
            test.evaluate(StationStateEnum.HIBERNATE),
            "Enum scalar within threshold failed",
        )

        # self.assertTrue(
        #     test.evaluate([StationStateEnum.STANDBY] * 3 + [StationStateEnum.OFF]),
        #     "Single dimensionsal list below threshold failed",
        # )

        # self.assertTrue(
        #     test.evaluate(
        #         [[StationStateEnum.STANDBY] * 3 + [StationStateEnum.OFF]] * 4
        #     ),
        #     "Multidimension dimensionsal list below threshold failed",
        # )
