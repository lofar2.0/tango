# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import unittest
from unittest import mock

from typing import Dict
from typing import Union

import testscenarios
from tangostationcontrol.common.lofar_logging import configure_logger

"""Setup logging for unit tests"""
configure_logger(debug=True)


class BaseTestCase(testscenarios.WithScenarios, unittest.TestCase):
    """Test base class."""

    def setUp(self):
        super().setUp()

    def proxy_patch(
        self, module, name, autospec=False, spec=None
    ) -> Dict[str, Union[mock.Mock, object]]:
        """Patch the name within module and return both the mock and original object"""

        proxy_patcher = mock.patch.object(module, name, autospec=autospec, spec=spec)
        patch_mock = proxy_patcher.start()
        self.addCleanup(proxy_patcher.stop)

        return {
            "patch": proxy_patcher,
            "mock": patch_mock,
            "object": getattr(proxy_patcher.target, proxy_patcher.attribute),
        }


class TestCase(BaseTestCase):
    """Test case base class for all unit tests."""

    def setUp(self):
        super().setUp()


class AsyncTestCase(testscenarios.WithScenarios, unittest.IsolatedAsyncioTestCase):
    """Test case base class for all asyncio unit tests."""

    def setUp(self):
        super().setUp()
