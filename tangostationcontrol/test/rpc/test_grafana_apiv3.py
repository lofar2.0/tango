# Copyright (C) 2025 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime, timezone, timedelta
import json
from os import path

from tangostationcontrol.common.constants import (
    N_subbands,
    N_pol,
    N_beamlets_ctrl,
    N_pn,
    S_pn,
    A_pn,
)
from tangostationcontrol.rpc.grafana_api import (
    GrafanaAPIV3,
    StatisticsToGrafana,
    BstToGrafana,
    SstToGrafana,
    XstToGrafana,
)
from tangostationcontrol.rpc._proto import grafana_apiv3_pb2
from tangostationcontrol.rpc.statistics import (
    Statistics,
    dB,
    NotAvailableError,
    TooOldError,
)
from tangostationcontrol.rpc._proto import statistics_pb2

from google.protobuf.timestamp_pb2 import Timestamp

from test import base


def field_labels(label_list: list[grafana_apiv3_pb2.Label]) -> dict[str, str]:
    """Turn a list of objects with key and value fields into a dict."""
    return {x.key: x.value for x in label_list}


class TestStatisticsToGrafana(base.TestCase):
    def test_verify_call_result(self):
        statistics = Statistics()
        sut = StatisticsToGrafana(statistics)

        now = datetime.now(tz=timezone.utc)

        reply = statistics_pb2.SstReply(result=statistics_pb2.SstResult(timestamp=now))

        # should not raise
        _ = sut._verify_call_result(
            reply, (now - timedelta(seconds=1), now + timedelta(seconds=1))
        )

        # too old raises
        with self.assertRaises(TooOldError):
            _ = sut._verify_call_result(
                reply, (now + timedelta(seconds=1), now + timedelta(seconds=2))
            )


class TestBstToGrafana(base.TestCase):
    def setUp(self):
        statistics = Statistics()
        self.sut = BstToGrafana(statistics)

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/bst-message.json") as f:
            self.bst_message = json.load(f)
            statistics.handle_statistics_message(
                "bst/lba/cs032", datetime.now(), self.bst_message
            )

        self.valid_time_window = (
            datetime.fromisoformat("2025-01-01T00:00+00:00"),
            datetime.fromisoformat("2026-01-01T00:00+00:00"),
        )

    def test_all_frames(self):
        frames = self.sut.all_frames(self.valid_time_window, "lba", None)

        self.assertEqual(1, len(frames))
        self.assertEqual("BST", frames[0].metric)
        self.assertEqual(3, len(frames[0].fields))
        self.assertEqual("beamlet", frames[0].fields[0].name)
        self.assertEqual("xx", frames[0].fields[1].name)
        self.assertEqual("yy", frames[0].fields[2].name)

        # all fields must be of the right size
        self.assertEqual(N_beamlets_ctrl, len(frames[0].timestamps))
        self.assertEqual(N_beamlets_ctrl, len(frames[0].fields[0].values))
        self.assertEqual(N_beamlets_ctrl, len(frames[0].fields[1].values))
        self.assertEqual(N_beamlets_ctrl, len(frames[0].fields[2].values))

        # all timestamps must be equal
        self.assertEqual(1, len(set([ts.ToDatetime() for ts in frames[0].timestamps])))

        # all beamlets must be there
        self.assertEqual(set(range(N_beamlets_ctrl)), set(frames[0].fields[0].values))

        # test values
        for beamlet_idx, value in enumerate(frames[0].fields[1].values):
            self.assertAlmostEqual(
                dB(self.bst_message["bst_data"][beamlet_idx][0]),
                value,
                5,
                msg=f"{beamlet_idx=}",
            )
        for beamlet_idx, value in enumerate(frames[0].fields[2].values):
            self.assertAlmostEqual(
                dB(self.bst_message["bst_data"][beamlet_idx][1]),
                value,
                5,
                msg=f"{beamlet_idx=}",
            )


class TestSstToGrafana(base.TestCase):
    def setUp(self):
        statistics = Statistics()
        self.sut = SstToGrafana(statistics)

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/sst-message.json") as f:
            self.sst_message = json.load(f)
            statistics.handle_statistics_message(
                "sst/lba/cs032", datetime.now(), self.sst_message
            )

        self.valid_time_window = (
            datetime.fromisoformat("2025-01-01T00:00+00:00"),
            datetime.fromisoformat("2026-01-01T00:00+00:00"),
        )

    def test_all_frames(self):
        frames = self.sut.all_frames(self.valid_time_window, "lba", None)

        self.assertEqual(1, len(frames))
        self.assertEqual("SST", frames[0].metric)
        self.assertEqual(1 + N_pn * S_pn, len(frames[0].fields))
        self.assertEqual("frequency", frames[0].fields[0].name)

        for idx, field in enumerate(frames[0].fields[1:], 1):
            self.assertEqual("power", field.name, msg=f"{idx=}")

        # all fields must be of the right size
        self.assertEqual(N_subbands - 1, len(frames[0].timestamps))
        self.assertEqual(N_subbands - 1, len(frames[0].fields[0].values))

        for idx, field in enumerate(frames[0].fields[1:], 1):
            self.assertEqual(N_subbands - 1, len(field.values), msg=f"{idx=}")

        # all timestamps must be equal
        self.assertEqual(1, len(set([ts.ToDatetime() for ts in frames[0].timestamps])))

        # all inputs must be there
        input_names = {
            field_labels(field.labels)["antenna"] for field in frames[0].fields[1:]
        }
        self.assertEqual(N_pn * A_pn, len(input_names), msg=f"{input_names=}")
        # both polarisarions must be there
        input_pols = [
            field_labels(field.labels)["pol"] for field in frames[0].fields[1:]
        ]
        self.assertEqual(N_pn * A_pn, input_pols.count("xx"), msg=f"{input_pols=}")
        self.assertEqual(N_pn * A_pn, input_pols.count("yy"), msg=f"{input_pols=}")

        # test values
        for field in frames[0].fields[1:]:
            labels = field_labels(field.labels)

            antenna_idx = int(labels["antenna"])
            pol_indices = {"xx": 0, "yy": 1}
            pol_idx = pol_indices[labels["pol"]]
            input_idx = antenna_idx * N_pol + pol_idx

            for idx, value in enumerate(field.values):
                self.assertAlmostEqual(
                    dB(self.sst_message["sst_data"][input_idx][idx + 1]),
                    value,
                    5,
                    msg=f"{labels=}, {idx=}",
                )


class TestXstToGrafana(base.TestCase):
    def setUp(self):
        statistics = Statistics()
        self.sut = XstToGrafana(statistics)

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/xst-message.json") as f:
            self.xst_message = json.load(f)
            statistics.handle_statistics_message(
                "xst/lba/cs032", datetime.now(), self.xst_message
            )

        self.valid_time_window = (
            datetime.fromisoformat("2025-01-01T00:00+00:00"),
            datetime.fromisoformat("2026-01-01T00:00+00:00"),
        )

    def test_all_frames(self):
        frames = self.sut.all_frames(self.valid_time_window, "lba", None)

        self.assertEqual(1, len(frames))
        self.assertEqual("XST", frames[0].metric)
        self.assertEqual(3 + 2 * N_pol**2, len(frames[0].fields))
        self.assertEqual("antenna1", frames[0].fields[0].name)
        self.assertEqual("antenna2", frames[0].fields[1].name)
        self.assertEqual("frequency", frames[0].fields[2].name)
        self.assertEqual(
            {"power", "phase"}, {field.name for field in frames[0].fields[3:]}
        )

        N_antennas = N_pn * A_pn
        N_baselines = N_antennas * (N_antennas + 1) // 2

        # all fields must be of the right size
        self.assertEqual(N_baselines, len(frames[0].timestamps))

        for idx, field in enumerate(frames[0].fields):
            self.assertEqual(N_baselines, len(field.values), msg=f"{idx=}")

        # all timestamps must be equal
        self.assertEqual(1, len(set([ts.ToDatetime() for ts in frames[0].timestamps])))

        # all polarisarions must be there (for power and phase)
        input_pols = [
            field_labels(field.labels)["pol"] for field in frames[0].fields[3:]
        ]
        self.assertEqual(2, input_pols.count("xx"))
        self.assertEqual(2, input_pols.count("xy"))
        self.assertEqual(2, input_pols.count("yx"))
        self.assertEqual(2, input_pols.count("yy"))

        # all antennas must be there
        self.assertEqual(set(range(N_antennas)), set(frames[0].fields[0].values))
        self.assertEqual(set(range(N_antennas)), set(frames[0].fields[1].values))

        # test specific value for regression
        self.assertAlmostEqual(30.7213135, frames[0].fields[3].values[99], 6)
        self.assertAlmostEqual(3.88297279, frames[0].fields[4].values[99], 6)
        self.assertAlmostEqual(27.2744141, frames[0].fields[5].values[99], 6)
        self.assertAlmostEqual(297.7411965, frames[0].fields[6].values[99], 6)


class TestGrafanaAPIV3(base.TestCase):
    def test_getqueryoptions(self):
        """Test GetQueryOptions."""

        statistics = Statistics()
        sut = GrafanaAPIV3(statistics, [])

        request = grafana_apiv3_pb2.GetOptionsRequest()
        reply = sut.GetQueryOptions(request, None)

        self.assertEqual(0, len(reply.options))

    def test_listdimensionkeys(self):
        """Test ListDimensionKeys."""

        statistics = Statistics()
        sut = GrafanaAPIV3(statistics, [])

        request = grafana_apiv3_pb2.ListDimensionKeysRequest()
        reply = sut.ListDimensionKeys(request, None)

        dimensions = {d.key for d in reply.results}

        self.assertIn("antenna_field", dimensions)
        self.assertIn("pol", dimensions)

    def test_listdimensionvalues(self):
        """Test ListDimensionValues."""

        statistics = Statistics()
        sut = GrafanaAPIV3(statistics, ["LBA", "HBA"])

        expected_values = {
            "antenna_field": {"LBA", "HBA"},
            "pol": {"xx", "xy", "yx", "yy"},
        }

        for dim, expected_value in expected_values.items():
            request = grafana_apiv3_pb2.ListDimensionValuesRequest(dimension_key=dim)
            reply = sut.ListDimensionValues(request, None)

            value = set([r.value for r in reply.results])
            self.assertEqual(expected_value, value, msg=f"{dim=}")

    def test_listmetrics(self):
        """Test ListMetrics."""

        statistics = Statistics()
        sut = GrafanaAPIV3(statistics, [])

        request = grafana_apiv3_pb2.ListMetricsRequest()
        reply = sut.ListMetrics(request, None)

        metrics = [m.name for m in reply.Metrics]

        self.assertIn("BST", metrics)
        self.assertIn("SST", metrics)
        self.assertIn("XST", metrics)


class TestGetMetricAggregate(base.TestCase):
    def setUp(self):
        statistics = Statistics()
        self.sut = GrafanaAPIV3(statistics, ["LBA"])

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/sst-message.json") as f:
            sst_message = json.load(f)
            statistics.handle_statistics_message(
                "sst/lba/cs032", datetime.now(), sst_message
            )

    def test_nometrics(self):
        """Test if there are no metrics available."""

        statistics = Statistics()
        sut = GrafanaAPIV3(statistics, ["LBA"])

        # request/response
        request = grafana_apiv3_pb2.GetMetricAggregateRequest(
            metrics=["SST"],
            dimensions=[grafana_apiv3_pb2.Dimension(key="antenna_field", value="LBA")],
            startDate=Timestamp(seconds=0),
            endDate=Timestamp(seconds=int(datetime.now().timestamp())),
        )

        with self.assertRaises(NotAvailableError):
            _ = sut.GetMetricAggregate(request, None)

    def test_validdaterange(self):
        """Test obtaining SSTs using a date range for which they are available."""

        # request/response
        request = grafana_apiv3_pb2.GetMetricAggregateRequest(
            metrics=["SST"],
            dimensions=[grafana_apiv3_pb2.Dimension(key="antenna_field", value="LBA")],
            startDate=Timestamp(seconds=0),
            endDate=Timestamp(seconds=int(datetime.now().timestamp())),
        )

        reply = self.sut.GetMetricAggregate(request, None)

        # validate output
        self.assertEqual(1, len(reply.frames))
        self.assertEqual("SST", reply.frames[0].metric)

    def test_tooold(self):
        """Test obtaining SSTs using a date range for which they are too old."""

        # request/response
        request = grafana_apiv3_pb2.GetMetricAggregateRequest(
            metrics=["SST"],
            dimensions=[grafana_apiv3_pb2.Dimension(key="antenna_field", value="LBA")],
            startDate=Timestamp(seconds=int(datetime.now().timestamp()) - 1),
            endDate=Timestamp(seconds=int(datetime.now().timestamp())),
        )

        reply = self.sut.GetMetricAggregate(request, None)

        # validate output
        self.assertEqual(0, len(reply.frames))
