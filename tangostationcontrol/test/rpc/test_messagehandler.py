# Copyright (C) 2025 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import time

from lofar_station_client.zeromq.publisher import ZeroMQPublisher
from timeout_decorator import timeout_decorator

from tangostationcontrol.rpc.messagehandler import MultiEndpointZMQMessageHandler

from test import base


class TestMultiEndpointZMQMessageHandler(base.TestCase):
    DEFAULT_PUBLISH_ADDRESS = "tcp://*:6001"
    DEFAULT_SUBSCRIBE_ADDRESS = "tcp://127.0.0.1:6001"

    @timeout_decorator.timeout(2)
    def test(self):
        t_msg = '{"foo": "bar"}'
        t_topic = "topic"

        received_msgs = []

        def handle_message(topic, timestamp, msg):
            received_msgs.append(msg)

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as publisher:
            with MultiEndpointZMQMessageHandler(handle_message) as sut:
                sut.add_receiver(self.DEFAULT_SUBSCRIBE_ADDRESS, [t_topic])

                # wait fo Publisher to spin up and Subscriber to connect
                while not sut._subscribers[self.DEFAULT_PUBLISH_ADDRESS].is_connected:
                    time.sleep(0.1)

                # send message and wait for it to arrive
                publisher.send(t_msg)

                while not received_msgs:
                    time.sleep(0.1)

                # validate message content
                self.assertEqual(1, len(received_msgs))
                self.assertDictEqual({"foo": "bar"}, received_msgs[0])
