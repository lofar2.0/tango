# Copyright (C) 2025 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime, timezone
import json
from os import path

import numpy

from tangostationcontrol.common.constants import (
    N_subbands,
    N_pol,
    N_beamlets_ctrl,
    N_pn,
    A_pn,
)
from tangostationcontrol.rpc.statistics import (
    Statistics,
    dB,
    TooOldError,
    NotAvailableError,
)
from tangostationcontrol.rpc._proto import statistics_pb2

from test import base


class TestStatistics(base.TestCase):
    def test_notavailable(self):
        sut = Statistics()

        # request/response
        request = statistics_pb2.SstRequest(antenna_field="lba")

        with self.assertRaises(NotAvailableError):
            _ = sut.Sst(request, None)

    def test_tooold(self):
        sut = Statistics()

        # request/response
        request = statistics_pb2.SstRequest(antenna_field="lba")

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/sst-message.json") as f:
            sst_message = json.load(f)
            sut.handle_statistics_message("sst/lba/cs032", datetime.now(), sst_message)

        # request/response
        request = statistics_pb2.SstRequest(
            antenna_field="lba", maxage=1  # sample file surely expired by now
        )

        with self.assertRaises(TooOldError):
            _ = sut.Sst(request, None)

    def test_sst(self):
        sut = Statistics()

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/sst-message.json") as f:
            sst_message = json.load(f)
            sut.handle_statistics_message("sst/lba/cs032", datetime.now(), sst_message)

        # request/response
        request = statistics_pb2.SstRequest(antenna_field="lba")

        reply = sut.Sst(request, None)

        # validate output
        self.assertEqual(
            datetime.fromisoformat(sst_message["timestamp"]),
            reply.result.timestamp.ToDatetime(tzinfo=timezone.utc),
        )
        self.assertAlmostEqual(
            sst_message["integration_interval"], reply.result.integration_interval, 6
        )

        # verify all data is there
        self.assertEqual(
            set(range(1, N_subbands)), set([s.subband for s in reply.result.subbands])
        )

        for subband in reply.result.subbands:
            self.assertEqual(
                set(range(N_pn * A_pn)), set([a.antenna for a in subband.antennas])
            )

        # verify data matches input
        for subband in reply.result.subbands:
            for antenna in subband.antennas:
                self.assertAlmostEqual(
                    dB(
                        sst_message["sst_data"][antenna.antenna * N_pol + 0][
                            subband.subband
                        ]
                    ),
                    antenna.x_power_db,
                    5,
                    msg=f"Antenna {antenna.antenna} subband {subband.subband} X pol does not match",
                )
                self.assertAlmostEqual(
                    dB(
                        sst_message["sst_data"][antenna.antenna * N_pol + 1][
                            subband.subband
                        ]
                    ),
                    antenna.y_power_db,
                    5,
                    msg=f"Antenna {antenna.antenna} subband {subband.subband} Y pol does not match",
                )

    def test_bst(self):
        sut = Statistics()

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/bst-message.json") as f:
            bst_message = json.load(f)
            sut.handle_statistics_message("bst/lba/cs032", datetime.now(), bst_message)

        # request/response
        request = statistics_pb2.BstRequest(antenna_field="lba")

        reply = sut.Bst(request, None)

        # validate output
        self.assertEqual(
            datetime.fromisoformat(bst_message["timestamp"]),
            reply.result.timestamp.ToDatetime(tzinfo=timezone.utc),
        )
        self.assertAlmostEqual(
            bst_message["integration_interval"], reply.result.integration_interval, 6
        )

        # verify all data is there
        self.assertEqual(
            set(range(N_beamlets_ctrl)), set([b.beamlet for b in reply.result.beamlets])
        )

        # verify data matches input
        for beamlet in reply.result.beamlets:
            self.assertAlmostEqual(
                dB(bst_message["bst_data"][beamlet.beamlet][0]),
                beamlet.x_power_db,
                5,
                msg=f"Beamlet {beamlet.beamlet} X pol does not match",
            )
            self.assertAlmostEqual(
                dB(bst_message["bst_data"][beamlet.beamlet][1]),
                beamlet.y_power_db,
                5,
                msg=f"Beamlet {beamlet.beamlet} Y pol does not match",
            )

    def test_xst(self):
        sut = Statistics()

        # provide with sample statistic
        with open(f"{path.dirname(__file__)}/xst-message.json") as f:
            xst_message = json.load(f)
            sut.handle_statistics_message("xst/lba/cs032", datetime.now(), xst_message)

        # request/response
        request = statistics_pb2.XstRequest(antenna_field="lba")

        reply = sut.Xst(request, None)

        # validate output
        self.assertEqual(
            datetime.fromisoformat(xst_message["timestamp"]),
            reply.result.timestamp.ToDatetime(tzinfo=timezone.utc),
        )
        self.assertEqual(xst_message["subband"], reply.result.subband)
        self.assertAlmostEqual(
            xst_message["integration_interval"], reply.result.integration_interval, 6
        )

        # verify all data is there
        self.assertEqual(
            set(
                [
                    (a, b)
                    for a in range(N_pn * A_pn)
                    for b in range(N_pn * A_pn)
                    if a <= b
                ]
            ),
            set([(b.antenna1, b.antenna2) for b in reply.result.baselines]),
        )

        # verify data matches input
        xst_data = (
            numpy.array(xst_message["xst_data_real"])
            + numpy.array(xst_message["xst_data_imag"]) * 1j
        )
        for baseline in reply.result.baselines:
            xx = xst_data[baseline.antenna1 * N_pol + 0][baseline.antenna2 * N_pol + 0]
            xy = xst_data[baseline.antenna1 * N_pol + 0][baseline.antenna2 * N_pol + 1]
            yx = xst_data[baseline.antenna1 * N_pol + 1][baseline.antenna2 * N_pol + 0]
            yy = xst_data[baseline.antenna1 * N_pol + 1][baseline.antenna2 * N_pol + 1]

            for pol in ("xx", "xy", "yx", "yy"):
                expected = locals()[pol]
                actual = getattr(baseline, pol)

                self.assertAlmostEqual(
                    dB(numpy.abs(expected)),
                    actual.power_db,
                    5,
                    msg=f"Baseline ({baseline.antenna1}, {baseline.antenna2}) {pol.upper()} power does not match",
                )

                self.assertAlmostEqual(
                    numpy.angle(expected),
                    actual.phase,
                    6,
                    msg=f"Baseline ({baseline.antenna1}, {baseline.antenna2}) {pol.upper()} phase does not match",
                )
