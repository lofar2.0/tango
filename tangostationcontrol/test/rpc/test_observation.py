# Copyright (C) 2025 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from unittest import mock

from tangostationcontrol.rpc.observation import Observation
from tangostationcontrol.rpc._proto import observation_pb2

from test import base


class TestObservation(base.TestCase):
    @mock.patch("tangostationcontrol.rpc.observation.create_device_proxy")
    def test_startobservation(self, m_create_device_proxy):
        sut = Observation()

        # request/response
        request = observation_pb2.StartObservationRequest(configuration="{}")

        reply = sut.StartObservation(request, None)

        # validate output
        self.assertTrue(reply.success)
        m_create_device_proxy.assert_called_with(
            "STAT/ObservationControl/1", Observation.TIMEOUT
        )
        m_create_device_proxy.return_value.add_observation.assert_called_with("{}")

    @mock.patch("tangostationcontrol.rpc.observation.create_device_proxy")
    def test_stopobservation(self, m_create_device_proxy):
        sut = Observation()

        # request/response
        request = observation_pb2.StopObservationRequest(observation_id=12345)

        reply = sut.StopObservation(request, None)

        # validate output
        self.assertTrue(reply.success)
        m_create_device_proxy.assert_called_with(
            "STAT/ObservationControl/1", Observation.TIMEOUT
        )
        m_create_device_proxy.return_value.stop_observation_now.assert_called_with(
            12345
        )
