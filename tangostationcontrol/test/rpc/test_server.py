# Copyright (C) 2025 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from threading import Thread

import grpc
from grpc_reflection.v1alpha.proto_reflection_descriptor_database import (
    ProtoReflectionDescriptorDatabase,
)

from tangostationcontrol.rpc._proto import grafana_apiv3_pb2
from tangostationcontrol.rpc._proto import grafana_apiv3_pb2_grpc
from tangostationcontrol.rpc.server import Server

from test import base


class TestServer(base.TestCase):
    def setUp(self):
        # Start gRPC server in a separate thread
        self.server = Server(["LBA"], port=0)
        self.server_thread = Thread(target=self.server.run)
        self.server_thread.start()

        # Cleanup in the correct order (LIFO)
        self.addCleanup(self.server_thread.join)
        self.addCleanup(self.server.stop)

    def test_api(self):
        """Check whether we actually expose the expected API."""

        with grpc.insecure_channel(f"localhost:{self.server.port}") as channel:
            reflection_db = ProtoReflectionDescriptorDatabase(channel)
            services = reflection_db.get_services()

            self.assertIn("Observation", services)
            self.assertIn("Antennafield", services)
            self.assertIn("Statistics", services)
            self.assertIn("grafanav3.GrafanaQueryAPI", services)

    def test_call(self):
        """Test a basic gRPC call to the server."""

        with grpc.insecure_channel(f"localhost:{self.server.port}") as channel:
            stub = grafana_apiv3_pb2_grpc.GrafanaQueryAPIStub(channel)
            _ = stub.GetQueryOptions(grafana_apiv3_pb2.GetOptionsRequest())
