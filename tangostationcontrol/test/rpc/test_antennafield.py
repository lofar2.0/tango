# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from unittest.mock import MagicMock, patch
from tangostationcontrol.rpc._proto.antennafield_pb2 import (
    Identifier,
    SetAntennaStatusRequest,
    SetAntennaUseRequest,
)
from tangostationcontrol.rpc.antennafield import AntennaField, AntennaNotFoundException

from test import base


class TestAntennaField(base.TestCase):

    def mock_tango_db_response(self, mock_tango_database):
        """Helper function to mock Tango database response."""
        mock_db = MagicMock()
        mock_tango_database.return_value = mock_db
        mock_db.get_device_exported_for_class.return_value = [
            "STAT/AFL/Antenna1",
            "STAT/AFL/Antenna2",  # This should match the identifier
        ]
        return mock_db

    @patch("tangostationcontrol.rpc.antennafield.create_device_proxy")
    @patch("tangostationcontrol.rpc.antennafield.tango.Database")
    def test_create_antennafield_device_proxy_success(
        self, mock_tango_database, mock_create_device_proxy
    ):
        mock_device = MagicMock()
        mock_create_device_proxy.return_value = mock_device
        identifier = Identifier(antennafield_name="LBA", antenna_name="Antenna2")

        self.mock_tango_db_response(mock_tango_database)

        # Act
        antenna_field = AntennaField()
        result = antenna_field._create_antennafield_device_proxy(
            identifier, write_access=True
        )

        # Assert
        mock_create_device_proxy.assert_called_once_with(
            "STAT/AFL/Antenna2", write_access=True
        )
        self.assertEqual(result, mock_device)

    @patch(
        "tangostationcontrol.rpc.antennafield.create_device_proxy"
    )  # Mocking create_device_proxy
    @patch("tangostationcontrol.rpc.antennafield.tango.Database")  # Mock tango Database
    def test_create_antennafield_device_proxy_failure(
        self, mock_tango_database, mock_create_device_proxy
    ):
        # Arrange
        mock_create_device_proxy.side_effect = IOError("Failed to create device proxy")
        identifier = Identifier(antennafield_name="LBA", antenna_name="Antenna2")
        self.mock_tango_db_response(mock_tango_database)

        # Act & Assert
        antenna_field = AntennaField()
        with self.assertRaises(IOError):
            antenna_field._create_antennafield_device_proxy(identifier)

    def test_get_antenna_index_found(self):
        # Arrange
        mock_device = MagicMock()
        mock_device.Antenna_Names_R = ["Antenna1", "Antenna2"]
        identifier = Identifier(antennafield_name="LBA", antenna_name="Antenna2")

        # Act
        antenna_field = AntennaField()
        result = antenna_field._get_antenna_index(identifier, mock_device)

        # Assert
        self.assertEqual(result, 1)

    def test_get_antenna_index_not_found(self):
        # Arrange
        mock_device = MagicMock()
        mock_device.Antenna_Names_R = ["Antenna1"]
        identifier = Identifier(antennafield_name="LBA", antenna_name="Antenna2")

        # Act
        antenna_field = AntennaField()

        # Assert
        with self.assertRaises(AntennaNotFoundException):
            antenna_field._get_antenna_index(identifier, mock_device)

    def test_get_antenna_reply(self):
        # Arrange
        mock_device = MagicMock()
        mock_device.Antenna_Status_R = [True, False]
        mock_device.Antenna_Use_R = [1, 0]

        identifier = Identifier(antennafield_name="LBA", antenna_name="Antenna1")

        antenna_field = AntennaField()
        antenna_index = 0

        # Act
        reply = antenna_field._get_antenna_reply(identifier, antenna_index, mock_device)

        # Assert
        self.assertTrue(reply.success, msg=f"{reply.exception}")
        self.assertEqual(reply.result.antenna_status, True)
        self.assertEqual(reply.result.antenna_use, 1)

    @patch("tangostationcontrol.rpc.antennafield.create_device_proxy")
    @patch("tangostationcontrol.rpc.antennafield.tango.Database")
    def test_set_antenna_status(self, mock_tango_database, mock_create_device_proxy):

        # Arrange
        self.mock_tango_db_response(mock_tango_database)

        mock_create_device_proxy.return_value.read_attribute.return_value.value = [1, 0]
        mock_create_device_proxy.return_value.Antenna_Status_R = [1, 0]
        mock_create_device_proxy.return_value.Antenna_Use_R = [1, 0]
        mock_create_device_proxy.return_value.Antenna_Names_R = ["Antenna0", "Antenna1"]

        request = SetAntennaStatusRequest(
            antenna_status=True,
            identifier=Identifier(antennafield_name="LBA", antenna_name="Antenna1"),
        )

        # Act
        antenna_field = AntennaField()
        reply = antenna_field.SetAntennaStatus(request, None)

        # Assert
        self.assertTrue(reply.success, msg=f"{reply.exception}")
        mock_create_device_proxy.return_value.put_property.assert_called_once_with(
            {"Antenna_Status": [1, 1]}
        )

    @patch("tangostationcontrol.rpc.antennafield.create_device_proxy")
    @patch("tangostationcontrol.rpc.antennafield.tango.Database")
    def test_set_antenna_use(self, mock_tango_database, mock_create_device_proxy):
        self.mock_tango_db_response(mock_tango_database)

        mock_create_device_proxy.return_value.read_attribute.return_value.value = [1, 0]
        mock_create_device_proxy.return_value.Antenna_Status_R = [1, 0]
        mock_create_device_proxy.return_value.Antenna_Use_R = [1, 0]
        mock_create_device_proxy.return_value.Antenna_Names_R = ["Antenna0", "Antenna1"]

        request = SetAntennaUseRequest(
            antenna_use=1,
            identifier=Identifier(antennafield_name="LBA", antenna_name="Antenna1"),
        )

        # Act
        antenna_field = AntennaField()
        reply = antenna_field.SetAntennaUse(request, None)

        # Assert
        self.assertTrue(reply.success, msg=f"{reply.exception}")
        mock_create_device_proxy.return_value.put_property.assert_called_once_with(
            {"Antenna_Use": [1, 1]}
        )
