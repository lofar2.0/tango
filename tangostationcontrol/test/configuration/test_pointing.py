#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from jsonschema.exceptions import ValidationError

from tangostationcontrol.configuration import Pointing
from test import base


class TestPointing(base.TestCase):
    def test_from_json(self):
        ps = Pointing.from_json('{"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}')

        self.assertEqual(1.2, ps.angle1)
        self.assertEqual(2.1, ps.angle2)
        self.assertEqual("LMN", ps.direction_type)

    def test_from_json_type_missmatch(self):
        for json in [
            '{"angle1":"1.2", "angle2": 2.1, "direction_type":"LMN"}',
            '{"angle1":1.2, "angle2": "2.1", "direction_type":"LMN"}',
            '{"angle1":1.2, "angle2": 2.1, "direction_type":"ABC"}',
        ]:
            with self.assertRaises(ValidationError):
                Pointing.from_json(json)

    def test_from_json_missing_fields(self):
        for json in [
            '{"angle2": 2.1, "direction_type":"LMN"}',
            '{"angle1":1.2, "direction_type":"LMN"}',
            '{"angle1":1.2, "angle2": 2.1}',
        ]:
            with self.assertRaises(ValidationError):
                Pointing.from_json(json)

    def test_to_json(self):
        ps = Pointing(1.3, 2.3, "URANUS")
        self.assertEqual(
            ps.to_json(), '{"angle1": 1.3, "angle2": 2.3, "direction_type": "URANUS"}'
        )
