# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from jsonschema.exceptions import ValidationError

from tangostationcontrol.configuration import ObservationSettings
from tangostationcontrol.configuration import Sap
from tangostationcontrol.configuration import Pointing
from tangostationcontrol.configuration import ObservationFieldSettings

from test import base


class TestObservationSettings(base.TestCase):
    def test_from_json(self):
        sut = ObservationSettings.from_json(
            """
            {
                "station": "cs0001",
                "antenna_fields": [{
                    "observation_id": 3,
                    "stop_time": "2012-04-23T18:25:43",
                    "antenna_field": "HBA",
                    "antenna_set": "ALL",
                    "filter": "HBA_110_190",
                    "SAPs": [{
                        "subbands": [3, 2, 1],
                        "pointing": {
                            "angle1":1.2, "angle2": 2.1, "direction_type":"LMN"
                        }
                    }]
                }]
            }"""
        )

        self.assertEqual(sut.station, "cs0001")
        self.assertEqual(sut.antenna_fields[0].observation_id, 3)
        self.assertEqual(sut.antenna_fields[0].stop_time, "2012-04-23T18:25:43")
        self.assertEqual(sut.antenna_fields[0].antenna_set, "ALL")
        self.assertEqual(sut.antenna_fields[0].filter, "HBA_110_190")
        self.assertEqual(len(sut.antenna_fields[0].SAPs), 1)

    def test_to_json(self):
        sut = ObservationSettings(
            station="cs001",
            antenna_fields=[
                ObservationFieldSettings(
                    5,
                    "2022-10-26T11:35:54.704150",
                    "2022-10-27T11:35:54.704150",
                    "HBA",
                    "ALL",
                    "filter settings",
                    [
                        Sap([3, 2], Pointing(1.2, 2.1, "LMN")),
                        Sap([1], Pointing(3.3, 4.4, "MOON")),
                    ],
                )
            ],
        )
        self.assertEqual(
            sut.to_json(),
            '{"station": "cs001", "antenna_fields": [{"observation_id": 5, "start_time": '
            '"2022-10-26T11:35:54.704150", "stop_time": "2022-10-27T11:35:54.704150", '
            '"antenna_field": "HBA", "antenna_set": "ALL", '
            '"filter": "filter settings", "SAPs": '
            '[{"subbands": [3, 2], "pointing": {"angle1": 1.2, "angle2": 2.1, '
            '"direction_type": "LMN"}}, {"subbands": [1], "pointing": {"angle1": 3.3, '
            '"angle2": 4.4, "direction_type": "MOON"}}], "first_beamlet": 0}]}',
        )

    def test_throw_wrong_instance(self):
        for json_str in [
            '{"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}',
            '{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
        ]:
            with self.assertRaises(ValidationError):
                ObservationSettings.from_json(json_str)
