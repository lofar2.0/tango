#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from jsonschema.exceptions import ValidationError

from tangostationcontrol.configuration import Pointing, Sap
from test import base


class TestSapSettings(base.TestCase):
    def test_from_json(self):
        sap = Sap.from_json(
            '{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}'
        )

        self.assertEqual(sap.subbands, [3, 2, 1])
        self.assertEqual(sap.pointing.angle1, 1.2)
        self.assertEqual(sap.pointing.angle2, 2.1)
        self.assertEqual(sap.pointing.direction_type, "LMN")

    def test_from_json_type_missmatch(self):
        for json in [
            '{"subbands": ["3", 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
            '{"subbands": "3", "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
            '{"subbands": 3, "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
            '{"subbands": 3, "pointing": {"angle1":"1.2", "angle2": 2.1, "direction_type":"LMN"}}',
            '{"subbands": "3", "pointing": "test"}',
            '{"angle1":"1.2", "angle2": 2.1, "direction_type":"LMN"}',
        ]:
            with self.assertRaises(ValidationError):
                Sap.from_json(json)

    def test_from_json_missing_fields(self):
        for json in [
            '{"subbands": [], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
            '{"pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
            '{"subbands": [1], "pointing": {"angle2": 2.1, "direction_type":"LMN"}}',
            '{"subbands": [1]}',
        ]:
            with self.assertRaises(ValidationError):
                Sap.from_json(json)

    def test_to_json(self):
        sut = Sap([3, 2, 1], Pointing(1.3, 2.3, "URANUS"))
        self.assertEqual(
            sut.to_json(),
            '{"subbands": [3, 2, 1], "pointing": {"angle1": 1.3, "angle2": 2.3, "direction_type": "URANUS"}}',
        )

    def test_throw_wrong_instance(self):
        with self.assertRaises(ValidationError):
            Sap.from_json('{"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}')
