#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import json

from jsonschema.exceptions import ValidationError

from tangostationcontrol.configuration import (
    Pointing,
    ObservationFieldSettings,
    Sap,
    XST,
)

from test import base


class TestObservationFieldSettings(base.TestCase):
    def test_xst_from_json_simple(self):
        json_str = '{ "subbands": [1, 2, 3], "integration_interval": 1.0 }'
        sut = XST.from_json(json_str)

        # check whether converting back to JSON does not throw
        _ = sut.to_json()

    def test_from_json_simple(self):
        json_str = (
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", '
            '"antenna_field": "HBA", '
            '"antenna_set": "ALL", "filter": "HBA_110_190",'
            '"SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}]}'
        )
        sut = ObservationFieldSettings.from_json(json_str)

        self.assertEqual(sut.observation_id, 3)
        self.assertEqual(sut.stop_time, "2012-04-23T18:25:43")
        self.assertEqual(sut.antenna_set, "ALL")
        self.assertEqual(sut.filter, "HBA_110_190")
        self.assertEqual(len(sut.SAPs), 1)

        # check whether converting back to JSON does not throw
        _ = sut.to_json()

    def test_from_json_hba(self):
        json_str = (
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", '
            '"antenna_field": "HBA", '
            '"antenna_set": "ALL", "filter": "HBA_110_190",'
            '"SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}],'
            '"HBA": { "tile_beam": {"angle1":2.2, "angle2": 3.1, "direction_type":"MOON"} } }'
        )
        sut = ObservationFieldSettings.from_json(json_str)

        self.assertEqual(sut.HBA.tile_beam.angle1, 2.2)
        self.assertEqual(sut.HBA.tile_beam.angle2, 3.1)
        self.assertEqual(sut.HBA.tile_beam.direction_type, "MOON")

        # check whether converting back to JSON does not throw
        _ = sut.to_json()

    def test_from_json_first_beamlet(self):
        sut = ObservationFieldSettings.from_json(
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", '
            '"antenna_field": "HBA", '
            '"antenna_set": "ALL", "filter": "HBA_110_190",'
            '"SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}],'
            '"HBA": {"tile_beam": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}, "first_beamlet": 2}'
        )

        self.assertEqual(sut.first_beamlet, 2)

        # check whether converting back to JSON does not throw
        _ = sut.to_json()

    def test_from_json_statistics(self):
        sut = ObservationFieldSettings.from_json(
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", '
            '"antenna_field": "HBA", '
            '"antenna_set": "ALL", "filter": "HBA_110_190",'
            '"SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}],'
            '"HBA": {"tile_beam": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}},'
            '"XST": { "subbands": [1, 2, 3], "integration_interval": 1.0 }}'
        )

        # check whether converting back to JSON does not throw
        _ = sut.to_json()

    def test_from_json_type_missmatch(self):
        for json_str in [
            # observation_id
            '{"observation_id": "3", "stop_time": "2012-04-23T18:25:43", "antenna_field": "HBA", "antenna_set": "ALL", "filter": "HBA_110_190","SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}], "first_beamlet": 2}',
            # stop_time
            '{"observation_id": 3, "stop_time": "test", "antenna_field": "HBA", "antenna_set": "ALL", "filter": "HBA_110_190","SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}], "first_beamlet": 2}',
            # antenna_set
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", "antenna_field": "HBA", "antenna_set": 4, "filter": "HBA_110_190","SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}], "first_beamlet": 2}',
            # filter
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", "antenna_field": "HBA", "antenna_set": "ALL", "filter": 1,"SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}], "first_beamlet": 2}',
            # SAPs
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", "antenna_field": "HBA", "antenna_set": "ALL", "filter": "HBA_110_190","SAPs": {"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}, "first_beamlet": 2}',
            # first_beamlet
            '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", "antenna_field": "HBA", "antenna_set": "ALL", "filter": "HBA_110_190","SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}], "first_beamlet": "2"}',
        ]:
            with self.assertRaises((ValidationError, ValueError), msg=f"{json_str}"):
                ObservationFieldSettings.from_json(json_str)

    def test_from_json_missing_fields(self):
        complete_json = '{"observation_id": 3, "stop_time": "2012-04-23T18:25:43", "antenna_field": "HBA", "antenna_set": "ALL", "filter": "HBA_110_190","SAPs": [{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}], "HBA": {"tile_beam": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}, "first_beamlet": 2}'

        for field in (
            "observation_id",
            "stop_time",
            "antenna_field",
            "antenna_set",
            "filter",
            "SAPs",
        ):
            # construct a JSON string with the provided field missing
            json_dict = json.loads(complete_json)
            del json_dict[field]
            json_str = json.dumps(json_dict)

            # trigger validation error
            with self.assertRaises(
                (ValidationError, ValueError), msg=f"Omitted field {field}: {json_str}"
            ):
                ObservationFieldSettings.from_json(json_str)

    def test_to_json(self):
        sut = ObservationFieldSettings(
            5,
            "2022-10-26T11:35:54.704150",
            "2022-10-27T11:35:54.704150",
            "HBA",
            "ALL",
            "filter settings",
            [
                Sap([3, 2], Pointing(1.2, 2.1, "LMN")),
                Sap([1], Pointing(3.3, 4.4, "MOON")),
            ],
        )
        self.assertEqual(
            sut.to_json(),
            '{"observation_id": 5, "start_time": "2022-10-26T11:35:54.704150", '
            '"stop_time": "2022-10-27T11:35:54.704150", '
            '"antenna_field": "HBA", '
            '"antenna_set": "ALL", "filter": "filter settings", "SAPs": '
            '[{"subbands": [3, 2], "pointing": {"angle1": 1.2, "angle2": 2.1, '
            '"direction_type": "LMN"}}, {"subbands": [1], "pointing": {"angle1": 3.3, '
            '"angle2": 4.4, "direction_type": "MOON"}}], "first_beamlet": 0}',
        )

    def test_to_json_datetime_named_args(self):
        sut = ObservationFieldSettings(
            observation_id=5,
            start_time=None,
            stop_time="2106-02-07T00:00:00",
            antenna_field="HBA",
            antenna_set="ALL",
            filter="filter settings",
            SAPs=[
                Sap([3, 2], Pointing(1.2, 2.1, "LMN")),
                Sap([1], Pointing(3.3, 4.4, "MOON")),
            ],
        )

        self.assertEqual(
            sut.to_json(),
            '{"observation_id": 5, "stop_time": "2106-02-07T00:00:00", '
            '"antenna_field": "HBA", "antenna_set": "ALL", "filter": '
            '"filter settings", "SAPs": [{"subbands": [3, 2], "pointing": '
            '{"angle1": 1.2, "angle2": 2.1, "direction_type": "LMN"}}, '
            '{"subbands": [1], "pointing": {"angle1": 3.3, "angle2": 4.4, '
            '"direction_type": "MOON"}}], "first_beamlet": 0}',
        )

    def test_throw_wrong_instance(self):
        for json_str in [
            '{"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}',
            '{"subbands": [3, 2, 1], "pointing": {"angle1":1.2, "angle2": 2.1, "direction_type":"LMN"}}',
        ]:
            with self.assertRaises(ValidationError):
                ObservationFieldSettings.from_json(json_str)
