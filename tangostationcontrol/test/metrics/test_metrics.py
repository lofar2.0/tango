# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tango.server import (
    Device,
    attribute,
    AttrWriteType,
    command,
)
from tango.test_context import DeviceTestContext
from prometheus_client import generate_latest
from prometheus_client.registry import REGISTRY
from enum import IntEnum
from typing import Dict
import asyncio
import numpy
import time

from tangostationcontrol.common.device_decorators import DurationMetric
from tangostationcontrol.devices.base_device_classes.lofar_device import AttributePoller
from tangostationcontrol.metrics import (
    device_metrics,
    AttributeMetric,
    ScalarAttributeMetric,
    SpectrumAttributeMetric,
    ImageAttributeMetric,
)
from tangostationcontrol.metrics._metrics import wrap_method

from test import base


def parse_prometheus_response(prometheus_response: str) -> Dict[str, float]:
    """Decode the prometheus metrics HTTP response into a key: value dictionary, discarding labels."""

    lines = prometheus_response.split("\n")

    # parse lines.
    return {
        line.split("{")[0]: float(line.split(" ")[-1])
        for line in lines
        if line and not line.startswith("#")
    }


class TestMetrics(base.TestCase):
    class TestDevice(Device):
        """A Device extended with test functionality."""

        def __init__(self, cl, devname):
            self.attribute_poller = AttributePoller(self)

            super().__init__(cl, devname)

        def _init_device(self):
            # hook for @device_metrics to process a new device
            pass

        def init_device(self):
            super().init_device()

            self._init_device()

        def setUp(self):
            super().setUp()

            # Clear all metrics to make sure we don't register the same one twice
            # between tests (the library forbids it).
            #
            # Unfortunately, Metrics draw copies of prometheus_client.registry.REGISTRY,
            # and that does not provide a public interface to query the registered collectors.
            for collector in list(REGISTRY._collector_to_names.keys()):
                REGISTRY.unregister(collector)

        @command(dtype_out=str)
        def prometheus_response(self):
            return generate_latest().decode()

    def test_attribute_metric(self):
        class test_device(Device):
            @command()
            def test(device):
                # create an attribute metric and assign a value
                A = AttributeMetric("name", "description", {"foo": "bar"})
                A.set_value(10.0)

                # check collected metrics
                metric = A.metric.collect()[0]

                self.assertEqual("ds_name", metric.name)
                self.assertEqual("description", metric.documentation)

                self.assertEqual(1, len(metric.samples))
                self.assertEqual("ds_name", metric.samples[0].name)
                self.assertEqual(10.0, metric.samples[0].value)

                # check labels as we provided
                self.assertDictEqual(
                    {
                        "foo": "bar",
                    },
                    metric.samples[0].labels,
                )

        with DeviceTestContext(test_device, process=False) as proxy:
            proxy.test()

    def test_scalar_attribute_metric(self):
        """Test ScalarAttributeMetric"""

        class MyEnum(IntEnum):
            ZERO = 0
            ONE = 1

        class test_device(Device):
            float_attr = attribute(
                doc="docstr",
                dtype=int,
                fget=lambda obj: 42,
            )

            str_attr = attribute(
                doc="docstr",
                dtype=str,
                fget=lambda obj: "foo",
            )

            enum_attr = attribute(
                doc="docstr",
                dtype=MyEnum,
                fget=lambda obj: MyEnum.ONE,
            )

            def init_device(self):
                # create an attribute metric and assign a value
                self.float_metric = ScalarAttributeMetric(self, self.float_attr)
                self.str_metric = ScalarAttributeMetric(self, self.str_attr)
                self.enum_metric = ScalarAttributeMetric(self, self.enum_attr)

            @command()
            def test(device):
                # check collected metrics (float_attr)
                metric = device.float_metric.metric.collect()[0]

                self.assertEqual("ds_float_attr", metric.name)
                self.assertEqual("docstr", metric.documentation)
                self.assertEqual(42.0, metric.samples[0].value)

                # check labels as the DeviceTestContext would result in
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                    },
                    metric.samples[0].labels,
                )

                # check collected metrics (str_attr)
                metric = device.str_metric.metric.collect()[0]

                self.assertEqual("ds_str_attr", metric.name)
                self.assertEqual("docstr", metric.documentation)
                self.assertEqual(1, metric.samples[0].value)

                # check labels as the DeviceTestContext would result in
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "str_value": "foo",
                    },
                    metric.samples[0].labels,
                )

                # check collected metrics (enum_attr)
                metric = device.enum_metric.metric.collect()[0]

                self.assertEqual("ds_enum_attr", metric.name)
                self.assertEqual("docstr", metric.documentation)

                # check labels as the DeviceTestContext would result in
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "ds_enum_attr": "ZERO",
                    },
                    metric.samples[0].labels,
                )
                self.assertEqual(0, metric.samples[0].value)

                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "ds_enum_attr": "ONE",
                    },
                    metric.samples[1].labels,
                )
                self.assertEqual(1, metric.samples[1].value)

        with DeviceTestContext(test_device, process=False) as proxy:
            # access the attribute to trigger value propagation to metric
            _ = proxy.float_attr
            _ = proxy.str_attr
            _ = proxy.enum_attr

            proxy.test()

    def test_metric_names_labels(self):
        """Test whether ScalarAttributeMetric results in the expected metric name and labels."""

        class test_device(Device):
            readonly_R = attribute(
                doc="docstr",
                dtype=int,
                fget=lambda obj: 42,
                access=AttrWriteType.READ,
            )

            readwrite_RW = attribute(
                doc="docstr",
                dtype=int,
                fget=lambda obj: 42,
                fset=lambda obj, value: None,
                access=AttrWriteType.READ_WRITE,
            )

            def init_device(self):
                # create an attribute metric and assign a value
                self.readonly_metric = ScalarAttributeMetric(self, self.readonly_R)
                self.readwrite_metric = ScalarAttributeMetric(self, self.readwrite_RW)

            @command()
            def test(device):
                # check collected metrics (readonly_R)
                metric = device.readonly_metric.metric.collect()[0]

                # check name
                self.assertEqual("ds_readonly", metric.name)

                # check labels
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                    },
                    metric.samples[0].labels,
                )

                # check collected metrics (readwrite_RW)
                metric = device.readwrite_metric.metric.collect()[0]

                self.assertEqual("ds_readwrite", metric.name)

                # check labels
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "rw",
                    },
                    metric.samples[0].labels,
                )

        with DeviceTestContext(test_device, process=False) as proxy:
            # access the attribute to trigger value propagation to metric
            _ = proxy.readonly_R
            _ = proxy.readwrite_RW

            proxy.test()

    def test_spectrum_attribute_metric(self):
        """Test SpectrumAttributeMetric"""

        class MyEnum(IntEnum):
            ZERO = 0
            ONE = 1

        class test_device(Device):
            float_attr = attribute(
                doc="docstr",
                dtype=(int,),
                max_dim_x=3,
                fget=lambda obj: [1, 2, 3],
            )

            enum_attr = attribute(
                doc="docstr",
                dtype=(MyEnum,),
                max_dim_x=2,
                fget=lambda obj: [MyEnum.ONE, MyEnum.ZERO],
            )

            def init_device(self):
                # create an attribute metric and assign a value
                self.float_metric = SpectrumAttributeMetric(self, self.float_attr)
                self.enum_metric = SpectrumAttributeMetric(self, self.enum_attr)

            @command()
            def test(device):
                # check collected metrics (float_attr)
                metric = device.float_metric.metric.collect()[0]

                self.assertEqual("ds_float_attr", metric.name)
                self.assertEqual("docstr", metric.documentation)

                # check labels as the DeviceTestContext would result in
                self.assertEqual(1.0, metric.samples[0].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "00",
                    },
                    metric.samples[0].labels,
                )

                self.assertEqual(2.0, metric.samples[1].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "01",
                    },
                    metric.samples[1].labels,
                )

                self.assertEqual(3.0, metric.samples[2].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "02",
                    },
                    metric.samples[2].labels,
                )

                # check collected metrics (enum_attr)
                metric = device.enum_metric.metric.collect()[0]

                self.assertEqual("ds_enum_attr", metric.name)
                self.assertEqual("docstr", metric.documentation)

                # check labels as the DeviceTestContext would result in
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "ds_enum_attr": "ZERO",
                        "x": "00",
                    },
                    metric.samples[0].labels,
                )
                self.assertEqual(0, metric.samples[0].value)

                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "ds_enum_attr": "ONE",
                        "x": "00",
                    },
                    metric.samples[1].labels,
                )
                self.assertEqual(1, metric.samples[1].value)

                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "ds_enum_attr": "ZERO",
                        "x": "01",
                    },
                    metric.samples[2].labels,
                )
                self.assertEqual(1, metric.samples[2].value)

                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "ds_enum_attr": "ONE",
                        "x": "01",
                    },
                    metric.samples[3].labels,
                )
                self.assertEqual(0, metric.samples[3].value)

        with DeviceTestContext(test_device, process=False) as proxy:
            # access the attribute to trigger value propagation to metric
            _ = proxy.float_attr
            _ = proxy.enum_attr

            proxy.test()

    def test_image_attribute_metric(self):
        """Test ImageAttributeMetric"""

        class test_device(Device):
            float_attr = attribute(
                doc="docstr",
                dtype=((int,),),
                max_dim_x=3,
                max_dim_y=2,
                fget=lambda obj: numpy.array([[1, 2, 3], [4, 5, 6]], dtype=int),
            )

            def init_device(self):
                # create an attribute metric and assign a value
                self.float_metric = ImageAttributeMetric(self, self.float_attr)

            @command()
            def test(device):
                # check collected metrics (float_attr)
                metric = device.float_metric.metric.collect()[0]

                self.assertEqual("ds_float_attr", metric.name)
                self.assertEqual("docstr", metric.documentation)

                # check labels as the DeviceTestContext would result in
                self.assertEqual(1.0, metric.samples[0].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "00",
                        "y": "00",
                        "idx": "000",
                    },
                    metric.samples[0].labels,
                )

                self.assertEqual(2.0, metric.samples[1].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "00",
                        "y": "01",
                        "idx": "001",
                    },
                    metric.samples[1].labels,
                )

                self.assertEqual(3.0, metric.samples[2].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "00",
                        "y": "02",
                        "idx": "002",
                    },
                    metric.samples[2].labels,
                )

                self.assertEqual(4.0, metric.samples[3].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "01",
                        "y": "00",
                        "idx": "003",
                    },
                    metric.samples[3].labels,
                )

                self.assertEqual(5.0, metric.samples[4].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "01",
                        "y": "01",
                        "idx": "004",
                    },
                    metric.samples[4].labels,
                )

                self.assertEqual(6.0, metric.samples[5].value)
                self.assertDictEqual(
                    {
                        "domain": "test",
                        "family": "nodb",
                        "member": "test_device",
                        "device_class": "test_device",
                        "access": "r",
                        "x": "01",
                        "y": "02",
                        "idx": "005",
                    },
                    metric.samples[5].labels,
                )

        with DeviceTestContext(test_device, process=False) as proxy:
            # access the attribute to trigger value propagation to metric
            _ = proxy.float_attr

            proxy.test()

    def test_wrap_device_server_default_metrics(self):
        """Test whether default metrics are exposed."""

        @device_metrics()
        class test_device(TestMetrics.TestDevice):
            pass

        with DeviceTestContext(test_device, process=False) as proxy:
            metrics = parse_prometheus_response(proxy.prometheus_response())

            # check if standard metrics are exposed
            self.assertIn("ds_access_count_total", metrics)
            self.assertIn("ds_state", metrics)
            self.assertIn("ds_software_version_info", metrics)

    def test_wrap_method_sync(self):
        """Test whether wrap_method correctly identifies and wraps synchronous methods."""

        class MyAsync:
            def __init__(self):
                self.executed = False
                self.wrapper_called = False

            def function(self):
                self.executed = True

        def wrapper(instance, *args, **kwargs):
            instance.wrapper_called = True

        wrap_method(MyAsync, MyAsync.function, wrapper)

        # run it
        obj = MyAsync()
        obj.function()

        # check if the coroutine was actually executed
        self.assertTrue(obj.executed)
        self.assertTrue(obj.wrapper_called)

    def test_wrap_method_async(self):
        """Test whether wrap_method correctly identifies and wraps asynchronous methods."""

        class MyAsync:
            def __init__(self):
                self.awaited = False
                self.wrapper_called = False

            async def function(self):
                self.awaited = True

        def wrapper(instance, *args, **kwargs):
            instance.wrapper_called = True

        wrap_method(MyAsync, MyAsync.function, wrapper)

        # check if the wrapped function is still a coroutine
        obj = MyAsync()
        self.assertTrue(asyncio.iscoroutinefunction(obj.function))

        # run it
        asyncio.run(obj.function())

        # check if the coroutine was actually executed
        self.assertTrue(obj.awaited)
        self.assertTrue(obj.wrapper_called)

    def test_wrap_device_server_attributes(self):
        """Test whether attributes are exposed as metrics."""

        @device_metrics()
        class test_device(TestMetrics.TestDevice):
            FOO = attribute(
                doc="This is FOO",
                dtype=int,
                fget=lambda obj: 42,
                fset=lambda obj, value: None,
            )

        with DeviceTestContext(test_device, process=False) as proxy:
            # make sure the attribute is written
            proxy.FOO = 123

            # check if FOO is exposed as a metric
            metrics = parse_prometheus_response(proxy.prometheus_response())
            self.assertIn("ds_foo", metrics)

    def test_set_value_none(self):
        """Test behaviour of setting None as a value in a metric."""

        @device_metrics()
        class test_device(TestMetrics.TestDevice):
            FOO = attribute(
                doc="This is FOO",
                dtype=str,
                fget=lambda obj: None,
                fset=lambda obj, value: None,
            )

            @command()
            def test(device):
                # writing None should fail
                with self.assertRaises(ValueError):
                    device.metrics["FOO"].set_value(None)

        with DeviceTestContext(test_device, process=False) as proxy:
            proxy.test()

    def test_track_value_modification(self):
        """Test whether metrics update if the attribute's value is accessed."""

        @device_metrics()
        class test_device(TestMetrics.TestDevice):
            BAR = attribute(
                doc="This is BAR",
                dtype=int,
                access=AttrWriteType.READ_WRITE,
                fget=lambda obj: 42,
                fset=lambda obj, value: None,
            )

        with DeviceTestContext(test_device, process=False) as proxy:
            # write the attribute, updating its metric
            proxy.BAR = 2

            # check metrics (which is the value written)
            metrics = parse_prometheus_response(proxy.prometheus_response())
            self.assertEqual(2.0, metrics["ds_bar"])

            # write the attribute, updating its metric
            proxy.BAR = 1

            # check metrics (which is the value written)
            metrics = parse_prometheus_response(proxy.prometheus_response())
            self.assertEqual(1.0, metrics["ds_bar"])

    def test_DurationMetric(self):
        """Test whether attributes are exposed as metrics."""

        class test_device(TestMetrics.TestDevice):
            def __init__(self, cls, dev_name):
                super().__init__(cls, dev_name)

                # create interface for DurationMetric
                self.metric_labels = {"foo": "bar"}

            @DurationMetric()
            @command(dtype_in=float)
            def sleep(self, duration):
                time.sleep(duration)

        with DeviceTestContext(test_device, process=False) as proxy:
            proxy.sleep(0.001)

            metrics = parse_prometheus_response(proxy.prometheus_response())
            self.assertIn("ds_duration_sleep_bucket", metrics)
