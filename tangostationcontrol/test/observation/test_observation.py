# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import copy
import datetime
from unittest import mock

from tango import DevState
from tangostationcontrol.observation.observation import Observation
from tangostationcontrol.observation import observation_field
from tangostationcontrol.test.dummy_observation_settings import (
    get_observation_settings_two_fields_core,
    get_observation_settings_hba_core_immediate,
)

from test import base


@mock.patch("tango.Util.instance")
class TestObservation(base.TestCase):
    def test_properties(self, _):
        sut = Observation("DMR", get_observation_settings_two_fields_core())
        self.assertEqual(5, sut.observation_id)
        self.assertEqual(["HBA0", "LBA"], sut.antenna_fields)

    @staticmethod
    def mocked_observation_field_base(m_obs_field) -> Observation:
        """Base function for tests using mocked ObservationField instances"""

        obs_settings = get_observation_settings_two_fields_core()
        sut = Observation("DMR", obs_settings)

        for antenna_field in obs_settings.antenna_fields:
            sut._observation_fields.append(
                copy.deepcopy(m_obs_field(tango_domain="DMR", parameters=antenna_field))
            )

        return sut

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_is_running_none(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)

        for obs_field in sut._observation_fields:
            obs_field.is_running.return_value = False

        self.assertFalse(sut.is_running())

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_is_running(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)

        for obs_field in sut._observation_fields:
            obs_field.is_running.return_value = True

        self.assertTrue(sut.is_running())

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_is_partially_running(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)

        for obs_field in sut._observation_fields:
            obs_field.is_running.return_value = False

        sut._observation_fields[0].is_running = True

        self.assertTrue(sut.is_partially_running())

    def test_create_devices(self, tu_mock):
        sut = Observation("DMR", get_observation_settings_two_fields_core())
        sut.create_devices()

        self.assertEqual(
            len(get_observation_settings_two_fields_core().antenna_fields),
            len(sut._observation_fields),
        )

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_destroy_devices(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)
        sut.destroy_devices()

        for obs_field in sut._observation_fields:
            obs_field.destroy_observation_field_device.assert_called_once()

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_initialise_observation(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)

        sut.initialise_observation()

        for obs_field in sut._observation_fields:
            obs_field.initialise_observation_field.assert_called_once()

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_start(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)

        sut.start()

        for obs_field in sut._observation_fields:
            obs_field.start.assert_called_once()

    def test_update_observation_state(self, tu_mock):
        sut = Observation("DMR", get_observation_settings_hba_core_immediate())

        sut._stop_antenna_field = mock.Mock()
        sut._start_antenna_field = mock.Mock()

        m_device_proxy = mock.Mock(
            antenna_field_R="HBA0",
            start_time_R=datetime.datetime.now().timestamp(),
            stop_time_R=(
                datetime.datetime.now() + datetime.timedelta(hours=1)
            ).timestamp(),
            lead_time_R=5,
            state=mock.Mock(return_value=DevState.STANDBY),
        )

        sut._started_antenna_fields = 1  # test default lambda callback
        sut._update_observation_state(m_device_proxy)
        sut._start_antenna_field.assert_called_once_with(m_device_proxy.antenna_field_R)

        sut._stopped_antenna_fields = 1  # test default lambda callback
        m_device_proxy.stop_time_R = datetime.datetime.now().timestamp()
        sut._update_observation_state(m_device_proxy)
        sut._stop_antenna_field.assert_called_once_with(m_device_proxy.antenna_field_R)

    def test_observation_callback(self, tu_mock):
        """Test the start / stop observation callbacks"""
        t_params = get_observation_settings_hba_core_immediate()

        t_callback = mock.Mock()

        sut = Observation(
            tango_domain="DMR",
            parameters=t_params,
            start_callback=t_callback,
            stop_callback=t_callback,
        )
        sut._stop_antenna_field = mock.Mock()
        sut._start_antenna_field = mock.Mock()

        m_device_proxy = mock.Mock(
            antenna_field_R="HBA",
            start_time_R=datetime.datetime.now().timestamp(),
            stop_time_R=(
                datetime.datetime.now() + datetime.timedelta(hours=1)
            ).timestamp(),
            lead_time_R=5,
            state=mock.Mock(return_value=DevState.STANDBY),
        )

        sut._started_antenna_fields = 1
        sut._update_observation_state(m_device_proxy)
        t_callback.assert_called_once_with(t_params.antenna_fields[0].observation_id)

        sut._stopped_antenna_fields = 1
        m_device_proxy.stop_time_R = datetime.datetime.now().timestamp()
        sut._update_observation_state(m_device_proxy)
        self.assertEqual(2, t_callback.call_count)

    def bound_callback(self, obs_id: int):
        """Test passing bound (self) callback"""
        self.assertEqual(
            get_observation_settings_hba_core_immediate()
            .antenna_fields[0]
            .observation_id,
            obs_id,
        )

    def test_bound_observation_callback(self, tu_mock):
        """Test calling bound callback"""
        sut = Observation(
            tango_domain="DMR",
            parameters=get_observation_settings_hba_core_immediate(),
            start_callback=self.bound_callback,
        )
        sut._stop_antenna_field = mock.Mock()
        sut._start_antenna_field = mock.Mock()

        m_device_proxy = mock.Mock(
            antenna_field_R="HBA",
            start_time_R=datetime.datetime.now().timestamp(),
            stop_time_R=(
                datetime.datetime.now() + datetime.timedelta(hours=1)
            ).timestamp(),
            lead_time_R=5,
            state=mock.Mock(return_value=DevState.STANDBY),
        )

        sut._started_antenna_fields = 1
        sut._update_observation_state(m_device_proxy)

    @mock.patch.object(observation_field, "ObservationField", autospec=True)
    def test_stop(self, m_obs_field, tu_mock):
        sut = self.mocked_observation_field_base(m_obs_field)

        sut.stop()

        for obs_field in sut._observation_fields:
            obs_field.stop.assert_called_once()
