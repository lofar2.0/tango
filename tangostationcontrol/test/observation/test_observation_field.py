# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import importlib
import sys
from unittest import mock
from unittest.mock import Mock

from tango import DevState, DevFailed

from tangostationcontrol.observation import observation_field
from tangostationcontrol.common.proxies.proxy import create_device_proxy
from tangostationcontrol.test.dummy_observation_settings import (
    get_observation_settings_two_fields_core,
)

from test import base


@mock.patch("tango.Util.instance")
class TestObservationField(base.TestCase):
    def test_properties(self, _):
        sut = observation_field.ObservationField(
            "DMR", get_observation_settings_two_fields_core().antenna_fields[0]
        )
        self.assertEqual(5, sut.observation_id)
        self.assertEqual("HBA0", sut.antenna_field)
        self.assertEqual("ObservationField", sut.class_name)
        self.assertEqual("DMR/ObservationField/5-HBA0", sut.device_name)

    def test_create_observation_device(self, tu_mock):
        sut = observation_field.ObservationField(
            "DMR", get_observation_settings_two_fields_core().antenna_fields[0]
        )
        sut.create_observation_field_device()

        tu_mock.return_value.create_device.assert_called_with(
            sut.class_name, sut.device_name
        )

    def test_create_observation_device_fail(self, tu_mock):
        """Test creation failed"""
        sut = observation_field.ObservationField(
            "DMR", get_observation_settings_two_fields_core().antenna_fields[0]
        )

        with mock.patch.object(observation_field, "logger") as m_logger:
            tu_mock.return_value.create_device.side_effect = [DevFailed]

            self.assertRaises(DevFailed, sut.create_observation_field_device)

            m_logger.exception.assert_called()

    @mock.patch("tango.DeviceProxy")
    def test_initialise_observation_field(self, dp_mock, tu_mock):
        importlib.reload(sys.modules[create_device_proxy.__module__])
        sut = observation_field.ObservationField(
            "DMR", get_observation_settings_two_fields_core().antenna_fields[0]
        )
        sut.initialise_observation_field()

        self.assertEqual(
            dp_mock.return_value.observation_field_settings_RW,
            get_observation_settings_two_fields_core().antenna_fields[0].to_json(),
        )
        dp_mock.return_value.Initialise.assert_called()

    @mock.patch("tango.DeviceProxy")
    def test_start(self, dp_mock, tu_mock):
        importlib.reload(sys.modules[create_device_proxy.__module__])
        sut = observation_field.ObservationField(
            "DMR", get_observation_settings_two_fields_core().antenna_fields[0]
        )
        sut.initialise_observation_field()
        sut.start()

        dp_mock.return_value.On.assert_called()

    def test_stop(self, tu_mock):
        importlib.reload(sys.modules[observation_field.ObservationField.__module__])
        sut = observation_field.ObservationField(
            "DMR", get_observation_settings_two_fields_core().antenna_fields[0]
        )

        dp_mock = Mock()
        dp_mock.state.return_value = DevState.OFF

        sut._device_proxy = dp_mock

        sut.stop()

        dp_mock.ping.assert_called()
        dp_mock.Off.assert_called()

        tu_mock.return_value.delete_device.assert_called()
