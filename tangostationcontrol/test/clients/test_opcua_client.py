# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
from contextlib import asynccontextmanager
import io
import unittest
from unittest import mock
from unittest.mock import patch

import asyncua
import numpy
import psutil

from tangostationcontrol.clients import opcua_client
from tangostationcontrol.clients.opcua_client import (
    OPCUAConnection,
    OPCUAConnectionStatus,
)

from test.opcua import make_opcua_server


class AttrProps:
    def __init__(self, numpy_type):
        self.numpy_type = numpy_type


ATTR_TEST_TYPES = [
    AttrProps(numpy_type=bool),
    AttrProps(numpy_type=str),
    AttrProps(numpy_type=numpy.float32),
    AttrProps(numpy_type=numpy.float64),
    AttrProps(numpy_type=numpy.double),
    AttrProps(numpy_type=numpy.uint8),
    AttrProps(numpy_type=numpy.uint16),
    AttrProps(numpy_type=numpy.uint32),
    AttrProps(numpy_type=numpy.uint64),
    AttrProps(numpy_type=numpy.int16),
    AttrProps(numpy_type=numpy.int32),
    AttrProps(numpy_type=numpy.int64),
]

SCALAR_SHAPE = (1,)
SPECTRUM_SHAPE = (4,)
IMAGE_SHAPE = (2, 3)
DIMENSION_TESTS = [SCALAR_SHAPE, SPECTRUM_SHAPE, IMAGE_SHAPE]


def create_mocked_coroutine(return_value=None):
    if return_value is None:
        return_value = mock.Mock()

    async def mock_coro(*args, **kwargs):
        return return_value

    return mock.Mock(wraps=mock_coro)


class TestOPCuaAgainstServer(unittest.IsolatedAsyncioTestCase):
    """Test OPCUAConnection against an actual server that we spawn."""

    def make_client(self) -> asyncua.Client:
        return OPCUAConnection(
            "opc.tcp://127.0.0.1:12345",
            "namespace",
            1.0,
            OPCUAConnectionStatus(),
        )

    @asynccontextmanager
    async def assert_no_connection_leak(self):
        """Raises AssertionError if the set of connections opened by this
        process changes over the lifetime of the managed context."""

        process = psutil.Process()
        connections_before = process.connections()
        try:
            yield
        finally:
            # wait for connection to drain and close
            await asyncio.sleep(0.1)

            connections_after = process.connections()
            self.assertEqual(set(connections_after), set(connections_before))

    async def asyncSetUp(self):
        self.server = await make_opcua_server()

    async def asyncTearDown(self):
        self.server = None

    async def test_connect(self):
        """A simple connect + disconnect should not cause issues."""

        async with self.server, self.assert_no_connection_leak():
            client = self.make_client()

            await client.connect()

            attr = await client._protocol_attribute(
                ["2:ServerInterfaces", "2:TestDevice", "2:float_R"], 1, 0, numpy.float32
            )
            self.assertEqual(20.0, await attr.read_function())

            await client.disconnect(None)

    @patch.object(OPCUAConnectionStatus, "connection_lost")
    async def test_handle_connecton_exception(self, m_connection_lost):
        """A connection exception should lead to a disconnect."""

        async with self.server, self.assert_no_connection_leak():
            client = self.make_client()

            await client.connect()
            await client.disconnect(Exception())

    async def test_reconnect(self):
        """Try reconnecting."""

        async with self.server, self.assert_no_connection_leak():
            client = self.make_client()

            await client.connect()
            client.running = (
                True  # simulate client.start() without starting the watcher thread
            )
            await client.disconnect(None)
            await client.reconnect()

            attr = await client._protocol_attribute(
                ["2:ServerInterfaces", "2:TestDevice", "2:float_R"], 1, 0, numpy.float32
            )
            self.assertEqual(20.0, await attr.read_function())

            await client.disconnect(None)


class TestOPCua(unittest.IsolatedAsyncioTestCase):
    @patch.object(OPCUAConnection, "ping")
    @patch.object(OPCUAConnection, "_servername")
    @patch.object(opcua_client, "Client")
    async def test_opcua_connection(self, m_opc_client, m_servername, m_ping):
        """
        This tests verifies whether the correct connection steps happen. It checks whether we can init an OPCUAConnection object
        Whether we can set the namespace, and the OPCua client.
        """

        m_opc_client_members = create_mocked_coroutine()
        m_opc_client_members.get_namespace_index = create_mocked_coroutine(
            return_value=42
        )
        m_opc_client_members.connect = create_mocked_coroutine()
        m_opc_client_members.disconnect = create_mocked_coroutine()
        m_opc_client_members.send_hello = create_mocked_coroutine()

        m_objects_node = mock.Mock()
        m_objects_node.get_child = create_mocked_coroutine()
        m_objects_node.get_children_descriptions = create_mocked_coroutine([])
        m_opc_client_members.get_objects_node = mock.Mock(return_value=m_objects_node)
        m_opc_client_members.update_protocol_attributes = create_mocked_coroutine()
        m_opc_client.return_value = m_opc_client_members

        test_client = OPCUAConnection(
            "opc.tcp://localhost:4874/freeopcua/server/",
            "http://lofar.eu",
            5,
            OPCUAConnectionStatus(),
        )

        try:
            await test_client.start()

            m_opc_client.assert_called_once()  # makes sure the actual freeOPCua client object is created only once

            # this also implies test_client.connect() is called
            m_opc_client_members.get_namespace_index.assert_called_once_with(
                "http://lofar.eu"
            )
            self.assertEqual(42, test_client.name_space_index)
        finally:
            await test_client.stop()

    @unittest.skip(reason="freezes sometimes")
    @patch.object(OPCUAConnection, "ping")
    @patch.object(OPCUAConnection, "_servername")
    @patch.object(opcua_client, "Client")
    @patch.object(opcua_client, "ProtocolAttribute")
    async def test_opcua_attr_setup(
        self, m_protocol_attr, m_opc_client, m_servername, m_ping
    ):
        """
        This tests covers the correct creation of read/write functions.
        In normal circumstances called by he attribute wrapper.
        Will be given 'comms_annotation', for OPCua that will be a node path and can access the attributes type and dimensions

        Test succeeds if there are no errors.
        """

        m_opc_client_members = create_mocked_coroutine()
        m_opc_client_members.get_namespace_index = create_mocked_coroutine(
            return_value=2
        )
        m_opc_client_members.connect = create_mocked_coroutine()
        m_opc_client_members.disconnect = create_mocked_coroutine()
        m_opc_client_members.send_hello = create_mocked_coroutine()
        m_objects_node = mock.Mock()
        m_objects_node.get_child = create_mocked_coroutine()
        m_objects_node.get_children_descriptions = create_mocked_coroutine([])
        m_opc_client_members.get_objects_node = mock.Mock(return_value=m_objects_node)
        m_opc_client_members.update_protocol_attributes = create_mocked_coroutine()

        m_opc_client.return_value = m_opc_client_members

        for i in ATTR_TEST_TYPES:

            class MockAttr:
                def __init__(self, dtype, x, y):
                    self.datatype = dtype
                    self.dim_x = x
                    self.dim_y = y

            for j in DIMENSION_TESTS:
                if len(j) == 1:
                    dim_x = j[0]
                    dim_y = 0
                else:
                    dim_x = j[1]
                    dim_y = j[0]

                # create a fake attribute with only the required variables in it.
                m_attribute = MockAttr(i.numpy_type, dim_x, dim_y)

                # pretend like there is a running OPCua server with a node that has this name
                m_annotation = [
                    f"2:testNode_{str(i.numpy_type)}_{str(dim_x)}_{str(dim_y)}"
                ]

                test_client = OPCUAConnection(
                    "opc.tcp://localhost:4874/freeopcua/server/",
                    "http://lofar.eu",
                    5,
                    OPCUAConnectionStatus(),
                )

                try:
                    await test_client.start()
                    await test_client.setup_attribute(m_annotation, m_attribute)
                finally:
                    await test_client.stop()

                # success if there are no errors.

    def test_protocol_attr(self):
        """
        This tests finding an OPCua node and returning a valid object with read/write functions.
        (This step is normally initiated by the AttributeWrapper)
        """

        # for all datatypes
        for i in ATTR_TEST_TYPES:
            # for all dimensions
            for j in DIMENSION_TESTS:
                node = mock.Mock()

                # handle scalars slightly differently
                if len(j) == 1:
                    dims = (j[0], 0)
                else:
                    dims = (j[1], j[0])

                ua_type = opcua_client.numpy_to_OPCua_dict[i.numpy_type]
                test = opcua_client.ProtocolAttribute(
                    node, asyncio.Lock(), dims[0], dims[1], ua_type
                )
                print(test.dim_y, test.dim_x, test.ua_type)

                """
                Part of the test already includes simply not throwing an exception, but for the sake coverage these asserts have also
                been added.
                """
                self.assertTrue(
                    test.dim_y == dims[1],
                    f"Dimensionality error, ProtocolAttribute.dim_y got: {test.dim_y} expected: {dims[1]}",
                )
                self.assertTrue(
                    test.dim_x == dims[0],
                    f"Dimensionality error, ProtocolAttribute.dim_y got: {test.dim_x} expected: {dims[0]}",
                )
                self.assertTrue(
                    test.ua_type == ua_type,
                    f"type error. Got: {test.ua_type} expected: {ua_type}",
                )
                self.assertTrue(
                    hasattr(test, "write_function"), f"No write function found"
                )
                self.assertTrue(
                    hasattr(test, "read_function"), f"No read function found"
                )

    def _get_test_value(self, dims, n_type):
        """get numpy array of the test value"""
        return numpy.zeros(dims, n_type)

    def _wrap_dims(self, value, dims):
        """Wrap a value in the current number of dimensions"""
        if dims == 0:
            return value
        elif dims == 1:
            return [value]
        elif dims == 2:
            return [[value]]

    def _get_mock_value(self, value, n_type):
        """get opcua Varianttype array of the test value"""
        return asyncua.ua.uatypes.Variant(
            Value=value, VariantType=opcua_client.numpy_to_OPCua_dict[n_type]
        )

    async def test_read(self):
        """
        This tests the read functions.
        """

        async def get_flat_value(j, i):
            return self._get_test_value(j, i.numpy_type).flatten()

        for j in DIMENSION_TESTS:
            for i in ATTR_TEST_TYPES:
                self._get_test_value(j, i.numpy_type)

                m_node = mock.Mock()

                if len(j) == 1:
                    test = opcua_client.ProtocolAttribute(
                        m_node,
                        asyncio.Lock(),
                        j[0],
                        0,
                        opcua_client.numpy_to_OPCua_dict[i.numpy_type],
                    )
                else:
                    test = opcua_client.ProtocolAttribute(
                        m_node,
                        asyncio.Lock(),
                        j[1],
                        j[0],
                        opcua_client.numpy_to_OPCua_dict[i.numpy_type],
                    )
                m_node.get_value.return_value = get_flat_value(j, i)
                val = await test.read_function()

                comp = val == self._get_test_value(j, i.numpy_type)
                self.assertTrue(
                    comp.all(),
                    "Read value unequal to expected value: \n\t{} \n\t{}".format(
                        val, self._get_test_value(j, i.numpy_type)
                    ),
                )

    async def test_read_unicode(self):
        """
        Test whether unicode characters are replaced by '?'.
        """

        async def get_unicode_value(dims):
            return self._wrap_dims(b"foo \xef\xbf\xbd bar".decode("utf-8"), dims)

        # test 0-2 dimensions of strings
        for dims in range(0, 2):
            m_node = mock.Mock()
            m_node.get_value.return_value = get_unicode_value(dims)

            # create the ProtocolAttribute to test
            test = opcua_client.ProtocolAttribute(
                m_node, asyncio.Lock(), 1, 0, opcua_client.numpy_to_OPCua_dict[str]
            )

            # check if unicode is replaced by ?
            val = await test.read_function()
            self.assertEqual(self._wrap_dims("foo ? bar", dims), val)

    def test_type_map(self):
        for numpy_type, opcua_type in opcua_client.numpy_to_OPCua_dict.items():
            # derive a default value that can get lost in a type translation
            if numpy_type is str:
                default_value = "foo"
            elif numpy_type == bool:
                default_value = True
            else:
                # integer or float type
                # integers: numpy will drop the decimals for us
                # floats: make sure we chose a value that has an exact binary representation
                default_value = 42.25

            # apply our mapping
            v = asyncua.ua.uatypes.Variant(
                Value=numpy_type(default_value), VariantType=opcua_type
            )

            try:
                # try to convert it to binary to force opcua to parse the value as the type
                binary = asyncua.ua.ua_binary.variant_to_binary(v)

                # reinterpret the resulting binary to obtain what opcua made of our value
                binary_stream = io.BytesIO(binary)
                reparsed_v = asyncua.ua.ua_binary.variant_from_binary(binary_stream)
            except Exception as e:
                raise Exception(
                    f"Conversion {numpy_type} -> {opcua_type} failed."
                ) from e

            # did the value get lost in translation?
            self.assertEqual(
                v.Value,
                reparsed_v.Value,
                msg=f"Conversion {numpy_type} -> {opcua_type} failed.",
            )

            # does the OPC-UA type have the same datasize (and thus, precision?)
            if numpy_type not in [str, bool]:
                self.assertEqual(
                    numpy_type().itemsize,
                    getattr(asyncua.ua.ua_binary.Primitives, opcua_type.name).size,
                    msg=f"Conversion {numpy_type} -> {opcua_type} failed: precision mismatch",
                )

    @unittest.skip(reason="broken")
    async def test_write(self):
        """
        Test the writing of values by instantiating a ProtocolAttribute attribute, and calling the write function.
        but the opcua function that writes to the server has been changed to the compare_values function.
        This allows the code to compare what values we want to write and what values would be given to a server.
        """

        async def compare_values(val, j, i):
            """comparison function that replaces `set_data_value` inside the attributes write function"""
            # test valuest
            val = val.tolist() if type(val) == numpy.ndarray else val
            if j != DIMENSION_TESTS[0]:
                comp = (
                    val.Value
                    == self._get_mock_value(
                        self._get_test_value(j, i.numpy_type).flatten(), i.numpy_type
                    ).Value
                )
                self.assertTrue(
                    comp.all(),
                    "Array attempting to write unequal to expected array: \n\t got: {} \n\texpected: {}".format(
                        val,
                        self._get_mock_value(
                            self._get_test_value(j, i.numpy_type), i.numpy_type
                        ),
                    ),
                )
            else:
                comp = val == self._get_mock_value(
                    self._get_test_value(j, i.numpy_type), i.numpy_type
                )
                self.assertTrue(
                    comp,
                    "value attempting to write unequal to expected value: \n\tgot: {} \n\texpected: {}".format(
                        val,
                        self._get_mock_value(
                            self._get_test_value(j, i.numpy_type), i.numpy_type
                        ),
                    ),
                )

        m_node = mock.Mock()
        m_node.set_data_value.return_value = asyncio.Future()
        m_node.set_data_value.return_value.set_result(None)

        # for all dimensionalities
        for j in DIMENSION_TESTS:
            # for all datatypes
            for i in ATTR_TEST_TYPES:
                # create the protocolattribute
                if len(j) == 1:
                    test = opcua_client.ProtocolAttribute(
                        m_node,
                        asyncio.Lock(),
                        j[0],
                        0,
                        opcua_client.numpy_to_OPCua_dict[i.numpy_type],
                    )
                else:
                    test = opcua_client.ProtocolAttribute(
                        m_node,
                        asyncio.Lock(),
                        j[1],
                        j[0],
                        opcua_client.numpy_to_OPCua_dict[i.numpy_type],
                    )

                # call the write function with the test values
                await test.write_function(self._get_test_value(j, i.numpy_type))

                await compare_values(m_node.call_args, j, i)
