# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from os import path
from unittest import mock
from test import base

import numpy
from pysnmp import hlapi
from pysnmp.smi import view, error
from pysnmp.smi.rfc1902 import ObjectIdentity

from tangostationcontrol.clients.snmp.snmp_client import MIBLoader
from tangostationcontrol.clients.snmp.snmp_client import SNMPClient
from tangostationcontrol.clients.snmp.snmp_client import SNMPComm
from tangostationcontrol.clients.snmp.attribute_classes import (
    SNMPAttribute,
    PCON_sim,
)


class SNMPServerFixture:
    # conversion dict
    SNMP_TO_NUMPY_DICT = {
        hlapi.Integer32: numpy.int64,
        hlapi.TimeTicks: numpy.int64,
        hlapi.OctetString: str,
        hlapi.Counter32: numpy.int64,
        hlapi.Gauge32: numpy.int64,
        hlapi.IpAddress: str,
    }

    # shortcut for testing dimensionality
    DIM_LIST = {
        "scalar": (1, 0),
        "spectrum": (4, 0),
    }

    # object identifier correspondent to sysDescr object in SNMPv2-MIB
    # value -> "1.3.6.1.2.1.1.1.0.1"
    sysDescr_oid = hlapi.ObjectIdentity("SNMPv2-MIB", "sysDescr", 0, 1)

    def get_return_val(self, snmp_type: type, dims: tuple):
        """
        provides the return value for the set/get functions that an actual server would return.
        """

        if dims == self.DIM_LIST["scalar"]:
            read_val = self._get_return_val_for_scalar(snmp_type)
        elif dims == self.DIM_LIST["spectrum"]:
            read_val = self._get_return_val_for_spectrum(snmp_type, dims)
        else:
            raise Exception("Image not supported :(")

        return read_val

    def _get_return_val_for_scalar(self, snmp_type: type):
        if snmp_type is hlapi.ObjectIdentity:
            read_val = [(self.sysDescr_oid,)]
        elif snmp_type is hlapi.IpAddress:
            read_val = [
                (
                    None,
                    snmp_type("1.1.1.1"),
                )
            ]
        elif snmp_type is hlapi.OctetString:
            read_val = [
                (
                    None,
                    snmp_type("1"),
                )
            ]
        else:
            read_val = [
                (
                    None,
                    snmp_type(1),
                )
            ]

        return read_val

    def _get_return_val_for_spectrum(self, snmp_type: type, dims: tuple):
        if snmp_type is hlapi.ObjectIdentity:
            read_val = []
            for _i in range(dims[0]):
                read_val.append((None, self.sysDescr_oid))
        elif snmp_type is hlapi.IpAddress:
            read_val = []
            for _i in range(dims[0]):
                read_val.append((None, snmp_type("1.1.1.1")))
        elif snmp_type is hlapi.OctetString:
            read_val = []
            for _i in range(dims[0]):
                read_val.append((None, snmp_type("1")))
        else:
            read_val = []
            for _i in range(dims[0]):
                read_val.append((None, snmp_type(1)))

        return read_val

    def val_check(self, snmp_type: type, dims: tuple):
        """
        provides the values we expect and would provide to the attribute after converting the
        """

        if dims == self.DIM_LIST["scalar"]:
            snmp_type_dict = {
                hlapi.ObjectIdentity: self.sysDescr_oid,
                hlapi.IpAddress: "1.1.1.1",
                hlapi.OctetString: "1",
            }
            check_val = 1
            for k, v in snmp_type_dict.items():
                if snmp_type is k:
                    check_val = v
        elif dims == self.DIM_LIST["spectrum"]:
            snmp_type_dict = {
                hlapi.ObjectIdentity: [self.sysDescr_oid] * dims[0],
                hlapi.IpAddress: ["1.1.1.1"] * dims[0],
                hlapi.OctetString: ["1"] * dims[0],
            }
            check_val = [1] * dims[0]
            for k, v in snmp_type_dict.items():
                if snmp_type is k:
                    check_val = v
        else:
            raise Exception("Image not yet supported :(")

        return check_val


class TestSNMP(base.TestCase):
    """Test class for SNMP client"""

    def test_annotation_fail(self):
        """
        unit test for the processing of annotation. Has 2 lists.
        1 with things that should succeed and 1 with things that should fail.
        """

        client = SNMPClient(
            community="public",
            host="localhost",
            version=1,
            timeout=10,
            attribute_class=None,
            fault_func=None,
            try_interval=2,
        )

        fail_list = [
            # no 'name'
            {"mib": "random-MIB", "index": 2},
            # no MIB
            {"name": "random-name", "index": 2},
        ]

        for i in fail_list:
            with self.assertRaises(ValueError):
                client._process_annotation(annotation=i)

    @mock.patch("pysnmp.hlapi.ObjectIdentity")
    @mock.patch("pysnmp.hlapi.ObjectType")
    @mock.patch("tangostationcontrol.clients.snmp.snmp_client.SNMPComm.getter")
    def test_snmp_obj_get(self, m_next, m_obj_T, m_obj_i):
        """
        Attempts to read a fake SNMP variable and checks whether it got what it expected
        """

        server = SNMPServerFixture()

        for j in server.DIM_LIST:
            for i in server.SNMP_TO_NUMPY_DICT:
                m_next.return_value = (
                    None,
                    None,
                    None,
                    server.get_return_val(i, server.DIM_LIST[j]),
                )

                def __fakeInit__(self):
                    pass

                with mock.patch.object(SNMPComm, "__init__", __fakeInit__):
                    m_comms = SNMPComm()

                    snmp_attr = SNMPAttribute(
                        comm=m_comms,
                        mib="test",
                        name="test",
                        idx=0,
                        dtype=server.SNMP_TO_NUMPY_DICT[i],
                        dim_x=server.DIM_LIST[j][0],
                        dim_y=server.DIM_LIST[j][1],
                    )

                    val = snmp_attr.read_function()

                    checkval = server.val_check(i, server.DIM_LIST[j])
                    self.assertEqual(
                        checkval,
                        val,
                        f"During test {j} {i}; Expected: {checkval} of type {i}, got: {val} of type {type(val)}",
                    )

    @mock.patch("pysnmp.hlapi.ObjectIdentity")
    @mock.patch("pysnmp.hlapi.ObjectType")
    @mock.patch("pysnmp.hlapi.setCmd")
    @mock.patch("tangostationcontrol.clients.snmp.snmp_client.SNMPComm.setter")
    def test_snmp_obj_set(self, m_next, m_nextCmd, m_obj_T, m_obj_ID):
        """
        Attempts to write a value to an SNMP server, but instead intercepts it
        and compared whether the values is as expected.
        """

        server = SNMPServerFixture()
        obj_type = hlapi.ObjectType

        for j in server.DIM_LIST:
            for i in server.SNMP_TO_NUMPY_DICT:
                # mocks the return value of the next function in snmp_client.SNMPComm.setter
                m_next.return_value = (
                    None,
                    None,
                    None,
                    server.get_return_val(i, server.DIM_LIST[j]),
                )

                def __fakeInit__(self):
                    pass

                with mock.patch.object(SNMPComm, "__init__", __fakeInit__):
                    m_comms = SNMPComm()

                    set_val = server.val_check(i, server.DIM_LIST[j])

                    # create an SNMP attribute object and temporarily
                    hlapi.ObjectType = obj_type
                    snmp_attr = SNMPAttribute(
                        comm=m_comms,
                        mib="test",
                        name="test",
                        idx=0,
                        dtype=server.SNMP_TO_NUMPY_DICT[i],
                        dim_x=server.DIM_LIST[j][0],
                        dim_y=server.DIM_LIST[j][1],
                    )

                    hlapi.ObjectType = mock.MagicMock()

                    hlapi.ObjectType.return_value = (
                        None,
                        None,
                        None,
                        server.get_return_val(i, server.DIM_LIST[j]),
                    )

                    # call the write function. This function should now call m_ObjectType itself.
                    snmp_attr.write_function(set_val)

                    # get a value to compare the value we got against
                    checkval = server.val_check(i, server.DIM_LIST[j])

                    res_lst = [
                        args[1] for args, _ in hlapi.ObjectType.call_args_list if args
                    ]
                    if len(res_lst) == 1:
                        res_lst = res_lst[0]

                    self.assertEqual(checkval, res_lst)

        hlapi.ObjectType = obj_type

    @mock.patch("tangostationcontrol.clients.snmp.snmp_client.SNMPComm.getter")
    def test_named_value(self, m_next):
        m_comms = mock.Mock()
        snmp_attr = SNMPAttribute(
            comm=m_comms, mib="test", name="test", idx=0, dtype=str, dim_x=1, dim_y=0
        )

        # create a named integer with the values: 'enable' for 1 and 'disable' for 0
        test_val = [
            [
                (
                    None,
                    hlapi.Integer.withNamedValues(enable=1, disable=0)(1),
                )
            ]
        ]
        ret_val = snmp_attr.convert(test_val)

        # should return 'enable' since we supplied the value 1
        self.assertEqual(
            ret_val,
            "enable",
            f"Expected: to get 'enable', got: {ret_val} of type {type(ret_val)}",
        )

        # create an unnamed integer with a value of 2
        test_val = [
            [
                (
                    None,
                    hlapi.Integer(2),
                )
            ]
        ]
        ret_val = snmp_attr.convert(test_val)

        # check to make sure the value is indeed 2
        self.assertEqual(
            ret_val, 2, f"Expected: to get {2}, got: {ret_val} of type {type(ret_val)}"
        )


class TestSNMPSimulators(base.TestCase):
    """Test class for SNMP device simulators"""

    def test_simulator(self):
        """Test whether simulator read and write operations work as expected"""
        # read a simple scalar attribute
        pwr_status = PCON_sim(name=PCON_sim.PCON_SCALAR_ATT, idx=1, dim_x=1, dim_y=0)
        self.assertEqual(
            pwr_status.read_function(), 1, "Could not read single value attribute"
        )


class TestMibLoading(base.TestCase):
    """Test class for MIB file load operations"""

    # name and directory of the pymib file
    MIB = "TEST-MIB"

    # mib file is in a folder that is in the same folder as this test
    REL_DIR = "snmp_mib_loading"

    def test_retrieve_mib_content(self):
        """
        This file contains a 1 variable named: testNamedValue with oid "1.3.99.1.99"
        with named values: ("test_name", 1), ("other_name", 2)
        In order to confirm that the mib is indeed loaded correctly this test has to get the oids,
        the values and the named values

        """

        abs_dir = path.dirname(__file__) + "/" + self.REL_DIR + "/"
        loader = MIBLoader(abs_dir)

        loader.load_pymib(self.MIB)

        # used to view mibs client side
        mib_view = view.MibViewController(loader.mibBuilder)

        # The expected testNamedValue parameters as written in TEST-MIB.mib
        testNamedValue_oid = "1.3.99.1.99"
        testNamedValue = "testNamedValue"
        testNamedValue_named = "test_name"
        testNamedValue_value = 1

        # get testValue and set a value of 1
        obj_type = hlapi.ObjectType(
            ObjectIdentity(self.MIB, testNamedValue), hlapi.Integer32(1)
        )
        obj_type.resolveWithMib(mib_view)

        # get the oid
        self.assertEqual(str(obj_type[0]), testNamedValue_oid)

        # get the name format: mib::name
        self.assertEqual(obj_type[0].prettyPrint(), f"{self.MIB}::{testNamedValue}")

        # get the namedValue
        self.assertEqual(str(obj_type[1]), testNamedValue_named)

        # get the numerical value
        self.assertEqual(int(obj_type[1]), testNamedValue_value)

    def test_mib_missing(self):
        """Test whether MIB file missing exception is raised"""
        abs_dir = path.dirname(__file__) + "/" + self.REL_DIR + "/"
        loader = MIBLoader(abs_dir)

        with self.assertRaises(error.MibNotFoundError):
            loader.load_pymib("FAKE-MIB")
