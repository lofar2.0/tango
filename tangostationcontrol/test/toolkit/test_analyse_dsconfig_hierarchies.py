# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from test import base
from tangostationcontrol.toolkit.analyse_dsconfig_hierarchies import DsConfigAnalysis


class TestDsConfigAnalysis(base.TestCase):
    def instances_to_full_config(
        self, instances: dict, hierarchy_child_property: str = "children"
    ) -> DsConfigAnalysis:
        """Take a set of instance specifications and load them into a DsConfigAnalysis object."""

        # surround with boilerplate
        config = {"servers": {"device_server": {"domain": {"family": instances}}}}

        # load config
        dca = DsConfigAnalysis(hierarchy_child_property)
        dca.load_dict(config)

        return dca

    def test_create_instance(self):
        """Test whether we can create an object"""
        _ = DsConfigAnalysis()

    def test_simple_configuration(self):
        """Test whether we can parse a simple configuration"""
        instances = {"instance": {"properties": {"children": ["foo"]}}}

        # load config
        dca = self.instances_to_full_config(instances)

        # parse children
        self.assertDictEqual({"instance": ["foo"]}, dca.device_children())
        # find root
        self.assertListEqual(["instance"], dca.root_devices())

    def test_correct_config(self):
        """Test whether a correct config detects no issues."""
        instances = {
            "instance1": {"properties": {"children": []}},
            "instance2": {"properties": {"children": ["instance1"]}},
            "instance3": {"properties": {"children": ["instance2"]}},
        }

        # load config
        dca = self.instances_to_full_config(instances)

        # parse
        self.assertListEqual([], dca.loops())
        self.assertListEqual([], dca.devices_with_multiple_parents())
        self.assertDictEqual({}, dca.devices_with_unknown_children())

    def test_self_loop(self):
        """Test whether a loop of size 1 is detected correctly."""
        instances = {"instance": {"properties": {"children": ["instance"]}}}

        # load config
        dca = self.instances_to_full_config(instances)

        # parse
        self.assertListEqual([["instance"]], dca.loops())

    def test_multi_loop(self):
        """Test whether a loop of size 2 is detected correctly."""
        instances = {
            "instance1": {"properties": {"children": ["instance2"]}},
            "instance2": {"properties": {"children": ["instance1"]}},
        }

        # load config
        dca = self.instances_to_full_config(instances)

        # parse
        self.assertListEqual([["instance1"], ["instance2"]], dca.loops())

    def test_multiple_parents(self):
        """Test whether multiple parents are detected correctly."""
        instances = {
            "instance1": {"properties": {"children": []}},
            "instance2": {"properties": {"children": ["instance1"]}},
            "instance3": {"properties": {"children": ["instance1"]}},
        }

        # load config
        dca = self.instances_to_full_config(instances)

        # parse
        self.assertListEqual(["instance1"], dca.devices_with_multiple_parents())

    def test_unknown_children(self):
        """Test whether unknown children are detected correctly."""
        instances = {
            "instance1": {"properties": {"children": ["instance2", "instance3"]}}
        }

        # load config
        dca = self.instances_to_full_config(instances)

        # parse
        self.assertDictEqual(
            {"instance1": ["instance2", "instance3"]},
            dca.devices_with_unknown_children(),
        )
