import asyncio
import asyncua
import time

from tangostationcontrol.asyncio import EventLoopThread


async def make_opcua_server(port=12345, namespace="namespace") -> asyncua.Server:
    """Start an OPC/UA server"""

    server = asyncua.Server()
    await server.init()
    server.set_endpoint(f"opc.tcp://0.0.0.0:{port}/freeopcua/server/")
    server.set_server_name("Test OPC/UA server")
    server.set_security_policy(
        [
            asyncua.ua.SecurityPolicyType.NoSecurity,
            asyncua.ua.SecurityPolicyType.Basic256Sha256_SignAndEncrypt,
            asyncua.ua.SecurityPolicyType.Basic256Sha256_Sign,
        ]
    )
    namespace_idx = await server.register_namespace(namespace)
    server_interfaces = await server.nodes.objects.add_folder(
        namespace_idx, "ServerInterfaces"
    )
    obj = await server_interfaces.add_object(namespace_idx, "TestDevice")

    await obj.add_variable(namespace_idx, "float_R", 20.0)

    return server


class RunOPCUAServer:
    """Run an OPCUA Server in the background."""

    def __init__(self, port, namespace):
        self.port = port
        self.namespace = namespace
        self.running = False
        self.stopping = False

        self.event_loop_thread = EventLoopThread("OPCUA Server")
        self.future = None

    async def run_server(self):
        async with await make_opcua_server(self.port, self.namespace) as opcua_server:
            self.running = True
            while not self.stopping:
                await asyncio.sleep(0.1)

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

    def start(self):
        self.future = self.event_loop_thread.run_coroutine_threadsafe(self.run_server())

        while not self.running:
            time.sleep(0.1)

    def stop(self):
        self.stopping = True
        _ = self.future.result()

        self.event_loop_thread.stop()
