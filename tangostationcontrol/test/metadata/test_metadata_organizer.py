# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime, timezone
import json
import logging
from unittest import mock
from unittest.mock import patch

import numpy
from tango import DevFailed

from lofar_station_client.common import CaseInsensitiveDict

from tangostationcontrol.common.lofar_logging import exception_to_str
from tangostationcontrol.common.proxies import create_proxies
from tangostationcontrol.metadata import metadata_organizer

from test import base

logger = logging.getLogger()


@patch("tango.Database")
class TestMetadataOrganizer(base.TestCase):

    def setUp(self):
        super(TestMetadataOrganizer, self).setUp()

        proxy_patcher = self.proxy_patch(
            create_proxies, "create_device_proxy", autospec=True
        )
        self.m_proxy = proxy_patcher["mock"]

    def test_create_proxies(self, tango_db):
        """Test proxy creation and storage access"""

        tango_db.return_value.get_device_exported_for_class.return_value = [
            "STAT/device/1",
            "STAT/Device/2",
        ]

        t_organizer = metadata_organizer.MetadataOrganizer(
            CaseInsensitiveDict({"Device": []})
        )
        t_organizer.create_proxies()

        self.assertEqual(2, self.m_proxy.call_count)
        self.assertIn("STAT/DEVICE/1", t_organizer._proxies.keys())
        self.assertIn("STAT/DEVICE/2", t_organizer._proxies.keys())

    def test_create_proxy_exception(self, tango_db):
        """Test creating the proxies but some fail"""

        tango_db.return_value.get_device_exported_for_class.return_value = [
            "STAT/device/1",
            "STAT/Device/2",
        ]

        self.m_proxy.side_effect = [mock.Mock(), DevFailed()]

        t_organizer = metadata_organizer.MetadataOrganizer(
            CaseInsensitiveDict({"Device": []})
        )
        t_organizer.create_proxies()

        self.assertEqual(2, self.m_proxy.call_count)
        self.assertIn("STAT/DEVICE/1", t_organizer._proxies.keys())
        self.assertTrue(isinstance(t_organizer._proxies["STAT/DEVICE/2"], DevFailed))

    def test_gather_metadata(self, tango_db):
        """Test gathering different types of data"""
        tango_db.return_value.get_device_exported_for_class.side_effect = lambda dc: (
            ["stat/DEVICE/1"] if dc.casefold() == "device1" else ["STAT/device/2"]
        )

        device_1_attributes = {
            "antenna_names_R": ["antenna_1", "antenna_2"],
            "observation_id_R": 125434612735345,
        }
        device_2_attributes = {
            "subband_weights_R": numpy.ndarray(shape=(2, 2), dtype=float)
        }

        m_proxy_1 = mock.Mock(**device_1_attributes)

        m_proxy_2 = mock.Mock(**device_2_attributes)

        self.m_proxy.side_effect = [m_proxy_1, m_proxy_2]

        t_organizer = metadata_organizer.MetadataOrganizer(
            CaseInsensitiveDict(
                {
                    "device1": ["antenna_names_R", "observation_id_R"],
                    "Device2": ["subband_weights_R"],
                }
            )
        )
        t_organizer.create_proxies()

        t_organizer.gather_metadata()

        self.assertIn("stat/device/1", t_organizer._data.keys())
        self.assertIn("stat/DEVICE/2", t_organizer._data.keys())

        self.assertEqual(
            t_organizer._data["stat/device/1"]["antenna_names_R"],
            device_1_attributes["antenna_names_R"],
        )
        self.assertEqual(
            t_organizer._data["stat/device/1"]["observation_id_R"],
            device_1_attributes["observation_id_R"],
        )

        numpy.testing.assert_almost_equal(
            t_organizer._data["stat/device/2"]["subband_weights_R"],
            device_2_attributes["subband_weights_R"].tolist(),
        )

    def test_gather_metadata_exceptions(self, tango_db):
        """Failed proxies and conversion errors should put exceptions into data"""

        tango_db.return_value.get_device_exported_for_class.return_value = [
            "STAT/device/1",
            "STAT/device/2",
        ]
        m_except = DevFailed()

        device_1_attributes = {"antenna_names_R": CaseInsensitiveDict()}

        self.m_proxy.side_effect = [mock.Mock(**device_1_attributes), m_except]

        t_organizer = metadata_organizer.MetadataOrganizer(
            CaseInsensitiveDict({"Device1": ["antenna_names_R"]})
        )
        t_organizer.create_proxies()

        t_organizer.gather_metadata()

        self.assertIn("stat/device/1", t_organizer._data.keys())
        self.assertIn("stat/DEVICE/2", t_organizer._data.keys())
        self.assertEqual(t_organizer._data["stat/device/2"], exception_to_str(m_except))
        self.assertIn(
            "Failed to convert object",
            t_organizer._data["stat/device/1"]["antenna_names_R"],
        )

    def test_partial_update(self, tango_db):
        """Update several attributes in isolation"""

        t_organizer = metadata_organizer.MetadataOrganizer(
            CaseInsensitiveDict({"Device": []})
        )

        m_proxy = mock.Mock()
        m_proxy.name.return_value = "stat/DEVICE/1"

        t_organizer.partial_update(
            m_proxy, "antenna_names_R", ["antenna_1", "antenna_2"]
        )

        self.assertEqual(
            CaseInsensitiveDict({"antenna_names_R": ["antenna_1", "antenna_2"]}),
            t_organizer._data["stat/device/1"],
        )

        t_organizer.partial_update(
            m_proxy, "antenna_names_R", ["antenna_3", "antenna_4"]
        )

        self.assertEqual(
            CaseInsensitiveDict({"antenna_names_R": ["antenna_3", "antenna_4"]}),
            t_organizer._data["stat/device/1"],
        )

    def test_get_json(self, _):
        """Test converting variety of data products to json"""

        t_organizer = metadata_organizer.MetadataOrganizer(
            config=CaseInsensitiveDict({})
        )
        t_organizer._data["stat/device/1"] = {}
        t_organizer._data["stat/device/1"]["antenna_names_R"] = [
            "antenna_1",
            "antenna_2",
        ]
        t_organizer._data["stat/device/1"]["subband_weights_R"] = numpy.ndarray(
            shape=(2, 2), dtype=float
        ).tolist()
        t_organizer._data["stat/device/1"]["observation_id_R"] = 125434612735345

        t_result = t_organizer.get_json()

        # parse & validate json
        t_result_dict = json.loads(t_result)

        self.assertIn("stat/device/1", t_result_dict)
        self.assertIn("antenna_names_R", t_result_dict["stat/device/1"])
        self.assertIn("antenna_1", t_result_dict["stat/device/1"]["antenna_names_R"])
        self.assertIn("subband_weights_R", t_result_dict["stat/device/1"])
        self.assertIn("observation_id_R", t_result_dict["stat/device/1"])
        self.assertEqual(
            125434612735345, t_result_dict["stat/device/1"]["observation_id_R"]
        )

        # validate timestamp format
        self.assertIn("ts", t_result_dict)
        timestamp = datetime.fromisoformat(t_result_dict["ts"])
        self.assertEqual(timezone.utc, timestamp.tzinfo)

    def test_end_to_end(self, tango_db):
        """Test the complete chain of functionality"""

        tango_db.return_value.get_device_exported_for_class.side_effect = lambda dc: (
            ["stat/DEVICE/1"]
            if dc.casefold() == "device1"
            else (
                ["STAT/device/2"] if dc.casefold() == "device2" else ["STAT/device/3"]
            )
        )

        m_except = DevFailed()

        device_1_attributes = {
            "antenna_names_R": ["antenna_1", "antenna_2"],
            "observation_id_R": 125434612735345,
        }
        device_2_attributes = {
            "subband_weights_R": numpy.ndarray(shape=(2, 2), dtype=float),
            "unconvertable_type_R": CaseInsensitiveDict(),
        }

        m_proxy_1 = mock.Mock(**device_1_attributes)

        m_proxy_2 = mock.Mock(**device_2_attributes)

        self.m_proxy.side_effect = [m_proxy_1, m_proxy_2, m_except]

        t_organizer = metadata_organizer.MetadataOrganizer(
            CaseInsensitiveDict(
                {
                    "device1": ["antenna_names_R", "observation_id_R"],
                    "device2": ["subband_weights_R", "unconvertable_type_R"],
                    "device3": [],
                }
            )
        )
        t_organizer.create_proxies()

        t_organizer.gather_metadata()

        self.assertEqual(
            t_organizer._data["stat/device/1"]["antenna_names_R"],
            device_1_attributes["antenna_names_R"],
        )

        numpy.testing.assert_almost_equal(
            t_organizer._data["stat/device/2"]["subband_weights_R"],
            device_2_attributes["subband_weights_R"].tolist(),
        )

        self.assertEqual(t_organizer._data["stat/device/3"], exception_to_str(m_except))
        self.assertIn(
            "Failed to convert object",
            t_organizer._data["stat/device/2"]["unconvertable_type_R"],
        )

        m_proxy = mock.Mock()
        m_proxy.name.return_value = "stat/DEVICE/1"
        t_organizer.partial_update(
            m_proxy, "antenna_names_R", ["antenna_3", "antenna_4"]
        )

        self.assertEqual(
            CaseInsensitiveDict(
                {
                    "antenna_names_R": ["antenna_3", "antenna_4"],
                    "observation_id_R": 125434612735345,
                }
            ),
            t_organizer._data["stat/device/1"],
        )

        t_result = t_organizer.get_json()

        self.assertIn("stat/device/1", t_result)
        self.assertIn("antenna_names_r", t_result)
        self.assertIn("subband_weights_r", t_result)
        self.assertIn("observation_id_r", t_result)
        self.assertIn("antenna_3", t_result)
        self.assertIn("125434612735345", t_result)
