# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
import datetime
import json
import time

import numpy
import timeout_decorator

from integration_test.device_proxy import TestDeviceProxy
from integration_test.default.devices.base import TestDeviceBase

from tangostationcontrol.common.constants import (
    CS001_TILES,
    MAX_ANTENNA,
    N_elements,
    N_pol,
)
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AntennaStatus,
    AntennaUse,
)
from tangostationcontrol.common.env_decorators import (
    ensure_device_boots,
)

logger = logging.getLogger()


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class TileBeamDeviceTests(TestDeviceBase):
    """Integration test class for device Tilebeam"""

    __test__ = False

    longMessage = True

    # HBA0 / HBA1
    LOFAR_REF_POINTING = [24, 25, 27, 29, 17, 18, 20, 21, 10, 11, 13, 14, 3, 4, 5, 7]
    LOFAR_REF_REPEATS = 2

    def setUp(
        self,
        tile_number: int = CS001_TILES,
        tilebeam_name: str = None,
        antennafield_name: str = None,
        sdp_name: str = None,
        sdpfirmware_name: str = None,
        recv_name: str = None,
    ):
        """Setup Tilebeam"""

        if (
            tilebeam_name is None
            or antennafield_name is None
            or sdp_name is None
            or sdpfirmware_name is None
            or recv_name is None
        ):
            raise RuntimeError("Missing required device names to initialize tests")

        self.pointing_direction = numpy.array(
            [["J2000", "0rad", "0rad"]] * tile_number
        ).flatten()

        super().setUp(tilebeam_name)

        self.tiles = tile_number

        self.station_manager_proxy = self.setup_proxy("STAT/StationManager/1")
        self.recv_proxy = self.setup_proxy(recv_name, defaults=True)
        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name, defaults=True)
        self.sdp_proxy = self.setup_proxy(sdp_name, defaults=True)
        self.antennafield_proxy = self.setup_proxy(
            antennafield_name,
            cb=self.setup_antennafield_property,
            restore_properties=True,
        )

        # check if AntennaField really exposes the expected number of tiles
        self.assertEqual(self.tiles, self.antennafield_proxy.nr_antennas_R)

    def setup_antennafield_property(self, proxy: TestDeviceProxy):
        """Setup AntennaField"""
        control_mapping = [[1, i] for i in range(self.tiles)]
        antenna_status = numpy.array([AntennaStatus.OK] * MAX_ANTENNA)
        antenna_use = numpy.array([AntennaUse.AUTO] * MAX_ANTENNA)
        proxy.put_property(
            {
                "Control_to_RECV_mapping": numpy.array(control_mapping).flatten(),
                "Antenna_Status": antenna_status,
                "Antenna_Use": antenna_use,
            }
        )

        # check if AntennaField really exposes the expected number of tiles
        self.assertEqual(self.tiles, proxy.nr_antennas_R)

    @ensure_device_boots()
    def test_delays_dims(self):
        """Verify delays are retrieved with correct dimensions"""
        delays = self.proxy.delays(self.pointing_direction)
        self.assertEqual(self.tiles * N_elements, len(delays))

    @ensure_device_boots()
    def test_set_pointing(self):
        """Verify if set pointing procedure is correctly executed"""
        # setup BEAM
        self.proxy.Tracking_enabled_RW = False

        # Verify attribute is present (all zeros if never used before)
        delays_r1 = numpy.array(
            self.antennafield_proxy.read_attribute("HBAT_BF_delay_steps_stage_RW").value
        )

        self.assertIsNotNone(delays_r1)

        time.sleep(3)

        # Verify writing operation does not lead to errors
        self.proxy.set_pointing(self.pointing_direction)  # write values to RECV
        delays_r2 = numpy.array(
            self.antennafield_proxy.read_attribute("HBAT_BF_delay_steps_stage_RW").value
        )

        self.assertIsNotNone(delays_r2)

        # Verify delays changed (to be discussed)
        # self.assertFalse((delays_r1==delays_r2).all())

    @ensure_device_boots()
    def test_pointing_to_zenith(self):
        self.proxy.Tracking_enabled_RW = False

        # Point to Zenith
        self.proxy.set_pointing(
            numpy.array([["AZELGEO", "0rad", "1.570796rad"]] * self.tiles).flatten()
        )

        calculated_HBAT_delay_steps = numpy.array(
            self.antennafield_proxy.read_attribute("HBAT_BF_delay_steps_stage_RW").value
        )

        expected_HBAT_delay_steps = numpy.array(
            [[15] * N_elements * N_pol] * self.tiles, dtype=numpy.int64
        )

        # Accept values of 14, 15 and 16
        numpy.testing.assert_allclose(
            calculated_HBAT_delay_steps, expected_HBAT_delay_steps, rtol=0.066, atol=1
        )

    @ensure_device_boots()
    def test_pointing_across_horizon(self):
        antennafield_proxy = self.antennafield_proxy

        self.proxy.Tracking_enabled_RW = False

        # point at north on the horizon
        self.proxy.set_pointing(["AZELGEO", "0rad", "0rad"] * self.tiles)

        # obtain delays of the X polarisation of all the elements of the first tile
        north_beam_delay_steps = antennafield_proxy.HBAT_BF_delay_steps_stage_RW[
            0
        ].reshape(4, 4, 2)[:, :, 0]

        # delays must differ under rotation, or our test will give a false positive
        self.assertNotEqual(
            north_beam_delay_steps.tolist(),
            numpy.rot90(north_beam_delay_steps).tolist(),
        )
        # 90, 180 and 270 degrees
        for angle in (1.570796, 3.141593, 4.712389):
            # point at angle degrees (90=E, 180=S, 270=W)
            self.proxy.set_pointing(["AZELGEO", f"{angle}rad", "0rad"] * self.tiles)

            # obtain delays of the X polarisation of all the elements of the first tile
            angled_beam_delay_steps = antennafield_proxy.HBAT_BF_delay_steps_stage_RW[
                0
            ].reshape(4, 4, 2)[:, :, 0]

            expected_delay_steps = numpy.rot90(
                north_beam_delay_steps, k=-(int((angle * 180) / numpy.pi) / 90)
            )

            self.assertListEqual(
                expected_delay_steps.tolist(),
                angled_beam_delay_steps.tolist(),
                msg=f"angle={angle}",
            )

    @ensure_device_boots()
    def test_delays_same_as_LOFAR_ref_pointing(self):
        self.proxy.Tracking_enabled_RW = False

        # Point to LOFAR 1 ref pointing (0.929342, 0.952579, J2000)
        pointings = numpy.array(
            [["J2000", "0.929342rad", "0.952579rad"]] * self.tiles
        ).flatten()
        # Need to set the time to '2022-01-18 11:19:35'
        timestamp = datetime.datetime(2022, 1, 18, 11, 19, 35).timestamp()

        parameters = {"pointing_direction": pointings, "timestamp": timestamp}

        json_string = json.dumps(parameters, cls=NumpyEncoder)
        self.proxy.set_pointing_for_specific_time(json_string)

        calculated_HBAT_delay_steps = numpy.array(
            self.antennafield_proxy.read_attribute("HBAT_BF_delay_steps_stage_RW").value
        )  # dims (self.tiles, 32)

        # Check all delay steps are zero with small margin
        # [24, 25, 27, 28, 17, 18, 20, 21, 10, 11, 13, 14, 3, 4, 5, 7] These are the real values from LOFAR.
        # The [3] = 28 diff is explained that we match the closest delay step and LOFAR 1 wants the one with
        # in 0.2ns but if it can't it will do a int(delay / 0.5ns) so we get slightly different results but
        # they can be explained.
        expected_HBAT_delay_steps = numpy.repeat(
            numpy.array(
                self.LOFAR_REF_POINTING,
                dtype=numpy.int64,
            ),
            repeats=self.LOFAR_REF_REPEATS,
        )
        numpy.testing.assert_equal(
            calculated_HBAT_delay_steps[0], expected_HBAT_delay_steps
        )
        numpy.testing.assert_equal(
            calculated_HBAT_delay_steps[self.tiles - 1],
            expected_HBAT_delay_steps,
        )

    @ensure_device_boots()
    def test_tilebeam_tracking(self):
        # check if we're really tracking
        self.assertTrue(self.proxy.Tracking_enabled_R)

        # point somewhere
        new_pointings = [("J2000", f"{tile}rad", "0rad") for tile in range(self.tiles)]
        self.proxy.Pointing_direction_RW = new_pointings

        # check pointing
        self.assertListEqual(
            new_pointings[0:2], list(self.proxy.Pointing_direction_R[0:2])
        )

    @timeout_decorator.timeout(120)
    def test_beam_tracking_95_percent_interval(self):
        """Verify that the beam tracking operates within 95% of interval"""

        self.proxy.initialise()
        self.proxy.Tracking_enabled_RW = True
        self.proxy.on()

        interval = float(
            self.proxy.get_property("Beam_tracking_interval")["Beam_tracking_interval"][
                0
            ]
        )

        # Allow beam tracking time to settle
        time.sleep(interval * 3)

        # We have to poll at regular interval due to not working subscribe
        # events
        for _ in range(0, 5):
            error = self.proxy.Pointing_error_R[0]
            self.assertTrue(
                -interval * 0.05 < error < interval * 0.05,
                f"Error: {error} larger than {interval * 0.05}",
            )
            logger.info("BeamTracking error: %s", error)
            time.sleep(interval)
