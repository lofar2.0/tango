# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase


class SDPDeviceTests(TestDeviceBase):
    __test__ = False

    def setUp(self, sdp_name: str = None, sdpfirmware_name: str = None):
        """Intentionally recreate the device object in each test"""

        if sdp_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        super().setUp(sdp_name)

        self.setup_proxy(sdpfirmware_name)
