# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase


class BSTDeviceTests(TestDeviceBase):
    __test__ = False

    def setUp(
        self, bst_name: str = None, sdpfirmware_name: str = None, sdp_name: str = None
    ):
        if bst_name is None or sdp_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        self.sdpfirmware_name = sdpfirmware_name
        self.sdp_name = sdp_name

        """Intentionally recreate the device object in each test"""
        super().setUp(bst_name)

    def test_device_read_all_attributes(self):
        # We need to connect to SDP first to read some of our attributes
        self.setup_proxy(self.sdpfirmware_name, defaults=True)
        self.setup_proxy(self.sdp_name, defaults=True)

        super().test_device_read_all_attributes()
