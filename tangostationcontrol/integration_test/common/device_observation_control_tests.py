# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import json
import logging
import time
from datetime import datetime
from datetime import timedelta

import numpy
from integration_test.default.devices.base import TestDeviceBase
from tango import DevFailed, DevState

from tangostationcontrol.common.constants import CS001_TILES
from timeout_decorator import timeout_decorator

logger = logging.getLogger()


class DeviceObservationControlTests(TestDeviceBase):
    __test__ = False

    def setUp(
        self,
        tile_number: int = CS001_TILES,
        hba_immediate_json: str = None,
        antennafield_name: str = None,
        recv_name: str = None,
        sdp_name: str = None,
        sdpfirmware_name: str = None,
        beamlet_name: str = None,
        digitalbeam_name: str = None,
        tilebeam_name: str = None,
        sst_name: str = None,
        xst_name: str = None,
        bst_name: str = None,
    ):
        super().setUp("STAT/ObservationControl/1")

        if hba_immediate_json is None:
            raise RuntimeError("JSON specification for immediate observation required")

        self.valid_json = hba_immediate_json
        self.expected_obs_id = json.loads(self.valid_json)["antenna_fields"][0][
            "observation_id"
        ]
        self.expected_antenna_field = json.loads(self.valid_json)["antenna_fields"][0][
            "antenna_field"
        ]

        if (
            antennafield_name is None
            or recv_name is None
            or sdp_name is None
            or sdpfirmware_name is None
            or beamlet_name is None
            or digitalbeam_name is None
            or tilebeam_name is None
            or sst_name is None
            or xst_name is None
            or bst_name is None
        ):
            raise RuntimeError("Missing required device names to initialize tests")

        self.recv_proxy = self.setup_proxy(recv_name, defaults=True)
        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name)
        self.sdp_proxy = self.setup_proxy(sdp_name)

        self.metadata_proxy = self.setup_proxy("STAT/Metadata/1")

        control_mapping = [[1, i] for i in range(tile_number)]
        sdp_mapping = [[i // 6, i % 6] for i in range(tile_number)]
        self.antennafield_proxy = self.setup_proxy(
            antennafield_name,
            defaults=True,
            restore_properties=True,
            cb=lambda x: {
                x.put_property(
                    {
                        "Antenna_Set": "ALL",
                        "Power_to_RECV_mapping": numpy.array(control_mapping).flatten(),
                        "Antenna_to_SDP_Mapping": numpy.array(sdp_mapping).flatten(),
                    }
                )
            },
        )

        self.beamlet_proxy = self.setup_proxy(beamlet_name, defaults=True)
        self.digitalbeam_proxy = self.setup_proxy(digitalbeam_name, defaults=True)
        self.tilebeam_proxy = self.setup_proxy(tilebeam_name, defaults=True)
        self.sst_proxy = self.setup_proxy(sst_name, defaults=True)
        self.xst_proxy = self.setup_proxy(xst_name, defaults=True)
        self.bst_proxy = self.setup_proxy(bst_name, defaults=True)

    def on_device_assert(self, proxy):
        """Transition the device to ON and assert intermediate states

        This will repeatedly call ``stop_all_observations_now`` in turn calling
        ``_destroy_all_observation_field_devices`` cleaning the Database from exported
        devices
        """

        proxy.Off()
        self.assertEqual(DevState.OFF, proxy.state())
        proxy.Initialise()
        self.assertEqual(DevState.STANDBY, proxy.state())
        proxy.On()
        self.assertEqual(DevState.ON, proxy.state())

    def test_device_on(self):
        """Transition the ObservationControl device to ON state"""
        self.on_device_assert(self.proxy)

    def test_no_observation_running(self):
        """Assert no current observations on fresh boot"""

        self.on_device_assert(self.proxy)
        self.assertFalse(self.proxy.is_any_observation_running())
        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))
        self.assertFalse(self.proxy.is_observation_running(54321))

    def test_add_observation_now(self):
        """Test starting an observation now"""

        self.on_device_assert(self.proxy)

        # Integration test JSON has no start time so will start immediately even when
        # using `add_observation`
        self.proxy.add_observation(self.valid_json)

        self.assertTrue(self.proxy.is_any_observation_running())
        self.assertTrue(self.proxy.is_observation_running(self.expected_obs_id))
        self.assertTrue(self.proxy.is_antenna_field_active(self.expected_antenna_field))

        self.proxy.stop_observation_now(self.expected_obs_id)

        self.assertFalse(self.proxy.is_any_observation_running())
        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))
        self.assertFalse(
            self.proxy.is_antenna_field_active(self.expected_antenna_field)
        )

    def test_add_observation_future(self):
        """Test starting an observation in the future"""

        self.on_device_assert(self.proxy)

        parameters = json.loads(self.valid_json)
        for antenna_field in parameters["antenna_fields"]:
            antenna_field["start_time"] = (
                datetime.now() + timedelta(days=1)
            ).isoformat()
            antenna_field["stop_time"] = (
                datetime.now() + timedelta(days=2)
            ).isoformat()

        self.proxy.add_observation(json.dumps(parameters))

        self.assertIn(self.expected_obs_id, self.proxy.observations_R)
        self.assertNotIn(self.expected_obs_id, self.proxy.running_observations_R)
        self.assertFalse(self.proxy.is_any_observation_running())
        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))

        self.proxy.stop_observation_now(self.expected_obs_id)

    def test_add_observation_multiple(self):
        """Test starting multiple observations"""

        second_observation_json = json.loads(self.valid_json)
        second_observation_json["antenna_fields"][0]["observation_id"] = 54321

        self.on_device_assert(self.proxy)

        self.proxy.add_observation(self.valid_json)
        self.proxy.add_observation(json.dumps(second_observation_json))

        self.assertTrue(self.proxy.is_any_observation_running())
        self.assertTrue(self.proxy.is_observation_running(self.expected_obs_id))
        self.assertTrue(self.proxy.is_observation_running(54321))

        self.proxy.stop_observation_now(self.expected_obs_id)
        self.proxy.stop_observation_now(54321)

    def test_stop_observation_invalid_id(self):
        """Test stop_observation exceptions for invalid ids"""

        self.on_device_assert(self.proxy)

        self.assertRaises(DevFailed, self.proxy.stop_observation_now, -1)

    def test_stop_observation_invalid_running(self):
        """Test stop_observation exceptions for not running"""

        self.on_device_assert(self.proxy)

        self.assertRaises(DevFailed, self.proxy.stop_observation_now, 2)

    def test_is_any_observation_running_after_stop_all_observations_now(self):
        """Test whether is_any_observation_running conforms when we start & stop an observation"""

        self.on_device_assert(self.proxy)

        self.proxy.add_observation(self.valid_json)
        self.proxy.stop_all_observations_now()

        # Test false
        self.assertFalse(self.proxy.is_any_observation_running())

    def test_start_stop_observation_now(self):
        """Test starting and stopping an observation"""

        self.on_device_assert(self.proxy)

        # uses ID 12345
        self.proxy.add_observation(self.valid_json)
        self.proxy.stop_observation_now(self.expected_obs_id)

        # Test false
        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))

    def test_start_multi_stop_all_observation(self):
        """Test starting and stopping multiple observations"""

        second_observation_json = json.loads(self.valid_json)
        second_observation_json["antenna_fields"][0]["observation_id"] = 54321

        self.on_device_assert(self.proxy)

        # uses ID 5
        self.proxy.add_observation(self.valid_json)
        self.proxy.add_observation(json.dumps(second_observation_json))
        self.proxy.stop_all_observations_now()

        # Test false
        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))
        self.assertFalse(self.proxy.is_observation_running(54321))

    def test_check_and_convert_parameters_invalid_id(self):
        """Test invalid parameter detection"""

        parameters = json.loads(self.valid_json)
        for station in parameters["antenna_fields"]:
            station["observation_id"] = -1

        self.on_device_assert(self.proxy)
        self.assertRaises(DevFailed, self.proxy.add_observation, json.dumps(parameters))

    def test_check_and_convert_parameters_invalid_empty(self):
        """Test empty parameter detection"""

        self.on_device_assert(self.proxy)
        self.assertRaises(DevFailed, self.proxy.add_observation, "{}")

    def test_check_and_convert_parameters_invalid_antenna_set(self):
        """Test invalid antenna set name"""
        parameters = json.loads(self.valid_json)
        for station in parameters["antenna_fields"]:
            station["antenna_set"] = "ZZZ"
        self.on_device_assert(self.proxy)
        self.assertRaises(DevFailed, self.proxy.add_observation, json.dumps(parameters))

    def test_check_and_convert_parameters_invalid_time(self):
        """Test invalid parameter detection"""

        parameters = json.loads(self.valid_json)
        for antenna_field in parameters["antenna_fields"]:
            antenna_field["stop_time"] = (
                datetime.now() - timedelta(seconds=1)
            ).isoformat()

        self.on_device_assert(self.proxy)
        self.assertRaises(DevFailed, self.proxy.add_observation, json.dumps(parameters))

    @timeout_decorator.timeout(60)
    def test_add_observation_callbacks(self):
        """Test adding an observation and checking start / stop callbacks work"""

        self.on_device_assert(self.proxy)

        parameters = json.loads(self.valid_json)
        for antenna_field in parameters["antenna_fields"]:
            antenna_field["start_time"] = (
                datetime.now() + timedelta(seconds=5)
            ).isoformat()
            antenna_field["stop_time"] = (
                datetime.now() + timedelta(seconds=20)
            ).isoformat()

        self.proxy.add_observation(json.dumps(parameters))

        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))

        time.sleep(5)

        while not self.proxy.is_observation_running(self.expected_obs_id):
            logging.info("Waiting for observation to start...")
            time.sleep(0.1)

        self.assertTrue(self.proxy.is_observation_running(self.expected_obs_id))
        self.assertGreater(self.metadata_proxy.messages_published_R, 0)

        time.sleep(20)

        while self.proxy.is_observation_running(self.expected_obs_id):
            logging.info("Waiting for observation to stop...")
            time.sleep(0.1)

        self.assertFalse(self.proxy.is_observation_running(self.expected_obs_id))
