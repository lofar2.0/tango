#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging
import time

import numpy
import timeout_decorator

from integration_test.device_proxy import TestDeviceProxy
from integration_test.default.devices.base import TestDeviceBase
from tangostationcontrol.common.constants import (
    MAX_ANTENNA,
    N_beamlets_ctrl,
    N_pn,
    A_pn,
    CLK_200_MHZ,
    CLK_160_MHZ,
    CS001_TILES,
)
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AntennaStatus,
    AntennaUse,
)

logger = logging.getLogger()

# tests will break in weird ways if our constants change beyond these constraints
assert CS001_TILES >= 2
assert MAX_ANTENNA >= 2


class DigitalBeamDeviceTests(TestDeviceBase):
    __test__ = False

    antenna_status_ok = numpy.array([AntennaStatus.OK] * MAX_ANTENNA)
    antenna_status_only_second = numpy.array(
        [AntennaStatus.BROKEN]
        + [AntennaStatus.OK]
        + [AntennaStatus.BROKEN] * (MAX_ANTENNA - 2)
    )
    antenna_use_ok = numpy.array([AntennaUse.AUTO] * MAX_ANTENNA)

    def setUp(
        self,
        tile_number: int = CS001_TILES,
        digitalbeam_name: str = None,
        antennafield_name: str = None,
        beamlet_name: str = None,
        sdp_name: str = None,
        sdpfirmware_name: str = None,
        recv_name: str = None,
    ):
        """Intentionally recreate the device object in each test"""

        if (
            digitalbeam_name is None
            or antennafield_name is None
            or beamlet_name is None
            or sdp_name is None
            or sdpfirmware_name is None
            or recv_name is None
        ):
            raise RuntimeError("Missing required device names to initialize tests")

        super().setUp(digitalbeam_name)

        self.tile_number = tile_number

        self.recv_proxy = self.setup_proxy(recv_name, defaults=True)
        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name, defaults=True)
        self.sdp_proxy = self.setup_proxy(sdp_name)
        self.beamlet_proxy = self.initialise_beamlet_proxy(beamlet_name)

        control_mapping = [[1, i] for i in range(tile_number)]
        sdp_mapping = [[i // 6, i % 6] for i in range(tile_number)]
        self.antennafield_proxy = self.setup_proxy(
            antennafield_name,
            cb=lambda x: {
                x.put_property(
                    {
                        "Control_to_RECV_mapping": numpy.array(
                            control_mapping
                        ).flatten(),
                        "Antenna_to_SDP_Mapping": numpy.array(sdp_mapping).flatten(),
                        "Antenna_Status": self.antenna_status_ok,
                        "Antenna_Use": self.antenna_use_ok,
                        "Antenna_Cables": ["50m", "80m"] * (tile_number // 2),
                        "Antenna_Sets": ["FIRST", "ALL"],
                        "Antenna_Set_Masks": [
                            "1" + ("0" * (tile_number - 1)),
                            "1" * tile_number,
                        ],
                        "Antenna_Type": "HBA",
                    }
                )
            },
        )

        self.addCleanup(TestDeviceProxy.test_device_turn_off, beamlet_name)

    def initialise_beamlet_proxy(self, name: str = None):
        if not name and self.beamlet_proxy:
            beamlet_proxy = self.beamlet_proxy
        else:
            beamlet_proxy = TestDeviceProxy(name)

        beamlet_proxy.off()
        beamlet_proxy.initialise()
        return beamlet_proxy

    def test_pointing_to_zenith_clock_change(self):
        self.beamlet_proxy.on()

        # Set first (default) clock configuration
        self.sdpfirmware_proxy.clock_RW = CLK_200_MHZ
        time.sleep(1)

        self.proxy.initialise()
        self.proxy.Tracking_enabled_RW = False
        self.proxy.on()

        # Point to Zenith
        self.proxy.set_pointing(
            numpy.array(
                [["AZELGEO", "0rad", "1.570796rad"]] * self.proxy.nr_beamlets_R
            ).flatten()
        )

        # beam weights should now be non-zero, we don't actually check their values for correctness
        FPGA_bf_weights_pp_clock200 = self.beamlet_proxy.FPGA_bf_weights_pp_RW.flatten()
        self.assertNotEqual(0, sum(FPGA_bf_weights_pp_clock200))

        self.beamlet_proxy = self.initialise_beamlet_proxy()
        self.beamlet_proxy.on()

        # Change clock configuration
        self.sdpfirmware_proxy.clock_RW = CLK_160_MHZ
        time.sleep(1)

        FPGA_bf_weights_pp_clock160 = self.beamlet_proxy.FPGA_bf_weights_pp_RW.flatten()
        # Assert some values are different
        self.assertNotEqual(
            sum(FPGA_bf_weights_pp_clock160), sum(FPGA_bf_weights_pp_clock200)
        )

    def test_pointing_to_zenith_subband_change(self):
        self.beamlet_proxy.subband_select_RW = numpy.array(
            list(range(317)) + [316] + list(range(318, N_beamlets_ctrl)),
            dtype=numpy.uint32,
        )
        self.beamlet_proxy.on()

        self.proxy.initialise()
        self.proxy.Tracking_enabled_RW = False
        self.proxy.on()

        # Point to Zenith
        self.proxy.set_pointing(
            numpy.array(
                [["AZELGEO", "0rad", "1.570796rad"]] * self.proxy.nr_beamlets_R
            ).flatten()
        )
        # Store values with first subband configuration
        FPGA_bf_weights_pp_subband_v1 = (
            self.beamlet_proxy.FPGA_bf_weights_pp_RW.flatten()
        )

        # Restart beamlet proxy
        self.beamlet_proxy = self.initialise_beamlet_proxy()
        self.beamlet_proxy.subband_select_RW = [317] * N_beamlets_ctrl
        self.beamlet_proxy.on()

        # Store values with second subband configuration
        FPGA_bf_weights_pp_subband_v2 = (
            self.beamlet_proxy.FPGA_bf_weights_pp_RW.flatten()
        )
        # Assert some values are different
        self.assertNotEqual(
            sum(FPGA_bf_weights_pp_subband_v1), sum(FPGA_bf_weights_pp_subband_v2)
        )

    def test_set_pointing_masked_enable(self):
        """Verify that only selected inputs are written"""

        self.proxy.initialise()
        self.proxy.Tracking_enabled_RW = False
        self.proxy.on()

        # Enable first input
        self.proxy.Antenna_Set_RW = "FIRST"

        # fill weights with values the beamformer will never use (|x| > 1)
        impossible_values = numpy.array([[2] * N_beamlets_ctrl * A_pn] * N_pn)
        self.beamlet_proxy.FPGA_bf_weights_pp_RW = impossible_values

        self.proxy.set_pointing(
            numpy.array(
                [["AZELGEO", "0rad", "1.570796rad"]] * self.proxy.nr_beamlets_R
            ).flatten()
        )

        # Verify all impossible values are replaced with other values for all inputs
        # which should be non-zero for the first antenna, and zero for the rest
        FPGA_bf_weights_pp_RW = self.beamlet_proxy.FPGA_bf_weights_pp_RW.reshape(
            (N_pn * A_pn, -1)
        )

        # first antenna should have values from the beamformer, so not 0 and not 2
        self.assertTrue(
            numpy.all(numpy.not_equal(0, FPGA_bf_weights_pp_RW[0, :])),
            f"{FPGA_bf_weights_pp_RW}",
        )
        self.assertTrue(
            numpy.all(numpy.not_equal(2, FPGA_bf_weights_pp_RW[0, :])),
            f"{FPGA_bf_weights_pp_RW}",
        )

        # rest of the antennas should have been given a weight of 0, as they
        # were not in the usage mask.
        self.assertTrue(
            numpy.all(numpy.equal(0, FPGA_bf_weights_pp_RW[1 : self.tile_number, :])),
            f"{FPGA_bf_weights_pp_RW}",
        )

    @timeout_decorator.timeout(15)
    def test_beam_tracking_90_percent_interval(self):
        """Verify that the beam tracking operates within 95% of interval"""

        self.proxy.initialise()
        self.proxy.Tracking_enabled_RW = True
        self.proxy.on()

        interval = float(
            self.proxy.get_property("Beam_tracking_interval")["Beam_tracking_interval"][
                0
            ]
        )

        # Allow beam tracking time to settle
        time.sleep(interval * 3)

        # We have to poll at regular interval due to not working subscribe
        # events
        for _ in range(0, 5):
            error = self.proxy.Pointing_error_R[0]
            self.assertTrue(
                -interval * 0.10 < error < interval * 0.10,
                f"Error: {error} larger than {interval * 0.10}",
            )
            logger.info("BeamTracking error: %s", error)
            time.sleep(interval)
