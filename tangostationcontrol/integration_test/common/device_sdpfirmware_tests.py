# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase

from tangostationcontrol.common.constants import N_pn
from tangostationcontrol.common.env_decorators import ensure_device_boots


class SDPFirmwareDeviceTests(TestDeviceBase):
    """Integration test class for device SDPFirmware"""

    __test__ = False

    def setUp(self, sdpfirmware_name: str = None):
        """Intentionally recreate the device object in each test"""

        if sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        super().setUp(sdpfirmware_name)

    @ensure_device_boots()
    def test_device_sdpfirmware_read_attribute(self):
        """Test if we can read an attribute obtained over OPC-UA"""
        self.assertListEqual(
            [True] * N_pn, list(self.proxy.TR_fpga_communication_error_R)
        )
