# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import time
from ctypes import c_short

import numpy
import numpy.testing

from integration_test.default.devices.base import TestDeviceBase

from tango import DevState
from tangostationcontrol.common.constants import (
    N_beamlets_ctrl,
    S_pn,
    CLK_200_MHZ,
    CLK_160_MHZ,
    N_pn,
    A_pn,
)


class BeamletDeviceTests(TestDeviceBase):
    """Integration test class for device Beamlet"""

    __test__ = False

    def setUp(
        self,
        beamlet_name: str = None,
        sdp_name: str = None,
        sdpfirmware_name: str = None,
    ):
        """Intentionally recreate the device object in each test"""

        if beamlet_name is None or sdp_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        super().setUp(beamlet_name)

        self.sdp_proxy = self.setup_proxy(sdp_name, defaults=True)
        self.sdp_proxy.nyquist_zone_RW = [[2] * S_pn] * N_pn

        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name, defaults=True)
        self.sdpfirmware_proxy.clock_RW = CLK_200_MHZ

    def test_pointing_to_zenith(self):
        self.proxy.initialise()
        self.proxy.on()

        # The subband frequency of HBA subband 0 is 200 MHz,
        # so its period is 5 ns. A delay of 2.5e-9 seconds is thus half a period,
        # and result in a 180 degree phase offset.
        delays = numpy.array([[[2.5e-9] * N_pn] * A_pn] * N_beamlets_ctrl)

        calculated_bf_weights = self.proxy.calculate_bf_weights(delays.flatten())

        # With a unit weight of 2**14, we thus expect beamformer weights of -2**14 + 0j,
        # which is 49152 when read as an uint32.
        self.assertEqual(-(2**14), c_short(49152).value)  # check our calculations
        expected_bf_weights = numpy.array(
            [49152] * N_pn * A_pn * N_beamlets_ctrl, dtype=numpy.uint32
        )

        numpy.testing.assert_almost_equal(expected_bf_weights, calculated_bf_weights)

    def test_subband_select_change(self):
        # Change subband
        self.proxy.off()
        self.proxy.initialise()
        self.assertEqual(DevState.STANDBY, self.proxy.state())
        self.proxy.subband_select_RW = [10] * N_beamlets_ctrl
        self.proxy.on()
        self.assertEqual(DevState.ON, self.proxy.state())

        # The subband frequency of HBA subband 10 is 201953125 Hz
        # so its period is 4.95 ns ca, half period is 2.4758e-9
        delays = numpy.array([[[2.4758e-9] * N_pn] * A_pn] * N_beamlets_ctrl)
        calculated_bf_weights_subband_10 = self.proxy.calculate_bf_weights(
            delays.flatten()
        )

        self.assertEqual(-(2**14), c_short(49152).value)  # check our calculations
        expected_bf_weights_10 = numpy.array(
            [49152] * N_pn * A_pn * N_beamlets_ctrl, dtype=numpy.uint32
        )
        numpy.testing.assert_almost_equal(
            expected_bf_weights_10, calculated_bf_weights_subband_10
        )

    def test_sdp_clock_change(self):
        sdpfirmware_proxy = self.sdpfirmware_proxy
        self.proxy.initialise()
        self.proxy.subband_select_RW = numpy.array(
            list(range(317)) + [316] + list(range(318, N_beamlets_ctrl)),
            dtype=numpy.uint32,
        )
        self.proxy.on()

        # any non-zero delay should result in different weights for different clocks
        delays = numpy.array([[[2.5e-9] * N_pn] * A_pn] * N_beamlets_ctrl)

        sdpfirmware_proxy.clock_RW = CLK_200_MHZ
        time.sleep(3)  # wait for beamlet device to process change event
        calculated_bf_weights_200 = self.proxy.calculate_bf_weights(delays.flatten())

        sdpfirmware_proxy.clock_RW = CLK_160_MHZ
        time.sleep(3)  # wait for beamlet device to process change event
        calculated_bf_weights_160 = self.proxy.calculate_bf_weights(delays.flatten())

        sdpfirmware_proxy.clock_RW = CLK_200_MHZ
        time.sleep(3)  # wait for beamlet device to process change event
        calculated_bf_weights_200_v2 = self.proxy.calculate_bf_weights(delays.flatten())

        # outcome should be changed back and forth across clock changes
        self.assertTrue((calculated_bf_weights_200 != calculated_bf_weights_160).all())
        self.assertTrue(
            (calculated_bf_weights_200 == calculated_bf_weights_200_v2).all()
        )

        # change subbands
        self.proxy.off()
        self.proxy.initialise()
        self.proxy.subband_select_RW = [317] * N_beamlets_ctrl
        self.proxy.on()
        calculated_bf_weights_200_v3 = self.proxy.calculate_bf_weights(delays.flatten())
        self.assertTrue(
            (calculated_bf_weights_200_v2 != calculated_bf_weights_200_v3).all()
        )

        sdpfirmware_proxy.clock_RW = CLK_160_MHZ
        time.sleep(1)  # wait for beamlet device to process change event
        calculated_bf_weights_160_v2 = self.proxy.calculate_bf_weights(delays.flatten())
        self.assertTrue(
            (calculated_bf_weights_160 != calculated_bf_weights_160_v2).all()
        )
