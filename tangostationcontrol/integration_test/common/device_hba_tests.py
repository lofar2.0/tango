#  Copyright (C)  2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

import logging
import time
import numpy

from integration_test.default.devices.base import TestDeviceBase
from tango import DevState

from tangostationcontrol.common.constants import (
    N_elements,
    MAX_ANTENNA,
    N_pol,
    N_rcu,
    N_rcu_inp,
    CS001_TILES,
    CLK_160_MHZ,
    N_pn,
    S_pn,
)
from tangostationcontrol.common.frequency_bands import bands
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AntennaStatus,
    AntennaUse,
)

# tests will break in weird ways if our constants change beyond these constraints
assert CS001_TILES >= 2
assert N_pn * S_pn >= 4
assert N_rcu_inp * N_rcu >= 2

logger = logging.getLogger()


class HBADeviceTests(TestDeviceBase):
    """Integration base test class for device HBA"""

    __test__ = False

    def setUp(
        self,
        tiles: int = CS001_TILES,
        afh_name: str = None,
        sdpfirmware_name: str = None,
        sdp_name: str = None,
    ):
        if afh_name is None or sdp_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        self.tiles = tiles

        self.stationmanager_proxy = self.setup_proxy("STAT/StationManager/1")

        # Setup will dump current properties and restore them for us
        super().setUp(afh_name)

        # Typical tests emulate 'CS001_TILES' number of antennas in
        # the AntennaField. 'MAX_ANTENNA' is the number of inputs
        # offered by a backing RECV device. Each antenna has
        # N_pol (2) inputs.
        self.proxy.put_property(
            {
                "Power_to_RECV_mapping": numpy.array(
                    [[1, x * 2 + 0] for x in range(tiles)]
                ).flatten(),
                "Control_to_RECV_mapping": numpy.array(
                    [[1, x * 2 + 1] for x in range(tiles)]
                ).flatten(),
                "Frequency_Band_RW_default": numpy.array(
                    [["HBA_110_190", "HBA_110_190"]] * tiles
                ).flatten(),
            }
        )
        self.recv_proxy = self.setup_proxy("STAT/RECVH/H0", defaults=True)
        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name)
        self.sdp_proxy = self.setup_proxy(sdp_name)

        # configure the frequencies, which allows access
        # to the calibration attributes and commands
        self.sdpfirmware_proxy.clock_RW = CLK_160_MHZ
        self.recv_proxy.RCU_band_select_RW = [[1] * N_rcu_inp] * N_rcu

    def test_ANT_mask_RW_configured_after_Antenna_Usage_Mask(self):
        """Verify if ANT_mask_RW values are correctly configured from Antenna_Usage_Mask values"""

        antennafield_proxy = self.proxy
        numpy.testing.assert_equal(
            numpy.array([[True] * N_rcu_inp] * N_rcu), self.recv_proxy.ANT_mask_RW
        )

        antenna_status = numpy.array([AntennaStatus.OK] * self.tiles)
        antenna_use = numpy.array(
            [AntennaUse.ON] + [AntennaUse.AUTO] * (self.tiles - 1)
        )
        antenna_properties = {
            "Antenna_Status": antenna_status,
            "Antenna_Use": antenna_use,
        }
        mapping_properties = {
            "Power_to_RECV_mapping": [-1, -1] * self.tiles,
            # Two inputs of recv device connected, only defined for 48 inputs
            # each pair is one input
            "Control_to_RECV_mapping": [1, 0, 1, 1] + [-1, -1] * (self.tiles - 2),
        }
        antennafield_proxy.off()
        antennafield_proxy.put_property(antenna_properties)
        antennafield_proxy.put_property(mapping_properties)
        antennafield_proxy.boot()
        antennafield_proxy.power_hardware_on()

        # Verify all antennas are indicated to work
        numpy.testing.assert_equal(
            numpy.array([True] * self.tiles), antennafield_proxy.Antenna_Usage_Mask_R
        )

        # Verify only connected inputs + Antenna_Usage_Mask_R are true
        # As well as dimensions of ANT_mask_RW must match control mapping
        numpy.testing.assert_equal(
            numpy.array([True] * 2 + [False] * (self.tiles - 2)),
            antennafield_proxy.ANT_mask_RW,
        )

        # Verify recv proxy values unaffected as default for ANT_mask_RW is true
        numpy.testing.assert_equal(
            numpy.array([True] * 2 + [True] * (MAX_ANTENNA - 2)),
            self.recv_proxy.ANT_mask_RW.flatten(),
        )

    def test_ANT_mask_RW_configured_after_Antenna_Usage_Mask_only_one_functioning_antenna(
        self,
    ):
        """Verify if ANT_mask_RW values are correctly configured from
        Antenna_Usage_Mask values (only second antenna is OK)"""

        antennafield_proxy = self.proxy

        # Broken antennas except second
        antenna_status = numpy.array(
            [AntennaStatus.BROKEN]
            + [AntennaStatus.OK]
            + [AntennaStatus.BROKEN] * (self.tiles - 2)
        )
        antenna_use = numpy.array([AntennaUse.AUTO] * self.tiles)
        antenna_properties = {
            "Antenna_Status": antenna_status,
            "Antenna_Use": antenna_use,
        }

        # Configure control mapping to control all 96 inputs of recv device
        mapping_properties = {
            "Power_to_RECV_mapping": [-1, -1] * self.tiles,
            "Control_to_RECV_mapping":
            # [1, 0,  1, 1,  1, 2,  1, x  ...  1, 95]
            numpy.array([[1, x] for x in range(self.tiles)]).flatten(),
        }

        # Cycle device and set properties
        antennafield_proxy.off()
        antennafield_proxy.put_property(antenna_properties)
        antennafield_proxy.put_property(mapping_properties)
        antennafield_proxy.boot()
        antennafield_proxy.power_hardware_on()

        # Antenna_Usage_Mask_R should be false except one
        numpy.testing.assert_equal(
            numpy.array([False] + [True] + [False] * (self.tiles - 2)),
            antennafield_proxy.Antenna_Usage_Mask_R,
        )
        # device.power_hardware_on() writes Antenna_Usage_Mask_R to ANT_mask_RW
        numpy.testing.assert_equal(
            numpy.array([False] + [True] + [False] * (self.tiles - 2)),
            antennafield_proxy.ANT_mask_RW,
        )
        # ANT_mask_RW on antennafield writes to configured recv devices for all
        # mapped inputs. Unmapped values at the end remain at True.
        numpy.testing.assert_equal(
            numpy.array(
                [False]
                + [True]
                + [False] * (self.tiles - 2)
                + [True] * (MAX_ANTENNA - self.tiles)
            ),
            self.recv_proxy.ANT_mask_RW.flatten(),
        )

    def test_antennafield_set_mapped_attribute_ignore_all(self):
        """Verify RECV device attribute unaffected by antennafield if not mapped"""

        # Connect recvh/1 device but no control inputs
        mapping_properties = {
            "Power_to_RECV_mapping": [-1, -1] * self.tiles,
            "Control_to_RECV_mapping": [-1, -1] * self.tiles,
        }

        # Cycle device an put properties
        antennafield_proxy = self.proxy
        antennafield_proxy.off()
        antennafield_proxy.put_property(mapping_properties)
        antennafield_proxy.boot()

        # Set HBAT_PWR_on_RW to false on recv device and read results
        self.recv_proxy.write_attribute(
            "HBAT_PWR_on_RW", [[False] * N_elements * N_pol] * MAX_ANTENNA
        )
        current_values = self.recv_proxy.read_attribute("HBAT_PWR_on_RW").value

        # write true through antennafield
        antennafield_proxy.write_attribute(
            "HBAT_PWR_on_RW", [[True] * N_elements * N_pol] * self.tiles
        )
        # Test that original recv values for HBAT_PWR_on_RW match current
        numpy.testing.assert_equal(
            current_values, self.recv_proxy.read_attribute("HBAT_PWR_on_RW").value
        )

        # Verify device did not enter FAULT state
        self.assertEqual(DevState.ON, antennafield_proxy.state())

    def test_antennafield_set_mapped_attribute(self):
        """Verify RECV device attribute changed by antennafield if mapped inputs"""

        mapping_properties = {
            "Power_to_RECV_mapping": [-1, -1] * self.tiles,
            # Each pair is one mapping so 2 inputs are connected
            "Control_to_RECV_mapping": [1, 0, 1, 1] + [-1, -1] * (self.tiles - 2),
        }

        antennafield_proxy = self.proxy
        antennafield_proxy.off()
        antennafield_proxy.put_property(mapping_properties)
        antennafield_proxy.boot()

        self.recv_proxy.write_attribute(
            "HBAT_PWR_on_RW", [[False] * N_elements * N_pol] * MAX_ANTENNA
        )

        try:
            antennafield_proxy.write_attribute(
                "HBAT_PWR_on_RW", [[True] * N_elements * N_pol] * self.tiles
            )
            numpy.testing.assert_equal(
                numpy.array(
                    [[True] * N_elements * N_pol] * 2
                    + [[False] * N_elements * N_pol] * (MAX_ANTENNA - 2)
                ),
                self.recv_proxy.read_attribute("HBAT_PWR_on_RW").value,
            )
        finally:
            # Always restore recv again
            self.recv_proxy.write_attribute(
                "HBAT_PWR_on_RW", [[False] * N_elements * N_pol] * MAX_ANTENNA
            )

        # Verify device did not enter FAULT state
        self.assertEqual(DevState.ON, antennafield_proxy.state())

    def test_antennafield_set_mapped_attribute_all(self):
        """Verify RECV device attribute changed by antennafield all inputs mapped"""

        mapping_properties = {
            "Power_to_RECV_mapping": [-1, -1] * self.tiles,
            "Control_to_RECV_mapping":
            # [1, 0, 1, 1, 1, 2, 1, x ... 1, 95]
            numpy.array([[1, x] for x in range(self.tiles)]).flatten(),
        }

        antennafield_proxy = self.proxy
        antennafield_proxy.off()
        antennafield_proxy.put_property(mapping_properties)
        antennafield_proxy.boot()

        self.recv_proxy.write_attribute(
            "HBAT_PWR_on_RW", [[False] * N_elements * N_pol] * MAX_ANTENNA
        )

        try:
            antennafield_proxy.write_attribute(
                "HBAT_PWR_on_RW", [[True] * N_elements * N_pol] * self.tiles
            )

            # Mapped values went to True
            numpy.testing.assert_equal(
                numpy.array([[True] * N_elements * N_pol] * self.tiles),
                self.recv_proxy.read_attribute("HBAT_PWR_on_RW").value[: self.tiles],
            )
            # Unmapped values went to False
            numpy.testing.assert_equal(
                numpy.array(
                    [[False] * N_elements * N_pol] * (MAX_ANTENNA - self.tiles)
                ),
                self.recv_proxy.read_attribute("HBAT_PWR_on_RW").value[self.tiles :],
            )
        finally:
            # Always restore recv again
            self.recv_proxy.write_attribute(
                "HBAT_PWR_on_RW", [[False] * N_elements * N_pol] * MAX_ANTENNA
            )

        # Verify device did not enter FAULT state
        self.assertEqual(DevState.ON, antennafield_proxy.state())

    def test_antennafield_set_mapped_attribute_small(self):
        """Verify small RECV device attribute changed all inputs mapped"""

        mapping_properties = {
            "Power_to_RECV_mapping": numpy.array(  # X maps on power
                [[1, x * 2 + 1] for x in range(self.tiles)]
            ).flatten(),
            "Control_to_RECV_mapping": numpy.array(  # Y maps on control
                [[1, x * 2 + 0] for x in range(self.tiles)]
            ).flatten(),
        }

        antennafield_proxy = self.proxy
        antennafield_proxy.off()
        antennafield_proxy.put_property(mapping_properties)
        antennafield_proxy.boot()

        self.recv_proxy.write_attribute("RCU_band_select_RW", [[0] * N_rcu_inp] * N_rcu)

        try:
            antennafield_proxy.write_attribute(
                # [X, Y]
                "RCU_band_select_RW",
                [[1, 2]] * self.tiles,
            )

            # Mapped values were overwritten
            numpy.testing.assert_equal(
                # [Power, Control]
                numpy.array([2, 1] * self.tiles),
                self.recv_proxy.read_attribute("RCU_band_select_RW").value.flatten()[
                    : (self.tiles * 2)
                ],
            )
            # Unmapped values remain at 0
            numpy.testing.assert_equal(
                # [Power, Control]
                numpy.array([0] * (MAX_ANTENNA - self.tiles * 2)),
                self.recv_proxy.read_attribute("RCU_band_select_RW").value.flatten()[
                    (self.tiles * 2) :
                ],
            )
        finally:
            # Always restore recv again
            self.recv_proxy.write_attribute(
                "RCU_band_select_RW", [[0] * N_rcu_inp] * N_rcu
            )

        # Verify device did not enter FAULT state
        self.assertEqual(DevState.ON, antennafield_proxy.state())

    def test_frequency_band(self):
        # Test whether changes in frequency band propagate properly.
        VALID_MODI = ["HBA_110_190", "HBA_170_230", "HBA_210_250"]

        properties = {
            "Control_to_RECV_mapping": [1, 1] + [-1, -1] * (self.tiles - 1),
            "Power_to_RECV_mapping": [1, 0] + [-1, -1] * (self.tiles - 1),
            "Antenna_to_SDP_Mapping": [0, 1, 0, 0] + [-1, -1] * (self.tiles - 2),
        }

        antennafield_proxy = self.proxy
        antennafield_proxy.off()
        antennafield_proxy.put_property(properties)
        antennafield_proxy.boot()

        for band in [b for b in bands.values() if b.name in VALID_MODI]:
            # clear downstream settings
            self.recv_proxy.write_attribute(
                "RCU_band_select_RW", [[0] * N_rcu_inp] * N_rcu
            )
            self.sdp_proxy.write_attribute("nyquist_zone_RW", [[0] * S_pn] * N_pn)

            antennafield_proxy.write_attribute(
                "Frequency_Band_RW", [[band.name, band.name]] * self.tiles
            )
            # Wait for Tango attributes updating
            time.sleep(1)

            # test whether clock propagated correctly
            numpy.testing.assert_equal(
                band.clock, self.sdpfirmware_proxy.clock_RW, err_msg=f"{band.name}"
            )

            # test whether nyquist zone propagated correctly (for both polarisations!)
            numpy.testing.assert_equal(
                numpy.array(
                    [band.nyquist_zone, band.nyquist_zone] * 2 + [0] * (N_pn * S_pn - 4)
                ),
                self.sdp_proxy.read_attribute("nyquist_zone_RW").value.flatten(),
                err_msg=f"{band.name}",
            )

            # test whether rcu filter propagated correctly
            numpy.testing.assert_equal(
                numpy.array(
                    [band.rcu_band, band.rcu_band] + [0] * (N_rcu_inp * N_rcu - 2)
                ),
                self.recv_proxy.read_attribute("RCU_band_select_RW").value.flatten(),
                err_msg=f"{band.name}",
            )

            # test whether reading back results in the same band for our inputs
            numpy.testing.assert_equal(
                numpy.array([band.name, band.name]),
                antennafield_proxy.read_attribute("Frequency_Band_RW").value[0],
                err_msg=f"{band.name}",
            )
