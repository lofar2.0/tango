# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""
Power Hierarchy module integration test
"""
import logging

from tango import DevState, DeviceProxy

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy
from lofar_station_client.common import CaseInsensitiveString
from tangostationcontrol.common.constants import N_rcu, N_rcu_inp
from tangostationcontrol.devices.base_device_classes.hierarchy_device import (
    NotFoundException,
    HierarchyMatchingFilter,
)
from tangostationcontrol.devices.base_device_classes.power_hierarchy import (
    PowerHierarchyControlDevice,
)

logger = logging.getLogger()


class DevicePowerHierarchyControlTests(base.IntegrationTestCase):
    """Integration Test class for PowerHierarchyDevice methods"""

    __test__ = False

    pwr_attr_name = "hardware_powered_R"

    stationmanager_name = "STAT/StationManager/1"
    ec_name = "STAT/EC/1"
    aps_l0_name = "STAT/APS/L0"
    aps_l1_name = "STAT/APS/L1"
    aps_h0_name = "STAT/APS/H0"
    apsct_name = "STAT/APSCT/H0"
    apspu_h0_name = "STAT/APSPU/H0"
    apspu_l0_name = "STAT/APSPU/L0"
    apspu_l1_name = "STAT/APSPU/L1"
    ccd_name = "STAT/CCD/1"
    pcon_name = "STAT/PCON/1"
    sdp_name = "STAT/SDP/HBA0"
    unb2_h0_name = "STAT/UNB2/H0"
    unb2_l0_name = "STAT/UNB2/L0"
    recvh_name = "STAT/RECVH/H0"
    recvl_name = "STAT/RECVL/L0"

    def setUp(
        self,
        station_name: str = "CS001",
        antennafield_name: str = None,
        sdp_name: str = None,
        sdpfirmware_name: str = None,
    ):
        super().setUp()

        if antennafield_name is None or sdp_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        self.station_name = station_name

        self.antennafield_name = antennafield_name
        self.sdp_name = sdp_name
        self.sdpfirmware_name = sdpfirmware_name

        self.setup_all_devices()

    def _unacceptable_exceptions(self):
        """Return the set of exceptions raised by the last state transition
        in the StationManager, that we do not accept.

        We must accept exceptions since we do not emulate interaction with
        actual hardware. In this function, we make sure to just ignore those
        which we know will be raised even in a sunny-day scenario."""

        result = []

        for ex_str in self.stationmanager_proxy.last_requested_transition_exceptions_R:
            # Skip "vetted" exceptions that involve switching power
            # on actual hardware, which obviously won't power on in
            # our simulators.
            if "Failed to execute command_inout on device" in ex_str:
                if "command power_hardware_on" in ex_str:
                    continue
                if "command power_hardware_off" in ex_str:
                    continue

            # Anything left is not acceptable
            result.append(ex_str)

        return result

    def setup_stationmanager_proxy(self):
        """Initialise StationManager device"""
        stationmanager_proxy = TestDeviceProxy(self.stationmanager_name)
        # extend timeout for running commands, as state transitions can take a long time
        stationmanager_proxy.set_timeout_millis(60000)

        stationmanager_proxy.off()
        stationmanager_proxy.initialise()
        stationmanager_proxy.on()
        self.assertEqual(stationmanager_proxy.state(), DevState.ON)
        return stationmanager_proxy

    def setup_proxy_off(self, device_name: str):
        """Initialise proxy and turn off device"""
        proxy = TestDeviceProxy(device_name)
        proxy.off()
        return proxy

    def setup_all_devices(self):
        """Initialise all Tango devices needed for state transitions"""
        self.stationmanager_proxy = self.setup_stationmanager_proxy()

        self.ec_proxy = self.setup_proxy_off(self.ec_name)
        self.aps_l0_proxy = self.setup_proxy_off(self.aps_l0_name)
        self.pcon_proxy = self.setup_proxy_off(self.pcon_name)
        self.ccd_proxy = self.setup_proxy_off(self.ccd_name)
        self.apspu_h0_proxy = self.setup_proxy_off(self.apspu_h0_name)
        self.apspu_l0_proxy = self.setup_proxy_off(self.apspu_l0_name)
        self.apsct_proxy = self.setup_proxy_off(self.apsct_name)
        self.unb2_h0_proxy = self.setup_proxy_off(self.unb2_h0_name)
        self.unb2_l0_proxy = self.setup_proxy_off(self.unb2_l0_name)
        self.recvh_proxy = self.setup_proxy_off(self.recvh_name)
        self.recvl_proxy = self.setup_proxy_off(self.recvl_name)
        self.sdpfirmware_proxy = self.setup_proxy_off(self.sdpfirmware_name)
        self.sdp_proxy = self.setup_proxy_off(self.sdp_name)
        self.antennafield_proxy = self.setup_proxy_off(self.antennafield_name)

    def test_power_sequence_definition(self):
        """
        Test whether Power Sequence is correctly retrieved from the HierarchyDevice
        """
        self.setup_stationmanager_proxy()
        self.setup_all_devices()

        stationmanager_ph = PowerHierarchyControlDevice()
        stationmanager_ph.init(self.stationmanager_name)
        children_hierarchy = stationmanager_ph.children(depth=2)

        # Check if EC is child of StationManager
        ec_name = self.ec_name.casefold()
        self.assertTrue(ec_name in children_hierarchy)
        self.assertTrue(isinstance(children_hierarchy[ec_name]["proxy"], DeviceProxy))

        # Check if CCD is child of EC
        self.assertTrue(
            self.ccd_name.casefold() in children_hierarchy[ec_name]["children"].keys()
        )

        # Check if EC retrieves correctly its parent state (StationManager -> ON)
        ec_ph = PowerHierarchyControlDevice()
        ec_ph.init(self.ec_name)
        self.assertEqual(ec_ph.parent_state(), DevState.ON)
        # Check if child reads correctly a parent attribute
        self.assertEqual(
            ec_ph.read_parent_attribute("Station_Name_R"),
            self.station_name,
        )

    def test_off_to_hibernate(self):
        """Test Tango devices are correctly triggered in the OFF to HIBERNATE transition"""
        self.assertEqual(self.pcon_proxy.state(), DevState.OFF)
        self.assertEqual(self.ccd_proxy.state(), DevState.OFF)
        # Switch from OFF to HIBERNATE
        self.stationmanager_proxy.station_hibernate()
        self.assertEqual(self.stationmanager_proxy.station_state_R.name, "HIBERNATE")
        self.assertEqual(
            self.stationmanager_proxy.requested_station_state_R.name, "HIBERNATE"
        )
        self.assertEqual(self.pcon_proxy.state(), DevState.ON)
        self.assertEqual(self.ccd_proxy.state(), DevState.ON)

        logger.info(
            "Exceptions suppressed in test_off_to_hibernate: %s",
            self.stationmanager_proxy.last_requested_transition_exceptions_R,
        )

        self.assertListEqual([], self._unacceptable_exceptions())

    def test_hibernate_to_standby(self):
        """
        Test whether Tango devices are correctly triggered in the HIBERNATE to STANDBY transition
        """
        # Switch from OFF to HIBERNATE
        self.stationmanager_proxy.station_hibernate()
        self.assertEqual(self.stationmanager_proxy.station_state_R.name, "HIBERNATE")
        self.assertEqual(
            self.stationmanager_proxy.requested_station_state_R.name, "HIBERNATE"
        )
        self.assertEqual(self.apspu_h0_proxy.state(), DevState.OFF)
        self.assertEqual(self.apspu_l0_proxy.state(), DevState.OFF)
        self.assertEqual(self.apsct_proxy.state(), DevState.OFF)
        self.assertEqual(self.unb2_h0_proxy.state(), DevState.OFF)
        self.assertEqual(self.unb2_l0_proxy.state(), DevState.OFF)
        self.assertEqual(self.recvh_proxy.state(), DevState.OFF)
        self.assertEqual(self.recvl_proxy.state(), DevState.OFF)
        self.assertEqual(self.sdpfirmware_proxy.state(), DevState.OFF)
        # Switch from HIBERNATE to STANDBY
        self.stationmanager_proxy.station_standby()
        self.assertEqual(self.stationmanager_proxy.station_state_R.name, "STANDBY")
        self.assertEqual(
            self.stationmanager_proxy.requested_station_state_R.name, "STANDBY"
        )
        self.assertEqual(self.apspu_h0_proxy.state(), DevState.ON)
        self.assertEqual(self.apspu_l0_proxy.state(), DevState.ON)
        self.assertEqual(self.apsct_proxy.state(), DevState.ON)
        self.assertEqual(self.unb2_h0_proxy.state(), DevState.ON)
        self.assertEqual(self.unb2_l0_proxy.state(), DevState.ON)
        self.assertEqual(self.recvh_proxy.state(), DevState.ON)
        self.assertEqual(self.recvl_proxy.state(), DevState.ON)
        self.assertEqual(self.sdpfirmware_proxy.state(), DevState.ON)
        # Check if SDP Firmware is booted with factory image
        firmware_images = self.sdpfirmware_proxy.FPGA_boot_image_RW.tolist()
        self.assertListEqual(
            [0] * len(firmware_images),
            firmware_images,
        )

        logger.info(
            "Exceptions suppressed: %s",
            self.stationmanager_proxy.last_requested_transition_exceptions_R,
        )

        self.assertListEqual([], self._unacceptable_exceptions())

    def test_standby_to_on(self):
        """
        Test whether Tango devices are correctly triggered in the STANDBY to ON transition
        """
        # Switch from OFF to HIBERNATE
        self.stationmanager_proxy.station_hibernate()
        # Switch from HIBERNATE to STANDBY
        self.stationmanager_proxy.station_standby()
        # Switch from STANDBY to ON
        self.assertEqual(self.sdp_proxy.state(), DevState.OFF)
        self.assertEqual(self.antennafield_proxy.state(), DevState.OFF)
        self.stationmanager_proxy.station_on()
        self.assertEqual(self.stationmanager_proxy.station_state_R.name, "ON")
        self.assertEqual(self.stationmanager_proxy.requested_station_state_R.name, "ON")
        self.assertEqual(self.sdp_proxy.state(), DevState.ON)
        self.assertEqual(self.antennafield_proxy.state(), DevState.ON)

        # Test if DTH and DAB are disabled
        self.assertListEqual(
            self.recvh_proxy.RCU_DTH_on_R.tolist(),
            [[False] * N_rcu_inp] * N_rcu,
        )
        self.assertListEqual(
            self.antennafield_proxy.RCU_DAB_filter_on_RW.tolist(),
            [False] * self.antennafield_proxy.nr_antennas_R,
        )

        logger.info(
            "Exceptions suppressed: %s",
            self.stationmanager_proxy.last_requested_transition_exceptions_R,
        )

        self.assertListEqual([], self._unacceptable_exceptions())

    def test_on_to_standby(self):
        """
        Test whether Tango devices are correctly triggered in the ON to STANDBY transition
        """
        # Switch from OFF to HIBERNATE
        self.stationmanager_proxy.station_hibernate()
        # Switch from HIBERNATE to STANDBY
        self.stationmanager_proxy.station_standby()
        # Switch from STANDBY to ON
        self.stationmanager_proxy.station_on()
        # Reverse to STANDBY
        self.stationmanager_proxy.station_standby()
        self.assertEqual(self.stationmanager_proxy.station_state_R.name, "STANDBY")
        self.assertEqual(
            self.stationmanager_proxy.requested_station_state_R.name, "STANDBY"
        )
        self.assertEqual(self.sdp_proxy.state(), DevState.OFF)
        self.assertEqual(self.antennafield_proxy.state(), DevState.OFF)

        logger.info(
            "Exceptions suppressed: %s",
            self.stationmanager_proxy.last_requested_transition_exceptions_R,
        )

        self.assertListEqual([], self._unacceptable_exceptions())

    def test_standby_to_hibernate(self):
        """
        Test whether Tango devices are correctly triggered in the STANDBY to HIBERNATE transition
        """
        # Switch from OFF to HIBERNATE
        self.stationmanager_proxy.station_hibernate()
        # Switch from HIBERNATE to STANDBY
        self.stationmanager_proxy.station_standby()
        # Reverse to HIBERNATE
        self.stationmanager_proxy.station_hibernate()
        self.assertEqual(self.stationmanager_proxy.station_state_R.name, "HIBERNATE")
        self.assertEqual(
            self.stationmanager_proxy.requested_station_state_R.name, "HIBERNATE"
        )
        self.assertEqual(self.apspu_h0_proxy.state(), DevState.OFF)
        self.assertEqual(self.apspu_l0_proxy.state(), DevState.OFF)
        self.assertEqual(self.apsct_proxy.state(), DevState.OFF)
        self.assertEqual(self.unb2_h0_proxy.state(), DevState.OFF)
        self.assertEqual(self.unb2_l0_proxy.state(), DevState.OFF)
        self.assertEqual(self.recvh_proxy.state(), DevState.OFF)
        self.assertEqual(self.recvl_proxy.state(), DevState.OFF)
        self.assertEqual(self.sdpfirmware_proxy.state(), DevState.OFF)

        logger.info(
            "Exceptions suppressed: %s",
            self.stationmanager_proxy.last_requested_transition_exceptions_R,
        )

        self.assertListEqual([], self._unacceptable_exceptions())

    def test_branch_child_deep_tree(self):
        """
        Test whether Tango devices are correctly retrieved with branch_child function
        """
        self.setup_all_devices()
        # Create a Hierarchy Device from Device STAT/RECVH/H0
        recvh_ph = PowerHierarchyControlDevice()
        recvh_ph.init(self.recvh_name)
        self.assertEqual(recvh_ph.parent(), "stat/apspu/h0")
        # Branch child method must not return its direct parent
        self.assertRaises(
            NotFoundException,
            recvh_ph.branch_child,
            "*/apspu/h*",
            HierarchyMatchingFilter.REGEX,
        )
        # Test if returns another device with the right query
        self.assertEqual(
            recvh_ph.branch_child("*/apspu/*", HierarchyMatchingFilter.REGEX).name(),
            CaseInsensitiveString(self.apspu_l0_name),
        )
        # Test if returns a device in the tree with depth > 1
        self.assertEqual(
            recvh_ph.branch_child("*/aps/l*", HierarchyMatchingFilter.REGEX).name(),
            CaseInsensitiveString(self.aps_l0_name),
        )

    def test_branch_children_names_deep_tree(self):
        """
        Test whether Tango devices are correctly retrieved with
        branch_children_names function
        """
        self.setup_all_devices()
        # Create a Hierarchy Device from Device STAT/RECVH/H0
        recvh_ph = PowerHierarchyControlDevice()
        recvh_ph.init(self.recvh_name)
        self.assertEqual(recvh_ph.parent(), "stat/apspu/h0")
        self.assertEqual(recvh_ph.children(), {})
        # Branch_children_names method must not return its direct parent
        self.assertListEqual(
            recvh_ph.branch_children_names("*/apspu/h*", HierarchyMatchingFilter.REGEX),
            [],
        )
        # Test if returns another device with the right query
        self.assertListEqual(
            recvh_ph.branch_children_names("*/apspu/*", HierarchyMatchingFilter.REGEX),
            [
                CaseInsensitiveString(self.apspu_l0_name),
                CaseInsensitiveString(self.apspu_l1_name),
            ],
        )
        # Test if returns a device in the tree with depth > 1
        self.assertListEqual(
            recvh_ph.branch_children_names("*/aps/*", HierarchyMatchingFilter.REGEX),
            [
                CaseInsensitiveString(self.aps_l0_name),
                CaseInsensitiveString(self.aps_l1_name),
                CaseInsensitiveString(self.aps_h0_name),
            ],
        )
