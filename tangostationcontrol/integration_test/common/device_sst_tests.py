# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import socket

from integration_test.default.devices.base import TestDeviceBase
from tangostationcontrol.common.env_decorators import ensure_device_boots

from tango import DevState


class SSTDeviceTests(TestDeviceBase):
    """Integration test class for device SST"""

    __test__ = False

    UDP_PORT = 5011
    TCP_PORT = 5111

    def setUp(self, sst_name: str = None, sdpfirmware_name: str = None):
        """Intentionally recreate the device object in each test"""

        if sst_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        super().setUp(sst_name)

        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name, defaults=True)

    @ensure_device_boots()
    def test_device_sst_send_udp(self):
        port_property = {"Statistics_Client_TCP_Port": "4998"}
        self.proxy.put_property(port_property)

        self.assertEqual(DevState.ON, self.proxy.state())

        s1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s1.connect(("device-sst.service.consul", 5001))

        # TODO(Corne): Change me into an actual SST packet
        s1.send("Hello World!".encode("UTF-8"))

        s1.close()
