# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase


class XSTDeviceTests(TestDeviceBase):
    __test__ = False

    def setUp(self, xst_name: str = None, sdpfirmware_name: str = None):
        """Intentionally recreate the device object in each test"""

        if xst_name is None or sdpfirmware_name is None:
            raise RuntimeError("Missing required device names to initialize tests")

        super().setUp(xst_name)

        self.sdpfirmware_proxy = self.setup_proxy(sdpfirmware_name, defaults=True)
