# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
import statistics
import time
import numpy

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy

from tango import DevState
from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AntennaStatus,
    AntennaUse,
)
from tangostationcontrol.common.constants import DEFAULT_N_HBA_TILES
from tangostationcontrol.common.env_decorators import (
    restore_properties_for_devices,
    preserve_off_state_for_devices,
)

logger = logging.getLogger()


class TestTilebeamPerformance(base.IntegrationTestCase):
    """Integration test class for Tilebeam device performances"""

    # The AntennaField is setup with self.NR_TILES tiles in the test configuration
    NR_TILES = 48
    POINTING_DIRECTION = numpy.array([["J2000", "0rad", "0rad"]] * NR_TILES).flatten()
    # List of devices that need to be started up / shut down during tests
    STARTUP_DEVICES = [
        "STAT/StationManager/1",
        "STAT/AFH/HBA0",
        "STAT/AFH/HBA1",
        "STAT/AFH/HBA2",
        "STAT/AFH/HBA3",
    ]

    def setUp(self):
        super(TestTilebeamPerformance, self).setUp()

    @restore_properties_for_devices(STARTUP_DEVICES)
    @preserve_off_state_for_devices(STARTUP_DEVICES)
    def test_tilebeam_performance(self):
        hba_devices = ["HBA0", "HBA1", "HBA2", "HBA3"]
        beam_proxies = []
        antenna_field_proxies = []
        recv_proxies = []

        # StationManager
        manager_proxy = TestDeviceProxy("STAT/StationManager/1")
        manager_children = manager_proxy.get_property("Control_Children")[
            "Control_Children"
        ]
        for hba_device in hba_devices:
            manager_children.append(f"STAT/AFH/{hba_device}")
        manager_proxy.put_property({"Control_Children": manager_children})

        # SDPFirmware and SDP must be ready before AntennaField
        sdpfirmware_proxy = TestDeviceProxy("STAT/SDPFirmware/HBA0")
        sdpfirmware_proxy.off()
        self.assertTrue(sdpfirmware_proxy.state() is DevState.OFF)
        sdpfirmware_proxy.boot()
        sdpfirmware_proxy.set_defaults()
        self.assertTrue(sdpfirmware_proxy.state() is DevState.ON)

        sdp_proxy = TestDeviceProxy("STAT/SDP/HBA0")
        sdp_proxy.off()
        self.assertTrue(sdp_proxy.state() is DevState.OFF)
        sdp_proxy.boot()
        sdp_proxy.set_defaults()
        self.assertTrue(sdp_proxy.state() is DevState.ON)

        # Beam / Recv [HBA0, HBA1, HBA2, HBA3]
        for i, item in enumerate(hba_devices):
            recv_proxies.append(TestDeviceProxy(f"STAT/RECVH/{i+1}"))
            antenna_field_proxies.append(TestDeviceProxy(f"STAT/AFH/{item}"))
            beam_proxies.append(TestDeviceProxy(f"STAT/TileBeam/{item}"))

        # Recv and AntennaField devices must be ready before TileBeam
        for proxy in recv_proxies:
            proxy.off()
            self.assertTrue(proxy.state() is DevState.OFF)
            proxy.boot()
            proxy.set_defaults()
            self.assertTrue(proxy.state() is DevState.ON)

        for n in range(1, 5):
            proxy = antenna_field_proxies[n - 1]
            # setup AntennaField
            nr_tiles = DEFAULT_N_HBA_TILES
            control_mapping = [[1, i] for i in range(nr_tiles)]
            antenna_status = numpy.array([AntennaStatus.OK] * nr_tiles)
            antenna_use = numpy.array([AntennaUse.AUTO] * nr_tiles)
            proxy.put_property(
                {
                    "Control_Children": [
                        "STAT/SDPFirmware/HBA0",
                        f"STAT/RECVH/{n}",
                        f"STAT/tilebeam/HBA{n-1}",
                    ],
                    "Control_to_RECV_mapping": numpy.array(control_mapping).flatten(),
                    "Antenna_Status": antenna_status,
                    "Antenna_Use": antenna_use,
                    "Antenna_Sets": [
                        "ALL",
                    ],
                    "Antenna_Set_Masks": [
                        "111111111111111111111111111111111111111111111111"
                    ],
                }
            )
            proxy.off()
            proxy.boot()
            self.assertEqual(nr_tiles, proxy.nr_antennas_R)
            self.assertTrue(proxy.state() is DevState.ON)

        for proxy in beam_proxies:
            proxy.off()
            self.assertTrue(proxy.state() is DevState.OFF)
            proxy.boot()
            proxy.set_defaults()
            proxy.Tracking_enabled_RW = False
            self.assertTrue(proxy.state() is DevState.ON)

        results = []
        for _i in range(25):
            start_time = time.monotonic_ns()
            for proxy in beam_proxies:
                proxy.set_pointing(self.POINTING_DIRECTION)
            stop_time = time.monotonic_ns()
            results.append(stop_time - start_time)

        logging.error(
            "Median %s Stdev %s",
            statistics.median(results) / 1.0e9,
            statistics.stdev(results) / 1.0e9,
        )
