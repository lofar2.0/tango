# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import os
import unittest

import pydevd_pycharm
import testscenarios

from tangostationcontrol.common.lofar_logging import configure_logger

from integration_test.reset_fixture import reset_exported_devices

"""Setup logging for integration tests"""
configure_logger(debug=True)

if os.environ.get("DEBUG_HOST"):
    pydevd_pycharm.settrace(
        os.environ["DEBUG_HOST"], port=12345, stdoutToServer=True, stderrToServer=True
    )


class BaseIntegrationTestCase(testscenarios.WithScenarios, unittest.TestCase):
    """Integration test base class."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        reset_exported_devices()

    def setUp(self):
        super().setUp()


class IntegrationTestCase(BaseIntegrationTestCase):
    """Integration test case base class for all unit tests."""

    def setUp(self):
        super().setUp()


class IntegrationAsyncTestCase(
    testscenarios.WithScenarios, unittest.IsolatedAsyncioTestCase
):
    """Integration test case base class for all asyncio unit tests."""

    def setUp(self):
        super().setUp()
