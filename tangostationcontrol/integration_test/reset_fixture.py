# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
import logging
import re

from tango import Database

from integration_test.device_proxy import TestDeviceProxy

DEFAULT_SKIP_SERVER_NAMES = [
    "configuration/stat",
    "databaseds/2",
    "tangorestserver/rest",
    "tangotest/test",
    "tangoaccesscontrol/1",
    "database/2",
]

logger = logging.getLogger()


def reset_exported_devices():
    """Find all devices exported by the database and turn them off"""
    db = Database()

    for device in db.get_device_exported("*"):
        dev_info = db.get_device_info(device)
        if dev_info.class_name == "DServer":
            continue

        if re.findall(
            r"(?=(" + "|".join(DEFAULT_SKIP_SERVER_NAMES) + r"))", dev_info.name
        ):
            logger.debug("Skipping: %s", dev_info.name)
            continue

        try:
            logger.debug("Turning off: %s", device)
            device_proxy = TestDeviceProxy(device)
            device_proxy.off()
        except Exception as e:
            logger.error("Turning off %s failed due to %s", device, e)
