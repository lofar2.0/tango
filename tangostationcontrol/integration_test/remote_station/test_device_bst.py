# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_bst_tests import BSTDeviceTests


class TestDeviceBSTHBA(BSTDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/BST/HBA", "STAT/sdpfirmware/HBA", "STAT/SDP/HBA")
