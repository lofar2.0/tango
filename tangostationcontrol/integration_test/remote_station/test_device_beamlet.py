# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_beamlet_tests import BeamletDeviceTests


class TestDeviceBeamletHBA(BeamletDeviceTests):
    __test__ = True

    def setUp(self):
        super().setUp("STAT/Beamlet/HBA", "STAT/SDP/HBA", "STAT/sdpfirmware/HBA")
