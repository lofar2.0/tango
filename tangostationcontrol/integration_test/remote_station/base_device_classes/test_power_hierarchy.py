# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""Power Hierarchy HBA integration test"""

import logging

from integration_test.common.base_device_classes.power_hierarchy_tests import (
    DevicePowerHierarchyControlTests,
)

logger = logging.getLogger()


class TestPowerHierarchyControlDeviceHBA(DevicePowerHierarchyControlTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("RS307", "STAT/AFH/HBA", "STAT/SDP/HBA", "STAT/sdpfirmware/HBA")
