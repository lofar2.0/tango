#  Copyright (C)  2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_sdp_tests import SDPDeviceTests


class TestSDPDeviceHBA(SDPDeviceTests):
    """Integration base test class for device HBA"""

    __test__ = True

    def setUp(self):
        super().setUp(
            sdp_name="STAT/SDP/HBA",
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA",
        )
