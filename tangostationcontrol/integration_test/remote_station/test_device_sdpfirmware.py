#  Copyright (C)  2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_sdpfirmware_tests import SDPFirmwareDeviceTests


class TestSDPFirmwareDeviceHBA(SDPFirmwareDeviceTests):
    """Integration base test class for device HBA"""

    __test__ = True

    def setUp(self):
        super().setUp(
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA",
        )
