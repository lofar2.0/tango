# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.constants import RS307_TILES

from integration_test.common.device_tilebeam_tests import TileBeamDeviceTests


class TestDeviceTilebeamHBA(TileBeamDeviceTests):
    __test__ = True

    def setUp(self):
        super().setUp(
            RS307_TILES,
            "STAT/tilebeam/HBA",
            "STAT/AFH/HBA",
            "STAT/SDP/HBA",
            "STAT/sdpfirmware/HBA",
            "STAT/recvh/h0",
        )

        self.LOFAR_REF_POINTING = [
            21,
            24,
            27,
            30,
            14,
            17,
            20,
            23,
            8,
            11,
            14,
            17,
            1,
            4,
            7,
            10,
        ]
        self.LOFAR_REF_REPEATS = 2
