# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.constants import RS307_TILES

from integration_test.common.device_digitalbeam_tests import DigitalBeamDeviceTests


class TestDeviceDigitalBeamHBA(DigitalBeamDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp(
            RS307_TILES,
            "STAT/digitalbeam/HBA",
            "STAT/AFH/HBA",
            "STAT/beamlet/HBA",
            "STAT/SDP/HBA",
            "STAT/sdpfirmware/HBA",
            "STAT/recvh/h0",
        )
