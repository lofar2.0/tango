# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.constants import RS307_TILES
from tangostationcontrol.test.dummy_observation_settings import (
    get_observation_settings_hba_core_immediate,
)

from integration_test.common.device_observation_control_tests import (
    DeviceObservationControlTests,
)


class TestDeviceObservationControlHBA(DeviceObservationControlTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        obs_data = get_observation_settings_hba_core_immediate()
        obs_data.station = "rs307"
        obs_data.antenna_fields[0].antenna_field = "HBA"

        super().setUp(
            RS307_TILES,
            obs_data.to_json(),
            "STAT/AFH/HBA",
            "STAT/recvh/h0",
            "STAT/SDP/HBA",
            "STAT/sdpfirmware/HBA",
            "STAT/beamlet/HBA",
            "STAT/digitalbeam/HBA",
            "STAT/tilebeam/HBA",
            "STAT/sst/HBA",
            "STAT/xst/HBA",
            "STAT/bst/HBA",
        )
