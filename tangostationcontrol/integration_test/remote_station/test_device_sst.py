# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_sst_tests import SSTDeviceTests


class TestDeviceBSTHBA(SSTDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/SST/HBA", "STAT/sdpfirmware/HBA")
