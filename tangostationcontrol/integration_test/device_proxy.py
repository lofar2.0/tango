# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
import time

from tango import DeviceProxy, DevSource

logger = logging.getLogger()


class TestDeviceProxy(DeviceProxy):
    def __init__(self, *args, **kwargs):
        super(TestDeviceProxy, self).__init__(*args, **kwargs)

        # extend timeout for running commands
        self.set_timeout_millis(60000)

        # When requesting attribute values and states,
        # always get values from the device, never the polling
        # cache. This makes sure updates are immediately reflected
        # in this proxy, which is essential for fast-paced tests.
        #
        # See also https://www.tango-controls.org/community/forum/c/development/python/attribute-direct-reading-from-device-when-polling-is-turned-on/
        self.set_source(DevSource.DEV)

    @staticmethod
    def test_device_turn_off(endpoint: str, sleep: float = 1):
        d = TestDeviceProxy(endpoint)
        try:
            d.Off()
        except Exception as exc:
            # Failing to turn Off devices should not raise errors here
            logger.error("Failed to turn device off in teardown {%s}", exc)
            # Wait to prevent propagating reconnection errors
            time.sleep(sleep)
