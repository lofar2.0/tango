# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_sst_tests import SSTDeviceTests


class TestDeviceSSTHBA0(SSTDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/SST/HBA0", "STAT/sdpfirmware/HBA0")


class TestDeviceSSTHBA1(SSTDeviceTests):
    __test__ = True

    UDP_PORT = 5021
    TCP_PORT = 5121

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/SST/HBA1", "STAT/sdpfirmware/HBA1")
