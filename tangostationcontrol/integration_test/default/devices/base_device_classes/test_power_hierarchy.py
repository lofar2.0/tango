# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""
Power Hierarchy module integration test
"""
import logging

import tangostationcontrol
from tango import Database

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy

from integration_test.common.base_device_classes.power_hierarchy_tests import (
    DevicePowerHierarchyControlTests,
)

logger = logging.getLogger()


class TestPowerAvailableInStateAttribute(base.IntegrationTestCase):
    expected = {
        "StationManager": "HIBERNATE",
        "CCD": "HIBERNATE",
        "PCON": "HIBERNATE",
        "SDPFirmware": "STANDBY",
        "ProtectionControl": "HIBERNATE",
        "APS": "STANDBY",
        "APSCT": "STANDBY",
        "APSPU": "STANDBY",
        "UNB2": "STANDBY",
        "RECVH": "STANDBY",
        "RECVL": "STANDBY",
        "ObservationField": "NONE",
    }

    longMessage = True

    def test_power_available_in_state_attribute(self):
        db = Database()

        for device in db.get_device_exported("*"):
            dev_info = db.get_device_info(device)
            if dev_info.class_name == "DServer":
                continue

            device_proxy = TestDeviceProxy(device)
            if dev_info.class_name not in dir(tangostationcontrol.devices):
                logger.warning(
                    f"device {device} has no attribute available_in_power_state_R"
                )
                continue

            available_in_state = device_proxy.available_in_power_state_R
            if dev_info.class_name in self.expected:
                self.assertEqual(
                    available_in_state,
                    self.expected[dev_info.class_name],
                    f"Device {dev_info.class_name}",
                )
            else:
                self.assertEqual(
                    available_in_state, "ON", f"Device {dev_info.class_name}"
                )


class TestPowerHierarchyControlDeviceHBA0(DevicePowerHierarchyControlTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp(
            "CS001", "STAT/AFH/HBA0", "STAT/SDP/HBA0", "STAT/sdpfirmware/HBA0"
        )
