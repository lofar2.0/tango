# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

"""
Power Hierarchy module integration test
"""
import logging

from tango import Database

import tangostationcontrol.devices
from integration_test import base
from integration_test.device_proxy import TestDeviceProxy

logger = logging.getLogger()


class TestDeviceProxyTimeoutAttribute(base.IntegrationTestCase):
    expected = {
        "StationManager": 60000,
    }

    longMessage = True

    def test_device_proxy_timeout_attribute(self):
        db = Database()

        for device in db.get_device_exported("*"):
            dev_info = db.get_device_info(device)
            if dev_info.class_name == "DServer":
                continue

            device_proxy = TestDeviceProxy(device)
            if dev_info.class_name not in dir(tangostationcontrol.devices):
                logger.warning(
                    f"device {device} has no attribute device_proxy_timeout_R"
                )
                continue

            device_proxy_timeout = device_proxy.device_proxy_timeout_R
            if dev_info.class_name in self.expected:
                self.assertEqual(
                    device_proxy_timeout,
                    self.expected[dev_info.class_name],
                    f"Device {dev_info.class_name}",
                )
            else:
                self.assertEqual(
                    device_proxy_timeout, 10000, f"Device {dev_info.class_name}"
                )
