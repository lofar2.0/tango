# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_xst_tests import XSTDeviceTests


class TestDeviceXSTHBA0(XSTDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/XST/HBA0", "STAT/sdpfirmware/HBA0")


class TestDeviceXSTHBA1(XSTDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/XST/HBA1", "STAT/sdpfirmware/HBA1")
