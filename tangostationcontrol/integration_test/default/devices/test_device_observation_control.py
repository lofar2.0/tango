# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.constants import CS001_TILES
from tangostationcontrol.test.dummy_observation_settings import (
    get_observation_settings_hba_core_immediate,
)

from integration_test.common.device_observation_control_tests import (
    DeviceObservationControlTests,
)


class TestDeviceObservationControlHBA0(DeviceObservationControlTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        json = get_observation_settings_hba_core_immediate().to_json()

        super().setUp(
            CS001_TILES,
            json,
            "STAT/AFH/HBA0",
            "STAT/recvh/h0",
            "STAT/SDP/HBA0",
            "STAT/sdpfirmware/HBA0",
            "STAT/beamlet/HBA0",
            "STAT/digitalbeam/HBA0",
            "STAT/tilebeam/HBA0",
            "STAT/sst/HBA0",
            "STAT/xst/HBA0",
            "STAT/bst/HBA0",
        )
