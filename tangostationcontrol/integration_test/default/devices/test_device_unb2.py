# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase


class TestDeviceUNB2H0(TestDeviceBase):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""
        super().setUp("STAT/UNB2/H0")
