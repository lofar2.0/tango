# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

from integration_test.default.devices.base import TestDeviceBase

logger = logging.getLogger()


class TestMetadataDevice(TestDeviceBase):
    __test__ = True

    def setUp(self):
        super().setUp("STAT/Metadata/1")

    def test_send_metadata(self):
        """Turn on the device and emit metadata"""

        self.proxy.boot()
        self.proxy.send_metadata()
