# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import time

import numpy

from integration_test.default.devices.base import TestDeviceBase


class TestDeviceRECV(TestDeviceBase):
    __test__ = True

    HBAT_DELAY_UPDATE_INTERVAL = 0.1

    def setUp(self):
        super().setUp("STAT/RECVH/H0")

    def test_update_HBAT_bf_delay_steps(self):
        """Verify HBAT_bf_delay_steps_stage propagates to HBAT_bf_delay_steps."""

        # update faster to speed up tests
        self.proxy.put_property(
            {"HBAT_BF_delay_steps_update_interval": self.HBAT_DELAY_UPDATE_INTERVAL}
        )
        self.proxy.off()
        self.proxy.boot()

        # write new value to HBAT_BF_delay_steps_stage_RW
        delay_steps = numpy.ones(
            self.proxy.HBAT_BF_delay_steps_stage_RW.shape, dtype=numpy.uint64
        )
        self.proxy.HBAT_BF_delay_steps_stage_RW = delay_steps

        # make sure HBAT_BF_delay_steps_RW is updated
        time.sleep(self.HBAT_DELAY_UPDATE_INTERVAL * 10)

        # verify whether it did
        numpy.testing.assert_equal(self.proxy.HBAT_BF_delay_steps_RW, delay_steps)
