# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tango import Database

from integration_test import base


class TestTangoDatabase(base.IntegrationTestCase):
    def setUp(self):
        """Intentionally recreate the device object in each test"""
        super(TestTangoDatabase, self).setUp()

    def test_database_servers(self):
        """Connect to the database and find at least 3 servers

        One for SDP, RECV and the databaseds itself.
        """

        d = Database()

        # Ensure this value is close to actual amount of servers defined by
        # integration_ConfigDb.json
        self.assertGreater(
            len(d.get_server_list()), 16, msg=f"Servers: {d.get_server_list()}"
        )
