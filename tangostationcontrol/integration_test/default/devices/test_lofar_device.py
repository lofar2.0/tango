# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import time

from tango import DevState

from tangostationcontrol.common.constants import DEFAULT_POLLING_PERIOD_MS

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy


class TestProxyAttributeAccess(base.IntegrationTestCase):
    """Test whether DeviceProxy's can always access attributes immediately after turning them on."""

    # We use RECVH as our victim. Any device would do.
    DEVICE_NAME = "STAT/RECVH/H0"

    # an attribute to access
    ATTRIBUTE_NAME = "ANT_mask_RW"

    def setUp(self):
        self.proxy = TestDeviceProxy(self.DEVICE_NAME)
        self.proxy.off()

    def poll_attribute(self):
        # make sure the attribute is polled, so we force proxies to access the poll first
        if self.proxy.is_attribute_polled(self.ATTRIBUTE_NAME):
            self.proxy.stop_poll_attribute(self.ATTRIBUTE_NAME)
        self.proxy.poll_attribute(self.ATTRIBUTE_NAME, DEFAULT_POLLING_PERIOD_MS)

    def dont_poll_attribute(self):
        # make sure the attribute is NOT polled, so we force proxies to access the device
        if self.proxy.is_attribute_polled(self.ATTRIBUTE_NAME):
            self.proxy.stop_poll_attribute(self.ATTRIBUTE_NAME)

    def read_attribute(self):
        # turn on the device
        self.proxy.initialise()
        self.assertEqual(DevState.STANDBY, self.proxy.state())

        # read the attribute -- shouldn't throw
        _ = self.proxy.read_attribute(self.ATTRIBUTE_NAME)

    def test_fast_setup_polled_attribute(self):
        """Setup a device as fast as possible and access its attributes immediately."""

        self.poll_attribute()
        self.read_attribute()

    def test_slow_setup_polled_attribute(self):
        """Have the device be off for a while, allowing Tango to poll. Then,
        Setup a device as fast as possible and access its attributes immediately."""

        self.poll_attribute()
        time.sleep(3)  # allow Tango to poll the attribute in OFF state
        self.read_attribute()

    def test_fast_setup_nonpolled_attribute(self):
        """Setup a device as fast as possible and access its attributes immediately."""
        self.dont_poll_attribute()
        self.read_attribute()

    def test_slow_setup_nonpolled_attribute(self):
        """Have the device be off for a while, allowing Tango to poll. Then,
        Setup a device as fast as possible and access its attributes immediately."""
        self.dont_poll_attribute()
        time.sleep(3)  # allow Tango to poll the attribute in OFF state
        self.read_attribute()
