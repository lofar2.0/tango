# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase


class TestDeviceRECV(TestDeviceBase):
    __test__ = True

    def setUp(self):
        super().setUp("STAT/RECVL/L0")
