#  Copyright (C)  2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.constants import CS001_TILES

from integration_test.common.device_hba_tests import HBADeviceTests


class TestHBADevice(HBADeviceTests):
    """Integration base test class for device HBA"""

    __test__ = True

    def setUp(self):
        super().setUp(
            tiles=CS001_TILES,
            afh_name="STAT/AFH/HBA0",
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA0",
            sdp_name="STAT/SDP/HBA0",
        )
