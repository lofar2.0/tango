# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
import time
from typing import List

import timeout_decorator
from tango import Database, DevState

from tangostationcontrol.devices import UNB2, SDPFirmware, RECVL, RECVH, APSCT, APSPU

from integration_test.default.devices.base import TestDeviceBase

logger = logging.getLogger()


class TestDeviceProtectionControl(TestDeviceBase):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""
        db = Database()

        self.stationmanager = self.setup_proxy("stat/stationmanager/1")
        self.stationmanager.protection_lock_RW = False

        unb2_devices = db.get_device_exported_for_class(UNB2.__name__)
        sdpfirmware_devices = db.get_device_exported_for_class(SDPFirmware.__name__)
        recvl_devices = db.get_device_exported_for_class(RECVL.__name__)
        recvh_devices = db.get_device_exported_for_class(RECVH.__name__)
        apsct_devices = db.get_device_exported_for_class(APSCT.__name__)
        apspu_devices = db.get_device_exported_for_class(APSPU.__name__)

        self.setup_proxies(unb2_devices)
        self.setup_proxies(sdpfirmware_devices)
        self.setup_proxies(recvl_devices)
        self.setup_proxies(recvh_devices)
        self.setup_proxies(unb2_devices)
        self.setup_proxies(apsct_devices)
        self.setup_proxies(apspu_devices)

        super().setUp("stat/protectioncontrol/1")

    def setup_proxies(self, names: List[str]):
        proxies = []

        for name in names:
            recv_proxy = self.setup_proxy(name)
            proxies.append(recv_proxy)

        return proxies

    def test_protection_locks_station(self):
        """Manually trigger protection and establish if it locks stationmanager"""

        try:
            self.proxy.boot()
            self.assertEqual(self.proxy.state(), DevState.ON)

            self.assertFalse(self.stationmanager.protection_lock_RW)

            self.proxy.protective_shutdown("Test shutdown!")

            time.sleep(1)

            self.assertTrue(self.stationmanager.protection_lock_RW)
        finally:
            self.stationmanager.protection_lock_RW = False

    def test_connection_metrics(self):
        """Read the metrics about the number of connected / discovered devices"""

        self.proxy.boot()

        self.assertEqual(self.proxy.state(), DevState.ON)
        self.assertFalse(self.stationmanager.protection_lock_RW)

        self.assertTrue(self.proxy.number_of_discovered_devices_R > 0)
        self.assertTrue(self.proxy.number_of_connected_devices_R > 0)

        self.assertEqual(
            self.proxy.number_of_discovered_devices_R,
            self.proxy.number_of_connected_devices_R,
        )

    @timeout_decorator.timeout(10)
    def test_change_events_received(self):

        self.proxy.boot()

        self.assertEqual(self.proxy.state(), DevState.ON)
        self.assertFalse(self.stationmanager.protection_lock_RW)

        while (
            self.proxy.number_of_discovered_devices_R
            != self.proxy.number_of_connected_devices_R
        ):
            logger.info("Waiting for subscriptions to connect")

        for index, value in enumerate(self.proxy.time_last_change_events_R):
            self.assertTrue(
                value > 0,
                f"Did not receive an update for: {self.proxy.identified_device_attribute_pairs_R[index]}",
            )
