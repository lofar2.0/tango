#  Copyright (C)  2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_sdpfirmware_tests import SDPFirmwareDeviceTests


class TestSDPFirmwareDeviceHBA0(SDPFirmwareDeviceTests):
    """Integration base test class for device HBA0"""

    __test__ = True

    def setUp(self):
        super().setUp(
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA0",
        )


class TestSDPFirmwareDeviceHBA1(SDPFirmwareDeviceTests):
    """Integration base test class for device HBA1"""

    __test__ = True

    def setUp(self):
        super().setUp(
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA1",
        )
