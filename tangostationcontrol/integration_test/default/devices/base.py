# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import Callable, Dict

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy

from tango import DevState, AttrWriteType

from tangostationcontrol.devices.base_device_classes.opcua_device import OPCUADevice


class TestDeviceBase(base.IntegrationTestCase):
    __test__ = False

    def setUp(self, name=""):
        # if name == "":
        #    raise unittest.SkipTest("This is a base class for other tests")

        # create a proxy
        self.name = name
        self.proxy = TestDeviceProxy(self.name)

        # make sure the device starts in Off
        self.proxy.Off()

        # make sure the device stops in Off
        self.addCleanup(TestDeviceProxy.test_device_turn_off, self.name)

        # make a backup of the properties, in case they're changed
        self.addCleanup(
            self.restore_properties, self.proxy, self.current_properties(self.proxy)
        )

        super().setUp()

    @staticmethod
    def current_properties(proxy: TestDeviceProxy) -> Dict:
        """Return the current properties of the given proxy"""
        return proxy.get_property(proxy.get_property_list("*") or {}) or {}

    @staticmethod
    def restore_properties(proxy: TestDeviceProxy, properties: Dict):
        """Restore the properties as they were before the test."""
        proxy.put_property(properties)

    def setup_proxy(
        self,
        name: str,
        defaults: bool = False,
        restore_properties: bool = False,
        cb: Callable[[TestDeviceProxy], None] = None,
    ):
        """Setup A TestDeviceProxy and handle cleanup"""
        proxy = TestDeviceProxy(name)

        if restore_properties:
            self.addCleanup(proxy.test_device_turn_off, name)
            self.addCleanup(
                self.restore_properties, proxy, self.current_properties(proxy)
            )

        if cb:
            cb(proxy)

        proxy.off()
        proxy.initialise()
        proxy.on()

        if defaults:
            proxy.set_defaults()

        self.assertEqual(proxy.state(), DevState.ON)

        if not restore_properties:
            self.addCleanup(proxy.test_device_turn_off, name)

        return proxy

    def test_device_fetch_state(self):
        """Test if we can successfully fetch state"""

        self.assertEqual(DevState.OFF, self.proxy.state())

    def test_device_ping(self):
        """Test if we can successfully ping the device server"""

        self.assertGreater(self.proxy.ping(), 0)

    def test_device_initialize(self):
        """Test if we can transition to standby"""

        self.proxy.initialise()

        self.assertEqual(DevState.STANDBY, self.proxy.state())

    def test_device_missing_attributes(self):
        """Test if any attributes are missing from opcua devices"""

        if isinstance(self.proxy, OPCUADevice):
            self.assertListEqual([], self.proxy.opcua_missing_attributes_R)

    def test_device_on(self):
        """Test if we can transition off -> standby -> on"""

        self.proxy.initialise()
        self.proxy.on()

        self.assertEqual(DevState.ON, self.proxy.state())

    def test_device_boot(self):
        """Test if we can transition off -> on using a warm boot"""

        self.proxy.boot()

        self.assertEqual(DevState.ON, self.proxy.state())

    def test_device_read_all_attributes(self):
        """Test if we can read all of the exposed attributes in the ON state.

        This test covers the reading logic of all attributes."""

        self.proxy.initialise()
        self.proxy.on()

        for attribute_name in self.proxy.get_attribute_list():
            try:
                _ = self.proxy.read_attribute(attribute_name).value
            except Exception as exc:
                raise AssertionError(
                    f"Could not read attribute {attribute_name} from device {self.name}"
                ) from exc

    def test_device_write_all_attributes(self):
        """Test if we can write all of the exposed attributes in the ON state.

        This test covers the writing logic of all attributes."""

        self.proxy.initialise()
        self.proxy.set_defaults()
        self.proxy.on()

        for attribute_name in self.proxy.get_attribute_list():
            if (
                self.proxy.get_attribute_config(attribute_name).writable
                != AttrWriteType.READ_WRITE
            ):
                # only process read/write attributes
                continue

            try:
                # the value returned by the attribute should be safe to write back,
                # after the set_default() calls earlier.
                value = self.proxy.read_attribute(attribute_name).value
                self.proxy.write_attribute(attribute_name, value)
            except Exception as exc:
                raise AssertionError(
                    f"Could not write attribute {attribute_name} from device {self.name}"
                ) from exc
