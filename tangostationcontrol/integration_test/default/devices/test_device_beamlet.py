# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_beamlet_tests import BeamletDeviceTests


class TestDeviceBeamletHBA0(BeamletDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/Beamlet/HBA0", "STAT/SDP/HBA0", "STAT/sdpfirmware/HBA0")


class TestDeviceBeamletHBA1(BeamletDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp("STAT/Beamlet/HBA1", "STAT/SDP/HBA1", "STAT/sdpfirmware/HBA1")
