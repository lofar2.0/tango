# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from tangostationcontrol.common.constants import CS001_TILES

from integration_test.common.device_digitalbeam_tests import DigitalBeamDeviceTests


class TestDeviceDigitalBeamHBA0(DigitalBeamDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp(
            CS001_TILES,
            "STAT/digitalbeam/HBA0",
            "STAT/AFH/HBA0",
            "STAT/beamlet/HBA0",
            "STAT/SDP/HBA0",
            "STAT/sdpfirmware/HBA0",
            "STAT/recvh/h0",
        )


class TestDeviceDigitalBeamHBA1(DigitalBeamDeviceTests):
    __test__ = True

    def setUp(self):
        """Intentionally recreate the device object in each test"""

        super().setUp(
            CS001_TILES,
            "STAT/digitalbeam/HBA1",
            "STAT/AFH/HBA1",
            "STAT/beamlet/HBA1",
            "STAT/SDP/HBA1",
            "STAT/sdpfirmware/HBA1",
            "STAT/recvh/h0",
        )
