#  Copyright (C)  2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from integration_test.common.device_sdp_tests import SDPDeviceTests


class TestSDPDeviceHBA0(SDPDeviceTests):
    """Integration base test class for device HBA0"""

    __test__ = True

    def setUp(self):
        super().setUp(
            sdp_name="STAT/SDP/HBA0",
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA0",
        )


class TestSDPDeviceHBA1(SDPDeviceTests):
    """Integration base test class for device HBA1"""

    __test__ = True

    def setUp(self):
        super().setUp(
            sdp_name="STAT/SDP/HBA1",
            sdpfirmware_name="STAT/SDPFIRMWARE/HBA1",
        )
