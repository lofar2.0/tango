# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test.default.devices.base import TestDeviceBase


class TestDeviceAPSCT(TestDeviceBase):
    __test__ = True

    def setUp(self):
        super().setUp("STAT/APSCT/H0")
