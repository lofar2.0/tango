# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy

from tango import DevState
from tangostationcontrol.common.env_decorators import (
    restore_properties_for_devices,
    preserve_off_state_for_devices,
)


class TestDecorators(base.IntegrationTestCase):
    """Class to test integration test decorators"""

    def setUp(self):
        super(TestDecorators, self).setUp()

        self.sdpfirmware_proxy = TestDeviceProxy("STAT/SDPFirmware/HBA0")
        self.sdp_proxy = TestDeviceProxy("STAT/SDP/HBA0")
        self.restore_test_env()

    def init_test_env(self):
        # Class to test the decorators
        class TestClass:
            def __init__(self, proxy, child_proxy=None):
                self.proxy = proxy
                self.child_proxy = child_proxy

            @restore_properties_for_devices(["STAT/SDP/HBA0"])
            def test_restore_prop_for_devices_wrapper(self):
                # Simulate some action in the method
                self.proxy.put_property({"Control_Children": "STAT/MOCK/0"})
                self.child_proxy.put_property({"Control_Children": "STAT/MOCK/999"})
                firmware_property = self.proxy.get_property("Control_Children")[
                    "Control_Children"
                ]
                sdp_property = self.child_proxy.get_property("Control_Children")[
                    "Control_Children"
                ]
                return firmware_property, sdp_property

            @preserve_off_state_for_devices(["STAT/SDP/HBA0"])
            def test_preserve_off_state_for_devices_wrapper(self):
                # Simulate some action in the method
                self.proxy.boot()
                self.child_proxy.boot()
                return self.proxy.state(), self.child_proxy.state()

        self.test_instance = TestClass(
            proxy=self.sdpfirmware_proxy, child_proxy=self.sdp_proxy
        )

    def restore_test_env(self):
        """Method necessary to prevent configuration device errors"""
        self.sdpfirmware_proxy.Off()
        self.sdp_proxy.Off()
        self.sdpfirmware_proxy.put_property(
            {
                "Control_Children": [
                    "STAT/SDP/HBA0",
                    "STAT/BST/HBA0",
                    "STAT/SST/HBA0",
                    "STAT/XST/HBA0",
                ]
            }
        )
        self.sdpfirmware_proxy.put_property(
            {
                "Power_Children": [
                    "STAT/SDP/HBA0",
                    "STAT/Beamlet/HBA0",
                    "STAT/BST/HBA0",
                    "STAT/SST/HBA0",
                    "STAT/XST/HBA0",
                ]
            }
        )
        self.sdp_proxy.put_property({"Control_Children": []})

    def test_decorator_restore_property_for_devices(self):
        """Test whether decorator restore device properties by name"""
        self.init_test_env()
        # Verify some properties on the proxy
        self.assertListEqual(
            list(
                self.sdpfirmware_proxy.get_property("Control_Children")[
                    "Control_Children"
                ]
            ),
            ["STAT/SDP/HBA0", "STAT/BST/HBA0", "STAT/SST/HBA0", "STAT/XST/HBA0"],
        )
        self.assertListEqual(
            list(self.sdp_proxy.get_property("Control_Children")["Control_Children"]),
            [],
        )

        # Call the decorated method
        (
            firmware_prop,
            sdp_prop,
        ) = self.test_instance.test_restore_prop_for_devices_wrapper()

        # Assert that the method result is as expected
        self.assertListEqual(list(firmware_prop), ["STAT/MOCK/0"])
        self.assertListEqual(list(sdp_prop), ["STAT/MOCK/999"])

        # Assert that only the properties in the selected device were saved and then restored
        self.assertListEqual(
            list(
                self.sdpfirmware_proxy.get_property("Control_Children")[
                    "Control_Children"
                ]
            ),
            ["STAT/MOCK/0"],
        )
        self.assertListEqual(
            list(self.sdp_proxy.get_property("Control_Children")["Control_Children"]),
            [],
        )
        self.restore_test_env()

    def test_decorator_preserve_off_state_for_devices(self):
        """Test whether decorator preserve the off state of selected device
        at function exit"""
        self.init_test_env()
        self.assertEqual(DevState.OFF, self.sdpfirmware_proxy.state())
        self.assertEqual(DevState.OFF, self.sdp_proxy.state())
        # Call the decorated method
        (
            firmware_state,
            sdp_state,
        ) = self.test_instance.test_preserve_off_state_for_devices_wrapper()
        # Assert that the method result is as expected
        self.assertEqual(firmware_state, DevState.ON)
        self.assertEqual(sdp_state, DevState.ON)
        # Assert that the wrapper has restored the OFF state
        self.assertEqual(DevState.ON, self.sdpfirmware_proxy.state())
        self.assertEqual(DevState.OFF, self.sdp_proxy.state())
        self.restore_test_env()
