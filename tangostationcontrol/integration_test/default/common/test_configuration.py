# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import json

from jsonschema.exceptions import ValidationError
from integration_test.base import BaseIntegrationTestCase
from tango import Database

from tangostationcontrol.common.configuration import StationConfiguration


class TestStationConfiguration(BaseIntegrationTestCase):

    TEST_CONFIGURATION = """{
                            "servers": {
                                "AFH": {
                                    "STAT": {
                                        "AFH": {
                                            "STAT/AFH/1": {
                                                "properties": {
                                                    "Control_Children": [ "STAT/MOCKRCU/1" ]
                                                }
                                            }
                                        }
                                    }
                                },
                                "Observation": {
                                    "STAT": {
                                        "Observation": {
                                            "STAT/Observation/11": {}
                                        }
                                    }
                                }
                            }
                        }"""

    def setUp(self):
        self.sc = StationConfiguration(Database())

    def test_query_to_tuples(self):
        """Test whether Tango DB data are correctly converted into tuples"""
        raw_result = [
            "device1",
            "property_name1",
            "value1",
            "device1",
            "property_name2",
            "value2",
        ]
        num_col = 3
        record1 = ("device1", "property_name1", "value1")
        record2 = ("device1", "property_name2", "value2")
        expected_result = [record1, record2]
        self.assertEqual(self.sc.query_to_tuples(raw_result, num_col), expected_result)

    def test_add_to_devices_dict(self):
        """Test whether data retrieved from DB are correctly inserted into devices dictionary"""
        data = [
            ("device1", "property_name1", "value1"),
            ("device1", "property_name2", "value2"),
        ]
        expected_result = {
            "device1": {
                "properties": {
                    "property_name1": ["value1"],
                    "property_name2": ["value2"],
                }
            }
        }
        self.assertEqual(self.sc.add_to_devices_dict({}, data), expected_result)

    def test_add_to_attrs_dict(self):
        """Test whether data retrieved from DB are correctly inserted into attributes dictionary"""
        # Two attributes
        data_2attrs = [
            ("device1", "attribute1", "attr_property_name1", "value1"),
            ("device1", "attribute2", "attr_property_name1", "value2"),
        ]
        expected_result = {
            "device1": {
                "attribute_properties": {
                    "attribute1": {"attr_property_name1": ["value1"]},
                    "attribute2": {"attr_property_name1": ["value2"]},
                }
            }
        }
        self.assertEqual(self.sc.add_to_attrs_dict({}, data_2attrs), expected_result)
        # One attribute, two property values
        data_1attr = [
            ("device1", "attribute1", "attr_property_name1", "value1"),
            ("device1", "attribute1", "attr_property_name1", "value2"),
        ]
        expected_result = {
            "device1": {
                "attribute_properties": {
                    "attribute1": {"attr_property_name1": ["value1", "value2"]}
                }
            }
        }
        self.assertEqual(self.sc.add_to_attrs_dict({}, data_1attr), expected_result)

    def test_add_to_server_dict(self):
        """Test whether data retrieved from DB are correctly inserted into server dictionary"""
        data = [("server_name/server_instance", "server_class", "device1")]
        devices_dict = {
            "device1": {
                "properties": {
                    "property_name1": ["value1"],
                    "property_name2": ["value2"],
                }
            }
        }
        expected_result = {
            "server_name": {
                "server_instance": {
                    "server_class": {
                        "device1": {
                            "properties": {
                                "property_name1": ["value1"],
                                "property_name2": ["value2"],
                            }
                        }
                    }
                }
            }
        }
        self.assertEqual(
            self.sc.add_to_server_dict({}, devices_dict, data), expected_result
        )

    def check_configuration_validity(self, configuration: str) -> bool:
        """Helper method that asserts whether the station configuration json input
        is valid or not"""
        validator = self.sc.get_validator()
        json_configuration = json.loads(configuration)
        try:
            validator.validate(json_configuration)
            return True
        except (ValidationError, ValueError):
            return False
        except Exception as exc:
            raise Exception from exc

    def test_validate_json_schema(self):
        """Test whether a valid json schema is accepted as station control configuration"""
        # Empty input
        self.assertTrue(self.check_configuration_validity("""{}"""))
        self.assertTrue(self.check_configuration_validity("""{"servers":{}}"""))
        self.assertFalse(self.check_configuration_validity("""{"serverrr":{}}"""))
        self.assertFalse(
            self.check_configuration_validity("""{"server":{ "server": {} }}""")
        )
        # Test configuration JSON
        self.assertTrue(self.check_configuration_validity(self.TEST_CONFIGURATION))
        # Insert invalid field
        json_test_configuration = json.loads(self.TEST_CONFIGURATION)
        invalid_configuration = json_test_configuration["servers"]["AFH"]["STAT"][
            "AFH"
        ]["STAT/AFH/1"]["new_field"] = "test"
        self.assertFalse(
            self.check_configuration_validity(json.dumps(invalid_configuration))
        )
        # Set invalid type (integer instead of object)
        invalid_type_configuration = json_test_configuration["servers"] = 1
        self.assertFalse(
            self.check_configuration_validity(json.dumps(invalid_type_configuration))
        )
