# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from asyncua import Client

from integration_test import base


class TestSDPTRSim(base.IntegrationAsyncTestCase):
    def setUp(self):
        super(TestSDPTRSim, self).setUp()

    async def test_opcua_connection(self):
        """Check if we can connect to sdptr-sim"""

        client = Client("opc.tcp://sdptr-sim.service.consul:4840")
        root_node = None

        try:
            await client.connect()
            root_node = client.get_root_node()
        finally:
            await client.disconnect()

        self.assertNotEqual(None, root_node)
