# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncua
import numpy
import asyncio

from tangostationcontrol.clients.opcua_client import (
    OPCUAConnection,
    OPCUA_connection_tester,
    OPCUAConnectionStatus,
)

from integration_test import base


class TestClientServer(base.IntegrationAsyncTestCase):
    """Test the OPCUAConnection against an OPCUA server we instantiate ourselves."""

    async def setup_server(self, port):
        """Setup a server on a dedicated port for the test, to allow
        the tests to be run in parallel."""

        # where we will run the server
        self.endpoint = f"opc.tcp://127.0.0.1:{port}"
        self.namespace = "http://example.com"

        # setup an OPC-UA server
        self.server = asyncua.Server()
        await self.server.init()
        self.server.set_endpoint(self.endpoint)
        self.server.set_server_name(f"Test server spawned by {__file__}")

        # create an interface
        idx = await self.server.register_namespace(self.namespace)
        obj = self.server.get_objects_node()

        # add double_R/double_RW
        double_R = await obj.add_variable(idx, "double_R", 42.0)
        double_RW = await obj.add_variable(idx, "double_RW", 42.0)
        await double_RW.set_writable()

        # add methods
        @asyncua.uamethod
        def multiply(parent, x, y):
            self.assertEqual(float, type(x))
            self.assertEqual(int, type(y))
            return x * y

        @asyncua.uamethod
        def procedure(parent):
            return

        @asyncua.uamethod
        def throws(parent):
            """This is not the exception raised by the opcua_client since it will
            be caught"""
            raise ArithmeticError("Expected test exception")

        multiply_method = await obj.add_method(
            idx,
            "multiply",
            multiply,
            [asyncua.ua.VariantType.Double, asyncua.ua.VariantType.Int64],
            [asyncua.ua.VariantType.Double],
        )
        procedure_method = await obj.add_method(idx, "procedure", procedure, [], [])
        throws_method = await obj.add_method(idx, "throws", throws, [], [])

        # run the server
        await self.server.start()

    def setUp(self):
        self.server = None

    async def asyncTearDown(self):
        if self.server:
            await self.server.stop()

    async def test_opcua_connection(self):
        await self.setup_server(14840)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )
        try:
            await test_client.start()
        finally:
            await test_client.stop()

    async def test_read_attribute(self):
        await self.setup_server(14841)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )
        try:
            await test_client.start()

            # setup the attribute
            class Attribute(object):
                dim_x = 1
                dim_y = 0
                datatype = numpy.double

            prot_attr = await test_client.setup_protocol_attribute(
                ["double_R"], Attribute()
            )

            # read it from the server
            self.assertEqual(42.0, await prot_attr.read_function())
        finally:
            await test_client.stop()

    async def test_write_attribute(self):
        await self.setup_server(14842)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )
        try:
            await test_client.start()

            # setup the attribute
            class Attribute(object):
                dim_x = 1
                dim_y = 0
                datatype = numpy.double

            prot_attr = await test_client.setup_protocol_attribute(
                ["double_RW"], Attribute()
            )

            # write it to the server and read it back to verify
            await prot_attr.write_function(123.0)

            self.assertEqual(123.0, await prot_attr.read_function())
        finally:
            await test_client.stop()

    async def test_method_without_args(self):
        await self.setup_server(14843)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )

        try:
            await test_client.start()

            self.assertEqual(None, await test_client._call_method(["procedure"]))
        finally:
            await test_client.stop()

    async def test_method_with_args(self):
        await self.setup_server(14843)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )
        try:
            await test_client.start()

            self.assertEqual(
                21.0,
                await test_client._call_method(
                    ["multiply"], numpy.double(3.0), numpy.int64(7)
                ),
            )
        finally:
            await test_client.stop()

    async def test_method_with_wrong_arg_types(self):
        await self.setup_server(14844)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )
        try:
            await test_client.start()

            with self.assertRaises(RuntimeError):
                # correct signature is multiply(double,int64)
                _ = await test_client._call_method(
                    ["multiply"], numpy.double(3.0), numpy.double(7)
                )
        finally:
            await test_client.stop()

    async def test_errorring_method(self):
        await self.setup_server(14845)

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            OPCUAConnectionStatus(),
        )

        try:
            await test_client.start()

            with self.assertRaises(RuntimeError):
                await test_client._call_method(["throws"])
        finally:
            await test_client.stop()

    async def test_reconnect(self):
        await self.setup_server(14846)

        class TestOPCUAConnectionStatus(OPCUAConnectionStatus):
            def __init__(self):
                super().__init__()

                self.nr_connection_losses = 0

            def connection_lost(self):
                self.nr_connection_losses += 1

        test_client = OPCUAConnection(
            self.endpoint,
            self.namespace,
            5,
            TestOPCUAConnectionStatus(),
        )

        try:
            await test_client.start()

            # we should not have encountered any disconnects yet and be connected
            self.assertTrue(test_client.connection_status.connected)
            self.assertEqual(test_client.connection_status.nr_connection_losses, 0)

            # delete the server as a crude way to break the connection
            await self.server.stop()
            await asyncio.sleep(5)

            # we should now not be connected and the number of disconnects should be 1
            self.assertFalse(test_client.connection_status.connected)
            self.assertEqual(test_client.connection_status.nr_connection_losses, 1)

            await self.server.start()
            await asyncio.sleep(5)

            # after starting the server again we should automatically reconnect again
            self.assertTrue(test_client.connection_status.connected)
            self.assertEqual(test_client.connection_status.nr_connection_losses, 1)

        finally:
            await test_client.stop()

    async def test_connection(self):
        port = 14847

        # test connection while server does not yet exist
        result = await OPCUA_connection_tester("127.0.0.1", port)
        self.assertFalse(result)

        await self.setup_server(port)

        # test connection while server has been started
        result = await OPCUA_connection_tester("127.0.0.1", port)
        self.assertTrue(result)
