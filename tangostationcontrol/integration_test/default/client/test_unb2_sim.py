# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from asyncua import Client

from integration_test import base


class TestUNB2Sim(base.IntegrationAsyncTestCase):
    def setUp(self):
        super(TestUNB2Sim, self).setUp()

    async def test_opcua_connection(self):
        """Check if we can connect to unb2-sim"""

        client = Client("opc.tcp://unb2-sim.service.consul:4840")
        root_node = None

        await client.connect()

        try:
            root_node = client.get_root_node()
        finally:
            await client.disconnect()

        self.assertNotEqual(None, root_node)
