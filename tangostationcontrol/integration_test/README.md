# Integration Tests

Integration tests are separated into multi modules. Each module requires a
different state and configuration. These configurations are managed externally.

## Approach

A docker container is created to perform the integration tests. This
docker container is automatically connected to the network setup by jumppad /
nomad.

* Setup nomad environment through jumppad.
* Reconfigure dsconfig to use these simulators.
* Create and start the integration-test container for the specific module.

## Running

```shell
source setup.sh
sbin/run_integration_test.sh
```

## Breakpoints & Debuggers with Integration Tests

Simply place `breakpoint()` in the integration test code (not device servers silly)
and run `sbin/run_integration_test.sh --interactive`. You will be provided a bash prompt with
access to `Python`, `tox` and others. To execute the integration test as normal run:

```shell
tox -e integration
```

Specifying particular modules can be done using the `TEST_MODULE` environment variable:

```shell
TEST_MODULE=digitalbeam_performance tox -e integration
```

### Pycharm IDE

Using `--interactive` will also automatically create a Pycharm debugging session. This
allows to debug from the Pycharm IDE as if running locally. To access these you must add
a `Python Debug Server` configuration to the project:

![](../docs/source/static/images/pycharm-integration-test.png)

When configuring ensure that the IP for `IDE host name` used is the same as detected using:

```shell
ip route get 195.169.155.206 | head -1 | cut -d' ' -f7
```

This should be your local ip address provided by a DHCP server.

References:
1. https://www.jetbrains.com/help/pycharm/remote-debugging-with-product.html#remote-debug-config

## Cleanup, recovery or starting over

The integration test script should automatically teardown the environment upon
cancellation, error or when finishing. If you want the environment to persist
use `--skip-tests` or `--preserve`.

In the case of errors about existing docker networks or others you can try:

```shell
./.bin/jumppad down
rm r ~/.jumppad
```

All docker content including images, containers, networks and volumes can
be deleted using the following:

`docker stop $(docker ps | tail -n+2 | awk '{NF=1}1' | awk '{printf("%s ",$0)} END { printf "\n" }'); docker system prune --all; docker volume prune`
