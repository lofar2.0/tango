# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
import statistics
import time
import numpy

from integration_test import base
from integration_test.device_proxy import TestDeviceProxy

from tango import DevState

from tangostationcontrol.devices.base_device_classes.antennafield_device import (
    AntennaStatus,
    AntennaUse,
)
from tangostationcontrol.common.constants import MAX_ANTENNA, N_beamlets_ctrl
from tangostationcontrol.common.env_decorators import (
    restore_properties_for_devices,
    preserve_off_state_for_devices,
)

logger = logging.getLogger()


class TestDigitalbeamPerformance(base.IntegrationTestCase):
    """Integration test class for Digitalbeam device performances"""

    # The AntennaField is setup with self.NR_TILES tiles in the test configuration
    POINTING_DIRECTION = numpy.array(
        [["J2000", "0rad", "0rad"]] * N_beamlets_ctrl
    ).flatten()
    # List of devices that need to be started up / shut down during tests
    STARTUP_DEVICES = ["STAT/AFH/HBA0", "STAT/AFH/HBA1", "STAT/AFL/LBA"]

    def setUp(self):
        super(TestDigitalbeamPerformance, self).setUp()

    @restore_properties_for_devices(STARTUP_DEVICES)
    @preserve_off_state_for_devices(STARTUP_DEVICES)
    def setup_performance_test(
        self,
        antenna_field_proxies,
        beamlet_proxies,
        beam_proxies,
        sdpfirmware_proxies,
        sdp_proxies,
        recv_proxies,
        tracking=False,
    ):
        """Setup and boot proxies for the test"""
        # Setup multi SDP / recv and separate hba / lba antennafield / digitalbeam
        for item in ["HBA0", "HBA1", "LBA"]:
            sdpfirmware_proxies.append(TestDeviceProxy(f"STAT/SDPFirmware/{item}"))
            sdp_proxies.append(TestDeviceProxy(f"STAT/SDP/{item}"))
            beamlet_proxies.append(TestDeviceProxy(f"STAT/Beamlet/{item}"))
            beam_proxies.append(TestDeviceProxy(f"STAT/DigitalBeam/{item}"))
            if item[0] == "H":
                antenna_field_proxies.append(TestDeviceProxy(f"STAT/AFH/{item}"))
                recv_proxies.append(TestDeviceProxy(f"STAT/RECV{item[0]}/H0"))
            elif item[0] == "L":
                antenna_field_proxies.append(TestDeviceProxy(f"STAT/AFL/{item}"))
                recv_proxies.append(TestDeviceProxy(f"STAT/RECV{item[0]}/L0"))
                recv_proxies.append(TestDeviceProxy(f"STAT/RECV{item[0]}/L1"))

        for proxy in sdpfirmware_proxies + sdp_proxies + recv_proxies + beamlet_proxies:
            proxy.off()
            self.assertTrue(proxy.state() is DevState.OFF)
            proxy.boot()
            self.assertTrue(proxy.state() is DevState.ON)

        for proxy in antenna_field_proxies:
            # setup AntennaField
            control_mapping = [[1, i] for i in range(MAX_ANTENNA)]
            power_mapping = [[1, i] for i in range(MAX_ANTENNA)]
            sdp_mapping = [[i // 6, i % 6] for i in range(MAX_ANTENNA)]
            antenna_status = numpy.array([AntennaStatus.OK] * MAX_ANTENNA)
            antenna_use = numpy.array([AntennaUse.AUTO] * MAX_ANTENNA)
            antenna_set_mask = "1" * MAX_ANTENNA
            proxy.put_property(
                {
                    "Control_to_RECV_mapping": numpy.array(control_mapping).flatten(),
                    "Power_to_RECV_mapping": numpy.array(power_mapping).flatten(),
                    "Antenna_to_SDP_Mapping": numpy.array(sdp_mapping).flatten(),
                    "Antenna_Status": antenna_status,
                    "Antenna_Use": antenna_use,
                    "Antenna_Sets": ["ALL"],
                    "Antenna_Set_Masks": [antenna_set_mask],
                }
            )
            proxy.off()
            proxy.boot()
            self.assertEqual(MAX_ANTENNA, proxy.nr_antennas_R)
            self.assertTrue(proxy.state() is DevState.ON)

        for proxy in beam_proxies:
            proxy.off()
            self.assertTrue(proxy.state() is DevState.OFF)
            proxy.boot()
            proxy.Tracking_enabled_RW = tracking
            self.assertTrue(proxy.state() is DevState.ON)
            self.assertEqual(MAX_ANTENNA, proxy.nr_antennas_R)

    def test_digitalbeam_performance(self):
        """Test performance when manaully updating the pointing"""

        antenna_field_proxies = []
        beamlet_proxies = []
        beam_proxies = []
        sdpfirmware_proxies = []
        sdp_proxies = []
        recv_proxies = []

        self.setup_performance_test(
            antenna_field_proxies=antenna_field_proxies,
            beamlet_proxies=beamlet_proxies,
            beam_proxies=beam_proxies,
            sdpfirmware_proxies=sdpfirmware_proxies,
            sdp_proxies=sdp_proxies,
            recv_proxies=recv_proxies,
            tracking=False,
        )

        # Insert verify that all attributes have been set across
        # one another~

        results = []
        for _i in range(25):
            start_time = time.monotonic_ns()
            for proxy in beam_proxies:
                proxy.set_pointing(self.POINTING_DIRECTION)
            stop_time = time.monotonic_ns()
            results.append(stop_time - start_time)

        for beam in beam_proxies:
            self.assertEqual(0, beam.Nr_update_pointing_exceptions_R)

        logging.error(
            f"Median {statistics.median(results) / 1.e9} Stdev "
            f"{statistics.stdev(results) / 1.e9}"
        )

    def test_digitalbeam_multi_tracking_performance(self):
        """Beamtracking performance with multiple devices in single server"""

        max_deviation_percentage = 0.15

        antenna_field_proxies = []
        beamlet_proxies = []
        beam_proxies = []
        sdpfirmware_proxies = []
        sdp_proxies = []
        recv_proxies = []

        self.setup_performance_test(
            antenna_field_proxies=antenna_field_proxies,
            beamlet_proxies=beamlet_proxies,
            beam_proxies=beam_proxies,
            sdpfirmware_proxies=sdpfirmware_proxies,
            sdp_proxies=sdp_proxies,
            recv_proxies=recv_proxies,
            tracking=True,
        )

        interval = float(
            beam_proxies[0].get_property("Beam_tracking_interval")[
                "Beam_tracking_interval"
            ][0]
        )
        max_lower_error = -interval * max_deviation_percentage
        max_upper_error = interval * max_deviation_percentage

        # Allow beam tracking time to settle
        time.sleep(interval * 3)

        # We have to poll at regular interval due to not working subscribe
        # events
        for _ in range(0, 5):
            for digitalbeam in beam_proxies:
                error = digitalbeam.Pointing_error_R[0]
                self.assertTrue(
                    max_lower_error < error < max_upper_error,
                    f"Error: {error} larger than {interval * max_deviation_percentage}",
                )
                logger.info(
                    "[BeamTracking %s]  error: %s", digitalbeam.dev_name(), error
                )
                time.sleep(interval)
