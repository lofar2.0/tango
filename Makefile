# forward everything to docker-compose subdirectory
$(firstword $(MAKECMDGOALS)):
	$(MAKE) -C docker-compose $(MAKECMDGOALS)

docker-compose:
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: docker-compose tox

%::
	cd .
